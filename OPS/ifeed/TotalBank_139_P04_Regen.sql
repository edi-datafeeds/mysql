use prices;
delete from client.pday139;
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
where
client.pfisin.accid = 139 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd='USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.exchgcd='USCOMP'
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd='USCOMP'
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.exchgcd='USCOMP'
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
order by prices.lasttrade.isin;
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D' and code not in (select isin from client.pday139)
and substring(wca.scmst.primaryexchgcd,1,2) = 'US'
and wca.scmst.primaryexchgcd<>'USOTC'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=latest.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency desc, pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=isinlatest.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency desc, pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'RU'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency, isinlatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'BR'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency, isinlatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd = 'USOTCB'
and wca.scexh.exchgcd = 'USOTC'
and wca.scmst.primaryexchgcd = 'USOTC'
#and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
#or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USOTCB'
and latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, currency desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd = 'USOTCB'
and wca.scmst.primaryexchgcd = 'USOTC'
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc  limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) not in ('BR','RU','US')
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D' and code not in (select isin from client.pday139)
and substring(wca.scmst.primaryexchgcd,1,2) ='ES'
and prices.lasttrade.exchgcd='ESSIBE'
#and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
#or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
'ESSIBE'=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
'ESSIBE'=prices.lasttrade.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday139
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                          and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 139 and client.pfisin.actflag<>'D' and code not in (select isin from client.pday139)
and prices.lasttrade.exchgcd<>'USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
where
isinlatest.isin<>''
and isinlatest.isin=prices.lasttrade.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc limit 1)
and (wca.scmst.actflag<>'D' or wca.scmst.actflag is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
order by prices.lasttrade.isin;
insert ignore into client.pday139
select distinct
'' as MIC,
'' as LocalCode,
client.pfisin.code as Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
null as MktCloseDate,
'' as Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
wca.scmst.USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfisin
LEFT OUTER JOIN wca.scmst on client.pfisin.secid = wca.scmst.secid
LEFT OUTER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where client.pfisin.accid = 139 and client.pfisin.actflag<>'D' and code not in (select isin from client.pday139);
