DROP TABLE IF EXISTS `wca2`.`t698_temp`;
CREATE TABLE `wca2`.`t698_temp` (
  `ScexhID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbcID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbeID` int(10) unsigned NOT NULL DEFAULT '0',
  `Actflag` char(1) DEFAULT NULL,
  `Changed` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `Isin` char(12) DEFAULT NULL,
  `Uscode` char(9) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `IndusID` int(10) unsigned DEFAULT NULL,
  `SectyCD` char(3) DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` char(1) DEFAULT NULL,
  `PrimaryExchgCD` char(6) DEFAULT NULL,
  `BbgCurrency` char(3) DEFAULT NULL,
  `BbgCompositeGlobalID` char(12) DEFAULT NULL,
  `BbgCompositeTicker` varchar(40) DEFAULT NULL,
  `BbgGlobalID` char(12) DEFAULT NULL,
  `BbgExchangeTicker` varchar(40) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) DEFAULT NULL,
  `Micseg` char(4) DEFAULT NULL,
  `LocalCode` varchar(50) DEFAULT NULL,
  `ListStatus` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ScexhID`,`BbcID`,`BbeID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 0, 1);
set @fromdate2=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 1, 1);
set @fromdate3=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
insert ignore into wca2.t698_temp
select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when ifnull(wca.bbc.acttime,'2000-01-01') > @fromdate then wca.bbc.acttime
    when ifnull(wca.bbe.acttime,'2000-01-01') > @fromdate then wca.bbe.acttime
     when ifnull(bbebnd.acttime,'2000-01-01') > @fromdate then bbebnd.acttime
     when ifnull(wca.mktsg.acttime,'2000-01-01') > @fromdate then wca.mktsg.acttime
     when ifnull(wca.exchg.acttime,'2000-01-01') > @fromdate then wca.exchg.acttime
     when wca.issur.acttime > @fromdate2 then wca.issur.acttime
     when wca.scmst.acttime > @fromdate2 then wca.scmst.acttime
     when wca.scexh.acttime > @fromdate2 then wca.scexh.acttime
     when ifnull(wca.bbc.acttime,'2000-01-01') > @fromdate2 then wca.bbc.acttime
     when ifnull(wca.bbe.acttime,'2000-01-01') > @fromdate2 then wca.bbe.acttime
     when ifnull(bbebnd.acttime,'2000-01-01') > @fromdate2 then bbebnd.acttime
     when ifnull(wca.mktsg.acttime,'2000-01-01') > @fromdate2 then wca.mktsg.acttime
     when ifnull(wca.exchg.acttime,'2000-01-01') > @fromdate2 then wca.exchg.acttime
     when wca.issur.acttime > @fromdate3 then wca.issur.acttime
     when wca.scmst.acttime > @fromdate3 then wca.scmst.acttime
     when wca.scexh.acttime > @fromdate3 then wca.scexh.acttime
     when ifnull(wca.bbc.acttime,'2000-01-01') > @fromdate3 then wca.bbc.acttime
     when ifnull(wca.bbe.acttime,'2000-01-01') > @fromdate3 then wca.bbe.acttime
     when ifnull(bbebnd.acttime,'2000-01-01') > @fromdate3 then bbebnd.acttime
     when ifnull(wca.mktsg.acttime,'2000-01-01') > @fromdate3 then wca.mktsg.acttime
     when ifnull(wca.exchg.acttime,'2000-01-01') > @fromdate3 then wca.exchg.acttime
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
case when ifnull(wca.bbc.curencd,'') <> '' then wca.bbc.curencd
     when ifnull(wca.bbe.curencd,'') <> '' then wca.bbe.curencd
     when ifnull(bbebnd.curencd,'') <> '' then bbebnd.curencd
     else ''
     end as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' 
     then wca.bbe.bbgexhid
     when ifnull(bbebnd.bbgexhid,'')<>''
     then bbebnd.bbgexhid
     else ''
     end as BbgGlobalID,
case when ifnull(wca.bbe.bbgexhtk,'')<>''
     then wca.bbe.bbgexhtk
     when ifnull(bbebnd.bbgexhtk,'')<>''
     then bbebnd.bbgexhtk
     else ''
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbe as bbebnd on wca.scmst.secid = bbebnd.secid and ''=bbebnd.exchgcd and 'D'<> bbebnd.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
WHERE
(3>wca.sectygrp.secgrpid
or ((wca.scmst.sectycd='BND'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and (wca.exchg.cntrycd='US' and wca.scexh.exchgcd<>'USBND')))
and (issur.acttime>@fromdate3
  or scmst.acttime>@fromdate3
  or scexh.acttime>@fromdate3
  or ifnull(bbc.acttime,'2001-01-01')>@fromdate3
  or ifnull(bbe.acttime,'2001-01-01')>@fromdate3
  or ifnull(exchg.acttime,'2001-01-01')>@fromdate3
  or ifnull(mktsg.acttime,'2001-01-01')>@fromdate3
  or ifnull(bbebnd.acttime,'2001-01-01')>@fromdate3)
;