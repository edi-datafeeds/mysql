use client;
delete from client.pday140;
insert ignore into client.pday140
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and prices.lasttrade.exchgcd = wca.scexh.exchgcd 
                          and wca.scexh.liststatus<>'D' 
                          and wca.scexh.actflag<>'D'
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 140 and client.pfisin.actflag<>'D'
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and prices.lasttrade.pricedate<=date_format(now(),'%Y-%m-%d')
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where 
isinlatest.isin=prices.lasttrade.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and prices.lasttrade.pricedate<=date_format(now(),'%Y-%m-%d')
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, isinlatest.currency limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and prices.lasttrade.pricedate<=date_format(now(),'%Y-%m-%d')
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1))
order by isin, pricedate desc;
insert into client.pday140
select distinct
'' as MIC,
'' as LocalCode,
client.pfisin.code as Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
null as MktCloseDate,
'' as Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
wca.scmst.USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfisin
LEFT OUTER JOIN wca.scmst on client.pfisin.secid = wca.scmst.secid
LEFT OUTER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where client.pfisin.accid=140 and client.pfisin.actflag<>'D' and code not in (select isin from client.pday140)