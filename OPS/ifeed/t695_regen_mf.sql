DROP TABLE IF EXISTS `wca2`.`t695_mf_temp`;
CREATE TABLE `wca2`.`t695_mf_temp` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `OptionID` int(10) unsigned NOT NULL,
  `SerialID` int(10) unsigned NOT NULL,
  `ScexhID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbcID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbeID` int(10) unsigned NOT NULL DEFAULT '0',
  `Actflag` char(1) DEFAULT NULL,
  `Changed` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `Isin` char(12) DEFAULT NULL,
  `Uscode` char(9) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `IndusID` int(10) unsigned DEFAULT NULL,
  `SectyCD` char(3) DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` char(1) DEFAULT NULL,
  `PrimaryExchgCD` char(6) DEFAULT NULL,
  `BbgCurrency` char(3) DEFAULT NULL,
  `BbgCompositeGlobalID` char(12) DEFAULT NULL,
  `BbgCompositeTicker` varchar(40) DEFAULT NULL,
  `BbgGlobalID` char(12) DEFAULT NULL,
  `BbgExchangeTicker` varchar(40) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) DEFAULT NULL,
  `Micseg` char(4) DEFAULT NULL,
  `LocalCode` varchar(50) DEFAULT NULL,
  `ListStatus` varchar(1) DEFAULT NULL,
  `Date1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date1` datetime DEFAULT NULL,
  `Date2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date2` datetime DEFAULT NULL,
  `Date3Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date3` datetime DEFAULT NULL,
  `Date4Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date4` datetime DEFAULT NULL,
  `Date5Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date5` datetime DEFAULT NULL,
  `Date6Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date6` datetime DEFAULT NULL,
  `Date7Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date7` datetime DEFAULT NULL,
  `Date8Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date8` datetime DEFAULT NULL,
  `Date9Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date9` datetime DEFAULT NULL,
  `Date10Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date10` datetime DEFAULT NULL,
  `Date11Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date11` datetime DEFAULT NULL,
  `Date12Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date12` datetime DEFAULT NULL,
  `Paytype` char(1) CHARACTER SET utf8 NOT NULL,
  `RdID` int(10) DEFAULT NULL,
  `Priority` int(10) DEFAULT NULL,
  `DefaultOpt` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `OutturnSecID` int(10) unsigned DEFAULT NULL,
  `OutturnIsin` char(12) CHARACTER SET utf8 DEFAULT NULL,
  `RatioOld` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `RatioNew` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Fractions` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `Currency` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `Rate1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate1` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Rate2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate2` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Field1Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field1` varchar(255) DEFAULT NULL,
  `Field2Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field2` varchar(255) DEFAULT NULL,
  `Field3Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field3` varchar(255) DEFAULT NULL,
  `Field4Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field4` varchar(255) DEFAULT NULL,
  `Field5Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field5` varchar(255) DEFAULT NULL,
  `Field6Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field6` varchar(255) DEFAULT NULL,
  `Field7Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field7` varchar(255) DEFAULT NULL,
  `Field8Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field8` varchar(255) DEFAULT NULL,
  `Field9Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field9` varchar(255) DEFAULT NULL,
  `Field10Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field10` varchar(255) DEFAULT NULL,
  `Field11Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field11` varchar(255) DEFAULT NULL,
  `Field12Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field12` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field13Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field13` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field14Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field14` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field15Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field15` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field16Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field16` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field17Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field17` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field18Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field18` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field19Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field19` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field20Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field20` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field21Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field21` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field22Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field22` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field23Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field23` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field24Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field24` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`EventCD`,`EventID`,`OptionID`,`SerialID`,`ScexhID`,`BbcID`,`BbeID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=695;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
set @histoffset=183;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='30';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AGMDate' as Date1Type,
vtab.AGMDate as Date1,
'FYEDate' as Date2Type,
vtab.FYEDate as Date2,
'RecDate' as Date3Type,
vtab.RecDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'AGMEGM' as Field1Name,
vtab.AGMEGM as Field1,
'AGMNo' as Field2Name,
vtab.AGMNo as Field2,
'AGMTime' as Field3Name,
vtab.AGMTime as Field3,
'Add1' as Field4Name,
vtab.Add1 as Field4,
'Add2' as Field5Name,
vtab.Add2 as Field5,
'Add3' as Field6Name,
vtab.Add3 as Field6,
'Add4' as Field7Name,
vtab.Add4 as Field7,
'Add5' as Field8Name,
vtab.Add5 as Field8,
'Add6' as Field9Name,
vtab.Add6 as Field9,
'City' as Field10Name,
vtab.City as Field10,
'CntryCD' as Field11Name,
vtab.CntryCD as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_agm as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on wca.issur.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid)  
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.agmdate>=@fromdate and vtab.agmdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ann as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = wca.stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_arr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AssimilationDate' as Date1Type,
vtab.AssimilationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_assm as vtab
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd = wca.scexh.ExchgCD
inner join wca.v20c_680_scmst_mf as stab on wca.scexh.secid = stab.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.assimilationdate>=@fromdate and vtab.assimilationdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime)
      and (mpay.acttime > rd.acttime) 
     then mpay.acttime
     when rd.acttime > vtab.acttime 
     then rd.acttime
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Startdate' as Date1Type,
vtab.Startdate as Date1,
'Enddate' as Date2Type,
vtab.Enddate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Paydate' as Date4Type,
mpay.Paydate as Date4,
'Withdrawalfromdate' as Date5Type,
mpay.Withdrawalfromdate as Date5,
'Withdrawaltodate' as Date6Type,
mpay.Withdrawaltodate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'OnOffFlag' as Field1Name,
vtab.OnOffFlag as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bb as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.bbid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or mpay.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or vtab.enddate>=@fromdate and vtab.enddate<=@caldate
 or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'Filing Date' as Date2Type,
FilingDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bkrp as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
case when wca.serialseq.seqnum is not null then wca.serialseq.seqnum else 1 end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
vtab.ressecid as OutturnSecID,
'' as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Ressectycd' as Field1Name,
vtab.SectyCD as Field1,
'LaspsedPremium' as Field2Name,
vtab.LapsedPremium as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bon as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.serialseq on vtab.eventid = wca.serialseq.rdid and 2=wca.serialseq.seqnum and 'BON'=wca.serialseq.eventcd
Where
(@runmode='E' and concat(vtab.eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartSubscription' as Date1Type,
vtab.StartSubscription Date1,
'EndSubscription' as Date2Type,
vtab.EndSubscription as Date2,
'SplitDate' as Date3Type,
vtab.SplitDate as Date3,
'StartTrade' as Date4Type,
vtab.StartTrade as Date4,
'EndTrade' as Date5Type,
vtab.Endtrade as Date5,
'ExDate' as Date6Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date6,
'Recdate' as Date7Type,
wca.rd.Recdate as Date7,
'PayDate' as Date8Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
vtab.ResSecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.IssuePrice Rate1,
'' as Rate2Type,
'' Rate2,
'TradingSecID' as Field1Name,
vtab.TraSecID as Field1,
'TradingIsin' as Field2Name,
trascmst.Isin as Field2,
'OverSubscription' as Field3Name,
case when wca.vtab.oversubscription='T' then 'T' else 'F' end as OverSubscription,
'ResSectyCD' as Field4Name,
vtab.SectyCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_br as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when rd.acttime > vtab.acttime 
     then rd.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'DueDate' as Date1Type,
vtab.DueDate Date1,
'Recdate' as Date2Type,
wca.rd.recdate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'CallNumber' as Field1Name,
vtab.CallNumber as Field1,
'CurenCD' as Field2Name,
vtab.CurenCD as Field2,
'ToFacevalue' as Field3Name,
vtab.ToFacevalue as Field3,
'ToPremium' as Field4Name,
vtab.ToPremium as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_call as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or vtab.duedate>=@fromdate and vtab.duedate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01')) 
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
     then rd.acttime
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'PayDate' as Date3Type,
vtab.paydate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_caprd as vtab
INNER join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or ifnull(icc.acttime,'2000-01-01')>=@fromdate
 or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
 or rd.acttime>=@fromdate
 or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'Currency' as Field3Name,
vtab.CurenCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_consd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
 or ifnull(icc.acttime,'2000-01-01')>=@fromdate
 or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'FromDate' as Date1Type,
vtab.FromDate Date1,
'ToDate' as Date2Type,
vtab.ToDate as Date2,
'RecDate' as Date3Type,
rd.Recdate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld as RatioOld,
vtab.RatioNew as RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'Price' as Rate1Type,
vtab.Price as Rate1,
'' as Rate2Type,
'' Rate2,
'MandOptFlag' as Field1Name,
vtab.MandOptFlag as Field1,
'FXrate' as Field2Name,
vtab.FXrate as Field2,
'PartFinalFlag' as Field3Name,
vtab.PartFinalFlag as Field3,	
'ResSectyCD' as Field4Name,
vtab.ResSectyCD as Field4,
'ConvType' as Field5Name,
vtab.ConvType as Field5,
'PriceAsPercent' as Field6Name,
vtab.PriceAsPercent as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_conv as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.todate>=@fromdate and vtab.todate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartDate' as Date1Type,
vtab.StartDate Date1,
'EndDate' as Date2Type,
EndDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.eventtype as Field1,
'ResSectyCD' as Field2Name,
vtab.SectyCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ctx as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.enddate>=@fromdate and vtab.enddate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldCurenCD' as Field2Name,
vtab.OldCurenCD as Field2,
'NewCurenCD' as Field3Name,
vtab.NewCurenCD as Field3,
'OldParValue' as Field4Name,
vtab.OldParValue as Field4,	
'NewParValue' as Field5Name,
vtab.NewParValue as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_currd as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = wca.stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.OldCurenCD <> '' or vtab.NewCurenCD <> '' or vtab.OldParValue <> '' or vtab.NewParValue <> '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (mpay.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
wca.mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
wca.mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
wca.mpay.OptElectionDate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then mpay.Paytype
     when wca.mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dist as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
case when wca.divpy.OptionSerialNo is null then '1' else wca.divpy.OptionSerialNo end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (divpy.acttime > vtab.acttime) 
      and (divpy.acttime > rd.acttime) 
      and (divpy.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (divpy.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then divpy.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Paydate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Paydate2 
     ELSE null END as Date4,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'Exdate2' as Date11Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Exdate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Exdate2 
     ELSE null END as Date11,
'FXDate' as Date12Type,
wca.divpy.FXDate as Date12,
ifnull(wca.divpy.Divtype,'') as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
substring(wca.divpy.ratioold,1,instr(wca.divpy.ratioold,'.')+7) as RatioOld,
substring(wca.divpy.rationew,1,instr(wca.divpy.rationew,'.')+7) as RatioNew,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'Dapflag' as Field12Name,
case when wca.prvsc.secid is not null then 'Y' else 'N' end as Dapflag,
'InstallmentPayDate' as Field13Name,
wca.divpy.InstallmentPayDate as Field13,
'DeclCurenCD' as Field14Name,
vtab.declcurencd as Field14,
'DeclGrossAmt' as Field15Name,
vtab.declgrossamt as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or divpy.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (mpay.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01') 
      when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
wca.mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
wca.mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
wca.mpay.OptElectionDate as Date6,
'EffectiveDate' as Date7Type,
vtab.EffectiveDate as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dmrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (div_my.acttime > vtab.acttime)
      and (div_my.acttime > rd.acttime)
      and (div_my.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (div_my.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then div_my.acttime 
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'DripLastdate' as Date1Type,
vtab.DripLastdate Date1,
'DripPaydate' as Date2Type,
vtab.DripPaydate as Date2,
'CrestDate' as Date3Type,
vtab.CrestDate as Date3,
'ExDate' as Date4Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'DripReinvPrice' as Field1Name,
vtab.DripReinvPrice as Field1,
'CntryCD' as Field2Name,
vtab.CntryCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_drip as vtab
inner join wca.div_my on vtab.divid=wca.div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on wca.div_my.rdid=wca.rdprt.rdid and 'DIV' = wca.rdprt.eventtype
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.CntryCD and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.ExchgCD
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and 'DIV' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or div_my.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,	
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when vtab.rationew<>'' and vtab.curencd<>'' then 'B'
     when vtab.curencd<>'' then 'C'
     when vtab.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
vtab.Minprice Rate1,
'MaxPrice' as Rate2Type,
vtab.MaxPrice Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'MaxQlyQty' as Field2Name,
vtab.MaxQlyQty as Field2,
'MaxAcpQty' as Field3Name,
vtab.MaxAcpQty as Field3,
'TradingSecID' as Field4Name,
vtab.TraSecID as Field4,
'TradingIsin' as Field5Name,
trascmst.Isin as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dvst as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or rd.acttime>=@fromdate 
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate 
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.EntIssuePrice Rate1,
'' as Rate2Type,
'' Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'OverSubscription' as Field2Name,
case when wca.vtab.OverSubscription='T' then 'T' else 'F' end as OverSubscription,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ent as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (div_my.acttime > vtab.acttime) 
      and (div_my.acttime > rd.acttime) 
      and (div_my.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (div_my.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then div_my.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Frankflag' as Field1Name,
vtab.Frankflag as Field1,
'FrankDiv' as Field2Name,
vtab.FrankDiv as Field2,
'UnFrankDiv' as Field3Name,
vtab.UnFrankDiv as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_frank as vtab
inner join wca.div_my on vtab.divid=div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and 'DIV' = wca.rdprt.eventtype
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.CntryCD and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.ExchgCD
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and 'DIV' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or div_my.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and vtab.cntrycd <> 'GB';
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.Actflag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'FTTStartDate' as Date1Type,
vtab.StartDate as Date1,
'FTTEndDate' as Date2Type,
vtab.Enddate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ftt as vtab
inner join v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on vtab.secid=wca.scexh.secid and vtab.cntrycd=substring(wca.scexh.exchgcd,1,2)
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.enddate>=@fromdate and vtab.enddate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'OldFYStartDate' as Date2Type,
vtab.OldFYStartDate as Date2,
'OldFYEndDate' as Date3Type,
vtab.OldFYEndDate as Date3,
'NewFYStartDate' as Date4Type,
vtab.NewFYStartDate as Date4,
'NewFYEndDate' as Date5Type,
vtab.NewFYEndDate as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.Eventtype as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_fychg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldIsin' as Field2Name,
vtab.OldIsin as Field2,
'NewIsin' as Field3Name,
vtab.NewIsin as Field3,
'OldUscode' as Field4Name,
vtab.OldUscode as Field4,
'NewUscode' as Field5Name,
vtab.NewUscode as Field5,	
'RelEventID' as Field6Name,
vtab.RelEventID as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_icc as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.OldIsin <> '' or vtab.NewIsin <> '' or vtab.OldUscode <> '' or vtab.NewUscode <> '')
and not (vtab.eventtype = 'CORR' and vtab.NewUscode = '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.InChgDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldCntryCD' as Field2Name,
vtab.OldCntryCD as Field2,
'NewCntryCD' as Field3Name,
vtab.NewCntryCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_inchg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.inchgdate>=@fromdate and vtab.inchgdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.OldCntryCD <> '' or vtab.NewCntryCD <> '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.NameChangeDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'IssOldName' as Field2Name,
vtab.IssOldName as Field2,
'IssNewName' as Field3Name,
vtab.IssNewName as Field3,
'LegalName' as Field4Name,
case when wca.vtab.LegalName='T' then 'T' else 'F' end as LegalName,
'MeetingDateFlag' as Field5Name,
case when wca.vtab.MeetingDateFlag='T' then 'T' else 'F' end as MeetingDateFlag,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ischg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.namechangedate>=@fromdate and vtab.namechangedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.IssOldName <> '' or vtab.IssNewName <> '' or vtab.LegalName <> '' or vtab.MeetingDateFlag <> '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'Regdate' as Date2Type,
vtab.Regdate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'LaType' as Field1Name,
vtab.LaType as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lawst as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLocalCode' as Field2Name,
vtab.OldLocalCode as Field2,
'NewLocalCode' as Field3Name,
vtab.NewLocalCode as Field3,
'RelEventID' as Field4Name,
vtab.RelEventID as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lcc as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.ExchgCD
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.OldLocalCode <> '' or vtab.NewLocalCode <> '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
     then mpay.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Recdate' as Date1Type,
vtab.Rddate as Date1,
'Paydate' as Date2Type,
wca.mpay.paydate as Date2,
'OptElectionDate' as Date3Type,
wca.mpay.OptElectionDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
null as RdID,
null as Priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'ResSectyCD' as Field1Name,
mpay.SectyCD as Field1,
'Liquidator' as Field2Name,
vtab.Liquidator as Field2,
'LiqAdd1' as Field3Name,
vtab.LiqAdd1 as Field3,
'LiqAdd2' as Field4Name,
vtab.LiqAdd2 as Field4,
'LiqAdd3' as Field5Name,
vtab.LiqAdd3 as Field5,
'LiqAdd4' as Field6Name,
vtab.LiqAdd4 as Field6,
'LiqAdd5' as Field7Name,
vtab.LiqAdd5 as Field7,
'LiqAdd6' as Field8Name,
vtab.LiqAdd6 as Field8,
'LiqCity' as Field9Name,
vtab.LiqCity as Field9,
'LiqCntryCD' as Field10Name,
vtab.LiqCntryCD as Field10,
'LiqTel' as Field11Name,
vtab.LiqTel as Field11,
'LiqFax' as Field12Name,
vtab.LiqFax as Field12,
'LiqEmail' as Field13Name,
vtab.LiqEmail as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_liq as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst_mf as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.mpay on vtab.liqid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or vtab.rddate>=@fromdate and vtab.rddate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='D' or wca.scexh.ListStatus='S' then wca.scexh.ListStatus else upper('L') end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'NotificationDate' as Date2Type,
vtab.NotificationDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLStatStatus' as Field2Name,
case when vtab.OldLStatStatus = '' or vtab.OldLStatStatus = 'N' or vtab.OldLStatStatus = 'R'
     then 'L'
     else vtab.OldLStatStatus 
     end as Field2,
'LStatStatus' as Field3Name,
vtab.LStatStatus as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lstat as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.ExchgCD
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or (stab.acttime>=@fromdate and stab.isin <> '' and vtab.acttime > date_sub(stab.acttime, interval 60 day))
or (wca.scexh.acttime>=@fromdate and wca.scexh.LocalCode <> '' and vtab.acttime > date_sub(wca.scexh.acttime, interval 60 day))
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLot' as Field2Name,
vtab.OldLot as Field2,
'OldMinTrdQty' as Field3Name,
vtab.OldMinTrdQty as Field3,
'NewLot' as Field4Name,
vtab.NewLot as Field4,
'NewMinTrdgQty' as Field5Name,
vtab.NewMinTrdgQty as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ltchg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.ExchgCD
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.OldLot <> '' or vtab.NewLot <> '' or vtab.OldMinTrdQty <> '' or vtab.NewMinTrdgQty <> '' );
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'EventType' as Field1Name,
vtab.EventType as Field1,
'OldMIC' as Field2Name,
oldseg.MIC as Field2,
'NewMIC' as Field3Name,
case when newseg.MIC = '' and newseg.mktsegment = ''
     then wca.exchg.MIC
     else newseg.MIC
     end as Field3,
'OldMktsegment' as Field4Name,
oldseg.Mktsegment as Field4,
'NewMktsegment' as Field5Name,
newseg.Mktsegment as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mkchg as vtab
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
inner join wca.v20c_680_scmst_mf as stab on wca.scexh.secid = wca.stab.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.mktsg as oldseg on vtab.oldmktsgid = oldseg.mktsgid
left outer join wca.mktsg as newseg on vtab.newmktsgid = newseg.mktsgid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (mpay.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'AppointedDate' as Date4Type,
vtab.AppointedDate as Date4,
'EffectiveDate' as Date5Type,
vtab.EffectiveDate as Date5,
'WithdrawalFromdate' as Date6Type,
wca.mpay.WithdrawalFromdate as Date6,
'WithdrawalTodate' as Date7Type,
wca.mpay.WithdrawalTodate as Date7,
'OptElectionDate' as Date8Type,
mpay.OptElectionDate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MrgrStatus' as Field1Name,
vtab.MrgrStatus as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (vtab.acttime > stab.acttime) 
      and (vtab.acttime > scexh.acttime) 
     then vtab.acttime 
     when (stab.acttime > scexh.acttime)  
     then stab.acttime  
     else scexh.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_nlist as vtab
inner join wca.scexh on wca.vtab.scexhid = wca.scexh.ScexhID
inner join wca.v20c_680_scmst_mf as stab on wca.scexh.secid = wca.stab.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or (stab.acttime>=@fromdate and stab.isin <> '' and vtab.acttime > date_sub(stab.acttime, interval 60 day))
or (wca.scexh.acttime>=@fromdate and wca.scexh.LocalCode <> '' and vtab.acttime > date_sub(wca.scexh.acttime, interval 60 day))
or wca.scexh.listdate>=@fromdate and wca.scexh.listdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (mpay.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartDate' as Date1Type,
vtab.StartDate Date1,
'EndDate' as Date2Type,
vtab.EndDate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Paydate' as Date4Type,
mpay.Paydate as Date4,
'Withdrawalfromdate' as Date5Type,
mpay.Withdrawalfromdate as Date5,
'Withdrawaltodate' as Date6Type,
mpay.Withdrawaltodate as Date6,
'OptElectionDate' as Date7Type,
wca.mpay.OptElectionDate as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MinQlyQty' as Field1Name,
wca.mpay.MinQlyQty as Field1,
'MaxQlyQty' as Field2Name,
wca.mpay.MaxQlyQty as Field2,
'WithdrawalRights' as Field3Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'MinOfrQty' as Field4Name,
wca.mpay.MinOfrQty as Field4,
'MaxOfrQty' as Field5Name,
wca.mpay.MaxOfrQty as Field5,
'MinAcpQty' as Field6Name,
wca.vtab.MinAcpQty as Field6,
'MaxAcpQty' as Field7Name,
wca.vtab.MaxAcpQty as Field7,
'Buyin' as Field8Name,
wca.vtab.Buyin as Field8,
'BuyInCurenCD' as Field9Name,
wca.vtab.BuyInCurenCD as Field9,
'BuyInPrice' as Field10Name,
wca.vtab.BuyInPrice as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_oddlt as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or vtab.enddate>=@fromdate and vtab.enddate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (div_my.acttime > vtab.acttime)
      and (div_my.acttime > rd.acttime)
      and (div_my.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (div_my.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then div_my.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'NonPID' as Field1Name,
vtab.NonPID as Field1,
'PID' as Field2Name,
vtab.PID as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_pid as vtab
inner join wca.div_my on vtab.divid=div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and 'DIV' = wca.rdprt.eventtype
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.CntryCD and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.ExchgCD
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and 'DIV' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate 
or div_my.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and vtab.cntrycd = 'GB';
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'OfferOpens' as Date4Type,
vtab.OfferOpens as Date4,
'OfferCloses' as Date5Type,
vtab.OfferCloses as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
MinPrice Rate1,
'MaxPrice' as Rate2Type,
vtab.MaxPrice Rate2,
'POMinPercent' as Field1Name,
POMinPercent as Field1,
'POMaxPercent' as Field2Name,
POMaxPercent as Field2,
'MinOfrQty' as Field3Name,
vtab.MinOfrQty as Field3,
'MaxOfrQty' as Field4Name,
vtab.MaxOfrQty as Field4,
'TndrStrkPrice' as Field5Name,
vtab.TndrStrkPrice as Field5,
'TndrPriceStep' as Field6Name,
vtab.TndrPriceStep as Field6,
'MinQlyQty' as Field7Name,
vtab.MinQlyQty as Field7,
'MaxQlyQty' as Field8Name,
vtab.MaxQlyQty as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'SealedBid' as Field11Name,
case when wca.vtab.SealedBid='T' then 'T' else 'F' end as SealedBid,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_po as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'EventType' as Field1Name,
vtab.eventtype as Field1,
'OldExchgCD' as Field2Name,
vtab.OldExchgCd as Field2,
'NewExchgCD' as Field3Name,
vtab.NewExchgCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_prchg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on vtab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
case when wca.serialseq.seqnum is not null then wca.serialseq.seqnum else 1 end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
Null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when vtab.rationew<>'' and vtab.curencd<>'' then 'B'
     when vtab.curencd<>'' then 'C'
     when vtab.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
vtab.MinPrice Rate1,
'MaxPrice' as Rate2Type,
MaxPrice Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'OffereeIssID' as Field2Name,
vtab.OffereeIssID as Field2,
'OffereeName' as Field3Name,
vtab.OffereeName as Field3,
'TndrStrkPrice' as Field4Name,
vtab.TndrStrkPrice as Field4,
'TndrPriceStep' as Field5Name,
vtab.TndrPriceStep as Field5,
'MinQlyQty' as Field6Name,
vtab.MinQlyQty as Field6,
'MaxQlyQty' as Field7Name,
vtab.MaxQlyQty as Field7,
'MinAcpQty' as Field8Name,
vtab.MinAcpQty as Field8,
'MaxAcpQty' as Field9Name,
vtab.MaxAcpQty as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_prf as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
left outer join wca.serialseq on vtab.eventid = wca.serialseq.rdid and 2=wca.serialseq.seqnum and 'PRF'=wca.serialseq.eventcd
Where
(@runmode='E' and concat(vtab.eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or rd.acttime>=@fromdate 
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate 
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.eventtype as Field1,
'PVCurency' as Field2Name,
vtab.Curencd as Field2,
'OldParValue' as Field3Name,
vtab.OldParValue as Field3,
'NewParValue' as Field4Name,
vtab.NewParValue as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_pvrd as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when rd.acttime > vtab.acttime 
     then rd.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'PayDate' as Date3Type,
vtab.Cspydate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.CurenCD as Currency,
'CashBak' as Rate1Type,
vtab.CashBak Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_rcap as vtab
INNER join wca.v20c_680_scmst_mf as stab ON stab.SecID = vtab.SecID
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or rd.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'RedemDate' as Date1Type,
RedemDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.CurenCD as Currency,
'RedemPrice' as Rate1Type,
vtab.RedemPrice Rate1,
'' as Rate2Type,
'' Rate2,
'MandOptFlag' as Field1Name,
vtab.MandOptFlag as Field1,
'PartFinal' as Field2Name,
vtab.PartFinal as Field2,
'Redemtype' as Field3Name,
vtab.Redemtype as Field3,
'AmountRedeemed' as Field4Name,
vtab.AmountRedeemed as Field4,
'RedemPremium' as Field5Name,
vtab.RedemPremium as Field5,
'PriceAsPercent' as Field6Name,
vtab.PriceAsPercent as Field6,
'PremiumAsPercent' as Field7Name,
vtab.PremiumAsPercent as Field7,
'PoolFactor' as Field8Name,
vtab.PoolFactor as Field8,
'RedemPercent' as Field9Name,
vtab.RedemPercent as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_redem as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.redemdate>=@fromdate and vtab.redemdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
case when wca.serialseq.seqnum is not null then wca.serialseq.seqnum else 1 end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'StartTrade' as Date6Type,
vtab.StartTrade as Date6,
'EndTrade' as Date7Type,
vtab.EndTrade as Date7,
'Splitdate' as Date8Type,
Splitdate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
substring(vtab.ratioold,1,instr(vtab.ratioold,'.')+7) as RatioOld,
substring(vtab.rationew,1,instr(vtab.rationew,'.')+7) as RatioNew,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.IssuePrice Rate1,
'LapsedPremium' as Rate2Type,
LapsedPremium Rate2,
'TradingSecID' as Field1Name,
vtab.TraSecID as Field1,
'TradingIsin' as Field2Name,
trascmst.Isin as Field2,
'PPSecID' as Field3Name,
vtab.PPSecID as Field3,
'PPIsin' as Field4Name,
ppscmst.isin as Field4,
'OverSubscription' as Field5Name,
case when wca.vtab.OverSubscription='T' then 'T' else 'F' end as OverSubscription,
'ResSectyCD' as Field6Name,
vtab.SectyCD as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_rts as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
left outer join wca.scmst as ppscmst on vtab.ppsecid = ppscmst.secid
left outer join wca.serialseq on vtab.eventid = wca.serialseq.rdid and 2=wca.serialseq.seqnum and 'RTS'=wca.serialseq.eventcd
Where
(@runmode='E' and concat(vtab.eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate 
or rd.acttime>=@fromdate 
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate 
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or (resscmst.acttime>=@fromdate and resscmst.isin <>'' and vtab.acttime > date_sub(resscmst.acttime, interval 60 day))
or (trascmst.acttime>=@fromdate and trascmst.isin <>'' and vtab.acttime > date_sub(trascmst.acttime, interval 60 day))
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.DateofChange as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'SecOldName' as Field2Name,
vtab.SecOldName as Field2,
'SecNewName' as Field3Name,
vtab.SecNewName as Field3,
'OldSectyCD' as Field4Name,
vtab.OldSectyCD as Field4,
'NewSectyCD' as Field5Name,
vtab.NewSectyCD as Field5,	
'OldRegS144A' as Field6Name,
vtab.OldRegS144A as Field6,
'NewRegS144A' as Field7Name,
vtab.NewRegS144A as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_scchg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.dateofchange>=@fromdate and vtab.dateofchange<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
and (vtab.SecOldName <> '' or vtab.SecNewName <> '' or vtab.OldSectyCD <> '' or vtab.NewSectyCD <> '' or vtab.OldRegS144A <> '' or vtab.NewRegS144A <> '');
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_scswp as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate 
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime 
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime 
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'OldCurrency' as Field3Name,
vtab.OldCurenCD as Field3,
'NewCurrency' as Field4Name,
vtab.NewCurenCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_sd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst_mf as stab on wca.rd.secid = wca.stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or ifnull(icc.acttime,'2000-01-01')>=@fromdate
or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
resscmst.secid as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioOld,
vtab.RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Relevent' as Field1Name,
vtab.Eventtype as Field1,
'ResSectyCD' as Field2Name,
vtab.SectyCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
FROM wca.v10s_secrc as vtab
INNER join wca.v20c_680_scmst_mf as stab ON stab.SecID = vtab.SecID
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst ON vtab.ressecID = resscmst.SecID
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
     then mpay.acttime 
     when rd.acttime > vtab.acttime 
     then rd.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Opendate' as Date1Type,
vtab.opendate as Date1,
'Closedate' as Date2Type,
vtab.closedate as Date2,
'Recdate' as Date3Type,
wca.rd.Recdate as Date3,
'Cmacqdate' as Date4Type,
vtab.Cmacqdate as Date4,
'Paydate' as Date5Type,
wca.mpay.Paydate as Date5,
'Optelectiondate' as Date6Type,
wca.mpay.Optelectiondate as Date6,
'Withdrawalfromdate' as Date7Type,
wca.mpay.Withdrawalfromdate as Date7,
'Withdrawaltodate' as Date8Type,
wca.mpay.Withdrawaltodate as Date8,
'UnconditionalDate' as Date9Type,
vtab.UnconditionalDate as Date9,
'ToDate' as Date10Type,
wca.rd.ToDate as Date10,
'RegistrationDate' as Date11Type,
wca.rd.RegistrationDate as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'TkovrStatus' as Field1Name,
vtab.TkovrStatus as Field1,
'OfferorIssID' as Field2Name,
vtab.OfferorIssID as Field2,
'OfferorName' as Field3Name,
vtab.OfferorName as Field3,
'Hostile' as Field4Name,
case when wca.vtab.Hostile='T' then 'T' else 'F' end as Hostile,
'MiniTkovr' as Field5Name,
case when wca.vtab.MiniTkovr='T' then 'T' else 'F' end as MiniTkovr,
'ResSecTyCD' as Field6Name,
mpay.SecTyCD as Field6,
'TargetQuantity' as Field7Name,
vtab.TargetQuantity as Field7,
'TargetPercent' as Field8Name,
vtab.TargetPercent as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'PreOfferQty' as Field11Name,
vtab.PreOfferQty as Field11,
'PreOfferPercent' as Field12Name,
vtab.PreOfferPercent as Field12,
'TndrStrkPrice' as Field13Name,
mpay.TndrStrkPrice as Field13,
'TndrStrkStep' as Field14Name,
mpay.TndrStrkStep as Field14,
'MinQlyQty' as Field15Name,
mpay.MinQlyQty as Field15,
'MaxQlyQty' as Field16Name,
mpay.MaxQlyQty as Field16,
'MinOfrQty' as Field17Name,
mpay.MinOfrQty as Field17,
'MaxOfrQty' as Field18Name,
mpay.MaxOfrQty as Field18,
'DutchAuction' as Field19Name,
case when wca.mpay.DutchAuction='T' then 'T' else 'F' end as DutchAuction,
'WithdrawalRights' as Field20Name,
mpay.WithdrawalRights as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_tkovr as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.tkovrid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or vtab.closedate>=@fromdate and vtab.closedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'' as Date1Type,
null as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
vtab.DRRatio as RatioOld,
vtab.USRatio as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'UnSecID' as Field1Name,
vtab.UnSecID as Field1,
'SpnFlag' as Field2Name,
vtab.SpnFlag as Field2,
'DRtype' as Field3Name,
vtab.DRtype as Field3,
'DepBank' as Field4Name,
vtab.DepBank as Field4,
'LevDesc' as Field5Name,
vtab.LevDesc as Field5,	
'OtherDepBank' as Field6Name,
vtab.OtherDepBank as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dprcp as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldDRRatio' as Field2Name,
vtab.OldDRRatio as Field2,
'NewDRRatio' as Field3Name,
vtab.NewDRRatio as Field3,
'OldUNRatio' as Field4Name,
vtab.OldUNRatio as Field4,
'NewUNRatio' as Field5Name,
vtab.NewUNRatio as Field5,
'OldUNSecID' as Field6Name,
vtab.OldUNSecID as Field6,
'NewUNSecID' as Field7Name,
vtab.NewUNSecID as Field7,	
'OldDepbank' as Field8Name,
vtab.OldDepbank as Field8,
'NewDepbank' as Field9Name,
vtab.NewDepbank as Field9,
'OldDRtype' as Field10Name,
vtab.OldDRtype as Field10,
'NewDRtype' as Field11Name,
vtab.NewDRtype as Field11,
'OldLevel' as Field12Name,
vtab.OldLevel as Field12,
'NewLevel' as Field13Name,
vtab.NewLevel as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_drchg as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_mf_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.effectivedate as Date1,
'OldOutstandingDate' as Date2Type,
vtab.OldOutstandingDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' as Rate1,
'' as Rate2Type,
'' as Rate2,
'EventType' as Field1Name,
vtab.EventType as Field1,
'OldSos' as Field2Name,
vtab.OldSos as Field2,
'NewSos' as Field3Name,
vtab.NewSos as Field3,
'RelEventID' as Field4Name,
vtab.RelEventID as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_shoch as vtab
inner join wca.v20c_680_scmst_mf as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));

