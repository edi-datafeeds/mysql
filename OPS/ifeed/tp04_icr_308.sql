DROP TABLE IF EXISTS `client`.`tp04_308`;
CREATE TABLE `client`.`tp04_308` (
  `MIC` varchar(6) DEFAULT NULL,
  `LocalCode` varchar(80) NOT NULL DEFAULT '',
  `Isin` varchar(12) DEFAULT NULL,
  `Currency` char(3) NOT NULL DEFAULT '',
  `PriceDate` date DEFAULT NULL,
  `Open` varchar(20) DEFAULT NULL,
  `High` varchar(20) DEFAULT NULL,
  `Low` varchar(20) DEFAULT NULL,
  `Close` varchar(20) DEFAULT NULL,
  `Mid` varchar(20) DEFAULT NULL,
  `Ask` varchar(20) DEFAULT NULL,
  `Last` varchar(20) DEFAULT NULL,
  `Bid` varchar(20) DEFAULT NULL,
  `BidSize` varchar(20) DEFAULT NULL,
  `AskSize` varchar(20) DEFAULT NULL,
  `TradedVolume` varchar(50) DEFAULT NULL,
  `SecID` int(11) NOT NULL DEFAULT '0',
  `MktCloseDate` date DEFAULT NULL,
  `Volflag` char(1) DEFAULT NULL,
  `Issuername` varchar(70) DEFAULT NULL,
  `SectyCD` char(3) DEFAULT NULL,
  `SecurityDesc` varchar(100) DEFAULT NULL,
  `Sedol` varchar(7) DEFAULT NULL,
  `uscode` varchar(9) DEFAULT NULL,
  `PrimaryExchgCD` varchar(6) DEFAULT NULL,
  `ExchgCD` varchar(9) NOT NULL,
  `TradedValue` varchar(50) DEFAULT NULL,
  `TotalTrades` varchar(50) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ExchgCD`,`SecID`,`Currency`,`LocalCode`) USING BTREE,
  KEY `IX_LocalCode` (`MIC`,`LocalCode`),
  KEY `IX_SECID` (`SecID`),
  KEY `IX_Sedol` (`Sedol`),
  KEY `IX_ISIN` (`Isin`) USING BTREE,
  KEY `IX_Localcode_exch` (`LocalCode`,`ExchgCD`),
  KEY `IX_Uscode` (`uscode`),
  KEY `IX_SectyCD` (`SectyCD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @fdate=(select max(todate) from client.feedlog where clientname='Athena'); 
set @tdate=(select date_sub(now(), interval 10 minute)); 
insert ignore into client.tp04_308
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
inner join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
where
prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -31 day)))
and accid=308
and client.pfisin.actflag<>'D'
and (comment not like 'terms%' or comment is null)
and prices.lasttrade.exchgcd in 
(select exchgcd from prices.markets
where lastoutput>@fdate and lastoutput<@tdate
and marketcode not like '%_MF'
and marketcode not like '%_IX'
and marketcode not like '%_FI'
and marketcode not like '%_UF'
and marketcode not like '%_UM'
and marketcode not like '%_UX');
insert into client.feedlog
(clientname, todate)
select
'Athena',
@tdate;