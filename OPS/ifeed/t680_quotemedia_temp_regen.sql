-- # Drop t680_quotemedia_temp
DROP TABLE IF EXISTS `wca2`.`t680_quotemedia_temp`;

-- # Create t680_quotemedia_temp
CREATE TABLE wca2.t680_quotemedia_temp like wca2.t680_temp;

-- # Insert into t680_quotemedia_temp everything from wca2.t680_temp
insert ignore into wca2.t680_quotemedia_temp select * from wca2.t680_temp where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH';

-- # Insert into t680_quotemedia_temp everything from wca2.t680_mf_temp
insert ignore into wca2.t680_quotemedia_temp select * from wca2.t680_mf_temp where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH';

-- # Insert into t680_quotemedia_temp everything from wca2.t680_temp_prf
insert ignore into wca2.t680_quotemedia_temp select * from wca2.t680_temp_prf where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH';

-- # Set field 23 name as NewLocalcode for BON, CONSD and SD
update wca2.t680_quotemedia_temp
set Field23Name='NewLocalcode'
where (eventcd='BON' or eventcd='CONSD' or eventcd='SD');

-- # Populate field 23 for CONSD and SD
update wca2.t680_quotemedia_temp
left outer join wca.lcc on wca2.t680_quotemedia_temp.EventID = wca.lcc.ReleventID 
                        and wca2.t680_quotemedia_temp.EventCD = wca.lcc.EventType
                        and wca2.t680_quotemedia_temp.ExchgCD = wca.lcc.ExchgCD
set wca2.t680_quotemedia_temp.Field23=
case when (wca.lcc.ReleventID is null or wca.lcc.OldLocalcode = '') 
     then '' 
     else wca.lcc.NewLocalcode 
     end                      
where (eventcd='CONSD' or eventcd='SD')
and (wca.lcc.lccid is null or wca.lcc.lccid = 
(select lccid from wca.lcc as sublcc
where
sublcc.releventid =  wca2.t680_quotemedia_temp.rdid
and sublcc.eventtype=wca2.t680_quotemedia_temp.EventCD
and sublcc.exchgcd=wca2.t680_quotemedia_temp.exchgcd
and sublcc.actflag<>'c'
and sublcc.actflag<>'d'
order by sublcc.effectivedate desc limit 1));

-- # Populate field 23 for BON
update wca2.t680_quotemedia_temp
left outer join wca.scexh as resLocal on wca2.t680_quotemedia_temp.outturnsecid = resLocal.secid
                        and wca2.t680_quotemedia_temp.ExchgCD = resLocal.ExchgCD
set wca2.t680_quotemedia_temp.Field23=reslocal.localcode                        
where (eventcd='BON')
and ifnull(wca2.t680_quotemedia_temp.outturnsecid,wca2.t680_quotemedia_temp.secid)<>wca2.t680_quotemedia_temp.secid;

-- # Set field 16 name as USDRatetoCurrency for DIV
update wca2.t680_quotemedia_temp
set Field16Name='USDRateToCurrency'
where eventcd='DIV';

-- # Populate field 16 for DIV
update wca2.t680_quotemedia_temp
left outer join wca.divpy on wca2.t680_quotemedia_temp.eventid = wca.divpy.divid
                          and wca2.t680_quotemedia_temp.optionid = wca.divpy.optionid
set wca2.t680_quotemedia_temp.Field16=wca.divpy.USDRateToCurrency
where eventcd='DIV';

-- # Add qualified dividend event
insert ignore into wca2.t680_quotemedia_temp
select
UPPER('DIVRC') AS EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
case when wca.divpy.OptionSerialNo is null then '1' else wca.divpy.OptionSerialNo end as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (frank.acttime > vtab.acttime) 
      and (frank.acttime > rd.acttime) 
      and (frank.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (frank.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then frank.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' as Rate1,
'' as Rate2Type,
'' as Rate2,
'Taxedflag' as Field1Name,
wca.frank.Frankflag as Field1,
'TaxedAmount' as Field2Name,
wca.frank.FrankDiv as Field2,
'UnTaxedAmount' as Field3Name,
wca.frank.UnFrankDiv as Field3,
'CapitalGain' as Field4Name,
wca.frank.CapitalGain as Field4,
'Under1250Section' as Field5Name,
wca.frank.Under1250Section as Field5,
'ReturnOfCapital' as Field6Name,
wca.frank.ReturnOfCapital as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
INNER JOIN wca.frank on vtab.divid = wca.frank.divid and wca.exchg.cntrycd = wca.frank.cntrycd
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
Where
vtab.actflag<>'D'
and ((wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and wca.frank.cntrycd='US'
and (wca.frank.acttime >=(select max(feeddate) from wca.tbl_opslog)
or wca.rd.acttime>=(select max(feeddate) from wca.tbl_opslog)
or ifnull(wca.exdt.acttime,'2000-01-01')>=(select max(feeddate) from wca.tbl_opslog)
or ifnull(pexdt.acttime,'2000-01-01')>=(select max(feeddate) from wca.tbl_opslog)
or vtab.acttime >=(select max(feeddate) from wca.tbl_opslog));

update wca2.t680_quotemedia_temp
set localcode=
case when LocalCode='BPRAP'
     then LocalCode
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),
                        replace(substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2),'PR','/PR'))
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-4),'/PR/CL')
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),
                        replace(substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3),'PR','/PR/'))
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-4),
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,4),'PR','/PR/')
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-5),'/PR/',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,1),'/',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2))
     when sectycd='WAR' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'/WS/',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1))
     when sectycd='WAR' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),'/WS')
     when (sectycd='CVR' or sectycd='TRT') and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = '/RT'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'/RT')
     when (sectycd='CVR' or sectycd='TRT') and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),'/RT')
     when (sectycd='EQS' or sectycd='DR') and securitydesc like '%Class A%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'A'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'/A')
     when (sectycd='EQS' or sectycd='DR') and securitydesc like '%Class B%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'B'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'/B')
     when substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'/WI')
     when substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'/WD')
     when securitydesc like 'Unit%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'U'
          and sectycd='STP'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'/U')
     else LocalCode
     end
where
exchgCD='USNYSE';
