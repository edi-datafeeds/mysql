DROP TABLE IF EXISTS `wca2`.`t680i_dtcc_temp`;

CREATE TABLE wca2.t680i_dtcc_temp like wca2.t680_temp;

insert into wca2.t680i_dtcc_temp select * from wca2.t680_temp where changed > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH';

ALTER TABLE `wca2`.`t680i_dtcc_temp` 
ADD COLUMN `Date13Type` VARCHAR(20) CHARACTER SET utf8 NOT NULL AFTER `Date12`,
ADD COLUMN `Date13` datetime DEFAULT NULL AFTER `Date13Type`;

update wca2.t680i_dtcc_temp
set Date13Type='DueBillsRedemDate'
where (eventcd='ARR' or eventcd='BON' or eventcd='BR' or eventcd='CONSD' or eventcd='DIST' or eventcd='DIV'
or eventcd='DMRGR' or eventcd='DRIP' or eventcd='DVST' or eventcd='ENT' or eventcd='FRANK' or eventcd='MRGR'
or eventcd='ODDLT' or eventcd='PO' or eventcd='PRF' or eventcd='RTS' or eventcd='SCSWP' 
or eventcd='SD');

update wca2.t680i_dtcc_temp
left outer join wca.exdt on wca2.t680i_dtcc_temp.rdid = wca.exdt.rdid 
                         and wca2.t680i_dtcc_temp.ExchgCD = wca.exdt.exchgcd 
                         and wca2.t680i_dtcc_temp.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca2.t680i_dtcc_temp.rdid = pexdt.rdid 
                                  and wca2.t680i_dtcc_temp.primaryexchgcd = pexdt.exchgcd 
                                  and wca2.t680i_dtcc_temp.eventcd = pexdt.eventtype
set wca2.t680i_dtcc_temp.Date13=
case when wca.exdt.rdid is not null 
     then wca.exdt.DueBillsRedemDate
     else wca.pexdt.DueBillsRedemDate
     end
where (eventcd='ARR' or eventcd='BON' or eventcd='BR' or eventcd='CONSD' or eventcd='DIST' or eventcd='DIV'
or eventcd='DMRGR' or eventcd='DRIP' or eventcd='DVST' or eventcd='ENT' or eventcd='FRANK' or eventcd='MRGR'
or eventcd='ODDLT' or eventcd='PO' or eventcd='PRF' or eventcd='RTS' or eventcd='SCSWP' 
or eventcd='SD');