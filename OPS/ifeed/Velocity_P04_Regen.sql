use client;
delete from client.pday113;
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'US'
and wca.scmst.primaryexchgcd<>'USOTC'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=latest.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency desc, pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=uslatest.exchgcd
and uslatest.exchgcd=wca.scexh.exchgcd
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.currency desc, pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'RU'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and uslatest.exchgcd=wca.scexh.exchgcd
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.currency, uslatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'BR'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and uslatest.exchgcd=wca.scexh.exchgcd
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.currency, uslatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D'
and prices.lasttrade.exchgcd = 'USOTCB'
and wca.scexh.exchgcd = 'USOTC'
and wca.scmst.primaryexchgcd = 'USOTC'
#and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
#or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USOTCB'
and latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, currency desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and uslatest.exchgcd = 'USOTCB'
and wca.scmst.primaryexchgcd = 'USOTC'
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.pricedate desc, uslatest.currency desc  limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) not in ('BR','RU','US')
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and uslatest.exchgcd=wca.scexh.exchgcd
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.pricedate desc, uslatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D' and code not in (select uscode from client.pday113)
and substring(wca.scmst.primaryexchgcd,1,2) ='ES'
and prices.lasttrade.exchgcd='ESSIBE'
#and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
#or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
'ESSIBE'=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
'ESSIBE'=prices.lasttrade.exchgcd
and latestscmst.uscode<>''
and latestscmst.uscode=wca.scmst.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.pricedate desc, uslatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into client.pday113
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code = prices.lasttrade.uscode
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D' and code not in (select uscode from client.pday113)
and prices.lasttrade.exchgcd<>'USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.currency=
(select currency from prices.lasttrade as uslatest
where
uslatest.uscode<>''
and uslatest.uscode=prices.lasttrade.uscode
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by uslatest.pricedate desc, uslatest.currency desc limit 1)
and (wca.scmst.actflag<>'D' or wca.scmst.actflag is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
order by prices.lasttrade.isin;
insert ignore into client.pday113
select distinct
'' as MIC,
'' as LocalCode,
wca.scmst.Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
null as MktCloseDate,
'' as Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
client.pfuscode.code as USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfuscode
LEFT OUTER JOIN wca.scmst on client.pfuscode.secid = wca.scmst.secid
LEFT OUTER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where client.pfuscode.accid = 113 and client.pfuscode.actflag<>'D' and code not in (select uscode from client.pday113);