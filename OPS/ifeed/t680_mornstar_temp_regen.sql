DROP TABLE IF EXISTS `wca2`.`t680_mornstar_temp`;

CREATE TABLE wca2.t680_mornstar_temp like wca2.t680_temp;

insert into wca2.t680_mornstar_temp select * from wca2.t680_temp where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd<>'SHOCH' and eventcd<>'DIV';

insert ignore into wca2.t680_mornstar_temp select * from wca2.t680_temp_prf where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd<>'SHOCH' and eventcd<>'DIV';

use wca;
set @feedseriesid=680;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
set @caloffset='30';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;

insert ignore into wca2.t680_mornstar_temp
select
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
case when wca.divpy.OptionSerialNo is null then '1' else wca.divpy.OptionSerialNo end as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (divpy.acttime > vtab.acttime) 
      and (divpy.acttime > rd.acttime) 
      and (divpy.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (divpy.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then divpy.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'RegistrationDate' as Date2Type,
CASE WHEN wca.rd.registrationdate is not null
     THEN wca.rd.registrationdate
     ELSE wca.rd.recdate
     END as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Paydate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Paydate2 
     ELSE null END as Date4,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RecordDate' as Date9Type,
CASE WHEN wca.rd.recdate is not null
     THEN wca.rd.recdate
     ELSE wca.rd.registrationdate
     END as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'Exdate2' as Date11Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Exdate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Exdate2 
     ELSE null END as Date11,
'FXDate' as Date12Type,
wca.divpy.FXDate as Date12,
ifnull(wca.divpy.Divtype,'') as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
substring(wca.divpy.ratioold,1,instr(wca.divpy.ratioold,'.')+7) as RatioOld,
substring(wca.divpy.rationew,1,instr(wca.divpy.rationew,'.')+7) as RatioNew,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'Dapflag' as Field12Name,
case when wca.prvsc.secid is not null then 'Y' else 'N' end as Dapflag,
'InstallmentPayDate' as Field13Name,
wca.divpy.InstallmentPayDate as Field13,
'DeclCurenCD' as Field14Name,
vtab.declcurencd as Field14,
'DeclGrossAmt' as Field15Name,
vtab.declgrossamt as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and wca.scexh.listcategory<>'R'
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
Where
(@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or divpy.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate));

insert ignore into wca2.t680_mornstar_temp
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ListDate' as Date1Type,
wca.scexh.listdate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
vtab.DRRatio as RatioOld,
vtab.USRatio as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'UnSecID' as Field1Name,
vtab.UnSecID as Field1,
'SpnFlag' as Field2Name,
vtab.SpnFlag as Field2,
'DRtype' as Field3Name,
vtab.DRtype as Field3,
'DepBank' as Field4Name,
vtab.DepBank as Field4,
'LevDesc' as Field5Name,
vtab.LevDesc as Field5,	
'OtherDepBank' as Field6Name,
vtab.OtherDepBank as Field6,
'UnIsin' as Field7Name,
unscmst.isin as Field7,
'UnUSCode' as Field8Name,
unscmst.uscode as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dprcp as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
inner join wca.scmst as unscmst on vtab.unsecid = unscmst.secid 
Where
vtab.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) 
and wca.scexh.actflag<>'D';
    
insert ignore into wca2.t680_mornstar_temp
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldDRRatio' as Field2Name,
vtab.OldDRRatio as Field2,
'NewDRRatio' as Field3Name,
vtab.NewDRRatio as Field3,
'OldUNRatio' as Field4Name,
vtab.OldUNRatio as Field4,
'NewUNRatio' as Field5Name,
vtab.NewUNRatio as Field5,
'OldUNSecID' as Field6Name,
vtab.OldUNSecID as Field6,
'NewUNSecID' as Field7Name,
vtab.NewUNSecID as Field7,	
'OldDepbank' as Field8Name,
vtab.OldDepbank as Field8,
'NewDepbank' as Field9Name,
vtab.NewDepbank as Field9,
'OldDRtype' as Field10Name,
vtab.OldDRtype as Field10,
'NewDRtype' as Field11Name,
vtab.NewDRtype as Field11,
'OldLevel' as Field12Name,
vtab.OldLevel as Field12,
'NewLevel' as Field13Name,
vtab.NewLevel as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_drchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
Where
vtab.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) 
and wca.scexh.actflag<>'D';

use wca;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 0, 1);
set @fromdate2=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 1, 1);
set @fromdate3=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);

insert ignore into wca2.t680_mornstar_temp
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when vtab.acttime>@fromdate then vtab.acttime
     when ifnull(wca.exdt.acttime,'2000-01-01')>@fromdate then ifnull(wca.exdt.acttime,'2000-01-01')
     when vtab.acttime>@fromdate2 then vtab.acttime
     when ifnull(wca.exdt.acttime,'2000-01-01')>@fromdate2 then ifnull(wca.exdt.acttime,'2000-01-01')
     when vtab.acttime>@fromdate3 then vtab.acttime
     when ifnull(wca.exdt.acttime,'2000-01-01')>@fromdate3 then ifnull(wca.exdt.acttime,'2000-01-01')
     else vtab.Acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ListDate' as Date1Type,
wca.scexh.listdate as Date1,
'RecordDate' as Date2Type,
vtab.RecDate as Date2,
'ToDate' as Date3Type,
vtab.ToDate as Date3,
'RegistrationDate' as Date4Type,
vtab.RegistrationDate as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
vtab.rdid as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RdSectyCD' as Field1Name,
vtab.sectycd as Field1,
'RdCalcFlag' as Field2Name,
vtab.reccalc as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_rd as vtab
inner join wca.dprcp on vtab.SecID = dprcp.SecID
left outer join wca.exdt on vtab.RdID = wca.exdt.RdID
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.issur on stab.issid = wca.issur.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
Where
((vtab.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
or ifnull(wca.exdt.acttime,'2000-01-01')>(select max(feeddate) from wca.tbl_opslog where seq=3)))
and (vtab.recdate < wca.scexh.acttime or wca.scexh.liststatus<>'D' or vtab.recdate is null)
and wca.scexh.actflag<>'D'
and wca.exdt.rdid is null
and vtab.sectycd='DR';




