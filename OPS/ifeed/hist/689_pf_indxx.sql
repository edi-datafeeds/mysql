DROP TABLE IF EXISTS `wca2`.`t689_hist`;
CREATE TABLE `wca2`.`t689_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `PFID` int(10) NOT NULL DEFAULT '0',
  `Changed` datetime DEFAULT NULL,
  `Fieldname` varchar(20) NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`PFID`,`Fieldname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=689;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
order by acttime desc
limit 2, 1);
set @histoffset=7347;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pffigi.secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcompid.secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information')
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and ifnull(exdate,'2000-01-01')>=@fromdate and ifnull(exdate,'2000-01-01')<=@caldate))
and (vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information'));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Companies' as Fieldname,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pffigi.secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcompid.secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information')
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and ifnull(exdate,'2000-01-01')>=@fromdate and ifnull(exdate,'2000-01-01')<=@caldate))
and (vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information'));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pffigi.secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfcompid.secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information')
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and ifnull(exdate,'2000-01-01')>=@fromdate and ifnull(exdate,'2000-01-01')<=@caldate))
and (vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information'));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pffigi.secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfcompid.secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information')
or (vtab.closedate>=@fromdate and vtab.closedate<=@caldate)
and (vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information'));
