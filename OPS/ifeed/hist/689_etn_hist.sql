DROP TABLE IF EXISTS `wca2`.`t689_etn_hist`;
CREATE TABLE `wca2`.`t689_etn_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `PFID` int(10) NOT NULL DEFAULT '0',
  `Changed` datetime DEFAULT NULL,
  `Fieldname` char(5) CHARACTER SET utf8 NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`PFID`,`Fieldname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=999;
set @fromdate='2014-02-01';
-- set @histoffset=183;
-- set @histdate='2013-01-01';
-- set @accid=999;
set @fcntrycd='US';
-- set @fexchgcd='GBLSE';
insert ignore into wca2.t689_etn_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
INTNotes
FROM wca.v10s_intpy as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
vtab.actflag<>'D'
and INTNotes<>''
and INTNotes<>'No further information'
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_etn_hist
SELECT
vtab.EventCD,
vtab.EventID,
vtab.issid as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
WHERE
vtab.issid in (select issid from wca.v20c_etn_scmst)
and vtab.actflag<>'D'
and LiquidationTerms<>''
and LiquidationTerms<>'No further information'
and (vtab.acttime>=@fromdate
and vtab.issid in (select issid from wca.scmst inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where wca.scexh.actflag<> 'D' and wca.scmst.sectycd<>'BND')
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfisin.issid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (@fexchgcd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    where ExchgCD = @fexchgcd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or vtab.issid in
    (select issid from wca.v20c_etn_scmst
    inner join wca.scexh on wca.v20c_etn_scmst.secid = wca.scexh.secid
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
or (vtab.rddate>=@fromdate and vtab.rddate<=@caldate);
insert ignore into wca2.t689_etn_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
Notes
FROM wca.v10s_prchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
vtab.actflag<>'D'
and Notes<>''
and Notes<>'No further information'
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
   and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_etn_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redm as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
vtab.actflag<>'D'
and RedemNotes<>''
and RedemNotes<>'No further information'
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
   and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
or (vtab.redemdate>=@fromdate and vtab.redemdate<=@caldate);
insert ignore into wca2.t689_etn_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
vtab.actflag<>'D'
and ScChgNotes<>''
and ScChgNotes<>'No further information'
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
or (vtab.dateofchange>=@fromdate and vtab.dateofchange<=@caldate);
