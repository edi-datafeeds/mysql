DROP TABLE IF EXISTS `wca2`.`t690_hist`;
CREATE TABLE `wca2`.`t690_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `OptionID` int(10) unsigned NOT NULL,
  `SerialID` int(10) unsigned NOT NULL,
  `ScexhID` int(10) unsigned NOT NULL DEFAULT '0',
  `SedolID` int(10) unsigned NOT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `Changed` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `Isin` char(12) DEFAULT NULL,
  `Uscode` char(9) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `IndusID` int(10) unsigned DEFAULT NULL,
  `SectyCD` char(3) DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` char(1) DEFAULT NULL,
  `PrimaryExchgCD` char(6) DEFAULT NULL,
  `Sedol` char(7) DEFAULT NULL,
  `SedolCurrency` char(3) DEFAULT NULL,
  `Defunct` char(1) DEFAULT NULL,
  `SedolRegCntry` char(2) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) DEFAULT NULL,
  `Micseg` char(4) DEFAULT NULL,
  `LocalCode` varchar(50) DEFAULT NULL,
  `ListStatus` varchar(1) DEFAULT NULL,
  `Date1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date1` datetime DEFAULT NULL,
  `Date2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date2` datetime DEFAULT NULL,
  `Date3Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date3` datetime DEFAULT NULL,
  `Date4Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date4` datetime DEFAULT NULL,
  `Date5Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date5` datetime DEFAULT NULL,
  `Date6Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date6` datetime DEFAULT NULL,
  `Date7Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date7` datetime DEFAULT NULL,
  `Date8Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date8` datetime DEFAULT NULL,
  `Date9Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date9` datetime DEFAULT NULL,
  `Date10Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date10` datetime DEFAULT NULL,
  `Date11Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date11` datetime DEFAULT NULL,
  `Date12Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date12` datetime DEFAULT NULL,
  `Paytype` char(1) CHARACTER SET utf8 NOT NULL,
  `RdID` int(10) DEFAULT NULL,
  `Priority` int(10) DEFAULT NULL,
  `DefaultOpt` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `OutturnSecID` int(10) unsigned DEFAULT NULL,
  `OutturnIsin` char(12) CHARACTER SET utf8 DEFAULT NULL,
  `RatioOld` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `RatioNew` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Fractions` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `Currency` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `Rate1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate1` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Rate2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate2` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Field1Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field1` varchar(255) DEFAULT NULL,
  `Field2Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field2` varchar(255) DEFAULT NULL,
  `Field3Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field3` varchar(255) DEFAULT NULL,
  `Field4Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field4` varchar(255) DEFAULT NULL,
  `Field5Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field5` varchar(255) DEFAULT NULL,
  `Field6Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field6` varchar(255) DEFAULT NULL,
  `Field7Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field7` varchar(255) DEFAULT NULL,
  `Field8Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field8` varchar(255) DEFAULT NULL,
  `Field9Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field9` varchar(255) DEFAULT NULL,
  `Field10Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field10` varchar(255) DEFAULT NULL,
  `Field11Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field11` varchar(255) DEFAULT NULL,
  `Field12Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field12` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field13Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field13` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field14Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field14` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field15Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field15` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field16Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field16` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field17Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field17` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field18Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field18` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field19Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field19` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field20Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field20` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field21Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field21` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field22Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field22` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field23Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field23` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field24Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field24` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`EventCD`,`EventID`,`OptionID`,`SerialID`,`ScexhID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=690;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
order by acttime desc
limit 2, 1);
set @histoffset=93;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='7';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t690_hist
select
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
case when wca.divpy.OptionSerialNo is null then '1' else wca.divpy.OptionSerialNo end as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (divpy.acttime > vtab.acttime) 
      and (divpy.acttime > rd.acttime) 
      and (divpy.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (divpy.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then divpy.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.PayDate2 IS NOT NULL then wca.exdt.PayDate2 ELSE pexdt.paydate2 END as Date4,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'InstallmentPayDate' as Date11Type,
wca.divpy.InstallmentPayDate as Date11,
'FXDate' as Date12Type,
wca.divpy.FXDate as Date12,
ifnull(wca.divpy.Divtype,'') as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
wca.divpy.RatioOld,
wca.divpy.RatioNew,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'Dapflag' as Field12Name,
case when wca.prvsc.secid is not null then 'Y' else 'N' end as Dapflag,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and (vtab.acttime>=@fromdate
or divpy.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
   or wca.sedol.Sedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
