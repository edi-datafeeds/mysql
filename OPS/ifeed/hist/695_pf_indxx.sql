DROP TABLE IF EXISTS `wca2`.`t695_hist`;
CREATE TABLE `wca2`.`t695_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `OptionID` int(10) unsigned NOT NULL,
  `SerialID` int(10) unsigned NOT NULL,
  `ScexhID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbcID` int(10) unsigned NOT NULL DEFAULT '0',
  `BbeID` int(10) unsigned NOT NULL DEFAULT '0',
  `Actflag` char(1) DEFAULT NULL,
  `Changed` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `Isin` char(12) DEFAULT NULL,
  `Uscode` char(9) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `IndusID` int(10) unsigned DEFAULT NULL,
  `SectyCD` char(3) DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` char(1) DEFAULT NULL,
  `PrimaryExchgCD` char(6) DEFAULT NULL,
  `BbgCurrency` char(3) DEFAULT NULL,
  `BbgCompositeGlobalID` char(12) DEFAULT NULL,
  `BbgCompositeTicker` varchar(40) DEFAULT NULL,
  `BbgGlobalID` char(12) DEFAULT NULL,
  `BbgExchangeTicker` varchar(40) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) DEFAULT NULL,
  `Micseg` char(4) DEFAULT NULL,
  `LocalCode` varchar(50) DEFAULT NULL,
  `ListStatus` varchar(1) DEFAULT NULL,
  `Date1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date1` datetime DEFAULT NULL,
  `Date2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date2` datetime DEFAULT NULL,
  `Date3Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date3` datetime DEFAULT NULL,
  `Date4Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date4` datetime DEFAULT NULL,
  `Date5Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date5` datetime DEFAULT NULL,
  `Date6Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date6` datetime DEFAULT NULL,
  `Date7Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date7` datetime DEFAULT NULL,
  `Date8Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date8` datetime DEFAULT NULL,
  `Date9Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date9` datetime DEFAULT NULL,
  `Date10Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date10` datetime DEFAULT NULL,
  `Date11Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date11` datetime DEFAULT NULL,
  `Date12Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Date12` datetime DEFAULT NULL,
  `Paytype` char(1) CHARACTER SET utf8 NOT NULL,
  `RdID` int(10) DEFAULT NULL,
  `Priority` int(10) DEFAULT NULL,
  `DefaultOpt` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `OutturnSecID` int(10) unsigned DEFAULT NULL,
  `OutturnIsin` char(12) CHARACTER SET utf8 DEFAULT NULL,
  `RatioOld` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `RatioNew` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Fractions` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `Currency` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `Rate1Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate1` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Rate2Type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Rate2` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Field1Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field1` varchar(255) DEFAULT NULL,
  `Field2Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field2` varchar(255) DEFAULT NULL,
  `Field3Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field3` varchar(255) DEFAULT NULL,
  `Field4Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field4` varchar(255) DEFAULT NULL,
  `Field5Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field5` varchar(255) DEFAULT NULL,
  `Field6Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field6` varchar(255) DEFAULT NULL,
  `Field7Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field7` varchar(255) DEFAULT NULL,
  `Field8Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field8` varchar(255) DEFAULT NULL,
  `Field9Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field9` varchar(255) DEFAULT NULL,
  `Field10Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field10` varchar(255) DEFAULT NULL,
  `Field11Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field11` varchar(255) DEFAULT NULL,
  `Field12Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field12` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field13Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field13` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field14Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field14` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field15Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field15` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field16Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field16` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field17Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field17` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field18Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field18` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field19Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field19` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field20Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field20` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field21Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field21` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Field22Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field22` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field23Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field23` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Field24Name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Field24` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`EventCD`,`EventID`,`OptionID`,`SerialID`,`ScexhID`,`BbcID`,`BbeID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=695;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
order by acttime desc
limit 2, 1);
set @histoffset=7347;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t695_hist
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (mpay.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'AppointedDate' as Date4Type,
vtab.AppointedDate as Date4,
'EffectiveDate' as Date5Type,
vtab.EffectiveDate as Date5,
'WithdrawalFromdate' as Date6Type,
wca.mpay.WithdrawalFromdate as Date6,
'WithdrawalTodate' as Date7Type,
wca.mpay.WithdrawalTodate as Date7,
'OptElectionDate' as Date8Type,
mpay.OptElectionDate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MrgrStatus' as Field1Name,
vtab.MrgrStatus as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t695_hist
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
     then mpay.acttime 
     when rd.acttime > vtab.acttime 
     then rd.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Opendate' as Date1Type,
vtab.opendate as Date1,
'Closedate' as Date2Type,
vtab.closedate as Date2,
'Recdate' as Date3Type,
wca.rd.Recdate as Date3,
'Cmacqdate' as Date4Type,
vtab.Cmacqdate as Date4,
'Paydate' as Date5Type,
wca.mpay.Paydate as Date5,
'Optelectiondate' as Date6Type,
wca.mpay.Optelectiondate as Date6,
'Withdrawalfromdate' as Date7Type,
wca.mpay.Withdrawalfromdate as Date7,
'Withdrawaltodate' as Date8Type,
wca.mpay.Withdrawaltodate as Date8,
'UnconditionalDate' as Date9Type,
vtab.UnconditionalDate as Date9,
'ToDate' as Date10Type,
wca.rd.ToDate as Date10,
'RegistrationDate' as Date11Type,
wca.rd.RegistrationDate as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'TkovrStatus' as Field1Name,
vtab.TkovrStatus as Field1,
'OfferorIssID' as Field2Name,
vtab.OfferorIssID as Field2,
'OfferorName' as Field3Name,
vtab.OfferorName as Field3,
'Hostile' as Field4Name,
case when wca.vtab.Hostile='T' then 'T' else 'F' end as Hostile,
'MiniTkovr' as Field5Name,
case when wca.vtab.MiniTkovr='T' then 'T' else 'F' end as MiniTkovr,
'ResSecTyCD' as Field6Name,
mpay.SecTyCD as Field6,
'TargetQuantity' as Field7Name,
vtab.TargetQuantity as Field7,
'TargetPercent' as Field8Name,
vtab.TargetPercent as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'PreOfferQty' as Field11Name,
vtab.PreOfferQty as Field11,
'PreOfferPercent' as Field12Name,
vtab.PreOfferPercent as Field12,
'TndrStrkPrice' as Field13Name,
mpay.TndrStrkPrice as Field13,
'TndrStrkStep' as Field14Name,
mpay.TndrStrkStep as Field14,
'MinQlyQty' as Field15Name,
mpay.MinQlyQty as Field15,
'MaxQlyQty' as Field16Name,
mpay.MaxQlyQty as Field16,
'MinOfrQty' as Field17Name,
mpay.MinOfrQty as Field17,
'MaxOfrQty' as Field18Name,
mpay.MaxOfrQty as Field18,
'DutchAuction' as Field19Name,
case when wca.mpay.DutchAuction='T' then 'T' else 'F' end as DutchAuction,
'WithdrawalRights' as Field20Name,
mpay.WithdrawalRights as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_tkovr as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.tkovrid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pfcomptk where actflag<>'D' and accid=@accid)
    or stab.secid in (select secid from client.pffigi where actflag<>'D' and accid=@accid) 
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate
or vtab.closedate>=@fromdate and vtab.closedate<=@caldate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
 and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcomptk where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pffigi where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select secid from client.pfcompid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
  or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
