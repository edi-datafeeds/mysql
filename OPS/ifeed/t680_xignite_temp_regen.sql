DROP TABLE IF EXISTS `wca2`.`t680_xignite_temp`;

CREATE TABLE wca2.t680_xignite_temp like wca2.t680_temp;

insert ignore into wca2.t680_xignite_temp select * from wca2.t680_temp where changed > (select max(feeddate) from wca.tbl_opslog where seq = 3) 
and eventcd<>'DPRCP' and eventcd <> 'SHOCH' and eventcd <> 'DRCHG';

insert ignore into wca2.t680_xignite_temp select * from wca2.t680_temp_prf where changed > (select max(feeddate) from wca.tbl_opslog where seq = 3) 
and eventcd<>'DPRCP' and eventcd <> 'SHOCH' and eventcd <> 'DRCHG';

update wca2.t680_xignite_temp
set sectycd='PRF'
where sectycd='BND';


