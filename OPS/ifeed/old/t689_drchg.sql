DROP TABLE IF EXISTS `wca2`.`t689_hist`;
CREATE TABLE `wca2`.`t689_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `PFID` int(10) NOT NULL DEFAULT '0',
  `Changed` datetime DEFAULT NULL,
  `Fieldname` char(5) CHARACTER SET utf8 NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`PFID`,`Fieldname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=689;
-- set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
-- order by acttime desc
-- limit 2, 1);
set @fromdate='2003-01-01';
set @histoffset=183;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='7';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode='H';
set @todate='2014-03-28';
set @cntrycd=null;
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DPRCPNotes as NotesText
FROM wca.v10s_dprcp as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DPRCPNotes<>'' and DPRCPNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DPRCPNotes<>'' and DPRCPNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DrchgNotes as NotesText
FROM wca.v10s_drchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and drchgNotes<>'' and drchgNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and drchgNotes<>'' and drchgNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
