DROP TABLE IF EXISTS `wca2`.`t689_hist`;
CREATE TABLE `wca2`.`t689_hist` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `PFID` int(10) NOT NULL DEFAULT '0',
  `Changed` datetime DEFAULT NULL,
  `Fieldname` char(5) CHARACTER SET utf8 NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`PFID`,`Fieldname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=689;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
order by acttime desc
limit 2, 1);
-- set @fromdate='2003-01-01';
set @histoffset=183;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='7';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ArrNotes as NotesText
FROM wca.v10s_arr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ArrNotes<>'' and ArrNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ArrNotes<>'' and ArrNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
BBNotes as NotesText
FROM wca.v10s_bb as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and BBNotes<>'' and BBNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and BBNotes<>'' and BBNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.enddate>=@fromdate and vtab.enddate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
vtab.issid as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
BkrpNotes as NotesText
FROM wca.v10s_bkrp as vtab
WHERE
vtab.issid in (select issid from wca.v20c_680_scmst)
and
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and BkrpNotes<>'' and BkrpNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
 or (vtab.issid in (select client.pfisin.issid from client.pfisin where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and BkrpNotes<>'' and BkrpNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfisin.issid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
BonNotes as NotesText
FROM wca.v10s_bon as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and BonNotes<>'' and BonNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
-- and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and BonNotes<>'' and BonNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
CallNotes as NotesText
FROM wca.v10s_call as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and CallNotes<>'' and CallNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and CallNotes<>'' and CallNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.duedate>=@fromdate and vtab.duedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM wca.v10s_caprd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and CapRdNotes<>'' and CapRdNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and CapRdNotes<>'' and CapRdNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ConsdNotes as NotesText
FROM wca.v10s_consd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ConsdNotes<>'' and ConsdNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ConsdNotes<>'' and ConsdNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ConvNotes as NotesText
FROM wca.v10s_conv as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ConvNotes<>'' and ConvNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ConvNotes<>'' and ConvNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.todate>=@fromdate and vtab.todate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
CtxNotes as NotesText
FROM wca.v10s_ctx as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and CtxNotes<>'' and CtxNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and CtxNotes<>'' and CtxNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.enddate>=@fromdate and vtab.enddate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
CurRdNotes as NotesText
FROM wca.v10s_currd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and CurRdNotes<>'' and CurRdNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and CurRdNotes<>'' and CurRdNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DistNotes as NotesText
FROM wca.v10s_dist as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DistNotes<>'' and DistNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DistNotes<>'' and DistNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DIVNotes
FROM wca.v10s_div as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DIVNotes<>'' and DIVNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DIVNotes<>'' and DIVNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DmrgrNotes as NotesText
FROM wca.v10s_dmrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DmrgrNotes<>'' and DmrgrNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DmrgrNotes<>'' and DmrgrNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DvstNotes as NotesText
FROM wca.v10s_dvst as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DvstNotes<>'' and DvstNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DvstNotes<>'' and DvstNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
EntNotes as NotesText
FROM wca.v10s_ent as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and EntNotes<>'' and EntNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and EntNotes<>'' and EntNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
vtab.issid as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
LawstNotes as NotesText
FROM wca.v10s_lawst as vtab
WHERE
vtab.issid in (select issid from wca.v20c_680_scmst)
and
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and LawstNotes<>'' and LawstNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (vtab.issid in (select client.pfisin.issid from client.pfisin where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and LawstNotes<>'' and LawstNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfisin.issid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
vtab.issid as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
WHERE
vtab.issid in (select issid from wca.v20c_680_scmst)
and
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and LiquidationTerms<>'' and LiquidationTerms<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (vtab.issid in (select client.pfisin.issid from client.pfisin where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and LiquidationTerms<>'' and LiquidationTerms<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfisin.issid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rddate>=@fromdate and vtab.rddate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and MrgrTerms<>'' and MrgrTerms<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Companies' as Fieldname,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and Companies<>'' and Companies<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ApprovalStatus<>'' and ApprovalStatus<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
PONotes
FROM wca.v10s_po as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and PONotes<>'' and PONotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and PONotes<>'' and PONotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
Notes
FROM wca.v10s_prchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and Notes<>'' and Notes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and Notes<>'' and Notes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
   and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
PrfNotes
FROM wca.v10s_prf as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and PrfNotes<>'' and PrfNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and PrfNotes<>'' and PrfNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
PvRdNotes as NotesText
FROM wca.v10s_pvrd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and PvRdNotes<>'' and PvRdNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and PvRdNotes<>'' and PvRdNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
RcapNotes as NotesText
FROM wca.v10s_rcap as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and RcapNotes<>'' and RcapNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and RcapNotes<>'' and RcapNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redem as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and RedemNotes<>'' and RedemNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and RedemNotes<>'' and RedemNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.redemdate>=@fromdate and vtab.redemdate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
RtsNotes
FROM wca.v10s_rts as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and RtsNotes<>'' and RtsNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and RtsNotes<>'' and RtsNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ScChgNotes<>'' and ScChgNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ScChgNotes<>'' and ScChgNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.dateofchange>=@fromdate and vtab.dateofchange<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ScSwpNotes
FROM wca.v10s_scswp as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ScSwpNotes<>'' and ScSwpNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ScSwpNotes<>'' and ScSwpNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
SDNotes
FROM wca.v10s_sd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and SDNotes<>'' and SDNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and SDNotes<>'' and SDNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM wca.v10s_secrc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and SecRcNotes<>'' and SecRcNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and SecRcNotes<>'' and SecRcNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and TkovrNotes<>'' and TkovrNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.closedate>=@fromdate and vtab.closedate<=@caldate);
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DPRCPNotes as NotesText
FROM wca.v10s_dprcp as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and DPRCPNotes<>'' and DPRCPNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and DPRCPNotes<>'' and DPRCPNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.t689_hist
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
DrchgNotes as NotesText
FROM wca.v10s_drchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and drchgNotes<>'' and drchgNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and drchgNotes<>'' and drchgNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))))
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate);
