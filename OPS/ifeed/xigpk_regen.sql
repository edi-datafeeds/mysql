insert ignore into wca.xigpk
select
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.actflag
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe on wca.scmst.secid =wca.bbe.secid and ''=ifnull(wca.bbe.exchgcd,'X') and ''<>ifnull(wca.bbe.bbgexhid,'') and 'D'<>ifnull(wca.bbe.bbgexhid,'')
where
wca.scmst.actflag<>'D'
and (wca.exchg.mic<>'' or wca.scmst.sectycd<>'BND')
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and substring(wca.exchg.exchgcd,3,3)<>'BND'
and wca.exchg.exchgcd<>'USTRCE'
and wca.exchg.exchgcd<>'ALTSE'
and wca.exchg.exchgcd<>'AOASE'
and wca.exchg.exchgcd<>'AROEX'
and wca.exchg.exchgcd<>'ARRCX'
and wca.exchg.exchgcd<>'AUSIMV'
and wca.exchg.exchgcd<>'CHSCH'
and wca.exchg.exchgcd<>'CNSZHK'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'DESCH'
and wca.exchg.exchgcd<>'DODRSX'
and wca.exchg.exchgcd<>'FINDX'
and wca.exchg.exchgcd<>'FOFSM'
and wca.exchg.exchgcd<>'GABVCA'
and wca.exchg.exchgcd<>'GBENLN'
and wca.exchg.exchgcd<>'GBOFX'
and wca.exchg.exchgcd<>'GBUT'
and wca.exchg.exchgcd<>'GIGSX'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'HNCASE'
and wca.exchg.exchgcd<>'HTHTSE'
and wca.exchg.exchgcd<>'JPNSE'
and wca.exchg.exchgcd<>'KNECSE'
and wca.exchg.exchgcd<>'LSMSM'
and wca.exchg.exchgcd<>'LUUL'
and wca.exchg.exchgcd<>'MVMSX'
and wca.exchg.exchgcd<>'MYLFX'
and wca.exchg.exchgcd<>'MZMSX'
and wca.exchg.exchgcd<>'NOOTC'
and wca.exchg.exchgcd<>'PLOTC'
and wca.exchg.exchgcd<>'SENDX'
and wca.exchg.exchgcd<>'TJCASE'
and wca.exchg.exchgcd<>'UAINX'
and wca.exchg.exchgcd<>'UAKISE'
and wca.exchg.exchgcd<>'UYUEX';
                       