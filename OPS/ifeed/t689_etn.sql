DROP TABLE IF EXISTS `wca2`.`t689_etn`;
CREATE TABLE `wca2`.`t689_etn` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `PFID` int(10) NOT NULL DEFAULT '0',
  `Changed` datetime DEFAULT NULL,
  `Fieldname` char(5) CHARACTER SET utf8 NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`PFID`,`Fieldname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=689;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
set @histoffset=183;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='30';
set @caldate=date_add(@fromdate, interval @caloffset day);
set @accid='';
set @runmode=null;
set @todate=null;
set @cntrycd=null;
insert ignore into wca2.t689_etn
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
INTNotes
FROM wca.v10s_intpy as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and INTNotes<>'' and INTNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and INTNotes<>'' and INTNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and INTNotes<>'' and INTNotes<>'No further information')
or (vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fromdate and exdate<=@caldate)));
insert ignore into wca2.t689_etn
SELECT
vtab.EventCD,
vtab.EventID,
vtab.issid as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
WHERE
vtab.issid in (select issid from wca.v20c_etn_scmst)
and
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and LiquidationTerms<>'' and LiquidationTerms<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (vtab.issid in (select client.pfisin.issid from client.pfisin where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag<>'D' and accid=@accid)
    or vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and LiquidationTerms<>'' and LiquidationTerms<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (vtab.issid in (select client.pfsecid.issid from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfisin.issid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfuscode.issid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or vtab.issid in (select client.pfsedol.issid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and LiquidationTerms<>'' and LiquidationTerms<>'No further information')
or (vtab.rddate>=@fromdate and vtab.rddate<=@caldate));
insert ignore into wca2.t689_etn
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
Notes
FROM wca.v10s_prchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and Notes<>'' and Notes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and Notes<>'' and Notes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
   and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and Notes<>'' and Notes<>'No further information')
or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate));
insert ignore into wca2.t689_etn
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redm as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and RedemNotes<>'' and RedemNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and RedemNotes<>'' and RedemNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and RedemNotes<>'' and RedemNotes<>'No further information')
or (vtab.redemdate>=@fromdate and vtab.redemdate<=@caldate));
insert ignore into wca2.t689_etn
SELECT
vtab.EventCD,
vtab.EventID,
wca.scmst.SecID as PFID,
vtab.Acttime as Changed,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid and wca.scmst.structcd='ETN'
WHERE
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and vtab.actflag<>'D' and ScChgNotes<>'' and ScChgNotes<>'No further information')
or (@runmode='H'
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
  or (wca.scmst.secid in (select secid from client.pfisin where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select secid from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or wca.scmst.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid))))
and (vtab.actflag<>'D' and ScChgNotes<>'' and ScChgNotes<>'No further information')
or (@runmode is null
and (vtab.acttime>=@fromdate
or (vtab.acttime>@histdate and vtab.actflag<>'D'
    and (wca.scmst.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfisin.secid from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.actflag<>'D' and ScChgNotes<>'' and ScChgNotes<>'No further information')
or (vtab.dateofchange>=@fromdate and vtab.dateofchange<=@caldate));
