use prices;
delete from client.pday103;
insert ignore into client.pday103
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
where
client.pfisin.accid = 103 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd='USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.exchgcd='USCOMP'
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd='USCOMP'
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.exchgcd='USCOMP'
order by latest.pricedate desc, latest.currency desc limit 1)
order by prices.lasttrade.isin;
insert ignore into client.pday103
select distinct
'' as MIC,
'' as LocalCode,
client.pfisin.code as Isin,
'' as Currency,
Null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
Null as MktCloseDate,
'' as Volflag,
'' as Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
wca.scmst.USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where client.pfisin.accid = 103 and code not in (select isin from client.pday103);