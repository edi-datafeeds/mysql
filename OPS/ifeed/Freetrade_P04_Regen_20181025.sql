use prices;
delete from prices.prices_freetrade;
insert into prices.prices_freetrade
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.exchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 376 and client.pfisin.actflag<>'D'
and wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.currency=prices.lasttrade.currency
order by latest.pricedate desc limit 1));
insert into prices.prices_freetrade
select distinct
'' as MIC,
'' as LocalCode,
client.pfisin.code as Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
0 as SecID,
null as MktCloseDate,
'' as Volflag,
'' as Issuername,
'' as SectyCD,
'' as SecurityDesc,
'' as Sedol,
'' as USCode,
'' as PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfisin
where client.pfisin.accid=376 and client.pfisin.actflag<>'D'
and code not in (select isin from prices.prices_freetrade);