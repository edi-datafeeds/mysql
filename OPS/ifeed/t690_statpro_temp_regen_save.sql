DROP TABLE IF EXISTS `wca2`.`t690_statpro_temp`;

CREATE TABLE wca2.t690_statpro_temp like wca2.t690_temp;

insert into wca2.t690_statpro_temp select * from wca2.t690_temp where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'NLIST' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH' and eventcd <> 'LSTAT';

ALTER TABLE `wca2`.`t690_statpro_temp` 
ADD COLUMN `Field25Name` VARCHAR(20) CHARACTER SET utf8 NOT NULL AFTER `Field24`,
ADD COLUMN `Field25` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `Field25Name`;

update wca2.t690_statpro_temp
set field25name = 'MarketIsin';

update wca2.t690_statpro_temp
set field25='';

update wca2.t690_statpro_temp
left outer join wca.scexh on wca2.t690_statpro_temp.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scexh.junklocalcode
where length(junklocalcode) = 12
and (ord(substring(wca.scexh.junklocalcode,1,2)) between 65 and 90 and ord(substring(wca.scexh.junklocalcode,12,1)) between 48 and 57
and substring(wca.scexh.junklocalcode,6,1) <> '/' and substring(wca.scexh.junklocalcode,5,1) <> '/')
and substring(wca.scexh.exchgcd,1,2) = 'DE';

update wca2.t690_statpro_temp
left outer join wca.scxtc on wca2.t690_statpro_temp.scexhid = wca.scxtc.scexhid
left outer join wca.scexh on wca.scxtc.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scxtc.isin
where 
substring(wca.scexh.exchgcd,1,2) = 'DE'
and wca.scxtc.isin <> '';

update wca2.t690_statpro_temp
set sedolcurrency = 'USD' 
where 
sedolcurrency = '' and exchgcntry = 'US';

delete from wca2.t690_statpro_temp
where eventcd='DIV'
and field4='T'
AND 1<(select count(subrd.rdid) from wca.rd as subrd
inner join wca.div_my on subrd.rdid = wca.div_my.rdid
where 
(subrd.rdid=wca2.t690_statpro_temp.rdid)) 
or (field1='CG' and field3='T');

delete from wca2.t690_statpro_temp where eventcd='LCC' and field3='';

delete from wca2.t690_statpro_temp where eventcd='LSTAT' and Field3='';

insert ignore into wca2.t690_statpro_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scmst.SecID,
wca.issur.IssID,
wca.scmst.isin,
wca.scmst.uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='D' or wca.scexh.ListStatus='S' then wca.scexh.ListStatus else upper('L') end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'NotificationDate' as Date2Type,
vtab.NotificationDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLStatStatus' as Field2Name,
vtab.OldLStatStatus as Field2,
'LStatStatus' as Field3Name,
vtab.LStatStatus as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24,
'' as Field25Name,
'' as Field25
from wca.v10s_lstat as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.ExchgCD
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
Where
(3>wca.sectygrp.secgrpid
or(wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and vtab.effectivedate>(select date_sub(max(feeddate), interval 15 day) from wca.tbl_opslog where seq=3)
and (vtab.effectivedate=(if(dayofweek(now())<>6, (select date_sub(max(feeddate), interval 14 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 14 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 13 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 12 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
and vtab.actflag<>'D'
and vtab.eventtype<>'CLEAN';

insert ignore into wca2.t690_statpro_temp
select distinct
'NLIST' AS EventCD,
wca.scexh.scexhid AS EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
wca.scexh.ActFlag,
wca.scexh.acttime as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24,
'' as Field25Name,
'' as Field25
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
Where
(3>wca.sectygrp.secgrpid
or(wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and wca.scexh.announcedate>(select date_sub(max(feeddate), interval 31 day) from wca.tbl_opslog where seq = 3)
and ((wca.scexh.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))
or (wca.scmst.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3)) 
or (wca.sedol.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))) 
and wca.scexh.liststatus<>'D' and wca.scexh.liststatus<>'S' and wca.scmst.statusflag<>'I';

