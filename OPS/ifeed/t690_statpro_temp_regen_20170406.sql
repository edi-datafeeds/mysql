DROP TABLE IF EXISTS `wca2`.`t690_statpro_temp`;

CREATE TABLE wca2.t690_statpro_temp like wca2.t690_temp;

insert into wca2.t690_statpro_temp select * from wca2.t690_temp;

ALTER TABLE `wca2`.`t690_statpro_temp` 
ADD COLUMN `Field25Name` VARCHAR(20) CHARACTER SET utf8 NOT NULL AFTER `Field24`,
ADD COLUMN `Field25` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `Field25Name`;

update wca2.t690_statpro_temp
set field25name = 'MarketIsin';

update wca2.t690_statpro_temp
set field25='';

update wca2.t690_statpro_temp
left outer join wca.scexh on wca2.t690_statpro_temp.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scexh.junklocalcode
where length(junklocalcode) = 12
and (ord(substring(wca.scexh.junklocalcode,1,2)) between 65 and 90 and ord(substring(wca.scexh.junklocalcode,12,1)) between 48 and 57
and substring(wca.scexh.junklocalcode,6,1) <> '/' and substring(wca.scexh.junklocalcode,5,1) <> '/')
and substring(wca.scexh.exchgcd,1,2) = 'DE';

update wca2.t690_statpro_temp
left outer join wca.scxtc on wca2.t690_statpro_temp.scexhid = wca.scxtc.scexhid
left outer join wca.scexh on wca.scxtc.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scxtc.isin
where 
substring(wca.scexh.exchgcd,1,2) = 'DE'
and wca.scxtc.isin <> '';

update wca2.t690_statpro_temp
set sedolcurrency = 'USD' 
where 
sedolcurrency = '' and exchgcntry = 'US';

delete from wca2.t690_statpro_temp
where eventcd='DIV'
and field4='T'
AND 1<(select count(subrd.rdid) from wca.rd as subrd
inner join wca.div_my on subrd.rdid = wca.div_my.rdid
where 
(subrd.rdid=wca2.t690_statpro_temp.rdid)) 
or (field1='CG' and field3='T');