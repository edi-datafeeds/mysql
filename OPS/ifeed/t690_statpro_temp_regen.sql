DROP TABLE IF EXISTS `wca2`.`t690_statpro_temp`;

CREATE TABLE wca2.t690_statpro_temp like wca2.t690_temp;

insert into wca2.t690_statpro_temp select * from wca2.t690_temp where changed > (select max(feeddate) from wca.tbl_opslog) 
and eventcd<>'NLIST' and eventcd <> 'SHOCH' and eventcd <> 'LSTAT';

delete from wca2.t690_statpro_temp where eventcd='LCC' and field3='';

insert ignore into wca2.t690_statpro_temp
select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scmst.SecID,
wca.issur.IssID,
wca.scmst.isin,
wca.scmst.uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='D' or wca.scexh.ListStatus='S' then wca.scexh.ListStatus else upper('L') end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'NotificationDate' as Date2Type,
vtab.NotificationDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLStatStatus' as Field2Name,
case when vtab.OldLStatStatus = '' or vtab.OldLStatStatus = 'N' or vtab.OldLStatStatus = 'R'
     then 'L'
     else vtab.OldLStatStatus 
     end as Field2,
'LStatStatus' as Field3Name,
vtab.LStatStatus as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lstat as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.ExchgCD
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
                             and ('X'<>substring(ifnull(wca.sedol.rcntrycd,''),1,1) and wca.exchg.exchgcd<>'HKHKSG' and wca.exchg.exchgcd<>'HKHKSZ' and wca.exchg.exchgcd<>'CNSGHK' and wca.exchg.exchgcd<>'CNSZHK'
                               or (wca.exchg.exchgcd='HKHKSG' and wca.sedol.rcntrycd='XG')
                               or (wca.exchg.exchgcd='HKHKSZ' and wca.sedol.rcntrycd='XZ')
                               or (wca.exchg.exchgcd='CNSGHK' and wca.sedol.rcntrycd='XH') 
                               or (wca.exchg.exchgcd='CNSZHK' and wca.sedol.rcntrycd='XH'))
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
Where
(3>wca.sectygrp.secgrpid
or(wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and vtab.effectivedate>(select date_sub(max(feeddate), interval 15 day) from wca.tbl_opslog where seq=3)
and (vtab.effectivedate=(if(dayofweek(now())<>6, (select date_sub(max(feeddate), interval 14 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 14 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 13 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
                or
    (vtab.effectivedate=(if(dayofweek(now())=6, (select date_sub(max(feeddate), interval 12 day) from wca.tbl_opslog where seq=3),
                        '1980-01-01'))AND vtab.lstatstatus='D')
and vtab.actflag<>'D'
and vtab.eventtype<>'CLEAN';

insert ignore into wca2.t690_statpro_temp
select distinct
'NLIST' AS EventCD,
wca.scexh.scexhid AS EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
wca.scexh.ActFlag,
wca.scexh.acttime as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
                             and ('X'<>substring(ifnull(wca.sedol.rcntrycd,''),1,1) and wca.exchg.exchgcd<>'HKHKSG' and wca.exchg.exchgcd<>'HKHKSZ' and wca.exchg.exchgcd<>'CNSGHK' and wca.exchg.exchgcd<>'CNSZHK'
                               or (wca.exchg.exchgcd='HKHKSG' and wca.sedol.rcntrycd='XG')
                               or (wca.exchg.exchgcd='HKHKSZ' and wca.sedol.rcntrycd='XZ')
                               or (wca.exchg.exchgcd='CNSGHK' and wca.sedol.rcntrycd='XH') 
                               or (wca.exchg.exchgcd='CNSZHK' and wca.sedol.rcntrycd='XH'))
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
Where
not (wca.scmst.sectycd='ETF' and wca.exchg.cntrycd='DE')
and (3>wca.sectygrp.secgrpid
or(wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and wca.scexh.announcedate>(select date_sub(max(feeddate), interval 150 day) from wca.tbl_opslog where seq = 3)
and ((wca.scexh.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))
or (wca.scmst.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3)) 
or (wca.sedol.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))) 
and wca.scexh.liststatus<>'D' and wca.scexh.liststatus<>'S' and wca.scmst.statusflag<>'I'
;

insert ignore into wca2.t690_statpro_temp
select
'IDIV' as EventCD,
vtab.rdid as EventID,
case when wca.intpy.OptionID is null then '1' else wca.intpy.OptionID end as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
case when wca.intpy.actflag IS NOT NULL 
     then wca.intpy.actflag 
     else vtab.Actflag 
     END as Actflag,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > vtab.Acttime) and (wca.rd.acttime > wca.exdt.acttime) and (wca.rd.acttime > pexdt.acttime)
     then wca.rd.acttime 
     when (wca.exdt.acttime is not null) and (wca.exdt.Acttime > vtab.Acttime) and (wca.exdt.acttime > pexdt.acttime)
     then wca.exdt.acttime 
     when (pexdt.acttime is not null) and (pexdt.acttime > vtab.acttime)
     then pexdt.acttime
     else vtab.Acttime 
     END as Changed,
vtab.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.USCode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
case when wca.scexh.ExchgCD = 'USTRCE' then 'USNYSE' else wca.scexh.ExchgCD end as ExchgCD,
case when wca.exchg.Mic = 'FINR' then 'XNYS' else wca.exchg.Mic end as Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN wca.exdt.Paydate2 
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN pexdt.Paydate2 
     ELSE null END as Date4,
'FYEDate' as Date5Type,
null as Date5,
'PeriodEndDate' as Date6Type,
null as Date6,
'OptElectionDate' as Date7Type,
wca.intpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
null as Date8,
'RegistrationDate' as Date9Type,
null as Date9,
'DeclarationDate' as Date10Type,
null as Date10,
'Exdate2' as Date11Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN wca.exdt.Exdate2 
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN pexdt.Exdate2 
     ELSE null END as Date11,
'FXDate' as Date12Type,
null as Date12,
wca.intpy.inttype as Paytype,
wca.rd.RdID,
'' as priority,
wca.intpy.DefaultOpt,
wca.intpy.ResSecID as OutturnSecID,
resscmst.isin as OutturnIsin,
substring(wca.intpy.ratioold,1,instr(wca.intpy.ratioold,'.')+7) as RatioOld,
substring(wca.intpy.rationew,1,instr(wca.intpy.rationew,'.')+7) as RatioNew,
wca.intpy.Fractions as Fractions,
wca.intpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
case when (wca.intpy.anlcouprate = '' or wca.intpy.anlcouprate is null or wca.scmst.parvalue = '')
     then ''
     when wca.bond.InterestPaymentFrequency = 'ANL'
     then cast(wca.intpy.AnlCoupRate*0.01*wca.scmst.parvalue as decimal(23,6))
     when wca.bond.InterestPaymentFrequency = 'MNT'
     then cast(wca.intpy.AnlCoupRate*0.01*wca.scmst.parvalue/12 as decimal(23,6))
     when wca.bond.InterestPaymentFrequency = 'QTR'
     then cast(wca.intpy.AnlCoupRate*0.01*wca.scmst.parvalue/4 as decimal(23,6))
     when wca.bond.InterestPaymentFrequency = 'SMA'
     then cast(wca.intpy.AnlCoupRate*0.01*wca.scmst.parvalue/2 as decimal(23,6))
     end as Rate1,
'NetDividend' as Rate2Type,
wca.intpy.NetInterest as  Rate2,
'Marker' as Field1Name,
'' as Field1,
'Frequency' as Field2Name,
wca.bond.InterestPaymentFrequency as Field2,
'Tbaflag' as Field3Name,
'' as Field3,
'NilDividend' as Field4Name,
vtab.nilint as Field4,
'DivRescind' as Field5Name,
wca.intpy.RescindInterest as Field5,
'RecindCashDiv' as Field6Name,
'' as Field6,
'RescindStockDiv' as Field7Name,
wca.intpy.RescindStockInterest as Field7,
'Approxflag' as Field8Name,
'' as Field8,
'TaxRate' as Field9Name,
wca.intpy.DomesticTaxRate as Field9,
'Depfees' as Field10Name,
'' as Field10,
'Coupon' as Field11Name,
wca.intpy.CouponNo as Field11,
'Dapflag' as Field12Name,
'' as Field12,
'InstallmentPayDate' as Field13Name,
'' as Field13,
'DeclCurenCD' as Field14Name,
'' as Field14,
'DeclGrossAmt' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_intpy as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
                             and ('X'<>substring(ifnull(wca.sedol.rcntrycd,''),1,1) and wca.exchg.exchgcd<>'HKHKSG' and wca.exchg.exchgcd<>'HKHKSZ' and wca.exchg.exchgcd<>'CNSGHK' and wca.exchg.exchgcd<>'CNSZHK'
                               or (wca.exchg.exchgcd='HKHKSG' and wca.sedol.rcntrycd='XG')
                               or (wca.exchg.exchgcd='HKHKSZ' and wca.sedol.rcntrycd='XZ')
                               or (wca.exchg.exchgcd='CNSGHK' and wca.sedol.rcntrycd='XH') 
                               or (wca.exchg.exchgcd='CNSZHK' and wca.sedol.rcntrycd='XH'))
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd 
left outer join wca.intpy on vtab.rdid=intpy.rdid
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.intpy.ressecid = resscmst.secid
Where
vtab.actflag<>'D'
and ((wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and ((wca.scmst.sectycd='BND'
or wca.scmst.sectycd='SP'
or wca.scmst.sectycd='PFS'
or wca.scmst.sectycd='PRF')
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and wca.exchg.cntrycd='US'
and (wca.rd.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3)
or ifnull(wca.exdt.acttime,'2000-01-01')>=(select max(feeddate) from wca.tbl_opslog where seq=3)
or ifnull(pexdt.acttime,'2000-01-01')>=(select max(feeddate) from wca.tbl_opslog where seq=3)
or ifnull(vtab.acttime,'2000-01-01')>=(select max(feeddate) from wca.tbl_opslog where seq=3));

ALTER TABLE `wca2`.`t690_statpro_temp` 
ADD COLUMN `Field25Name` VARCHAR(20) CHARACTER SET utf8 NOT NULL AFTER `Field24`,
ADD COLUMN `Field25` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `Field25Name`;

update wca2.t690_statpro_temp
set field25name = 'MarketIsin';

update wca2.t690_statpro_temp
set field25='';

update wca2.t690_statpro_temp
left outer join wca.scexh on wca2.t690_statpro_temp.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scexh.junklocalcode
where length(junklocalcode) = 12
and (ord(substring(wca.scexh.junklocalcode,1,2)) between 65 and 90 and ord(substring(wca.scexh.junklocalcode,12,1)) between 48 and 57
and substring(wca.scexh.junklocalcode,6,1) <> '/' and substring(wca.scexh.junklocalcode,5,1) <> '/');

update wca2.t690_statpro_temp
left outer join wca.scxtc on wca2.t690_statpro_temp.scexhid = wca.scxtc.scexhid
left outer join wca.scexh on wca.scxtc.scexhid = wca.scexh.scexhid
set wca2.t690_statpro_temp.field25 = wca.scxtc.isin
where 
wca.scxtc.isin <> '';

update wca2.t690_statpro_temp
set sedolcurrency = 'USD' 
where 
sedolcurrency = '' and exchgcntry = 'US';

delete from wca2.t690_statpro_temp
where eventcd='DIV'
and field4='T'
AND 1<(select count(subrd.rdid) from wca.rd as subrd
inner join wca.div_my on subrd.rdid = wca.div_my.rdid
where 
(subrd.rdid=wca2.t690_statpro_temp.rdid)) 
or (field1='CG' and field3='T');

insert ignore into wca2.t690_statpro_temp
select distinct
'NLIST' AS EventCD,
wca.scexh.scexhid AS EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
wca.scexh.ActFlag,
wca.scexh.acttime as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is not null
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when wca.scxtc.scexhid is not null
     then wca.scxtc.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
case when wca.scxtc.scexhid is not null
     then wca.scxtc.ListDate
     else wca.scexh.ListDate
     end as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24,
'' as Field25Name,
'' as Field25
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.scxtc on wca.scexh.scexhid=wca.scxtc.scexhid and 'D'<>wca.scxtc.actflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
Where
wca.scmst.sectycd='ETF' and wca.exchg.cntrycd='DE'
and (3>wca.sectygrp.secgrpid
or(wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and wca.scexh.announcedate>(select date_sub(max(feeddate), interval 150 day) from wca.tbl_opslog where seq = 3)
and ((wca.scexh.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))
or (wca.scmst.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3)) 
or (wca.sedol.acttime>=(select max(feeddate) from wca.tbl_opslog where seq=3))) 
and wca.scexh.liststatus<>'D' and wca.scexh.liststatus<>'S' and wca.scmst.statusflag<>'I'
;
