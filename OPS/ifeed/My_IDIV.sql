DROP TABLE IF EXISTS `wca2`.`t695_temp_idiv`;
CREATE TABLE `wca2`.`t695_temp_idiv` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `OptionID` int(10) unsigned NOT NULL,
  `SerialID` int(10) unsigned NOT NULL,
  `ScexhID` int(10) unsigned NOT NULL DEFAULT '0',
  `Actflag` char(1) DEFAULT NULL,
  `Changed` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `Isin` char(12) DEFAULT NULL,
  `Uscode` char(9) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `IndusID` int(10) unsigned DEFAULT NULL,
  `SectyCD` char(3) DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` char(1) DEFAULT NULL,
  `PrimaryExchgCD` char(6) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) DEFAULT NULL,
  `Micseg` char(4) DEFAULT NULL,
  `LocalCode` varchar(50) DEFAULT NULL,
  `ListStatus` varchar(1) DEFAULT NULL,
  `Exdate` datetime DEFAULT NULL,
  `RecDate` datetime DEFAULT NULL,
  `Paydate` datetime DEFAULT NULL,
  `Paytype` char(1) CHARACTER SET utf8 NOT NULL,
  `RdID` int(10) DEFAULT NULL,
  `Currency` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `GrossDividend` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `NetDividend` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Frequency` varchar(255) DEFAULT NULL,
  `NilDividend` varchar(255) DEFAULT NULL,
  `DivRescind` varchar(255) DEFAULT NULL,
  `DomesticTaxRate` varchar(255) DEFAULT NULL,
  `CouponNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EventCD`,`EventID`,`OptionID`,`SerialID`,`ScexhID`,`BbcID`,`BbeID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
insert ignore into wca2.t695_temp_idiv
select
'IDIV' as EventCD,
vtab.rdid as EventID,
case when wca.intpy.OptionID is null then '1' else wca.intpy.OptionID end as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.intpy.actflag IS NOT NULL 
     then wca.intpy.actflag 
     else vtab.Actflag 
     END as Actflag,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > vtab.Acttime) and (wca.rd.acttime > wca.exdt.acttime) and (wca.rd.acttime > pexdt.acttime)
     then wca.rd.acttime 
     when (wca.exdt.acttime is not null) and (wca.exdt.Acttime > vtab.Acttime) and (wca.exdt.acttime > pexdt.acttime)
     then wca.exdt.acttime 
     when (pexdt.acttime is not null) and (pexdt.acttime > vtab.acttime)
     then pexdt.acttime
     else vtab.Acttime 
     END as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.USCode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
case when stab.SectyCD = 'BND' then 'PRF' else stab.SectyCD end as SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
case when wca.scexh.ExchgCD = 'USTRCE' then 'USNYSE' else wca.scexh.ExchgCD end as ExchgCD,
case when wca.exchg.Mic = 'FINR' then 'XNYS' else wca.exchg.Mic end as Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Exdate,
wca.rd.Recdate as Recdate,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Paydate,
wca.intpy.inttype as Paytype,
wca.rd.RdID,
wca.intpy.CurenCD as Currency,
wca.intpy.GrossInterest*stab.ParValue as GrossDividend,
wca.intpy.NetInterest as  NetDividend,
wca.bond.InterestPaymentFrequency as Frequency,
vtab.nilint as NilDividend,
wca.intpy.RescindInterest as DivRescind,
wca.intpy.DomesticTaxRate,
wca.intpy.CouponNo
from wca.v10s_intpy as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.v20c_680_scmst_fi as stab on wca.rd.secid = stab.secid
LEFT OUTER join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd 
left outer join wca.intpy on vtab.rdid=intpy.rdid
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.intpy.ressecid = resscmst.secid
Where
wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>'2018-01-01' and vtab.acttime < '2019-01-01' and vtab.actflag<>'D';
