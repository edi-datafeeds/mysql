-- # Drop t680i4_merqurian_temp
DROP TABLE IF EXISTS `wca2`.`t680i4_merqurian_temp`;

-- # Create t680i4_merqurian_temp
CREATE TABLE wca2.t680i4_merqurian_temp like wca2.t680_temp;

-- # Insert into t680i4_merqurian_temp everything from wca2.t680_temp
insert ignore into wca2.t680i4_merqurian_temp select * from wca2.t680_temp 
where eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH' and eventcd <> 'TKOVR';

-- # Insert into t680i4_merqurian_temp everything from wca2.t680_temp_prf
insert ignore into wca2.t680i4_merqurian_temp select * from wca2.t680_temp_prf 
where eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH' and eventcd <> 'TKOVR';

-- # Set @fromdate
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);

-- # Insert TKOVR into t680i4_merqurian_temp
insert ignore into wca2.t680i4_merqurian_temp
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (mpay.acttime >= vtab.acttime) 
      and (mpay.acttime >= rd.acttime) 
     then mpay.acttime 
     when rd.acttime >= vtab.acttime 
     then rd.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Opendate' as Date1Type,
vtab.opendate as Date1,
'Closedate' as Date2Type,
vtab.closedate as Date2,
'Recdate' as Date3Type,
wca.rd.Recdate as Date3,
'Cmacqdate' as Date4Type,
vtab.Cmacqdate as Date4,
'Paydate' as Date5Type,
wca.mpay.Paydate as Date5,
'Optelectiondate' as Date6Type,
wca.mpay.Optelectiondate as Date6,
'Withdrawalfromdate' as Date7Type,
wca.mpay.Withdrawalfromdate as Date7,
'Withdrawaltodate' as Date8Type,
wca.mpay.Withdrawaltodate as Date8,
'UnconditionalDate' as Date9Type,
vtab.UnconditionalDate as Date9,
'ToDate' as Date10Type,
wca.rd.ToDate as Date10,
'RegistrationDate' as Date11Type,
wca.rd.RegistrationDate as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioOld,
wca.mpay.RatioNew,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'TkovrStatus' as Field1Name,
vtab.TkovrStatus as Field1,
'OfferorIssID' as Field2Name,
vtab.OfferorIssID as Field2,
'OfferorName' as Field3Name,
vtab.OfferorName as Field3,
'Hostile' as Field4Name,
case when wca.vtab.Hostile='T' then 'T' else 'F' end as Hostile,
'MiniTkovr' as Field5Name,
case when wca.vtab.MiniTkovr='T' then 'T' else 'F' end as MiniTkovr,
'ResSecTyCD' as Field6Name,
mpay.SecTyCD as Field6,
'TargetQuantity' as Field7Name,
vtab.TargetQuantity as Field7,
'TargetPercent' as Field8Name,
vtab.TargetPercent as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'PreOfferQty' as Field11Name,
vtab.PreOfferQty as Field11,
'PreOfferPercent' as Field12Name,
vtab.PreOfferPercent as Field12,
'TndrStrkPrice' as Field13Name,
mpay.TndrStrkPrice as Field13,
'TndrStrkStep' as Field14Name,
mpay.TndrStrkStep as Field14,
'MinQlyQty' as Field15Name,
mpay.MinQlyQty as Field15,
'MaxQlyQty' as Field16Name,
mpay.MaxQlyQty as Field16,
'MinOfrQty' as Field17Name,
mpay.MinOfrQty as Field17,
'MaxOfrQty' as Field18Name,
mpay.MaxOfrQty as Field18,
'DutchAuction' as Field19Name,
case when wca.mpay.DutchAuction='T' then 'T' else 'F' end as DutchAuction,
'WithdrawalRights' as Field20Name,
mpay.WithdrawalRights as Field20,
'ExpCompletionDate' as Field21Name,
vtab.ExpCompletionDate as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_tkovr as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.tkovrid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and wca.scexh.exchgcd<>'CNSGHK' and wca.scexh.exchgcd<>'HKHKSG' and wca.scexh.exchgcd<>'HKHKSZ' and wca.scexh.exchgcd<>'CNSZHK'
and (vtab.acttime>=@fromdate
or mpay.acttime>=@fromdate
or rd.acttime>=@fromdate);


