INSERT INTO wca.bochg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldOutValue, NewOutValue, bochgID, bochgNotes, Eventtype, OldOutDate, NewOutDate, ReleventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldOutValue, NewOutValue, efdtlinkid, zbochgNotes, Eventtype, OldOutDate, NewOutDate, ReleventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zbochg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select bochgid from wca.bochg)
and zbochgid = (select zbochgid from wca.zbochg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.bochg as m
inner join wca.zbochg as z on m.bochgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldOutValue=z.OldOutValue, m.bochgNotes=z.zbochgNotes, m.Eventtype=z.Eventtype,
m.OldOutDate=z.OldOutDate, m.ReleventID=z.ReleventID,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zbochgid = (select zbochgid from wca.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.bochg as m
inner join wca.zbochg as z on m.bochgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewOutValue=z.NewOutValue, m.bochgNotes=z.zbochgNotes, m.Eventtype=z.Eventtype,
m.NewOutDate=z.NewOutDate, m.ReleventID=z.ReleventID,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zbochgid = (select zbochgid from wca.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.bochg as m
inner join wca.zbochg as z on m.bochgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zbochgid = (select zbochgid from wca.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.bschg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldBondType, NewBondType, OldCurenCD, NewCurenCD, OldPIU, NewPIU, OldInterestBasis, NewInterestBasis,
Eventtype, BschgID, BschgNotes, OldInterestCurrency, NewInterestCurrency, OldMaturityCurrency, NewMaturityCurrency,
OldIntBusDayConv, NewIntBusDayConv, OldMatBusDayConv, NewMatBusDayConv, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldBondType, NewBondType, OldCurenCD, NewCurenCD, OldPIU, NewPIU, OldInterestBasis, NewInterestBasis,
Eventtype, EfdtLinkID, zbschgNotes, OldInterestCurrency, NewInterestCurrency, OldMaturityCurrency, NewMaturityCurrency,
OldIntBusDayConv, NewIntBusDayConv, OldMatBusDayConv, NewMatBusDayConv, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zbschg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select bschgid from wca.bschg)
and zbschgid = (select zbschgid from wca.zbschg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.bschg as m
inner join wca.zbschg as z on m.bschgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldBondType=z.OldBondType, m.OldCurenCD=z.OldCurenCD, m.OldPIU=z.OldPIU,
m.OldInterestBasis=z.OldInterestBasis, m.Eventtype=z.Eventtype, m.BschgNotes=z.zbschgNotes, m.OldInterestCurrency=z.OldInterestCurrency,
m.OldMaturityCurrency=z.OldMaturityCurrency, m.OldIntBusDayConv=z.OldIntBusDayConv, m.OldMatBusDayConv=z.OldMatBusDayConv,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zbschgid = (select zbschgid from wca.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.bschg as m
inner join wca.zbschg as z on m.bschgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewBondType=z.NewBondType, m.NewCurenCD=z.NewCurenCD, m.NewPIU=z.NewPIU,
m.NewInterestBasis=z.NewInterestBasis, m.Eventtype=z.Eventtype, m.BschgNotes=z.zbschgNotes, m.NewInterestCurrency=z.NewInterestCurrency,
m.NewMaturityCurrency=z.NewMaturityCurrency, m.NewIntBusDayConv=z.NewIntBusDayConv, m.NewMatBusDayConv=z.NewMatBusDayConv,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zbschgid = (select zbschgid from wca.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.bschg as m
inner join wca.zbschg as z on m.bschgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zbschgid = (select zbschgid from wca.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.crchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, RatingAgency, NewRatingDate, OldRating, NewRating, OldDirection, OldWatchList, OldWatchListReason, crchgID, EventType,
OldRatingDate, NewDirection, NewWatchList, NewWatchListReason, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, RatingAgency, NewRatingDate, OldRating, NewRating, OldDirection, OldWatchList, OldWatchListReason, EfdtLinkID, EventType,
OldRatingDate, NewDirection, NewWatchList, NewWatchListReason,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zcrchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select crchgid from wca.crchg)
and zcrchgid = (select zcrchgid from wca.zcrchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.crchg as m
inner join wca.zcrchg as z on m.crchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.RatingAgency=z.RatingAgency, m.OldRating=z.OldRating, m.OldDirection=z.OldDirection, m.OldWatchList=z.OldWatchList,
m.OldWatchListReason=z.OldWatchListReason, m.EventType=z.EventType, m.OldRatingDate=z.OldRatingDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zcrchgid = (select zcrchgid from wca.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.crchg as m
inner join wca.zcrchg as z on m.crchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecId=z.SecId, m.RatingAgency=z.RatingAgency, m.NewRatingDate=z.NewRatingDate, m.NewRating=z.NewRating, m.EventType=z.EventType,
m.NewDirection=z.NewDirection, m.NewWatchList=z.NewWatchList, m.NewWatchListReason=z.NewWatchListReason,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zcrchgid = (select zcrchgid from wca.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.crchg as m
inner join wca.zcrchg as z on m.crchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zcrchgid = (select zcrchgid from wca.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.ctchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldResultantRatio, NewResultantRatio, OldSecurityRatio, NewSecurityRatio, OldCurrency, NewCurrency, OldConversionPrice,
NewConversionPrice, ResSecTyCD, OldResSecID, NewResSecID, EventType, RelEventID, Notes, CtchgID, OldFromDate, NewFromDate, OldTodate, NewToDate,
ConvtID, OldFXRate, NewFXRate, OldPriceAsPercent, NewPriceAsPercent, OldCurPair, NewCurPair, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldResultantRatio, NewResultantRatio, OldSecurityRatio, NewSecurityRatio, OldCurrency, NewCurrency, OldConversionPrice, NewConversionPrice, ResSecTyCD, OldResSecID, NewResSecID, EventType, RelEventID, zCtchgNotes, efdtlinkid, OldFromDate, NewFromDate, OldTodate, NewToDate, ConvtID, OldFXRate, NewFXRate, OldPriceAsPercent, NewPriceAsPercent, OldCurPair, NewCurPair, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate FROM wca.zctchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ctchgid from wca.ctchg)
and zctchgid = (select zctchgid from wca.zctchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ctchg as m
inner join wca.zctchg as z on m.CtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.OldResultantRatio=z.OldResultantRatio, m.OldSecurityRatio=z.OldSecurityRatio, 
m.OldCurrency=z.OldCurrency, m.OldConversionPrice=z.OldConversionPrice, m.ResSecTyCD=z.ResSecTyCD, m.OldResSecID=z.OldResSecID,
m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.OldFromDate=z.OldFromDate, m.OldTodate=z.OldTodate, m.ConvtID=z.ConvtID,
m.OldFXRate=z.OldFXRate, m.OldPriceAsPercent=z.OldPriceAsPercent, m.OldCurPair=z.OldCurPair,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zctchgid = (select zctchgid from wca.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ctchg as m
inner join wca.zctchg as z on m.CtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.OldResultantRatio=z.OldResultantRatio, m.OldSecurityRatio=z.OldSecurityRatio, 
m.OldCurrency=z.OldCurrency, m.OldConversionPrice=z.OldConversionPrice, m.ResSecTyCD=z.ResSecTyCD, m.OldResSecID=z.OldResSecID,
m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.OldFromDate=z.OldFromDate, m.OldTodate=z.OldTodate, m.ConvtID=z.ConvtID,
m.OldFXRate=z.OldFXRate, m.OldPriceAsPercent=z.OldPriceAsPercent, m.OldCurPair=z.OldCurPair,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zctchgid = (select zctchgid from wca.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.ctchg as m
inner join wca.zctchg as z on m.CtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zctchgid = (select zctchgid from wca.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.ffc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFreeFloat, NewFreeFloat, EventType, FfcID, OldFreeFloatDate, NewFreeFloatDate,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFreeFloat, NewFreeFloat, EventType, EfdtLinkID, OldFreeFloatDate, NewFreeFloatDate,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zffc as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ffcid from wca.ffc)
and zffcid = (select zffcid from wca.zffc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ffc as m
inner join wca.zffc as z on m.ffcID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldFreeFloat=z.OldFreeFloat, m.EventType=z.EventType, m.OldFreeFloatDate=z.OldFreeFloatDate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zffcid = (select zffcid from wca.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ffc as m
inner join wca.zffc as z on m.ffcID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewFreeFloat=z.NewFreeFloat, m.EventType=z.EventType, m.NewFreeFloatDate=z.NewFreeFloatDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zffcid = (select zffcid from wca.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.ffc as m
inner join wca.zffc as z on m.ffcID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zffcid = (select zffcid from wca.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.ifchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldIntPayFrqncy, OldIntPayDate1, OldIntPayDate2, OldIntPayDate3, OldIntPayDate4, NewIntPayFrqncy, NewIntPayDate1,
NewIntPayDate2, NewIntPayDate3, NewIntPayDate4, IfchgID, EventType, OldVarIntPaydate, NewVarIntPaydate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldIntPayFrqncy, OldIntPayDate1, OldIntPayDate2, OldIntPayDate3, OldIntPayDate4, NewIntPayFrqncy, NewIntPayDate1,
NewIntPayDate2, NewIntPayDate3, NewIntPayDate4, EfdtLinkID, EventType, OldVarIntPaydate, NewVarIntPaydate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zifchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ifchgid from wca.ifchg)
and zifchgid = (select zifchgid from wca.zifchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ifchg as m
inner join wca.zifchg as z on m.ifchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldIntPayFrqncy=z.OldIntPayFrqncy, m.OldIntPayDate1=z.OldIntPayDate1,
m.OldIntPayDate2=z.OldIntPayDate2, m.OldIntPayDate3=z.OldIntPayDate3, m.OldIntPayDate4=z.OldIntPayDate4, m.EventType=z.EventType,
m.OldVarIntPaydate=z.OldVarIntPaydate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zifchgid = (select zifchgid from wca.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ifchg as m
inner join wca.zifchg as z on m.ifchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewIntPayFrqncy=z.NewIntPayFrqncy, m.NewIntPayDate1=z.NewIntPayDate1,
m.NewIntPayDate2=z.NewIntPayDate2, m.NewIntPayDate3=z.NewIntPayDate3, m.NewIntPayDate4=z.NewIntPayDate4, m.EventType=z.EventType,
m.NewVarIntPaydate=z.NewVarIntPaydate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zifchgid = (select zifchgid from wca.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.ifchg as m
inner join wca.zifchg as z on m.ifchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zifchgid = (select zifchgid from wca.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.icc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldISIN, NewISIN, OldUSCode, NewUSCode, OldVALOREN, NewVALOREN, IccID, EventType, RelEventID, OldCommonCode, NewCommonCode, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldISIN, NewISIN, OldUSCode, NewUSCode, OldVALOREN, NewVALOREN, EfdtLinkID, EventType, RelEventID, OldCommonCode, NewCommonCode, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zicc as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select iccid from wca.icc)
and ziccid = (select ziccid from wca.zicc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.icc as m
inner join wca.zicc as z on m.iccID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldISIN=z.OldISIN, m.OldUSCode=z.OldUSCode, m.OldVALOREN=z.OldVALOREN, m.EventType=z.EventType,
m.RelEventID=z.RelEventID, m.OldCommonCode=z.OldCommonCode, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.ziccid = (select ziccid from wca.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.icc as m
inner join wca.zicc as z on m.iccID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewISIN=z.NewISIN, m.NewUSCode=z.NewUSCode, m.NewVALOREN=z.NewVALOREN, m.EventType=z.EventType,
m.RelEventID=z.RelEventID, m.NewCommonCode=z.NewCommonCode, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.ziccid = (select ziccid from wca.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.icc as m
inner join wca.zicc as z on m.iccID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.ziccid = (select ziccid from wca.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.irchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestRate, NewInterestRate, IRChgID, Notes, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestRate, NewInterestRate, EfdtLinkID, zIRChgNotes, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zirchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select irchgid from wca.irchg)
and zirchgid = (select zirchgid from wca.zirchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.irchg as m
inner join wca.zirchg as z on m.irchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldInterestRate=z.OldInterestRate, m.Notes=z.zIRChgNotes, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zirchgid = (select zirchgid from wca.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.irchg as m
inner join wca.zirchg as z on m.irchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewInterestRate=z.NewInterestRate, m.Notes=z.zIRChgNotes, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zirchgid = (select zirchgid from wca.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.irchg as m
inner join wca.zirchg as z on m.irchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zirchgid = (select zirchgid from wca.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.ischg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
IssID, NameChangeDate, IssOldName, IssNewName, EventType, IsChgID, IsChgNotes, LegalName, MeetingDateFlag, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
IssID, NameChangeDate, IssOldName, IssNewName, EventType, EfdtLinkID, zIsChgNotes, LegalName, MeetingDateFlag, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zischg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ischgid from wca.ischg)
and zischgid = (select zischgid from wca.zischg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ischg as m
inner join wca.zischg as z on m.ischgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.IssID=z.IssID, m.NameChangeDate=z.NameChangeDate, m.IssOldName=z.IssOldName, m.EventType=z.EventType, m.IsChgNotes=z.zIsChgNotes,
m.LegalName=z.LegalName, m.MeetingDateFlag=z.MeetingDateFlag, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zischgid = (select zischgid from wca.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ischg as m
inner join wca.zischg as z on m.ischgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.IssID=z.IssID, m.NameChangeDate=z.NameChangeDate, m.IssNewName=z.IssNewName, m.EventType=z.EventType, m.IsChgNotes=z.zIsChgNotes,
m.LegalName=z.LegalName, m.MeetingDateFlag=z.MeetingDateFlag, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zischgid = (select zischgid from wca.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.ischg as m
inner join wca.zischg as z on m.ischgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zischgid = (select zischgid from wca.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.frnfx (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFRNType, OldFRNIndexBenchmark, OldMarkup, OldMinimumInterestRate, OldMaximumInterestRate, OldRounding, NewFRNType, 
NewFRNindexBenchmark, NewMarkup, NewMinimumInterestRate, NewMaximumInterestRate, NewRounding, FrnfxID, EventType, OldFrnIntAdjFreq, NewFrnIntAdjFreq, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFRNType, OldFRNIndexBenchmark, OldMarkup, OldMinimumInterestRate, OldMaximumInterestRate, OldRounding, NewFRNType,
NewFRNindexBenchmark, NewMarkup, NewMinimumInterestRate, NewMaximumInterestRate, NewRounding, EfdtLinkID, EventType, OldFrnIntAdjFreq, NewFrnIntAdjFreq, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zfrnfx as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select frnfxid from wca.frnfx)
and zfrnfxid = (select zfrnfxid from wca.zfrnfx as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.frnfx as m
inner join wca.zfrnfx as z on m.frnfxID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldFRNType=z.OldFRNType, m.OldFRNIndexBenchmark=z.OldFRNIndexBenchmark, m.OldMarkup=z.OldMarkup,
m.OldMinimumInterestRate=z.OldMinimumInterestRate, m.OldMaximumInterestRate=z.OldMaximumInterestRate, m.OldRounding=z.OldRounding, m.EventType=z.EventType,
m.OldFrnIntAdjFreq=z.OldFrnIntAdjFreq, m.NewFrnIntAdjFreq=z.NewFrnIntAdjFreq, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zfrnfxid = (select zfrnfxid from wca.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.frnfx as m
inner join wca.zfrnfx as z on m.frnfxID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate,  m.NewFRNType=z.NewFRNType, m.NewFRNindexBenchmark=z.NewFRNindexBenchmark, m.NewMarkup=z.NewMarkup,
m.NewMinimumInterestRate=z.NewMinimumInterestRate, m.NewMaximumInterestRate=z.NewMaximumInterestRate, m.NewRounding=z.NewRounding, m.EventType=z.EventType,
m.OldFrnIntAdjFreq=z.OldFrnIntAdjFreq, m.NewFrnIntAdjFreq=z.NewFrnIntAdjFreq, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zfrnfxid = (select zfrnfxid from wca.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.frnfx as m
inner join wca.zfrnfx as z on m.frnfxID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zfrnfxid = (select zfrnfxid from wca.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.lcc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLocalCode, NewLocalCode, LccID, EventType, RelEventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLocalCode, NewLocalCode, EfdtLinkID, EventType, RelEventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zlcc as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select lccid from wca.lcc)
and zlccid = (select zlccid from wca.zlcc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.lcc as m
inner join wca.zlcc as z on m.lccID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.OldLocalCode=z.OldLocalCode, m.EventType=z.EventType, m.RelEventID=z.RelEventID, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zlccid = (select zlccid from wca.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.lcc as m
inner join wca.zlcc as z on m.lccID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.NewLocalCode=z.NewLocalCode, m.EventType=z.EventType, m.RelEventID=z.RelEventID, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zlccid = (select zlccid from wca.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.lcc as m
inner join wca.zlcc as z on m.lccID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zlccid = (select zlccid from wca.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.lstat (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
LstatID, SecID, ExchgCD, NotificationDate, EffectiveDate, LStatStatus, Reason, Eventtype, OldLStatStatus, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
EfdtLinkID, SecID, ExchgCD, NotificationDate, EffectiveDate, LStatStatus, Reason, Eventtype, OldLStatStatus, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zlstat as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select lstatid from wca.lstat)
and zlstatid = (select zlstatid from wca.zlstat as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.lstat as m
inner join wca.zlstat as z on m.lstatID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.NotificationDate=z.NotificationDate, m.EffectiveDate=z.EffectiveDate, m.Reason=z.Reason,
m.Eventtype=z.Eventtype, m.OldLStatStatus=z.OldLStatStatus, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zlstatid = (select zlstatid from wca.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.lstat as m
inner join wca.zlstat as z on m.lstatID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.NotificationDate=z.NotificationDate, m.EffectiveDate=z.EffectiveDate, m.LStatStatus=z.LStatStatus,
m.Reason=z.Reason, m.Eventtype=z.Eventtype, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zlstatid = (select zlstatid from wca.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.lstat as m
inner join wca.zlstat as z on m.lstatID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zlstatid = (select zlstatid from wca.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.ltchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLot, OldMinTrdQty, NewLot, NewMinTrdgQty, LtChgID, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLot, OldMinTrdQty, NewLot, NewMinTrdgQty, EfdtLinkID, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zltchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ltchgid from wca.ltchg)
and zltchgid = (select zltchgid from wca.zltchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ltchg as m
inner join wca.zltchg as z on m.ltchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.OldLot=z.OldLot, m.OldMinTrdQty=z.OldMinTrdQty, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zltchgid = (select zltchgid from wca.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.ltchg as m
inner join wca.zltchg as z on m.ltchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.NewLot=z.NewLot, m.NewMinTrdgQty=z.NewMinTrdgQty, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zltchgid = (select zltchgid from wca.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.ltchg as m
inner join wca.zltchg as z on m.ltchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zltchgid = (select zltchgid from wca.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.mtchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldMaturityDate, NewMaturityDate, Reason, MtChgID, Notes, EventType, OldMaturityBenchmark, NewMaturityBenchmark, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldMaturityDate, NewMaturityDate, Reason, EfdtLinkID, zMtChgNotes, EventType, OldMaturityBenchmark, NewMaturityBenchmark, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zmtchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select mtchgid from wca.mtchg)
and zmtchgid = (select zmtchgid from wca.zmtchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.mtchg as m
inner join wca.zmtchg as z on m.mtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldMaturityDate=z.OldMaturityDate, m.Reason=z.Reason, m.Notes=z.zMtChgNotes,
m.EventType=z.EventType, m.OldMaturityBenchmark=z.OldMaturityBenchmark, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zmtchgid = (select zmtchgid from wca.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.mtchg as m
inner join wca.zmtchg as z on m.mtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewMaturityDate=z.NewMaturityDate, m.Reason=z.Reason, m.Notes=z.zMtChgNotes,
m.EventType=z.EventType, m.NewMaturityBenchmark=z.NewMaturityBenchmark, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zmtchgid = (select zmtchgid from wca.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.mtchg as m
inner join wca.zmtchg as z on m.mtchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zmtchgid = (select zmtchgid from wca.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.rconv (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestAccrualConvention, NewInterestAccrualConvention, Notes, RconvID, OldConvMethod, NewConvMethod, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestAccrualConvention, NewInterestAccrualConvention, zRconvNotes, EfdtLinkID, OldConvMethod, NewConvMethod, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zrconv as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select rconvid from wca.rconv)
and zrconvid = (select zrconvid from wca.zrconv as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.rconv as m
inner join wca.zrconv as z on m.rconvID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldInterestAccrualConvention=z.OldInterestAccrualConvention, m.Notes=z.zRconvNotes, m.OldConvMethod=z.OldConvMethod,  m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zrconvid = (select zrconvid from wca.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.rconv as m
inner join wca.zrconv as z on m.rconvID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewInterestAccrualConvention=z.NewInterestAccrualConvention, m.Notes=z.zRconvNotes, m.NewConvMethod=z.NewConvMethod, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zrconvid = (select zrconvid from wca.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.rconv as m
inner join wca.zrconv as z on m.rconvID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zrconvid = (select zrconvid from wca.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.rdnom (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldDenomination1, OldDenomination2, OldDenomination3, OldDenomination4, OldDenomination5, OldDenomination6,
OldDenomination7, OldMinimumDenomination, OldDenominationMultiple, NewDenomination1, NewDenomination2, NewDenomination3,
NewDenomination4, NewDenomination5, NewDenomination6, NewDenomination7, NewMinimumDenomination, NewDenominationMultiple, Notes, RdnomID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldDenomination1, OldDenomination2, OldDenomination3, OldDenomination4, OldDenomination5, OldDenomination6,
OldDenomination7, OldMinimumDenomination, OldDenominationMultiple, NewDenomination1, NewDenomination2, NewDenomination3,
NewDenomination4, NewDenomination5, NewDenomination6, NewDenomination7, NewMinimumDenomination, NewDenominationMultiple, zRdnomNotes, EfdtLinkID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zrdnom as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select rdnomid from wca.rdnom)
and zrdnomid = (select zrdnomid from wca.zrdnom as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.rdnom as m
inner join wca.zrdnom as z on m.rdnomID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldDenomination1=z.OldDenomination1, m.OldDenomination2=z.OldDenomination2,
m.OldDenomination3=z.OldDenomination3, m.OldDenomination4=z.OldDenomination4, m.OldDenomination5=z.OldDenomination5,
m.OldDenomination6=z.OldDenomination6, m.OldDenomination7=z.OldDenomination7, m.OldMinimumDenomination=z.OldMinimumDenomination,
m.OldDenominationMultiple=z.OldDenominationMultiple, m.Notes=z.zRdnomNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zrdnomid = (select zrdnomid from wca.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.rdnom as m
inner join wca.zrdnom as z on m.rdnomID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewDenomination1=z.NewDenomination1, m.NewDenomination2=z.NewDenomination2,
m.NewDenomination3=z.NewDenomination3, m.NewDenomination4=z.NewDenomination4, m.NewDenomination5=z.NewDenomination5,
m.NewDenomination6=z.NewDenomination6, m.NewDenomination7=z.NewDenomination7, m.NewMinimumDenomination=z.NewMinimumDenomination,
m.NewDenominationMultiple=z.NewDenominationMultiple, m.Notes=z.zRdnomNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zrdnomid = (select zrdnomid from wca.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.rdnom as m
inner join wca.zrdnom as z on m.rdnomID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zrdnomid = (select zrdnomid from wca.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.scchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, DateofChange, SecOldName, SecNewName, EventType, ScChgID, ScChgNotes, OldSectyCD, NewSectyCD, OldRegS144A, NewRegS144A, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, DateofChange, SecOldName, SecNewName, EventType, EfdtLinkID, zScChgNotes, OldSectyCD, NewSectyCD, OldRegS144A, NewRegS144A, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zscchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select scchgid from wca.scchg)
and zscchgid = (select zscchgid from wca.zscchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.scchg as m
inner join wca.zscchg as z on m.scchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.DateofChange=z.DateofChange, m.SecOldName=z.SecOldName, m.EventType=z.EventType, m.ScChgNotes=z.zScChgNotes, 
m.OldSectyCD=z.OldSectyCD, m.OldRegS144A=z.OldRegS144A, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zscchgid = (select zscchgid from wca.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.scchg as m
inner join wca.zscchg as z on m.scchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.DateofChange=z.DateofChange, m.SecNewName=z.SecNewName, m.EventType=z.EventType, m.ScChgNotes=z.zScChgNotes,
m.NewSectyCD=z.NewSectyCD, m.NewRegS144A=z.NewRegS144A, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zscchgid = (select zscchgid from wca.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.scchg as m
inner join wca.zscchg as z on m.scchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zscchgid = (select zscchgid from wca.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.shoch (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldSos, NewSos, ShochID, ShochNotes, EventType, RelEventID, OldOutstandingDate, NewOutstandingDate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldSos, NewSos, EfdtLinkID, zShochNotes, EventType, RelEventID, OldOutstandingDate, NewOutstandingDate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.zshoch as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select shochid from wca.shoch)
and zshochid = (select zshochid from wca.zshoch as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.shoch as m
inner join wca.zshoch as z on m.shochID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldSos=z.OldSos, m.ShochNotes=z.zShochNotes, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.OldOutstandingDate=z.OldOutstandingDate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zshochid = (select zshochid from wca.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.shoch as m
inner join wca.zshoch as z on m.shochID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewSos=z.NewSos, m.ShochNotes=z.zShochNotes, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.NewOutstandingDate=z.NewOutstandingDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.zshochid = (select zshochid from wca.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.shoch as m
inner join wca.zshoch as z on m.shochID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zshochid = (select zshochid from wca.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
INSERT INTO wca.trchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldTreasurySharesDate, NewTreasurySharesDate, OldTreasuryShares, NewTreasuryShares, EventType, RelEventID, Notes, TrchgID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldTreasurySharesDate, NewTreasurySharesDate, OldTreasuryShares, NewTreasuryShares, EventType, RelEventID, zTrchgNotes, EfdtLinkID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.ztrchg as main
where
main.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select trchgid from wca.trchg)
and ztrchgid = (select ztrchgid from wca.ztrchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.trchg as m
inner join wca.ztrchg as z on m.trchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.OldTreasurySharesDate=z.OldTreasurySharesDate, m.OldTreasuryShares=z.OldTreasuryShares,m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.Notes=z.zTrchgNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.ztrchgid = (select ztrchgid from wca.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid limit 1);
UPDATE wca.trchg as m
inner join wca.ztrchg as z on m.trchgID=z.EfdtLinkID
set m.Acttime=z.Acttime,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.NewTreasurySharesDate=z.NewTreasurySharesDate, m.NewTreasuryShares=z.NewTreasuryShares, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.Notes=z.zTrchgNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and z.actflag<>'D'
and z.ztrchgid = (select ztrchgid from wca.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc limit 1);
UPDATE wca.trchg as m
inner join wca.ztrchg as z on m.trchgID=z.EfdtLinkID
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
where
z.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and 0 = (select count(*) from wca.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.ztrchgid = (select ztrchgid from wca.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc limit 1);
