--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_NAICH
--fileheadertext=NAICH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('NAICH') as TableName,
wca.naich.Actflag,
wca.naich.AnnounceDate as Created,
wca.naich.Acttime as Changed,
wca.naich.IssID,
wca.naich.EffectiveDate,
wca.naich.OldNAICS,
wca.naich.NewNAICS,
wca.naich.NAICSChID
from wca.naich
inner join wca.scmst on wca.naich.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.naich.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
