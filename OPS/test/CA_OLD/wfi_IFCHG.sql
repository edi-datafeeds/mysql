--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('IFCHG') as TableName,
wca.ifchg.Actflag,
wca.ifchg.AnnounceDate as Created,
wca.ifchg.Acttime as Changed,
wca.ifchg.IfchgID,
wca.ifchg.SecID,
wca.scmst.ISIN,
wca.ifchg.NotificationDate,
wca.ifchg.OldIntPayFrqncy,
wca.ifchg.OldIntPayDate1,
wca.ifchg.OldIntPayDate2,
wca.ifchg.OldIntPayDate3,
wca.ifchg.OldIntPayDate4,
wca.ifchg.NewIntPayFrqncy,
wca.ifchg.NewIntPayDate1,
wca.ifchg.NewIntPayDate2,
wca.ifchg.NewIntPayDate3,
wca.ifchg.NewIntPayDate4,
wca.ifchg.Eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.ifchg.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
