--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CH144
--fileheadertext=CH144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CH144') as TableName,
wca.ch144.Actflag,
wca.ch144.AnnounceDate as Created,
wca.ch144.Acttime as Changed,
wca.ch144.LK144ID,
wca.ch144.EffectiveDate,
wca.ch144.SerialNumber,
wca.ch144.OldSecID144A,
wca.ch144.NewSecID144a,
wca.scmst.ISIN as ISIN144A,
wca.ch144.OldSecIDRegS,
wca.ch144.NewSecIDRegS,
s1.ISIN as ISINRegS,
wca.ch144.OldSecIDUnrestricted,
wca.ch144.NewSecIDUnrestricted,
wca.ch144.OldLinkOutstandingAmount,
wca.ch144.NewLinkOutstandingAmount,
wca.ch144.OldLinkOutstandingAmountDate,
wca.ch144.NewLinkOutstandingAmountDate,
wca.ch144.EventType,
wca.ch144.RelEventID,
wca.ch144.CH144ID
from wca.ch144
inner join wca.scmst on wca.ch144.OldSecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.ch144.OldSecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.ch144.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
