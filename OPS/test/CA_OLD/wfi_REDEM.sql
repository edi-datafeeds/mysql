--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select
'REDEM' as TableName,
wca.redem.Actflag,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.RedemID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.redem.RedemDate,
wca.redem.CurenCD as RedemCurrency,
wca.redem.RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
wca.redem.AmountRedeemed,
wca.redem.RedemPremium,
wca.redem.RedemPercent,
wca.redem.PoolFactor,
wca.redem.PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.redem.InDefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.ExpTime,
wca.redem.ExpTimeZone,
wca.redem.MatGraceDays,
wca.redem.MatGraceDayConvention,
wca.redem.TenderOfferor,
wca.redem.DutchAuction,
wca.redem.DutchMinPrice,
wca.redem.DutchMinAmount,
wca.redem.DutchMaxPrice,
wca.redem.DutchMaxAmount,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.redem.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
