--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SXTCH
--fileheadertext=SXTCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SXTCH') as TableName,
wca.sxtch.Actflag,
wca.sxtch.AnnounceDate as Created,
wca.sxtch.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.sxtch.ScxtcID,
wca.sxtch.EffectiveDate,
wca.sxtch.OldCurenCD,
wca.sxtch.NewCurenCD,
wca.sxtch.OldLocalCode,
wca.sxtch.NewLocalCode,
wca.sxtch.OldPriceTick,
wca.sxtch.NewPriceTick,
wca.sxtch.OldTickSize,
wca.sxtch.NewTickSize,
wca.sxtch.OldISIN,
wca.sxtch.NewISIN,
wca.sxtch.SxtChID
from wca.sxtch
inner join wca.scxtc on wca.sxtch.ScxtcID = wca.scxtc.ScxtcID
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.sxtch.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
