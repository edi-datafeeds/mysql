--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRCHG') as TableName,
wca.crchg.Actflag,
wca.crchg.AnnounceDate as Created,
wca.crchg.Acttime as Changed, 
wca.crchg.CrChgID,
wca.crchg.SecID,
wca.crchg.RatingAgency,
wca.crchg.NewRatingDate,
wca.crchg.OldRating,
wca.crchg.NewRating,
wca.crchg.OldDirection,
wca.crchg.OldWatchList,
wca.crchg.OldWatchListReason,
wca.crchg.EventType,
wca.crchg.OldRatingDate,
wca.crchg.NewDirection,
wca.crchg.NewWatchList,
wca.crchg.NewWatchListReason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.secid in (select client.pfsecid.code from client.pfsecid where accid = 990)
and wca.crchg.actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'