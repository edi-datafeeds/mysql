--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_EXCHG
--fileheadertext=EXCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('EXCHG') as TableName,
wca.exchg.Actflag,
wca.exchg.AnnounceDate as Created,
wca.exchg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.exchg.ExchgCD,
wca.exchg.ExchgName,
wca.exchg.CntryCD,
wca.exchg.WklyDayOff1,
wca.exchg.WklyDayOff2,
wca.exchg.MIC,
wca.exchg.ExchgID
from wca.exchg
inner join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.exchg.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
