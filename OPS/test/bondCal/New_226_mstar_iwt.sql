--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_yield
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\226\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct
client.pfisin.code as Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.calcout.Callput,
bondsigma.calcout.CpType,
bondsigma.calcout.CpoptID,
DATE_FORMAT(bondsigma.calcout.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(bondsigma.calcout.ToDate, '%Y-%m-%d') as ToDate,
bondsigma.calcout.CpCurrency,
bondsigma.calcout.CpPrice,
bondsigma.calcout.CpPriceAsPercent,


     
DATE_FORMAT(bondsigma.calcout.SettlementDate, '%Y-%m-%d') as SettlementDate,     
     
     
case when bondsigma.calcout.ExchgCD<>'' and bondsigma.calcout.ExchgCD is not null
     then bondsigma.calcout.ExchgCD
     else wca.scmst.primaryexchgcd
     end as ExchgCD,

case when bondsigma.calcout.LocalCode<>'' and bondsigma.calcout.LocalCode is not null
     then bondsigma.calcout.LocalCode
     else wca.scexh.LocalCode
      end as LocalCode,

#bondsigma.calcout.LocalCode,


case when bondsigma.calcout.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when bondsigma.calcout.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(bondsigma.calcout.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
bondsigma.calcout.ClosingPrice,
bondsigma.calcout.PriceCurrency,
bondsigma.calcout.AccruedInterest,
bondsigma.calcout.YieldtoMaturity,
bondsigma.calcout.YieldtoCall,
bondsigma.calcout.YieldtoPut,
bondsigma.calcout.ModifiedDuration,
bondsigma.calcout.EffectiveDuration,
bondsigma.calcout.MacaulayDuration,
bondsigma.calcout.Convexity,
wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD,
CASE WHEN client.pfisin.code = 'GB00BYP0Y667' THEN '55 Baker Street, London, United Kingdom, W1U 7EU' else '' end as Contact
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 226=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join bondsigma.calcout on wca.scmst.secid=bondsigma.calcout.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid

#left outer join bondsigma.calcout on bondsigma.refpricefull.secid=bondsigma.calcout.secid
 #                                   and bondsigma.refpricefull.exchgcd=bondsigma.calcout.exchgcd
   #                                 and bondsigma.refpricefull.pricecurrency=bondsigma.calcout.pricecurrency
left outer join wca.scexh on bondsigma.calcout.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D';