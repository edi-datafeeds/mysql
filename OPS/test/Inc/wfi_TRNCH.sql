--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('TRNCH') as TableName,
wca.trnch.Actflag,
wca.trnch.AnnounceDate as Created,
wca.trnch.Acttime as Changed,
wca.trnch.TrnchID,
wca.trnch.SecID,
wca.scmst.ISIN,
wca.trnch.TrnchNumber,
wca.trnch.TrancheDate,
wca.trnch.TrancheAmount,
wca.trnch.ExpiryDate,
wca.trnch.Notes as Notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.trnch.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))


