--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_BDC
--fileheadertext=EDI_BDC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('BDC') as TableName,
wca.bdc.Actflag,
wca.bdc.AnnounceDate as Created,
wca.bdc.Acttime as Changed,
wca.bdc.BdcID,
wca.bdc.SecID,
wca.scmst.ISIN,
wca.bdc.BDCAppliedTo,
wca.bdc.CntrID,
wca.bdc.Notes
from wca.bdc
inner join wca.bond on wca.bdc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.bdc.acttime  BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' )

