--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGNCY') as TableName,
agncy.Actflag,
agncy.AnnounceDate as Created,
agncy.Acttime as Changed,
agncy.AgncyID,
agncy.RegistrarName,
agncy.Add1,
agncy.Add2,
agncy.Add3,
agncy.Add4,
agncy.Add5,
agncy.Add6,
agncy.City,
agncy.CntryCD,
agncy.Website,
agncy.Contact1,
agncy.Tel1,
agncy.Fax1,
agncy.Email1,
agncy.Contact2,
agncy.Tel2,
agncy.Fax2,
agncy.Email2,
agncy.Depository,
agncy.State
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.agncy.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
