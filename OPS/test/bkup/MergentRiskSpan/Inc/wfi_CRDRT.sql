--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CRDRT') as TableName,
wca.crdrt.Actflag,
wca.crdrt.AnnounceDate as Created,
wca.crdrt.Acttime as Changed, 
wca.crdrt.SecID,
wca.crdrt.RatingAgency,
wca.crdrt.RatingDate,
wca.crdrt.Rating,
wca.crdrt.Direction,
wca.crdrt.WatchList,
wca.crdrt.WatchListReason
from wca.crdrt
inner join wca.bond on wca.crdrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.crdrt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

