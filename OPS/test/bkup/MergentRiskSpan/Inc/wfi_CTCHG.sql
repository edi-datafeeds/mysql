--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CTCHG') as TableName,
wca.ctchg.Actflag,
wca.ctchg.AnnounceDate as Created,
wca.ctchg.Acttime as Changed, 
wca.ctchg.CtChgID,
wca.ctchg.SecID,
wca.scmst.ISIN,
wca.ctchg.EffectiveDate,
wca.ctchg.OldResultantRatio,
wca.ctchg.NewResultantRatio,
wca.ctchg.OldSecurityRatio,
wca.ctchg.NewSecurityRatio,
wca.ctchg.OldCurrency,
wca.ctchg.NewCurrency,
wca.ctchg.OldCurPair,
wca.ctchg.NewCurPair,
wca.ctchg.OldConversionPrice,
wca.ctchg.NewConversionPrice,
wca.ctchg.ResSectyCD,
wca.ctchg.OldResSecID,
wca.ctchg.NewResSecID,
wca.ctchg.EventType,
wca.ctchg.RelEventID,
wca.ctchg.OldFromDate,
wca.ctchg.NewFromDate,
wca.ctchg.OldTodate,
wca.ctchg.NewToDate,
wca.ctchg.ConvtID,
wca.ctchg.OldFXRate,
wca.ctchg.NewFXRate,
wca.ctchg.OldPriceAsPercent,
wca.ctchg.NewPriceAsPercent,
wca.ctchg.Notes
from wca.ctchg
inner join wca.bond on wca.ctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.ctchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

