--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('SFUND') as TableName,
wca.sfund.Actflag,
wca.sfund.AnnounceDate as Created,
wca.sfund.Acttime as Changed,
wca.sfund.SfundID,
wca.sfund.SecID,
wca.scmst.ISIN,
wca.sfund.SfundDate,
wca.sfund.CurenCD as Sinking_Currency,
wca.sfund.Amount,
wca.sfund.AmountasPercent,
wca.sfund.SfundNotes as Notes
from wca.sfund
inner join wca.bond on wca.sfund.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.sfund.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

