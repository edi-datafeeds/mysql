--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=EDI_AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SECID,
wca.auct.AuctionDate,
wca.auct.AuctionAmount,
wca.auct.IssueDate,
'' as AllotmentPriceAsPercent,
wca.auct.AmountAccepted,
wca.auct.AuctionID
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.auct.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
