--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_INDEF
--fileheadertext=EDI_INDEF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
'INDEF' as TableName,
wca.indef.Actflag,
wca.indef.AnnounceDate as Created,
wca.indef.Acttime as Changed,
wca.indef.IndefID,
wca.indef.SecID,
wca.scmst.ISIN,
wca.indef.DefaultType,
wca.indef.DefaultDate,
wca.indef.Notes
from wca.indef
inner join wca.bond on wca.indef.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.indef.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

