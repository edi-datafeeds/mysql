--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CURRD') as TableName,
wca.currd.Actflag,
wca.currd.AnnounceDate as Created,
wca.currd.Acttime as Changed,
wca.currd.CurrdID,
wca.currd.SecID,
wca.scmst.ISIN,
wca.currd.EffectiveDate,
wca.currd.OldCurenCD,
wca.currd.NewCurenCD,
wca.currd.OldParValue,
wca.currd.NewParValue,
wca.currd.EventType,
wca.currd.CurRdNotes as Notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.currd.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)