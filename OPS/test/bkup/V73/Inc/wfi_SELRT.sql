--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Incremental\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SELRT
--fileheadertext=SELRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SELRT') as TableName,
wca.selrt.Actflag,
wca.selrt.AnnounceDate as Created,
wca.selrt.Acttime as Changed,
wca.selrt.SelrtID,
wca.selrt.SecID,
wca.scmst.ISIN,
wca.selrt.CntryCD,
wca.selrt.Restriction
from wca.selrt
inner join wca.bond on wca.selrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.selrt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'