--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.frnfx.Actflag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnfxID,
wca.frnfx.SecID,
wca.scmst.ISIN,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFRNType,
wca.frnfx.OldFRNIndexBenchmark,

-- wca.frnfx.OldMarkup As OldFrnMargin,
CASE WHEN locate('.',frnfx.OldMarkup) >0
                THEN substring(frnfx.OldMarkup,1,locate('.',frnfx.OldMarkup)+5)
                ELSE frnfx.OldMarkup
                END AS OldFrnMargin,

-- wca.frnfx.OldMinimumInterestRate,
CASE WHEN locate('.',frnfx.OldMinimumInterestRate) >0
                THEN substring(frnfx.OldMinimumInterestRate,1,locate('.',frnfx.OldMinimumInterestRate)+5)
                ELSE frnfx.OldMinimumInterestRate
                END AS OldMinimumInterestRate,

-- wca.frnfx.OldMaximumInterestRate,
CASE WHEN locate('.',frnfx.OldMaximumInterestRate) >0
                THEN substring(frnfx.OldMaximumInterestRate,1,locate('.',frnfx.OldMaximumInterestRate)+5)
                ELSE frnfx.OldMaximumInterestRate
                END AS OldMaximumInterestRate,

wca.frnfx.OldRounding,
wca.frnfx.NewFRNType,
wca.frnfx.NewFRNindexBenchmark,

-- wca.frnfx.NewMarkup As NewFrnMargin,
CASE WHEN locate('.',frnfx.NewMarkup) >0
                THEN substring(frnfx.NewMarkup,1,locate('.',frnfx.NewMarkup)+5)
                ELSE frnfx.NewMarkup
                END AS NewFrnMargin,

-- wca.frnfx.NewMinimumInterestRate,
CASE WHEN locate('.',frnfx.NewMinimumInterestRate) >0
                THEN substring(frnfx.NewMinimumInterestRate,1,locate('.',frnfx.NewMinimumInterestRate)+5)
                ELSE frnfx.NewMinimumInterestRate
                END AS NewMinimumInterestRate,

-- wca.frnfx.NewMaximumInterestRate,
CASE WHEN locate('.',frnfx.NewMaximumInterestRate) >0
                THEN substring(frnfx.NewMaximumInterestRate,1,locate('.',frnfx.NewMaximumInterestRate)+5)
                ELSE frnfx.NewMaximumInterestRate
                END AS NewMaximumInterestRate,

wca.frnfx.NewRounding,
wca.frnfx.Eventtype,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.frnfx.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'