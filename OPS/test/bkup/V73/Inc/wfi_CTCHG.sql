--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CTCHG') as TableName,
wca.zctchg.Actflag,
wca.zctchg.AnnounceDate as Created,
wca.zctchg.Acttime as Changed, 
wca.zctchg.zCtChgID as CtChgID,
wca.zctchg.SecID,
wca.scmst.ISIN,
wca.zctchg.EffectiveDate,

-- wca.zctchg.OldResultantRatio,
CASE WHEN locate('.',zctchg.OldResultantRatio) >0
                THEN substring(zctchg.OldResultantRatio,1,locate('.',zctchg.OldResultantRatio)+7)
                ELSE zctchg.OldResultantRatio
                END AS OldResultantRatio,

-- wca.zctchg.NewResultantRatio,
CASE WHEN locate('.',zctchg.NewResultantRatio) >0
                THEN substring(zctchg.NewResultantRatio,1,locate('.',zctchg.NewResultantRatio)+7)
                ELSE zctchg.NewResultantRatio
                END AS NewResultantRatio,

-- wca.zctchg.OldSecurityRatio,
CASE WHEN locate('.',zctchg.OldSecurityRatio) >0
                THEN substring(zctchg.OldSecurityRatio,1,locate('.',zctchg.OldSecurityRatio)+7)
                ELSE zctchg.OldSecurityRatio
                END AS OldSecurityRatio,

-- wca.zctchg.NewSecurityRatio,
CASE WHEN locate('.',zctchg.NewSecurityRatio) >0
                THEN substring(zctchg.NewSecurityRatio,1,locate('.',zctchg.NewSecurityRatio)+7)
                ELSE zctchg.NewSecurityRatio
                END AS NewSecurityRatio,

wca.zctchg.OldCurrency,
wca.zctchg.NewCurrency,
wca.zctchg.OldCurPair,
wca.zctchg.NewCurPair,

-- wca.zctchg.OldConversionPrice,
CASE WHEN locate('.',zctchg.OldConversionPrice) >0
                THEN substring(zctchg.OldConversionPrice,1,locate('.',zctchg.OldConversionPrice)+5)
                ELSE zctchg.OldConversionPrice
                END AS OldConversionPrice,          

-- wca.zctchg.NewConversionPrice,
CASE WHEN locate('.',zctchg.NewConversionPrice) >0
                THEN substring(zctchg.NewConversionPrice,1,locate('.',zctchg.NewConversionPrice)+5)
                ELSE zctchg.NewConversionPrice
                END AS NewConversionPrice,

wca.zctchg.ResSectyCD,
wca.zctchg.OldResSecID,
wca.zctchg.NewResSecID,
wca.zctchg.EventType,
wca.zctchg.RelEventID,
wca.zctchg.OldFromDate,
wca.zctchg.NewFromDate,
wca.zctchg.OldTodate,
wca.zctchg.NewToDate,
wca.zctchg.ConvtID,

-- wca.zctchg.OldFXRate,
CASE WHEN locate('.',zctchg.OldFXRate) >0
                THEN substring(zctchg.OldFXRate,1,locate('.',zctchg.OldFXRate)+5)
                ELSE zctchg.OldFXRate
                END AS OldFXRate,

-- wca.zctchg.NewFXRate,
CASE WHEN locate('.',zctchg.NewFXRate) >0
                THEN substring(zctchg.NewFXRate,1,locate('.',zctchg.NewFXRate)+5)
                ELSE zctchg.NewFXRate
                END AS NewFXRate,

-- wca.zctchg.OldPriceAsPercent,
CASE WHEN locate('.',zctchg.OldPriceAsPercent) >0
                THEN substring(zctchg.OldPriceAsPercent,1,locate('.',zctchg.OldPriceAsPercent)+9)
                ELSE zctchg.OldPriceAsPercent
                END AS OldPriceAsPercent,

-- wca.zctchg.NewPriceAsPercent,
CASE WHEN locate('.',zctchg.NewPriceAsPercent) >0
                THEN substring(zctchg.NewPriceAsPercent,1,locate('.',zctchg.NewPriceAsPercent)+9)
                ELSE zctchg.NewPriceAsPercent
                END AS NewPriceAsPercent,

wca.zctchg.OldMinPrice,
wca.zctchg.NewMinPrice,
wca.zctchg.OldMaxPrice,
wca.zctchg.NewMaxPrice,
wca.zctchg.zCtChgNotes as Notes
from wca.zctchg
inner join wca.bond on wca.zctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zctchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'