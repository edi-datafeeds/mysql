--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SCXTC
--fileheadertext=SCXTC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCXTC') as TableName,
wca.scxtc.Actflag,
wca.scxtc.AnnounceDate as Created,
wca.scxtc.Acttime as Changed,
wca.scxtc.ISIN,
wca.bond.SecID,
wca.scxtc.ScExhID,
wca.scxtc.CurenCD,
wca.scxtc.ListDate,
wca.scxtc.LocalCode,
wca.scxtc.PriceTick,

-- wca.scxtc.TickSize,
CASE WHEN locate('.',scxtc.TickSize) >0
                THEN substring(scxtc.TickSize,1,locate('.',scxtc.TickSize)+5)
                ELSE scxtc.TickSize
                END AS TickSize,

wca.scxtc.ScxtcID
from wca.scxtc
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scxtc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'