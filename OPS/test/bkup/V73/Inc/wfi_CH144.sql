--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CH144
--fileheadertext=CH144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CH144') as TableName,
wca.ch144.Actflag,
wca.ch144.AnnounceDate as Created,
wca.ch144.Acttime as Changed,
wca.ch144.LK144ID,
wca.ch144.EffectiveDate,
wca.ch144.SerialNumber,
wca.ch144.OldSecID144A,
wca.ch144.NewSecID144a,
wca.scmst.ISIN as ISIN144A,
wca.ch144.OldSecIDRegS,
wca.ch144.NewSecIDRegS,
s1.ISIN as ISINRegS,
wca.ch144.OldSecIDUnrestricted,
wca.ch144.NewSecIDUnrestricted,

-- wca.ch144.OldLinkOutstandingAmount,
CASE WHEN locate('.',ch144.OldLinkOutstandingAmount) >0
                THEN substring(ch144.OldLinkOutstandingAmount,1,locate('.',ch144.OldLinkOutstandingAmount)+5)
                ELSE ch144.OldLinkOutstandingAmount
                END AS OldLinkOutstandingAmount,

-- wca.ch144.NewLinkOutstandingAmount,
CASE WHEN locate('.',ch144.NewLinkOutstandingAmount) >0
                THEN substring(ch144.NewLinkOutstandingAmount,1,locate('.',ch144.NewLinkOutstandingAmount)+5)
                ELSE ch144.NewLinkOutstandingAmount
                END AS NewLinkOutstandingAmount,

wca.ch144.OldLinkOutstandingAmountDate,
wca.ch144.NewLinkOutstandingAmountDate,
wca.ch144.EventType,
wca.ch144.RelEventID,
wca.ch144.CH144ID
from wca.ch144
inner join wca.scmst on wca.ch144.OldSecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.ch144.OldSecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.ch144.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'