--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('TRNCH') as TableName,
wca.trnch.Actflag,
wca.trnch.AnnounceDate as Created,
wca.trnch.Acttime as Changed,
wca.trnch.TrnchID,
wca.trnch.SecID,
wca.scmst.ISIN,
wca.trnch.TrnchNumber,
wca.trnch.TrancheDate,

-- wca.trnch.TrancheAmount,
CASE WHEN locate('.',trnch.TrancheAmount) >0
                THEN substring(trnch.TrancheAmount,1,locate('.',trnch.TrancheAmount)+5)
                ELSE trnch.TrancheAmount
                END AS TrancheAmount,

wca.trnch.ExpiryDate,
wca.trnch.Notes as Notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.trnch.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'