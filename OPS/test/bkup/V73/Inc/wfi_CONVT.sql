--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CONVT') as TableName,
wca.convt.Actflag,
wca.convt.AnnounceDate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.convt.FromDate,
wca.convt.ToDate,

-- wca.convt.RatioNew,
CASE WHEN locate('.',convt.RatioNew) >0
                THEN substring(convt.RatioNew,1,locate('.',convt.RatioNew)+7)
                ELSE convt.RatioNew
                END AS RatioNew,

-- wca.convt.RatioOld,
CASE WHEN locate('.',convt.RatioOld) >0
                THEN substring(convt.RatioOld,1,locate('.',convt.RatioOld)+7)
                ELSE convt.RatioOld
                END AS RatioOld,

wca.convt.CurenCD,
wca.convt.CurPair,

-- wca.convt.Price,
CASE WHEN locate('.',convt.Price) >0
                THEN substring(convt.Price,1,locate('.',convt.Price)+5)
                ELSE convt.Price
                END AS Price,

wca.convt.MandOptFlag,
wca.convt.ResSecID,
wca.convt.SectyCD as ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,

-- wca.convt.FXrate,
CASE WHEN locate('.',convt.FXrate) >0
                THEN substring(convt.FXrate,1,locate('.',convt.FXrate)+5)
                ELSE convt.FXrate
                END AS FXrate,

wca.convt.PartFinalFlag,

-- wca.convt.PriceAsPercent,
CASE WHEN locate('.',convt.PriceAsPercent) >0
                THEN substring(convt.PriceAsPercent,1,locate('.',convt.PriceAsPercent)+9)
                ELSE convt.PriceAsPercent
                END AS PriceAsPercent,

wca.convt.MinPrice,
wca.convt.MaxPrice,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.convt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'