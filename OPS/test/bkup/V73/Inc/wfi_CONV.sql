--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CONV
--fileheadertext=CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'CONV' as TableName,
wca.conv.Actflag,
wca.conv.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) THEN wca.rd.Acttime ELSE wca.conv.Acttime END as Changed,
wca.conv.ConvID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.conv.FromDate,
wca.conv.ToDate,

-- wca.conv.RatioNew,
CASE WHEN locate('.',conv.RatioNew) >0
                THEN substring(conv.RatioNew,1,locate('.',conv.RatioNew)+7)
                ELSE conv.RatioNew
                END AS RatioNew,

-- wca.conv.RatioOld,
CASE WHEN locate('.',conv.RatioOld) >0
                THEN substring(conv.RatioOld,1,locate('.',conv.RatioOld)+7)
                ELSE conv.RatioOld
                END AS RatioOld,

wca.conv.CurenCD as ConvCurrency,
wca.conv.CurPair,

-- wca.conv.Price,
CASE WHEN locate('.',conv.Price) >0
                THEN substring(conv.Price,1,locate('.',conv.Price)+5)
                ELSE conv.Price
                END AS Price,

wca.conv.MandOptFlag,
wca.conv.ResSecID,
wca.conv.ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.conv.Fractions,

-- wca.conv.FXrate,
CASE WHEN locate('.',conv.FXrate) >0
                THEN substring(conv.FXrate,1,locate('.',conv.FXrate)+5)
                ELSE conv.FXrate
                END AS FXrate,

wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.RDID,

-- wca.conv.PriceAsPercent,
CASE WHEN locate('.',conv.PriceAsPercent) >0
                THEN substring(conv.PriceAsPercent,1,locate('.',conv.PriceAsPercent)+9)
                ELSE conv.PriceAsPercent
                END AS PriceAsPercent,

-- wca.conv.AmountConverted,
CASE WHEN locate('.',conv.AmountConverted) >0
                THEN substring(conv.AmountConverted,1,locate('.',conv.AmountConverted)+5)
                ELSE conv.AmountConverted
                END AS AmountConverted,

wca.conv.SettlementDate,
wca.conv.ExpTime,
wca.conv.ExpTimeZone,
wca.conv.ConvNotes as Notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.conv.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'