--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
'INTPY' as TableName,
CASE WHEN wca.int_my.Actflag = 'D' or wca.int_my.actflag='C' or wca.intpy.actflag is null THEN wca.int_my.Actflag ELSE wca.intpy.Actflag END as Actflag,
wca.int_my.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.int_my.Acttime) and (wca.rd.Acttime > wca.exdt.Acttime) THEN wca.rd.Acttime WHEN (wca.exdt.Acttime is not null) and (wca.exdt.Acttime > wca.int_my.Acttime) THEN wca.exdt.Acttime ELSE wca.int_my.Acttime END as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.intpy.BCurenCD as DebtCurrency,

-- wca.intpy.BParValue as NominalValue,
CASE WHEN locate('.',intpy.BParValue) >0
                THEN substring(intpy.BParValue,1,locate('.',intpy.BParValue)+5)
                ELSE intpy.BParValue
                END AS NominalValue,

wca.scexh.ExchgCD,
wca.int_my.RdID,
case when wca.intpy.optionid is null then '1' else wca.intpy.optionid end as OptionID,
wca.rd.Recdate,
wca.exdt.Exdate,
wca.exdt.Paydate,
wca.int_my.InterestFromDate,
wca.int_my.InterestToDate,
wca.int_my.Days,
wca.intpy.CurenCD as Interest_Currency,

-- wca.intpy.IntRate as Interest_Rate,
CASE WHEN locate('.',intpy.IntRate) >0
                THEN substring(intpy.IntRate,1,locate('.',intpy.IntRate)+9)
                ELSE intpy.IntRate
                END AS Interest_Rate,

-- wca.intpy.GrossInterest,
CASE WHEN locate('.',intpy.GrossInterest) >0
                THEN substring(intpy.GrossInterest,1,locate('.',intpy.GrossInterest)+9)
                ELSE intpy.GrossInterest
                END AS GrossInterest,

-- wca.intpy.NetInterest,
CASE WHEN locate('.',intpy.NetInterest) >0
                THEN substring(intpy.NetInterest,1,locate('.',intpy.NetInterest)+9)
                ELSE intpy.NetInterest
                END AS NetInterest,

-- wca.intpy.DomesticTaxRate,
CASE WHEN locate('.',intpy.DomesticTaxRate) >0
                THEN substring(intpy.DomesticTaxRate,1,locate('.',intpy.DomesticTaxRate)+5)
                ELSE intpy.DomesticTaxRate
                END AS DomesticTaxRate,

-- wca.intpy.NonResidentTaxRate,
CASE WHEN locate('.',intpy.NonResidentTaxRate) >0
                THEN substring(intpy.NonResidentTaxRate,1,locate('.',intpy.NonResidentTaxRate)+5)
                ELSE intpy.NonResidentTaxRate
                END AS NonResidentTaxRate,

wca.intpy.RescindInterest,

-- wca.intpy.AgencyFees,
CASE WHEN locate('.',intpy.AgencyFees) >0
                THEN substring(intpy.AgencyFees,1,locate('.',intpy.AgencyFees)+5)
                ELSE intpy.AgencyFees
                END AS AgencyFees,

wca.intpy.CouponNo,
-- null as CouponID,
wca.intpy.DefaultOpt,
wca.intpy.OptElectionDate,

-- wca.intpy.AnlCoupRate,
CASE WHEN locate('.',intpy.AnlCoupRate) >0
                THEN substring(intpy.AnlCoupRate,1,locate('.',intpy.AnlCoupRate)+9)
                ELSE intpy.AnlCoupRate
                END AS AnlCoupRate,

wca.int_my.InDefPay,
wca.int_my.NilInt,
wca.int_my.InterestGraceDays,
wca.int_my.InterestGraceDayConvention
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
(wca.intpy.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.int_my.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.rd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.exdt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))