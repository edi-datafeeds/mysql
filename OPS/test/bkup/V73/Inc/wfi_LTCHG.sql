--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LTCHG') as TableName,
wca.ltchg.Actflag,
wca.ltchg.AnnounceDate as Created,
wca.ltchg.Acttime as Changed,
wca.ltchg.LtChgID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.ltchg.ExchgCD,
wca.ltchg.EffectiveDate,
wca.ltchg.OldLot,

-- wca.ltchg.OldMinTrdQty,
CASE WHEN locate('.',ltchg.OldMinTrdQty) >0
                THEN substring(ltchg.OldMinTrdQty,1,locate('.',ltchg.OldMinTrdQty)+0)
                ELSE ltchg.OldMinTrdQty
                END AS OldMinTrdQty,

wca.ltchg.NewLot,

-- wca.ltchg.NewMinTrdgQty
CASE WHEN locate('.',ltchg.NewMinTrdgQty) >0
                THEN substring(ltchg.NewMinTrdgQty,1,locate('.',ltchg.NewMinTrdgQty)+0)
                ELSE ltchg.NewMinTrdgQty
                END AS NewMinTrdgQty

from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ltchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'