--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_BOND
--fileheadertext=BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BOND') as Tablename,
wca.bond.Actflag,
wca.bond.AnnounceDate as Created,
wca.bond.Acttime as Changed,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bond.BondType,
wca.bond.DebtMarket,
wca.bond.CurenCD as DebtCurrency,

-- case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue else wca.bond.parvalue end as NominalValue,

CASE WHEN locate('.',bond.largeparvalue) >0
                THEN substring(bond.largeparvalue,1,locate('.',bond.largeparvalue)+5)
     WHEN wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0           
				THEN wca.bond.largeparvalue
    
    
     WHEN locate('.',bond.parvalue) >0
                THEN substring(bond.parvalue,1,locate('.',bond.parvalue)+5)
     WHEN wca.bond.parvalue<>'' and wca.bond.parvalue<> 0           
				THEN wca.bond.parvalue           
                
                
    when locate('.',scmst.parvalue) >0
                THEN substring(scmst.parvalue,1,locate('.',scmst.parvalue)+5)            
                
    else             
              wca.scmst.parvalue end as NominalValue,




case when (wca.bond.Source like 'AU%' or wca.bond.Source like 'ASX%')
and wca.scmst.ISIN like 'AU%'
and (wca.bond.BondType = 'CD' OR wca.bond.BondType = 'CP')
and wca.bond.IssueDate is null then wca.bond.SourceDate 
else wca.bond.IssueDate end as IssueDate,
wca.bond.IssueCurrency,

-- wca.bond.IssuePrice,
CASE WHEN locate('.',bond.IssuePrice) >0
                THEN substring(bond.IssuePrice,1,locate('.',bond.IssuePrice)+5)
                ELSE bond.IssuePrice
                END AS IssuePrice,

-- wca.bond.IssueAmount,
CASE WHEN locate('.',bond.IssueAmount) >0
                THEN substring(bond.IssueAmount,1,locate('.',bond.IssueAmount)+5)
                ELSE bond.IssueAmount
                END AS IssueAmount,

wca.bond.IssueAmountDate,

-- wca.bond.OutstandingAmount,
CASE WHEN locate('.',bond.OutstandingAmount) >0
                THEN substring(bond.OutstandingAmount,1,locate('.',bond.OutstandingAmount)+5)
                ELSE bond.OutstandingAmount
                END AS OutstandingAmount,

wca.bond.OutstandingAmountDate,
wca.bond.InterestBasis,

-- wca.bond.InterestRate,
CASE WHEN locate('.',bond.InterestRate) >0
                THEN substring(bond.InterestRate,1,locate('.',bond.InterestRate)+5)
                ELSE bond.InterestRate
                END AS InterestRate,

wca.bond.InterestAccrualConvention,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.OddFirstCoupon,
wca.bond.FirstCouponDate,
wca.bond.OddLastCoupon,
wca.bond.LastCouponDate,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,

-- wca.bondx.DomesticTaxRate,
CASE WHEN locate('.',bondx.DomesticTaxRate) >0
                THEN substring(bondx.DomesticTaxRate,1,locate('.',bondx.DomesticTaxRate)+5)
                ELSE bondx.DomesticTaxRate
                END AS DomesticTaxRate,      

-- wca.bondx.NonResidentTaxRate,
CASE WHEN locate('.',bondx.NonResidentTaxRate) >0
                THEN substring(bondx.NonResidentTaxRate,1,locate('.',bondx.NonResidentTaxRate)+5)
                ELSE bondx.NonResidentTaxRate
                END AS NonResidentTaxRate,

wca.bond.FRNType,
wca.bond.FRNIndexBenchmark,

-- wca.bond.Markup as FrnMargin,
CASE WHEN locate('.',bond.Markup) >0
                THEN substring(bond.Markup,1,locate('.',bond.Markup)+5)
                ELSE bond.Markup
                END AS FrnMargin,

-- wca.bond.MinimumInterestRate as FrnMinInterestRate,
CASE WHEN locate('.',bond.MinimumInterestRate) >0
                THEN substring(bond.MinimumInterestRate,1,locate('.',bond.MinimumInterestRate)+5)
                ELSE bond.MinimumInterestRate
                END AS FrnMinInterestRate,

-- wca.bond.MaximumInterestRate as FrnMaxInterestRate,
CASE WHEN locate('.',bond.MaximumInterestRate) >0
                THEN substring(bond.MaximumInterestRate,1,locate('.',bond.MaximumInterestRate)+5)
                ELSE bond.MaximumInterestRate
                END AS FrnMaxInterestRate,

wca.bond.Rounding,
wca.bondx.Series,
wca.bondx.Class,
wca.bondx.OnTap,

-- wca.bondx.MaximumTapAmount,
CASE WHEN locate('.',bondx.MaximumTapAmount) >0
                THEN substring(bondx.MaximumTapAmount,1,locate('.',bondx.MaximumTapAmount)+5)
                ELSE bondx.MaximumTapAmount
                END AS MaximumTapAmount,

wca.bondx.TapExpiryDate,
wca.bond.Guaranteed,
wca.bond.SecuredBy,
wca.bond.SecurityCharge,
wca.bond.Subordinate,
wca.bond.SeniorJunior,
wca.bond.WarrantAttached,
wca.bond.MaturityStructure,
wca.bond.Perpetual,
wca.bond.MaturityDate,
wca.bond.MaturityExtendible,
wca.bond.Callable,
wca.bond.Puttable,

-- wca.bondx.Denomination1,
CASE WHEN locate('.',bondx.Denomination1) >0
                THEN substring(bondx.Denomination1,1,locate('.',bondx.Denomination1)+6)
                ELSE bondx.Denomination1
                END AS Denomination1,

-- wca.bondx.Denomination2,
CASE WHEN locate('.',bondx.Denomination2) >0
                THEN substring(bondx.Denomination2,1,locate('.',bondx.Denomination2)+6)
                ELSE bondx.Denomination2
                END AS Denomination2,

-- wca.bondx.Denomination3,
CASE WHEN locate('.',bondx.Denomination3) >0
                THEN substring(bondx.Denomination3,1,locate('.',bondx.Denomination3)+6)
                ELSE bondx.Denomination3
                END AS Denomination3,

-- wca.bondx.Denomination4,
CASE WHEN locate('.',bondx.Denomination4) >0
                THEN substring(bondx.Denomination4,1,locate('.',bondx.Denomination4)+6)
                ELSE bondx.Denomination4
                END AS Denomination4,

-- wca.bondx.Denomination5,
CASE WHEN locate('.',bondx.Denomination5) >0
                THEN substring(bondx.Denomination5,1,locate('.',bondx.Denomination5)+6)
                ELSE bondx.Denomination5
                END AS Denomination5,

-- wca.bondx.Denomination6,
CASE WHEN locate('.',bondx.Denomination6) >0
                THEN substring(bondx.Denomination6,1,locate('.',bondx.Denomination6)+6)
                ELSE bondx.Denomination6
                END AS Denomination6,

-- wca.bondx.Denomination7,
CASE WHEN locate('.',bondx.Denomination7) >0
                THEN substring(bondx.Denomination7,1,locate('.',bondx.Denomination7)+6)
                ELSE bondx.Denomination7
                END AS Denomination7,

-- wca.bondx.MinimumDenomination,
CASE WHEN locate('.',bondx.MinimumDenomination) >0
                THEN substring(bondx.MinimumDenomination,1,locate('.',bondx.MinimumDenomination)+6)
                ELSE bondx.MinimumDenomination
                END AS MinimumDenomination,

wca.bondx.DenominationMultiple,
wca.bond.Strip,
wca.bond.StripInterestNumber, 
wca.bond.Bondsrc,
wca.bond.MaturityBenchmark,
wca.bond.ConventionMethod,
wca.bond.FrnIntAdjFreq as FrnInterestAdjFreq,
wca.bond.IntBusDayConv as InterestBusDayConv,
wca.bond.InterestCurrency,
wca.bond.MatBusDayConv as MaturityBusDayConv,
wca.bond.MaturityCurrency, 
wca.bondx.TaxRules,
wca.bond.VarIntPayDate as VarInterestPaydate,

-- wca.bond.PriceAsPercent,
CASE WHEN locate('.',bond.PriceAsPercent) >0
                THEN substring(bond.PriceAsPercent,1,locate('.',bond.PriceAsPercent)+9)
                ELSE bond.PriceAsPercent
                END AS PriceAsPercent,

wca.bond.PayOutMode,
wca.bond.Cumulative,

-- case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
--      when rtrim(wca.bond.largeparvalue)='' then null
--      when rtrim(wca.bond.MatPriceAsPercent)='' then null
--      else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.MatPriceAsPercent as decimal(18,4))/100 
--      end as MatPrice,
case when wca.bond.matprice<>'' then substring(wca.bond.matprice,1,locate('.',wca.bond.matprice)+6)
     when rtrim(wca.bond.largeparvalue)='' then ''
     when rtrim(wca.bond.MatPriceAsPercent)='' then ''
     else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.MatPriceAsPercent as decimal(18,3))/100 
     end as MatPrice,


-- wca.bond.MatPriceAsPercent,
CASE WHEN locate('.',bond.MatPriceAsPercent) >0
                THEN substring(bond.MatPriceAsPercent,1,locate('.',bond.MatPriceAsPercent)+9)
                ELSE bond.MatPriceAsPercent
                END AS MatPriceAsPercent,

wca.bond.SinkingFund,
wca.bondx.GovCity,
wca.bondx.GovState,
wca.bondx.GovCntry,
wca.bondx.GovLawLkup as GovLaw,
wca.bondx.GoverningLaw as GovLawNotes,
wca.bond.Municipal,
wca.bond.PrivatePlacement,
wca.bond.Syndicated,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.Collateral,
wca.bond.CoverPool,
wca.bond.PikPay,

-- wca.bond.YieldAtIssue,
CASE WHEN locate('.',bond.YieldAtIssue) >0
                THEN substring(bond.YieldAtIssue,1,locate('.',bond.YieldAtIssue)+5)
                ELSE bond.YieldAtIssue
                END AS YieldAtIssue,

wca.bond.CoCoTrigger,
wca.bond.CoCoAct,
wca.bond.NonViability as CoCoNonViability,
wca.bondx.Taxability,
wca.bond.LatestAppliedINTPYAnlCpnRateDate,
wca.bond.LatestAppliedINTPYAnlCpnRate,
wca.bond.FirstAnnouncementDate,
wca.bond.PerformanceBenchmarkISIN,
wca.bondx.InterestGraceDays,
wca.bondx.InterestGraceDayConvention,
wca.bondx.MatGraceDays,
wca.bondx.MatGraceDayConvention,
wca.bond.ExpMatDate,
wca.bond.GreenBond,
wca.bond.IndicativeIssAmtDate,
wca.bond.IndicativeIssAmt,
wca.bond.CoCoCapRatioTriggerPercent,
wca.bond.ZCMarker,
wca.bond.Notes
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
where
wca.bond.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'
