--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CTCHG') as TableName,
wca.zctchg.Actflag,
wca.zctchg.AnnounceDate as Created,
wca.zctchg.Acttime as Changed, 
wca.zctchg.zCtChgID as CtChgID,
wca.zctchg.SecID,
wca.scmst.ISIN,
wca.zctchg.EffectiveDate,
wca.zctchg.OldResultantRatio,
wca.zctchg.NewResultantRatio,
wca.zctchg.OldSecurityRatio,
wca.zctchg.NewSecurityRatio,
wca.zctchg.OldCurrency,
wca.zctchg.NewCurrency,
wca.zctchg.OldCurPair,
wca.zctchg.NewCurPair,
wca.zctchg.OldConversionPrice,
wca.zctchg.NewConversionPrice,
wca.zctchg.ResSectyCD,
wca.zctchg.OldResSecID,
wca.zctchg.NewResSecID,
wca.zctchg.EventType,
wca.zctchg.RelEventID,
wca.zctchg.OldFromDate,
wca.zctchg.NewFromDate,
wca.zctchg.OldTodate,
wca.zctchg.NewToDate,
wca.zctchg.ConvtID,
wca.zctchg.OldFXRate,
wca.zctchg.NewFXRate,
wca.zctchg.OldPriceAsPercent,
wca.zctchg.NewPriceAsPercent,
wca.zctchg.OldMinPrice,
wca.zctchg.NewMinPrice,
wca.zctchg.OldMaxPrice,
wca.zctchg.NewMaxPrice,
wca.zctchg.zCtChgNotes as Notes
from wca.zctchg
inner join wca.bond on wca.zctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zctchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)