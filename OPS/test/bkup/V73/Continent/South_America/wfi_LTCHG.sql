--filepath=O:\Datafeed\Debt\V73\Continent\South_America\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\Continent\South_America\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('LTCHG') as TableName,
wca.ltchg.Actflag,
wca.ltchg.AnnounceDate as Created,
wca.ltchg.Acttime as Changed,
wca.ltchg.LtChgID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.ltchg.ExchgCD,
wca.ltchg.EffectiveDate,
wca.ltchg.OldLot,
wca.ltchg.OldMinTrdQty,
wca.ltchg.NewLot,
wca.ltchg.NewMinTrdgQty
from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
(wca.continent.country = 'Mexico' or wca.continent.country = 'Argentina' or wca.continent.country = 'Chile'
or wca.continent.country = 'Columbia' or wca.continent.country = 'Peru ' or wca.continent.country = 'Brazil')
and (wca.ltchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
