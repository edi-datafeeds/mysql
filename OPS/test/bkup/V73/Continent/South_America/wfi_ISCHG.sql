--filepath=O:\Datafeed\Debt\V73\Continent\South_America\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\Continent\South_America\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('ISCHG') as TableName,
wca.ischg.Actflag,
wca.ischg.AnnounceDate as Created,
wca.ischg.Acttime as Changed,
wca.ischg.IschgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.ischg.IssID,
wca.ischg.NameChangeDate,
wca.ischg.IssOldName,
wca.ischg.IssNewName,
wca.ischg.EventType,
wca.ischg.LegalName,
wca.ischg.MeetingDateFlag, 
wca.ischg.IsChgNotes as Notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
(wca.continent.country = 'Mexico' or wca.continent.country = 'Argentina' or wca.continent.country = 'Chile'
or wca.continent.country = 'Columbia' or wca.continent.country = 'Peru ' or wca.continent.country = 'Brazil')
and (wca.ischg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))

and (bond.issuedate<=wca.ischg.namechangedate
     or bond.issuedate is null or wca.ischg.namechangedate is null
    )
