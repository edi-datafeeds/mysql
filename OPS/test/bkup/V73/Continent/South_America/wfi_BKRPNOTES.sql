--filepath=O:\Datafeed\Debt\V73\Continent\South_America\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\Continent\South_America\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('BKRPNOTES') as TableName,
wca.bkrp.Actflag,
wca.bkrp.BkrpID,
wca.bkrp.IssId,
wca.bkrp.BkrpNotes as Notes
from wca.bkrp
where
wca.bkrp.issid in (select wca.scmst.issid from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
(wca.continent.country = 'Mexico' or wca.continent.country = 'Argentina' or wca.continent.country = 'Chile'
or wca.continent.country = 'Columbia' or wca.continent.country = 'Peru ' or wca.continent.country = 'Brazil'))
and wca.bkrp.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

