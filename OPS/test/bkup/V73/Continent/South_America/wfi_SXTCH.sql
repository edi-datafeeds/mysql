--filepath=O:\Datafeed\Debt\V73\Continent\South_America\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SXTCH
--fileheadertext=SXTCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\Continent\South_America\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('SXTCH') as TableName,
wca.sxtch.Actflag,
wca.sxtch.AnnounceDate as Created,
wca.sxtch.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.sxtch.ScxtcID,
wca.sxtch.EffectiveDate,
wca.sxtch.OldCurenCD,
wca.sxtch.NewCurenCD,
wca.sxtch.OldLocalCode,
wca.sxtch.NewLocalCode,
wca.sxtch.OldPriceTick,
wca.sxtch.NewPriceTick,
wca.sxtch.OldTickSize,
wca.sxtch.NewTickSize,
wca.sxtch.OldISIN,
wca.sxtch.NewISIN,
wca.sxtch.SxtChID
from wca.sxtch
inner join wca.scxtc on wca.sxtch.ScxtcID = wca.scxtc.ScxtcID
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
(wca.continent.country = 'Mexico' or wca.continent.country = 'Argentina' or wca.continent.country = 'Chile'
or wca.continent.country = 'Columbia' or wca.continent.country = 'Peru ' or wca.continent.country = 'Brazil')
and (wca.sxtch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
