--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSDD
--fileheadertext=ISSDD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('ISSDD') as TableName,
wca.issdd.Actflag,
wca.issdd.AnnounceDate as Created,
wca.issdd.Acttime as Changed,
wca.issdd.IssID,
wca.issdd.StartDate,
wca.issdd.EndDate,
wca.issdd.IssuerDebtDefaultID,
wca.issdd.Notes
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issdd.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)