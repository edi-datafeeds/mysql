--filepath=O:\Datafeed\Debt\V73\SRF\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_SRF_Global
--fileheadertext=WFI_SRF_Global_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\SRF\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
SELECT DISTINCT
wca.bond.Actflag,
wca.bond.AnnounceDate AS Created,
CASE
	WHEN (bond.acttime > scmst.acttime)
        AND (bond.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (bond.acttime > issur.acttime)
        AND (bond.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN bond.acttime
    WHEN
        (scmst.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (scmst.acttime > issur.acttime)
        AND (scmst.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scmst.acttime
    WHEN
        (scexh.acttime > issur.acttime)
        AND (scexh.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scexh.acttime
    WHEN
        (issur.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN issur.acttime
END AS Changed,
wca.scmst.SecID,
wca.scmst.IssID,
CASE
    WHEN wca.bbe.BbeID IS NULL THEN 0
    ELSE wca.bbe.BbeID
END AS BbeID,
CASE
    WHEN wca.scexh.ScexhID IS NULL THEN 0
    ELSE wca.scexh.ScexhID
END AS ScexhID,
    
    
CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.SedolID
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.SedolID
    ELSE '0'
END AS SedolID,

  
CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.actflag
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.actflag
    ELSE ''
END AS SedolActflag,   
wca.scmst.ISIN,
wca.scmst.USCode,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.Sedol
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.Sedol
    ELSE ''
END as Sedol,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.Defunct
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.Defunct
    ELSE ''
END as SedolDefunct,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.rcntrycd
    ELSE ''
END as SedolRegCntryCD,

wca.bbe.BbgexhId,
wca.bbe.Bbgexhtk,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.scexh.LocalCode,
wca.scmst.SecurityDesc,
wca.scmst.CurenCD,
wca.scmst.SectyCD,
wca.issur.IssuerName,
case when (wca.bond.Source like 'AU%' or wca.bond.Source like 'ASX%')
and wca.scmst.ISIN like 'AU%'
and (wca.bond.BondType = 'CD' OR wca.bond.BondType = 'CP')
and wca.bond.IssueDate is null then wca.bond.SourceDate 
else wca.bond.IssueDate end as IssueDate,
wca.bond.IssueAmount,
wca.issur.GICS,
wca.issur.CntryofIncorp,
wca.issur.isstype,
wca.bond.InterestRate,
wca.bond.MatPrice,
CASE
    WHEN wca.bond.largeparvalue <> '' THEN wca.bond.largeparvalue
    WHEN wca.bond.parvalue <> '' THEN wca.bond.parvalue
    ELSE wca.scmst.parvalue
END AS NominalValue,
wca.bond.OutstandingAmount,
wca.scmst.ParValue,
wca.bondx.DenominationMultiple,
wca.bond.MaturityDate,
wca.bond.MaturityStructure,
wca.issur.IndusID,
wca.scmst.CFI,
wca.issur.CIK,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.SeniorJunior,
wca.bond.Subordinate,
wca.bond.guaranteed,


CASE
    WHEN wca.scmst.statusflag = '' THEN 'A'
    ELSE wca.scmst.statusflag
END AS StatusFlag
FROM wca.bond
INNER JOIN wca.scmst ON wca.bond.secid = wca.scmst.secid
LEFT OUTER JOIN wca.bondx ON wca.bond.secid = wca.bondx.secid
INNER JOIN wca.issur ON wca.scmst.issid = wca.issur.issid



LEFT OUTER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
AND 'D' <> scexh.actflag
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd


LEFT OUTER JOIN wca.sedol as xsedol ON wca.scexh.secid = xsedol.secid
AND wca.exchg.cntrycd = xsedol.cntrycd
AND 'D' <> xsedol.actflag
  
LEFT OUTER JOIN wca.sedol as bsedol ON wca.bond.secid = bsedol.secid
AND 'zz' = bsedol.cntrycd  
AND 'D' <> bsedol.actflag


LEFT OUTER JOIN wca.bbe ON wca.scmst.secid = wca.bbe.secid
AND 'D' <> wca.bbe.actflag

WHERE

(wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog))