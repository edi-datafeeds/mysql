--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LSTAT') as TableName,
wca.lstat.Actflag,
wca.lstat.AnnounceDate as Created,
wca.lstat.Acttime as Changed,
wca.lstat.LstatID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.lstat.ExchgCD,
wca.lstat.NotificationDate,
wca.lstat.EffectiveDate,
wca.lstat.LStatStatus,
wca.lstat.EventType,
wca.lstat.Reason
from wca.lstat
inner join wca.bond on wca.lstat.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.lstat.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
