--filepath=O:\Upload\Acc\359\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Nectar
--fileheadertext=EDI_Nectar_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\359\Feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.bond.Actflag,
wca.bond.AnnounceDate AS Created,
CASE
	WHEN (bond.acttime > scmst.acttime)
        AND (bond.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (bond.acttime > issur.acttime)
        AND (bond.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN bond.acttime
    WHEN
        (scmst.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (scmst.acttime > issur.acttime)
        AND (scmst.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scmst.acttime
    WHEN
        (scexh.acttime > issur.acttime)
        AND (scexh.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scexh.acttime
    WHEN
        (issur.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN issur.acttime
END AS Changed,


wca.scmst.ISIN,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,

CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN DATE_FORMAT(wca.exdt.paydate, '%Y-%m-%d')
ELSE ''
END AS PayDate,


CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN wca.intpy.AnlCoupRate
ELSE ''
END AS AnlCoupRate,

DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,

wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,

wca.bond.IssuePrice,
wca.bond.IssueDate,
wca.bond.Perpetual,

case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,

case when scexh.ExchgCD<>'' and scexh.ExchgCD is not null
     then scexh.ExchgCD
     end as ExchgCD,


CASE WHEN wca.scexh.LocalCode REGEXP '^[[:digit:]]' or substring(wca.scexh.LocalCode,1,1) ='.' THEN wca.scexh.LocalCode
     WHEN wca.scexh.LocalCode<>'' and wca.scexh.LocalCode is not null      
     THEN
       SUBSTRING_INDEX(
        SUBSTRING_INDEX(
         SUBSTRING_INDEX(
          SUBSTRING_INDEX(
           SUBSTRING_INDEX(
            SUBSTRING_INDEX(
             SUBSTRING_INDEX(
              SUBSTRING_INDEX(
               SUBSTRING_INDEX(
                SUBSTRING_INDEX(
                 SUBSTRING_INDEX(
                  SUBSTRING_INDEX(
                   SUBSTRING_INDEX(
                    SUBSTRING_INDEX(
                     SUBSTRING_INDEX(wca.scexh.LocalCode, '.', 1),
                      '/', 1),
                     '_', 1),
                    '-', 1),
                   ' ', 1),
                  '1', 1),
                 '2', 1),
                '3', 1),
               '4', 1),
              '5', 1),
             '6', 1),
            '7', 1),
           '8', 1),
          '9', 1),
         '0', 1) 
               
         else 'N/A'
         end AS LocalCode,
 
wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD,
wca.issur.SIC,
wca.issur.NAICS,
wca.issur.GICS,
wca.bond.IssueAmount,
wca.bond.FirstCouponDate,
wca.bond.LastCouponDate,
wca.bond.IntCommencementDate,
wca.bond.IntBusDayConv as InterestBusDayConv
     

from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid

left outer join wca.rd on wca.bond.secid = wca.rd.secid
left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
left outer join wca.exdt ON wca.intpy.rdid = wca.exdt.RdID and 'INT'=wca.exdt.eventtype
left outer join wca.cpopt on wca.scmst.secid = wca.cpopt.secid

left outer join wca.scexh on wca.scmst.secid= wca.scexh.secid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
where
(isin in (select code from client.pfisin where accid=359 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=359 and actflag='U')
and (wca.bond.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.issur.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog) and wca.scexh.actflag = 'I')));