--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('IRCHG') as TableName,
wca.irchg.Actflag,
wca.irchg.AnnounceDate as Created,
wca.irchg.Acttime as Changed,
wca.irchg.IrchgID,
wca.irchg.SecID,
wca.scmst.ISIN,
wca.irchg.EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.Eventtype,
wca.irchg.Notes 
from wca.irchg
inner join wca.bond on wca.irchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.irchg.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
