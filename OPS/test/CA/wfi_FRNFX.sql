--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.frnfx.Actflag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnfxID,
wca.frnfx.SecID,
wca.scmst.ISIN,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFRNType,
wca.frnfx.OldFRNIndexBenchmark,
wca.frnfx.OldMarkup As OldFrnMargin,
wca.frnfx.OldMinimumInterestRate,
wca.frnfx.OldMaximumInterestRate,
wca.frnfx.OldRounding,
wca.frnfx.NewFRNType,
wca.frnfx.NewFRNindexBenchmark,
wca.frnfx.NewMarkup As NewFrnMargin,
wca.frnfx.NewMinimumInterestRate,
wca.frnfx.NewMaximumInterestRate,
wca.frnfx.NewRounding,
wca.frnfx.Eventtype,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.frnfx.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-02 11:51:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
