--filepath=O:\Datafeed\Debt\V73\Cntry\Inc\CA\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CONV
--fileheadertext=CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\Cntry\CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'CONV' as TableName,
wca.conv.Actflag,
wca.conv.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) THEN wca.rd.Acttime ELSE wca.conv.Acttime END as Changed,
wca.conv.ConvID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.conv.FromDate,
wca.conv.ToDate,
wca.conv.RatioNew,
wca.conv.RatioOld,
wca.conv.CurenCD as ConvCurrency,
wca.conv.CurPair,
wca.conv.Price,
wca.conv.MandOptFlag,
wca.conv.ResSecID,
wca.conv.ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.conv.Fractions,
wca.conv.FXrate,
wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.RDID,
wca.conv.PriceAsPercent,
wca.conv.AmountConverted,
wca.conv.SettlementDate,
wca.conv.ExpTime,
wca.conv.ExpTimeZone,
wca.conv.ConvNotes as Notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'Canada'
and (wca.conv.acttime   BETWEEN '2018-11-01 16:15:00' AND '2018-11-02 14:51:00'
or (wca.scexh.acttime   BETWEEN '2018-11-01 16:15:00' AND '2018-11-02 14:51:00' and wca.scexh.actflag = 'I'))
