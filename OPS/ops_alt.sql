USE wca;

ALTER TABLE `wca`.`tbl_opslog`
ADD COLUMN `acttime_new` DATETIME NULL AFTER `comment`,
ADD COLUMN `seq_new` INT(10) NULL AFTER `acttime_new`;

INSERT INTO tbl_opslog (`file_name`, `acttime`, `mode`, `insert_cnt`, `update_cnt`, `error_cnt`, `result`, `feeddate`, `seq`) VALUES ('20190114.z04', '2019-01-14 21:26:42', 'Daily', '0', '0', '0', 'Dummy Records', '2019-01-14 00:00:00', '4');
