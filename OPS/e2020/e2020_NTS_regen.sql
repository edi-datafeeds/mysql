DROP TABLE IF EXISTS `wca2`.`e2020_NTS`;
CREATE TABLE `wca2`.`e2020_NTS` (
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `EventActflag` char(1) DEFAULT NULL,
  `EventChangeDT` datetime DEFAULT NULL,
  `PflinkID` int(10) NOT NULL DEFAULT '0',
  `NotesType` varchar(20) NOT NULL,
  `NotesText` longtext,
  PRIMARY KEY (`EventCD`,`EventID`,`NotesType`),
  KEY `ix_PfLinkID` (`PfLinkID`),
  KEY `ix_EventChangeDT` (`EventChangeDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes' as NotesType,
ArrNotes as NotesText
FROM wca.v10s_arr as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ArrNotes<>'' and ArrNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
BBNotes as NotesText
FROM wca.v10s_bb as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
BBNotes<>'' and BBNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
vtab.issid as PflinkID,
'Notes'as NotesType,
BkrpNotes as NotesText
FROM wca.v10s_bkrp as vtab
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
BkrpNotes<>'' and BkrpNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
BonNotes as NotesText
FROM wca.v10s_bonid as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
BonNotes<>'' and BonNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
BrNotes as NotesText
FROM wca.v10s_brid as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
BrNotes<>'' and BrNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
CallNotes as NotesText
FROM wca.v10s_call as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
CallNotes<>'' and CallNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
CapRdNotes as NotesText
FROM wca.v10s_caprd as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
CapRdNotes<>'' and CapRdNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
ConsdNotes as NotesText
FROM wca.v10s_consd as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ConsdNotes<>'' and ConsdNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
ConvNotes as NotesText
FROM wca.v10s_conv as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ConvNotes<>'' and ConvNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
CtxNotes as NotesText
FROM wca.v10s_ctx as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
CtxNotes<>'' and CtxNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
CurRdNotes as NotesText
FROM wca.v10s_currd as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
CurRdNotes<>'' and CurRdNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DistNotes as NotesText
FROM wca.v10s_dist as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
DistNotes<>'' and DistNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DIVNotes
FROM wca.v10s_div as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd
       and vtab.eventid=etab.eventid 
       and vtab.rdid=etab.rdid 
WHERE
DIVNotes<>'' and DIVNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DmrgrNotes as NotesText
FROM wca.v10s_dmrgr as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
DmrgrNotes<>'' and DmrgrNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DvstNotes as NotesText
FROM wca.v10s_dvst as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
DvstNotes<>'' and DvstNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
EntNotes as NotesText
FROM wca.v10s_entid as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
EntNotes<>'' and EntNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
vtab.issid as PflinkID,
'Notes'as NotesType,
Notes as NotesText
FROM wca.v10s_clsac as vtab
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
Notes<>'' and Notes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
vtab.issid as PflinkID,
'Notes'as NotesType,
IschgNotes as NotesText
FROM wca.v10s_ischg as vtab
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
IschgNotes<>'' and IschgNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
vtab.issid as PflinkID,
'Notes'as NotesType,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
LiquidationTerms<>'' and LiquidationTerms<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'MrgrTerms'as NotesType,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
MrgrTerms<>'' and MrgrTerms<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Companies'as NotesType,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
Companies<>'' and Companies<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'ApprovalStatus'as NotesType,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ApprovalStatus<>'' and ApprovalStatus<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
PONotes
FROM wca.v10s_po as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
PONotes<>'' and PONotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
Notes
FROM wca.v10s_prchg as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
Notes<>'' and Notes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
PrfNotes
FROM wca.v10s_prfid as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
PrfNotes<>'' and PrfNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
PvRdNotes as NotesText
FROM wca.v10s_pvrd as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
PvRdNotes<>'' and PvRdNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
RcapNotes as NotesText
FROM wca.v10s_rcap as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
RcapNotes<>'' and RcapNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
RedemNotes as NotesText
FROM wca.v10s_redem as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
RedemNotes<>'' and RedemNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
RtsNotes
FROM wca.v10s_rts as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
RtsNotes<>'' and RtsNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ScChgNotes<>'' and ScChgNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
ScSwpNotes
FROM wca.v10s_scswp as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ScSwpNotes<>'' and ScSwpNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
SDNotes
FROM wca.v10s_sd as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
SDNotes<>'' and SDNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
SecRcNotes as NotesText
FROM wca.v10s_secrc as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
SecRcNotes<>'' and SecRcNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
TkovrNotes<>'' and TkovrNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DPRCPNotes as NotesText
FROM wca.v10s_dprcp as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
DPRCPNotes<>'' and DPRCPNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
DrchgNotes as NotesText
FROM wca.v10s_drchg as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
drchgNotes<>'' and drchgNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
ShochNotes as NotesText
FROM wca.v10s_shoch as vtab
inner join wca.scmst as PfLink on vtab.secid = PfLink.secid
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
ShochNotes<>'' and ShochNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
vtab.EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
vtab.issid as PflinkID,
'Notes'as NotesType,
AnnNotes as NotesText
FROM wca.v10s_ann as vtab
inner join wca2.e2020_EVT as etab on vtab.eventcd=etab.eventcd and vtab.eventid=etab.eventid 
WHERE
AnnNotes<>'' and AnnNotes<>'No further information';
insert ignore into wca2.e2020_NTS
SELECT
'DIV' as EventCD,
vtab.EventID,
vtab.Actflag as EventActflag,
vtab.Acttime as EventChangeDT,
PfLink.SecID as PflinkID,
'Notes'as NotesType,
INTNotes as NotesText
FROM wca.v10s_intpy as vtab
inner join wca.rd as PfLink on vtab.rdid=PfLink.rdid
inner join wca2.e2020_EVT as etab on 'DIV'=etab.eventcd
       and vtab.eventid=etab.eventid 
       and vtab.rdid=etab.rdid 
WHERE
INTNotes<>'' and INTNotes<>'No further information';
