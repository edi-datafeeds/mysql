select bbg.curencd, bbg.curensrcflag, currency from wca2.e2020_bbg as bbg
left outer join prices.lasttrade as pri on bbg.secid=pri.secid and bbg.exchgcd=pri.exchgcd 
      and substring(concat('0000',bbg.bbgexhtk),length(bbg.bbgexhtk)-3,5)=pri.localcode
where
bbg.cntrycd='HK' and bbg.liststatus='L'
and bbg.localcode=substring(concat('0000',bbg.bbgexhtk),length(bbg.bbgexhtk)-3,5)
and currency is not null
and currency=bbg.curencd
and pricedate>'2020/06/01';

select bbg.curencd, bbg.curensrcflag, currency,
substring(bbg.bbgexhtk,1,instr(bbg.bbgexhtk,' ')-1)
from wca2.e2020_bbg as bbg
left outer join prices.lasttrade as pri on bbg.secid=pri.secid and bbg.exchgcd=pri.exchgcd 
      and substring(bbg.bbgexhtk,1,instr(bbg.bbgexhtk,' ')-1)=pri.localcode
where
bbg.exchgcd='GBLSE' and bbg.liststatus='L'
and pricedate>'2020/06/01'
and bbg.curencd<>pri.currency
and pri.currency<>'GBX';

select bbg.exchgcd, bbg.curencd, bbg.curensrcflag, currency, bbg.localcode,
substring(bbg.bbgexhtk,1,instr(bbg.bbgexhtk,' ')-1), pricedate
from wca2.e2020_bbg as bbg
left outer join prices.lasttrade as pri on bbg.secid=pri.secid and bbg.exchgcd=pri.exchgcd 
where
bbg.exchgcd='RUMICX' and bbg.liststatus='L'
and pricedate>'2010/06/01'
-- and bbg.localcode<>pri.localcode;
and bbg.curencd<>pri.currency;
