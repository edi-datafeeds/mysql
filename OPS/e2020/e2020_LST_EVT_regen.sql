-- # Combine latest List and Event extracts

drop table if exists `wca2`.`e2020_LST_EVT`;
use wca2;
create table wca2.e2020_LST_EVT(
select distinct * from wca2.e2020_LST as lst
left outer join wca2.e2020_EVT as evt on lst.EvtLinkID=evt.LstLinkID);
alter table `wca2`.`e2020_LST_EVT`
add primary key (`LstRecordID`,`EvtRecordID`);

-- # Remove unwanted fields

ALTER TABLE `wca2`.`e2020_LST_EVT` DROP COLUMN `EvtLinkID`;
ALTER TABLE `wca2`.`e2020_LST_EVT` DROP COLUMN `ListActflag`;
ALTER TABLE `wca2`.`e2020_LST_EVT` DROP COLUMN `ListChangeDT`;
ALTER TABLE `wca2`.`e2020_LST_EVT` DROP COLUMN `ListCreateDT`;

-- # Update Resultant ISIN and UScode, where Outturn=Parent secid

update wca2.e2020_LST_EVT as etab
set etab.outisin=etab.isin, etab.outturntype='P', etab.outuscode=etab.uscode
where
etab.OutSecid is not null
and etab.OutSecID=etab.SecID;

update wca2.e2020_LST_EVT as etab
set etab.outlocalcode=etab.localcode, etab.outturntype='P'
where
etab.OutSecid is not null
and etab.OutSecID=etab.SecID;

update wca2.e2020_LST_EVT as etab
set etab.outsedol=etab.sedol, etab.outturntype='P'
where
etab.OutSecid is not null
and etab.OutSecID=etab.SecID;

update wca2.e2020_LST_EVT as etab
set etab.outfigi=etab.figi, etab.outfigitk=etab.figiticker, etab.outturntype='P'
where
etab.OutSecid is not null
and etab.OutSecID=etab.SecID;

update wca2.e2020_LST_EVT as etab
set etab.outbbgcompid=etab.bbgcompid, etab.outbbgcomptk=etab.bbgcomptk, etab.outturntype='P'
where
etab.OutSecid is not null
and etab.OutSecID=etab.SecID;

-- # Update Sucessor, Old and code status values

update wca2.e2020_LST_EVT as etab
left outer join wca.icc as ctab on etab.eventid=ctab.releventid 
           and etab.eventcd=ctab.eventtype
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.isin=ctab.oldisin, etab.outisin=ctab.newisin, etab.isinflag='O', etab.outturntype='N',
        etab.uscode=ctab.olduscode, etab.outuscode=ctab.newuscode, etab.uscodeflag='O'
where
isinflag<>'S'
and (ctab.oldisin<>'' or ctab.olduscode<>'')
and ctab.iccid = 
(select iccid from wca.icc as subicc
where
subicc.releventid=etab.eventid
and subicc.eventtype=etab.eventcd
and subicc.actflag<>'c'
and subicc.actflag<>'d'
order by subicc.iccid desc limit 1);

update wca2.e2020_LST_EVT as etab
left outer join wca.lcc as ctab on etab.eventid=ctab.releventid 
           and etab.eventcd=ctab.eventtype
           and etab.exchgcd=ctab.exchgcd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.localcode=ctab.oldlocalcode, etab.outlocalcode=ctab.newlocalcode, etab.localcodeflag='O', etab.outturntype='N'
where
localcodeflag<>'S'
and ctab.oldlocalcode<>''
and ctab.lccid = 
(select lccid from wca.lcc as sublcc
where
sublcc.releventid=etab.eventid
and sublcc.eventtype=etab.eventcd
and sublcc.exchgcd=etab.exchgcd
and sublcc.actflag<>'c'
and sublcc.actflag<>'d'
order by sublcc.lccid desc limit 1);

update wca2.e2020_LST_EVT as etab
left outer join wca.sdchg as ctab on etab.eventid=ctab.releventid 
           and etab.listcntrycd=ctab.cntrycd
           and etab.eventcd=ctab.eventtype
           and etab.registercntrycd=ctab.rcntrycd
           and etab.tradingcurencd=ctab.oldcurencd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.sedol=ctab.oldsedol, etab.outsedol=ctab.newsedol, etab.sedolflag='O', etab.outturntype='N'
where
ctab.oldsedol<>''
and ctab.sdchgid = 
(select sdchgid from wca.sdchg as subsdchg
where
subsdchg.releventid=etab.eventid
and subsdchg.cntrycd=etab.listcntrycd
and subsdchg.eventtype=etab.eventcd
and subsdchg.rcntrycd=etab.registercntrycd
and subsdchg.oldcurencd=etab.tradingcurencd
and subsdchg.actflag<>'c'
and subsdchg.actflag<>'d'
order by subsdchg.sdchgid desc limit 1);

-- # Update Resultant Coding, where Outturn<>Parent secid

update wca2.e2020_LST_EVT as etab
left outer join wca.scmst as ctab on etab.outsecid=ctab.secid 
set etab.outisin=ctab.isin, etab.outturntype='R', etab.outuscode=ctab.uscode
where
isinflag='M'
and etab.OutSecid is not null
and etab.OutSecID<>etab.SecID;

update wca2.e2020_LST_EVT as etab
left outer join wca.scexh as ctab on etab.outsecid=ctab.secid 
           and etab.exchgcd=ctab.exchgcd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.outlocalcode=ctab.localcode, etab.outturntype='R'
where
(localcodeflag='M' or localcodeflag='C')
and etab.OutSecid is not null
and etab.OutSecID<>etab.SecID;

update wca2.e2020_LST_EVT as etab
left outer join wca_other.sedol as ctab on etab.outsecid=ctab.secid 
           and etab.listcntrycd=ctab.cntrycd
           and etab.tradingcurencd=ctab.curencd
           and etab.registercntrycd=ctab.rcntrycd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.outsedol=ctab.sedol, etab.outturntype='R'
where
etab.OutSecid is not null
and etab.OutSecID<>etab.SecID;

update wca2.e2020_LST_EVT as etab
left outer join wca.bbe as ctab on etab.outsecid=ctab.secid 
           and etab.exchgcd=ctab.exchgcd
           and etab.tradingcurencd=ctab.curencd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.outfigi=ctab.bbgexhid, etab.outfigitk=ctab.bbgexhtk, etab.outturntype='R'
where
etab.OutSecid is not null
and etab.OutSecID<>etab.SecID;

update wca2.e2020_LST_EVT as etab
left outer join wca.bbc as ctab on etab.outsecid=ctab.secid 
           and etab.listcntrycd=ctab.cntrycd
           and etab.tradingcurencd=ctab.curencd
           and ctab.actflag<>'C' and ctab.actflag<>'D'
set etab.outbbgcompid=ctab.bbgcompid, etab.outbbgcomptk=ctab.bbgcomptk, etab.outturntype='R'
where
etab.OutSecid is not null
and etab.OutSecID<>etab.SecID;
