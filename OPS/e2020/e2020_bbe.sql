use wca2;
drop table if exists e2020_bbg;
CREATE TABLE `e2020bbe` (
  `Acttime` datetime DEFAULT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `ExchgcdCD` char(6) NOT NULL DEFAULT '',
  `CurenCD` char(3) DEFAULT '',
  `BBGCompID` char(12) NOT NULL DEFAULT '',
  `BBGCompTk` varchar(40) DEFAULT '',
  `BBGExhID` char(12) NOT NULL DEFAULT '',
  `BBGExhTk` varchar(40) DEFAULT '',
  `CurenSrcFlag` char(1) DEFAULT '',
  `BbcID` int(10) NOT NULL DEFAULT '0',
  `BbeID` int(10) NOT NULL DEFAULT '0',
  `AnnounceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`BbcID`,`BbeID`),
  UNIQUE KEY `IX_Unique` (`SecID`,`CntryCD`,`ExchgCD`,`CurenCD`),
  KEY `ix_acttime_BBG` (`Acttime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
insert ignore into wca2.e2020bbg
select
case when bbe.acttime > @fromdate then bbe.acttime
     when bbc.actdate > @fromdate then bbc.actdate
     when sce.acttime > @fromdate then sce.acttime
     else bbc.acttime
     end as acttime,
bbc.actflag,
bbc.SecID,
bbc.CntryCD,
bbc.ExchgCD,
bbc.CurenCD,

sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then sed.curencd
     else lse.unitofqcurrcode
     end as curencd,
case when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then 'S'
     else 'L'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca.bbc as bbc


inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
inner join wca.scexh as sce on scm.secid=sce.secid and sed.cntrycd=substring(sce.exchgcd,1,2)
inner join wca.scxtc as scx on sce.scexhid=scx.scexhid
where
sed.actflag<>'D'
and scx.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and (ifnull(lse.unitofqcurrcode,'')=scx.curencd or ifnull(sed.curencd,'')=scx.curencd)
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
scx.curencd,
'X' as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
inner join wca.scexh as sce on scm.secid=sce.secid and sed.cntrycd=substring(sce.exchgcd,1,2)
inner join wca.scxtc as scx on sce.scexhid=scx.scexhid
where
sed.actflag<>'D'
and scx.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and ifnull(lse.unitofqcurrcode,'')=''
and ifnull(sed.curencd,'')=''
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when ifnull(sed.curencd,'')<>''
     then sed.curencd
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when ifnull(sed.curencd,'')<>''
     then 'S'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and lse.statusflag='T'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when ifnull(sed.curencd,'')<>''
     then sed.curencd
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when ifnull(sed.curencd,'')<>''
     then 'S'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and lse.statusflag='C'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when ifnull(sed.curencd,'')<>''
     then sed.curencd
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')<>'' and sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when ifnull(sed.curencd,'')<>''
     then 'S'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and lse.statusflag='D'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
