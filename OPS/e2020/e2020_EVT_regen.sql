DROP TABLE IF EXISTS `wca2`.`e2020_EVT`;
CREATE TABLE `wca2`.`e2020_EVT` (
  `LstLinkID` int(10) unsigned NOT NULL DEFAULT '0',
  `EvtRecordID` bigint NOT NULL DEFAULT '0',
  `EventActflag` char(1) DEFAULT NULL,
  `EventChangeDT` datetime DEFAULT NULL,
  `EventCreateDT` datetime DEFAULT NULL,
  `EventCD` varchar(10) NOT NULL,
  `EventID` int(10) NOT NULL DEFAULT '0',
  `EventSubType` varchar(10) NOT NULL,
  `RelatedEventCD` varchar(10) NOT NULL,
  `RelatedEventID` int(10) DEFAULT NULL,
  `MandVoluFlag` char(1) DEFAULT NULL,
  `RdID` int(10) DEFAULT NULL,
  `Priority` int(10) DEFAULT NULL,
  `PrimaryDateName` varchar(30) NOT NULL DEFAULT '',
  `PrimaryDT` datetime DEFAULT NULL,
  `EffectiveDT` datetime DEFAULT NULL,
  `ExDT` datetime DEFAULT NULL,
  `RecordDT` datetime DEFAULT NULL,
  `PayDT` datetime DEFAULT NULL,
  `DuebillsRedemDT` datetime DEFAULT NULL,
  `FromDT` datetime DEFAULT NULL,
  `ToDT` datetime DEFAULT NULL,
  `RegistrationDT` datetime DEFAULT NULL,
  `StartDT` datetime DEFAULT NULL,
  `EndDT` datetime DEFAULT NULL,
  `OpenDT` datetime DEFAULT NULL,
  `CloseDT` datetime DEFAULT NULL,
  `StartSubscriptionDT` datetime DEFAULT NULL,
  `EndSubscriptionDT` datetime DEFAULT NULL,
  `OptElectionDT` datetime DEFAULT NULL,
  `WithdrawalFromDate` datetime DEFAULT NULL,
  `WithdrawalToDate` datetime DEFAULT NULL,
  `NotificationDT` datetime DEFAULT NULL,
  `FinancialYearEndDT` datetime DEFAULT NULL,
  `ExpCompletionDT` datetime DEFAULT NULL,
  `Date1Name` varchar(30) NOT NULL DEFAULT '',
  `Date1` datetime DEFAULT NULL,
  `Date2Name` varchar(30) NOT NULL DEFAULT '',
  `Date2` datetime DEFAULT NULL,
  `Date3Name` varchar(30) NOT NULL DEFAULT '',
  `Date3` datetime DEFAULT NULL,
  `Date4Name` varchar(30) NOT NULL DEFAULT '',
  `Date4` datetime DEFAULT NULL,
  `Date5Name` varchar(30) NOT NULL DEFAULT '',
  `Date5` datetime DEFAULT NULL,
  `Date6Name` varchar(30) NOT NULL DEFAULT '',
  `Date6` datetime DEFAULT NULL,
  `Date7Name` varchar(30) NOT NULL DEFAULT '',
  `Date7` datetime DEFAULT NULL,
  `Date8Name` varchar(30) NOT NULL DEFAULT '',
  `Date8` datetime DEFAULT NULL,
  `Paytype` char(1) NOT NULL DEFAULT '',
  `OptionID` int(10) unsigned DEFAULT NULL,
  `SerialID` int(10) unsigned DEFAULT NULL,
  `DefaultOpt` varchar(1) NOT NULL DEFAULT '',
  `RateCurenCD` char(3) NOT NULL DEFAULT '',
  `Rate1Name` varchar(20) NOT NULL DEFAULT '',
  `Rate1` varchar(20) NOT NULL DEFAULT '',
  `Rate2Name` varchar(20) NOT NULL DEFAULT '',
  `Rate2` varchar(20) NOT NULL DEFAULT '',
  `RatioOld` varchar(20) NOT NULL DEFAULT '',
  `RatioNew` varchar(20) NOT NULL DEFAULT '',
  `Fractions` char(1) NOT NULL DEFAULT '',
  `ParvalueOld` varchar(20) NOT NULL DEFAULT '',
  `ParvalueNew` varchar(20) NOT NULL DEFAULT '',
  `OutturnType` char(1) NOT NULL DEFAULT '',
  `OutSectyCD` char(3) NOT NULL DEFAULT '',
  `OutSecID` int(10) unsigned DEFAULT NULL,
  `OutIsin` char(12) NOT NULL DEFAULT '',
  `OutUsCode` char(9) NOT NULL DEFAULT '',
  `OutLocalcode` varchar(50) NOT NULL DEFAULT '',
  `OutSedol` char(7) NOT NULL DEFAULT '',
  `OutBbgCompID` char(12) NOT NULL DEFAULT '',
  `OutBbgCompTk` varchar(40) NOT NULL DEFAULT '',
  `OutFigi` char(12) NOT NULL DEFAULT '',
  `OutFigiTk` varchar(40) NOT NULL DEFAULT '',
  `MinOfrQty` varchar(20) NOT NULL DEFAULT '',
  `MaxOfrQty` varchar(20) NOT NULL DEFAULT '',
  `MinQlyQty` varchar(20) NOT NULL DEFAULT '',
  `MaxQlyQty` varchar(20) NOT NULL DEFAULT '',
  `MinAcpQty` varchar(20) NOT NULL DEFAULT '',
  `MaxAcpQty` varchar(20) NOT NULL DEFAULT '',
  `TndrStrkPrice` varchar(20) NOT NULL DEFAULT '',
  `TndrPriceStep` varchar(20) NOT NULL DEFAULT '',
  `WithdrawalRights` char(1) NOT NULL DEFAULT '',
  `OedExpTime` varchar(5) NOT NULL DEFAULT '',
  `OedExpTimeZone` varchar(10) NOT NULL DEFAULT '',
  `WrtExpTime` varchar(5) NOT NULL DEFAULT '',
  `WrtExpTimeZone` varchar(10) NOT NULL DEFAULT '',
  `ExpTime` varchar(5) NOT NULL DEFAULT '',
  `ExpTimeZone` varchar(10) NOT NULL DEFAULT '',
  `Field01Name` varchar(20) NOT NULL DEFAULT '',
  `Field01` varchar(255) DEFAULT NULL,
  `Field02Name` varchar(20) NOT NULL DEFAULT '',
  `Field02` varchar(255) DEFAULT NULL,
  `Field03Name` varchar(20) NOT NULL DEFAULT '',
  `Field03` varchar(255) DEFAULT NULL,
  `Field04Name` varchar(20) NOT NULL DEFAULT '',
  `Field04` varchar(255) DEFAULT NULL,
  `Field05Name` varchar(20) NOT NULL DEFAULT '',
  `Field05` varchar(255) DEFAULT NULL,
  `Field06Name` varchar(20) NOT NULL DEFAULT '',
  `Field06` varchar(255) DEFAULT NULL,
  `Field07Name` varchar(20) NOT NULL DEFAULT '',
  `Field07` varchar(255) DEFAULT NULL,
  `Field08Name` varchar(20) NOT NULL DEFAULT '',
  `Field08` varchar(255) DEFAULT NULL,
  `Field09Name` varchar(20) NOT NULL DEFAULT '',
  `Field09` varchar(255) DEFAULT NULL,
  `Field10Name` varchar(20) NOT NULL DEFAULT '',
  `Field10` varchar(255) DEFAULT NULL,
  `Field11Name` varchar(20) NOT NULL DEFAULT '',
  `Field11` varchar(255) DEFAULT NULL,
  `Field12Name` varchar(20) NOT NULL DEFAULT '',
  `Field12` varchar(255) DEFAULT NULL,
  `Field13Name` varchar(20) NOT NULL DEFAULT '',
  `Field13` varchar(255) DEFAULT NULL,
  `Field14Name` varchar(20) NOT NULL DEFAULT '',
  `Field14` varchar(255) DEFAULT NULL,
  `Field15Name` varchar(20) NOT NULL DEFAULT '',
  `Field15` varchar(255) DEFAULT NULL,
  `Field16Name` varchar(20) NOT NULL DEFAULT '',
  `Field16` varchar(255) DEFAULT NULL,
  `Field17Name` varchar(20) NOT NULL DEFAULT '',
  `Field17` varchar(255) DEFAULT NULL,
  `Field18Name` varchar(20) NOT NULL DEFAULT '',
  `Field18` varchar(255) DEFAULT NULL,
  `Field19Name` varchar(20) NOT NULL DEFAULT '',
  `Field19` varchar(255) DEFAULT NULL,
  `Field20Name` varchar(20) NOT NULL DEFAULT '',
  `Field20` varchar(255) DEFAULT NULL,
  `Field21Name` varchar(20) NOT NULL DEFAULT '',
  `Field21` varchar(255) DEFAULT NULL,
  `Field22Name` varchar(20) NOT NULL DEFAULT '',
  `Field22` varchar(255) DEFAULT NULL,
  `Field23Name` varchar(20) NOT NULL DEFAULT '',
  `Field23` varchar(255) DEFAULT NULL,
  `Field24Name` varchar(20) NOT NULL DEFAULT '',
  `Field24` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EvtRecordID`),
  KEY `ix_LstLink` (`LstLinkID`),
  KEY `ix_NtsLink` (`EventCD`,`EventID`),
  KEY `ix_EventChangeDT` (`EventChangeDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
use wca;
set @feedseriesid=2020;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
set @histoffset=1095;
set @histdate=date_sub(@fromdate, interval @histoffset day);
set @caloffset='30';
set @caldate=date_add(@fromdate, interval @caloffset day);
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
vtab.AGMEGM as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'AGM Date' as PrimaryDateName,
vtab.AGMDate as PrimaryDT,
vtab.AGMDate as EffectiveDT,
null as ExDT,
vtab.RecDate as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.fyedate as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'AGMNo' as Field01Name,
vtab.AGMNo as Field01,
'AGMTime' as Field02Name,
vtab.AGMTime as Field02,
'AddressLine1' as Field03Name,
vtab.Add1 as Field03,
'AddressLine2' as Field04Name,
vtab.Add2 as Field04,
'AddressLine3' as Field05Name,
vtab.Add3 as Field05,
'AddressLine4' as Field06Name,
vtab.Add4 as Field06,
'AddressLine5' as Field07Name,
vtab.Add5 as Field07,
'AddressLine6' as Field08Name,
vtab.Add6 as Field08,
'City' as Field09Name,
vtab.City as Field09,
'CntryCD' as Field10Name,
vtab.CntryCD as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_agm as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
/*     or (vtab.agmdate>=@fromdate and vtab.agmdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C') */
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'' as MandVoluFlag,
null as RdID,
null as Priority,
'Notification Date' as PrimaryDateName,
vtab.NotificationDate as PrimaryDT,
null as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
vtab.NotificationDate as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ann as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
/*     or (vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C') */
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
vtab.ExpCompletionDate as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_arr as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Assimilation Date' as PrimaryDateName,
vtab.AssimilationDate as PrimaryDT,
vtab.AssimilationDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_assm as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and vtab.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.AssimilationDate>=@fromdate and vtab.AssimilationDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when mpay.dutchauction='T'
     then 'DUTCH'
     else 'BUYBACK'
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Close Date' as PrimaryDateName,
vtab.Enddate as PrimaryDT,
vtab.Startdate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
vtab.Startdate as StartDT,
vtab.Enddate as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'BBMinPct' as Field01Name,
vtab.BBMinPct as Field01,
'BBMaxPct' as Field02Name,
vtab.BBMaxPct as Field02,
'OnOffFlag' as Field03Name,
vtab.OnOffFlag as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bb as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and vtab.startdate is not null
and vtab.enddate is not null
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or (vtab.enddate>=@fromdate and vtab.enddate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldBbgCompID' as Field01Name,
vtab.OldBbgCompID as Field01,
'NewBbgCompID' as Field02Name,
vtab.NewBbgCompID as Field02,
'OldBbgCompTk' as Field03Name,
vtab.OldBbgCompTk as Field03,
'NewBbgCompTk' as Field04Name,
vtab.NewBbgCompTk as Field04,
'OldCurenCD' as Field05Name,
vtab.OldCurenCD as Field05,
'NewCurenCD' as Field06Name,
vtab.NewCurenCD as Field06,
'OldCntryCD' as Field07Name,
vtab.OldCntryCD as Field07,
'NewCntryCD' as Field08Name,
vtab.NewCntryCD as Field08,
'BbcID' as Field09Name,
vtab.BbcID as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bbcc as vtab
inner join wca.bbc on vtab.bbcid = wca.bbc.bbcid
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and wca.bbc.cntrycd = substring(wca.scexh.exchgcd,1,2)
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldBbgExhID' as Field01Name,
vtab.OldBbgExhID as Field01,
'NewBbgExhID' as Field02Name,
vtab.NewBbgExhID as Field02,
'OldBbgExhTk' as Field03Name,
vtab.OldBbgExhTk as Field03,
'NewBbgExhTk' as Field04Name,
vtab.NewBbgExhTk as Field04,
'OldCurenCD' as Field05Name,
vtab.OldCurenCD as Field05,
'NewCurenCD' as Field06Name,
vtab.NewCurenCD as Field06,
'OldExchgCD' as Field07Name,
vtab.OldExchgCD as Field07,
'NewExchgCD' as Field08Name,
vtab.NewExchgCD as Field08,
'BbeID' as Field09Name,
vtab.BbeID as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bbec as vtab
inner join wca.bbe on vtab.bbeid = wca.bbe.bbeid
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and wca.bbe.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'' as MandVoluFlag,
null as RdID,
null as Priority,
'Filing Date' as PrimaryDateName,
vtab.FilingDate as PrimaryDT,
vtab.FilingDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
vtab.NotificationDate as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bkrp as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.filingdate>=@fromdate and vtab.filingdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.notificationdate>=@fromdate and vtab.notificationdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.lapsedpremium<>''
     then 'DRCA'
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID as OutResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'LaspsedPremium' as Field01Name,
vtab.LapsedPremium as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_bonid as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
vtab.StartSubscription as  StartSubscriptionDT,
vtab.EndSubscription as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'StarttradeDT' as Date1Name,
vtab.Starttrade as Date1,
'EndtradeDT' as Date2Name,
vtab.Endtrade as Date2,
'SpliteDT' as Date3Name,
vtab.SplitDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'IssuePrice' as Rate1Name,
vtab.IssuePrice as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID as OutResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'SubExpTime' as Field01Name,
vtab.SubExpTime as Field01,
'SubExpTimeZone' as Field02Name,
vtab.SubExpTimeZone as Field02,
'OverSubscription' as Field03Name,
vtab.OverSubscription as Field03,
'TraSecID' as Field04Name,
vtab.TraSecID as Field04,
'TraIsin' as Field05Name,
ifnull(trascmst.isin,'') as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_brid as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Due Date' as PrimaryDateName,
vtab.DueDate as PrimaryDT,
vtab.DueDate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     WHEN vtab.DueDate is not null then vtab.DueDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'ToFaceValue' as Rate1Name,
vtab.ToFaceValue as Rate1,
'ToPremium' as Rate2Name,
vtab.ToPremium as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'CallNumber' as Field01Name,
vtab.CallNumber as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_call as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.duedate>=@fromdate and vtab.duedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
vtab.PayDate as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
vtab.OldParvalue as ParvalueOld,
vtab.NewParvalue as ParvalueNew,
'P' as OutturnType,
wca.scmst.SectyCD as OutSectyCD,
wca.scmst.SecID as OutSecID,
'' as OutIsin,
'' as OutUsCode,
'' as OutLocalcode,
'' as OutSedol,
'' as OutBbgCompID,
'' as OutBbgCompTk,
'' as OutFigi,
'' as OutFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_caprd as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     when icc.acttime > @fromdate then icc.acttime
     when lcc.acttime > @fromdate then lcc.acttime
     when sdchg.acttime > @fromdate then sdchg.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
vtab.OldParvalue as ParvalueOld,
vtab.NewParvalue as ParvalueNew,
'P' as OutturnType,
wca.scmst.SectyCD as OutSectyCD,
wca.scmst.SecID as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'EventParValueCurenCD' as Field01Name,
vtab.CurenCD as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_consd as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.icc on vtab.eventid = wca.icc.releventid 
                       and vtab.eventcd = wca.icc.eventtype
                       and 'D'<>wca.icc.Actflag
left outer join wca.lcc ON vtab.eventid = wca.lcc.releventid 
                  and vtab.eventcd = wca.lcc.EventType 
                  and wca.scexh.ExchgCD = wca.lcc.exchgcd 
                  and 'D'<>wca.lcc.Actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.releventid 
                    and wca.exchg.CntryCD = wca.sdchg.cntrycd
                    and vtab.eventcd = wca.sdchg.eventtype 
                    and 'D'<>wca.sdchg.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.icc.effectivedate>=@fromdate and wca.icc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.lcc.effectivedate>=@fromdate and wca.lcc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.sdchg.effectivedate>=@fromdate and wca.sdchg.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
vtab.convtype as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
case when substring(vtab.Convtype,1,2)='BB'
     then 'V'
     when vtab.mandoptflag='O'
     then 'V'
     else 'M'
     end as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
case when vtab.convtype='BB'
     then 'To Date'
     else 'Effective Date'
     end as PrimaryDateName,
case when vtab.convtype='BB'
     then vtab.Todate
     else vtab.Fromdate
     end as PrimaryDT,
vtab.Fromdate as EffectiveDT,
wca.rd.RecDate as RecordDT,
null as ExDT,
null as PayDT,
null as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
case when vtab.convtype='BB'
     then vtab.fromdate
     else null
     end as OpenDT,
case when vtab.convtype='BB'
     then vtab.todate
     else null
     end as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'Settlement Date' as Date1Name,
vtab.Settlementdate as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'Price' as Rate1Name,
vtab.Price as Rate1,
'PriceAsPercent' as Rate2Name,
vtab.PriceAsPercent as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.ResSectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'PartFinal' as Field01Name,
vtab.PartFinalFlag as Field01,
'AmountConverted' as Field02Name,
vtab.AmountConverted as Field02,
'CurPair' as Field03Name,
vtab.CurPair as Field03,
'FXRate' as Field04Name,
vtab.FXRate as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_conv as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate
     or wca.rdprt.acttime>=@fromdate
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.todate>=@todate and vtab.todate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Close Date' as PrimaryDateName,
vtab.EndDate as PrimaryDT,
null as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
vtab.Startdate as StartDT,
vtab.Enddate as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ctx as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.enddate>=@todate and vtab.enddate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.OldParvalue as ParvalueOld,
vtab.NewParvalue as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldParvalueCurenCD' as Field01Name,
vtab.OldCurenCD as Field01,
'NewParvalueCurenCD' as Field02Name,
vtab.NewCurenCD as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_currd as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@todate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dist as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
case when wca.divpy.OptionID is not null and ifnull(wca.divpy.OptionSerialNo,'')=''
     then concat('2',substring(concat('0',wca.divpy.OptionID),length(wca.divpy.OptionID),2),'01',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid)
     when wca.divpy.OptionID is not null and ifnull(wca.divpy.OptionSerialNo,'')<>''
     then concat('2',substring(concat('0',wca.divpy.OptionID),length(wca.divpy.OptionID),2),
           substring(concat('0',wca.divpy.OptionSerialNo),length(wca.divpy.OptionSerialNo),2),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid)
     else  
       concat('20101',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid)
     end as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.divpy.acttime > @fromdate then wca.divpy.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.Marker='ISC' and vtab.Marker='CG' and vtab.Marker='CGL' and vtab.Marker='CGS' 
     then vtab.Marker
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.divpy.OptElectiondate as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.FYEDate as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
vtab.Declarationdate as Date1,
'Period End Date' as Date2Name,
vtab.Periodenddate  as Date2,
'FX Date' as Date3Name,
wca.divpy.FXDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
ifnull(wca.divpy.Divtype,'') as Paytype,
ifnull(wca.divpy.OptionID,1) as OptionID,
ifnull(wca.divpy.OptionSerialNo,1) as SerialID,
ifnull(wca.divpy.DefaultOpt,'') as DefaultOpt,
ifnull(wca.divpy.CurenCD,'') as RateCurenCD,
'GrossDividend' as Rate1Name,
ifnull(wca.divpy.GrossDividend,'') as Rate1,
'NetDividend' as Rate2Name,
ifnull(wca.divpy.NetDividend,'') as Rate2,
ifnull(wca.divpy.RatioOld,'') as RatioOld,
ifnull(wca.divpy.RatioNew,'') as RatioNew,
ifnull(wca.divpy.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.divpy.ResSecID
     then 'P'
     when wca.divpy.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
ifnull(wca.divpy.SectyCD,'') as OutSectyCD,
wca.divpy.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
vtab.Marker as Field01,
'Frequency' as Field02Name,
vtab.Frequency as Field02,
'TbaFlag' as Field03Name,
vtab.tbaflag as Field03,
'NilDividend' as Field04Name,
vtab.NilDividend as Field04,
'DivRescind' as Field05Name,
vtab.DivRescind as Field05,
'DeclCurenCD' as Field06Name,
vtab.DeclCurenCD as Field06,
'DeclGrossAmt' as Field07Name,
vtab.DeclGrossAmt as Field07,
'DefaultOpt' as Field08Name,
ifnull(wca.divpy.DefaultOpt,'') as Field08,
'Approxflag' as Field09Name,
wca.divpy.Approxflag as Field09,
'Taxrate' as Field10Name,
ifnull(wca.divpy.Taxrate,'') as Field10,
'Coupon' as Field11Name,
ifnull(wca.divpy.Coupon,'') as Field11,
'CouponID' as Field12Name,
ifnull(wca.divpy.CouponID,'') as Field12,
'Divrate' as Field13Name,
ifnull(wca.divpy.Divrate,'') as Field13,
'CurPair' as Field14Name,
ifnull(wca.divpy.CurPair,'') as Field14,
'Depfees' as Field15Name,
ifnull(wca.divpy.Depfees,'') as Field15,
'DepfeesCurenCD' as Field16Name,
ifnull(wca.divpy.DepfeesCurenCD,'') as Field16,
'RescindCashDiv' as Field17Name,
ifnull(wca.divpy.RecindCashDiv,'') as Field17,
'RescindStockDiv' as Field18Name,
ifnull(wca.divpy.RecindStockDiv,'') as Field18,
'USDRatetoCurrency' as Field19Name,
ifnull(wca.divpy.USDRatetoCurrency,'') as Field19,
'Group2Grossdiv' as Field20Name,
ifnull(wca.divpy.group2grossdiv,'') as Field20,
'Group2Netdiv' as Field21Name,
ifnull(wca.divpy.group2netdiv,'') as Field21,
'Equalisation' as Field22Name,
ifnull(wca.divpy.equalisation,'') as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.divpy on vtab.divid=wca.divpy.divid
left outer join wca.event on vtab.eventcd=wca.event.eventtype
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or wca.divpy.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
vtab.ExpCompletionDate as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dmrgr as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
vtab.StartSubscription as  StartSubscriptionDT,
vtab.EndSubscription as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when vtab.ressecid is not null and vtab.curencd<>''
     then 'B'
     when vtab.ressecid is not null
     then 'S'
     when vtab.curencd<>'' is not null
     then 'C'
     else ''
     end as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Name,
vtab.MinPrice as Rate1,
'MaxPrice' as Rate2Name,
vtab.MaxPrice as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
vtab.MinQlyQty,
vtab.MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
vtab.TndrStrkPrice,
vtab.TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime,
vtab.ExpTimeZone,
'TraSecID' as Field01Name,
vtab.TraSecID as Field01,
'TraIsin' as Field02Name,
ifnull(trascmst.isin,'') as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_dvst as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.Enttype=''
     then 'NRENRTS'
     else vtab.Enttype
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
case when vtab.Enttype=''
     then 'M'
     else 'V'
     end as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
vtab.StartSubscription as  StartSubscriptionDT,
vtab.EndSubscription as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'EntIssuePrice' as Rate1Name,
vtab.EntIssuePrice as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'OverSubscription' as Field01Name,
vtab.OverSubscription as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_entid as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Notification Date' as PrimaryDateName,
vtab.NotificationDate as PrimaryDT,
null as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
vtab.NotificationDate as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'OldFYStartDate' as Date1Name,
vtab.OldFYStartDate as Date1,
'OldFYEndDate' as Date2Name,
vtab.OldFYEndDate as Date2,
'NewFYStartDate' as Date3Name,
vtab.NewFYStartDate as Date3,
'NewFYEndDate' as Date4Name,
vtab.NewFYEndDate as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_fychg as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.NotificationDate>=@fromdate and vtab.NotificationDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldIsin' as Field01Name,
vtab.OldIsin as Field01,
'NewIsin' as Field02Name,
vtab.NewIsin as Field02,
'OldUSCode' as Field03Name,
vtab.OldUSCode as Field03,
'NewUSCode' as Field04Name,
vtab.NewUSCode as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_icc as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldIsin <> '' or vtab.NewIsin <> '' or vtab.OldUSCode <> '' or vtab.NewUSCode <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when icc.acttime > @fromdate then icc.acttime
     when lcc.acttime > @fromdate then lcc.acttime
     when sdchg.acttime > @fromdate then sdchg.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Incorporation Change Date' as PrimaryDateName,
vtab.InChgDate as PrimaryDT,
vtab.InChgDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldCntryCD' as Field01Name,
vtab.OldCntryCD as Field01,
'NewCntryCD' as Field02Name,
vtab.NewCntryCD as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_inchg as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.icc on vtab.eventid = wca.icc.releventid 
                       and vtab.eventcd = wca.icc.eventtype
                       and 'D'<>wca.icc.Actflag
left outer join wca.lcc ON vtab.eventid = wca.lcc.releventid 
                  and vtab.eventcd = wca.lcc.EventType 
                  and wca.scexh.ExchgCD = wca.lcc.exchgcd 
                  and 'D'<>wca.lcc.Actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.releventid 
                    and wca.exchg.CntryCD = wca.sdchg.cntrycd
                    and vtab.eventcd = wca.sdchg.eventtype 
                    and 'D'<>wca.sdchg.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (wca.icc.effectivedate>=@fromdate and wca.icc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.lcc.effectivedate>=@fromdate and wca.lcc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.sdchg.effectivedate>=@fromdate and wca.sdchg.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('1',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when icc.acttime > @fromdate then icc.acttime
     when lcc.acttime > @fromdate then lcc.acttime
     when sdchg.acttime > @fromdate then sdchg.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Name Change Date' as PrimaryDateName,
vtab.NameChangeDate  as PrimaryDT,
vtab.NameChangeDate  as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'IssOldName' as Field01Name,
vtab.IssOldName as Field01,
'IssNewName' as Field02Name,
vtab.IssNewName as Field02,
'LegalName' as Field03Name,
case when wca.vtab.LegalName='T' then 'T' else 'F' end as Field03,
'MeetingDateFlag' as Field04Name,
case when wca.vtab.MeetingDateFlag='T' then 'T' else 'F' end as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ischg as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.icc on vtab.eventid = wca.icc.releventid 
                       and vtab.eventcd = wca.icc.eventtype
                       and 'D'<>wca.icc.Actflag
left outer join wca.lcc ON vtab.eventid = wca.lcc.releventid 
                  and vtab.eventcd = wca.lcc.EventType 
                  and wca.scexh.ExchgCD = wca.lcc.exchgcd 
                  and 'D'<>wca.lcc.Actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.releventid 
                    and wca.exchg.CntryCD = wca.sdchg.cntrycd
                    and vtab.eventcd = wca.sdchg.eventtype 
                    and 'D'<>wca.sdchg.Actflag
Where
wca.scexh.Actflag<>'D'
and (vtab.IssOldName <> '' or vtab.IssNewName <> '' or vtab.LegalName <> '' or vtab.MeetingDateFlag <> '')
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (wca.icc.effectivedate>=@fromdate and wca.icc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.lcc.effectivedate>=@fromdate and wca.lcc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.sdchg.effectivedate>=@fromdate and wca.sdchg.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldLocalcode' as Field01Name,
vtab.OldLocalcode as Field01,
'NewLocalcode' as Field02Name,
vtab.NewLocalcode as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lcc as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and vtab.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldLocalCode <> '' or vtab.NewLocalCode <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('3',ifnull(wca.mpay.OptionID,'1'),ifnull(wca.mpay.SerialID,'1'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Record Date' as PrimaryDateName,
vtab.RdDate as PrimaryDT,
null as EffectiveDT,
null as ExDT,
vtab.RdDate as RecordDT,
wca.mpay.paydate as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Liquidator' as Field01Name,
vtab.Liquidator as Field01,
'LiqAdd1' as Field02Name,
vtab.LiqAdd1 as Field02,
'LiqAdd2' as Field03Name,
vtab.LiqAdd2 as Field03,
'LiqAdd3' as Field04Name,
vtab.LiqAdd3 as Field04,
'LiqAdd4' as Field05Name,
vtab.LiqAdd4 as Field05,
'LiqAdd5' as Field06Name,
vtab.LiqAdd5 as Field06,
'LiqAdd6' as Field07Name,
vtab.LiqAdd6 as Field07,
'LiqCity' as Field08Name,
vtab.LiqCity as Field08,
'LiqCntryCD' as Field09Name,
vtab.LiqCntryCD as Field09,
'LiqTel' as Field10Name,
vtab.LiqTel as Field10,
'LiqFax' as Field11Name,
vtab.LiqFax as Field11,
'LiqEmail' as Field12Name,
vtab.LiqEmail as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_liq as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or mpay.acttime>=@fromdate 
     or (vtab.rddate>=@fromdate and vtab.rddate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
vtab.NotificationDate as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldLstatStatus' as Field01Name,
case when vtab.OldLstatStatus='N' or vtab.OldLstatStatus='R' or vtab.OldLstatStatus='' then 'L'
     when vtab.OldLstatStatus='S' then 'S'
     when vtab.OldLstatStatus='D' then 'D'
     else ''
     end as Field01,
'NewLstatStatus' as Field02Name,
case when vtab.LstatStatus='N' or vtab.LstatStatus='R' or vtab.LstatStatus='' then 'L'
     when vtab.LstatStatus='S' then 'S'
     when vtab.LstatStatus='D' then 'D'
     else ''
     end as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_lstat as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and vtab.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldLotsize' as Field01Name,
ifnull(vtab.OldLot,'') as Field01,
'NewLotsize' as Field02Name,
ifnull(vtab.NewLot,'') as Field02,
'OldMinTraQty' as Field03Name,
ifnull(vtab.OldMinTrdQty,'') as Field03,
'NewMinTraQty' as Field04Name,
ifnull(vtab.NewMinTrdgQty,'') as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ltchg as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and vtab.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldLot <> '' or vtab.NewLot <> '' or vtab.OldMinTrdQty <> '' or vtab.NewMinTrdgQty <> '' );
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldMktsgID' as Field01Name,
vtab.OldMktsgID as Field01,
'NewMktsgID' as Field02Name,
vtab.NewMktsgID as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mkchg as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
       and vtab.exchgcd = wca.scexh.exchgcd
       and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldMktsgID <> '' or vtab.NewMktsgID <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
vtab.ExpCompletionDate as ExpCompletionDT,
'AppointedDT' as Date1Name,
vtab.AppointedDate as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'MrgrStatus' as Field01Name,
vtab.mrgrstatus as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mrgr as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.mpay.paydate>=@fromdate and wca.mpay.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));

insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Close Date' as PrimaryDateName,
vtab.Enddate as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
vtab.Startdate as StartDT,
vtab.Enddate as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'BuyInCurenCD' as Field01Name,
vtab.BuyInCurenCD as Field01,
'BuyInPrice' as Field02Name,
vtab.BuyInPrice as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_oddlt as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.enddate>=@fromdate and vtab.enddate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Close Date' as PrimaryDateName,
vtab.OfferCloses as PrimaryDate,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
vtab.OfferOpens as OpenDT,
vtab.OfferCloses as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'C' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Name,
vtab.MinPrice as Rate1,
'MaxPrice' as Rate2Name,
vtab.MaxPrice as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
vtab.MinOfrQty,
vtab.MaxOfrQty,
vtab.MinQlyQty,
vtab.MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
vtab.TndrStrkPrice,
vtab.TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime,
vtab.ExpTimeZone,
'NegotiatedPrice' as Field01Name,
vtab.NegotiatedPrice as Field01,
'SealedBid' as Field02Name,
vtab.SealedBid as Field02,
'MinPercent' as Field03Name,
vtab.POMinPercent as Field03,
'MaxPercent' as Field04Name,
vtab.POMaxPercent as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_po as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (vtab.OfferCloses>=@fromdate and vtab.OfferCloses<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldExchgCD' as Field01Name,
vtab.OldExchgCD as Field01,
'NewExchgCD' as Field02Name,
vtab.NewExchgCD as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_prchg as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldExchgCD <> '' or vtab.NewExchgCD <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'End Subscription Date' as PrimaryDateName,
vtab.EndSubscription as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
vtab.StartSubscription as  StartSubscriptionDT,
vtab.EndSubscription as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when vtab.ressecid is not null and vtab.curencd<>''
     then 'B'
     when vtab.ressecid is not null
     then 'S'
     when vtab.curencd<>'' is not null
     then 'C'
     else ''
     end as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Name,
vtab.MinPrice as Rate1,
'MaxPrice' as Rate2Name,
vtab.MaxPrice as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
vtab.MinQlyQty,
vtab.MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
vtab.TndrStrkPrice,
vtab.TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'OffereeIssID' as Field01Name,
vtab.OffereeIssID as Field01,
'OffereeName' as Field02Name,
vtab.OffereeName as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_prfid as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (vtab.EndSubscription>=@fromdate and vtab.EndSubscription<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.OldParvalue as ParvalueOld,
vtab.NewParvalue as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'EventParValueCurenCD' as Field01Name,
vtab.CurenCD as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_pvrd as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.RocBasis=''
     then 'RCAP'
     else vtab.RocBasis
     end as EventSubtype,
vtab.EventType as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Pay Date' as PrimaryDateName,
vtab.CSPYDate  as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
vtab.CSPYDate as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
vtab.FinancialYearEndDate as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'C' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'Cashbak' as Rate1Name,
vtab.CashBak as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_rcap as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (vtab.Effectivedate>=@fromdate and vtab.Effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.RelEventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'OldOutstandingDate' as Date1Name,
vtab.OldOutstandingDate as Date1,
'NewOutstandingDate' as Date2Name,
vtab.NewOutstandingDate as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldSOS' as Field01Name,
vtab.OldSOS as Field01,
'NewSOS' as Field02Name,
vtab.NewSOS as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_shoch as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.Effectivedate>=@fromdate and vtab.Effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.LapsedPremium<>''
     then 'DRCA'
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
vtab.StartSubscription as  StartSubscriptionDT,
vtab.EndSubscription as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'StarttradeDT' as Date1Name,
vtab.Starttrade as Date1,
'EndtradeDT' as Date2Name,
vtab.Endtrade as Date2,
'SpliteDT' as Date3Name,
vtab.SplitDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'IssuePrice' as Rate1Name,
vtab.IssuePrice as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'SubExpTime' as Field01Name,
vtab.SubExpTime as Field01,
'SubExpTimeZone' as Field02Name,
vtab.SubExpTimeZone as Field02,
'OverSubscription' as Field03Name,
vtab.OverSubscription as Field03,
'LapsedPremium' as Field04Name,
vtab.LapsedPremium as Field04,
'RatioOldDist' as Field05Name,
vtab.OldDistRatio as Field05,
'RatioNewDist' as Field06Name,
vtab.NewDistRatio as Field06,
'RatioOldExer' as Field07Name,
vtab.OldExerciseRatio as Field07,
'RatioNewExer' as Field08Name,
vtab.NewExerciseRatio as Field08,
'TraSecID' as Field09Name,
vtab.TraSecID as Field09,
'TraIsin' as Field10Name,
ifnull(trascmst.isin,'') as Field10,
'PPSecID' as Field11Name,
vtab.PPSecID as Field11,
'PPIsin' as Field12Name,
ifnull(ppscmst.isin,'') as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_rtsid as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
left outer join wca.scmst as ppscmst on vtab.ppsecid = ppscmst.secid
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.DateOfChange as PrimaryDT,
vtab.DateOfChange as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'SecOldName' as Field01Name,
vtab.SecOldName as Field01,
'SecNewName' as Field02Name,
vtab.SecNewName as Field02,
'OldSectyCD' as Field03Name,
vtab.OldSectyCD as Field03,
'NewSectyCD' as Field04Name,
vtab.NewSectyCD as Field04,
'OldRegS144A' as Field05Name,
vtab.OldRegS144A as Field05,
'NewRegS144A' as Field06Name,
vtab.NewRegS144A as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_scchg as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.DateOfChange>=@fromdate and vtab.DateOfChange<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.SecOldName <> '' or vtab.SecNewName <> '' or vtab.OldSectyCD <> '' or vtab.NewSectyCD <> '' or vtab.OldRegS144A <> '' or vtab.NewRegS144A <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_scswp as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     when icc.acttime > @fromdate then icc.acttime
     when lcc.acttime > @fromdate then lcc.acttime
     when sdchg.acttime > @fromdate then sdchg.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
vtab.OldParvalue as ParvalueOld,
vtab.NewParvalue as ParvalueNew,
'P' as OutturnType,
wca.scmst.SectyCD as OutSectyCD,
wca.scmst.SecID as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldParvalueCurenCD' as Field01Name,
vtab.OldCurenCD as Field01,
'NewParvalueCurenCD' as Field02Name,
vtab.NewCurenCD as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_sd as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.icc on vtab.eventid = wca.icc.releventid 
                       and vtab.eventcd = wca.icc.eventtype
                       and 'D'<>wca.icc.Actflag
left outer join wca.lcc ON vtab.eventid = wca.lcc.releventid 
                  and vtab.eventcd = wca.lcc.EventType 
                  and wca.scexh.ExchgCD = wca.lcc.exchgcd 
                  and 'D'<>wca.lcc.Actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.releventid 
                    and wca.exchg.CntryCD = wca.sdchg.cntrycd
                    and vtab.eventcd = wca.sdchg.eventtype 
                    and 'D'<>wca.sdchg.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.icc.effectivedate>=@fromdate and wca.icc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.lcc.effectivedate>=@fromdate and wca.lcc.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.sdchg.effectivedate>=@fromdate and wca.sdchg.effectivedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
vtab.EventID as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldSedol' as Field01Name,
vtab.OldSedol as Field01,
'NewSedol' as Field02Name,
vtab.NewSedol as Field02,
'OldCntryCD' as Field03Name,
vtab.CntryCD as Field03,
'NewCntryCD' as Field04Name,
vtab.NewCntryCD as Field04,
'OldRegCntryCD' as Field05Name,
vtab.RCntryCD as Field05,
'NewRegCntryCD' as Field06Name,
vtab.NewRCntryCD as Field06,
'OldTradingCurenCD' as Field07Name,
vtab.OldCurenCD as Field07,
'NewTradingCurenCD' as Field08Name,
vtab.NewCurenCD as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_sdchg as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
       and vtab.newcntrycd = wca.exchg.cntrycd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))))
and (vtab.OldSedol <> '' or vtab.NewSedol <> '');
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.RatioOld,
vtab.RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.SectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_secrc as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('2',
       ifnull(substring(concat('0',wca.mpay.OptionID),length(wca.mpay.OptionID),2),'01'),
       ifnull(substring(concat('0',wca.mpay.SerialID),length(wca.mpay.SerialID),2),'01'),
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.mpay.acttime > @fromdate then wca.mpay.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.minitkovr='T'
     then 'MINITKOVR'
     when vtab.closedate is not null and vtab.opendate is null
     then 'MRGR'
     when vtab.opendate is not null and vtab.cmacqdate is not null
     then 'MRGR'
     when vtab.opendate is not null and vtab.tkovrstatus='C'
     then 'MRGR'
     else 'TEND'
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
case when vtab.minitkovr='T'
     then 'V'
     when vtab.cmacqdate is not null
     then 'M'
     when vtab.closedate is not null and vtab.opendate is null
     then 'M'
     when vtab.closedate is not null and vtab.tkovrstatus='C'
     then 'M'
     else 'V'
     end as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
case when vtab.minitkovr='T' or (vtab.cmacqdate is null and vtab.tkovrstatus<>'C' and vtab.opendate is not null)
     then 'Close Date'
     else 'Effective Date'
     end as PrimaryDateName,
case when vtab.minitkovr='T' or (vtab.cmacqdate is null and vtab.tkovrstatus<>'C' and vtab.opendate is not null)
     then vtab.closedate
     when vtab.cmacqdate is not null
     then vtab.cmacqdate
     else vtab.closedate
     end as PrimaryDT,
case when vtab.minitkovr='T' or (vtab.cmacqdate is null and vtab.tkovrstatus<>'C' and vtab.opendate is not null)
     then vtab.opendate
     when vtab.cmacqdate is not null
     then vtab.cmacqdate
     when vtab.tkovrstatus='C'
     then vtab.closedate
     else null
     end as EffectiveDT,
CASE WHEN wca.exdt.exdtID IS NOT NULL then wca.exdt.ExDate 
     ELSE pexdt.exdate END as ExDT,
wca.rd.RecDate as RecordDT,
CASE WHEN wca.mpay.paydate IS NOT NULL then wca.mpay.paydate
     WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.PayDate
     ELSE pexdt.paydate END as PayDT,
CASE WHEN wca.exdt.exdtid IS NOT NULL then wca.exdt.DuebillsRedemDate 
     ELSE pexdt.DuebillsRedemDate END as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
vtab.Opendate as OpenDT,
vtab.Closedate as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.mpay.OptElectiondate as OptElectionDT,
wca.mpay.WithdrawalFromDate as WithdrawalFromDT,
wca.mpay.WithdrawalToDate as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
vtab.ExpCompletionDate as ExpCompletionDT,
'Unconditional Date' as Date1Name,
vtab.UnconditionalDate as Date1,
'Compulsory Acquisition Date' as Date2Name,
vtab.cmacqdate as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when ifnull(wca.mpay.Paytype,'')<>'' then wca.mpay.Paytype
     when ifnull(wca.mpay.rationew,'')<>'' and ifnull(wca.mpay.curencd,'')<>'' then 'B'
     when ifnull(wca.mpay.curencd,'')<>'' then 'C'
     when ifnull(wca.mpay.rationew,'')<>'' then 'S'
     else '' end as Paytype,
ifnull(wca.mpay.OptionID,1),
ifnull(wca.mpay.SerialID,1),
ifnull(wca.mpay.DefaultOpt,'') as DefaultOpt,
ifnull(wca.mpay.CurenCD,'') as CurenCD,
'Minprice' as Rate1Name,
ifnull(wca.mpay.Minprice,'') as Rate1,
'Maxprice' as Rate2Name,
ifnull(wca.mpay.Maxprice,'') as Rate2,
ifnull(wca.mpay.RatioOld,'') as RatioOld,
ifnull(wca.mpay.RatioNew,'') as RatioNew,
ifnull(wca.mpay.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=wca.mpay.ResSecID
     then 'P'
     when wca.mpay.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=wca.mpay.ResSecID
     then wca.scmst.sectycd
     when wca.mpay.ResSecID is null
     then ''
     else wca.mpay.SectyCD
     end as OutSectyCD,
wca.mpay.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
ifnull(wca.mpay.MinOfrQty,'') as MinOfrQty,
ifnull(wca.mpay.MaxOfrQty,'') as MaxOfrQty,
ifnull(wca.mpay.MinQlyQty,'') as MinQlyQty,
ifnull(wca.mpay.MaxQlyQty,'') as MaxQlyQty,
vtab.MinAcpQty,
vtab.MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
ifnull(wca.mpay.WithdrawalRights,'') as WithdrawalRights,
ifnull(wca.mpay.OedExpTime,'') as OedExpTime,
ifnull(wca.mpay.OedExpTimeZone,'') as OedExpTimeZone,
ifnull(wca.mpay.WrtExpTime,'') as WrtExpTime,
ifnull(wca.mpay.WrtExpTimeZone,'') as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'PreOfrQTy' as Field01Name,
vtab.PreOfferQty as Field01,
'PreOfrPct' as Field02Name,
vtab.PreOfferPercent as Field02,
'TargetQty' as Field03Name,
vtab.TargetQuantity as Field03,
'TargetPct' as Field04Name,
vtab.TargetPercent as Field04,
'OfferorIssID' as Field05Name,
vtab.OfferorIssID as Field05,
'OfferorName' as Field06Name,
vtab.OfferorName as Field06,
'Hostile' as Field07Name,
vtab.Hostile as Field07,
'EventStatus' as Field08Name,
vtab.TkovrStatus as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_tkovr as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid
            and vtab.eventcd = wca.mpay.sEvent
            and 'D' <> wca.mpay.Actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.mpay.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or (vtab.closedate>=@fromdate and vtab.closedate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.cmacqdate>=@fromdate and vtab.cmacqdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.EffectiveDate as PrimaryDT,
vtab.EffectiveDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldDRRatio' as Field01Name,
vtab.OldDRRatio as Field01,
'NewDRRatio' as Field02Name,
vtab.NewDRRatio as Field02,
'OldUNRatio' as Field03Name,
vtab.OldUNRatio as Field03,
'NewUNRatio' as Field04Name,
vtab.NewUNRatio as Field04,
'OldUNSecID' as Field05Name,
vtab.OldUNSecID as Field05,
'NewUNSecID' as Field06Name,
vtab.NewUNSecID as Field06,	
'OldDepbank' as Field07Name,
vtab.OldDepbank as Field07,
'NewDepbank' as Field08Name,
vtab.NewDepbank as Field08,
'OldDRtype' as Field09Name,
vtab.OldDRtype as Field09,
'NewDRtype' as Field10Name,
vtab.NewDRtype as Field10,
'OldLevel' as Field11Name,
vtab.OldLevel as Field11,
'NewLevel' as Field12Name,
vtab.NewLevel as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_drchg as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       substring(concat('0000000',wca.scmst.secid),length(wca.scmst.secid),8),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.clssc.acttime > @fromdate then wca.clssc.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Period End Date' as PrimaryDateName,
vtab.PeriodEndDate as PrimaryDT,
vtab.PeriodStartDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
vtab.PeriodStartDate as StartDT,
vtab.PeriodEndDate as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'Filing Date' as Date1Name,
vtab.FilingDate as Date1,
'Lead Plaintiff Deadline Date' as Date2Name,
vtab.LeadPDate as Date2,
'Proof Filing Deadline Date' as Date3Name,
vtab.PrffDate as Date3,
'Claims Filing Deadline Date' as Date4Name,
vtab.ClaimfDate as Date4,
'Objection Deadline Date' as Date5Name,
vtab.ObjDate as Date5,
'Exclusion Deadline Date' as Date6Name,
vtab.ExclDate as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.Setcurcd as RateCurenCD,
'Settlement Amount' as Rate1Name,
vtab.Setamount as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'CourtID' as Field01Name,
vtab.CourtID as Field01,
'CaseNumber' as Field02Name,
vtab.CaseNo as Field02,
'Judge' as Field03Name,
vtab.Judge as Field03,
'CaseName' as Field04Name,
vtab.CaseName as Field04,
'Allegations' as Field05Name,
vtab.Allegation as Field05,
'EligibleSecurities' as Field06Name,
vtab.Eligsec as Field06,
'InvestorClass' as Field07Name,
vtab.Investcls as Field07,
'OnBehalfOf' as Field08Name,
vtab.OnBehalf as Field08,
'LeadPlaintiff' as Field09Name,
vtab.LeadPtif as Field09,
'CaseStatus' as Field10Name,
vtab.CaseStatus as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_clsac as vtab
inner join wca.clssc on vtab.clsacid = wca.clssc.clsacid
inner join wca.scmst on wca.clssc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.event on vtab.eventcd = wca.event.eventtype
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.clssc.acttime>=@fromdate 
     or (vtab.PeriodEndDate>=@fromdate and vtab.PeriodEndDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'V' as MandVoluFlag,
null as RdID,
null as Priority,
'To Date' as PrimaryDateName,
vtab.ToDate as PrimaryDT,
vtab.FromDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
vtab.FromDate as FromDT,
vtab.ToDate as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'StrikePrice' as Rate1Name,
vtab.StrikePrice as Rate1,
'PricePerShare' as Rate2Name,
vtab.PricePerShare as Rate2,
vtab.RatioOld,
vtab.RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when vtab.ExerSecID is not null
     then 'R'
     else 'U'
     end as OutturnType,
scmstexer.SectyCD as OutSectyCD,
vtab.ExerSecID as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_warex as vtab
inner join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.scmst as scmstexer on vtab.exersecid=scmstexer.secid
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.ToDate>=@fromdate and vtab.ToDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
case when wca.intpy.OptionID is not null
     then concat('5',substring(concat('0',wca.intpy.OptionID),length(wca.intpy.OptionID),2),'01',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid)
     else  
       concat('50101',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid)
     end as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.intpy.acttime > @fromdate then wca.intpy.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
'DIV' as EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Pay Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
wca.intpy.OptElectiondate as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
null as Date1,
'Period End Date' as Date2Name,
null as Date2,
'FX Date' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
ifnull(wca.intpy.inttype,'C') as Paytype,
ifnull(wca.intpy.OptionID,'1') as OptionID,
'1' as OptionSerialNo,
ifnull(wca.intpy.DefaultOpt,'') as DefaultOpt,
ifnull(wca.intpy.CurenCD,'') as RateCurenCD,
'GrossDividend' as Rate1Name,
wca.intpy.grossinterest*wca.scmst.parvalue as Rate1,
'NetDividend' as Rate2Name,
ifnull(wca.intpy.NetInterest,'') as Rate2,
ifnull(wca.intpy.RatioOld,'') as RatioOld,
ifnull(wca.intpy.RatioNew,'') as RatioNew,
ifnull(wca.intpy.Fractions,'') as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.intpy.ResSecID is not null and wca.scmst.secid<>wca.intpy.ResSecID
     then 'R'
     when wca.intpy.ResSecID is not null and wca.scmst.secid=wca.intpy.ResSecID
     then 'P'
     when wca.intpy.inttype='S'
     then 'U'
     else ''
     end as OutturnType,
ifnull(wca.intpy.SectyCD,'') as OutSectyCD,
wca.intpy.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
'' as Field01,
'Frequency' as Field02Name,
wca.bond.InterestPaymentFrequency as Field02,
'TbaFlag' as Field03Name,
'' as Field03,
'NilDividend' as Field04Name,
'' as Field04,
'DivRescind' as Field05Name,
'' as Field05,
'DeclCurenCD' as Field06Name,
'' as Field06,
'DeclGrossAmt' as Field07Name,
'' as Field07,
'DefaultOpt' as Field08Name,
ifnull(wca.intpy.DefaultOpt,'') as Field08,
'Approxflag' as Field09Name,
'' as Field09,
'Taxrate' as Field10Name,
ifnull(wca.intpy.DomesticTaxRate,'') as Field10,
'Coupon' as Field11Name,
ifnull(wca.intpy.CouponNo,'') as Field11,
'CouponID' as Field12Name,
'' as Field12,
'Divrate' as Field13Name,
'' as Field13,
'CurPair' as Field14Name,
'' as Field14,
'Depfees' as Field15Name,
'' as Field15,
'DepfeesCurenCD' as Field16Name,
'' as Field16,
'RescindCashDiv' as Field17Name,
ifnull(wca.intpy.RescindInterest,'') as Field17,
'RescindStockDiv' as Field18Name,
ifnull(wca.intpy.RescindStockInterest,'') as Field18,
'USDRatetoCurrency' as Field19Name,
'' as Field19,
'Group2Grossdiv' as Field20Name,
'' as Field20,
'Group2Netdiv' as Field21Name,
'' as Field21,
'Equalisation' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_int_my as vtab
inner join wca.intpy on vtab.rdid=intpy.rdid
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.event on vtab.eventcd=wca.event.eventtype
Where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (vtab.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or wca.intpy.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.exdt.paydate>=@fromdate and wca.exdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.paydate>=@fromdate and pexdt.paydate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
vtab.Eventtype as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.Effectivedate as PrimaryDT,
vtab.Effectivedate as EffectiveDT,
null as RecordDT,
null as ExDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.RatioOld,
vtab.RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
scmstconv.SectyCD as OutSectyCD,
vtab.ResSecID as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'OldUTType' as Field01Name,
vtab.OldUTType as Field01,
'NewUTType' as Field02Name,
vtab.NewUTType as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_mfcon as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.scmst as scmstconv on vtab.ressecid=scmstconv.secid
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.EffectiveDate>=@fromdate and vtab.EffectiveDate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.redemtype='' then 'REDEM'
     else vtab.redemtype
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
case when substring(vtab.redemtype,1,2)='BB' or vtab.redemtype='PUT'
     then 'V'
     when vtab.mandoptflag='O'
     then 'V'
     else 'M'
     end as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
'Pay Date' as PrimaryDateName,
vtab.Redemdate as PrimaryDT,
case when substring(vtab.redemtype,1,2)<>'BB' and vtab.redemtype<>'PUT'
     then vtab.Redemdate
     else null
     end as EffectiveDT,
null as ExDT,
wca.rd.RecDate as RecordDT,
vtab.Redemdate as PayDT,
null as DuebillsRedemDT,
case when wca.rd.ToDate is not null then wca.rd.RecDate
     else null
     end as FromDT,
case when wca.rd.ToDate is not null then wca.rd.ToDate
     else null
     end as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
vtab.tenderopendate as OpenDT,
vtab.tenderclosedate as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'S' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'RedemPrice' as Rate1Name,
vtab.RedemPrice as Rate1,
'RedemPremium' as Rate2Name,
vtab.RedemPremium as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'PartFinal' as Field01Name,
vtab.PartFinal as Field01,
'AmountRedeemed' as Field02Name,
vtab.AmountRedeemed as Field02,
'RedemPct' as Field03Name,
vtab.RedemPercent as Field03,
'Poolfactor' as Field04Name,
vtab.Poolfactor as Field04,
'PriceAsPct' as Field05Name,
vtab.PriceAsPercent as Field05,
'TenderOfferor' as Field06Name,
vtab.TenderOfferor as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_redem as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or (vtab.redemdate>=@fromdate and vtab.redemdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
case when vtab.convtype = '' then 'CONV'
     else vtab.convtype
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
case when vtab.convtype='BB'
     then 'V'
     when vtab.mandoptflag='O'
     then 'V'
     else 'M'
     end as MandVoluFlag,
vtab.RdID,
wca.rdprt.Priority,
case when vtab.convtype='BB'
     then 'To Date'
     else 'Pay Date'
     end as 'PrimaryDateName',
case when vtab.convtype='BB'
     then vtab.Todate
     else vtab.SettlementDate
     end as PrimaryDT,
case when vtab.convtype<>'BB'
     then vtab.Fromdate
     else null
     end as EffectiveDT,
wca.rd.RecDate as RecordDT,
null as ExDT,
vtab.Settlementdate as PayDT,
null as DuebillsRedemDT,
vtab.Fromdate as FromDT,
vtab.Todate as ToDT,
wca.rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
case when vtab.convtype='BB'
     then 'C'
     else 'S'
     end as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
vtab.CurenCD as RateCurenCD,
'Price' as Rate1Name,
vtab.Price as Rate1,
'' as Rate2Name,
'' as Rate2,
vtab.RatioOld,
vtab.RatioNew,
vtab.Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
case when wca.scmst.secid=vtab.ResSecID
     then 'P'
     when vtab.ResSecID is null
     then 'U'
     else 'R'
     end as OutturnType,
case when wca.scmst.secid=vtab.ResSecID
     then wca.scmst.sectycd
     when vtab.ResSecID is null
     then ''
     else vtab.ResSectyCD
     end as OutSectyCD,
vtab.ResSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
vtab.ExpTime as ExpTime,
vtab.ExpTimeZone as ExpTimeZone,
'PartFinalFlag' as Field01Name,
vtab.PartFinalFlag as Field01,
'AmountConverted' as Field02Name,
vtab.AmountConverted as Field02,
'PriceAsPct' as Field03Name,
vtab.PriceAsPercent as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_conv as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or (vtab.Todate>=@fromdate and vtab.Todate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.Settlementdate>=@fromdate and vtab.Settlementdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
vtab.EventID as ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wcasedol.acttime > @fromdate then wcasedol.acttime
     when wca.bbe.acttime > @fromdate then wca.bbe.acttime
     when wca.bbc.acttime > @fromdate then wca.bbc.acttime
     when bbebnd.acttime > @fromdate then bbebnd.acttime
     else vtab.acttime
     end as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'Effective Date' as PrimaryDateName,
vtab.Listdate as PrimaryDT,
vtab.Listdate as EffectiveDT,
null as RecordDT,
null as ExDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'' as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_nlist_scexh as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
LEFT OUTER join wca.exchg ON vtab.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca_other.sedol as wcasedol ON vtab.secid = wcasedol.secid and wca.exchg.cntrycd=wcasedol.cntrycd and 'D'<>wcasedol.Actflag
left outer join wca.bbe on vtab.secid=wca.bbe.secid and vtab.exchgcd=wca.bbe.exchgcd and 'D'<>wca.bbe.Actflag
left outer join wca.bbc on vtab.secid=wca.bbc.secid and wca.exchg.cntrycd=wca.bbc.cntrycd and 'D'<>wca.bbc.Actflag
left outer join wca.bbe as bbebnd on vtab.secid=bbebnd.secid and ''=bbebnd.exchgcd and 'D'<>bbebnd.Actflag
Where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.announcedate>date_sub(@fromdate, interval 150 day)
     and (vtab.acttime>=@fromdate
     or wca.scmst.acttime=@fromdate
     or wcasedol.acttime=@fromdate
     or wca.bbe.acttime=@fromdate
     or wca.bbc.acttime=@fromdate
     or bbebnd.acttime=@fromdate
     or (vtab.Listdate>=@fromdate and vtab.Listdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       wca.drip.divid) as EvtRecordID,
case when vtab.Actflag='C' or vtab.Actflag='D' then vtab.Actflag
     else wca.drip.Actflag
     end as Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.drip.acttime > @fromdate then wca.drip.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
wca.drip.AnnounceDate as EventCreateDT,
wca.event.eventtype as EventCD,
vtab.EventID,
case when vtab.Marker='ISC' and vtab.Marker='CG' and vtab.Marker='CGL' and vtab.Marker='CGS' 
     then vtab.Marker
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.FYEDate as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
vtab.Declarationdate as Date1,
'Period End Date' as Date2Name,
vtab.Periodenddate  as Date2,
'Drip Last Date' as Date3Name,
wca.drip.DripLastdate as Date3,
'Drip Pay Date' as Date4Name,
wca.drip.DripPaydate as Date4,
'Crest Date' as Date5Name,
wca.drip.Crestdate as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
ifnull(wca.divpy.CurenCD,'') as RateCurenCD,
'Drip ReinvPrice' as Rate1Name,
wca.drip.DripReinvPrice as Rate1,
'' as Rate2Name,
'' as Rate2,
wca.drip.RatioOld,
wca.drip.RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'P' as OutturnType,
wca.scmst.SectyCD as OutSectyCD,
wca.scmst.secid as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
vtab.Marker as Field01,
'Frequency' as Field02Name,
vtab.Frequency as Field02,
'TbaFlag' as Field03Name,
vtab.tbaflag as Field03,
'NilDividend' as Field04Name,
vtab.NilDividend as Field04,
'DivRescind' as Field05Name,
vtab.DivRescind as Field05,
'DeclCurenCD' as Field06Name,
vtab.DeclCurenCD as Field06,
'DeclGrossAmt' as Field07Name,
vtab.DeclGrossAmt as Field07,
'StampDuty' as Field08Name,
wca.divpy.StampDuty as Field08,
'StampAsPercent' as Field09Name,
wca.divpy.StampAsPercent as Field09,
'ComFee' as Field10Name,
wca.divpy.ComFee as Field10,
'ComAsPercent' as Field11Name,
wca.divpy.ComAsPercent as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.drip on vtab.eventid=wca.drip.divid
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd and wca.drip.cntrycd=wca.exchg.cntrycd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.divpy on vtab.divid=wca.divpy.divid and wca.drip.cntrycd=substring(wca.divpy.curencd,1,2)
left outer join wca.event on 'DRIP'=wca.event.eventtype
Where
wca.scexh.Actflag<>'D'
and wca.drip.cntrycd<>'US'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.drip.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or wca.divpy.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));

insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       wca.frank.frankid) as EvtRecordID,
case when vtab.Actflag='C' or vtab.Actflag='D' then vtab.Actflag
     else wca.frank.Actflag
     end as Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.frank.acttime > @fromdate then wca.frank.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
wca.frank.AnnounceDate as EventCreateDT,
wca.event.eventtype as EventCD,
vtab.EventID,
case when vtab.Marker='ISC' and vtab.Marker='CG' and vtab.Marker='CGL' and vtab.Marker='CGS' 
     then vtab.Marker
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL 
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.Paydate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.Paydate
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.FYEDate as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
vtab.Declarationdate as Date1,
'Period End Date' as Date2Name,
vtab.Periodenddate  as Date2,
'Tax Year End Date' as Date3Name,
wca.frank.TaxYearendDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
'GBP' as CurenCD,
'PID' as Rate1Name,
wca.frank.PID as Rate1,
'NonPID' as Rate2Name,
wca.frank.NonPID as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
vtab.Marker as Field01,
'Frequency' as Field02Name,
vtab.Frequency as Field02,
'TbaFlag' as Field03Name,
vtab.tbaflag as Field03,
'NilDividend' as Field04Name,
vtab.NilDividend as Field04,
'DivRescind' as Field05Name,
vtab.DivRescind as Field05,
'DeclCurenCD' as Field06Name,
vtab.DeclCurenCD as Field06,
'DeclGrossAmt' as Field07Name,
vtab.DeclGrossAmt as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.frank on vtab.divid=wca.frank.divid
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd and wca.frank.cntrycd=wca.exchg.cntrycd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.event on 'PID'=wca.event.eventtype
Where
wca.scexh.Actflag<>'D'
and wca.frank.cntrycd='GB'
and (wca.frank.pid<>'' or wca.frank.nonpid<>'')
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.frank.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));

insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       wca.frank.frankid) as EvtRecordID,
case when vtab.Actflag='C' or vtab.Actflag='D' then vtab.Actflag
     else wca.frank.Actflag
     end as Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.frank.acttime > @fromdate then wca.frank.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
wca.frank.AnnounceDate as EventCreateDT,
wca.event.eventtype as EventCD,
vtab.EventID,
case when vtab.Marker='ISC' and vtab.Marker='CG' and vtab.Marker='CGL' and vtab.Marker='CGS' 
     then vtab.Marker
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL 
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.Paydate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.Paydate
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.FYEDate as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
vtab.Declarationdate as Date1,
'Period End Date' as Date2Name,
vtab.Periodenddate  as Date2,
'Tax Year End Date' as Date3Name,
wca.frank.TaxYearendDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
wca.cntry.curencd as CurenCD,
'Frankdiv' as Rate1Name,
wca.frank.frankdiv as Rate1,
'UNFrankdiv' as Rate2Name,
wca.frank.unfrankdiv as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
vtab.Marker as Field01,
'Frequency' as Field02Name,
vtab.Frequency as Field02,
'TbaFlag' as Field03Name,
vtab.tbaflag as Field03,
'NilDividend' as Field04Name,
vtab.NilDividend as Field04,
'DivRescind' as Field05Name,
vtab.DivRescind as Field05,
'DeclCurenCD' as Field06Name,
vtab.DeclCurenCD as Field06,
'DeclGrossAmt' as Field07Name,
vtab.DeclGrossAmt as Field07,
'Frankflag' as Field08Name,
wca.frank.frankflag as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.frank on vtab.divid=wca.frank.divid
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd and wca.frank.cntrycd=wca.exchg.cntrycd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.event on 'FRANK'=wca.event.eventtype
left outer join wca.cntry on wca.frank.cntrycd=wca.cntry.cntrycd
Where
wca.scexh.Actflag<>'D'
and wca.frank.frankflag<>'N'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.frank.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       vtab.eventid) as EvtRecordID,
vtab.Actflag,
vtab.Acttime as EventChangeDT,
vtab.AnnounceDate as EventCreateDT,
vtab.EventCD,
vtab.EventID,
'' as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
null as RdID,
null as Priority,
'End Date' as PrimaryDateName,
vtab.EndDate as PrimaryDT,
vtab.StartDate as EffectiveDT,
null as ExDT,
null as RecordDT,
null as PayDT,
null as DuebillsRedemDT,
null as FromDT,
null as ToDT,
null as RegistrationDT,
vtab.Startdate as StartDT,
vtab.Enddate as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDT,
null as WithdrawalToDT,
null as NotificationDT,
null as FinancialYearEndDT,
null as ExpCompletionDT,
'' as Date1Name,
null as Date1,
'' as Date2Name,
null as Date2,
'' as Date3Name,
null as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
'' as Paytype,
null as OptionID,
null as SerialID,
'' as DefaultOpt,
''as RateCurenCD,
'' as Rate1Name,
'' as Rate1,
'' as Rate2Name,
'' as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'' as Field01Name,
'' as Field01,
'' as Field02Name,
'' as Field02,
'' as Field03Name,
'' as Field03,
'' as Field04Name,
'' as Field04,
'' as Field05Name,
'' as Field05,
'' as Field06Name,
'' as Field06,
'' as Field07Name,
'' as Field07,
'' as Field08Name,
'' as Field08,
'' as Field09Name,
'' as Field09,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_ftt as vtab
left outer join wca.event on vtab.eventcd=wca.event.eventtype
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
                    and vtab.cntrycd=wca.exchg.cntrycd
Where
wca.scexh.Actflag<>'D'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or (vtab.enddate>=@fromdate and vtab.enddate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
insert ignore into wca2.e2020_EVT
select distinct
wca.scexh.ScexhID,
concat('4',
       substring(concat('00',wca.event.eventID),length(wca.event.eventid),3),
       wca.frank.frankid) as EvtRecordID,
case when vtab.Actflag='C' or vtab.Actflag='D' then vtab.Actflag
     else wca.frank.Actflag
     end as Actflag,
case when vtab.acttime > @fromdate then vtab.acttime 
     when wca.frank.acttime > @fromdate then wca.divpy.acttime
     when wca.divpy.acttime > @fromdate then wca.frank.acttime
     when wca.rd.acttime > @fromdate then wca.rd.acttime
     when wca.rdprt.acttime > @fromdate then wca.rd.acttime
     when wca.exdt.acttime > @fromdate then wca.exdt.acttime
     when pexdt.acttime > @fromdate then pexdt.acttime
     else vtab.acttime
     end as EventChangeDT,
wca.frank.AnnounceDate as EventCreateDT,
wca.event.eventtype as EventCD,
vtab.EventID,
case when vtab.Marker='ISC' and vtab.Marker='CG' and vtab.Marker='CGL' and vtab.Marker='CGS' 
     then vtab.Marker
     else ''
     end as EventSubtype,
'' as RelatedEventCD,
null as RelatedEventID,
'M' as MandVoluFlag,
wca.rd.RdID,
wca.rdprt.Priority as RdPriority,
'Ex Date' as PrimaryDateName,
CASE WHEN wca.exdt.rdid IS NOT NULL 
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as PrimaryDT,
null as EffectiveDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as ExDT,
rd.Recdate as RecordDT,
CASE WHEN wca.exdt.rdid IS NOT NULL
     THEN wca.exdt.Paydate
     WHEN pexdt.rdid IS NOT NULL
     THEN pexdt.Paydate
     ELSE pexdt.Paydate END as PayDT,
null as DuebillsRedemDT,
rd.Recdate as FromDT,
rd.Todate as ToDT,
rd.RegistrationDate as RegistrationDT,
null as StartDT,
null as EndDT,
null as OpenDT,
null as CloseDT,
null as StartSubscriptionDT,
null as EndSubscriptionDT,
null as OptElectionDT,
null as WithdrawalFromDate,
null as WithdrawalToDate,
null as NotificationDT,
vtab.FYEDate as FinancialYearEndDT,
null as ExpCompletionDT,
'Declaration Date' as Date1Name,
vtab.Declarationdate as Date1,
'Period End Date' as Date2Name,
vtab.Periodenddate  as Date2,
'Tax Year End Date' as Date3Name,
wca.frank.TaxYearendDate as Date3,
'' as Date4Name,
null as Date4,
'' as Date5Name,
null as Date5,
'' as Date6Name,
null as Date6,
'' as Date7Name,
null as Date7,
'' as Date8Name,
null as Date8,
ifnull(wca.divpy.Divtype,'') as Paytype,
ifnull(wca.divpy.OptionID,1) as OptionID,
ifnull(wca.divpy.OptionSerialNo,1) as SerialID,
ifnull(wca.divpy.DefaultOpt,'') as DefaultOpt,
ifnull(wca.divpy.CurenCD,'') as RateCurenCD,
'GrossDividend' as Rate1Name,
ifnull(wca.divpy.GrossDividend,'') as Rate1,
'NetDividend' as Rate2Name,
ifnull(wca.divpy.NetDividend,'') as Rate2,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as ParvalueOld,
'' as ParvalueNew,
'' as OutturnType,
'' as OutSectyCD,
null as OutSecID,
'' as ResIsin,
'' as ResUsCode,
'' as ResLocalcode,
'' as ResSedol,
'' as ResBbgCompID,
'' as ResBbgCompTk,
'' as ResFigi,
'' as ResFigiTk,
'' as MinOfrQty,
'' as MaxOfrQty,
'' as MinQlyQty,
'' as MaxQlyQty,
'' as MinAcpQty,
'' as MaxAcpQty,
'' as TndrStrkPrice,
'' as TndrPriceStep,
'' as WithdrawalRights,
'' as OedExpTime,
'' as OedExpTimeZone,
'' as WrtExpTime,
'' as WrtExpTimeZone,
'' as ExpTime,
'' as ExpTimeZone,
'Marker' as Field01Name,
vtab.Marker as Field01,
'Frequency' as Field02Name,
vtab.Frequency as Field02,
'CapitalGain' as Field03Name,
wca.frank.CapitalGain as Field03,
'Under1250Section' as Field04Name,
wca.frank.Under1250Section as Field04,
'ReturnOfCapital' as Field05Name,
wca.frank.ReturnOfCapital as Field05,
'DivAllocTaxYear' as Field06Name,
wca.frank.DivAllocTaxYear as Field06,
'IncomeDivs' as Field07Name,
wca.frank.IncomeDivs as Field07,
'QualIncomeDivs' as Field08Name,
wca.frank.QualIncomeDivs as Field08,
'STCapGains' as Field09Name,
wca.frank.STCapGains as Field09,
'LTCapGains' as Field10Name,
wca.frank.LTCapGains as Field10,
'TotalCapGains' as Field11Name,
wca.frank.TotalCapGains as Field11,
'QualSTCapGains' as Field12Name,
wca.frank.QualSTCapGains as Field12,
'QualLTCapGains' as Field13Name,
wca.frank.QualLTCapGains as Field13,
'QualTotalCapGains' as Field14Name,
wca.frank.QualTotalCapGains as Field14,
'ForeignTaxPaid' as Field15Name,
wca.frank.ForeignTaxPaid as Field15,
'QualForeignTaxPaid' as Field16Name,
wca.frank.QualForeignTaxPaid as Field16,
'Section1202Gain' as Field17Name,
wca.frank.Section1202Gain as Field17,
'Collec28PctGain' as Field18Name,
wca.frank.Collec28PctGain as Field18,
'CashLiqDist' as Field19Name,
wca.frank.CashLiqDist as Field19,
'NoncashLiqDist' as Field20Name,
wca.frank.NoncashLiqDist as Field20,
'ExemptInterestDivs' as Field21Name,
wca.frank.ExemptInterestDivs as Field21,
'PercentageOfAMT' as Field22Name,
wca.frank.PercentageOfAMT as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.frank on vtab.divid=wca.frank.divid
inner join wca.rd on vtab.rdid = wca.rd.rdid
            and 'D' <> wca.rd.Actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
            and 'D' <> wca.scexh.Actflag
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd and wca.frank.cntrycd=wca.exchg.cntrycd
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd
            and vtab.eventcd = wca.exdt.eventtype
            and 'D' <> wca.exdt.Actflag
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd
            and vtab.eventcd = pexdt.eventtype
            and 'D' <> pexdt.Actflag
left outer join wca.event on 'DIVRC'=wca.event.eventtype
left outer join wca.cntry on wca.frank.cntrycd=wca.cntry.cntrycd
Where
wca.scexh.Actflag<>'D'
and wca.frank.cntrycd='US'
and wca.divpy.curencd='USD'
and wca.divpy.divtype='C'
and wca.frank.frankflag='N'
and (ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(vtab.acttime, interval 30 day)
     and (ifnull(wca.scexh.delistdate,wca.scexh.acttime)>date_add(vtab.announcedate, interval 30 day)
          or wca.scexh.ListStatus<>'D'))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='MF' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                                 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (vtab.acttime>=@fromdate 
     or wca.frank.acttime>=@fromdate 
     or wca.divpy.acttime>=@fromdate 
     or wca.exdt.acttime>=@fromdate 
     or pexdt.acttime>=@fromdate 
     or wca.rd.acttime>=@fromdate 
     or wca.rdprt.acttime>=@fromdate 
     or (wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (pexdt.exdate>=@fromdate and pexdt.exdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (wca.rd.recdate>=@fromdate and wca.rd.recdate<=@caldate and vtab.Actflag<>'D' and vtab.Actflag<>'C')
     or (vtab.acttime>@histdate and vtab.Actflag<>'D' and vtab.Actflag<>'C'
         and (wca.scmst.secid in (select secid from client.pfisin where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfsedol where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pffigi where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
              or wca.scmst.secid in (select secid from client.pfuscode where Actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid)))));
