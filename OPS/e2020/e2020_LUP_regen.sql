DROP TABLE IF EXISTS `wca2`.`e2020_LUP`;
CREATE TABLE `wca2`.`e2020_LUP` (
  `LupActflag` char(1) DEFAULT '',
  `LupChangeDT` datetime DEFAULT NULL,
  `TypeGroup` char(10) NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` varchar(70) NOT NULL,
  PRIMARY KEY (`Typegroup`,`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert ignore into wca2.e2020_LUP
SELECT
Actflag,
Acttime,
case when upper(TypeGroup) = 'LIQPRI' then 'SENJUN'
     else upper(TypeGroup)
     end as TypeGroup,
upper(Code) as Code,
lookup.Lookup
from wca.lookup
where
typegroup<>'PARTFINAL';
insert ignore into wca2.e2020_LUP
SELECT
lookupextra.Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
from wca.lookupextra
where
(typegroup<>'FRNINDXBEN' and typegroup<>'SENJUN' and typegroup <> 'EXSTYLE');
insert ignore into wca2.e2020_LUP
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
dvprd.dvprdcd,
dvprd.divperiod
from wca.dvprd;
insert ignore into wca2.e2020_LUP
SELECT
upper('I') as Actflag,
secty.acttime,
upper('SECTYPE') as TypeGroup,
secty.sectycd,
secty.securitydescriptor
from wca.secty;
insert ignore into wca2.e2020_LUP
SELECT
case when exchg.actflag='d' then 'c' else exchg.actflag end as actflag,
exchg.acttime,
upper('EXCHANGE') as TypeGroup,
exchg.exchgcd as code,
exchg.exchgname as lookup
from wca.exchg;
insert ignore into wca2.e2020_LUP
SELECT
case when exchg.actflag='d' then 'c' else exchg.actflag end as actflag,
exchg.acttime,
upper('MICCODE') as TypeGroup,
exchg.mic as code,
exchg.exchgname as lookup
from wca.exchg
where MIC <> '' and MIC is not null;
insert ignore into wca2.e2020_LUP
SELECT distinct
mktsg.Actflag,
mktsg.Acttime,
upper('MICSEG') as TypeGroup,
mktsg.MIC as Code,
mktsg.MKtSegment as Lookup
from wca.mktsg
inner join wca.scexh on wca.mktsg.mktsgid=wca.scexh.mktsgid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where mktsg.MIC <> '' and mktsg.MIC is not null and mktsg.MIC<>'MTAA'
and mktsg.Actflag<>'D';
insert ignore into wca2.e2020_LUP
SELECT
exchg.actflag,
exchg.acttime,
upper('MICMAP') as TypeGroup,
exchg.exchgcd as code,
exchg.mic as lookup
from wca.exchg;
insert ignore into wca2.e2020_LUP
SELECT
indus.actflag,
indus.acttime,
upper('INDUS') as TypeGroup,
indus.indusID as Code,
indus.indusname as lookup
from wca.indus;
insert ignore into wca2.e2020_LUP
SELECT
cntry.actflag,
cntry.acttime,
upper('CNTRY') as TypeGroup,
cntry.cntrycd as code,
cntry.country as lookup
from wca.cntry;
insert ignore into wca2.e2020_LUP
SELECT
curen.actflag,
curen.acttime,
upper('CUREN') as TypeGroup,
curen.curencd as code,
curen.currency as lookup
from wca.curen;
insert ignore into wca2.e2020_LUP
SELECT
case when event.Actflag<>'D' then 'I' else event.Actflag end as Actflag,
'2005/01/01 15:00:00' as Acttime,
upper('EVENT') as TypeGroup,
event.EventType as Code,
event.EventName as Lookup
from wca.event
where
event.EventName <> 'Agency Detail Change';
insert ignore into wca2.e2020_LUP
SELECT
case when lookupextra.Actflag<>'D' then 'I' else lookupextra.Actflag end as Actflag,
lookupextra.Acttime,
upper('EVENT') as TypeGroup,
lookupextra.code as Code,
lookupextra.lookup as Lookup
from wca.lookupextra
where
typegroup='TABLENAME'
and lookupextra.code<>'BOCHG'
and lookupextra.code<>'FRNFX'
and lookupextra.code<>'INCHG'
and lookupextra.code<>'SCCHG'
and lookupextra.code<>'SD'
and lookupextra.code<>'CRCHG'
and lookupextra.code<>'LSTAT'
and lookupextra.code<>'WKN';
insert ignore into wca2.e2020_LUP
SELECT
upper('I') as Actflag,
null as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from wca.sectygrp
where secgrpid = 2;
