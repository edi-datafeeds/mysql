DROP TABLE IF EXISTS `wca2`.`e2020_LST`;
CREATE TABLE `wca2`.`e2020_LST` (
  `LstRecordID` bigint(20) NOT NULL DEFAULT '0',
  `EvtLinkID` int(10) NOT NULL DEFAULT '0',
  `ListActFlag` char(1) NOT NULL DEFAULT '',
  `ListChangeDT` datetime DEFAULT NULL,
  `ListCreateDT` datetime DEFAULT NULL,
  `GlobalActiveFlag` char(1) NOT NULL DEFAULT '',
  `SecID` int(10) NOT NULL DEFAULT '0',
  `IssID` int(10) NOT NULL DEFAULT '0',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDescription` varchar(70) NOT NULL DEFAULT '',
  `Parvalue` varchar(20) NOT NULL DEFAULT '',
  `ParvalueCurenCD` char(3) NOT NULL DEFAULT '',
  `Isin` char(12) NOT NULL DEFAULT '',
  `IsinFlag` char(1) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `USCodeFlag` char(1) NOT NULL DEFAULT '',
  `SectyCD` char(3) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ListCntryCD` char(2) NOT NULL DEFAULT '',
  `RegisterCntryCD` char(2) NOT NULL DEFAULT '',
  `TradingCurenCD` char(3) NOT NULL DEFAULT '',
  `BbgCompID` char(12) NOT NULL DEFAULT '',
  `BbgCompTk` varchar(40) NOT NULL DEFAULT '',
  `BbgCompFlag` char(1) NOT NULL DEFAULT '',
  `Figi` char(12) NOT NULL DEFAULT '',
  `FigiTicker` varchar(40) NOT NULL DEFAULT '',
  `FigiFlag` char(1) NOT NULL DEFAULT '',
  `FigiGlobalShareclassID` char(12) NOT NULL DEFAULT '',
  `Sedol` char(7) NOT NULL DEFAULT '',
  `SedolFlag` char(1) NOT NULL DEFAULT '',
  `SedolMic` char(4) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `Mic` char(4) NOT NULL DEFAULT '',
  `LotSize` int(10) unsigned DEFAULT NULL,
  `MicSegmentName` varchar(50) NOT NULL DEFAULT '',
  `SegmentMic` char(6) NOT NULL DEFAULT '',
  `LocalCode` varchar(60) NOT NULL DEFAULT '',
  `LocalCodeFlag` char(1) NOT NULL DEFAULT '',
  `ListStatusCD` char(1) NOT NULL DEFAULT '',
  `ListDT` datetime DEFAULT NULL,
  `DelistDT` datetime DEFAULT NULL,
  PRIMARY KEY (`LstRecordID`),
  KEY `ix_EvtLink` (`EvtLinkID`),
  KEY `ix_ListChangeDT` (`ListChangeDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
use wca;
insert ignore into wca2.e2020_LST
select distinct
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when bbebnd.acttime > @fromdate then bbebnd.acttime
     when wca.scxtc.acttime > @fromdate then wca.scxtc.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag end as GlobalActiveFlag,
wca.scmst.SecID,
wca.scmst.IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.isin
     else wca.scxtc.Isin
     end as Isin,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as IsinFlag,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.uscode
     else ''
     end as USCode,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolbbg.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolbbg.curencd,'') as TradingCurenCD,
ifnull(sedolbbg.bbgcompid,'') as BbgCompID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompTk,
'M' as BbgCompFlag,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as Figi,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FigiTicker,
'M' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolbbg.Sedol,'') as Sedol,
'M' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
CASE when wca.scxtc.ScexhID is not null
     then wca.scxtc.LocalCode
     when wca.scexh.LocalCode='BPRAP'
     then wca.scexh.LocalCode
     when wca.scexh.exchgcd<>'USNYSE'
     then wca.scexh.LocalCode
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2),'PR','/PR'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),'/PR/CL')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3),'PR','/PR/'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4),'PR','/PR/')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-5),'/PR/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,1),'/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WS/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/WS')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/RT')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/RT')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and wca.scmst.securitydesc like '%Class A%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'A'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/A')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and wca.scmst.securitydesc like '%Class B%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'B'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/B')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WI')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WD')
     when wca.scmst.securitydesc like 'Unit%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'U'
          and wca.scmst.sectycd='STP'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/U')
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scxtc.ScexhID is null
     then 'M'
     else 'S'
     end as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now() or ifnull(wca.scxtc.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' 
                 or ifnull(wca.ipo.ipostatus,'')='NEW' 
                 or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
case when wca.scxtc.scxtcid is not null then wca.scxtc.listdate
     else wca.scexh.listdate
     end as ListDT,
case when wca.scxtc.scxtcid is not null then null
     else wca.scexh.delistdate
     end as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.ScexhID = wca.scxtc.ScexhID and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.exchg.cntrycd='US';
insert ignore into wca2.e2020_LST
select distinct
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when bbebnd.acttime > @fromdate then bbebnd.acttime
     when wca.scxtc.acttime > @fromdate then wca.scxtc.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.isin
     else wca.scxtc.Isin
     end as Isin,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as IsinFlag,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.uscode
     else ''
     end as USCode,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolbbg.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolbbg.curencd,'') as TradingCurenCD,
ifnull(sedolbbg.bbgcompid,'') as BbgCompID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompTk,
'M' as BbgCompFlag,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as Figi,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FigiTicker,
'M' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolbbg.Sedol,'') as Sedol,
'M' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
case when wca.scxtc.ScexhID is not null
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scxtc.ScexhID is null
     then 'M'
     else 'S'
     end as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.ScexhID = wca.scxtc.ScexhID and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.exchg.cntrycd='DE';
insert ignore into wca2.e2020_LST
select
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when bbebnd.acttime > @fromdate then bbebnd.acttime
     when wca.scxtc.acttime > @fromdate then wca.scxtc.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag end as GlobalActiveFlag,
wca.scmst.SecID,
wca.scmst.IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.isin
     else wca.scxtc.Isin
     end as Isin,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as IsinFlag,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.uscode
     else ''
     end as USCode,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolbbg.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolbbg.curencd,'') as TradingCurenCD,
ifnull(sedolbbg.bbgcompid,'') as BbgCompID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompTk,
'M' as BbgCompFlag,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as Figi,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FigiTicker,
'M' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolbbg.Sedol,'') as Sedol,
'M' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
case when ifnull(gbp04.TIDM,'')<>'' and ifnull(sedolbbg.sedoldefunct,'')<>'T' and ifnull(smf4.security.statusflag,'')='T'
     then replace(gbp04.TIDM,'.L','')
     when ifnull(wca.scxtc.localCode,'')<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scxtc.localcode<>'' or ifnull(gbp04.TIDM,'')<>'' and  replace(gbp04.TIDM,'.L','')<>wca.scexh.localcode
     then 'S'
     else 'M'
     end as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now() or ifnull(wca.scxtc.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' 
                 or ifnull(wca.ipo.ipostatus,'')='NEW' 
                 or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
case when wca.scxtc.scxtcid is not null then wca.scxtc.listdate
     else wca.scexh.listdate
     end as ListDT,
case when wca.scxtc.scxtcid is not null then null
     else wca.scexh.delistdate
     end as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.ScexhID = wca.scxtc.ScexhID and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.scexh.exchgcd='GBLSE';
insert ignore into wca2.e2020_LST
select distinct
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when bbebnd.acttime > @fromdate then bbebnd.acttime
     when wca.scxtc.acttime > @fromdate then wca.scxtc.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.isin
     else wca.scxtc.Isin
     end as Isin,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as IsinFlag,
case when ifnull(wca.scxtc.isin,'')=''
     then wca.scmst.uscode
     else ''
     end as USCode,
case when ifnull(wca.scxtc.isin,'')=''
     then 'M'
     else 'S'
     end as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolbbg.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolbbg.curencd,'') as TradingCurenCD,
ifnull(sedolbbg.bbgcompid,'') as BbgCompID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompTk,
'M' as BbgCompFlag,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as Figi,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FigiTicker,
'M' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolbbg.Sedol,'') as Sedol,
'M' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
case when wca.scxtc.ScexhID is not null
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scxtc.ScexhID is null
     then 'M'
     else 'S'
     end as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.ScexhID = wca.scxtc.ScexhID and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.exchg.cntrycd<>'DE'
and wca.exchg.cntrycd<>'US'
and wca.scexh.exchgcd<>'GBLSE'
and wca.scexh.exchgcd<>'CNSGHK'
and wca.scexh.exchgcd<>'CNSZHK'
and wca.scexh.exchgcd<>'HKHKSG'
and wca.scexh.exchgcd<>'HKHKSZ';
insert ignore into wca2.e2020_LST
select distinct
concat('1111',wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
wca.scmst.Isin,
'M' as IsinFlag,
wca.scmst.uscode,
'M' as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolseq.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolseq.curencd,'') as TradingCurenCD,
ifnull(bbcseq.bbgcompid,'') as BbgCompID,
ifnull(bbcseq.bbgcomptk,'') as BbgCompTk,
'C' as BbgCompFlag,
ifnull(bbeseq.bbgexhid,'') as Figi,
ifnull(bbeseq.bbgexhtk,'') as FigiTicker,
'C' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolseq.Sedol,'') as Sedol,
'C' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
wca.scexh.LocalCode,
'C' as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe as bbeseq on wca.scexh.secid=bbeseq.secid and wca.scexh.exchgcd=ifnull(bbeseq.exchgcd,'') and 'D'<>bbeseq.actflag
left outer join wca.bbc as bbcseq on bbeseq.secid=bbcseq.secid and 'XG'=ifnull(bbcseq.cntrycd,'') and 'D'<>bbcseq.actflag
left outer join wca_other.sedol as sedolseq on wca.scexh.secid=sedolseq.secid and 'CN'=sedolseq.cntrycd and 'XH'=sedolseq.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join smf4.security on sedolseq.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.scexh.exchgcd='CNSGHK';
insert ignore into wca2.e2020_LST
select distinct
concat('1111',wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
wca.scmst.Isin,
'M' as IsinFlag,
wca.scmst.uscode,
'M' as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolseq.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolseq.curencd,'') as TradingCurenCD,
ifnull(bbcseq.bbgcompid,'') as BbgCompID,
ifnull(bbcseq.bbgcomptk,'') as BbgCompTk,
'C' as BbgCompFlag,
ifnull(bbeseq.bbgexhid,'') as Figi,
ifnull(bbeseq.bbgexhtk,'') as FigiTicker,
'C' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolseq.Sedol,'') as Sedol,
'C' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
wca.scexh.LocalCode,
'C' as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe as bbeseq on wca.scexh.secid=bbeseq.secid and wca.scexh.exchgcd=ifnull(bbeseq.exchgcd,'') and 'D'<>bbeseq.actflag
left outer join wca.bbc as bbcseq on bbeseq.secid=bbcseq.secid and 'XZ'=ifnull(bbcseq.cntrycd,'') and 'D'<>bbcseq.actflag
left outer join wca_other.sedol as sedolseq on wca.scexh.secid=sedolseq.secid and 'CN'=sedolseq.cntrycd and 'XH'=sedolseq.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join smf4.security on sedolseq.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.scexh.exchgcd='CNSZHK';
insert ignore into wca2.e2020_LST
select distinct
concat('1111',wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
wca.scmst.Isin,
'M' as IsinFlag,
wca.scmst.uscode,
'M' as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolseq.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolseq.curencd,'') as TradingCurenCD,
ifnull(bbcseq.bbgcompid,'') as BbgCompID,
ifnull(bbcseq.bbgcomptk,'') as BbgCompTk,
'C' as BbgCompFlag,
ifnull(bbeseq.bbgexhid,'') as Figi,
ifnull(bbeseq.bbgexhtk,'') as FigiTicker,
'C' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolseq.Sedol,'') as Sedol,
'C' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
wca.scexh.LocalCode,
'C' as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe as bbeseq on wca.scexh.secid=bbeseq.secid and wca.scexh.exchgcd=ifnull(bbeseq.exchgcd,'') and 'D'<>bbeseq.actflag
left outer join wca.bbc as bbcseq on bbeseq.secid=bbcseq.secid and 'XH'=ifnull(bbcseq.cntrycd,'') and 'D'<>bbcseq.actflag
left outer join wca_other.sedol as sedolseq on wca.scexh.secid=sedolseq.secid and 'HK'=sedolseq.cntrycd and 'XZ'=sedolseq.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join smf4.security on sedolseq.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.scexh.exchgcd='HKHKSZ';
insert ignore into wca2.e2020_LST
select distinct
concat('1111',wca.scexh.ScexhID) as LstRecordID,
wca.scexh.ScexhID,
case when exchg.actflag = 'D' then 'D'
     when scmst.actflag = 'D' then 'D'
     when scexh.actflag = 'D' then 'D'
     else scexh.actflag end as LstActFlag,
case when wca.scexh.acttime > @fromdate then wca.scexh.acttime
     when wca.scmst.acttime > @fromdate then wca.scmst.acttime
     when wca.issur.acttime > @fromdate then wca.issur.acttime
     when wca.exchg.acttime > @fromdate then wca.exchg.acttime
     when sedolseq.acttime > @fromdate then sedolseq.acttime
     when bbeseq.acttime > @fromdate then bbeseq.acttime
     when bbcseq.acttime > @fromdate then bbcseq.acttime
     when wca.ipo.acttime > @fromdate then wca.ipo.acttime
     when wca.mktsg.acttime > @fromdate then wca.mktsg.acttime
     when wca.fggsc.acttime > @fromdate then wca.fggsc.acttime
     when smf4.security.actdate > @fromdate then smf4.security.actdate
     else wca.scexh.acttime
     end as ListChangeDT,
wca.scexh.AnnounceDate as ListCreateDT,
case when wca.scmst.Statusflag = '' then 'A'
     else wca.scmst.Statusflag
     end as GlobalActiveFlag,
wca.scmst.SecID as SecID,
wca.scmst.IssID as IssID,
wca.issur.IssuerName,
wca.scmst.SecurityDesc as SecurityDescription,
wca.scmst.Parvalue,
wca.scmst.CurenCD as ParvalueCurenCD,
wca.scmst.Isin,
'M' as IsinFlag,
wca.scmst.uscode,
'M' as USCodeFlag,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD as PrimaryExchgCD,
wca.exchg.CntryCD as ListCntryCD,
ifnull(sedolseq.RcntryCD,'') as RegisterCntryCD,
ifnull(sedolseq.curencd,'') as TradingCurenCD,
ifnull(bbcseq.bbgcompid,'') as BbgCompID,
ifnull(bbcseq.bbgcomptk,'') as BbgCompTk,
'C' as BbgCompFlag,
ifnull(bbeseq.bbgexhid,'') as Figi,
ifnull(bbeseq.bbgexhtk,'') as FigiTicker,
'C' as FigiFlag,
ifnull(wca.fggsc.FigiGlbShrClsID,'') as FigiGlobalShareclassID,
ifnull(sedolseq.Sedol,'') as Sedol,
'C' as SedolFlag,
ifnull(smf4.security.OPOL,'') as SedolMic,
wca.scexh.ExchgCD,
ifnull(wca.exchg.Mic,'') as Mic,
wca.scexh.lot as LotSize,
ifnull(wca.mktsg.mktsegment,'') as MicSegmentName,
ifnull(wca.mktsg.mic,'') as SegmentMic,
wca.scexh.LocalCode,
'C' as LocalCodeFlag,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'L'
     when wca.scexh.ListStatus='S' then 'S'
     when wca.scexh.ListStatus='D' then 'D'
     else ''
     end as ListStatusCD,
wca.scexh.listdate as ListDT,
wca.scexh.delistdate as DelistDT
from wca.scexh
inner join wca2.e2020_EVT on wca.scexh.ScexhID=wca2.e2020_EVT.LstLinkID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe as bbeseq on wca.scexh.secid=bbeseq.secid and wca.scexh.exchgcd=ifnull(bbeseq.exchgcd,'') and 'D'<>bbeseq.actflag
left outer join wca.bbc as bbcseq on bbeseq.secid=bbcseq.secid and 'XH'=ifnull(bbcseq.cntrycd,'') and 'D'<>bbcseq.actflag
left outer join wca_other.sedol as sedolseq on wca.scexh.secid=sedolseq.secid and 'HK'=sedolseq.cntrycd and 'XG'=sedolseq.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join smf4.security on sedolseq.sedol=smf4.security.sedol
left outer join wca.fggsc on wca.scmst.secid = wca.fggsc.secid
where
wca.scexh.exchgcd='HKHKSG';
