use wca2;
drop table if exists e2020_bbg;
CREATE TABLE `e2020_bbg` (
  `Acttime` datetime DEFAULT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `CurenCD` char(3) DEFAULT '',
  `CurenSrcFlag` char(1) DEFAULT '',
  `ListStatus` char(1) DEFAULT '',
  `Delistdate` datetime DEFAULT NULL,
  `Localcode` varchar(50) DEFAULT '',
  `BBGCompID` char(12) NOT NULL DEFAULT '',
  `BBGCompTk` varchar(40) DEFAULT '',
  `BBGExhID` char(12) NOT NULL DEFAULT '',
  `BBGExhTk` varchar(40) DEFAULT '',
  `BbcID` int(10) NOT NULL DEFAULT '0',
  `BbeID` int(10) NOT NULL DEFAULT '0',
  `AnnounceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`BbeID`),
  UNIQUE KEY `ix_unique` (`SecID`,`CntryCD`,`ExchgCD`,`CurenCD`),
  KEY `ix_bbc_bbg` (`BbcID`),
  KEY `ix_cntrycd_bbg` (`CntryCD`),
  KEY `ix_acttime_bbg` (`Acttime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
insert ignore into wca2.e2020_bbg
select
case when be.acttime > @fromdate then be.acttime
     when bc.acttime > @fromdate then bc.acttime
     when sc.acttime > @fromdate then sc.acttime
     else bc.acttime
     end as acttime,
be.Actflag,
be.SecID,
bc.CntryCD,
be.ExchgCD,
case when ifnull(bc.curencd,'')<>''
     then bc.curencd
     when be.curencd<>''
     then be.curencd
     else sm.curencd
     end as CurenCD,
case when ifnull(bc.curencd,'')=be.curencd and be.curencd<>''
     then 'M'
     when bc.curencd<>''
     then 'C'
     when be.curencd<>''
     then 'E'
     else 'P'
     end as CurenSrcFlag,
case when sc.Liststatus='D' or sc.Liststatus='S'
     then sc.Liststatus
     else 'L'
     end as ListStatus,
sc.Delistdate,
sc.Localcode,
bc.BBGCompID,
bc.BBGCompTk,
be.BBGExhID,
be.BBGExhTk,
bc.BbcID,
be.BbeID,
be.AnnounceDate
from wca.bbe as be
inner join wca.bbc as bc on be.secid=bc.secid and substring(be.exchgcd,1,2)=bc.cntrycd
       and substring(bc.bbgcomptk,1,instr(concat(bc.bbgcomptk,' '),' ')-1)
          =substring(be.bbgexhtk,1,instr(concat(be.bbgexhtk,' '),' ')-1)
inner join wca.scexh as sc on be.secid=sc.secid and be.exchgcd=sc.exchgcd
inner join wca.scmst as sm on be.secid=sm.secid
where
sm.sectycd<>'CW'
and be.exchgcd<>''
and be.exchgcd<>'HKHKSG'
and be.exchgcd<>'HKHKSZ'
and be.exchgcd<>'CNSGHK'
and be.exchgcd<>'CNSZHK'
and sm.actflag<>'D'
and sc.actflag<>'D'
and be.actflag<>'D'
and bc.actflag<>'D'
order by secid;
insert ignore into wca2.e2020_bbg
select
case when be.acttime > @fromdate then be.acttime
     when bc.acttime > @fromdate then bc.acttime
     when sc.acttime > @fromdate then sc.acttime
     else bc.acttime
     end as acttime,
be.Actflag,
be.SecID,
bc.CntryCD,
be.ExchgCD,
case when ifnull(bc.curencd,'')<>''
     then bc.curencd
     when be.curencd<>''
     then be.curencd
     else sm.curencd
     end as CurenCD,
case when ifnull(bc.curencd,'')=be.curencd and be.curencd<>''
     then 'M'
     when bc.curencd<>''
     then 'C'
     when be.curencd<>''
     then 'E'
     else 'P'
     end as CurenSrcFlag,
case when sc.Liststatus='D' or sc.Liststatus='S'
     then sc.Liststatus
     else 'L'
     end as ListStatus,
sc.Delistdate,
sc.Localcode,
bc.BBGCompID,
bc.BBGCompTk,
be.BBGExhID,
be.BBGExhTk,
bc.BbcID,
be.BbeID,
be.AnnounceDate
from wca.bbe as be
inner join wca.bbc as bc on be.secid=bc.secid and substring(be.exchgcd,1,2)=bc.cntrycd
       and substring(bc.bbgcomptk,1,instr(concat(bc.bbgcomptk,' '),' ')-1)
          =substring(be.bbgexhtk,1,instr(concat(be.bbgexhtk,' '),' ')-1)
inner join wca.scexh as sc on be.secid=sc.secid and be.exchgcd=sc.exchgcd
inner join wca.scmst as sm on be.secid=sm.secid
where
sm.sectycd<>'CW'
and be.exchgcd<>''
and be.exchgcd<>'HKHKSG'
and be.exchgcd<>'HKHKSZ'
and be.exchgcd<>'CNSGHK'
and be.exchgcd<>'CNSZHK'
and sm.actflag<>'D'
and sc.actflag<>'D'
and be.actflag<>'D'
and bc.actflag<>'D'
order by secid;
