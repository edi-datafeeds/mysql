use wca2;
drop table if exists e2020_sedol;
CREATE TABLE `e2020_sedol` (
  `Acttime` datetime DEFAULT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `CntryCD` char(2) DEFAULT NULL,
  `Sedol` char(7) DEFAULT NULL,
  `Defunct` char(1) DEFAULT NULL,
  `RcntryCD` char(2) DEFAULT NULL,
  `SedolID` int(10) NOT NULL DEFAULT '0',
  `CurenCD` char(3) DEFAULT '',
  `CurenSrcFlag` char(1) DEFAULT '',
  `Opol` char(4) DEFAULT '',
  `AnnounceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SedolId`),
  UNIQUE KEY `IX_Unique` (`SecID`,`CntryCD`,`RcntryCD`,`CurenCD`),
  KEY `ix_acttime_SEDOL` (`Acttime`),
  KEY `IX_SEDOL` (`Sedol`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
use wca;
set @fromdate=(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
where seq<>0
order by acttime desc
limit 2, 1);
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when gbraw.currency='GBX'
     then 'GBP'
     else gbraw.currency
     end as CurenCD,
'G' as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join prices.GBLSE_Raw_Daily_Prices as gbraw on sed.sedol=gbraw.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
inner join wca.scexh as sce on scm.secid=sce.secid and sed.cntrycd=substring(sce.exchgcd,1,2)
where
sed.actflag<>'D'
and sed.actflag<>'D'
and scm.actflag<>'D'
and sce.actflag<>'D'
and lse.actflag<>'D'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd='GB';
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     else scx.curencd
     end as curencd,
case when sed.curencd<>''
     then 'S'
     else 'L'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
inner join wca.scexh as sce on scm.secid=sce.secid and sed.cntrycd=substring(sce.exchgcd,1,2)
inner join wca.scxtc as scx on sce.scexhid=scx.scexhid
where
sed.actflag<>'D'
and scx.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and (ifnull(lse.unitofqcurrcode,'')=scx.curencd or ifnull(sed.curencd,'')=scx.curencd)
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ'
and sed.rcntrycd<>'XG'
and sed.rcntrycd<>'XH'
and sed.rcntrycd<>'XZ';
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
scx.curencd,
'X' as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
inner join wca.scexh as sce on scm.secid=sce.secid and sed.cntrycd=substring(sce.exchgcd,1,2)
inner join wca.scxtc as scx on sce.scexhid=scx.scexhid
where
sed.actflag<>'D'
and scx.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and ifnull(lse.unitofqcurrcode,'')=''
and ifnull(sed.curencd,'')=''
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ'
and sed.rcntrycd<>'XG'
and sed.rcntrycd<>'XH'
and sed.rcntrycd<>'XZ';
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'USD'
     when sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'H'
     when sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then 'P'
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
left outer join wca.scexh as sce on scm.secid=sce.secid 
        and sed.cntrycd=substring(sce.exchgcd,1,2)
        and sed.rcntrycd<>'XH'
        and sed.rcntrycd<>'XG'
        and sed.rcntrycd<>'XZ'
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and (substring(ifnull(sce.exchgcd,'BND'),3,3)<>'BND'
      or sed.rcntrycd='XH'      
      or sed.rcntrycd='XG'      
      or sed.rcntrycd='XZ')      
and lse.statusflag='T'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'USD'
     when sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'H'
     when sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then 'P'
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
left outer join wca.scexh as sce on scm.secid=sce.secid 
        and sed.cntrycd=substring(sce.exchgcd,1,2)
        and sed.rcntrycd<>'XH'
        and sed.rcntrycd<>'XG'
        and sed.rcntrycd<>'XZ'
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and (substring(ifnull(sce.exchgcd,'BND'),3,3)<>'BND'
      or sed.rcntrycd='XH'      
      or sed.rcntrycd='XG'      
      or sed.rcntrycd='XZ')      
and lse.statusflag='C'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
insert ignore into wca2.e2020_sedol
select
case when sed.acttime > @fromdate then sed.acttime
     when lse.actdate > @fromdate then lse.actdate
     when scm.acttime > @fromdate then scm.acttime
     else sed.acttime
     end as acttime,
sed.actflag,
sed.SecID,
sed.CntryCD,
sed.Sedol,
case when ifnull(lse.statusflag,'')='D'
     then 'T'
     when ifnull(scm.statusflag,'')='I'
     then 'T'
     else 'F'
     end as Defunct,
case when sed.RcntryCD<>''
     then sed.RcntryCD
     else lse.cregcode
     end as RcntryCD,
sed.SedolID,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'GBP'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'USD'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'USD'
     when sed.curencd<>''
     then sed.curencd
     when ifnull(lse.unitofqcurrcode,'')<>''
     then lse.unitofqcurrcode
     when sed.rcntrycd='XH'
     then 'CNY'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'HKD'
     when ifnull(scm.curencd,'')<>''
     then scm.curencd
     else 'XXX'
     end as CurenCD,
case when ifnull(lse.unitofqcurrcode,'')='GBX'
     then 'L'
     when ifnull(lse.unitofqcurrcode,'')='USX'
     then 'L'
     when sed.cntrycd='US' and sed.curencd<>'USD'
     then 'H'
     when sed.curencd<>''
     then 'S'
     when ifnull(lse.unitofqcurrcode,'')<>''
     then 'L'
     when sed.rcntrycd='XH'
     then 'H'
     when sed.rcntrycd='XG' or sed.rcntrycd='XZ'
     then 'H'
     when ifnull(scm.curencd,'')<>''
     then 'P'
     else 'H'
     end as CurenSrcFlag,
lse.Opol,
sed.AnnounceDate
from wca_other.sedol as sed
inner join smf4.security as lse on sed.sedol=lse.sedol
inner join wca.scmst as scm on sed.secid=scm.secid
left outer join wca.scexh as sce on scm.secid=sce.secid 
        and sed.cntrycd=substring(sce.exchgcd,1,2)
        and sed.rcntrycd<>'XH'
        and sed.rcntrycd<>'XG'
        and sed.rcntrycd<>'XZ'
where
sed.actflag<>'D'
and scm.actflag<>'D'
and lse.actflag<>'D'
and (substring(ifnull(sce.exchgcd,'BND'),3,3)<>'BND'
      or sed.rcntrycd='XH'      
      or sed.rcntrycd='XG'      
      or sed.rcntrycd='XZ')      
and lse.statusflag='D'
and scm.sectycd<>'CW'
and scm.sectycd<>'IDX'
and sed.cntrycd<>'ZZ';
