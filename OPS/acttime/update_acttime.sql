SET @newActtime = ( SELECT 
					CASE seq
						WHEN 0 THEN CONCAT( SUBSTRING_INDEX(feeddate, " ",1)," 04:00:00") -- "0"
						WHEN 1 THEN CONCAT( SUBSTRING_INDEX(feeddate, " ",1)," 04:05:00") -- "1"
						WHEN 2 THEN CONCAT( SUBSTRING_INDEX(feeddate, " ",1)," 14:30:00") -- "2"
						WHEN 3 THEN CONCAT( SUBSTRING_INDEX(feeddate, " ",1)," 18:30:00") -- "3"
						ELSE ""
					END AS LastWorkingDay
				FROM wca.tbl_opslog 
				ORDER BY acttime DESC
				LIMIT 1 );

SET @oldActtime = ( SELECT acttime FROM wca.tbl_opslog ORDER BY acttime DESC LIMIT 1);

UPDATE wca.agchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.agm SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.agncy SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.agydt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ann SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.arr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.assm SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.auct SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bb SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bbc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bbcc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bbe SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bbec SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bdc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bkrp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bndlq SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bndpa SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bochg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bon SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bond SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bondx SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.br SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bschg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bskcc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bskwc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bskwt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.bwchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.calcout SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.call_my SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.caprd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ch144 SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cikch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.clsac SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.clsag SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.clssc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.clsst SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cntr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cntrg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cntry SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.consd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.continent SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.conv SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.convt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cosnt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cowar SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cpopn SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cpopt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.crchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.crdrt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ctchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ctx SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.curen SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.currd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.cwchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.dist SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.div_my SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.divid_full SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.divpy SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.divpyid_full SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.dmrgr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.dprcp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.drchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.drip SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.dvprd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.dvst SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ent SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.etp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.event SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.exchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.exchgmap SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.exchl SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.exdt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ffc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.finch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.fpo SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.fraction SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.frank SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.frnfx SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ftran SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ftt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.fychg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.gicch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.hear SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.icc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ifchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.inchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.indef SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.indus SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.int_my SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.intbc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.intpy SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ipo SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.irchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ischg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.isscn SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.issdd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.issur SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lawst SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lcc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lei SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.leihr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.liq SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lk144 SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lookup SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lookupextra SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.lstat SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ltchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mifid SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mkchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mktsg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mpay SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mrgr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mtchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mubnd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.muiss SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mutxcr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.naich SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.nlist SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.oddlt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ordlk SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.pfds SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.po SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.prchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.prf SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.prftm SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.prvsc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.pvrd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rcap SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rconv SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rdnom SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rdprt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.redem SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.redmt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.region SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rochg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rtrac SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rts SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.rum SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sachg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scact SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scagy SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scexh SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scmst SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scswp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.scxtc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sdchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.secrc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.secty SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sedol SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.selrt SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sfund SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.shoch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.spcnd SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.spobs SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.spun SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.state SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.sxtch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.tbl_lookups SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.tbl_opslog SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.tkovr SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.tlelk SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.trchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.trnch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.uit SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.umchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.umprg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.umtrf SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.unstp SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mf SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.mfcon SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.warex SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.wartm SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.whtax SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.wknch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.wtchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.wxchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zbochg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zbschg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zcrchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zctchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zffc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zfrnfx SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zicc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zifchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zirchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zischg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zlcc SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zlstat SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zltchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zmtchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zrconv SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zrdnom SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zscchg SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.zshoch SET acttime = @newActtime WHERE acttime >= @oldActtime;
UPDATE wca.ztrchg SET acttime = @newActtime WHERE acttime >= @oldActtime;