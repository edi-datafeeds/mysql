--filepath=O:\Datafeed\Prices\missing_files\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Prices_Missing
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Prices\missing_files\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
SELECT distinct prices.markets.marketcode,prices.markets.description,
prices.markets.name,notifications.notificationlog.mktclosedate
FROM notifications.notificationlog
inner join prices.markets on notifications.notificationlog.providerid = prices.markets.id
                          and prices.markets.mktclosedate between '2012-11-23' and '2012-11-25'
where notifications.notificationlog.mktclosedate between '2012-11-23' and '2012-11-25'
and notifications.notificationlog.type = 'DEL'
order by marketcode