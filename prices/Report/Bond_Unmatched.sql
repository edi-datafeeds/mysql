--filepath=O:\Datafeed\Prices\Bonds_Unmatched\
--filenameprefix=
--filename=Bond_Unmatched
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
select distinct
MIC, 
LocalCode,
Isin, 
Currency,
MktCloseDate, 
Issuername,
SectyCD, 
SecurityDesc, 
Sedol,
UsCode, 
PrimaryExchgCD,
ExchgCD, 
Comment 
from prices.liveprices
where providerid in(select id from prices.markets where marketcode like '%FI')
and secid = 0

and isin not in(select isin FROM pipelinemaster.defunct)
and localcode not in(select commoncode FROM pipelinemaster.defunct)

and isin not in(select isin FROM pipelinemaster.ignored)
and localcode not in(select commoncode FROM pipelinemaster.ignored)


and isin not in(select isin FROM pipelinemaster.dnf)
and localcode not in(select commoncode FROM pipelinemaster.dnf)


and isin not in(select isin FROM pipelinemaster.nonprocessed)
and localcode not in(select commoncode FROM pipelinemaster.nonprocessed)

and isin not in(select isin FROM pipelinemaster.nonenglish)
and localcode not in(select commoncode FROM pipelinemaster.nonenglish)

and isin not in(select isin FROM wca.scmst)
and localcode not in(select x FROM wca.scmst)
and localcode not in(select localcode FROM wca.scexh)

and sectycd <> 'WAR'
order by MIC,LocalCode,Isin