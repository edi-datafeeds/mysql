--filepath=O:\Datafeed\Prices\MissingIDXNmList\
--filenameprefix=
--filename=MissingIDXNmList
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--#1
select prices.liveprices.exchgcd,prices.liveprices.localcode,prices.liveprices.isin, prices.liveprices.PriceDate from prices.liveprices
left outer join prices.markets on prices.liveprices.providerid = prices.markets.id
where prices.markets.marketcode like '%_IX'
and ( prices.liveprices.issuername is null or  prices.liveprices.issuername = '')
and prices.liveprices.PriceDate > DATE_SUB(now(), INTERVAL 1 month)
order by prices.liveprices.exchgcd