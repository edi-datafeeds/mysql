--filepath=O:\Datafeed\Prices\missing_files_new\
--filenameprefix=
--filename=yyyymmdd_new
--filenamealt=
--fileextension=.txt
--suffix=_Prices_Missing
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Prices\missing_files\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
SELECT prices.markets.marketcode,prices.markets.description,prices.markets.mic,
prices.markets.name,notifications.notificationlog.mktclosedate as FromDate,notifications.notificationlog.mktclosedate as ToDate
FROM notifications.notificationlog
inner join prices.markets on notifications.notificationlog.providerid = prices.markets.id
                          and prices.markets.mktclosedate = SUBDATE(CURDATE(),INTERVAL 2 DAY)
where notifications.notificationlog.mktclosedate = SUBDATE(CURDATE(),INTERVAL 1 DAY)
and notifications.notificationlog.type = 'DEL'
union
select
m.marketcode,m.description,m.mic,m.name,
date_add(l.mktclosedate,interval 1 day) as FromDate, date_add(min(fr.mktclosedate) ,interval -1 day) as ToDate
from prices.marketloadlog as l
left outer join prices.marketloadlog as r on l.mktclosedate = date_add(r.mktclosedate,interval -1 day)
               and l.providerid = r.providerid and
               date_add(l.mktclosedate,interval 1 day) >= date_add(now(),interval -7 day)
left outer join prices.marketloadlog as fr on l.mktclosedate < fr.mktclosedate
               and l.providerid = fr.providerid and
               date_add(fr.mktclosedate,interval 1 day) >= date_add(now(),interval -7 day)
 left outer join prices.markets as m on l.providerid = m.id
where r.mktclosedate is null and
fr.mktclosedate is not null and
date_add(l.mktclosedate,interval 1 day) >= date_add(now(),interval -7 day)
group by l.mktclosedate, r.mktclosedate;