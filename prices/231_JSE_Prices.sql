--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MKTCAP
--fileheadertext=_WFI_LastTrade
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--shownulls=n


--#1
select
client.pfisin.code as isin,
format(wca.scmst.sharesoutstanding,0) as sharesoutstanding,
exchgrates.liverates.rate as usd_exchange_rate,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
prices.lasttrade.mic,
prices.lasttrade.localcode,
prices.lasttrade.isin,
prices.lasttrade.currency,
prices.lasttrade.pricedate,
prices.lasttrade.open,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.close,
prices.lasttrade.mid,
prices.lasttrade.ask,
prices.lasttrade.last,
prices.lasttrade.bid,
prices.lasttrade.bidsize,
prices.lasttrade.asksize,
prices.lasttrade.tradedvolume,
prices.lasttrade.secid,
prices.lasttrade.mktclosedate,
prices.lasttrade.volflag,
prices.lasttrade.issuername,
prices.lasttrade.sectycd,
prices.lasttrade.securitydesc,
prices.lasttrade.sedol,
prices.lasttrade.uscode,
prices.lasttrade.primaryexchgcd,
prices.lasttrade.exchgcd,
prices.lasttrade.tradedvalue,
prices.lasttrade.totaltrades,
prices.lasttrade.comment
from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin and client.pfisin.accid=231 and client.pfisin.actflag<>'D'
inner join prices.lasttrade on wca.scmst.secid = prices.lasttrade.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
order by client.pfisin.code, exchgcd;
