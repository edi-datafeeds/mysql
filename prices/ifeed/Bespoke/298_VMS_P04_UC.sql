-- arp=n:\upload\acc\298\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_UC_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- hdt=
-- hpx=
-- hsx=_Prices_P04_UC

-- # 1
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
client.pfuscode.code as USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfuscode
left outer join prices.lasttrade on client.pfuscode.code=prices.lasttrade.uscode
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=298
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (prices.lasttrade.secid is null or prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc limit 1))
and (prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd is null)
and (prices.lasttrade.localcode not in (select localcode from prices.lasttrade where exchgcd='USOTCB' and pricedate>(select(adddate( prices.lasttrade.mktclosedate, INTERVAL -365 DAY )))))
and client.pfuscode.actflag<>'D'
and (comment not like 'terms%' or comment is null));
