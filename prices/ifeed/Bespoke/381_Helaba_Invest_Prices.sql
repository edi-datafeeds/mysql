-- arc=y
-- arp=n:\upload\acc\381\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Helaba_Invest_Prices
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_Prices_Helaba_Invest_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
where 
client.pfisin.accid = 381 and client.pfisin.actflag<>'D'
and (client.pfisin.secid=0
or (prices.lasttrade.secid is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency desc, pricedate desc limit 1))
and (prices.lasttrade.secid is null or prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency desc, pricedate desc limit 1)))
order by isin, pricedate desc;