-- arp=n:\upload\acc\892\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- hdt=
-- hpx=EDI_Prices_P04_
-- hsx=

-- # 1

select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
client.pfsedol.code as Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsedol
inner join prices.lasttrade on client.pfsedol.code=prices.lasttrade.sedol
where
client.pfsedol.accid=892
and client.pfsedol.actflag<>'D'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (comment not like 'terms%' or comment is null)
UNION
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
client.pfsedol.code as Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsedol
inner join wca.sedol on client.pfsedol.code=wca.sedol.sedol
inner join prices.lasttrade on wca.sedol.secid=prices.lasttrade.secid
                            and wca.sedol.cntrycd=substring(prices.lasttrade.exchgcd,1,2)
where
client.pfsedol.accid=892
and client.pfsedol.actflag<>'D'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and client.pfsedol.code not in (select ifnull(sedol,'xxxxxxx') from prices.lasttrade)
and (comment not like 'terms%' or comment is null)
UNION
select distinct
wca.exchg.MIC,
wca.scexh.LocalCode,
wca.scmst.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
wca.scmst.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
client.pfsedol.code as Sedol,
wca.scmst.uscode,
wca.scmst.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsedol
left outer join wca.sedol on client.pfsedol.code=wca.sedol.sedol
left outer join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.sedol.secid=prices.lasttrade.secid
                                 and wca.sedol.cntrycd=substring(prices.lasttrade.exchgcd,1,2)
where
client.pfsedol.accid=892
and client.pfsedol.actflag<>'D'
and client.pfsedol.code not in (select ifnull(sedol,'xxxxxxx') from prices.lasttrade
                                where (comment not like 'terms%' or comment is null))
and concat(client.pfsedol.secid, ifnull(wca.sedol.cntrycd,'xx')) 
not in (select concat(prices.lasttrade.secid, substring(prices.lasttrade.exchgcd,1,2)) 
        from prices.lasttrade where (comment not like 'terms%' or comment is null));


