-- arc=y
-- arp=n:\bespoke\trafix\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Trafix_EOD_Prices
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=Trafix_EOD_Prices_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from prices.lasttrade
inner join wca.sectygrp on prices.lasttrade.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
where 
prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 7 day))
and substring(prices.lasttrade.exchgcd,1,2)<>'US'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where 
isinlatest.secid=prices.lasttrade.secid
and isinlatest.currency=prices.lasttrade.currency
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 7 day))
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc limit 1);
