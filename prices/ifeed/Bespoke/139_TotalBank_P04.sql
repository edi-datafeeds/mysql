-- arc=y
-- arp=n:\bespoke\totalbank\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Prices_P04
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_Prices_P04_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select client.pday139.MIC,
client.pday139.pricefilesymbol as LocalCode,
client.pday139.Isin,
client.pday139.Currency,
client.pday139.PriceDate,
client.pday139.Open,
client.pday139.High,
client.pday139.Low,
client.pday139.Close,
client.pday139.Mid,
client.pday139.Ask,
client.pday139.Last,
client.pday139.Bid,
client.pday139.BidSize,
client.pday139.AskSize,
client.pday139.TradedVolume,
client.pday139.SecID,
client.pday139.MktCloseDate,
client.pday139.Volflag,
client.pday139.Issuername,
client.pday139.SectyCD,
client.pday139.SecurityDesc,
client.pday139.Sedol,
client.pday139.USCode,
client.pday139.PrimaryExchgCD,
client.pday139.ExchgCD,
client.pday139.TradedValue,
client.pday139.TotalTrades,
client.pday139.Comment
from client.pday139
left outer join wca.sectygrp on client.pday139.sectycd=wca.sectygrp.sectycd
where (pricedate is null
and ((3>wca.sectygrp.secgrpid or client.pday139.sectycd is null)
or (client.pday139.sectycd='MF')
or ((client.pday139.sectycd='BND' 
and (ord(substring(client.pday139.uscode,7,1)) between 48 and 57 and ord(substring(client.pday139.uscode,8,1)) between 48 and 57)))))
or (3>wca.sectygrp.secgrpid
or (client.pday139.sectycd='MF')
or ((client.pday139.sectycd='BND' 
and (ord(substring(client.pday139.uscode,7,1)) between 48 and 57 and ord(substring(client.pday139.uscode,8,1)) between 48 and 57)))
and pricedate is not null)
order by pricedate desc;