-- arc=y
-- arp=n:\bespoke\staticdata\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=EDI_Prices_P04_GB_
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_Prices_P04_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode AS PriceFileSymbol,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from prices.lasttrade
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
prices.lasttrade.sectycd='ETF'
and prices.lasttrade.mic='XLON'
AND prices.lasttrade.pricedate>=(select(DATE_SUB(max(acttime), interval 186 day)) from wca.tbl_opslog where seq = 3)
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
-- wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
-- and 
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and latest.currency=prices.lasttrade.currency
order by latest.pricedate desc limit 1))
order by pricedate desc;