-- arc=y
-- arp=n:\upload\acc\362\
-- ddt=
-- dfn=y
-- dfo=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_Prices_P04_
-- hsx=

-- # 1
select distinct
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
client.pfisinmic.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisinmic
left outer join prices.lasttrade on client.pfisinmic.code=prices.lasttrade.isin and client.pfisinmic.mic=prices.lasttrade.mic
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=362
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and client.pfisinmic.actflag<>'D'
and (comment not like 'terms%' or comment is null));
