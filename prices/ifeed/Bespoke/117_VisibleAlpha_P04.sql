-- arp=n:\upload\acc\117\
-- fsx=select concat('_Acc_117_P04_', (select lpad(max(counter), 5, "0")from client.acc_117_runlog))
-- hsx=select concat('_P04_', (select lpad(max(counter), 5, "0")from client.acc_117_runlog))
-- hdt=SELECT DATE_FORMAT((select max(fromdate)from client.acc_117_runlog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(fromdate)from client.acc_117_runlog), '%Y%m%d')
-- hpx=VisibleAlpha_
-- dfn=l
-- fty=y

-- # 1

select * from prices.lasttrade
where
isin in (select code from client.pfisin where accid = 117 and actflag <> 'D')
and exchgcd in 
(select exchgcd from prices.ltloadlog where acttime >= (select date_sub(max(fromdate), interval '5' minute) from client.acc_117_runlog))
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where 
isinlatest.isin=prices.lasttrade.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 31 day))
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, isinlatest.currency limit 1);
