-- arp=n:\upload\acc\305\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_NIKKEI300_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- hdt=
-- hpx=
-- hsx=_NIKKEI300_Prices_P04

-- # 1

select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
client.pfsecid.code as SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsecid
left outer join prices.lasttrade on client.pfsecid.code = prices.lasttrade.secid
where
prices.lasttrade.mic='XJPX'
AND ((prices.lasttrade.pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and client.pfsecid.accid=305
and client.pfsecid.actflag<>'D'
and (comment not like 'terms%' or comment is null));