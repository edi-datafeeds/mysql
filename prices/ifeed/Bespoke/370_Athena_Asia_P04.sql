-- arp=n:\upload\acc\370\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_Asia_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- hdt=
-- hpx=
-- hsx=_Prices_Asia_P04

-- # 1
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code=prices.lasttrade.isin
left outer join wca.continent on substring(prices.lasttrade.exchgcd,1,2) = wca.continent.cntrycd
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=370
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and client.pfisin.actflag<>'D'
and wca.continent.continent='Asia'
and (comment not like 'terms%' or comment is null));