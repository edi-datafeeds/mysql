-- arp=n:\upload\acc\303\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_P04
-- fex=.txt
-- dfn=L
-- fty=y
-- hdt=
-- hpx=
-- hsx=_Prices_P04

-- # 1

select distinct
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
client.pfisinmic.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisinmic
left outer join prices.lasttrade on concat(client.pfisinmic.code, client.pfisinmic.mic)=concat(prices.lasttrade.isin, prices.lasttrade.mic)
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=303
and client.pfisinmic.actflag<>'D'
and (comment not like 'terms%' or comment is null));