set @enddate = concat(year(now()),'-',month(now()),'-1');
set @startdate = DATE_ADD(@enddate,interval -1 MONTH);
set @iyear = year(@startdate);
set @imonth = month(@startdate);
select @startdate;
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'AGM' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.agm
inner join wca.issur on wca.agm.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.agm.acttime >= @startdate and wca.agm.acttime < @enddate
and wca.agm.actflag = 'i') as 'ict',
(select count(*) from wca.agm
inner join wca.issur on wca.agm.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.agm.acttime >= @startdate and wca.agm.acttime < @enddate
and wca.agm.actflag = 'u') as 'uct',
(select count(*) from wca.agm
inner join wca.issur on wca.agm.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.agm.acttime >= @startdate and wca.agm.acttime < @enddate
and wca.agm.actflag = 'c') as 'cct',
(select count(*) from wca.agm
inner join wca.issur on wca.agm.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.agm.acttime >= @startdate and wca.agm.acttime < @enddate
and wca.agm.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'BKRP' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.bkrp
inner join wca.issur on wca.bkrp.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bkrp.acttime >= @startdate and wca.bkrp.acttime < @enddate
and wca.bkrp.actflag = 'i') as 'ict',
(select count(*) from wca.bkrp
inner join wca.issur on wca.bkrp.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bkrp.acttime >= @startdate and wca.bkrp.acttime < @enddate
and wca.bkrp.actflag = 'u') as 'uct',
(select count(*) from wca.bkrp
inner join wca.issur on wca.bkrp.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bkrp.acttime >= @startdate and wca.bkrp.acttime < @enddate
and wca.bkrp.actflag = 'c') as 'cct',
(select count(*) from wca.bkrp
inner join wca.issur on wca.bkrp.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bkrp.acttime >= @startdate and wca.bkrp.acttime < @enddate
and wca.bkrp.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'BOND' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bond.acttime >= @startdate and wca.bond.acttime < @enddate
and wca.bond.actflag = 'i') as 'ict',
(select count(*) from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bond.acttime >= @startdate and wca.bond.acttime < @enddate
and wca.bond.actflag = 'u') as 'uct',
(select count(*) from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bond.acttime >= @startdate and wca.bond.acttime < @enddate
and wca.bond.actflag = 'c') as 'cct',
(select count(*) from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bond.acttime >= @startdate and wca.bond.acttime < @enddate
and wca.bond.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'BON' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bon.acttime >= @startdate and wca.bon.acttime < @enddate
and wca.bon.actflag = 'i') as 'ict',
(select count(*) from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bon.acttime >= @startdate and wca.bon.acttime < @enddate
and wca.bon.actflag = 'u') as 'uct',
(select count(*) from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bon.acttime >= @startdate and wca.bon.acttime < @enddate
and wca.bon.actflag = 'c') as 'cct',
(select count(*) from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bon.acttime >= @startdate and wca.bon.acttime < @enddate
and wca.bon.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'BB' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.bb
inner join wca.scmst on wca.bb.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bb.acttime >= @startdate and wca.bb.acttime < @enddate
and wca.bb.actflag = 'i') as 'ict',
(select count(*) from wca.bb
inner join wca.scmst on wca.bb.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bb.acttime >= @startdate and wca.bb.acttime < @enddate
and wca.bb.actflag = 'u') as 'uct',
(select count(*) from wca.bb
inner join wca.scmst on wca.bb.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bb.acttime >= @startdate and wca.bb.acttime < @enddate
and wca.bb.actflag = 'c') as 'cct',
(select count(*) from wca.bb
inner join wca.scmst on wca.bb.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.bb.acttime >= @startdate and wca.bb.acttime < @enddate
and wca.bb.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'CPOPT' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.cpopt
inner join wca.scmst on wca.cpopt.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.cpopt.acttime >= @startdate and wca.cpopt.acttime < @enddate
and wca.cpopt.actflag = 'i') as 'ict',
(select count(*) from wca.cpopt
inner join wca.scmst on wca.cpopt.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.cpopt.acttime >= @startdate and wca.cpopt.acttime < @enddate
and wca.cpopt.actflag = 'u') as 'uct',
(select count(*) from wca.cpopt
inner join wca.scmst on wca.cpopt.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.cpopt.acttime >= @startdate and wca.cpopt.acttime < @enddate
and wca.cpopt.actflag = 'c') as 'cct',
(select count(*) from wca.cpopt
inner join wca.scmst on wca.cpopt.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.cpopt.acttime >= @startdate and wca.cpopt.acttime < @enddate
and wca.cpopt.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'CAPRD' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.caprd.acttime >= @startdate and wca.caprd.acttime < @enddate
and wca.caprd.actflag = 'i') as 'ict',
(select count(*) from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.caprd.acttime >= @startdate and wca.caprd.acttime < @enddate
and wca.caprd.actflag = 'u') as 'uct',
(select count(*) from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.caprd.acttime >= @startdate and wca.caprd.acttime < @enddate
and wca.caprd.actflag = 'c') as 'cct',
(select count(*) from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.caprd.acttime >= @startdate and wca.caprd.acttime < @enddate
and wca.caprd.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'CONSD' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.consd.acttime >= @startdate and wca.consd.acttime < @enddate
and wca.consd.actflag = 'i') as 'ict',
(select count(*) from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.consd.acttime >= @startdate and wca.consd.acttime < @enddate
and wca.consd.actflag = 'u') as 'uct',
(select count(*) from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.consd.acttime >= @startdate and wca.consd.acttime < @enddate
and wca.consd.actflag = 'c') as 'cct',
(select count(*) from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.consd.acttime >= @startdate and wca.consd.acttime < @enddate
and wca.consd.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'CONV' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.conv
inner join wca.scmst on wca.conv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.conv.acttime >= @startdate and wca.conv.acttime < @enddate
and wca.conv.actflag = 'i') as 'ict',
(select count(*) from wca.conv
inner join wca.scmst on wca.conv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.conv.acttime >= @startdate and wca.conv.acttime < @enddate
and wca.conv.actflag = 'u') as 'uct',
(select count(*) from wca.conv
inner join wca.scmst on wca.conv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.conv.acttime >= @startdate and wca.conv.acttime < @enddate
and wca.conv.actflag = 'c') as 'cct',
(select count(*) from wca.conv
inner join wca.scmst on wca.conv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.conv.acttime >= @startdate and wca.conv.acttime < @enddate
and wca.conv.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DRIP' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.drip
inner join wca.div_my on wca.drip.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.drip.acttime >= @startdate and wca.drip.acttime < @enddate
and wca.drip.actflag = 'i') as 'ict',
(select count(*) from wca.drip
inner join wca.div_my on wca.drip.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.drip.acttime >= @startdate and wca.drip.acttime < @enddate
and wca.drip.actflag = 'u') as 'uct',
(select count(*) from wca.drip
inner join wca.div_my on wca.drip.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.drip.acttime >= @startdate and wca.drip.acttime < @enddate
and wca.drip.actflag = 'c') as 'cct',
(select count(*) from wca.drip
inner join wca.div_my on wca.drip.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.drip.acttime >= @startdate and wca.drip.acttime < @enddate
and wca.drip.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DMRGR' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.dmrgr
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dmrgr.acttime >= @startdate and wca.dmrgr.acttime < @enddate
and wca.dmrgr.actflag = 'i') as 'ict',
(select count(*) from wca.dmrgr
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dmrgr.acttime >= @startdate and wca.dmrgr.acttime < @enddate
and wca.dmrgr.actflag = 'u') as 'uct',
(select count(*) from wca.dmrgr
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dmrgr.acttime >= @startdate and wca.dmrgr.acttime < @enddate
and wca.dmrgr.actflag = 'c') as 'cct',
(select count(*) from wca.dmrgr
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dmrgr.acttime >= @startdate and wca.dmrgr.acttime < @enddate
and wca.dmrgr.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DPRCP' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dprcp.acttime >= @startdate and wca.dprcp.acttime < @enddate
and wca.dprcp.actflag = 'i') as 'ict',
(select count(*) from wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dprcp.acttime >= @startdate and wca.dprcp.acttime < @enddate
and wca.dprcp.actflag = 'u') as 'uct',
(select count(*) from wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dprcp.acttime >= @startdate and wca.dprcp.acttime < @enddate
and wca.dprcp.actflag = 'c') as 'cct',
(select count(*) from wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dprcp.acttime >= @startdate and wca.dprcp.acttime < @enddate
and wca.dprcp.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DIST' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.dist
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dist.acttime >= @startdate and wca.dist.acttime < @enddate
and wca.dist.actflag = 'i') as 'ict',
(select count(*) from wca.dist
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dist.acttime >= @startdate and wca.dist.acttime < @enddate
and wca.dist.actflag = 'u') as 'uct',
(select count(*) from wca.dist
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dist.acttime >= @startdate and wca.dist.acttime < @enddate
and wca.dist.actflag = 'c') as 'cct',
(select count(*) from wca.dist
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dist.acttime >= @startdate and wca.dist.acttime < @enddate
and wca.dist.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DVST' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dvst.acttime >= @startdate and wca.dvst.acttime < @enddate
and wca.dvst.actflag = 'i') as 'ict',
(select count(*) from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dvst.acttime >= @startdate and wca.dvst.acttime < @enddate
and wca.dvst.actflag = 'u') as 'uct',
(select count(*) from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dvst.acttime >= @startdate and wca.dvst.acttime < @enddate
and wca.dvst.actflag = 'c') as 'cct',
(select count(*) from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.dvst.acttime >= @startdate and wca.dvst.acttime < @enddate
and wca.dvst.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'ENT' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ent.acttime >= @startdate and wca.ent.acttime < @enddate
and wca.ent.actflag = 'i') as 'ict',
(select count(*) from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ent.acttime >= @startdate and wca.ent.acttime < @enddate
and wca.ent.actflag = 'u') as 'uct',
(select count(*) from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ent.acttime >= @startdate and wca.ent.acttime < @enddate
and wca.ent.actflag = 'c') as 'cct',
(select count(*) from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ent.acttime >= @startdate and wca.ent.acttime < @enddate
and wca.ent.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'FRANK' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.frank
inner join wca.div_my on wca.frank.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.frank.acttime >= @startdate and wca.frank.acttime < @enddate
and wca.frank.actflag = 'i') as 'ict',
(select count(*) from wca.frank
inner join wca.div_my on wca.frank.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.frank.acttime >= @startdate and wca.frank.acttime < @enddate
and wca.frank.actflag = 'u') as 'uct',
(select count(*) from wca.frank
inner join wca.div_my on wca.frank.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.frank.acttime >= @startdate and wca.frank.acttime < @enddate
and wca.frank.actflag = 'c') as 'cct',
(select count(*) from wca.frank
inner join wca.div_my on wca.frank.divid = wca.div_my.divid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.frank.acttime >= @startdate and wca.frank.acttime < @enddate
and wca.frank.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'IPO' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ipo.acttime >= @startdate and wca.ipo.acttime < @enddate
and wca.ipo.actflag = 'i') as 'ict',
(select count(*) from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ipo.acttime >= @startdate and wca.ipo.acttime < @enddate
and wca.ipo.actflag = 'u') as 'uct',
(select count(*) from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ipo.acttime >= @startdate and wca.ipo.acttime < @enddate
and wca.ipo.actflag = 'c') as 'cct',
(select count(*) from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ipo.acttime >= @startdate and wca.ipo.acttime < @enddate
and wca.ipo.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'LIQ' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.liq
inner join wca.issur on wca.liq.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.liq.acttime >= @startdate and wca.liq.acttime < @enddate
and wca.liq.actflag = 'i') as 'ict',
(select count(*) from wca.liq
inner join wca.issur on wca.liq.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.liq.acttime >= @startdate and wca.liq.acttime < @enddate
and wca.liq.actflag = 'u') as 'uct',
(select count(*) from wca.liq
inner join wca.issur on wca.liq.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.liq.acttime >= @startdate and wca.liq.acttime < @enddate
and wca.liq.actflag = 'c') as 'cct',
(select count(*) from wca.liq
inner join wca.issur on wca.liq.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.liq.acttime >= @startdate and wca.liq.acttime < @enddate
and wca.liq.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'MRGR' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.mrgr
inner join wca.rd on wca.mrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.mrgr.acttime >= @startdate and wca.mrgr.acttime < @enddate
and wca.mrgr.actflag = 'i') as 'ict',
(select count(*) from wca.mrgr
inner join wca.rd on wca.mrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.mrgr.acttime >= @startdate and wca.mrgr.acttime < @enddate
and wca.mrgr.actflag = 'u') as 'uct',
(select count(*) from wca.mrgr
inner join wca.rd on wca.mrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.mrgr.acttime >= @startdate and wca.mrgr.acttime < @enddate
and wca.mrgr.actflag = 'c') as 'cct',
(select count(*) from wca.mrgr
inner join wca.rd on wca.mrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.mrgr.acttime >= @startdate and wca.mrgr.acttime < @enddate
and wca.mrgr.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'NLIST' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.nlist
inner join wca.scexh on wca.nlist.scexhid = wca.scexh.scexhid
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.nlist.acttime >= @startdate and wca.nlist.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.nlist.actflag = 'i') as 'ict',
(select count(*) from wca.nlist
inner join wca.scexh on wca.nlist.scexhid = wca.scexh.scexhid
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.nlist.acttime >= @startdate and wca.nlist.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.nlist.actflag = 'u') as 'uct',
(select count(*) from wca.nlist
inner join wca.scexh on wca.nlist.scexhid = wca.scexh.scexhid
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.nlist.acttime >= @startdate and wca.nlist.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.nlist.actflag = 'c') as 'cct',
(select count(*) from wca.nlist
inner join wca.scexh on wca.nlist.scexhid = wca.scexh.scexhid
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.nlist.acttime >= @startdate and wca.nlist.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.nlist.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'PO' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.po
inner join wca.rd on wca.po.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.po.acttime >= @startdate and wca.po.acttime < @enddate
and wca.po.actflag = 'i') as 'ict',
(select count(*) from wca.po
inner join wca.rd on wca.po.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.po.acttime >= @startdate and wca.po.acttime < @enddate
and wca.po.actflag = 'u') as 'uct',
(select count(*) from wca.po
inner join wca.rd on wca.po.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.po.acttime >= @startdate and wca.po.acttime < @enddate
and wca.po.actflag = 'c') as 'cct',
(select count(*) from wca.po
inner join wca.rd on wca.po.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.po.acttime >= @startdate and wca.po.acttime < @enddate
and wca.po.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'REDEM' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.redem
inner join wca.scmst on wca.redem.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.redem.acttime >= @startdate and wca.redem.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.redem.actflag = 'i') as 'ict',
(select count(*) from wca.redem
inner join wca.scmst on wca.redem.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.redem.acttime >= @startdate and wca.redem.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.redem.actflag = 'u') as 'uct',
(select count(*) from wca.redem
inner join wca.scmst on wca.redem.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.redem.acttime >= @startdate and wca.redem.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.redem.actflag = 'c') as 'cct',
(select count(*) from wca.redem
inner join wca.scmst on wca.redem.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.redem.acttime >= @startdate and wca.redem.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.redem.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'RDNOM' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.rdnom
inner join wca.scmst on wca.rdnom.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rdnom.acttime >= @startdate and wca.rdnom.acttime < @enddate
and wca.rdnom.actflag = 'i') as 'ict',
(select count(*) from wca.rdnom
inner join wca.scmst on wca.rdnom.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rdnom.acttime >= @startdate and wca.rdnom.acttime < @enddate
and wca.rdnom.actflag = 'u') as 'uct',
(select count(*) from wca.rdnom
inner join wca.scmst on wca.rdnom.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rdnom.acttime >= @startdate and wca.rdnom.acttime < @enddate
and wca.rdnom.actflag = 'c') as 'cct',
(select count(*) from wca.rdnom
inner join wca.scmst on wca.rdnom.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rdnom.acttime >= @startdate and wca.rdnom.acttime < @enddate
and wca.rdnom.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'RCAP' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.rcap
inner join wca.scmst on wca.rcap.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rcap.acttime >= @startdate and wca.rcap.acttime < @enddate
and wca.rcap.actflag = 'i') as 'ict',
(select count(*) from wca.rcap
inner join wca.scmst on wca.rcap.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rcap.acttime >= @startdate and wca.rcap.acttime < @enddate
and wca.rcap.actflag = 'u') as 'uct',
(select count(*) from wca.rcap
inner join wca.scmst on wca.rcap.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rcap.acttime >= @startdate and wca.rcap.acttime < @enddate
and wca.rcap.actflag = 'c') as 'cct',
(select count(*) from wca.rcap
inner join wca.scmst on wca.rcap.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rcap.acttime >= @startdate and wca.rcap.acttime < @enddate
and wca.rcap.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'RTS' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rts.acttime >= @startdate and wca.rts.acttime < @enddate
and wca.rts.actflag = 'i') as 'ict',
(select count(*) from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rts.acttime >= @startdate and wca.rts.acttime < @enddate
and wca.rts.actflag = 'u') as 'uct',
(select count(*) from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rts.acttime >= @startdate and wca.rts.acttime < @enddate
and wca.rts.actflag = 'c') as 'cct',
(select count(*) from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rts.acttime >= @startdate and wca.rts.acttime < @enddate
and wca.rts.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SCSWP' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.scswp
inner join wca.rd on wca.scswp.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scswp.acttime >= @startdate and wca.scswp.acttime < @enddate
and wca.scswp.actflag = 'i') as 'ict',
(select count(*) from wca.scswp
inner join wca.rd on wca.scswp.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scswp.acttime >= @startdate and wca.scswp.acttime < @enddate
and wca.scswp.actflag = 'u') as 'uct',
(select count(*) from wca.scswp
inner join wca.rd on wca.scswp.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scswp.acttime >= @startdate and wca.scswp.acttime < @enddate
and wca.scswp.actflag = 'c') as 'cct',
(select count(*) from wca.scswp
inner join wca.rd on wca.scswp.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scswp.acttime >= @startdate and wca.scswp.acttime < @enddate
and wca.scswp.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SD' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sd.acttime >= @startdate and wca.sd.acttime < @enddate
and wca.sd.actflag = 'i') as 'ict',
(select count(*) from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sd.acttime >= @startdate and wca.sd.acttime < @enddate
and wca.sd.actflag = 'u') as 'uct',
(select count(*) from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sd.acttime >= @startdate and wca.sd.acttime < @enddate
and wca.sd.actflag = 'c') as 'cct',
(select count(*) from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sd.acttime >= @startdate and wca.sd.acttime < @enddate
and wca.sd.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'TKOVR' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.tkovr
inner join wca.scmst on wca.tkovr.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.tkovr.acttime >= @startdate and wca.tkovr.acttime < @enddate
and wca.tkovr.actflag = 'i') as 'ict',
(select count(*) from wca.tkovr
inner join wca.scmst on wca.tkovr.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.tkovr.acttime >= @startdate and wca.tkovr.acttime < @enddate
and wca.tkovr.actflag = 'u') as 'uct',
(select count(*) from wca.tkovr
inner join wca.scmst on wca.tkovr.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.tkovr.acttime >= @startdate and wca.tkovr.acttime < @enddate
and wca.tkovr.actflag = 'c') as 'cct',
(select count(*) from wca.tkovr
inner join wca.scmst on wca.tkovr.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.tkovr.acttime >= @startdate and wca.tkovr.acttime < @enddate
and wca.tkovr.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'CURRD' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.eventtype = 'corr') as 'corrct',
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.actflag = 'i') as 'ict',
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.actflag = 'u') as 'uct',
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.actflag = 'c') as 'cct',
(select count(*) from wca.currd
inner join wca.scmst on wca.currd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.currd.acttime >= @startdate and wca.currd.acttime < @enddate
and wca.currd.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'LSTAT' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.eventtype = 'corr') as 'corrct',
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.actflag = 'i') as 'ict',
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.actflag = 'u') as 'uct',
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.actflag = 'c') as 'cct',
(select count(*) from wca.lstat
inner join wca.scmst on wca.lstat.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lstat.acttime >= @startdate and wca.lstat.acttime < @enddate
and wca.lstat.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'LCC' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.eventtype = 'corr') as 'corrct',
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.actflag = 'i') as 'ict',
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.actflag = 'u') as 'uct',
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.actflag = 'c') as 'cct',
(select count(*) from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.lcc.acttime >= @startdate and wca.lcc.acttime < @enddate
and wca.lcc.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'LTCHG' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.eventtype = 'corr') as 'corrct',
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.actflag = 'i') as 'ict',
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.actflag = 'u') as 'uct',
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.actflag = 'c') as 'cct',
(select count(*) from wca.ltchg
inner join wca.scmst on wca.ltchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.ltchg.acttime >= @startdate and wca.ltchg.acttime < @enddate
and wca.ltchg.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'RCONV' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.eventtype = 'corr') as 'corrct',
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.actflag = 'i') as 'ict',
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.actflag = 'u') as 'uct',
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.actflag = 'c') as 'cct',
(select count(*) from wca.rconv
inner join wca.scmst on wca.rconv.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.rconv.acttime >= @startdate and wca.rconv.acttime < @enddate
and wca.rconv.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SDCHG' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.eventtype = 'corr') as 'corrct',
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.actflag = 'i') as 'ict',
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.actflag = 'u') as 'uct',
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.actflag = 'c') as 'cct',
(select count(*) from wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.sdchg.acttime >= @startdate and wca.sdchg.acttime < @enddate
and wca.sdchg.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SCCHG' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.eventtype = 'corr') as 'corrct',
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.actflag = 'i') as 'ict',
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.actflag = 'u') as 'uct',
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.actflag = 'c') as 'cct',
(select count(*) from wca.scchg
inner join wca.scmst on wca.scchg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.scchg.acttime >= @startdate and wca.scchg.acttime < @enddate
and wca.scchg.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SHOCH' as eventcode,
@iyear as iyear,
@imonth as imonth,
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.eventtype = 'clean') as 'cleanct',
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.eventtype = 'corr') as 'corrct',
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.actflag = 'i') as 'ict',
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.actflag = 'u') as 'uct',
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.actflag = 'c') as 'cct',
(select count(*) from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.issur.cntryofincorp = 'CA'
and wca.shoch.acttime >= @startdate and wca.shoch.acttime < @enddate
and wca.shoch.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'DIV' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.div_my.acttime >= @startdate and wca.div_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.div_my.actflag = 'i') as 'ict',
(select count(*) from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.div_my.acttime >= @startdate and wca.div_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.div_my.actflag = 'u') as 'uct',
(select count(*) from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.div_my.acttime >= @startdate and wca.div_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.div_my.actflag = 'c') as 'cct',
(select count(*) from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.div_my.acttime >= @startdate and wca.div_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.div_my.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'INT' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.int_my.acttime >= @startdate and wca.int_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.int_my.actflag = 'i') as 'ict',
(select count(*) from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.int_my.acttime >= @startdate and wca.int_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.int_my.actflag = 'u') as 'uct',
(select count(*) from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.int_my.acttime >= @startdate and wca.int_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.int_my.actflag = 'c') as 'cct',
(select count(*) from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.int_my.acttime >= @startdate and wca.int_my.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.int_my.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SCMST' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scmst.acttime >= @startdate and wca.scmst.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scmst.actflag = 'i') as 'ict',
(select count(*) from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scmst.acttime >= @startdate and wca.scmst.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scmst.actflag = 'u') as 'uct',
(select count(*) from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scmst.acttime >= @startdate and wca.scmst.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scmst.actflag = 'c') as 'cct',
(select count(*) from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scmst.acttime >= @startdate and wca.scmst.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scmst.actflag = 'd') as 'dct';
insert into webapp.eventct1 (cntrycd, cntryrule, eventcode, iyear, imonth, cleanct, corrct, ict, uct, cct, dct)
select 'CA' as cntrycd,
1 as cntryrule,
'SCEXH' as eventcode,
@iyear as iyear,
@imonth as imonth,
0 as cleanct,
0 as corrct,
(select count(*) from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scexh.acttime >= @startdate and wca.scexh.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scexh.actflag = 'i') as 'ict',
(select count(*) from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scexh.acttime >= @startdate and wca.scexh.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scexh.actflag = 'u') as 'uct',
(select count(*) from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scexh.acttime >= @startdate and wca.scexh.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scexh.actflag = 'c') as 'cct',
(select count(*) from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where wca.scexh.acttime >= @startdate and wca.scexh.acttime < @enddate
and wca.issur.cntryofincorp = 'CA'
and wca.scexh.actflag = 'd') as 'dct';