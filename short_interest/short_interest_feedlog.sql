INSERT INTO edi_si.si_feedlog(feed_type,fromdate,todate,filename,acttime )
SELECT 
	'si_cond' AS feed_type,ToDate AS FRomDate, NOW() AS ToDate,
	CONCAT('_',replace(CURDATE(),'-',''),'_',replace(CURTIME(),':','')) AS filename, NOW()
FROM edi_si.si_feedlog
WHERE feed_type = 'si_cond' AND ToDate = ( SELECT MAX(ToDate) FROM edi_si.si_feedlog WHERE feed_type = 'si_cond');


INSERT INTO edi_si.si_feedlog(feed_type,fromdate,todate,filename,acttime )
SELECT 
	'si_res' AS feed_type,ToDate AS FRomDate, NOW() AS ToDate,
	CONCAT('_',replace(CURDATE(),'-',''),'_',replace(CURTIME(),':','')) AS filename, NOW()
FROM edi_si.si_feedlog
WHERE feed_type = 'si_res' AND ToDate = ( SELECT MAX(ToDate) FROM edi_si.si_feedlog WHERE feed_type = 'si_res');


INSERT INTO edi_si.si_feedlog(feed_type,fromdate,todate,filename,acttime )
SELECT 
	'si_inv' AS feed_type,ToDate AS FRomDate, NOW() AS ToDate,
	CONCAT('_',replace(CURDATE(),'-',''),'_',replace(CURTIME(),':','')) AS filename, NOW()
FROM edi_si.si_feedlog
WHERE feed_type = 'si_inv' AND ToDate = ( SELECT MAX(ToDate) FROM edi_si.si_feedlog WHERE feed_type = 'si_inv');


INSERT INTO edi_si.si_feedlog(feed_type,fromdate,todate,filename,acttime )
SELECT 
	'si_sec' AS feed_type,ToDate AS FRomDate, NOW() AS ToDate,
	CONCAT('_',replace(CURDATE(),'-',''),'_',replace(CURTIME(),':','')) AS filename, NOW()
FROM edi_si.si_feedlog
WHERE feed_type = 'si_sec' AND ToDate = ( SELECT MAX(ToDate) FROM edi_si.si_feedlog WHERE feed_type = 'si_sec');

/*INSERT INTO `edi_si`.`si_feedlog` (`feed_type`, `FromDate`, `ToDate`, `acttime`) VALUES ('si_sec', '2017-07-18 15:28:52', '2017-07-18 15:28:52', '2019-02-13 19:50:13');*/

