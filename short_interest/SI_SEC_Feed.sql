-- filenameprefix=SI_SEC_TOT
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_SEC_TOT
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=            
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n
 
-- # 0
SELECT
	'invest_id' AS f3,
	'actflag' AS f3,
	'pub_date' AS f3,
	'cntrycd' AS f3,
	'issuer' AS f3,
	'local_code' AS f3,
	'us_code' AS f3,
	'isin' AS f3,
	'sedol' AS f3,
	'composite_global_id' AS f3,
	'bloomberg_composite_ticker' AS f3,
	'bloomberg_global_id' AS f3,
	'bloomberg_exchange_ticker' AS f3,
	'security' AS f3,
	'Disclosed_position_holder_total' AS f3,
	'pos_date' AS f3,
	'secid' 
-- # 1
 
SELECT 
	invest_id,
	actflag,
	#changed,
	pub_date,
	cntrycd,
	issuer,
	local_code,
	us_code,
	isin,
	sedol,
	composite_global_id,
	bloomberg_composite_ticker,
	bloomberg_global_id,
	bloomberg_exchange_ticker,
	security,
	sum(holder_net) as Disclosed_position_holder_total,
	pos_date,
	secid                
FROM edi_si.sint_investor
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R' and pub_date is not null and holder_net is not null 
and isin is not null and cntrycd is not null and pos_date is not null
group by isin,cntrycd,pos_date
ORDER BY acttime desc;
