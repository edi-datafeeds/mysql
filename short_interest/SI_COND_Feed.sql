-- filenameprefix=SI_COND
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_COND
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

SELECT 
	'consd_id' AS f3,
	'actflag' AS f3,
	'acttime' AS f3,
	'pub_date' AS f3,
	'cntrycd' AS f3,
	'issuer' AS f3,
	'local_code' AS f3,
	'us_code' AS f3,
	'isin' AS f3,
	'sedol' AS f3,
	'composite_global_id' AS f3,
	'bloomberg_composite_ticker' AS f3,
	'bloomberg_global_id' AS f3,
	'bloomberg_exchange_ticker' AS f3,
	'security' AS f3,
	'product_class' AS f3,
	'gross_sale' AS f3,
	'issued_capital' AS f3,
	'free_float' AS f3,
	'sale_value' AS f3,
	'sale_valume' AS f3,
	'total_trades' AS f3,
	'high_sale_price' AS f3,
	'low_sale_price' AS f3,
	'prev_sales_shares' AS f3,
	'cuurent_number' AS f3,
	'change_in_number' AS f3,
	'interest_change' AS f3,
	'change_in_average' AS f3,
	'days_to_cover' AS f3,
	'change_to_cover' AS f3,
	'stock_split_flag' AS f3,
	'new_issue_flag' AS f3,
	'position_shares' AS f3,
	'total_shares_issue' AS f3,
	'NIS' AS f3,
	'interest_ratio' AS f3,
	'secid'


-- # 1

SELECT 
	consd_id,
	actflag,
	acttime,
	pub_date,
	cntrycd,
	issuer,
	local_code,
	us_code,
	isin,
	sedol,
	composite_global_id,
	bloomberg_composite_ticker,
	bloomberg_global_id,
	bloomberg_exchange_ticker,
	security,
	product_class,
	gross_sale,
	issued_capital,
	free_float,
	sale_value,
	sale_valume,
	total_trades,
	high_sale_price,
	low_sale_price,
	prev_sales_shares,
	cuurent_number,
	change_in_number,
	interest_change,
	change_in_average,
	days_to_cover,
	change_to_cover,
	stock_split_flag,
	new_issue_flag,
	position_shares,
	total_shares_issue,
	NIS,
	interest_ratio,
	secid
FROM edi_si.sint_consd
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R'
ORDER BY acttime desc
