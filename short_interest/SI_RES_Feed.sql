-- filenameprefix=SI_RES
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_RES
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

select
	'res_id' AS f3,
	'actflag' AS f3,
	'acttime' AS f3,
	'pub_date' AS f3,
	'cntrycd' AS f3,
	'issuer' AS f3,
	'local_code' AS f3,
	'us_code' AS f3,
	'isin' AS f3,
	'sedol' AS f3,
	'composite_global_id' AS f3,
	'bloomberg_composite_ticker' AS f3,
	'bloomberg_global_id' AS f3,
	'bloomberg_exchange_ticker' AS f3,
	'security' AS f3,
	'start_date' AS f3,
	'end_date' AS f3,
	'notes' AS f3,
	'secid' AS f3

-- # 1

SELECT 
	res_id,
	actflag,
	acttime,
	pub_date,
	cntrycd,
	issuer,
	local_code,
	us_code,
	isin,
	sedol,
	composite_global_id,
	bloomberg_composite_ticker,
	bloomberg_global_id,
	bloomberg_exchange_ticker,
	security,
	start_date,
	end_date,
	notes,
	secid
FROM edi_si.sint_restriction
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R'
ORDER BY acttime desc
