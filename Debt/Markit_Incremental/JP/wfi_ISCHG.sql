--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('ISCHG') as TableName,
wca.ischg.Actflag,
wca.ischg.AnnounceDate as Created,
wca.ischg.Acttime as Changed,
wca.ischg.IschgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.ischg.IssID,
wca.ischg.NameChangeDate,
wca.ischg.IssOldName,
wca.ischg.IssNewName,
wca.ischg.EventType,
wca.ischg.LegalName,
wca.ischg.MeetingDateFlag, 
wca.ischg.IsChgNotes as Notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and (wca.bond.issuedate<=wca.ischg.nameChangedate
     or wca.bond.issuedate is null or wca.ischg.nameChangedate is null
    )
and wca.ischg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))





































