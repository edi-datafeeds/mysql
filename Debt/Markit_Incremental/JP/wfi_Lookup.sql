--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_LOOKUP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
upper(Actflag) as Actflag,
wca.lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
wca.lookupextra.Lookup
FROM wca.lookupextra

union

SELECT
upper(Actflag) as Actflag,
wca.lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
wca.lookup.Lookup
FROM wca.lookup



union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
wca.dvprd.DvprdCD,
wca.dvprd.Divperiod
from wca.dvprd

union

SELECT
upper('I') as Actflag,
wca.secty.Acttime,
upper('SECTYPE') as TypeGroup,
wca.secty.SectyCD,
wca.secty.SecurityDescriptor
from wca.secty

union

SELECT
wca.exchg.Actflag,
wca.exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
wca.exchg.ExchgCD as Code,
wca.exchg.Exchgname as Lookup
from wca.exchg

union

SELECT
wca.exchg.Actflag,
wca.exchg.Acttime,
upper('MICCODE') as TypeGroup,
wca.exchg.MIC as Code,
wca.exchg.Exchgname as Lookup
from wca.exchg
where MIC <> '' and MIC is not null

union

SELECT
wca.exchg.Actflag,
wca.exchg.Acttime,
upper('MICMAP') as TypeGroup,
wca.exchg.ExchgCD as Code,
wca.exchg.MIC as Lookup
from wca.exchg

union

SELECT
wca.indus.Actflag,
wca.indus.Acttime,
upper('INDUS') as TypeGroup,
cast(wca.indus.IndusID as char(10)) as Code,
wca.indus.IndusName as Lookup
from wca.indus

union

SELECT
wca.cntry.Actflag,
wca.cntry.Acttime,
upper('CNTRY') as TypeGroup,
wca.cntry.CntryCD as Code,
wca.cntry.Country as Lookup
from wca.cntry

union

SELECT
wca.curen.Actflag,
wca.curen.Acttime,
upper('CUREN') as TypeGroup,
wca.curen.CurenCD as Code,
wca.curen.Currency as Lookup
from wca.curen

union

SELECT
wca.event.Actflag,
wca.event.Acttime,
upper('EVENT') as TypeGroup,
wca.event.EventType as Code,
wca.event.EventName as Lookup
from wca.event

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
wca.sectygrp.sectycd as Code,
wca.sectygrp.securitydescriptor as Lookup
from wca.sectygrp
where secgrpid = 2
and 1=2

order by TypeGroup, Code
