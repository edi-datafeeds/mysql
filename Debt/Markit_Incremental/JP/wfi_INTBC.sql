--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('INTBC') as TableName,
wca.intbc.Actflag,
wca.intbc.AnnounceDate as Created,
wca.intbc.Acttime as Changed,
wca.intbc.IntbcID,
wca.intbc.SecID,
wca.scmst.ISIN,
wca.intbc.EffectiveDate,
wca.intbc.OldIntBasis,
wca.intbc.NewIntBasis,
wca.intbc.OldIntBDC,
wca.intbc.NewIntBDC
from wca.intbc
inner join wca.bond on wca.intbc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.intbc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))



