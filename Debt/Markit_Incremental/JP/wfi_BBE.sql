--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT distinct
upper('BBE') as tablename,
wca.bbe.actflag,
wca.bbe.announcedate,
wca.bbe.acttime,
wca.bbe.bbeid,
wca.bbe.secid,
wca.bbe.exchgcd,
wca.bbe.curencd,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk
from wca.bbe
inner join wca.scmst on wca.bbe.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.bbe.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
