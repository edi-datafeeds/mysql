--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.actflag,
wca.bbcc.announcedate,
wca.bbcc.acttime,
wca.bbcc.bbccid,
wca.bbcc.secid,
wca.bbcc.bbcid,
wca.bbcc.oldcntrycd,
wca.bbcc.oldcurencd,
wca.bbcc.effectivedate,
wca.bbcc.newcntrycd,
wca.bbcc.newcurencd,
wca.bbcc.oldbbgcompid,
wca.bbcc.newbbgcompid,
wca.bbcc.oldbbgcomptk,
wca.bbcc.newbbgcomptk,
wca.bbcc.releventid,
wca.bbcc.eventtype,
wca.bbcc.notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.bbcc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
