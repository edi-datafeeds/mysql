--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBEC') as tablename,
wca.bbec.actflag,
wca.bbec.announcedate,
wca.bbec.acttime,
wca.bbec.bbecid,
wca.bbec.secid,
wca.bbec.bbeid,
wca.bbec.oldexchgcd,
wca.bbec.oldcurencd,
wca.bbec.effectivedate,
wca.bbec.newexchgcd,
wca.bbec.newcurencd,
wca.bbec.oldbbgexhid,
wca.bbec.newbbgexhid,
wca.bbec.oldbbgexhtk,
wca.bbec.newbbgexhtk,
wca.bbec.releventid,
wca.bbec.eventtype,
wca.bbec.notes
from wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.bbec.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
