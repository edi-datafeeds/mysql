--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
upper('BBC') as tablename,
wca.bbc.actflag,
wca.bbc.announcedate,
wca.bbc.acttime,
wca.bbc.bbcid,
wca.bbc.secid,
wca.bbc.cntrycd,
wca.bbc.curencd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk
from wca.bbc
inner join wca.scmst on wca.bbc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.bbc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))
