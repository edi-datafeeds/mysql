--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=EDI_CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRCHG') as TableName,
wca.crchg.Actflag,
wca.crchg.AnnounceDate as Created,
wca.crchg.Acttime as Changed, 
wca.crchg.CrChgID,
wca.crchg.SecID,
wca.crchg.RatingAgency,
wca.crchg.RatingDate,
wca.crchg.OldRating,
wca.crchg.NewRating,
wca.crchg.Direction,
wca.crchg.WatchList,
wca.crchg.WatchListReason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.crchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))



