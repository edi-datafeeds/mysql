--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('BKRPNOTES') as TableName,
wca.bkrp.Actflag,
wca.bkrp.BkrpID,
wca.bkrp.BkrpNotes as Notes
from wca.bkrp
where
bkrp.Issid in (select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where (client.pfisin.accid=211 and client.pfisin.actflag='I')
or (client.pfisin.accid=211 and client.pfisin.actflag='U')
and bkrp.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog))
