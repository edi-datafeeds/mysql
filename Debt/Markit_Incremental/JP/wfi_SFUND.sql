--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SFUND') as TableName,
wca.sfund.Actflag,
wca.sfund.AnnounceDate as Created,
wca.sfund.Acttime as Changed,
wca.sfund.SfundID,
wca.sfund.SecID,
wca.scmst.ISIN,
wca.sfund.SfundDate,
wca.sfund.CurenCD as Sinking_Currency,
wca.sfund.Amount,
wca.sfund.AmountasPercent,
wca.sfund.SfundNotes as Notes
from wca.sfund
inner join wca.bond on wca.sfund.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.sfund.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))



