--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CONVT') as TableName,
wca.convt.Actflag,
wca.convt.AnnounceDate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.convt.FromDate,
wca.convt.ToDate,
wca.convt.RatioNew,
wca.convt.RatioOld,
wca.convt.CurenCD,
wca.convt.CurPair,
wca.convt.Price,
wca.convt.MandOptFlag,
wca.convt.ResSecID,
wca.convt.SectyCD as ResSectyCD,
resscmst.ISIN as ResISIN,
resissur.Issuername as ResIssuername,
resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,
wca.convt.FXrate,
wca.convt.PartFinalFlag,
wca.convt.PriceAsPercent,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = wca.resissur.issid
where
wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.convt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))







































