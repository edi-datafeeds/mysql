--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BOCHG') as TableName,
wca.bochg.Actflag,
wca.bochg.AnnounceDate as Created,
wca.bochg.Acttime as Changed,  
wca.bochg.BochgID,
wca.bochg.RelEventID,
wca.bochg.SecID,
wca.scmst.ISIN,
wca.bochg.EffectiveDate,
wca.bochg.OldOutValue,
wca.bochg.NewOutValue,
wca.bochg.EventType,
wca.bochg.OldOutDate,
wca.bochg.NewOutDate,
wca.bochg.BochgNotes as Notes
from wca.bochg
inner join wca.bond on wca.bochg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=211 and actflag='U')
and wca.bochg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))




