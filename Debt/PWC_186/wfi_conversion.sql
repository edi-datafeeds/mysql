--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Conversion
--fileheadertext=EDI_WFIPORT_Conversion_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'Conversion' as EventName,
wca.conv.ConvID,
wca.conv.Actflag,
wca.conv.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) THEN wca.rd.Acttime ELSE wca.conv.Acttime END as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.conv.FromDate,
wca.conv.ToDate,
wca.conv.RatioNew,
wca.conv.RatioOld,
wca.conv.CurenCD as ConvCurrency,
wca.conv.Price,
wca.conv.MandOptFlag,
wca.conv.Fractions,
wca.conv.FXrate,
wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.PriceAsPercent,
wca.conv.ResSectyCD,
wca.conv.ResSecID,
wca.resscmst.ISIN as ResISIN,
wca.conv.ConvNotes as Notes
FROM wca.conv
INNER JOIN wca.bond ON wca.conv.SecID = wca.bond.SecID
INNER JOIN wca.scmst ON wca.bond.SecID = wca.scmst.SecID
INNER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.rd ON wca.conv.RdID = wca.rd.RdID
LEFT OUTER JOIN wca.scmst as resscmst ON wca.conv.ResSecID = wca.resscmst.SecID
where
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'U')
and (wca.conv.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.rd.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
)
OR
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'I')
and (FromDate>='2008/09/01' or (wca.conv.FromDate is null and wca.conv.AnnounceDate>='2008/09/01'))
and wca.conv.actflag<>'D'
)
