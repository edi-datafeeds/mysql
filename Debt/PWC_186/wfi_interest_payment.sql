--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Interest_Payment
--fileheadertext=EDI_WFIPORT_Interest_Payment_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'Interest_Payment' as EventName,
wca.int_my.RdID,
CASE WHEN wca.int_my.Actflag = 'D' or wca.int_my.actflag='C' or wca.intpy.actflag is null THEN wca.int_my.Actflag ELSE wca.intpy.Actflag END as Actflag,
wca.int_my.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.int_my.Acttime) and (wca.rd.Acttime > wca.exdt.Acttime) THEN wca.rd.Acttime WHEN (wca.exdt.Acttime is not null) and (wca.exdt.Acttime > wca.int_my.Acttime) THEN wca.exdt.Acttime ELSE wca.int_my.Acttime END as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.intpy.BCurenCD as DebtCurrency,
wca.intpy.BParValue as NominalValue,
wca.scexh.ExchgCD,
wca.rd.Recdate,
wca.exdt.Exdate,
wca.exdt.Paydate,
wca.int_my.InterestFromDate,
wca.int_my.InterestToDate,
wca.int_my.Days,
' ' as InterestDefault,
wca.intpy.CurenCD,
wca.intpy.IntRate,
wca.intpy.GrossInterest,
wca.intpy.NetInterest,
wca.intpy.DomesticTaxRate,
wca.intpy.NonResidentTaxRate,
wca.intpy.RescindInterest,
wca.intpy.AgencyFees,
wca.intpy.CouponNo,
null as CouponID,
wca.intpy.DefaultOpt,
wca.intpy.OptElectionDate,
wca.intpy.AnlCoupRate,
wca.int_my.IntNotes as Notes
from wca.int_my
INNER JOIN wca.rd ON wca.int_my.RdID = wca.rd.RdID
INNER JOIN wca.bond ON wca.rd.SecID = wca.bond.SecID
INNER JOIN wca.scmst ON wca.bond.SecID = wca.scmst.SecID
INNER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.scexh ON wca.bond.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.exdt ON wca.rd.RdID = wca.exdt.RdID 
     AND wca.scexh.ExchgCD = wca.exdt.ExchgCD AND 'INT' = wca.exdt.EventType
     AND wca.exdt.Actflag<>'D'
LEFT OUTER JOIN wca.intpy ON wca.int_my.RdID = wca.intpy.RdID
left outer join wca.lstat on wca.scexh.secid = wca.lstat.secid 
     and wca.scexh.exchgcd = wca.lstat.exchgcd 
     and 'D'= wca.lstat.lstatstatus 
     and wca.scexh.liststatus = 'D'
where
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'U')
and (wca.int_my.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.rd.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.exdt.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.intpy.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
)
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
)
OR
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'I')
and (wca.exdt.paydate>='2008/09/01' or (wca.exdt.paydate is null and wca.int_my.AnnounceDate>='2008/09/01'))
AND (wca.intpy.actflag<>'D' OR wca.intpy.ACTFLAG IS NULL)
and wca.intpy.actflag<>'D'
)