--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Redemption
--fileheadertext=EDI_WFIPORT_188_Redemption_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'Redemption' as EventName,
wca.redem.RedemID,
wca.redem.Actflag,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.redem.RedemDate,
wca.redem.CurenCD as RedemCurrency,
wca.redem.RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
wca.redem.AmountRedeemed,
wca.redem.RedemPremium,
wca.redem.RedemPercent,
' ' AS RedemDefault,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,

wca.redem.PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.redem.RedemNotes as Notes
FROM wca.redem
INNER JOIN wca.bond ON wca.redem.SecID = wca.bond.SecID
INNER JOIN wca.scmst ON wca.bond.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.rd ON wca.redem.RdID = wca.rd.RdID
where
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'U')
and (wca.redem.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.rd.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
)
OR
(
wca.scmst.Isin in (select portfolio.fisin.code from portfolio.fisin where accid = 186 and actflag = 'I')
and (wca.redem.RedemDate>='2008/09/01' or (wca.redem.RedemDate is null and wca.redem.AnnounceDate>='2008/09/01'))
and wca.redem.actflag<>'D'
)