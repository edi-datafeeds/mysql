--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('BOND') as tablename,
wca.bond.actflag,
wca.bond.announcedate as created,
wca.bond.acttime as changed,
wca.bond.secid,
wca.scmst.isin,
wca.bond.bondtype,
wca.bond.debtmarket,
wca.bond.curencd as debtcurrency,
case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue else wca.bond.parvalue end as NominalValue,

wca.bond.issuedate,
wca.bond.issuecurrency,
wca.bond.issueprice,
wca.bond.issueamount,
wca.bond.issueamountdate,
wca.bond.outstandingamount,
wca.bond.outstandingamountdate,
wca.bond.interestbasis,
wca.bond.interestrate,
wca.bond.interestaccrualconvention,
wca.bond.interestpaymentfrequency,
wca.bond.intcommencementdate as interestcommencementdate,
wca.bond.firstcoupondate,
wca.bond.interestpaydate1,
wca.bond.interestpaydate2,
wca.bond.interestpaydate3,
wca.bond.interestpaydate4,
wca.bondx.DomesticTaxRate,
wca.bondx.NonResidentTaxRate,
wca.bond.frntype,
wca.bond.frnindexbenchmark,
wca.bond.markup as frnmargin,
wca.bond.minimuminterestrate as frnmininterestrate,
wca.bond.maximuminterestrate as frnmaxinterestrate,
wca.bond.rounding as frnrounding,
wca.bondx.Series,
wca.bondx.Class,
wca.bondx.OnTap,
wca.bondx.MaximumTapAmount,
wca.bondx.TapExpiryDate,
wca.bond.guaranteed,
wca.bond.securedby,
wca.bond.securitycharge,
wca.bond.subordinate,
wca.bond.seniorjunior,
wca.bond.warrantattached,
wca.bond.maturitystructure,
wca.bond.perpetual,
wca.bond.maturitydate,
wca.bond.maturityextendible,
wca.bond.callable,
wca.bond.puttable,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.Denomination7,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.bond.strip,
wca.bond.stripinterestnumber, 
wca.bond.bondsrc,
wca.bond.maturitybenchmark,
wca.bond.conventionmethod,
wca.bond.frnintadjfreq as frninterestadjfreq,
wca.bond.intbusdayconv as interestbusdayconv,
wca.bond.interestcurrency,
wca.bond.matbusdayconv as maturitybusdayconv,
wca.bond.maturitycurrency, 
wca.bondx.TaxRules,
wca.bond.varintpaydate as varinterestpaydate,
wca.bond.priceaspercent,
wca.bond.payoutmode,
wca.bond.cumulative,
case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
     when rtrim(wca.bond.largeparvalue)='' then null
     when rtrim(wca.bond.matpriceaspercent)='' then null
     else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.matpriceaspercent as decimal(18,4))/100 
     end as maturityprice,
wca.bond.matpriceaspercent as maturitypriceaspercent,
wca.bond.sinkingfund,
wca.bondx.GoverningLaw,
wca.bond.municipal,
wca.bond.privateplacement,
wca.bond.syndicated,
wca.bond.tier,
wca.bond.upplow,
wca.bond.collateral,
wca.bond.coverpool,
wca.bond.pikpay,
wca.bond.notes
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
where 
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and (wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
  or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)))

