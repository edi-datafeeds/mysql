--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('REDMT') as tablename,
wca.redmt.actflag,
wca.redmt.announcedate as created,
wca.redmt.acttime as changed,
wca.redmt.redmtid,
wca.scmst.secid,
wca.scmst.isin,
wca.redmt.redemptiondate as redemdate,
wca.redmt.curencd as redemcurrency,
wca.redmt.redemptionprice as redemprice,
wca.redmt.mandoptflag,
wca.redmt.partfinal,
wca.redmt.redemptiontype as redemtype,
wca.redmt.redemptionamount as redemamount,
wca.redmt.redemptionpremium as redempremium,
wca.redmt.redeminpercent,
wca.redmt.priceaspercent,
wca.redmt.premiumaspercent,
wca.redmt.redmtnotes as notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.redmt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
