--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCEXH') as tablename,
wca.scexh.actflag,
wca.scexh.announcedate as created,
wca.scexh.acttime as changed,
wca.scexh.scexhid,
wca.scexh.secid,
wca.scmst.isin,
wca.scexh.exchgcd,
wca.scexh.liststatus,
wca.scexh.lot,
wca.scexh.mintrdgqty,
wca.scexh.listdate,
wca.scexh.localcode,
SUBSTRING(wca.scexh.JunkLocalCode, 1, 50) as JunkLocalCode
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

