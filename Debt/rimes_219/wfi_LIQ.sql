--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('LIQ') as tablename,
wca.liq.actflag,
wca.liq.announcedate as created,
wca.liq.acttime as changed,
wca.liq.liqid,
wca.bond.secid,
wca.scmst.isin,
wca.liq.issid,
wca.liq.liquidator,
wca.liq.liqadd1,
wca.liq.liqadd2,
wca.liq.liqadd3,
wca.liq.liqadd4,
wca.liq.liqadd5,
wca.liq.liqadd6,
wca.liq.liqcity,
wca.liq.liqcntrycd,
wca.liq.liqtel,
wca.liq.liqfax,
wca.liq.liqemail,
wca.liq.rddate
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.liq.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
