--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCCHG') as tablename,
wca.scchg.actflag,
wca.scchg.announcedate as created,
wca.scchg.acttime as changed,
wca.scchg.scchgid,
wca.scchg.secid,
wca.scmst.isin,
wca.scchg.dateofchange,
wca.scchg.secoldname,
wca.scchg.secnewname,
wca.scchg.eventtype,
wca.scchg.oldsectycd,
wca.scchg.newsectycd,
wca.scchg.oldregs144a,
wca.scchg.newregs144a,
wca.scchg.scchgnotes as notes
from wca.scchg
inner join wca.bond on wca.scchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.scchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
