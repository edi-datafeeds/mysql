--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCAGY') as tablename,
wca.scagy.actflag,
wca.scagy.announcedate as created,
wca.scagy.acttime as changed,
wca.scagy.scagyid,
wca.scagy.secid,
wca.scmst.isin,
wca.scagy.relationship,
wca.scagy.agncyid,
wca.scagy.guaranteetype,
wca.scagy.spstartdate,
wca.scagy.spenddate,
wca.scagy.notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.scagy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
