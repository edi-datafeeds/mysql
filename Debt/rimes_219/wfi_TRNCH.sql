--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('TRNCH') as tablename,
wca.trnch.actflag,
wca.trnch.announcedate as created,
wca.trnch.acttime as changed,
wca.trnch.trnchnumber,
wca.trnch.secid,
wca.scmst.isin,
wca.trnch.tranchedate,
wca.trnch.trancheamount,
wca.trnch.expirydate,
wca.trnch.notes as notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.trnch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))


