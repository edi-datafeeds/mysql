--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_CNTR
--fileheadertext=EDI_CNTR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CNTR') as tablename,
wca.cntr.actflag,
wca.cntr.announcedate as created,
wca.cntr.acttime as changed,
wca.cntr.cntrid,
wca.cntr.centrename,
wca.cntr.cntrycd
from wca.cntr
where
wca.cntr.actflag<>'d'
-- wca.cntr.acttime > (select max(acttime)-0.1 from wca.tbl_opslog where seq = 3)
