--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_CONV
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'CONV' as tablename,
wca.conv.actflag,
wca.conv.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.conv.acttime) then wca.rd.acttime else wca.conv.acttime end as changed,
wca.conv.convid,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.recdate,
wca.conv.fromdate,
wca.conv.todate,
wca.conv.rationew,
wca.conv.ratioold,
wca.conv.curencd as convcurrency,
wca.conv.price,
wca.conv.mandoptflag,
wca.conv.fractions,
wca.conv.fxrate,
wca.conv.partfinalflag,
wca.conv.convtype,
wca.conv.priceaspercent,
wca.conv.ressectycd,
wca.conv.ressecid,
resscmst.isin as resisin,
resissur.issuername as resissuername,
resscmst.securitydesc as ressecuritydesc,
wca.conv.convnotes as notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = resissur.issid
where
(wca.scmst.isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.conv.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)))
