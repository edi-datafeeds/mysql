--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('MPAY') as tablename,
wca.mpay.actflag,
wca.mpay.announcedate,
wca.mpay.acttime,
wca.mpay.sevent as eventtype,
wca.mpay.eventid,
wca.mpay.optionid,
wca.mpay.serialid,
wca.mpay.sectycd as ressectycd,
wca.mpay.ressecid,
resscmst.isin as resisin,
resissur.issuername as resissuername,
resscmst.securitydesc as ressecuritydesc,
wca.mpay.rationew,
wca.mpay.ratioold,
wca.mpay.fractions,
wca.mpay.minofrqty,
wca.mpay.maxofrqty,
wca.mpay.minqlyqty,
wca.mpay.maxqlyqty,
wca.mpay.paydate,
wca.mpay.curencd,
wca.mpay.minprice,
wca.mpay.maxprice,
wca.mpay.tndrstrkprice,
wca.mpay.tndrstrkstep,
wca.mpay.paytype,
wca.mpay.dutchauction,
wca.mpay.defaultopt,
wca.mpay.optelectiondate
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = resissur.issid
where
wca.scmst.isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.mpay.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
