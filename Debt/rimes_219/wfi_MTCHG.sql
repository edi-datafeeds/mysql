--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('MTCHG') as tablename,
wca.mtchg.actflag,
wca.mtchg.announcedate as created,
wca.mtchg.acttime as changed,
wca.mtchg.mtchgid,
wca.mtchg.secid,
wca.scmst.isin,
wca.mtchg.notificationdate,
wca.mtchg.oldmaturitydate,
wca.mtchg.newmaturitydate,
wca.mtchg.reason,
wca.mtchg.eventtype,
wca.mtchg.oldmaturitybenchmark,
wca.mtchg.newmaturitybenchmark,
wca.mtchg.notes as notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=219 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=219 and actflag='u')
and wca.mtchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
