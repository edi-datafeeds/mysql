--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('AGM') as tablename,
wca.agm.actflag,
wca.agm.announcedate as created,
wca.agm.acttime as changed, 
wca.agm.agmid,
wca.bond.secid,
wca.scmst.isin,
wca.agm.issid,
wca.agm.agmdate,
wca.agm.agmegm,
wca.agm.agmno,
wca.agm.fyedate,
wca.agm.agmtime,
wca.agm.add1,
wca.agm.add2,
wca.agm.add3,
wca.agm.add4,
wca.agm.add5,
wca.agm.add6,
wca.agm.city,
wca.agm.cntrycd
from wca.agm
inner join wca.scmst on wca.agm.bondsecid = wca.scmst.secid
inner join wca.bond on wca.agm.bondsecid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.agm.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))