--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('IFCHG') as tablename,
wca.ifchg.actflag,
wca.ifchg.announcedate as created,
wca.ifchg.acttime as changed,
wca.ifchg.ifchgid,
wca.ifchg.secid,
wca.scmst.isin,
wca.ifchg.notificationdate,
wca.ifchg.oldintpayfrqncy,
wca.ifchg.oldintpaydate1,
wca.ifchg.oldintpaydate2,
wca.ifchg.oldintpaydate3,
wca.ifchg.oldintpaydate4,
wca.ifchg.newintpayfrqncy,
wca.ifchg.newintpaydate1,
wca.ifchg.newintpaydate2,
wca.ifchg.newintpaydate3,
wca.ifchg.newintpaydate4,
wca.ifchg.eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.ifchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
