--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('LCC') as tablename,
wca.lcc.actflag,
wca.lcc.announcedate as created,
wca.lcc.acttime as changed,
wca.lcc.lccid,
wca.lcc.secid,
wca.scmst.isin,
wca.lcc.exchgcd,
wca.lcc.effectivedate,
wca.lcc.oldlocalcode,
wca.lcc.newlocalcode,
wca.lcc.eventtype,
wca.lcc.releventid
from wca.lcc
inner join wca.bond on wca.lcc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.lcc.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
