--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'REDEM' as tablename,
wca.redem.actflag,
wca.redem.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.redem.acttime) then wca.rd.acttime else wca.redem.acttime end as changed,
wca.redem.redemid,
wca.scmst.secid,
wca.scmst.isin,
wca.rd.recdate,
wca.redem.redemdate,
wca.redem.curencd as redemcurrency,
wca.redem.redemprice,
wca.redem.mandoptflag,
wca.redem.partfinal,
wca.redem.redemtype,
wca.redem.amountredeemed,
wca.redem.redempremium,
wca.redem.redempercent,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,

wca.redem.priceaspercent,
wca.redem.premiumaspercent,
wca.redem.indefpay,
wca.redem.redemnotes as notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.redem.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
