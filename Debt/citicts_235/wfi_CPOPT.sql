--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CPOPT') as tablename,
wca.cpopt.actflag,
wca.cpopt.announcedate as created,
wca.cpopt.acttime as changed,
wca.cpopt.cpoptid,
wca.scmst.secid,
wca.scmst.isin,
wca.cpopt.callput, 
wca.cpopt.fromdate,
wca.cpopt.todate,
wca.cpopt.noticefrom,
wca.cpopt.noticeto,
wca.cpopt.currency,
wca.cpopt.price,
wca.cpopt.mandatoryoptional,
wca.cpopt.minnoticedays,
wca.cpopt.maxnoticedays,
wca.cpopt.cptype,
wca.cpopt.priceaspercent,
wca.cpopt.inwholepart,
wca.cpopt.formulabasedprice,
wca.cpopn.notes as notes 
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.cpopn on wca.cpopt.secid = wca.cpopn.secid and wca.cpopt.callput = wca.cpopn.callput
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.cpopt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
