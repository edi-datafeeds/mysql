--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('BSCHG') as tablename,
wca.bschg.actflag,
wca.bschg.announcedate as created,
wca.bschg.acttime as changed,
wca.bschg.bschgid,
wca.bschg.secid,
wca.scmst.isin,
wca.bschg.notificationdate,
wca.bschg.oldbondtype,
wca.bschg.newbondtype,
wca.bschg.oldcurencd,
wca.bschg.newcurencd,
wca.bschg.oldpiu,
wca.bschg.newpiu,
wca.bschg.oldinterestbasis,
wca.bschg.newinterestbasis,
wca.bschg.eventtype,
wca.bschg.oldinterestcurrency,
wca.bschg.newinterestcurrency,
wca.bschg.oldmaturitycurrency,
wca.bschg.newmaturitycurrency,
wca.bschg.oldintbusdayconv,
wca.bschg.newintbusdayconv,
wca.bschg.oldmatbusdayconv,
wca.bschg.newmatbusdayconv,
wca.bschg.bschgnotes as notes
from wca.bschg
inner join wca.bond on wca.bschg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.bschg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
