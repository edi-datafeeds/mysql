--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('ISCHG') as tablename,
wca.ischg.actflag,
wca.ischg.announcedate as created,
wca.ischg.acttime as changed,
wca.ischg.ischgid,
wca.bond.secid,
wca.scmst.isin,
wca.ischg.issid,
wca.ischg.namechangedate,
wca.ischg.issoldname,
wca.ischg.issnewname,
wca.ischg.eventtype,
wca.ischg.legalname,
wca.ischg.meetingdateflag, 
wca.ischg.ischgnotes as notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.ischg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
and (wca.bond.issuedate<=wca.ischg.namechangedate
     or wca.bond.issuedate is null or wca.ischg.namechangedate is null
    )

