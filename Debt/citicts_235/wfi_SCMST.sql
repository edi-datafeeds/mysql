--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCMST') as tablename,
wca.scmst.actflag,
wca.scmst.announcedate as created,
wca.scmst.acttime as changed,
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.issid,
wca.scmst.sectycd,
wca.scmst.securitydesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.primaryexchgcd,
wca.scmst.curencd,
wca.scmst.parvalue,
wca.scmst.uscode,
wca.scmst.x as commoncode,
wca.scmst.holding,
wca.scmst.structcd,
wca.scmst.regs144a
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and (wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
  or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)))

