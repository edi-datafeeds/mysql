--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('AGNCY') as tableName,
wca.agncy.actflag,
wca.agncy.announcedate as created,
wca.agncy.acttime as changed,
wca.agncy.agncyid,
wca.agncy.registrarname,
wca.agncy.add1,
wca.agncy.add2,
wca.agncy.add3,
wca.agncy.add4,
wca.agncy.add5,
wca.agncy.add6,
wca.agncy.city,
wca.agncy.cntrycd,
wca.agncy.website,
wca.agncy.contact1,
wca.agncy.tel1,
wca.agncy.fax1,
wca.agncy.email1,
wca.agncy.contact2,
wca.agncy.tel2,
wca.agncy.fax2,
wca.agncy.email2,
wca.agncy.depository
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.agncy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
