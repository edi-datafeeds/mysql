--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SDCHG') as tablename,
wca.sdchg.actflag,
wca.sdchg.announcedate as created, 
wca.sdchg.acttime as changed,
wca.sdchg.sdchgid,
wca.sdchg.secid,
wca.scmst.isin,
wca.sdchg.effectivedate,
wca.sdchg.eventtype,
wca.sdchg.oldsedol,
wca.sdchg.newsedol,
wca.sdchg.cntrycd as oldcntrycd,
wca.sdchg.newcntrycd,
wca.sdchg.rcntrycd as oldrcntrycd,
wca.sdchg.newrcntrycd
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.sdchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
