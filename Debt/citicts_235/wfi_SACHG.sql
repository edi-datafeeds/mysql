--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SACHG') as tablename,
wca.sachg.actflag,
wca.sachg.announcedate as created,
wca.sachg.acttime as changed,
wca.sachg.sachgid,
wca.sachg.scagyid,
wca.bond.secid,
wca.scmst.isin,
wca.sachg.effectivedate,
wca.sachg.oldrelationship,
wca.sachg.newrelationship,
wca.sachg.oldagncyid,
wca.sachg.oldspstartdate,
wca.sachg.newspstartdate,
wca.sachg.oldspenddate,
wca.sachg.newspenddate,
wca.sachg.oldguaranteetype,
wca.sachg.newguaranteetype
from wca.sachg
inner join wca.scagy on wca.sachg.scagyid = wca.scagy.scagyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.sachg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
