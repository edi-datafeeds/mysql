--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CONVT') as tablename,
wca.convt.actflag,
wca.convt.announcedate as created,
wca.convt.acttime as changed,
wca.convt.convtid,
wca.scmst.secid,
wca.scmst.isin,
wca.convt.fromdate,
wca.convt.todate,
wca.convt.rationew,
wca.convt.ratioold,
wca.convt.curencd,
wca.convt.price,
wca.convt.mandoptflag,
wca.convt.fractions,
wca.convt.fxrate,
wca.convt.partfinalflag,
wca.convt.priceaspercent,
wca.convt.sectycd as ressectycd,
wca.convt.ressecid,
resscmst.isin as resisin,
resissur.issuername as resissuername,
resscmst.securitydesc as ressecuritydesc,
wca.convt.convtnotes as notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = resissur.issid
where
wca.scmst.isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(scmst.isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.convt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
