--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('BKRPNOTES') as tablename,
wca.bkrp.actflag,
wca.bkrp.bkrpid,
wca.bkrp.bkrpnotes as notes
from wca.bkrp
where
wca.bkrp.issid in (
select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where client.pfisin.accid=235
and (wca.bkrp.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or client.pfisin.actflag='i'))
