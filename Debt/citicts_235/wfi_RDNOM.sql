--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('RDNOM') as tablename,
wca.rdnom.actflag,
wca.rdnom.announcedate as created,
wca.rdnom.acttime as changed, 
wca.rdnom.rdnomid,
wca.rdnom.secid,
wca.scmst.isin,
wca.rdnom.effectivedate,
wca.rdnom.olddenomination1,
wca.rdnom.olddenomination2,
wca.rdnom.olddenomination3,
wca.rdnom.olddenomination4,
wca.rdnom.olddenomination5,
wca.rdnom.olddenomination6,
wca.rdnom.olddenomination7,
wca.rdnom.oldminimumdenomination,
wca.rdnom.olddenominationmultiple,
wca.rdnom.newdenomination1,
wca.rdnom.newdenomination2,
wca.rdnom.newdenomination3,
wca.rdnom.newdenomination4,
wca.rdnom.newdenomination5,
wca.rdnom.newdenomination6,
wca.rdnom.newdenomination7,
wca.rdnom.newminimumdenomination,
wca.rdnom.newdenominationmultiple,
wca.rdnom.notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.rdnom.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
