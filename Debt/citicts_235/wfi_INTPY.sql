--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select distinct
'INTPY' as tablename,
case when wca.int_my.actflag = 'd' or wca.int_my.actflag='c' or wca.intpy.actflag is null then wca.int_my.actflag else wca.intpy.actflag end as actflag,
wca.int_my.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.int_my.acttime) and (wca.rd.acttime > wca.exdt.acttime) then wca.rd.acttime when (wca.exdt.acttime is not null) and (wca.exdt.acttime > wca.int_my.acttime) then wca.exdt.acttime else wca.int_my.acttime end as changed,
wca.scmst.secid,
wca.scmst.isin,
wca.intpy.bcurencd as debtcurrency,
wca.intpy.bparvalue as nominalvalue,
wca.scexh.exchgcd,
case when wca.intpy.optionid is null then '1' else wca.intpy.optionid end as OptionID,
wca.int_my.rdid,
wca.rd.recdate,
wca.exdt.exdate,
wca.exdt.paydate,
wca.int_my.interestfromdate,
wca.int_my.interesttodate,
wca.int_my.days,
wca.intpy.curencd as interest_currency,
wca.intpy.intrate as interest_rate,
wca.intpy.grossinterest,
wca.intpy.netinterest,
wca.intpy.domestictaxrate,
wca.intpy.nonresidenttaxrate,
wca.intpy.rescindinterest,
wca.intpy.agencyfees,
wca.intpy.couponno,
null as couponid,
wca.intpy.defaultopt,
wca.intpy.optelectiondate,
wca.intpy.anlcouprate,
wca.int_my.indefpay
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
(wca.scmst.isin in (select code from client.pfisin where accid=235 and actflag='i')
and wca.rd.actflag<>'d'
and wca.scmst.actflag<>'d'
and wca.bond.actflag<>'d'
and wca.issur.actflag<>'d'
and wca.int_my.actflag<>'d'
and wca.intpy.actflag<>'d')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=235 and actflag='u')
and (wca.intpy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.int_my.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.rd.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.exdt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)))

