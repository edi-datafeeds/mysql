--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('INTBC') as tablename,
wca.intbc.actflag,
wca.intbc.announcedate as created,
wca.intbc.acttime as changed,
wca.intbc.intbcid,
wca.intbc.secid,
wca.scmst.isin,
wca.intbc.effectivedate,
wca.intbc.oldintbasis,
wca.intbc.newintbasis,
wca.intbc.oldintbdc,
wca.intbc.newintbdc
from wca.intbc
inner join wca.bond on wca.intbc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.intbc.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
