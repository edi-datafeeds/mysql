--filepath=o:\upload\acc\235\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\235\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('FRNFX') as tablename,
wca.frnfx.actflag,
wca.frnfx.announcedate as created,
wca.frnfx.acttime as changed,
wca.frnfx.frnfxid,
wca.frnfx.secid,
wca.scmst.isin,
wca.frnfx.effectivedate,
wca.frnfx.oldfrntype,
wca.frnfx.oldfrnindexbenchmark,
wca.frnfx.oldmarkup,
wca.frnfx.oldminimuminterestrate,
wca.frnfx.oldmaximuminterestrate,
wca.frnfx.oldrounding,
wca.frnfx.newfrntype,
wca.frnfx.newfrnindexbenchmark,
wca.frnfx.newmarkup,
wca.frnfx.newminimuminterestrate,
wca.frnfx.newmaximuminterestrate,
wca.frnfx.newrounding,
wca.frnfx.eventtype,
wca.frnfx.oldfrnintadjfreq,
wca.frnfx.newfrnintadjfreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=235 and actflag='i')
or 
(isin in (select code from client.pfisin where accid=235 and actflag='u')
and wca.frnfx.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
