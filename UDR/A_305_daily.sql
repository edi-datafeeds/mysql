/*
FILE FORMAT OF DATA FEED 305_DAILY.SQL

Fieldname		Datatype(Maxwidth)	Notes
Uscode,			Char(9)
IssID,			Integer(10)		Unique (always populated) IssuerID
drSecID,		Integer(10)		DR SecurityID
unSecID,		Integer(10)		UN SecurityID
Changed,		Date(10)		Record changed on or Realign date
Announced,		Date(10)		DR create date
Actflag,		Char(1)			Always 'F' for Full align otherwise DPRCP.Actflag
DRSedol,		Char(7)
Symbol,			varchar(20)		DR US ticker
Exchange,		varchar(6)		DR exchange
Issuername,		varchar(70)
Descrip,		varchar(70)
drIsin,			Char(12)
drSos,			Char(25)		DR Sharesoutstanding
DRtype,			char(3)			ADR, GDR etc
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnIsin, 		Char(12)
unSos,			Char(25)		UN Sharesoutstanding
unExchange,		Char(10)
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Levdesc,		Varchar(10)
CntryofIncorp,		Char(2)			ISO Country of Incorporation
Indusname,		Char(70)		Industry Sector description (EDI proprietary)
Statusflag,		Char(1)			I (Inactive security)
Notes			ntext(memo)		DR notes
*/

--filepath=o:\datafeed\UDR\305dup\
--filenameprefix=
--filename=yyyymmdd
--FileHeaderDate=YYYYMMDD
--filenamealt=
--fileextension=.305
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n
--FileHeaderTEXT=EDI_UDR_305_Daily_
--FileFooterTEXT=EDI_ENDOFFILE




/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */


--# 1
use UDR
SELECT 
Uscode,
IssID,
Secid as drSecID,
unSecid,
getdate() as Changed,
Credate as Announced,
Actflag,
DRSedol,
Symbol,
Exchange,
Issuername,
Descrip,
drIsin,
drSos,
DRtype,
Ratio, 
UnDescrip,
unSedol,
UnIsin, 
unSos,
unExchange,
Sponsored,
Depbank, 
Levdesc,
CntryofIncorp,
Indusname,
Statusflag,
Additinfo as Notes
FROM UDRNEW

where 
(DPRCPActtime > getdate()-0.5 OR 
priSCEXHActtime > getdate()-0.5 OR 
ISSURActtime > getdate()-0.5 OR 
drSCMSTActtime > getdate()-0.5 OR 
unSCMSTActtime > getdate()-0.5 OR 
drusSEDOLActtime > getdate()-0.5 OR 
drSEDOLActtime > getdate()-0.5 OR 
unSEDOLActtime > getdate()-0.5 OR 
ungbSEDOLActtime > getdate()-0.5)
and (Statusflag<>'I' or drSCMSTActtime > getdate() - 7)
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END

