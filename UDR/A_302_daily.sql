
--filepath=o:\datafeed\UDR\302\
--filenameprefix=AL
--filename=yyyymmdd
--FileHeaderDate=
--filenamealt=
--fileextension=.302
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n
--FileHeaderTEXT=EDI_UDR_302_
--FileFooterTEXT=EDI_ENDOFFILE




--# 1
use udr

SELECT 
UDRNEWDIV.DRorUN,
UDRNEWDIV.id, 
DRSedol = CASE DRorUN WHEN 'DR' THEN UDRNEWDIV.drsedol ELSE '' END,
UNSedol = CASE DRorUN WHEN 'UN' THEN UDRNEWDIV.unsedol ELSE '' END,
UDRNEWDIV.Exdate, UDRNEWDIV.Paydate, 
UDRNEWDIV.Recdate, 
UDRNEWDIV.Currcode,
UDRNEWDIV.Payrate, 
UDRNEWDIV.Approxflag,
UDRNEWDIV.Taxflag, 
UDRNEWDIV.Grossrate,
UDRNEWDIV.Netrate, 
UDRNEWDIV.Taxrate, 
UDRNEWDIV.DivType,
UDRNEWDIV.Depfee, 
UDRNEWDIV.DivPeriod, 
UDRNEWDIV.Finalflag, 
wca.dbo.DIV.DivNotes as Notes,
UDRNEWDIV.Acttime, 
UDRNEWDIV.Creation
from UDRNEWDIV
left outer join wca.dbo.div on UDRNEWDIV.ID = wca.dbo.DIV.divid