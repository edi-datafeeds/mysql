/*
FILE FORMAT FOR DATA FEED UD_FULL.SQL

Fieldname Datatype(Maxwidth)	Notes
UscodeKey,		Char(10)		Uscode with A on the end (legacy)
Uscode,			Char(9)
Issuername,		varchar(70)
Descrip,		varchar(70)
DRSedol,		Char(7)
drIsin,			Char(12)
drTradstat,		Char(3)			LSE's DR trading status
Symbol,			varchar(20)		DR US ticker
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnIsin, 		Char(12)
unTradstat,		Char(3)			LSE's UN trading status
old01			null			redundant
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Exchange,		varchar(6)		DR exchange
CntryofIncorp,		Char(2)			ISO Country of Incorporation
old02			null			redundant
old03 etc. ..
old31			null			redundant
Acttime,		Date(10)		DR last modified date
*/


--filepath=o:\datafeed\UDR\UDdup\
--filenameprefix=UD
--filename=yymmdd
--filenamealt=
--fileextension=.TXT
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n







--# 1
use udr

SELECT 

'"'+ UscodeKey +'"'as '"USCODEKEY"',
'"'+ Uscode +'"'as '"USCODE"',
'"'+ Issuername +'"'as '"ISSUERNAME"',
'"'+ Descrip +'"'as '"DESCRIP"',
'"'+ DRSedol +'"'as '"DRSEDOL"',
'"'+ drIsin +'"'as '"DRISIN"',
'"'+ DrTradstat +'"'as '"DRTRADSTAT"',
'"'+ Symbol +'"'as '"SYMBOL"',
'"'+ Ratio +'"'as '"RATIO"', 
'"'+ UnDescrip +'"'as '"UNDESCRIP"',
'"'+ unSedol +'"'as '"UNSEDOL"',
'"'+ UnIsin +'"'as '"UNISIN"', 
'"'+ unTradstat +'"'as '"UNTRADSTAT"',
'""' as '"old01"',
'"'+ Sponsored +'"'as '"SPONSORED"',
'"'+ Depbank +'"'as '"DEPBANK"', 
'"'+ Exchange +'"'as '"EXCHANGE"',
'"'+ CntryofIncorp +'"'as '"CNTRYOFINCORP"',
'""' as '"old02"',
'""' as '"old03"',
'""' as '"old04"',
'""' as '"old05"',
'""' as '"old06"',
'""' as '"old07"',
'""' as '"old08"',
'""' as '"old09"',
'""' as '"old10"',
'""' as '"old11"',
'""' as '"old12"',
'""' as '"old13"',
'""' as '"old14"',
'""' as '"old15"',
'""' as '"old16"',
'""' as '"old17"',
'""' as '"old18"',
'""' as '"old19"',
'""' as '"old20"',
'""' as '"old21"',
'""' as '"old22"',
'""' as '"old23"',
'""' as '"old24"',
'""' as '"old25"',
'""' as '"old26"',
'""' as '"old27"',
'""' as '"old28"',
'""' as '"old29"',
'""' as '"old30"',
'""' as '"old31"',




drSCMSTActtime as Acttime
FROM UDRNEW
where 
DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END


