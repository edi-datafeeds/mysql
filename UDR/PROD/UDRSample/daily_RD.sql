-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_RD
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_RD_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

SELECT
upper('RD') as Tablename,
wca.rd.Actflag,
wca.rd.Acttime,
wca.rd.Announcedate,
wca.rd.RdID,
wca.rd.SecID,
wca.rd.RecDate as Fromdate,
wca.rd.ToDate,
wca.rd.RegistrationDate,
wca.rd.SectyCD,
wca.rd.RecCalc,
wca.rd.RDNotes as Notes
FROM wca.rd
inner join wca.dprcp on wca.rd.SecID = wca.dprcp.SecID
left outer join wca.exdt on wca.rd.RdID = wca.exdt.RdID
where
wca.exdt.rdid is null
and wca.rd.sectycd='DR'
and wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)