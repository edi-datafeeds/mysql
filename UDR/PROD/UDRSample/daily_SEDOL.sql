-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_SEDOL
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_SEDOL_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1 
select
upper('SEDOL') as TableName,
wca.SEDOL.Actflag,
wca.SEDOL.AnnounceDate,
wca.SEDOL.Acttime, 
wca.SEDOL.SedolId,
wca.SEDOL.SecID,
wca.SEDOL.CntryCD,
wca.SEDOL.Sedol,
wca.SEDOL.Defunct,
wca.SEDOL.RcntryCD
FROM wca.dprcp
inner join wca.sedol on wca.dprcp.secid = wca.sedol.secid
where 
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)

union

select
upper('SEDOL') as TableName,
wca.SEDOL.Actflag,
wca.SEDOL.AnnounceDate,
wca.SEDOL.Acttime, 
wca.SEDOL.SedolId,
wca.SEDOL.SecID,
wca.SEDOL.CntryCD,
wca.SEDOL.Sedol,
wca.SEDOL.Defunct,
wca.SEDOL.RcntryCD
FROM wca.dprcp
inner join wca.sedol on wca.dprcp.unsecid = wca.sedol.secid
where 
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)
