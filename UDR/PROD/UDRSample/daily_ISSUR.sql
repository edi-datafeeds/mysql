-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_ISSUR
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_ISSUR_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT distinct
upper('ISSUR') as TableName,
wca.ISSUR.Actflag,
wca.ISSUR.AnnounceDate,
wca.ISSUR.Acttime,
wca.ISSUR.IssID,
wca.ISSUR.IssuerName,
wca.ISSUR.IndusID,
wca.ISSUR.CntryofIncorp
FROM wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
INNER JOIN wca.issur ON wca.SCMST.IssID = wca.issur.IssID
where 
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)

