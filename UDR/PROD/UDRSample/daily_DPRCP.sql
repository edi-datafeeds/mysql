-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_DPRCP
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_DPRCP_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT
upper('DPRCP') as Tablename,
wca.DPRCP.Actflag,
wca.DPRCP.Acttime,
wca.DPRCP.Announcedate,
wca.DPRCP.SecID,
wca.DPRCP.UnSecID,
wca.DPRCP.DRRatio,
wca.DPRCP.USRatio,
wca.DPRCP.SpnFlag,
wca.DPRCP.DPRCPNotes,
wca.DPRCP.DRtype,
wca.DPRCP.DepBank,
wca.DPRCP.LevDesc,
wca.DPRCP.OtherDepBank
FROM wca.dprcp
WHERE wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)
