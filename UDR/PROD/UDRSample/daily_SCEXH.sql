-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_SCEXH
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_SCEXH_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT
upper('SCEXH') as TableName,
wca.SCEXH.Actflag,
wca.SCEXH.AnnounceDate,
wca.SCEXH.Acttime,
wca.SCEXH.ScExhID,
wca.SCEXH.SecID,
wca.SCEXH.ExchgCD,
case when wca.scexh.ListStatus='S' OR wca.scexh.ListStatus ='R' OR wca.scexh.ListStatus ='D' then wca.scexh.ListStatus else 'L' end as ListStatus,
wca.SCEXH.Lot,
wca.SCEXH.MinTrdgQty,
wca.SCEXH.ListDate,
wca.SCEXH.LocalCode,
wca.exchg.CntryCD,
wca.exchg.MIC
FROM wca.dprcp
inner join wca.scexh on wca.dprcp.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)
union
SELECT
upper('SCEXH') as TableName,
wca.SCEXH.Actflag,
wca.SCEXH.AnnounceDate,
wca.SCEXH.Acttime,
wca.SCEXH.ScExhID,
wca.SCEXH.SecID,
wca.SCEXH.ExchgCD,
case when wca.scexh.ListStatus='S' OR wca.scexh.ListStatus ='R' OR wca.scexh.ListStatus ='D' then wca.scexh.ListStatus else 'L' end as ListStatus,
wca.SCEXH.Lot,
wca.SCEXH.MinTrdgQty,
wca.SCEXH.ListDate,
wca.SCEXH.LocalCode,
wca.exchg.CntryCD,
wca.exchg.MIC
FROM wca.dprcp
inner join wca.scexh on wca.dprcp.unsecid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid);
