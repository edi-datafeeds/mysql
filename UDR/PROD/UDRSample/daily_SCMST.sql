-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_SCMST
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_SCMST_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Announcedate,
wca.scmst.Acttime,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.Statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding,
wca.scmst.Regs144a
FROM wca.dprcp
inner join wca.scmst on wca.dprcp.secid = wca.scmst.secid
WHERE
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)
and wca.scmst.secid is not null

union

SELECT
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Announcedate,
wca.scmst.Acttime,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.Statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding,
wca.scmst.Regs144a
FROM wca.dprcp
inner join wca.scmst on wca.dprcp.unsecid = wca.scmst.secid
WHERE
wca.DPRCP.secid in (select code from client.pfsecid where accid = @accid)
order by sectycd, isin 