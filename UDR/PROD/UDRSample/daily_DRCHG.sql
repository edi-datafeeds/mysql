-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_DRCHG
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_UDR_DRCHG_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT
upper('DRCHG') as Tablename,
wca.DRCHG.Actflag,
wca.DRCHG.Acttime,
wca.DRCHG.Announcedate,
wca.DRCHG.SecID,
wca.DRCHG.EffectiveDate,
wca.DRCHG.OldDRRatio,
wca.DRCHG.NewDRRatio,
wca.DRCHG.OldUNRatio,
wca.DRCHG.NewUNRatio,
wca.DRCHG.DRCHGID,
wca.DRCHG.OldUNSecID,
wca.DRCHG.NewUNSecID,
wca.DRCHG.Eventtype,
wca.DRCHG.OldDepbank,
wca.DRCHG.NewDepbank,
wca.DRCHG.OldDRtype,
wca.DRCHG.NewDRtype,
wca.DRCHG.OldLevel,
wca.DRCHG.NewLevel,
wca.DRCHG.DrchgNotes
FROM wca.DRCHG
WHERE wca.DRCHG.secid in (select code from client.pfsecid where accid = @accid)
