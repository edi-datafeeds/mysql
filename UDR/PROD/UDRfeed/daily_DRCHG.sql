--filepath=o:\Datafeed\UDR\UDRfeed\Refdata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_DRCHG
--fileheadertext=EDI_UDR_DRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\UDRfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('DRCHG') as Tablename,
DRCHG.Actflag,
DRCHG.Acttime,
DRCHG.Announcedate,
DRCHG.SecID,
DRCHG.EffectiveDate,
DRCHG.OldDRRatio,
DRCHG.NewDRRatio,
DRCHG.OldUNRatio,
DRCHG.NewUNRatio,
DRCHG.DRCHGID,
DRCHG.OldUNSecID,
DRCHG.NewUNSecID,
DRCHG.Eventtype,
DRCHG.OldDepbank,
DRCHG.NewDepbank,
DRCHG.OldDRtype,
DRCHG.NewDRtype,
DRCHG.OldLevel,
DRCHG.NewLevel,
DRCHG.DrchgNotes
FROM DRCHG
WHERE DRCHG.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
