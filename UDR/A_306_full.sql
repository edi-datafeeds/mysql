/*
FILE FORMAT OF DATA FEED 306_FULL.SQL

Fieldname		Datatype(Maxwidth)	Notes
Uscode,			Char(9)
IssID,			Integer(10)		Unique (always populated) IssuerID
DRSedol,		Char(7)
Symbol,			varchar(20)		DR US ticker
Exchange,		varchar(6)		DR exchange
Issuername,		varchar(70)
Descrip,		varchar(70)
drIsin,			Char(12)
DRtype,			char(3)			ADR, GDR etc
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnIsin, 		Char(12)
unExchange,		Char(10)
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Levdesc,		Varchar(10)
CntryofIncorp,		Char(2)			ISO Country of Incorporation
Acttime,		Date(10)		DR last modified date
Credate,		Date(10)		DR create date
Statusflag,		Char(1)			I (Inactive security)
DPRCPNotes		ntext(memo)		DR notes
*/


--filepath=o:\datafeed\UDR\306dup\
--filenameprefix=AL
--filename=yymmdd
--FileHeaderDate=
--filenamealt=
--fileextension=.306
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n
--FileHeaderTEXT=EDI_UDR_306_
--FileFooterTEXT=EDI_ENDOFFILE
-




--# 1
use UDR
SELECT 
Uscode,
IssID,
DRSedol,
Symbol,
Exchange,
Issuername,
Descrip,
drIsin,
DRtype,
Ratio, 
UnDescrip,
unSedol,
UnIsin, 
Exchange as PrimaryExchgCD,
Sponsored,
Depbank, 
Levdesc,
CntryofIncorp,
drSCMSTActtime as Acttime,
Credate,
Statusflag,
Additinfo as DPRCPNotes
FROM UDRNEW

where (Statusflag<>'I' or drSCMSTActtime > getdate() - 7)
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END

