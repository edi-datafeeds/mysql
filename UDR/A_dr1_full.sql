/*
FILE FORMAT FOR DATA FEED DR1_FULL.SQL

where (Statusflag<>'I' or drScmstActtime > getdate() - 7)

Fieldname		Datatype(Maxwidth)	Notes
UscodeKey,		Char(10)		Uscode with A on the end (legacy)
Uscode,			Char(9)
IssID,			Integer(10)		Unique (always populated) IssuerID
Comcode,		Char(0)			Legacy
DRSedol,		Char(7)
Symbol,			varchar(20)		DR US ticker
Exchange,		varchar(6)		DR exchange
Issuername,		varchar(70)
Descrip,		varchar(70)
drIsin,			Char(12)
drTradstat,		Char(3)			LSE's DR trading status
drSos,			varchar(20)		DR sharesoutstanding
DRtype,			char(3)			ADR, GDR etc
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnIsin, 		Char(12)
SMFLocalcode,		Char(0)			Legacy
unTradstat,		Char(3)			LSE's UN trading status
unSos,			varchar(20)		UN sharesoutstanding
unExchange,		Char(10)
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Levdesc,		Varchar(10)
CntryofIncorp,		Char(2)			ISO Country of Incorporation
WorldvestSector,	Char(0)			Legacy
Acttime,		Date(10)		DR last modified date
Credate,		Date(10)		DR create date
Statusflag,		Char(1)			I (Inactive security)
DPRCPNotes		ntext(memo)		DR notes
*/

--filepath=o:\datafeed\UDR\DR1dup\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.DR1
--suffix=
--fileheadertext=
/*--fileheaderdate=yyyymmdd*/
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n


/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYYMMDD
--DataTimeFormat=
--ForceTime

--# 1
use udr

SELECT
UscodeKey,
udr.dbo.udrnew.Uscode,
IssID,
'' as Comcode,
DRSedol,
Symbol,
Exchange,
Issuername,
Descrip,
drIsin,
DrTradstat,
drSos,
DRtype,
Ratio, 
UnDescrip,
unSedol,
UnIsin, 
'' as SMFLocalcode,
unTradstat,
unSos,
unExchange,
Sponsored,
Depbank, 
case when sponsored = 'U' then '' else Levdesc end as Levdesc,
CntryofIncorp,
'' as WorldvestSector,
drSCMSTActtime as Acttime,
Credate,
Statusflag,
Additinfo as DPRCPNotes
FROM UDRNEW
LEFT OUTER JOIN DBnoGO ON udrnew.Uscode = DBnoGO.uscode
where (Statusflag<>'I' or drScmstActtime > getdate() - 7)
and DBnoGO.uscode IS NULL
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'

order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END

