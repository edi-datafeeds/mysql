--filepath=o:\Datafeed\UDR\MEL\
--filenameprefix=
--filename=UDR_MEL
--filenamealt=
--fileextension=.TXT
--suffix=
--fileheadertext=dd/MM/yyyy EDI CORE nnnnnnn
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=FW
--archive=y
--archivepath=n:\WSO\503mel\
--fieldheaders=n
--filetidy=
--fwoffsets=LS13,LS10,LS35,LS1,LS40,LS1,LS8,LS13,LS4,LS16,Rz30,LS1,LS4,LS22,LS16,LS40,LS1,LS8,LS13,LS10,Rz30,LS5,LS2,LS51,LS11,LS255,LS1,LS11,LS9,LS11,LS9
--incremental=
--shownulls=





--# 1
use udr
SELECT
uscodekey,
uscode,
issuername,
' ' as dummy,
descrip,
' ' as dummy,
drsedol,
drisin,
drtradstat,
symbol,
drsos,
' ' as dummy,
drtype,
' ' as dummy1,
upper(ratio) as ratio,
UnDescrip,
' ' as dummy,
unsedol,
unisin,
' ' as dummy3,
unsos,
' ' as dummy4,
sponsored,
depbank,
exchange,
additinfo,
' ' as dummy5,
drscmstacttime,
CONVERT ( varchar , drscmstacttime,108) as dracttime,
datelisted,
case when datelisted is not null then '00:00:00' else '' end as dummytime
from udrnew
order by uscodekey
