--filepath=o:\datafeed\UDR\308\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=
--fileextension=.308
--suffix=
--fileheadertext=EDI_UDR_RATIOCHANGE_
--fileheaderdate=YYYYMMDD
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=
--fwoffsets=
--sevent=n
--shownulls=n



Declare @AlignDate datetime
Declare @StartDate datetime
Declare @EndDate datetime

set @AlignDate = '2002/03/01'
set @StartDate = getDate()-30
set @EndDate = getDate()+1



/* Use script Var and Sets*/
--UseIt @AlignDate
--UseIt @StartDate
--UseIt @EndDate

--# 1
USE WCA
SELECT * 
FROM v51f_600_Depositary_Receipt_Change
WHERE (CHANGED BETWEEN getDate()-30 AND getDate()+1) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol
