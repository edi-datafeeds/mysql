-- arc=n
-- arp=n:\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Rockall_P04_Lasttrade_USD
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_Rockall_P04_Lasttrade_USD_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct prices.lasttrade.*
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where
prices.lasttrade.isin<>''
and prices.lasttrade.currency='USD'
and prices.lasttrade.pricedate>'2017/12/31'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57) or (wca.sectygrp.secgrpid<3))
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D'
order by prices.lasttrade.exchgcd;
