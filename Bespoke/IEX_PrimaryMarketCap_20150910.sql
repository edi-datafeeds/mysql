-- arc=y
-- arp=n:\bespoke\iex\
-- fsx=_US_Primary
-- hpx=EDI_US_Primary_
-- hdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fty=y

-- # 1
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
prices.lasttrade.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when prices.lasttrade.mic='XOTC' or prices.lasttrade.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
case when prices.lasttrade.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.lasttrade.comment like '%when issued%' then 'Y' else '' end as WIflag,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor,
prices.lasttrade.comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join client.tholdsec on prices.lasttrade.localcode=client.tholdsec.symbol
                      and prices.lasttrade.mktclosedate=client.tholdsec.feeddate
left outer join wca.bbc on prices.lasttrade.secid = wca.bbc.secid and substring(prices.lasttrade.exchgcd,1,2) = wca.bbc.cntrycd and prices.lasttrade.currency = wca.bbc.curencd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on prices.lasttrade.secid = wca.bbe.secid and substring(prices.lasttrade.exchgcd,1,2) = substring(wca.bbe.exchgcd,1,2) and prices.lasttrade.currency=bbe.curencd and 'D'<>wca.bbe.actflag
left outer join prices.ajfactor on prices.lasttrade.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = prices.lasttrade.mic
                      and prices.ajfactor.exdate = date_format(now(),'%Y-%m-%d')
                      and 1=prices.ajfactor.option
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>'2006/12/31')
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and prices.lasttrade.exchgcd<>'USCOMP')
or (wca.scmst.uscode = '' and prices.lasttrade.sectycd <>'BND' and prices.lasttrade.exchgcd<>'USCOMP')
or ((prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB') and prices.lasttrade.secid in (select secid from wca.scexh where exchgcd = 'USBATS' 
                                           and wca.scexh.actflag <> 'D' and wca.scexh.liststatus <> 'D')))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and prices.lasttrade.exchgcd<>'USTRCE'
and ((prices.lasttrade.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd='' or wca.scmst.primaryexchgcd is null)
or (substring(wca.scmst.primaryexchgcd,1,2)='CA' and substring(prices.lasttrade.exchgcd,1,2)='US' and prices.lasttrade.exchgcd<>'USCOMP'));
