-- arc=y
-- arp=n:\bespoke\iex\
-- fsx=_US_Primary
-- hpx=EDI_US_Primary_
-- hdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fty=y

-- # 1
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
prices.lasttrade.ExchgCD,
wca.scmst.SecurityDesc,
'' as Lotsize,
null as listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*wca.scmst.sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
case when prices.lasttrade.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.lasttrade.comment like '%when issued%' then 'Y' else '' end as WIflag,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor,
prices.lasttrade.comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join client.tholdsec on prices.lasttrade.localcode=client.tholdsec.symbol
                      and prices.lasttrade.mktclosedate=client.tholdsec.feeddate
left outer join wca.bbc on prices.lasttrade.secid = wca.bbc.secid 
                      and substring(prices.lasttrade.exchgcd,1,2) = wca.bbc.cntrycd 
                      and prices.lasttrade.currency = wca.bbc.curencd
                      and 'D'<>wca.bbc.actflag
left outer join wca.bbe on prices.lasttrade.secid = wca.bbe.secid 
                      and prices.lasttrade.exchgcd = wca.bbe.exchgcd
                      and prices.lasttrade.currency=bbe.curencd
                      and 'D'<>wca.bbe.actflag
left outer join prices.ajfactor on prices.lasttrade.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = prices.lasttrade.mic
                      and prices.ajfactor.exdate = date_format(now(),'%Y-%m-%d')
                      and 1=prices.ajfactor.option
where
wca.scexh.exchgcd is null
and (prices.lasttrade.pricedate is null or  prices.lasttrade.pricedate>date_sub(date_format(now(),'%Y-%m-%d'), interval 7 day))
and (prices.lasttrade.exchgcd='USNASD' or prices.lasttrade.exchgcd='USNYSE' or prices.lasttrade.exchgcd='USPAC' or prices.lasttrade.exchgcd='USAMEX')
and prices.lasttrade.currency='USD'
and 1=(select count(secid) from wca.scexh as sub1
where 
prices.lasttrade.secid = sub1.secid 
and (sub1.exchgcd='USNASD' or sub1.exchgcd='USNYSE' or sub1.exchgcd='USPAC' or sub1.exchgcd='USAMEX')
and sub1.liststatus<>'D' and sub1.actflag<>'D')
and 1=(select count(sub2.secid) from prices.lasttrade as sub2
where 
prices.lasttrade.secid = sub2.secid 
and (sub2.exchgcd='USNASD' or sub2.exchgcd='USNYSE' or sub2.exchgcd='USPAC' or sub2.exchgcd='USAMEX')
and (sub2.pricedate is null or sub2.pricedate>date_sub(date_format(now(),'%Y-%m-%d'), interval 31 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
order by prices.lasttrade.localcode;
