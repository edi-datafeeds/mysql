use prices;
delete from prices.prices_spaf;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
client.pfsecid.code as SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsecid
inner join prices.lasttrade on client.pfsecid.code = prices.lasttrade.secid
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfsecid.accid = 372 and client.pfsecid.actflag<>'D'
and wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and wca.exchg.cntrycd<>'US'
and prices.lasttrade.isin<>''
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
order by isin, pricedate desc;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
client.pfsecid.code as SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsecid
left outer join prices.lasttrade on client.pfsecid.code = prices.lasttrade.secid
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfsecid.accid = 372 and client.pfsecid.actflag<>'D'
and wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and wca.exchg.cntrycd='US'
and prices.lasttrade.isin<>''
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by pricedate desc, latest.currency desc limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
order by isin, pricedate desc;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 372 and client.pfisin.actflag<>'D'
and wca.exchg.cntrycd<>'US'
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and isinlatest.isin=prices.lasttrade.isin
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, isinlatest.currency limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1))
order by isin, pricedate desc;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 372 and client.pfisin.actflag<>'D'
and wca.exchg.cntrycd='US'
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and isinlatest.isin=prices.lasttrade.isin
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, isinlatest.currency desc limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency desc limit 1))
order by isin, pricedate desc;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfisin.accid = 372 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) ='ES'
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.exchgcd='ESSIBE'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
'ESSIBE'=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
'ESSIBE'=prices.lasttrade.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
client.pfsecid.code as SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsecid
left outer join prices.lasttrade on client.pfsecid.code = prices.lasttrade.secid
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
                                                   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                                                   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
client.pfsecid.accid = 372 and client.pfsecid.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) ='ES'
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.exchgcd='ESSIBE'
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
 (select localcode from prices.lasttrade as latest
where
'ESSIBE'=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
'ESSIBE'=prices.lasttrade.exchgcd
and latestscmst.secid=wca.scmst.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
client.pfisin.code as Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisin
inner join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin
where 
client.pfisin.accid = 372 and client.pfisin.actflag<>'D'
and prices.lasttrade.isin<>''
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.exchgcd<>'USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
where
isinlatest.isin<>''
and isinlatest.isin=prices.lasttrade.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc limit 1)
order by prices.lasttrade.isin;
insert ignore into prices.prices_spaf
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
client.pfsecid.code as SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.USCode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfsecid
inner join prices.lasttrade on client.pfsecid.code = prices.lasttrade.secid
where 
client.pfsecid.accid = 372 and client.pfsecid.actflag<>'D'
and prices.lasttrade.isin<>''
and prices.lasttrade.isin not in (select isin from prices.prices_spaf)
and prices.lasttrade.exchgcd<>'USCOMP'
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1))
order by prices.lasttrade.isin;
insert ignore into prices.prices_spaf
select distinct
'' as MIC,
'' as LocalCode,
client.pfisin.code as Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
null as MktCloseDate,
'' as Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
wca.scmst.USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
'' as Comment
from client.pfisin
LEFT OUTER JOIN wca.scmst on client.pfisin.secid = wca.scmst.secid
LEFT OUTER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where client.pfisin.accid=372 and client.pfisin.actflag<>'D' and code not in (select isin from prices.prices_spaf);