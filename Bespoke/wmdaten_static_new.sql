-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=_Static
-- headerprefix=EDI_WCA_Static_Change_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=;
-- archive=n
-- archivepath=n:\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
select 
'StaticCD' as f1,
'EventID' as f2,
'Actflag' as f3,
'ScexhID' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'SecurityDesc' as f3,
'PrimaryExchgCD' as f3,
'ExchgCD' as f3,	
'TradingCurrency' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'MktSegment' as f3,	
'Lot' as f3,	
'LocalCode' as f3,	
'Eventtype' as f14,
'EffectiveDate' as f14,
'OldStatic' as f34,
'NewStatic' as f36;

-- # 2
select
upper('ISIN') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.EffectiveDate,
vtab.OldIsin as OldStatic,
vtab.NewIsin as NewStatic
from wca.v10s_icc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldIsin <> '' or vtab.NewIsin <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 3
select
upper('USCODE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.EffectiveDate,
vtab.OldUscode as OldStatic,
vtab.NewUscode as NewStatic
from wca.v10s_icc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldUscode <> '' or vtab.NewUscode <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 4
select
upper('LOCAL') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.EffectiveDate,
vtab.OldLocalcode as OldStatic,
vtab.NewLocalcode as NewStatic
from wca.v10s_lcc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldLocalcode <> '' or vtab.NewLocalcode <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 5
select
upper('SEDOL') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.EffectiveDate,
vtab.OldSedol as OldStatic,
vtab.NewSedol as NewStatic
from wca.v10s_sdchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldSedol <> '' or vtab.NewSedol <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 6
select
upper('SECNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.DateofChange as EffectiveDate,
vtab.SecOldName as OldStatic,
vtab.SecNewName as NewStatic
from wca.v10s_scchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.DateofChange>=date_sub(now(), interval 1 day) and vtab.DateofChange<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.DateofChange<date_sub(now(), interval 1 day)))
and (vtab.SecOldName <> vtab.SecNewName <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 7
select
upper('NEWLIST') as StaticCD,
wca.scexh.ScexhID as EventID,
wca.scexh.actflag as Actflag,
wca.scexh.ScexhID,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
'' as eventtype,
wca.scexh.listdate as EffectiveDate,
'' as OldStatic,
'' as NewStatic
from wca.scexh
inner join wca.v20c_680_scmst as stab on wca.scexh.secid = wca.stab.secid
LEFT OUTER JOIN wca.nlist ON wca.scexh.scexhid = wca.nlist.scexhid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((wca.scexh.listdate>=date_sub(now(), interval 1 day) and wca.scexh.listdate<now())
or
(wca.scexh.listdate is null and (wca.nlist.acttime>date_sub(now(), interval 1 day)
                                 or wca.scexh.announcedate>date_sub(now(), interval 1 day))))
and wca.scexh.actflag<>'D';  

-- # 8
select
upper('ISSNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.NameChangeDate as EffectiveDate,
vtab.IssOldName as OldStatic,
vtab.IssNewName as NewStatic
from wca.v10s_ischg as vtab
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.NameChangeDate>=date_sub(now(), interval 1 day) and vtab.NameChangeDate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.NameChangeDate<date_sub(now(), interval 1 day)))
and (vtab.IssOldName <> vtab.IssNewName <> '')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 9
select
upper('INCORP') as StaticCD,
vtab.EventID,
wca.scexh.ScexhID,
vtab.actflag as Actflag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.InChgDate as EffectiveDate,
vtab.OldCntryCD as OldStatic,
vtab.NewCntryCD as NewStatic
from wca.v10s_inchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.InChgDate>=date_sub(now(), interval 1 day) and vtab.InChgDate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.InChgDate<date_sub(now(), interval 1 day)))
and (vtab.OldCntryCD <> vtab.NewCntryCD)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 10
select
upper('LISTSTATUS') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
vtab.eventtype,
vtab.EffectiveDate,
'' as OldStatic,
vtab.LstatStatus as NewStatic
from wca.v10s_lstat as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and ((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<@todate)
or
(vtab.acttime>date_sub(now(), interval 1 day)and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.LstatStatus <> '')
and wca.scexh.actflag<>'D';

-- # 11
select
upper('IPO') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
prices.lasttrade.Currency as TradingCurrency,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
mktsg.MktSegment,
wca.scexh.Lot,
wca.scexh.LocalCode,
'' as eventtype,
vtab.FirstTradingDate as EffectiveDate,
'' as OldStatic,
'' as NewStatic
from wca.v10s_ipo as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd= prices.lasttrade.exchgcd 
                                and wca.scexh.secid= prices.lasttrade.secid 
WHERE
(wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='PTBVL')
and vtab.FirstTradingDate>=date_sub(now(), interval 1 day) and vtab.FirstTradingDate<@todate
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';
