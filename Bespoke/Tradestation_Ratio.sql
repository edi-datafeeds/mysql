-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=_Ratio
-- headerprefix=EDI_WCA_Ratio_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
select
'StaticCD' as f1,
'EventID' as f2,
'Actflag' as f3,
'ScexhID' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'SecurityDesc' as f3,
'PrimaryExchgCD' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'Exdate' as f15,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'OutturnUScode' as f24,
'OutturnLocalCode' as f26,
'RatioOld' as f25,
'RatioNew' as f26;

-- # 2
select
upper('CONSD') as EventCD,
vtab.EventID,
case when vtab.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.newisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.newuscode end as Uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Exdate,
stab.secid as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then '' else wca.icc.newisin end as OutturnIsin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then '' else wca.icc.newuscode end as OutturnUScode,
case when (wca.icc.releventid is null or wca.lcc.oldlocalcode = '') then '' else wca.lcc.newlocalcode end as OutturnLocalCode,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld
from wca.v10s_consd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
WHERE
((wca.exdt.ExDate>=date_sub(now(), interval 1 day)
and wca.exdt.ExDate<now())
or
(pexdt.ExDate>=date_sub(now(), interval 1 day)
and pexdt.ExDate<now()))
and (wca.scexh.exchgcd = 'USNYSE'
  or wca.scexh.exchgcd = 'USNASD'
  or wca.scexh.exchgcd = 'USOTC')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 3
select
upper('SD') as EventCD,
vtab.EventID,
case when vtab.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.newisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.newuscode end as Uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Exdate,
stab.secid as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then '' else wca.icc.newisin end as OutturnIsin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then '' else wca.icc.newuscode end as OutturnUScode,
case when (wca.icc.releventid is null or wca.lcc.oldlocalcode = '') then '' else wca.lcc.newlocalcode end as OutturnLocalCode,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld
from wca.v10s_sd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
WHERE
((wca.exdt.ExDate>=date_sub(now(), interval 1 day)
and wca.exdt.ExDate<now())
or
(pexdt.ExDate>=date_sub(now(), interval 1 day)
and pexdt.ExDate<now()))
and (wca.scexh.exchgcd = 'USNYSE'
  or wca.scexh.exchgcd = 'USNASD'
  or wca.scexh.exchgcd = 'USOTC')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D';

-- # 4
select
upper('STOCKDIV') as EventCD,
vtab.EventID,
case when vtab.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Exdate,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
resscmst.UScode as OutturnUScode,
'' as OutturnLocalcode,
wca.divpy.RatioNew,
wca.divpy.RatioOld
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.v10s_divpy as divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
WHERE
((wca.exdt.ExDate>=date_sub(now(), interval 1 day)
and wca.exdt.ExDate<now())
or
(pexdt.ExDate>=date_sub(now(), interval 1 day)
and pexdt.ExDate<now()))
and (wca.scexh.exchgcd = 'USNYSE'
  or wca.scexh.exchgcd = 'USNASD'
  or wca.scexh.exchgcd = 'USOTC')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and (wca.divpy.divtype = 'S' or wca.divpy.divtype = 'B')
and wca.divpy.rationew <> ''
and wca.divpy.ratioold <> '';
