-- arc=y
-- arp=n:\bespoke\bluematrix\
-- fsx=_HistoricalPrices
-- hpx=EDI_BlueMatrix_HistoricalPrices_
-- hdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1
select distinct
client.pfcomptk.code,
client.pdat296.*,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor
from client.pfcomptk
left outer join wca.bbc on code = BbgComptk
left outer join wca.scexh on wca.bbc.cntrycd=substring(wca.scexh.exchgcd,1,2)
		             and wca.bbc.secid=wca.scexh.secid
                             and 'D'<>wca.scexh.liststatus
                             and 'D'<>wca.scexh.actflag
left outer join client.pdat296 on wca.bbc.secid = client.pdat296.secid
                      and client.pdat296.exchgcd=wca.scexh.exchgcd
left outer join prices.ajfactor on client.pdat296.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = client.pdat296.mic
                      and prices.ajfactor.exdate = client.pdat296.mktclosedate
                      and 1=prices.ajfactor.option
Where
client.pfcomptk.accid=296
and client.pfcomptk.actflag='I'
and (client.pdat296.mic is not null or wca.scexh.secid is null);

