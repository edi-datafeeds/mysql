-- arp=n:\bespoke\fundsaxis\
-- fex=.txt
-- fty=y
-- hpx=EDI_WCA_Shares_Outstanding_
-- fpx=FundsAxis_SOS_

-- # 1

select
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
case when wca.shh.Actflag='D' or wca.scmst.actflag='D' then 'D' else '' end as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
wca.scmst.VotePerSec,
case when wca.shh.shochid is not null
     then wca.scmst.votepersec*wca.shh.newsos
     else wca.scmst.votepersec*wca.scmst.sharesoutstanding
     end as TotalVotingRights,
'' as shochNotes
FROM wca.scmst
inner JOIN wca.sectygrp on wca.scmst.SectyCD = wca.sectygrp.SectyCD and 3>secgrpid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where
wca.exchg.cntrycd = 'GB'
and (shh.shochid is not null or wca.scmst.sharesoutstanding <> '')
and (shh.acttime >= (select max(acttime) from wca.tbl_opslog where seq = 1)
    or wca.scmst.announcedate >= (select max(acttime) from wca.tbl_opslog where seq = 1)
    or wca.scmst.sharesoutstandingdate is not null and shh.shochid is null)
