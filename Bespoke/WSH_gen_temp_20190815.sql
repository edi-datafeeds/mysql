use prices;
drop table if exists prices.WSH_temp;
CREATE TABLE `WSH_temp` (
  `ScexhID` int(10) unsigned DEFAULT '0',
  `Isin` char(12) NOT NULL,
  `Uscode` varchar(9) DEFAULT NULL,
  `IssuerName` varchar(70) DEFAULT '',
  `CntryofIncorp` char(2) DEFAULT NULL,
  `SIC` varchar(10) DEFAULT NULL,
  `CIK` varchar(10) DEFAULT NULL,
  `LEI` varchar(20) DEFAULT NULL,
  `NAICS` varchar(6) DEFAULT NULL,
  `CFI` varchar(10) DEFAULT NULL,
  `CIN` char(0) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ShellComp` char(1) DEFAULT NULL,
  `CIC` varchar(4) DEFAULT NULL,
  `FISN` varchar(35) DEFAULT NULL,
  `IndusID` int(10) DEFAULT NULL,
  `SectyCD` char(3) DEFAULT NULL,
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `ParValue` varchar(20) DEFAULT NULL,
  `PVCurrency` char(3) DEFAULT NULL,
  `StatusFlag` varchar(1) DEFAULT NULL,
  `PrimaryExchgCD` varchar(6) DEFAULT NULL,
  `BbgCurrency` char(3) DEFAULT '',
  `BbgCompositeGlobalID` char(12) DEFAULT NULL,
  `BbgCompositeTicker` varchar(40) DEFAULT NULL,
  `BbgGlobalID` varchar(12) DEFAULT NULL,
  `BbgExchangeTicker` varchar(40) DEFAULT NULL,
  `StructCD` varchar(10) DEFAULT NULL,
  `ExchgCntry` char(2) DEFAULT '',
  `ExchgCD` varchar(9),
  `MIC` varchar(6) DEFAULT NULL,
  `Micseg` varchar(6) DEFAULT NULL,
  `LocalCode` varchar(80) DEFAULT '',
  `ListStatus` varchar(1) DEFAULT NULL,
  `Close` varchar(20) DEFAULT NULL,
  `MktCloseDate` date DEFAULT NULL,
  `Shares` varchar(25) DEFAULT NULL,
  `Currency` char(3) DEFAULT '',
  `USD_Market_Cap` double(17,0) DEFAULT NULL,
  `HOAdd1` varchar(50) DEFAULT NULL,
  `HOAdd2` varchar(50) DEFAULT NULL,
  `HOAdd3` varchar(50) DEFAULT NULL,
  `HOAdd4` varchar(50) DEFAULT NULL,
  `HOAdd5` varchar(50) DEFAULT NULL,
  `HOAdd6` varchar(50) DEFAULT NULL,
  `HOCity` varchar(50) DEFAULT NULL,
  `HOState` varchar(25) DEFAULT NULL,
  `HOCntryCD` char(2) DEFAULT NULL,
  `HOPostcode` varchar(15) DEFAULT NULL,
  `HOTel` varchar(50) DEFAULT NULL,
  `HOFax` varchar(50) DEFAULT NULL,
  `HOEmail` varchar(50) DEFAULT NULL,
  `Website` varchar(50) DEFAULT NULL,
  `UnSecID` int(10) DEFAULT NULL,
  `DRRatio` varchar(20) DEFAULT NULL,
  `UNRatio` varchar(20) DEFAULT NULL,
  `UNIsin` char(12) DEFAULT NULL,
  PRIMARY KEY (`Isin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'US'
and wca.scmst.primaryexchgcd<>'USOTC'
and wca.scmst.primaryexchgcd<>'USBATS'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=latest.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency desc, pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=isinlatest.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency desc, pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and prices.lasttrade.comment like '%T+2%'
and substring(wca.scmst.primaryexchgcd,1,2) = 'RU'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.comment like '%T+2%'
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and isinlatest.comment like '%T+2%'
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency, isinlatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) = 'BR'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.currency, latest.pricedate desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.currency, isinlatest.pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and 'USOTC'=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and 'USOTC'=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd = 'USOTCB'
and wca.scexh.exchgcd = 'USOTC'
and wca.scmst.primaryexchgcd = 'USOTC'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USOTCB'
and latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, currency desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd = 'USOTCB'
and wca.scmst.primaryexchgcd = 'USOTC'
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc  limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and 'USBATS'=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and 'USBATS'=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd = 'USBATY'
and wca.scmst.primaryexchgcd = 'USBATS'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USBATY'
and latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, currency desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd = 'USBATY'
and wca.scmst.primaryexchgcd = 'USBATS'
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc  limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and prices.lasttrade.primaryexchgcd=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and substring(wca.scmst.primaryexchgcd,1,2) not in ('BR','RU','US')
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
wca.scmst.primaryexchgcd=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd=wca.scexh.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D' and code not in (select isin from prices.WSH_temp)
and substring(wca.scmst.primaryexchgcd,1,2) ='ES'
and prices.lasttrade.exchgcd='ESSIBE'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
'ESSIBE'=prices.lasttrade.exchgcd
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
'ESSIBE'=prices.lasttrade.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D' and code not in (select isin from prices.WSH_temp)
and prices.lasttrade.exchgcd<>'USCOMP'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.exchgcd=
(select exchgcd from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, latest.currency desc limit 1)
and prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
where
isinlatest.isin<>''
and isinlatest.isin=prices.lasttrade.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc limit 1)
and (wca.scmst.actflag<>'D' or wca.scmst.actflag is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
order by prices.lasttrade.isin;
insert ignore into prices.WSH_temp
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
'' as Close,
'' ascMktCloseDate,
wca.scmst.sharesoutstanding as Shares,
'' as Currency,
'' as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and wca.scmst.primaryexchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D' and code not in (select isin from prices.WSH_temp);
