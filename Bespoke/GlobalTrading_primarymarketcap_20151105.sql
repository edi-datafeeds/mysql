-- arp=n:\Bespoke\GlobalTrading\
-- fsx=_US_Primary
-- fty=y
-- dsp=select char(44)
-- fex=.csv

-- # 1

select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
replace(wca.issur.issuername,',','') as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
prices.lasttrade.ExchgCD,
replace(wca.scmst.SecurityDesc,',','') as SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as usd_market_cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 183 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
or prices.lasttrade.sectycd <>'BND'
or wca.scmst.sectycd = 'ETF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USCOMP'
union
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
replace(wca.issur.issuername,',','') as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
case when prices.lasttrade.ExchgCD='USCOMP' or prices.lasttrade.ExchgCD='USOTCB' then 'USBATS' else prices.lasttrade.ExchgCD end as ExchgCD,
replace(wca.scmst.SecurityDesc,',','') as SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as usd_market_cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and ((prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB') and wca.scexh.exchgcd='USBATS')
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>'2006/12/31')
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 183 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
or (prices.lasttrade.sectycd <>'BND'))
or (wca.scmst.sectycd = 'ETF')
or ((prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB') 
              and prices.lasttrade.secid in (select secid from wca.scexh where exchgcd = 'USBATS' 
              and wca.scexh.actflag <> 'D' and wca.scexh.liststatus <> 'D' and wca.scexh.liststatus <> 'S')))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
