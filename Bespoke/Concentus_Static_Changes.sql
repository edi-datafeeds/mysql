-- arc=y
-- arp=n:\bespoke\concentus\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=n
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_StaticDataChanges
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_StaticDataChanges_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE


-- # 1
select distinct
upper('BBC') as Tablename,
main.Actflag,
main.AnnounceDate,
main.Acttime,
main.BbcID,
main.SecID,
main.CntryCD,
main.CurenCD,
main.BbgCompID,
main.BbgCompTk
from wca.bbc as main
inner join wca.scmst on main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.bbe on main.secid = wca.bbe.secid and substring(wca.bbe.exchgcd,1,2) = main.cntrycd and main.curencd=wca.bbe.curencd
WHERE 
main.cntrycd='US'
and main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)


-- # 2
SELECT distinct
upper('BBE') as Tablename,
main.Actflag,
main.Announcedate,
main.Acttime,
main.BbeID,
main.SecID,
main.ExchgCD,
main.CurenCD,
main.BbgExhID,
main.BbgExhTk
from wca.bbe as main
inner join wca.scmst on main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE 
substring(main.exchgcd,1,2)='US'
and main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)


-- # 3
SELECT
upper('BBCC') as Tablename,
main.Actflag,
main.Announcedate,
main.Acttime,
main.BbccID,
main.SecID,
main.BbcID,
main.OldCntryCD,
main.OldCurenCD,
main.Effectivedate,
main.NewCntryCD,
main.NewCurenCD,
main.OldBbgCompID,
main.NewBbgCompID,
main.OldBbgCompTk,
main.NewBbgCompTk,
main.ReleventID,
main.EventType,
main.Notes
FROM wca.bbcc as main
inner join wca.scmst on main.secid = wca.scmst.secid
inner join wca.bbc on main.bbcid = wca.bbc.bbcid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.bbc.cntrycd='US'
AND main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)


-- # 4
SELECT
upper('BBEC') as Tablename,
main.Actflag,
main.Announcedate,
main.Acttime,
main.BbecID,
main.SecID,
main.BbeID,
main.OldExchgCD,
main.OldCurenCD,
main.Effectivedate,
main.NewExchgCD,
main.NewCurenCD,
main.OldBbgexhID,
main.NewBbgexhID,
main.OldBbgExhTk,
main.NewBbgExhTk,
main.ReleventID,
main.EventType,
main.Notes
FROM wca.bbec as main
inner join wca.scmst on main.secid = wca.scmst.secid
inner join wca.bbe on main.bbeid = wca.bbe.bbeid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
substring(wca.bbe.exchgcd,1,2)='US'
AND main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)


-- # 5
select 
upper('ICC') as Tablename,
wca.icc.Actflag,
wca.icc.Announcedate,
wca.icc.Acttime,
wca.icc.SecID,
wca.icc.Effectivedate,
wca.icc.OldIsin,
wca.icc.NewIsin,
wca.icc.OldUScode,
wca.icc.NewUScode
FROM wca.icc
inner join wca.scmst on wca.icc.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
WHERE
wca.exchg.cntrycd='US'
AND wca.icc.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.icc.OldIsin <> '' or wca.icc.NewIsin <> '' or wca.icc.OldUscode <> '' or wca.icc.NewUscode <> '');


-- # 6
select 
upper('SDCHG') as Tablename,
wca.sdchg.Actflag,
wca.sdchg.Announcedate,
wca.sdchg.Acttime,
wca.sdchg.SecID,
wca.sdchg.CntryCD,
wca.sdchg.Effectivedate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.NewCntryCD,
wca.sdchg.OldCurenCD,
wca.sdchg.NewCurenCD
FROM wca.sdchg
inner join wca.scmst on wca.sdchg.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
(wca.sdchg.cntrycd='US' or wca.sdchg.newcntrycd='US')
AND wca.sdchg.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.sdchg.OldSedol <> '' or wca.sdchg.NewSedol <> '' 
or wca.sdchg.NewCntryCD <> '' or wca.sdchg.NewRcntryCD <> '' 
or wca.sdchg.OldCurenCD <> '' or wca.sdchg.NewCurenCD <> '');


-- # 7
select 
upper('LCC') as Tablename,
wca.lcc.Actflag,
wca.lcc.Announcedate,
wca.lcc.Acttime,
wca.lcc.SecID,
wca.lcc.ExchgCD,
wca.lcc.EffectiveDate,
wca.lcc.OldLocalCode,
wca.lcc.NewLocalCode
from wca.lcc
inner join wca.scmst on wca.lcc.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.exchg.cntrycd='US'
AND wca.lcc.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.lcc.OldLocalCode <> '' or wca.lcc.NewLocalCode <> '');

