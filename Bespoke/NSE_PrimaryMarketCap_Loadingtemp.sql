select distinct
replace(prices.loadingtemp.localcode,'_','') as Symbol,
'' as Name,
prices.loadingtemp.SectyCD,
'' as SecID,
prices.loadingtemp.Isin,
prices.loadingtemp.uscode as UScode,
'' as GICS,
'' as SIC,
'' as IndusID,
'' as Primaryexchange,
'' as Sedol,
prices.loadingtemp.Exchange as ExchgCD,
'' as SecurityDesc,
'' as Lot,
'' as Lotsize,
'' as Listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.loadingtemp.Currency,
prices.loadingtemp.High,
prices.loadingtemp.Low,
prices.loadingtemp.opening as Open,
prices.loadingtemp.closing as Close,
prices.loadingtemp.MktCloseDate,
prices.loadingtemp.tradedvolume*1000 as Tradedvolume,
'' as Shares,
'' as USD_market_cap,
case when prices.loadingtemp.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.loadingtemp.comment like '%when issued%' then 'Y' else '' end as WIflag,
prices.loadingtemp.Comment
from prices.loadingtemp
left outer join prices.lasttrade on prices.loadingtemp.localcode=prices.lasttrade.localcode
        and prices.loadingtemp.exchange=prices.lasttrade.exchgcd
left outer join client.tholdsec on prices.loadingtemp.localcode=client.tholdsec.symbol
                      and prices.loadingtemp.mktclosedate=client.tholdsec.feeddate

where
(prices.loadingtemp.exchange='USAMEX'
  or prices.loadingtemp.exchange='USCHI'
  or prices.loadingtemp.exchange='USNASD'
  or prices.loadingtemp.exchange='USNYSE'
  or prices.loadingtemp.exchange='USPAC')
and prices.lasttrade.secid is null
and substring(prices.loadingtemp.localcode,5,1)='Z'
and length(prices.loadingtemp.localcode)=5;
