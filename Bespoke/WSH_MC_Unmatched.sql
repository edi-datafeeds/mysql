SELECT code, wca.scmst.isin, lasttrade.exchgcd, lasttrade.primaryexchgcd, lasttrade.pricedate, lasttrade.close  from client.pfisin 
left outer join prices.lasttrade on client.pfisin.code= prices.lasttrade.isin
left outer join wca.scmst on lasttrade.secid= wca.scmst.secid
where accid=367 and client.pfisin.actflag<>'D'
and code not in (SELECT code from client.pfisin where accid=996 and actflag<>'D')
and lasttrade.pricedate>'2018-03/01';