-- arc=y
-- arp=n:\bespoke\transmedia\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_MarketCap
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq=3), '%Y%m%d')
-- hpx=EDI_TransmissionMedia_MarketCap_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

select distinct
prices.lasttrade.Isin,
prices.lasttrade.uscode as Uscode,
prices.lasttrade.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
prices.lasttrade.PrimaryExchgCD,
wca.scmst.StructCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.Sedol,
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
where
(prices.lasttrade.exchgcd=prices.lasttrade.primaryexchgcd or prices.lasttrade.primaryexchgcd='')
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 7 day))
and wca.scmst.actflag<>'D'
and wca.scmst.sharesoutstanding<>''
and prices.lasttrade.close<>0;
