-- arc=y
-- arp=n:\bespoke\researchpool\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=ResearchPool_SRF_
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_SRF_ResearchPool_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1
select distinct 
wca.issur.IssuerName,
replace(prices.lasttrade.localcode,'_','') as Symbol,
wca.scmst.Isin,
wca.issur.CntryofIncorp,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates3.rate)*wca.scmst.sharesoutstanding),0)
     end as USD_Market_Cap,
prices.lasttrade.MIC,
wca.issur.IndusID,
case when wca.exchg.mic <> ''
     then wca.exchg.mic
     else prices.lasttrade.PrimaryExchgCD
     end as PrimaryExchange
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exchg on prices.lasttrade.primaryexchgcd = wca.exchg.exchgcd
left outer join exchgrates.liverates3 on prices.lasttrade.currency = exchgrates.liverates3.curr
                      and 'USD'=exchgrates.liverates3.base
where
prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and prices.lasttrade.exchgcd<>'USCOMP'
and prices.lasttrade.pricedate>=date_sub(now(), interval 1 month)
and prices.lasttrade.mktclosedate>=date_sub(now(), interval 7 day)
and prices.lasttrade.exchgcd <> ''
and wca.scmst.isin <> ''
and (prices.lasttrade.primaryexchgcd = prices.lasttrade.exchgcd or prices.lasttrade.primaryexchgcd = '' 
or (prices.lasttrade.primaryexchgcd='ESMSE' and prices.lasttrade.exchgcd='ESSIBE') 
or (prices.lasttrade.primaryexchgcd='ESBSE' and prices.lasttrade.exchgcd='ESSIBE') 
or (prices.lasttrade.primaryexchgcd='USOTC' and prices.lasttrade.exchgcd='USOTCB'))
and prices.lasttrade.secid is not null
and prices.lasttrade.secid <> 0
and prices.lasttrade.currency <> '';