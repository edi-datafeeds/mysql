-- arc=y
-- arp=n:\bespoke\dividendmax\sedolmap\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(44)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.csv
-- fpx=
-- fsx=_SedolMapping
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_SedolMapping_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1
select wca.scmst.SecID,
wca.scmst.Isin,
wca.sedol.Sedol
from wca.scmst
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid
INNER JOIN wca.sectygrp ON wca.scmst.sectycd = wca.sectygrp.sectycd AND 3>wca.sectygrp.secgrpid
where wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.sedol.actflag<>'D'
and wca.sedol.defunct<>'T'
order by wca.scmst.isin;