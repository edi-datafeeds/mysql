select distinct
replace(wca.scexh.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     else ''
     end as Lotsize,
wca.scexh.listdate,
'' as RegshoExempt,
'' as SubPenny,
'' as currency,
'' as high,
'' as low,
'' as open,
'' as close,
date_sub(date_format(now(),'%Y-%m-%d'), interval 0 day) as MktCloseDate,
'' as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
'' as usd_market_cap,
'' as WDflag,
'' as WIflag,
'' as factor,
'' as comment
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                      and wca.scexh.exchgcd=prices.lasttrade.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
                      and substring( wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
                      and 'D'<>wca.bbc.actflag
left outer join wca.bbe on  wca.scexh.secid = wca.bbe.secid 
                      and  wca.scexh.exchgcd = wca.bbe.exchgcd
                      and 'D'<>wca.bbe.actflag
where
prices.lasttrade.secid is null
and (wca.scexh.localcode<>'' or wca.scmst.uscode <>'')
and (wca.scexh.exchgcd='USAMEX'
  or wca.scexh.exchgcd='USCHI'
  or wca.scexh.exchgcd='USNASD'
  or wca.scexh.exchgcd='USNYSE'
  or wca.scexh.exchgcd='USPAC')
and (wca.scexh.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 90 day))
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and prices.lasttrade.sectycd <>'BND')
or (wca.scmst.sectycd = 'ETF')
or (wca.scmst.sectycd = 'SP'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null)
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
order by wca.scexh.localcode;
AIC    XNYS               US0413565021
AMUB   ARCX               US90274D3742
BCDA   XNGS               US09060U1016
BDCZ   ARCX               US90274D4161
BOXL   XNGS               US1031971096
BOXLW  XNGS               US1031971179
BSJN   ARCX               US18383M1835
CELGZ  XNGS               US1510201122
CLACU  XNGS               US14055M2052
CYHHZ  XNGS               US2036681169
EAB    XNYS               US29364D7619
EACQ   XNGS               US27616L1026
EACQW  XNGS               US27616L1109
ECAC   XNGS               KYG2920Y1017
ECACR  XNGS               KYG2920Y1199
ENJ    XNYS               US29364P5098
EWAS   BATS               N.A.        
EWCS   BATS               N.A.        
FXJP   ARCX               US73937B5223
GCVRZ  XNGS               US80105N1138
GPAC   XNGS               US37954X1054
HCAC   XNGS               US42588J1007
LBDC   ARCX               US90274D4245
MLPB   ARCX               US90274D3825
MRRL   ARCX               US90274D4328
NEE-P  XNYS               US65339F8611
NEWTZ  XNGS               US6525263025
SBBP   XNGS               IE00BYZ5XL97
UCIB   ARCX               US90274D3908
WMGIZ  XNGS               NL0011449376
WYIG   XNGS               US46590H1014
WYIGW  XNGS               US46590H1196
ZBK    XNYS               US9897018183
ZNWAA  XNGS               US9896961417
ZXYZ.A XNGS               US3579904158
