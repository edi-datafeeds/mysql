-- fex=.665
-- hpx=EDI_STATIC_665_
-- arc=y
-- arp=n:\bespoke\ionasset\
-- dfo=n
-- fty=y

-- # 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE
(substring(wca.scexh.exchgcd,1,2)='US'
or substring(wca.scexh.exchgcd,1,2)='CA'
or substring(wca.scexh.exchgcd,1,2)='JP'
or substring(wca.scexh.exchgcd,1,2)='DE')
and wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3

-- # 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.NoParValue,
wca.scmst.Voting,
wca.scmst.Holding,
wca.scmst.RegS144A,
wca.scmst.VotePerSec,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
WHERE
(substring(wca.scexh.exchgcd,1,2)='US'
or substring(wca.scexh.exchgcd,1,2)='CA'
or substring(wca.scexh.exchgcd,1,2)='JP'
or substring(wca.scexh.exchgcd,1,2)='DE')
and wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3

-- # 3
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.sedol.secid = wca.scexh.secid
WHERE
(substring(wca.scexh.exchgcd,1,2)='US'
or substring(wca.scexh.exchgcd,1,2)='CA'
or substring(wca.scexh.exchgcd,1,2)='JP'
or substring(wca.scexh.exchgcd,1,2)='DE')
and wca.sedol.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3

-- # 4
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE
(substring(wca.scexh.exchgcd,1,2)='US'
or substring(wca.scexh.exchgcd,1,2)='CA'
or substring(wca.scexh.exchgcd,1,2)='JP'
or substring(wca.scexh.exchgcd,1,2)='DE')
and wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3

-- # 5
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
WHERE
wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.exchg.exchgcd='CATSE'
or wca.exchg.exchgcd='CACVE'
or wca.exchg.exchgcd='CABND'
or wca.exchg.exchgcd='CACNQ'
or wca.exchg.exchgcd='DEFSX'
or wca.exchg.exchgcd='DEBSE'
or wca.exchg.exchgcd='DEDSE'
or wca.exchg.exchgcd='DEHSE'
or wca.exchg.exchgcd='DEMSE'
or wca.exchg.exchgcd='DESSE'
or wca.exchg.exchgcd='DEHNSE'
or wca.exchg.exchgcd='DESCH'
or wca.exchg.exchgcd='DEBND'
or wca.exchg.exchgcd='JPTSE'
or wca.exchg.exchgcd='JPBND'
or wca.exchg.exchgcd='JPNSE'
or wca.exchg.exchgcd='USAMEX'
or wca.exchg.exchgcd='USNASD'
or wca.exchg.exchgcd='USOTC'
or wca.exchg.exchgcd='USNYSE'
or wca.exchg.exchgcd='USPAC'
or wca.exchg.exchgcd='USPORT'
or wca.exchg.exchgcd='USBND'
or wca.exchg.exchgcd='USTRCE'
or wca.exchg.exchgcd='USBATS'
or wca.exchg.exchgcd='USFNBB');