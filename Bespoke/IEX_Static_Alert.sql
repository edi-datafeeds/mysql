-- arc=y
-- arp=n:\bespoke\iex\
-- fsx=_Static_Alert
-- hpx=EDI_WCA_Static_Change_
-- hdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fty=n
-- dfn=n

-- # 1
select 
'StaticCD' as f1,
'EventID' as f2,
'Actflag' as f3,
'ScexhID' as f3,
'Changed' as f3,
'Created' as f3,
'Localcode' as f3,
'IssuerName' as f3,
'SectyCD' as f2,
'SecID' as f2,
'Isin' as f3,
'UScode' as f3,
'SIC' as f3,
'GICS' as f3,
'EDIIndusID' as f3,
'PrimaryExchgCD' as f3,
'bbgcompid' as f3,
'bbgcomptk' as f3,
'bbgexhid' as f3,
'bbgexhtk' as f3,
'ExchgCD' as f3,	
'SecurityDesc' as f3,
'Lotsize' as f3,	
'Listdate' as f3,	
'RegshoExempt' as f3,	
'SubPenny' as f3,	
'Micseg' as f3,		
'MktSegment' as f3,	
'RelatedEventCD' as f14,
'EffectiveDate' as f14,
'OldStatic' as f34,
'NewStatic' as f36,
'WDflag' as f34,
'WIflag' as f36;

-- # 2
select
upper('ISIN') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldIsin as OldStatic,
vtab.NewIsin as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_icc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldIsin <> vtab.NewIsin)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 3
select
upper('USCODE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldUScode as OldStatic,
vtab.NewUScode as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_icc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldUScode <> vtab.NewUScode)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 4
select
upper('LOCALCODE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldLocalcode as OldStatic,
vtab.NewLocalcode as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_lcc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldLocalcode <> vtab.NewLocalcode)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 6
select
upper('SECNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.DateofChange,
vtab.SecOldName as OldStatic,
vtab.SecNewName as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.DateofChange>=date_sub(now(), interval 1 day) and vtab.DateofChange<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.DateofChange<date_sub(now(), interval 1 day)))
and (vtab.SecOldName <> vtab.SecNewName)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 7
select
upper('NLIST') as EventCD,
wca.scexh.ScexhID as EventID,
case when wca.scexh.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
'' as RelatedEventCD,
wca.scexh.listdate as EffectiveDate,
'' as OldStatic,
'' as NewStatic,
'' as WDflag,
'' as WIflag
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
wca.scexh.listdate>=date_sub(now(), interval 1 day) and wca.scexh.listdate<date_add(now(), interval 7 day)
and wca.exchg.cntrycd='US'
and wca.scexh.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 8
select
upper('ISSNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.NameChangeDate as EffectiveDate,
vtab.IssOldName as OldStatic,
vtab.IssNewName as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ischg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scmst.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scmst.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.NameChangeDate>=date_sub(now(), interval 1 day) and vtab.NameChangeDate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.NameChangeDate<date_sub(now(), interval 1 day)))
and (vtab.IssOldName <> vtab.IssNewName)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 9
select
upper('INCORP') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.InChgDate as EffectiveDate,
vtab.OldCntryCD as OldStatic,
vtab.NewCntryCD as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_inchg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scmst.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scmst.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.InChgDate>=date_sub(now(), interval 1 day) and vtab.InChgDate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.InChgDate<date_sub(now(), interval 1 day)))
and (vtab.OldCntryCD <> vtab.NewCntryCD)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 10
select
upper('LISTSTATUS') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
'' as OldStatic,
vtab.LstatStatus as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_lstat as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and vtab.LstatStatus <> ''
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 11
select
upper('PRCHG') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldExchgCD as OldStatic,
vtab.NewExchgCD as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_prchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldExchgCD <> vtab.NewExchgCD)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 12
select
upper('IPO') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
'' as RelatedEventCD,
vtab.FirstTradingDate as FirstTradingDate,
'' as OldStatic,
'' as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ipo as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
vtab.ipostatus<>'HISTORY'
and ((vtab.FirstTradingDate>=date_sub(now(), interval 1 day) and vtab.FirstTradingDate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.FirstTradingDate<date_sub(now(), interval 1 day)))
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 13
select
upper('LOTCHANGE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldLot as OldStatic,
vtab.NewLot as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ltchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on vtab.secid = wca.bbc.secid 
            and substring(wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
            and 'D'<>wca.bbc.actflag
left outer join wca.bbe on vtab.secid = wca.bbe.secid
            and wca.scexh.exchgcd = wca.bbe.exchgcd
            and 'D'<>wca.bbe.actflag
WHERE
((vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 7 day))
and wca.exchg.cntrycd='US'
or
(vtab.acttime>date_sub(now(), interval 1 day) and vtab.effectivedate<date_sub(now(), interval 1 day)))
and (vtab.OldLot <> vtab.NewLot)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND')
or (wca.scmst.uscode='46429B523'
or wca.scmst.uscode='46429B515'
or wca.scmst.uscode='74347B706'
or wca.scmst.uscode='46429B481'
or wca.scmst.uscode='46429B473'
or wca.scmst.uscode='46429B465'
or wca.scmst.uscode='46429B416'
or wca.scmst.uscode='74348A541'
or wca.scmst.uscode='46434V878'
or wca.scmst.uscode='46429B598'
or wca.scmst.uscode='74348A566'
or wca.scmst.uscode='46431W507'
or wca.scmst.uscode='74348A533'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';
