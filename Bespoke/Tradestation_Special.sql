-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=_Special
-- headerprefix=EDI_WCA_Special_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
select
'StaticCD' as f1,
'EventID' as f2,
'Actflag' as f3,
'ScexhID' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'SecurityDesc' as f3,
'PrimaryExchgCD' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'Exdate' as f15,
'CurenCD' as f23,
'GrossDividend' as f24,
'NetDividend' as f25;

-- # 2
select
upper('SPECIALDIV') as EventCD,
vtab.EventID,
case when vtab.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.SecurityDesc,
stab.PrimaryExchgCD,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Exdate,
wca.divpy.CurenCD,
wca.divpy.GrossDividend,
wca.divpy.NetDividend
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.v10s_divpy as divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
WHERE
((wca.exdt.ExDate>=date_sub(now(), interval 1 day)
and wca.exdt.ExDate<now())
or
(pexdt.ExDate>=date_sub(now(), interval 1 day)
and pexdt.ExDate<now()))
and (wca.scexh.exchgcd = 'USNYSE'
  or wca.scexh.exchgcd = 'USNASD'
  or wca.scexh.exchgcd = 'USOTC')
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.divpy.curencd <> ''
and (wca.divpy.grossdividend <> '' or wca.divpy.netdividend <> '')
and (vtab.divperiodcd ='SPL'
or vtab.divperiodcd ='NL'
or vtab.divperiodcd ='CG'
or vtab.divperiodcd ='CGL'
or vtab.divperiodcd ='CGS'
or vtab.divperiodcd ='INS'
or vtab.divperiodcd ='MEM'
or vtab.divperiodcd ='SUP'
or vtab.divperiodcd ='TEI'
or vtab.divperiodcd ='ARR');
