-- arp=n:\Bespoke\GlobalTrading\
-- fsx=_US_Primary
-- fty=y
-- dsp=select char(44)
-- fex=.csv
-- hdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')

-- # 1
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
replace(wca.issur.issuername,',','') as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
prices.lasttrade.ExchgCD,
replace(wca.scmst.SecurityDesc,',','') as SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as usd_market_cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
where
prices.lasttrade.sectycd <>'MF'
and substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 3 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USCOMP'
union
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
replace(wca.issur.issuername,',','') as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
prices.lasttrade.ExchgCD,
replace(wca.scmst.SecurityDesc,',','') as SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as usd_market_cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
where
prices.lasttrade.exchgcd like 'USOTC%'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
union
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
replace(wca.issur.issuername,',','') as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
case when prices.lasttrade.ExchgCD='USCOMP' or prices.lasttrade.ExchgCD='USOTCB' then 'USBATS' else prices.lasttrade.ExchgCD end as ExchgCD,
replace(wca.scmst.SecurityDesc,',','') as SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as usd_market_cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and (wca.ipo.ipoid is null or (wca.ipo.ipostatus<>'PENDING' and wca.ipo.ipostatus<>'WITHDRAWN'))
and wca.scexh.exchgcd='USBATS'
and (prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB')
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>'2006/12/31')
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF')
or ((prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB') and prices.lasttrade.secid in (select secid from wca.scexh where exchgcd = 'USBATS' 
                                          and wca.scexh.actflag <> 'D' and wca.scexh.liststatus <> 'D' and wca.scexh.liststatus <> 'S')))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 3 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
union
select distinct
replace(prices.loadingtemp.localcode,'_','') as Symbol,
'' as Name,
'' as SectyCD,
'' as SecID,
prices.loadingtemp.Isin,
prices.loadingtemp.uscode as UScode,
'' as Primaryexchange,
prices.loadingtemp.Exchange as ExchgCD,
'' as SecurityDesc,
prices.loadingtemp.Currency,
prices.loadingtemp.High,
prices.loadingtemp.Low,
prices.loadingtemp.opening as Open,
prices.loadingtemp.closing as Close,
prices.loadingtemp.MktCloseDate,
prices.loadingtemp.tradedvolume*1000 as Tradedvolume,
'' as Shares,
'' as USD_market_cap
from prices.loadingtemp
left outer join prices.lasttrade on prices.loadingtemp.localcode=prices.lasttrade.localcode
        and prices.loadingtemp.exchange=prices.lasttrade.exchgcd
left outer join client.tholdsec on prices.loadingtemp.localcode=client.tholdsec.symbol
                      and prices.loadingtemp.mktclosedate=client.tholdsec.feeddate

where
(prices.loadingtemp.exchange='USAMEX'
  or prices.loadingtemp.exchange='USCHI'
  or prices.loadingtemp.exchange='USNASD'
  or prices.loadingtemp.exchange='USNYSE'
  or prices.loadingtemp.exchange='USPAC')
and prices.lasttrade.secid is null
and substring(prices.loadingtemp.localcode,5,1)='Z'
and length(prices.loadingtemp.localcode)=5
union
select distinct
replace(wca.scexh.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.scmst.PrimaryExchgcd,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
'' as currency,
'' as high,
'' as low,
'' as open,
'' as close,
date_sub(date_format(now(),'%Y-%m-%d'), interval 0 day) as MktCloseDate,
'' as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
'' as usd_market_cap
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                      and wca.scexh.exchgcd=prices.lasttrade.exchgcd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid
where
prices.lasttrade.secid is null
and (wca.ipo.ipoid is null or (wca.ipo.ipostatus<>'PENDING' and wca.ipo.ipostatus<>'WITHDRAWN'))
and (wca.ipo.ipoid is null or wca.scmst.uscode<>'')
and (wca.scexh.localcode<>'' or wca.scmst.uscode <>'')
and (wca.scexh.exchgcd='USAMEX'
  or wca.scexh.exchgcd='USCHI'
  or wca.scexh.exchgcd='USNASD'
  or wca.scexh.exchgcd='USNYSE'
  or wca.scexh.exchgcd='USPAC')
and wca.scexh.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 120 day))
and wca.scmst.sectycd <>'BND'
and prices.lasttrade.sectycd <>'MF'
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null)
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
