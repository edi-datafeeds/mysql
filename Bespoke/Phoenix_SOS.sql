-- arc=y
-- arp=n:\bespoke\phoenix\
-- fex=.txt
-- fpx=PhoenixFund_SOS_
-- fty=y
-- hpx=PhoenixFundAdministration_Shares_Outstanding_

-- # 1

select DISTINCT
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
client.pfisin.code as Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate
FROM client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
LEFT OUTER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
client.pfisin.accid=265
and client.pfisin.actflag<>'D'
AND (wca.scmst.actflag<>'D'
or wca.scmst.secid is null)