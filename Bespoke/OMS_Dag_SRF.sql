-- arc=y
-- arp=n:\bespoke\oms_dag\ixcon\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=OMS_DAG_Ixcon_SRF_
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=OMS_DAG_Ixcon_SRF_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select wca.issur.IssID,
wca.scmst.SecID,
wca.issur.Issuername,
wca.scmst.Isin,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scexh.ExchgCD,
wca.scexh.localcode,
ixcon.ixcon.IXnamID,
ixcon.ixcon.EffectiveDate,
ixcon.ixcon.OnIndex
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join ixcon.ixcon on wca.scmst.secid = ixcon.ixcon.secid
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
where (wca.scexh.exchgcd = wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd='')
and (ixcon.ixcon.ixnamid=1
or ixcon.ixcon.ixnamid=6 
or ixcon.ixcon.ixnamid=11 
or ixcon.ixcon.ixnamid=14 
or ixcon.ixcon.ixnamid=15 
or ixcon.ixcon.ixnamid=16 
or ixcon.ixcon.ixnamid=42 
or ixcon.ixcon.ixnamid=458 
or ixcon.ixcon.ixnamid=459)
and ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()))