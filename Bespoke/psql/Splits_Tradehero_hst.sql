-- filenameprefix=
-- filenamedate=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=
-- headerdate=yyyymmdd
-- datadateformat=yyyymmdd
-- datatimeformat=
-- forcetime=n
-- footertext=
-- fieldseparator=
-- archive=n
-- archivepath=n:\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
select
'Symbol' as f1,
'Date' as f2,
'Ratio' as f3,
'SecID' as f4,
'Isin' as f5,
'ExchgCD' as f6


-- # 2
select
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then localcode
     else wca.lcc.oldlocalcode
     end as Symbol,
case when wca.exdt.exdate is not null then wca.exdt.exdate
     else pexdt.exdate
     end as Date,
case when wca.consd.newratio is null or wca.consd.newratio = ''
     then ''
     else concat(replace(wca.consd.newratio,'.0000000',''),'-',replace(wca.consd.oldratio,'.0000000',''))
     end as Ratio,
wca.scmst.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then isin
     else wca.icc.oldisin
     end as Isin,
wca.scexh.ExchgCD
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'consd' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'consd' = pexdt.eventtype
left outer join wca.lcc on wca.rd.rdid = wca.lcc.releventid and 'consd' = wca.lcc.eventtype
       and wca.scexh.exchgcd = wca.lcc.exchgcd
       and wca.lcc.actflag<>'d'
left outer join wca.icc on wca.rd.rdid = wca.icc.releventid and 'sd' = wca.icc.eventtype
       and wca.icc.actflag<>'d'
where
wca.scexh.exchgcd=@fexchgcd
and (@ffdate is null or wca.exdt.exdate>@ffdate or pexdt.exdate>@ffdate)
and newratio<>oldratio
and wca.consd.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';

-- # 3
select
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then localcode
     else wca.lcc.oldlocalcode
     end as Symbol,
case when wca.exdt.exdate is not null then wca.exdt.exdate
     else pexdt.exdate
     end as Date,
case when wca.sd.newratio is null or wca.sd.newratio = ''
     then ''
     else concat(replace(wca.sd.newratio,'.0000000',''),'-',replace(wca.sd.oldratio,'.0000000',''))
     end as Ratio,
wca.scmst.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then isin
     else wca.icc.oldisin
     end as Isin,
wca.scexh.ExchgCD
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'sd' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'sd' = pexdt.eventtype
left outer join wca.lcc on wca.rd.rdid = wca.lcc.releventid and 'sd' = wca.lcc.eventtype
       and wca.scexh.exchgcd = wca.lcc.exchgcd
       and wca.lcc.actflag<>'d'
left outer join wca.icc on wca.rd.rdid = wca.icc.releventid and 'sd' = wca.icc.eventtype
       and wca.icc.actflag<>'d'
where
wca.scexh.exchgcd=@fexchgcd
and (@ffdate is null or wca.exdt.exdate>@ffdate or pexdt.exdate>@ffdate)
and newratio<>oldratio
and wca.sd.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';

-- # 4
select
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then localcode
     else wca.lcc.oldlocalcode
     end as Symbol,
wca.caprd.effectivedate as Date,
case when wca.caprd.newratio is null or wca.caprd.newratio = ''
     then ''
     else concat(replace(wca.caprd.newratio,'.0000000',''),'-',replace(wca.caprd.oldratio,'.0000000',''))
     end as Ratio,
wca.scmst.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then isin
     else wca.icc.oldisin
     end as Isin,
wca.scexh.ExchgCD
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.lcc on wca.caprd.caprdid = wca.lcc.releventid and 'caprd' = wca.lcc.eventtype
       and wca.scexh.exchgcd = wca.lcc.exchgcd
       and wca.lcc.actflag<>'d'
left outer join wca.icc on wca.caprd.caprdid = wca.icc.releventid and 'sd' = wca.icc.eventtype
       and wca.icc.actflag<>'d'
where
wca.scexh.exchgcd=@fexchgcd
and (@ffdate is null or wca.caprd.effectivedate>@ffdate)
and newratio<>oldratio
and wca.caprd.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';

-- # 5
select
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then localcode
     else wca.lcc.oldlocalcode
     end as Symbol,
case when wca.exdt.exdate is not null then wca.exdt.exdate
     else pexdt.exdate
     end as Date,
case when wca.bon.rationew is null or wca.bon.rationew = ''
     then ''
     else concat(replace(wca.bon.rationew,'.0000000',''),'-',replace(wca.bon.ratioold,'.0000000',''))
     end as Ratio,
wca.scmst.SecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then isin
     else wca.icc.oldisin
     end as Isin,
wca.scexh.ExchgCD
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'bon' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'bon' = pexdt.eventtype
left outer join wca.lcc on wca.rd.rdid = wca.lcc.releventid and 'bon' = wca.lcc.eventtype
       and wca.scexh.exchgcd = wca.lcc.exchgcd
       and wca.lcc.actflag<>'d'
left outer join wca.icc on wca.rd.rdid = wca.icc.releventid and 'bon' = wca.icc.eventtype
       and wca.icc.actflag<>'d'
where
wca.scexh.exchgcd=@fexchgcd
and (@ffdate is null or wca.exdt.exdate>@ffdate or pexdt.exdate>@ffdate)
and rationew<>ratioold
and wca.bon.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';
