-- filenameprefix=
-- filenamedate=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=
-- headerdate=yyyymmdd
-- datadateformat=yyyymmdd
-- datatimeformat=
-- forcetime=n
-- footertext=
-- fieldseparator=
-- archive=n
-- archivepath=n:\
-- fieldheaders=y
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=y

-- # 1
select distinct
wca.scexh.localcode as Symbol,
wca.issur.issuername as Name,
wca.indus.indusname as Sector,
'' as Industry,
'' as PE,
'' as EPS,
'' as DivYield,
wca.scmst.sharesoutstanding as Shares,
'' as DPS,
'' as PEG,
'' as PtS,
'' as PtB,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scexh.ExchgCD,
bbgcompid,
bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
wca.scmst.SecurityDesc,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.close,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
prices.lasttrade.MktCloseDate,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
case when prices.lasttrade.MktCloseDate is not null then finhols.pubhol_open_hrs.Openhrs else '' end as Openhrs,
case when prices.lasttrade.MktCloseDate is not null then finhols.pubhol_open_hrs.Closehrs else '' end as Closehrs,
prices.lasttrade.open
from wca.scexh
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.indus on wca.issur.indusid = wca.indus.indusid
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join prices.lasttrade on wca.scexh.exchgcd=prices.lasttrade.exchgcd
                      and wca.scexh.secid=prices.lasttrade.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join finhols.pubhol_exch_map on wca.exchg.exchgcd=finhols.pubhol_exch_map.exchgcd
left outer join finhols.pubhol_open_hrs on finhols.pubhol_exch_map.fin_Code=finhols.pubhol_open_hrs.Fin_Code
where
wca.scexh.exchgcd=@fexchgcd
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';
