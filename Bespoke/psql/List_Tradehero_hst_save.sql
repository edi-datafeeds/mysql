-- filenameprefix=
-- filenamedate=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=
-- headerdate=yyyymmdd
-- datadateformat=yyyymmdd
-- datatimeformat=
-- forcetime=n
-- footertext=
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=y
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=y

-- # 1
select
wca.scexh.localcode as Symbol,
wca.issur.issuername as Name,
wca.indus.indusname as Sector,
'' as Industry,
'' as PE,
'' as EPS,
'' as DivYield,
wca.scmst.sharesoutstanding as Shares,
'' as DPS,
'' as PEG,
'' as PtS,
'' as PtB,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scexh.ExchgCD,
bbgcompid,
bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
wca.scmst.SecurityDesc
from wca.scexh
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.indus on wca.issur.indusid = wca.indus.indusid
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
where
wca.scexh.exchgcd=@fexchgcd
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.scexh.liststatus<>'D'
and wca.scexh.actflag<>'D';