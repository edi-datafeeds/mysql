-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.680
-- filenamesuffix=
-- headerprefix=EDI_WCA_680_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\no_cull_feeds\680\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Actflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

-- # 1
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AssimilationDate' as Date1Type,
vtab.AssimilationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_assm as vtab
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd = wca.scexh.exchgcd
inner join wca.v20c_680_scmst as stab on wca.scexh.secid = stab.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.assimilationdate>=@fcfdate and vtab.assimilationdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';

-- # 2
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldIsin' as Field2Name,
vtab.OldIsin as Field2,
'NewIsin' as Field3Name,
vtab.NewIsin as Field3,
'OldUscode' as Field4Name,
vtab.OldUscode as Field4,
'NewUscode' as Field5Name,
vtab.NewUscode as Field5,	
'RelEventID' as Field6Name,
vtab.RelEventID as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_icc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D'))
and (vtab.OldIsin <> '' or vtab.NewIsin <> '' or vtab.OldUscode <> '' or vtab.NewUscode <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';

-- # 3
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (mpay.acttime > vtab.acttime) 
      and (mpay.acttime > rd.acttime) 
      and (mpay.acttime > exdt.acttime) 
      and (mpay.acttime > pexdt.acttime) 
     then mpay.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > exdt.acttime) 
      and (rd.acttime > pexdt.acttime) 
     then rd.acttime 
     when (exdt.acttime > vtab.acttime) 
      and (exdt.acttime > pexdt.acttime) 
     then exdt.acttime 
     when (pexdt.acttime > vtab.acttime) 
     then pexdt.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'AppointedDate' as Date4Type,
vtab.AppointedDate as Date4,
'EffectiveDate' as Date5Type,
vtab.EffectiveDate as Date5,
'WithdrawalFromdate' as Date6Type,
wca.mpay.WithdrawalFromdate as Date6,
'WithdrawalTodate' as Date7Type,
wca.mpay.WithdrawalTodate as Date7,
'OptElectionDate' as Date8Type,
mpay.OptElectionDate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MrgrStatus' as Field1Name,
vtab.MrgrStatus as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_mrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (mpay.acttime>=@ffdate and (@ftdate is null or mpay.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';

-- # 4
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime) 
      and (rd.acttime > exdt.acttime) 
      and (rd.acttime > pexdt.acttime) 
     then rd.acttime 
     when (exdt.acttime > vtab.acttime) 
      and (exdt.acttime > pexdt.acttime) 
     then exdt.acttime 
     when (pexdt.acttime > vtab.acttime) 
     then pexdt.acttime 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_scswp as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';

-- # 5
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.secid as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Relevent' as Field1Name,
vtab.Eventtype as Field1,
'ResSectyCD' as Field2Name,
vtab.SectyCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
FROM wca.v10s_secrc as vtab
INNER JOIN wca.v20c_680_scmst as stab ON stab.SecID = vtab.SecID
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer JOIN wca.scmst as resscmst ON vtab.ressecID = resscmst.SecID
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';