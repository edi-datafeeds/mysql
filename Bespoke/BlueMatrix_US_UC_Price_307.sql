-- arc=y
-- arp=n:\bespoke\bluematrix\
-- fsx=_US_UC_Prices
-- hpx=EDI_BlueMatrix_US_UC_Prices_
-- hdt=SELECT DATE_FORMAT((select max(mktclosedate) from prices.lasttrade where substring(exchgcd,1,2)='US'), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(mktclosedate) from prices.lasttrade where substring(exchgcd,1,2)='US'), '%Y%m%d')
-- fty=n

-- # 1
select distinct
client.pfcomptk.code,
prices.lasttrade.*,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor
from client.pfcomptk
left outer join wca.bbc on code = BbgComptk
left outer join wca.scexh on wca.bbc.cntrycd=substring(wca.scexh.exchgcd,1,2)
		             and wca.bbc.secid=wca.scexh.secid
                             and 'D'<>wca.scexh.liststatus
                             and 'D'<>wca.scexh.actflag
left outer join prices.lasttrade on wca.bbc.secid = prices.lasttrade.secid
                      and ((prices.lasttrade.exchgcd=wca.scexh.exchgcd) or (wca.scexh.exchgcd='USOTC' and substring(prices.lasttrade.exchgcd,1,5)=wca.scexh.exchgcd))
                      and prices.lasttrade.pricedate>date_sub(now(), interval 30 day)
left outer join prices.ajfactor on prices.lasttrade.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = prices.lasttrade.mic
                      and prices.ajfactor.exdate = date_format(date_sub(now(), interval 1 day),'%Y-%m-%d')
                      and 1=prices.ajfactor.option
Where
client.pfcomptk.accid=307
and substring(wca.scexh.exchgcd,1,2)='US'
and client.pfcomptk.actflag<>'D'
and (client.pfcomptk.secid=0
or (prices.lasttrade.secid is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency desc, pricedate desc limit 1))
and (prices.lasttrade.secid is null or prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency desc, pricedate desc limit 1)))
order by isin, pricedate desc;
