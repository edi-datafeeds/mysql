-- arc=y
-- arp=n:\bespoke\deloitte\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_deloitte_srf
-- fty=y
-- fex=.txt
-- dfn=L
-- hdt=
-- hpx=
-- hsx=_deloitte_srf
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')

-- # 1
select distinct
wca.scexh.ScexhID,
wca.scexh.ActFlag,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca2.sedolbbg.curencd as TradingCurrency,
wca2.sedolbbg.Sedol,
wca2.sedolbbg.RcntryCD,
wca2.sedolbbg.bbgcompid as BbgCompositeGlobalID,
wca2.sedolbbg.bbgcomptk as BbgCompositeTicker,
wca2.sedolbbg.bbgexhid as BbgGlobalID,
wca2.sedolbbg.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.scexh.ExchgCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.MktCloseDate,
prices.lasttrade.PriceDate,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
wca.issur.GICS,
wca.issur.LEI,
wca.scmst.SharesOutstanding
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbg on wca.scexh.secid = wca2.sedolbbg.secid 
                                and substring(wca.scexh.exchgcd,1,2) = wca2.sedolbbg.exchgcd
left outer join prices.lasttrade on wca.scexh.exchgcd=prices.lasttrade.exchgcd and wca.scexh.secid=prices.lasttrade.secid
                                   and wca2.sedolbbg.curencd=prices.lasttrade.currency
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                              and (wca2.sedolbbg.curencd=prices.lasttrade.currency or wca2.sedolbbg.curencd is null or wca2.sedolbbg.curencd='')
WHERE
wca.scmst.statusflag<>'I'
and wca.scexh.liststatus<>'D'
and wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and (comment is null or comment not like 'terms%');