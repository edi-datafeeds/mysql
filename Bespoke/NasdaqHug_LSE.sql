-- arc=y
-- arp=n:\temp\
-- dfn=l
-- hpx=EDI_NasdaqHug_SRF_
-- fex=.txt
-- fty=n

-- # 1
select distinct
wca.scmst.issid,
wca.issur.issuername,
wca.issur.lei,
wca.scmst.securitydesc,
wca.scmst.isin,
wca.sedol.sedol,
case when wca.scmst.sectyCD = 'EQS' then '1 Equity'
     when wca.scmst.sectyCD = 'PRF' then '2 Preference'
     when wca.scmst.sectyCD = 'DR'  then '3 Depositary Receipt'
     when wca.scmst.sectyCD = 'BND' then '4 Debt'
else '5 Other'
end as SecurityType,
wca.scexh.localcode,
substring(wca.scexh.exchgcd,1,2) as ExchgCntryCD,
wca.issur.CntryofIncorp
from wca.scmst
left outer join wca.scexh on scmst.secid = wca.scexh.secid
left outer join wca.sedol on scmst.secid = wca.sedol.secid 
                     and substring(wca.scexh.exchgcd,1,2)=wca.sedol.cntrycd
                     and 'D'<>wca.sedol.actflag
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
where 
wca.scexh.exchgcd = @exchgcd
and wca.scmst.sectycd<>'CW'
and wca.scexh.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and wca.scmst.actflag <> 'D'
and (wca.scmst.statusflag <> 'I' or wca.scmst.statusflag is null)
and wca.scmst.actflag <> 'D'
order by wca.scmst.issid, securitytype, wca.scexh.Localcode desc;

