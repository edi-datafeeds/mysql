-- arc=n
-- arp=n:\temp\
-- ddt=
-- dfn=n
-- dfo=n
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=select ''
-- dzc=y
-- fdt=select ''
-- fex=.txt
-- fpx=secid
-- fsx=
-- fty=y
-- hdt=select ''
-- hpx=
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=select ''

-- # 1

select secid from ixcon.ixcon 
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
where
ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ixcon.ixcon.ixnamid = 18
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()));