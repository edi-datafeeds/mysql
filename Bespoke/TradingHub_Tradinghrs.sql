-- arc=y
-- arp=n:\bespoke\tradinghub\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_ExchangeClosingHours
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_ExchangeClosingHours_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct 
finhols.pubhol_country.country_desc as CountryName,
finhols.pubhol_institution.description as InstitutionName,
finhols.pubhol_institution.MIC,
finhols.pubhol_exch_map.ExchgCD,
finhols.pubhol_holidays.FIN_CODE,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk')
      when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk')
      when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
      when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')
      else upper('exc')
      end as InstTypeCD,
finhols.pubhol_institution.Gmt_Offset,
case when finhols.pubhol_open_hrs.daysofweek is null or upper(finhols.pubhol_open_hrs.daysofweek='NULL') then '' else finhols.pubhol_open_hrs.daysofweek end as OpeningDays,
case when finhols.pubhol_open_hrs.openhrs is null or upper(finhols.pubhol_open_hrs.openhrs='NULL') then '' else finhols.pubhol_open_hrs.openhrs end as OpenHrs,
case when finhols.pubhol_open_hrs.closehrs is null or upper(finhols.pubhol_open_hrs.closehrs='NULL') then '' else finhols.pubhol_open_hrs.closehrs end as CloseHrs
from finhols.pubhol_country 
inner join finhols.pubhol_institution on finhols.pubhol_country.country = finhols.pubhol_institution.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
inner join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
inner join finhols.pubhol_exch_map on finhols.pubhol_holidays.fin_code = finhols.pubhol_exch_map.fin_code 
where
finhols.pubhol_holidays.actflag<>'D'
and finhols.pubhol_exch_map.ExchgCD is not null
and finhols.pubhol_exch_map.ExchgCD <> ''
and (finhols.pubhol_open_hrs.openhrs<>'' or finhols.pubhol_open_hrs.closehrs<>'')
order by finhols.pubhol_country.country_desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc
