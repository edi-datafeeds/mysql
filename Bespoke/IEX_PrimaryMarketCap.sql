-- arc=y
-- arp=n:\bespoke\iex\
-- fsx=_US_Primary
-- hpx=EDI_US_Primary_
-- hdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fty=y

-- # 1
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
case when prices.lasttrade.ExchgCD='USCOMP' or prices.lasttrade.ExchgCD='USOTCB' then 'USBATS' else prices.lasttrade.ExchgCD end as ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when prices.lasttrade.mic='XOTC' or prices.lasttrade.mic='OTCB'
     then '100'
     else '100'
     end as Lotsize,
wca.scexh.listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*wca.scmst.sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
case when prices.lasttrade.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.lasttrade.comment like '%when issued%' then 'Y' else '' end as WIflag,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor,
prices.lasttrade.comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join client.tholdsec on prices.lasttrade.localcode=client.tholdsec.symbol
                      and prices.lasttrade.mktclosedate=client.tholdsec.feeddate
left outer join wca.bbc on prices.lasttrade.secid = wca.bbc.secid 
                      and substring(prices.lasttrade.exchgcd,1,2) = wca.bbc.cntrycd 
                      and prices.lasttrade.currency = wca.bbc.curencd
                      and 'D'<>wca.bbc.actflag
left outer join wca.bbe on prices.lasttrade.secid = wca.bbe.secid 
                      and prices.lasttrade.exchgcd = wca.bbe.exchgcd
                      and prices.lasttrade.currency=bbe.curencd
                      and 'D'<>wca.bbe.actflag
left outer join prices.ajfactor on prices.lasttrade.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = prices.lasttrade.mic
                      and prices.ajfactor.exdate = date_format(now(),'%Y-%m-%d')
                      and 1=prices.ajfactor.option
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.currency='USD'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1)
and (prices.ajfactor.seq is null or prices.ajfactor.seq =
(select seq from prices.ajfactor as ajlatest
where prices.lasttrade.secid = ajlatest.secid 
  and prices.lasttrade.mic = ajlatest.market
  and date_format(now(),'%Y-%m-%d') = ajlatest.exdate
  and 1=ajlatest.option
order by seq desc limit 1))
and wca.scexh.scexhid =
(select scexhid from wca.scexh as top1
where wca.scexh.secid = top1.secid 
and wca.scexh.localcode = top1.localcode
and (top1.exchgcd='USNASD' or top1.exchgcd='USNYSE' or top1.exchgcd='USPAC' or top1.exchgcd='USAMEX')
and top1.liststatus<>'D' and top1.actflag<>'D'
order by ifnull(top1.mktsgid,0) limit 1)
and prices.lasttrade.exchgcd=wca.scexh.exchgcd 
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>'2016/07/31')
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND')
or (wca.scexh.localcode = 'ZBK')
or (wca.scexh.localcode = 'NEWTZ'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USOTC'
and prices.lasttrade.exchgcd<>'USCOMP'
union
select distinct
replace(prices.lasttrade.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
case when prices.lasttrade.ExchgCD='USCOMP' or prices.lasttrade.ExchgCD='USOTCB' then 'USBATS' else prices.lasttrade.ExchgCD end as ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when prices.lasttrade.mic='XOTC' or prices.lasttrade.mic='OTCB'
     then '100'
     else '100'
     end as Lotsize,
wca.scexh.listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.lasttrade.currency,
prices.lasttrade.high,
prices.lasttrade.low,
prices.lasttrade.open,
prices.lasttrade.close,
prices.lasttrade.MktCloseDate,
prices.lasttrade.tradedvolume*1000 as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
case when prices.lasttrade.currency='USD'
     then format(round(prices.lasttrade.close*wca.scmst.sharesoutstanding),0)
     when substring(prices.lasttrade.currency,3,1)='X'
     then format(round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding),0)
     else format(round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding),0)
     end as usd_market_cap,
case when prices.lasttrade.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.lasttrade.comment like '%when issued%' then 'Y' else '' end as WIflag,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor,
prices.lasttrade.comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.secid=wca.scexh.secid
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join client.tholdsec on prices.lasttrade.localcode=client.tholdsec.symbol
                      and prices.lasttrade.mktclosedate=client.tholdsec.feeddate
left outer join wca.bbc on prices.lasttrade.secid = wca.bbc.secid 
                      and substring(prices.lasttrade.exchgcd,1,2) = wca.bbc.cntrycd 
                      and prices.lasttrade.currency = wca.bbc.curencd
                      and 'D'<>wca.bbc.actflag
left outer join wca.bbe on prices.lasttrade.secid = wca.bbe.secid 
                      and prices.lasttrade.exchgcd = wca.bbe.exchgcd
                      and prices.lasttrade.currency=bbe.curencd
                      and 'D'<>wca.bbe.actflag
left outer join prices.ajfactor on prices.lasttrade.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = prices.lasttrade.mic
                      and prices.ajfactor.exdate = date_format(now(),'%Y-%m-%d')
                      and 1=prices.ajfactor.option
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.currency='USD'
and (wca.ipo.ipoid is null or (wca.ipo.ipostatus<>'PENDING' and wca.ipo.ipostatus<>'WITHDRAWN'))
and wca.scexh.exchgcd='USBATS'
and (prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB')
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>'2006/12/31')
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND')
or ((prices.lasttrade.exchgcd='USCOMP' or prices.lasttrade.exchgcd='USOTCB') and prices.lasttrade.secid in (select secid from wca.scexh where exchgcd = 'USBATS' 
                                          and wca.scexh.actflag <> 'D' and wca.scexh.liststatus <> 'D' and wca.scexh.liststatus <> 'S')))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 1 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
union
select distinct
replace(prices.loadingtemp.localcode,'_','') as Symbol,
'' as Name,
'' as SectyCD,
'' as SecID,
prices.loadingtemp.Isin,
prices.loadingtemp.uscode as UScode,
'' as GICS,
'' as SIC,
'' as IndusID,
'' as Primaryexchange,
'' as bbgcompid,
'' as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
prices.loadingtemp.Exchange as ExchgCD,
'' as SecurityDesc,
'' as Lotsize,
'' as Listdate,
case when client.tholdsec.symbol<>'' then 'Yes' else '' end as RegshoExempt,
'' as SubPenny,
prices.loadingtemp.Currency,
prices.loadingtemp.High,
prices.loadingtemp.Low,
prices.loadingtemp.opening as Open,
prices.loadingtemp.closing as Close,
prices.loadingtemp.MktCloseDate,
prices.loadingtemp.tradedvolume*1000 as Tradedvolume,
'' as Shares,
'' as USD_market_cap,
case when prices.loadingtemp.comment like '%when distributed%' then 'Y' else '' end as WDflag,
case when prices.loadingtemp.comment like '%when issued%' then 'Y' else '' end as WIflag,
'' as factor,
prices.loadingtemp.Comment
from prices.loadingtemp
left outer join prices.lasttrade on prices.loadingtemp.localcode=prices.lasttrade.localcode
        and prices.loadingtemp.exchange=prices.lasttrade.exchgcd
left outer join client.tholdsec on prices.loadingtemp.localcode=client.tholdsec.symbol
                      and prices.loadingtemp.mktclosedate=client.tholdsec.feeddate

where
( prices.loadingtemp.exchange='USNASD'
  or prices.loadingtemp.exchange='USNYSE')
and prices.lasttrade.currency='USD'
and prices.lasttrade.secid is null
and substring(prices.loadingtemp.localcode,5,1)='Z'
and length(prices.loadingtemp.localcode)=5
union
select distinct
replace(wca.scexh.localcode,'_','') as Symbol,
wca.issur.issuername as Name,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as UScode,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.IndusID,
wca.scmst.PrimaryExchgcd,
wca.bbc.bbgcompid,
wca.bbc.bbgcomptk,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     else '100'
     end as Lotsize,
wca.scexh.listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.bbe.curencd as currency,
'' as high,
'' as low,
'' as open,
'' as close,
date_sub(date_format(now(),'%Y-%m-%d'), interval 0 day) as MktCloseDate,
'' as tradedvolume,
wca.scmst.sharesoutstanding as Shares,
'' as usd_market_cap,
'' as WDflag,
'' as WIflag,
'' as factor,
'' as comment
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                      and wca.scexh.exchgcd=prices.lasttrade.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
                      and substring( wca.scexh.exchgcd,1,2) = wca.bbc.cntrycd 
                      and 'D'<>wca.bbc.actflag
left outer join wca.bbe on  wca.scexh.secid = wca.bbe.secid 
                      and  wca.scexh.exchgcd = wca.bbe.exchgcd
                      and 'D'<>wca.bbe.actflag
                      and 'USD'=wca.bbe.curencd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid
where
prices.lasttrade.secid is null
and (wca.ipo.ipoid is null or wca.scmst.uscode<>'')
and (wca.ipo.ipoid is null or (wca.ipo.ipostatus<>'PENDING' and wca.ipo.ipostatus<>'WITHDRAWN'))
and wca.scexh.localcode<>''
and (wca.scexh.exchgcd='USAMEX'
  or wca.scexh.exchgcd='USCHI'
  or wca.scexh.exchgcd='USNASD'
  or wca.scexh.exchgcd='USNYSE'
  or wca.scexh.exchgcd='USPAC')
and wca.scexh.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null)
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
