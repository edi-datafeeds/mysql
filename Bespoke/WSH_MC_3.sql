-- arc=n
-- arp=n:\bespoke\wsh\
-- ddt=yyyy/mm/dd
-- dfn=n
-- dfo=n
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_WSH_SRF
-- fty=n
-- hdt=select ''
-- hpx=
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=select ''
-- dfo=

-- # 1
select distinct
wca.scexh.ScexhID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when prices.lasttrade.exchgcd<>wca.scexh.exchgcd
     then ''
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from client.pfisin
left outer join prices.lasttrade on client.pfisin.code = prices.lasttrade.isin 
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
			   and 'USOTC'=wca.scmst.primaryexchgcd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
			   and 'USOTC'=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
               and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where 
client.pfisin.accid = 367 and client.pfisin.actflag<>'D'
and prices.lasttrade.exchgcd = 'USOTCB'
and wca.scexh.exchgcd = 'USOTC'
and wca.scmst.primaryexchgcd = 'USOTC'
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND'))
and (prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USOTCB'
and latest.secid=prices.lasttrade.secid
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by latest.pricedate desc, currency desc limit 1))
and (prices.lasttrade.currency is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as isinlatest
inner join wca.scmst as latestscmst on isinlatest.secid=latestscmst.secid
where
wca.scmst.primaryexchgcd=wca.scexh.exchgcd
and isinlatest.exchgcd = 'USOTCB'
and wca.scmst.primaryexchgcd = 'USOTC'
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by isinlatest.pricedate desc, isinlatest.currency desc  limit 1))
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
