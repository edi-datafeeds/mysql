-- arc=n
-- arp=N:\Future_Takeover\
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_Future_Takeover_
-- fpx=EDI_Future_Takeover_
-- eof=select ''

-- # 1
select distinct
case when wca.tkovr.minitkovr='T' then 'NOOF' 
     when wca.tkovr.opendate is null then 'MRGR'
     when wca.tkovr.cmacqdate is null then 'TEND'
     else 'TEMR' end as Event,
wca.tkovr.tkovrid as EventID,
wca.tkovr.Actflag,
wca.tkovr.acttime as Changed,
wca.tkovr.announcedate as Created,
wca.exchg.exchgname as Exchange,
wca.issur.CntryofIncorp as Incorp,
wca.issur.Issuername as Issuer_Name,
wca.scmst.ISIN,
wca.sedol.SEDOL,
wca.scmst.uscode as US_code,
wca.scmst.primaryexchgcd as Primary_Exchange,
wca.exchg.MIC,
wca.tkovr.OfferorName as Offeror_Name,
wca.tkovr.offerorissid,
wca.scmst.SecID,
wca.issur.IssID,
wca.tkovr.minitkovr as Mini_Takeover,
wca.tkovr.closedate as Close_Date,
wca.tkovr.opendate as Open_Date,
wca.tkovr.cmacqdate as Compulsory_Acquistion_Date,
case when 3>wca.sectygrp.secgrpid then offsec.isin else '' end as OfferorEquityIsin
from wca.tkovr
inner join wca.scmst on wca.tkovr.secid=wca.scmst.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.scmst as offsec on wca.tkovr.offerorissid=offsec.issid
left outer join wca.sectygrp on offsec.sectycd=wca.sectygrp.sectycd
inner join wca.scexh on wca.tkovr.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
left outer join wca.mpay on wca.tkovr.tkovrid=mpay.eventid and 'TKOVR'=sevent
left outer join wca.scmst as res on wca.mpay.ressecid=res.secid
left outer join wca.sedol on wca.tkovr.secid= wca.sedol.secid
                         and wca.exchg.cntrycd=wca.sedol.cntrycd
where
wca.tkovr.closedate is not null
and (wca.tkovr.closedate > now() or wca.tkovr.cmacqdate>now())
and wca.scexh.liststatus<>'D'
and (wca.exchg.cntrycd='AT'
or wca.exchg.cntrycd='BE'
or wca.exchg.cntrycd='CY'
or wca.exchg.cntrycd='CZ'
or wca.exchg.cntrycd='GR'
or wca.exchg.cntrycd='ES'
or wca.exchg.cntrycd='CH'
or wca.exchg.cntrycd='FR');


-- select distinct
-- 'MRGR' as Event,
-- wca.mrgr.rdid as EventID,
-- wca.mrgr.Actflag,
-- wca.mrgr.acttime as Changed,
-- wca.mrgr.announcedate as Created,
-- wca.exchg.exchgname as Exchange,
-- wca.issur.CntryofIncorp as Incorp,
-- wca.issur.Issuername as Issuer_Name,
-- wca.scmst.ISIN,
-- wca.sedol.SEDOL,
-- wca.scmst.uscode as US_code,
-- wca.scmst.primaryexchgcd as Primary_Exchange,
-- wca.exchg.MIC,
-- wca.mrgr.Companies as Offeror_Name,
-- Offsec.issid,
-- wca.scmst.SecID,
-- wca.issur.IssID,
-- '' Mini_Takeover,
-- case when exdt.rdid is not null then exdt.exdate
--      else pexdt.exdate
--      end as Market_Date,
-- mrgr.effectivedate as effectiveDate,
-- case when 3>wca.sectygrp.secgrpid then offsec.isin else '' end as OfferorEquityIsin
-- from wca.mrgr
-- inner join wca.rd on wca.mrgr.rdid = wca.rd.rdid
-- inner join wca.scmst on wca.rd.secid = wca.scmst.secid
-- inner join wca.issur on wca.scmst.issid = wca.issur.issid
-- inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
-- inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
-- left outer join wca.sedol on wca.rd.secid= wca.sedol.secid
--                          and wca.exchg.cntrycd=wca.sedol.cntrycd
-- left outer join  wca.exdt on  wca.rd.rdid =  wca.exdt.rdid and  wca.scexh.exchgcd =  wca.exdt.exchgcd and 'MRGR' =  wca.exdt.eventtype
-- left outer join  wca.exdt as pexdt on rd.rdid = pexdt.rdid and  wca.scmst.primaryexchgcd = pexdt.exchgcd and 'MRGR' = pexdt.eventtype
-- left outer join wca.mpay on wca.mrgr.rdid=mpay.eventid and 'MRGR'=sevent
-- left outer join  wca.scmst as resscmst on  wca.mpay.ressecid = resscmst.secid
-- left outer join  wca.issur as resissur on resscmst.issid=resissur.issid
-- left outer join wca.scmst as offsec on resscmst.issid=offsec.issid
-- left outer join wca.sectygrp on offsec.sectycd=wca.sectygrp.sectycd
-- where
-- (pexdt.exdate > '2010/01/01' or wca.exdt.exdate > '2010/01/01')
-- and sectygrp.secgrpid is not null
-- and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
-- and scexh.actflag<>'D'
-- and wca.scexh.liststatus<>'D'
-- and (wca.exchg.cntrycd='AT'
-- or wca.exchg.cntrycd='BE'
-- or wca.exchg.cntrycd='CY'
-- or wca.exchg.cntrycd='CZ'
-- or wca.exchg.cntrycd='GR'
-- or wca.exchg.cntrycd='ES'
-- or wca.exchg.cntrycd='CH'
-- or wca.exchg.cntrycd='FR');
