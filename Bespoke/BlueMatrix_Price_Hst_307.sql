-- arc=y
-- arp=n:\bespoke\bluematrix\
-- fsx=_HistoricalPrices
-- hpx=EDI_BlueMatrix_HistoricalPrices_
-- hdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1
select distinct
client.pfcomptk.code,
client.pdat307.*,
case when prices.ajfactor.factor <> '' then prices.ajfactor.factor else '' end as factor
from client.pfcomptk
left outer join wca.bbc on code = BbgComptk
left outer join wca.scexh on wca.bbc.cntrycd=substring(wca.scexh.exchgcd,1,2)
			     and wca.bbc.secid=wca.scexh.secid
                             and 'D'<>wca.scexh.liststatus
                             and 'D'<>wca.scexh.actflag
left outer join prices.markets on wca.bbc.cntrycd = prices.markets.cntrycd                              
left outer join client.pdat307 on wca.bbc.secid = client.pdat307.secid
                      and client.pdat307.exchgcd=prices.markets.exchgcd
left outer join prices.ajfactor on client.pdat307.secid = prices.ajfactor.secid 
                      and prices.ajfactor.market = client.pdat307.mic
                      and prices.ajfactor.exdate = client.pdat307.mktclosedate
                      and 1=prices.ajfactor.option
Where
client.pfcomptk.accid=307
and client.pfcomptk.actflag='I'
and (client.pdat307.mic is not null or wca.scexh.secid is null);
-- and client.pdat307.pricedate=client.pdat307.mktclosedate;
