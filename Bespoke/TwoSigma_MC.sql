-- arc=y
-- arp=n:\bespoke\twosigma\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fex=.txt
-- fpx=
-- fdt=select DATE_FORMAT(max(feeddate), '%Y%m%d') from wca.tbl_opslog
-- hdt=select DATE_FORMAT(max(feeddate), '%Y%m%d') from wca.tbl_opslog
-- fty=y
-- hpx=EDI_SRF_TwoSigma_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE


-- # 1
select distinct
wca.scexh.ScexhID,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scmst.statusflag='I'
     then upper('D')
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' 
     then upper('L') 
     else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Close,
prices.lasttrade.Currency,
wca.scmst.sharesoutstanding as Shares,
wca.scmst.sharesoutstandingdate as EffectiveDate,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and prices.lasttrade.exchgcd=wca.scexh.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                     and 'USD'=exchgrates.liverates.base
where
prices.lasttrade.exchgcd=@exchgcd
and 
(prices.lasttrade.localcode is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and prices.lasttrade.mktclosedate<=now()
order by latest.currency desc, pricedate desc limit 1))
and wca.scmst.actflag<>'D'
and wca.scexh.liststatus<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
