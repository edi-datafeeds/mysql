-- arc=y
-- arp=n:\bespoke\wsh\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_WSH_SRF
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_SRF_WallStreetHorizon_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct
wca.scexh.ScexhID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
prices.lasttrade.sectycd <>'MF'
and substring(prices.lasttrade.exchgcd,1,2)='US'
and (prices.lasttrade.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd ='')
and prices.lasttrade.exchgcd not like 'USOTC%'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USCOMP'
and (latest.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd ='')
and prices.lasttrade.exchgcd not like 'USOTC%'
and latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency desc limit 1)
and (wca.scmst.isin ='' or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
substring(prices.lasttrade.exchgcd,1,2)='US'
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USCOMP'
and (uslatest.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd ='')
and prices.lasttrade.exchgcd not like 'USOTC%'
and uslatest.exchgcd=prices.lasttrade.exchgcd
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and uslatest.currency=prices.lasttrade.currency
order by uslatest.currency desc limit 1))
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 3 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and prices.lasttrade.exchgcd<>'USTRCE'
and prices.lasttrade.exchgcd<>'USCOMP'
union
select distinct
wca.scexh.ScexhID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on prices.lasttrade.exchgcd=wca.scexh.exchgcd and prices.lasttrade.secid=wca.scexh.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
prices.lasttrade.sectycd <>'MF'
and (prices.lasttrade.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd ='')
and substring(prices.lasttrade.exchgcd,1,2)='CA'
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
substring(prices.lasttrade.exchgcd,1,2)='CA'
and (latest.exchgcd=latest.primaryexchgcd or latest.primaryexchgcd ='')
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.currency limit 1)
and (wca.scmst.isin ='' or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
substring(prices.lasttrade.exchgcd,1,2)='CA'
and uslatest.exchgcd=prices.lasttrade.exchgcd
and (uslatest.exchgcd=wca.scmst.primaryexchgcd or wca.scmst.primaryexchgcd ='')
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
order by uslatest.currency limit 1))
and (prices.lasttrade.pricedate is null or pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 3 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
union
select distinct
wca.scexh.ScexhID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X'
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio as UNRatio,
unscmst.isin as UNIsin
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on prices.lasttrade.secid=wca.scexh.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join exchgrates.liverates on substring(lasttrade.currency,1,2) = substring(exchgrates.liverates.curr,1,2)
                      and 'USD'=exchgrates.liverates.base
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = wca.bbc.secid 
               and wca.exchg.cntrycd=wca.bbc.cntrycd
               and prices.lasttrade.currency=wca.bbc.curencd
               and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid 
               and wca.scexh.exchgcd = wca.bbe.exchgcd 
               and prices.lasttrade.currency=wca.bbe.curencd 
               and 'D'<>wca.bbe.actflag
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
left outer join wca.scmst as unscmst on wca.dprcp.unsecid = unscmst.secid
where
prices.lasttrade.sectycd <>'MF'
and prices.lasttrade.exchgcd = 'USOTCB'
and wca.scexh.exchgcd = 'USOTC'
and (wca.scmst.primaryexchgcd = 'USOTC' or wca.scmst.primaryexchgcd ='')
and prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.exchgcd = 'USOTCB'
and (wca.scmst.primaryexchgcd = 'USOTC' or wca.scmst.primaryexchgcd ='')
and latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
order by latest.pricedate desc, latest.currency desc limit 1)
and (wca.scmst.isin ='' or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as uslatest
inner join wca.scmst as latestscmst on uslatest.secid=latestscmst.secid
where
uslatest.exchgcd = 'USOTCB'
and (wca.scmst.primaryexchgcd = 'USOTC' or wca.scmst.primaryexchgcd ='')
and latestscmst.isin<>''
and latestscmst.isin=wca.scmst.isin
and uslatest.currency=prices.lasttrade.currency
order by uslatest.pricedate desc, uslatest.currency desc limit 1))
and (prices.lasttrade.pricedate is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 400 day)))
and ((ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
or (prices.lasttrade.sectycd <>'BND' and prices.lasttrade.sectycd <>'MF'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.liststatus<>'S' or wca.scexh.liststatus is null or prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 3 day)))
and (wca.scexh.liststatus<>'D' or wca.scexh.liststatus is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
