--filepath=o:\Datafeed\Beskoke\Ancerno_srf\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_WCA_ANCERNO_SRF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select distinct
wca.scmst.secid,
wca.scmst.issid,
wca.scmst.isin,
wca.scmst.uscode,
wca.issur.issuername,
wca.issur.cntryofincorp,
wca.scmst.securitydesc,
wca.scmst.sharesoutstanding,
wca.scmst.sectycd,
wca.scmst.primaryexchgcd,
prices.liveprices.exchgcd,
prices.liveprices.currency,
prices.liveprices.tradedvolume
from wca.scmst
inner join wca.issur on scmst.issid = wca.issur.issid
left outer join prices.liveprices on prices.liveprices.secid = wca.scmst.secid
                      and prices.liveprices.currency is not null
                      and prices.liveprices.currency <> ''
where
(wca.issur.issuername like '%Heineken%' or wca.issur.issuername like '%Deutsche bank%')
and wca.scmst.statusflag <> 'I'
and wca.scmst.actflag <> 'd'
and wca.scmst.sectycd<>'CW'
and wca.scmst.sectycd<>'BND'
order by prices.liveprices.exchgcd desc, wca.scmst.issid;