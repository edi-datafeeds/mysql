-- arc=y
-- arp=n:\bespoke\nse\
-- hpx=EDI_WCA_US_Static_Alert_
-- hdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((now()), '%Y%m%d')
-- fsx=select concat('_US_Static_Alert','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y
-- dfn=n

-- # 1
select 
'StaticCD' as f1,
'EventID' as f2,
'Actflag' as f3,
'ScexhID' as f3,
'Changed' as f3,
'Created' as f3,
'Localcode' as f3,
'IssuerName' as f3,
'SectyCD' as f2,
'SecID' as f2,
'Isin' as f3,
'UScode' as f3,
'SIC' as f3,
'GICS' as f3,
'EDIIndusID' as f3,
'PrimaryExchgCD' as f3,
'Sedol' as f3,
'ExchgCD' as f3,	
'SecurityDesc' as f3,
'Lotsize' as f3,	
'Listdate' as f3,	
'RegshoExempt' as f3,	
'SubPenny' as f3,	
'Micseg' as f3,		
'MktSegment' as f3,	
'RelatedEventCD' as f14,
'EffectiveDate' as f14,
'OldStatic' as f34,
'NewStatic' as f36,
'WDflag' as f34,
'WIflag' as f36;

-- # 2
select
upper('ISIN') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldIsin as OldStatic,
vtab.NewIsin as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_icc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldIsin <> vtab.NewIsin)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 3
select
upper('USCODE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldUScode as OldStatic,
vtab.NewUScode as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_icc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldUScode <> vtab.NewUScode)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 4
select
upper('LOCALCODE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldLocalcode as OldStatic,
vtab.NewLocalcode as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_lcc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldLocalcode <> vtab.NewLocalcode)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 5
select
upper('SEDOL') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldSedol as OldStatic,
vtab.NewSedol as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_sdchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.cntrycd=substring(wca.scexh.exchgcd,1,2)
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and vtab.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldSedol <> vtab.NewSedol)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 6
select
upper('SECNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.DateofChange,
vtab.SecOldName as OldStatic,
vtab.SecNewName as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.DateofChange>=date_sub(now(), interval 1 day) and vtab.DateofChange<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.SecOldName <> vtab.SecNewName)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';


-- # 7
select
upper('NLIST') as EventCD,
wca.scexh.ScexhID as EventID,
case when wca.scexh.actflag='D' then 'D' else '' end as Actflag,
wca.scexh.ScexhID,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
'' as RelatedEventCD,
wca.scexh.listdate as EffectiveDate,
'' as OldStatic,
'' as NewStatic,
'' as WDflag,
'' as WIflag
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.nlist on wca.scexh.scexhid = wca.nlist.scexhid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and wca.scexh.listdate>=date_sub(now(), interval 1 day) and wca.scexh.listdate<date_add(now(), interval 0 day)
and wca.nlist.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.exchg.cntrycd='US'
and wca.scexh.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 8
select
upper('ISSNAME') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.NameChangeDate as EffectiveDate,
vtab.IssOldName as OldStatic,
vtab.IssNewName as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ischg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.NameChangeDate>=date_sub(now(), interval 1 day) and vtab.NameChangeDate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.IssOldName <> vtab.IssNewName)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 9
select
upper('INCORP') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.InChgDate as EffectiveDate,
vtab.OldCntryCD as OldStatic,
vtab.NewCntryCD as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_inchg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.InChgDate>=date_sub(now(), interval 1 day) and vtab.InChgDate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldCntryCD <> vtab.NewCntryCD)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 10
select
upper('LISTSTATUS') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
'' as OldStatic,
vtab.LstatStatus as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_lstat as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and vtab.LstatStatus <> ''
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 11
select
upper('PRCHG') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldExchgCD as OldStatic,
vtab.NewExchgCD as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_prchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldExchgCD <> vtab.NewExchgCD)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 12
select
upper('IPO') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
'' as RelatedEventCD,
vtab.FirstTradingDate as FirstTradingDate,
'' as OldStatic,
'' as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ipo as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.ipostatus<>'HISTORY'
and vtab.FirstTradingDate>=date_sub(now(), interval 1 day) and vtab.FirstTradingDate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';

-- # 13
select
upper('LOTCHANGE') as StaticCD,
vtab.EventID,
vtab.actflag as Actflag,
wca.scexh.ScexhID,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
case when prices.lasttrade.localcode<>'' and prices.lasttrade.localcode is not null 
     then prices.lasttrade.localcode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecID,
wca.scmst.Isin,
wca.scmst.UScode,
wca.issur.SIC,
wca.issur.GICS,
wca.issur.IndusID,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.scexh.ExchgCD,
wca.scmst.SecurityDesc,
case when wca.scexh.lot<>'' and wca.scexh.lot is not null 
     then wca.scexh.lot 
     when wca.exchg.mic='XOTC' or wca.exchg.mic='OTCB'
     then '100'
     else ''
     end as Lotsize,
wca.scexh.Listdate,
'' as RegshoExempt,
'' as SubPenny,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
vtab.eventtype as RelatedEventCD,
vtab.EffectiveDate,
vtab.OldLot as OldStatic,
vtab.NewLot as NewStatic,
'' as WDflag,
'' as WIflag
from wca.v10s_ltchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.lasttrade on vtab.secid=prices.lasttrade.secid
                                and wca.scexh.exchgcd=prices.lasttrade.exchgcd
                                and date_sub(now(), interval 90 day) <ifnull(prices.lasttrade.pricedate,'2000-01-01')
WHERE
(prices.lasttrade.localcode is null or prices.lasttrade.localcode =
(select localcode from prices.lasttrade as latest
where
latest.currency=prices.lasttrade.currency
and latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by latest.pricedate desc limit 1))
and vtab.effectivedate>=date_sub(now(), interval 1 day) and vtab.effectivedate<date_add(now(), interval 0 day)
and vtab.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (vtab.OldLot <> vtab.NewLot)
and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
and wca.exchg.cntrycd='US'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                            or (wca.scmst.uscode = '' and wca.scmst.sectycd <>'BND'))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scexh.exchgcd<>'USTRCE';
