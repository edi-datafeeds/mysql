-- arp=n:\upload\acc\270
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
-- hpx=EDI_SRF_Cinnober_

-- # 1
select client.pfisin.code as Isin,
wca.issur.Issuername,
wca.issur.Shortname,
wca.scmst.SectyCD,
wca.scmst.ParValue,
wca.scmst.CurenCD,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.indus.IndusID,
wca.indus.IndusName,
wca.scmst.PrimaryExchgCD,
case when ifnull(wca.mktsg.mic,'') <> '' then wca.mktsg.mic else wca.exchg.mic end as MIC,
wca.scexh.ListDate,
case when wca.lstat.lstatstatus='D' then wca.lstat.effectivedate else null end as DelistDate,
wca.scmst.SharesOutstanding,
'Y' as EEARegFlag
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.indus on wca.issur.indusid = wca.indus.indusid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.lstat on wca.scexh.secid = wca.lstat.secid and wca.lstat.exchgcd = wca.scexh.exchgcd
where client.pfisin.accid = 270 and client.pfisin.actflag<>'D'
and (wca.scmst.primaryexchgcd = wca.scexh.exchgcd or wca.scmst.primaryexchgcd = '' or wca.scmst.actflag is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and wca.scmst.secid in (select client.pfisin.secid from client.pfisin
                             inner join wca.scexh on client.pfisin.secid = wca.scexh.secid
                             inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
                             inner join client.cinnobermic ON wca.exchg.mic = client.cinnobermic.mic
                             where client.pfisin.accid = 270 and client.pfisin.actflag<>'D')
and (wca.lstat.lstatid is null
or wca.lstat.lstatid = (select lstatid from wca.lstat as subl
                     where subl.secid = wca.lstat.secid and subl.exchgcd = lstat.exchgcd
                     and subl.effectivedate is not null
                     and subl.actflag<>'D'
                     order by subl.effectivedate desc limit 1))
union
select distinct client.pfisin.code as Isin,
'' as Issuername,
'' as Shortname,
'' as SectyCD,
'' as ParValue,
'' as CurenCD,
'' as CntryofIncorp,
'' as SIC,
'' as CIK,
'' as GICS,
'' as IndusID,
'' as IndusName,
'' as PrimaryExchgCD,
case when ifnull(wca.exchg.mic,'') <> '' then 'XXXX' else '' end as MIC,
'' as ListDate,
'' as DelistDate,
'' as SharesOutstanding,
'N' as EEARegFlag
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.indus on wca.issur.indusid = wca.indus.indusid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.lstat on wca.scexh.secid = wca.lstat.secid and wca.lstat.exchgcd = wca.scexh.exchgcd
where
client.pfisin.accid = 270 and client.pfisin.actflag<>'D'
and (wca.scmst.secid is null
or wca.scmst.secid not in (select client.pfisin.secid from client.pfisin
                           inner join wca.scexh on client.pfisin.secid = wca.scexh.secid
                           inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
                           inner join client.cinnobermic ON wca.exchg.mic = client.cinnobermic.mic
                           where client.pfisin.accid = 270 and client.pfisin.actflag<>'D'));