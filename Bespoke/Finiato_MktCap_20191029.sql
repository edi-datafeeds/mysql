-- arc=y
-- arp=n:\bespoke\finiato\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Finiato_MktCap
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq=3), '%Y%m%d')
-- hpx=Finiato_MktCap_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

select distinct
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     else '1'
     end as BbgExhSeqnum,
prices.lasttrade.Isin,
prices.lasttrade.uscode as Uscode,
prices.lasttrade.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
prices.lasttrade.PrimaryExchgCD,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Close,
prices.lasttrade.MktCloseDate,
wca.scmst.sharesoutstanding as Shares,
wca.scmst.sharesoutstandingdate as EffectiveDate,
prices.lasttrade.Currency,
case when prices.lasttrade.currency='USD'
     then round(prices.lasttrade.close*wca.scmst.sharesoutstanding)
     when prices.lasttrade.currency='GBX'
     then round(prices.lasttrade.close/100*wca.scmst.sharesoutstanding)
     when substring(prices.lasttrade.currency,3,1)='X' 
     then round((prices.lasttrade.close/exchgrates.liverates.rate)/100*wca.scmst.sharesoutstanding)
     else round((prices.lasttrade.close/exchgrates.liverates.rate)*wca.scmst.sharesoutstanding)
     end as USD_Market_Cap,wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.isscn.Website
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.bbc on substring(prices.lasttrade.exchgcd,1,2) = wca.bbc.cntrycd
                        and prices.lasttrade.secid = wca.bbc.secid
                        and substring(prices.lasttrade.currency,1,2) = substring(wca.bbc.curencd,1,2)
left outer join wca.bbe on prices.lasttrade.exchgcd = wca.bbe.exchgcd
                        and prices.lasttrade.secid = wca.bbe.secid
                        and substring(prices.lasttrade.currency,1,2) = substring(wca.bbe.curencd,1,2)
left outer join wca2.bbcseq as bbcseq on wca.bbc.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on wca.bbe.bbeid=bbeseq.bbeid
left outer join wca.isscn on wca.issur.issid = wca.isscn.issid
inner join exchgrates.liverates on prices.lasttrade.currency=exchgrates.liverates.curr
                                and 'USD'=exchgrates.liverates.base
where
(prices.lasttrade.exchgcd=prices.lasttrade.primaryexchgcd or prices.lasttrade.primaryexchgcd='')
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as isinlatest
where 
isinlatest.isin=prices.lasttrade.isin
and isinlatest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, isinlatest.currency limit 1)
and (prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1))
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.scmst.sharesoutstanding<>''
and prices.lasttrade.close<>0