-- arc=y
-- arp=n:\temp\
-- dfn=l
-- dsp=select ';'
-- hpx=EDI_621_
-- fdt=SELECT ''
-- fex=.txt
-- fty=n
-- eof=select ''

-- # 1
select
wca.scmst.issid,
wca.issur.issuername,
wca.scmst.securitydesc,
wca.scmst.isin,
wca.sedol.sedol,
case when wca.scmst.sectyCD = 'EQS' then '1 Equity'
     when wca.scmst.sectyCD = 'PRF' then '2 Preference'
     when wca.scmst.sectyCD = 'DR'  then '3 Depositary Receipt'
     when wca.scmst.sectyCD = 'BND' then '4 Debt'
else '5 Other'
end as SecurityType,
case when wca.scmst.primaryexchgcd = 'GBLSE' then smf4.market.TIDM
     else pscexh.localcode 
     end as PrimaryLocalcode,
smf4.market.TIDM as LSETicker,
smf4.Security.Longdesc as LSESecurityDesc,
wca.issur.lei,
''
from wca.scmst
left outer join wca.scexh on scmst.secid = wca.scexh.secid
left outer join wca.scexh as pscexh on wca.scmst.primaryexchgcd = pscexh.exchgcd
                      and wca.scmst.secid = pscexh.secid
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid 
                     and substring(wca.scexh.exchgcd,1,2)=wca.sedol.cntrycd
                     and 'D'<>wca.sedol.actflag
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
inner join smf4.security on sedol.sedol = smf4.security.sedol
left outer join smf4.market on smf4.security.securityid = smf4.market.securityid
                           and smf4.security.opol = smf4.market.mic
where (opol = 'AIMX' or opol = 'XLON')
and (wca.scexh.exchgcd = 'GBLSE' or scexh.exchgcd is null)
and (wca.scexh.actflag <> 'D' or scexh.actflag is null)
and wca.scexh.liststatus <> 'D'
and wca.scmst.actflag <> 'D'
and (wca.scmst.statusflag <> 'I' or wca.scmst.statusflag is null)
and wca.scmst.actflag <> 'D'
and smf4.market.tidm is not null
and (smf4.market.statusflag <> 'D' and smf4.market.statusflag is not null)
order by wca.scmst.issid, securitytype, primarylocalcode desc;

                           