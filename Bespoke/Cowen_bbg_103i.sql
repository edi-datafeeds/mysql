-- hpx=select concat('EDI_IPO_103_', DATE_FORMAT(feeddate, '%Y%m%d'),'_',seq) from wca.tbl_opslog order by wca.tbl_opslog.acttime_new desc limit 1;
-- fex=.103
-- fty=y
-- eor=Select concat(char(13),char(10))
-- fsx=select concat('_',seq) FROM wca.tbl_opslog order by wca.tbl_opslog.acttime_new desc limit 1;
-- fdt=select DATE_FORMAT(max(feeddate), '%Y%m%d') from wca.tbl_opslog;
-- dtm=select '';
-- hdt=SELECT '';
-- arp=N:\Bespoke\Cowen\BBG\
-- dfn=n



-- # 0

select 
'Created' as Created,
'Modified' as Modified,
'ID' as ID,
'Status' as Status,
'SedolSeqnumReg' as SedolSeqnumReg,
'SedolSeqnumCur' as SedolSeqnumCur,
'BbgCompSeqnum' as BbgCompSeqnum,
'BbgExhSeqnum' as BbgExhSeqnum,
'Actflag' as Actflag,
'IssuerName' as IssuerName,
'CntryInc' as CntryofIncorp,
'ExchgCd' as ExchgCd,
'ISIN' as ISIN,
'Sedol' as Sedol,
'RegCntry' as RegCntry,
'TradingCurrency' as TradingCurrency,
'BbgCompositeGlobalID' as BbgCompositeGlobalID,
'BbgCompositeTicker' as BbgCompositeTicker,
'BbgGlobalID' as BbgGlobalID,
'BbgExchangeTicker' as BbgExchangeTicker,
'USCode' as USCode,
'Symbol' as Symbol,
'SecDescription' as SecDescription,
'PVCurrency' as PVCurrency,
'ParValue' as ParValue,
'Sectype' as Sectype,
'LotSize' as LotSize,
'SubPeriodFrom' as SubPeriodFrom,
'SubperiodTo' as SubperiodTo,
'MinSharesOffered' as MinSharesOffered,
'MaxSharesOffered' as MaxSharesOffered,
'SharesOutstanding' as SharesOutstanding,
'Currency' as Currency,
'SharePriceLowest' as SharePriceLowest,
'SharePriceHighest' as SharePriceHighest,
'ProposedPrice' as ProposedPrice,
'InitialPrice' as InitialPrice,
'InitialTradedVolume' as InitialTradedVolume,
'Underwriter' as Underwriter,
'DealType' as DealType,
'LawFirm' as LawFirm,
'TransferAgent' as TransferAgent,
'IndusCode' as IndusCode,
'FirstTradingDate' as FirstTradingDate,
'SecLevel' as SecLevel,
'TOFCurrency' as TOFCurrency,
'TotalOfferSize' as TotalOfferSize,
'MIC' as MIC,
'Notes' as Notes


-- # 1

select 
AnnounceDate as Created,
DATE_FORMAT(@time, '%Y/%m/%d') as Modified,
RumID as ID,
'Rumor' as Status,
'1' as SedolSeqnumReg,
'1' as SedolSeqnumCur,
'1' as BbgCompSeqnum,
'1' as BbgExhSeqnum,
Actflag as Actflag,
IssuerName,
'' as CntryofIncorp,
ExchgCD,
'' as ISIN,
'' as Sedol,
'' as RegCntry,
'' as TradingCurrency,
'' as BbgCompositeGlobalID,
'' as BbgCompositeTicker,
'' as BbgGlobalID,
'' as BbgExchangeTicker,
'' as USCode,
LocalCode as Symbol,
'' as SecDescription,
'' as PVCurrency,
'' as ParValue,
SectyCD as SecType,
'' as LotSize,
'' as SubPeriodFrom,
'' as SubperiodTo,
'' as MinSharesOffered,
NoOfShares as MaxSharesOffered,
'' as SharesOutstanding,
CurenCD as Currency,
'' as SharePriceLowest,
IssPrice as SharePriceHighest,
'' as ProposedPrice,
'' as InitialPrice,
'' as InitialTradedVolume,
'' as Underwriter,
'' as DealType,
'' as LawFirm,
'' as TransferAgent,
'' as IndusCode,
'' as FirstTradingDate,
'' as SecLevel,
'' as TOFCurrency,
case when IssValue <>'' then (IssValue * 1000000 ) else null end as TotalOfferSize,
'' as MIC,
Notes as Notes
from wca.rum
where 
substring(exchgcd,1,2)='US'
and wca.rum.acttime >= (select max(acttime_new) from wca.tbl_opslog)
and wca.rum.IPOID is NULL



-- # 2

select 
wca.ipo.AnnounceDate as Created,
DATE_FORMAT(@time, '%Y/%m/%d') as Modified,
wca.ipo.IpoID+100000 as ID,
wca.ipo.IPOStatus as Status,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.ipo.Actflag,
wca.issur.Issuername as Issuer,
wca.issur.CntryofIncorp as CntryInc,
wca.scexh.ExchgCd,
wca.scmst.ISIN,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
wca.scmst.USCode,
wca.scexh.Localcode as Symbol,
wca.scmst.SecurityDesc as SecDescription,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ParValue,
wca.scmst.SecTyCD as Sectype,
wca.scexh.Lot as LotSize,
wca.ipo.SubPeriodFrom,
wca.ipo.SubperiodTo,
wca.ipo.MinSharesOffered,
wca.ipo.MaxSharesOffered,
wca.ipo.SharesOutstanding,
wca.ipo.TOFCurrency as Currency,
wca.ipo.SharePriceLowest,
wca.ipo.SharePriceHighest,
wca.ipo.ProposedPrice,
wca.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.ipo.Underwriter,
wca.ipo.DealType,
wca.ipo.LawFirm,
wca.ipo.TransferAgent,
'' as IndusCode,
wca.ipo.FirstTradingDate,
'' as SecLevel,
wca.ipo.TOFCurrency,
case when wca.ipo.TotalOfferSize<>'' then ((wca.ipo.TotalOfferSize )*1000000 ) else null end as TotalOfferSize,
wca.exchg.mic as MIC,
wca.ipo.Notes
from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.ipo.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
where
(wca.ipo.acttime >= (select max(acttime_new) from wca.tbl_opslog)
or 
(wca.scmst.acttime >= (select max(acttime_new) from wca.tbl_opslog)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
or 
(wca.scexh.acttime >= (select max(acttime_new) from wca.tbl_opslog)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
or 
(wca2.sedolseq.acttime >= (select max(acttime_new) from wca.tbl_opslog)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
or 
(wca2.bbcseq.acttime >= (select max(acttime_new) from wca.tbl_opslog)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate) 
or 
(wca2.bbeseq.acttime >= (select max(acttime_new) from wca.tbl_opslog)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)      
)
and wca.exchg.cntrycd='US'
and wca.scexh.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scmst.primaryexchgcd = wca.scexh.exchgcd