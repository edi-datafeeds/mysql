-- arc=y
-- arp=n:\bespoke\mergent\
-- ddt=yyyymmdd
-- dsp=select'|'
-- dtm=select ''
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- hdt=select ''
-- eof=select ''
-- hpx=select concat('DATE','|','01','|','00','|',date_format(now(),'%d-%b-%Y'),'|')
-- dfn=n
-- fty=y

-- # 1
select distinct
'' as VendorID,
'01' as RecordCode,
'01' as RecordSequence,
date_format(prices.lasttrade.pricedate,'%d-%b-%Y') as LatestTradeDate,
prices.lasttrade.issuername as CompanyName,
prices.lasttrade.localcode as TickerSymbol,
cast(prices.lasttrade.close as decimal(23,6)) as LatestClosePrice,
case when phista.secid is not null and cast(phista.high as decimal(23,6)) < cast(prices.lasttrade.close as decimal(23,6)) 
     then cast(prices.lasttrade.close as decimal(23,6)) 
     when phista.secid is not null 
     then cast(phista.high as decimal(23,6))
     when phistu.secid is not null and cast(phistu.high as decimal(23,6)) < cast(prices.lasttrade.close as decimal(23,6))
     then cast(prices.lasttrade.close as decimal(23,6)) 
     else cast(phistu.high as decimal(23,6)) 
     end as 52WeekHighPrice,
case when phista.secid is not null and cast(phista.low as decimal(23,6)) > cast(prices.lasttrade.close as decimal(23,6))
     then cast(prices.lasttrade.close as decimal(23,6))
     when phista.secid is not null
     then cast(phista.low as decimal (23,6))
     when phistu.secid is not null and cast(phistu.low as decimal(23,6)) > cast(prices.lasttrade.close as decimal(23,6)) 
     then cast(prices.lasttrade.close as decimal(23,6))
     else cast(phistu.low as decimal(23,6))
     end as 52WeekLowPrice,
'' as Alpha_60_Months,
phistu.adv as 52_Week_Avg_Daily_Volume,
'' as Beta_60_Months,
case when prices_aux.cashdivregseq1.frequency='ANL'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))
     when prices_aux.cashdivregseq1.frequency='SMA'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))*2
     when prices_aux.cashdivregseq1.frequency='TRM'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))*3
     when prices_aux.cashdivregseq1.frequency='QTR'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))*4
     when prices_aux.cashdivregseq1.frequency='MNT'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))*12
     when prices_aux.cashdivregseq1.frequency='BIM'
     then cast(prices_aux.cashdivregseq1.GrossDividend as decimal(23,6))*24
     else ''
     end as DividendRate,
'' as 'Float',
'' as Net_Insider_Transaction_Latest_Month,
wca.scmst.sharesoutstanding as Latest_Shares_Outstanding,
'' as Latest_Short_Interest_Ratio,
'' as Number_Institutions_Holding_Shares,
'' as Shares_Held_by_Institutions,
'' as Percent_of_Shares_Out_Owned_by_Instit,
date_format(spl1.splitdate, '%Y%m%d') as SplitDate1,
date_format(spl2.splitdate, '%Y%m%d') as SplitDate2,
date_format(spl3.splitdate, '%Y%m%d') as SplitDate3,
date_format(spl4.splitdate, '%Y%m%d') as SplitDate4,
date_format(spl5.splitdate, '%Y%m%d') as SplitDate5,
case when spl1.newratio = ''
     then ''
     else cast(spl1.newratio as decimal(23,2))/cast(spl1.oldratio as decimal(23,2))
     end as SplitFactor1,
case when spl2.newratio = ''
     then ''
     else cast(spl2.newratio as decimal(23,2))/cast(spl2.oldratio as decimal(23,2))
     end as SplitFactor2,
case when spl3.newratio = ''
     then ''
     else cast(spl3.newratio as decimal(23,2))/cast(spl3.oldratio as decimal(23,2))
     end as SplitFactor3,
case when spl4.newratio = ''
     then ''
     else cast(spl4.newratio as decimal(23,2))/cast(spl4.oldratio as decimal(23,2))
     end as SplitFactor4,
case when spl5.newratio = ''
     then ''
     else cast(spl5.newratio as decimal(23,2))/cast(spl5.oldratio as decimal(23,2))
     end as SplitFactor5,
prices.lasttrade.mic as ExchangeID,
prices.lasttrade.currency as PriceCurrencyID,
'' as CompanyID,
prices.lasttrade.uscode as Cusip,
prices.lasttrade.isin as Isin
FROM prices.lasttrade
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
left outer join prices_aux.cashdivregseq1 on 1 = prices_aux.cashdivregseq1.seqnum and prices.lasttrade.secid = prices_aux.cashdivregseq1.secid and prices.lasttrade.currency = prices_aux.cashdivregseq1.curencd
left outer join prices_aux.splitseq1 as spl1 on 1 = prices_aux.spl1.seqnum and prices.lasttrade.exchgcd = spl1.exchgcd and prices.lasttrade.secid = spl1.secid
left outer join prices_aux.splitseq1 as spl2 on 2 = prices_aux.spl2.seqnum and prices.lasttrade.exchgcd = spl2.exchgcd and prices.lasttrade.secid = spl2.secid
left outer join prices_aux.splitseq1 as spl3 on 3 = prices_aux.spl3.seqnum and prices.lasttrade.exchgcd = spl3.exchgcd and prices.lasttrade.secid = spl3.secid
left outer join prices_aux.splitseq1 as spl4 on 4 = prices_aux.spl4.seqnum and prices.lasttrade.exchgcd = spl4.exchgcd and prices.lasttrade.secid = spl4.secid
left outer join prices_aux.splitseq1 as spl5 on 5 = prices_aux.spl5.seqnum and prices.lasttrade.exchgcd = spl5.exchgcd and prices.lasttrade.secid = spl5.secid
left outer join pricing_hist.t_p04_52hla as phistu on prices.lasttrade.exchgcd = phistu.exchgcd
                                                and prices.lasttrade.secid = phistu.secid
                                                and prices.lasttrade.currency = phistu.currency
                                                and prices.lasttrade.localcode = phistu.pricefilesymbol
left outer join pricing_hist.t_adj_52hla as phista on prices.lasttrade.exchgcd = phista.exchgcd
                                                and prices.lasttrade.secid = phista.secid
                                                and prices.lasttrade.currency = phista.currency
                                                and prices.lasttrade.localcode = phista.pricefilesymbol
where
substring(prices.lasttrade.exchgcd,1,2)<>'US'
and substring(prices.lasttrade.exchgcd,1,2)<>'CA'
and prices.lasttrade.sectycd<>'BND'
and prices.lasttrade.sectycd<>'CW'
and prices.lasttrade.sectycd<>'MF'
and prices.lasttrade.mktclosedate>date_sub(now(), interval 7 day)
and prices.lasttrade.pricedate>date_sub(now(), interval 31 day)
and (phistu.secid is not null or phista.secid is not null)