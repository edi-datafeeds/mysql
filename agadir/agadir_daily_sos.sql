-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Daily_SOS
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Daily_SOS_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.dvst.rationew)/wca.dvst.ratioold as newsos
from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'dvst'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and (wca.scmst.secid = wca.dvst.ressecid or wca.dvst.ressecid is null)
and wca.scmst.sharesoutstanding <> ''
and wca.dvst.rationew <> ''
and wca.dvst.ratioold <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.mpay.ratioold + wca.mpay.rationew)/wca.mpay.ratioold as newsos
from wca.dist
left outer join wca.mpay on dist.rdid = mpay.eventid and 1 = mpay.optionid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dist.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'DIST'=mpay.sevent
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DIST'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and mpayopt2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.mpay.rationew)/wca.mpay.ratioold as newsos
from wca.dmrgr
left outer join wca.mpay on dmrgr.rdid = mpay.eventid and 1 = mpay.optionid and 'DMRGR'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dmrgr.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'dmrgr'=mpay.sevent
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DMRGR'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and mpayopt2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.divpy.ratioold + wca.divpy.rationew)/wca.divpy.ratioold as newsos
from wca.div_my
left outer join wca.v10s_divpy as divpy on div_my.divid = divpy.divid and 1 = divpy.optionid
left outer join wca.v10s_divpy as divopt2 on div_my.divid = divopt2.divid and 2 = divopt2.optionid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'div'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and divopt2.divid is null and divpy.divtype='S'
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.divpy.ratioold <> ''
and wca.divpy.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.caprd.newratio)/wca.caprd.oldratio as newsos
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rd on wca.caprd.rdid = wca.rd.rdid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'SD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.caprd.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3)and wca.caprd.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.caprd.oldratio <> ''
and wca.caprd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.sd.newratio)/wca.sd.oldratio as newsos
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'SD'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.sd.oldratio <> ''
and wca.sd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union ###
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.rts.ratioold + wca.rts.rationew)/wca.rts.ratioold as newsos
from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'rts'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.rts.ratioold <> ''
and wca.rts.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.ent.ratioold + wca.ent.rationew)/wca.ent.ratioold as newsos
from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'ent'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.ent.ratioold <> ''
and wca.ent.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.consd.newratio)/wca.consd.oldratio as newsos
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'consd'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'ENT'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.consd.oldratio <> ''
and wca.consd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.bon.ratioold + wca.bon.rationew)/wca.bon.ratioold as newsos
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'bon'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.bon.ratioold <> ''
and wca.bon.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
shh.oldsos,
shh.newsos
from wca.shoch as shh
inner join wca.scmst on shh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.scmst.actflag<>'D'
and wca.issur.actflag<>'D'
and (shh.actflag<>'D' or shh.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3))
and shh.newsos<>''
and shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1)
union
select
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.primaryexchgcd,
wca.shoch.oldsos,
wca.scmst.sharesoutstanding as newsos
from wca.scmst
left outer join wca.shoch on wca.scmst.secid=wca.shoch.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(substring(wca.scmst.primaryexchgcd,1,2)='BE' or substring(wca.scmst.primaryexchgcd,1,2)='CH' or substring(wca.scmst.primaryexchgcd,1,2)='DE' or substring(wca.scmst.primaryexchgcd,1,2)='SA' 
or substring(wca.scmst.primaryexchgcd,1,2)='FR' or substring(wca.scmst.primaryexchgcd,1,2)='IT' or substring(wca.scmst.primaryexchgcd,1,2)='PT') 
and wca.shoch.secid is null
and wca.scmst.actflag<>'D'
and wca.scmst.sharesoutstanding <> '';