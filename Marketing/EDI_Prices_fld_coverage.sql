DROP TABLE IF EXISTS `prices`.`tmp_mkt`;

CREATE TABLE  `prdextra`.`tmp_mkt` (
  `providerid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exchgcd` varchar(45) NOT NULL,
  `exchgname` varchar(150) NOT NULL,
  `localcode` varchar(1) NOT NULL DEFAULT 'N',
  `isin` varchar(1) NOT NULL DEFAULT 'N',
  `uscode` varchar(1) NOT NULL DEFAULT 'N',
  `open` varchar(1) DEFAULT 'N',
  `high` varchar(1) NOT NULL DEFAULT 'N',
  `low` varchar(1) NOT NULL DEFAULT 'N',
  `close` varchar(1) NOT NULL DEFAULT 'N',
  `mid` varchar(1) NOT NULL DEFAULT 'N',
  `last` varchar(1) NOT NULL DEFAULT 'N',
  `bid` varchar(1) NOT NULL DEFAULT 'N',
  `ask` varchar(1) NOT NULL DEFAULT 'N',
  `bidsize` varchar(1) NOT NULL DEFAULT 'N',
  `asksize` varchar(1) NOT NULL DEFAULT 'N',
  `tradedvolume` varchar(1) NOT NULL DEFAULT 'N',
  `tradedvalue` varchar(1) NOT NULL DEFAULT 'N',
  `totaltrades` varchar(1) NOT NULL DEFAULT 'N',
  `mktclosedate` datetime NOT NULL,
  PRIMARY KEY (`providerid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



INSERT INTO prices.tmp_mkt (providerid,exchgcd,exchgname,mktclosedate)
  (SELECT markets.id, markets.marketcode, markets.description, markets.mktclosedate
    FROM markets WHERE markets.outputscript like '%bat'and markets.mktclosedate >= '2010-03-02'
order by marketcode);



update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.totaltrades = 'Y'
where t.totaltrades > 0 and t.totaltrades <> 0 and t.totaltrades is not null and t.totaltrades <> ''
and t.exchgcd <> '' and t.exchgcd is not null;

update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.tradedvalue = 'Y'
where t.tradedvalue > 0 and t.tradedvalue <> 0 and t.tradedvalue is not null and t.tradedvalue <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.tradedvolume = 'Y'
where t.tradedvolume > 0 and t.tradedvolume <> 0 and t.tradedvolume is not null and t.tradedvolume <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.asksize = 'Y'
where t.asksize > 0 and t.asksize <> 0 and t.asksize is not null and t.asksize <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.bidsize = 'Y'
where t.bidsize > 0 and t.bidsize <> 0 and t.bidsize is not null and t.bidsize <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.ask = 'Y'
where t.Ask > 0 and t.Ask <> 0 and t.Ask is not null and t.Ask <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.bid = 'Y'
where t.bid > 0 and t.bid <> 0 and t.bid is not null and t.bid <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.last = 'Y'
where t.last > 0 and t.last <> 0 and t.last is not null and t.last <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.mid = 'Y'
where t.mid > 0 and t.mid <> 0 and t.mid is not null and t.mid <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.close = 'Y'
where t.close > 0 and t.close <> 0 and t.close is not null and t.close <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.low = 'Y'
where t.low > 0 and t.low <> 0 and t.low is not null and t.low <> ''
and t.exchgcd <> '' and t.exchgcd is not null;



update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.high = 'Y'
where t.high > 0 and t.high <> 0 and t.high is not null and t.high <> ''
and t.exchgcd <> '' and t.exchgcd is not null;



update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.open = 'Y'
where t.open > 0 and t.open <> 0 and t.open is not null and t.open <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.uscode = 'Y'
where t.uscode is not null and t.uscode <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.isin = 'Y'
where t.isin is not null and t.isin <> ''
and t.exchgcd <> '' and t.exchgcd is not null;


update prices.tmp_mkt as e
inner join prices.liveprices as t on e.providerid = t.providerid
set e.localcode = 'Y'
where t.localcode is not null and t.localcode <> ''
and t.exchgcd <> '' and t.exchgcd is not null;