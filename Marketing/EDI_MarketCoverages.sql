INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'WCA' AS prod_cd, exchgcd from wca.exchg
where actflag <>'d'
order by exchgcd;


INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'EODP' AS prod_cd, marketcode from prices.markets
where prices.markets.viewonweb = 1
and prices.markets.marketcode not like '%\\_%'
and prices.markets.mktclosedate > date_sub(curdate(),interval 550 day);

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'WDI' AS prod_cd, exchange_code from wca2.evf_dividend
where evf_dividend.changed>date_sub(curdate(),interval 550 day);

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'WSO' AS prod_cd, exchange_code from wca2.evf_shares_outstanding_change
where evf_shares_outstanding_change.changed>date_sub(curdate(),interval 550 day);

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'WFI' AS prod_cd, wca.scexh.exchgcd from wca.scexh
inner join wca.bond on  wca.scexh.secid = wca.bond.secid  and  liststatus <> 'D'
Where wca.scexh.Actflag <> 'D' and wca.scexh.ListStatus <> 'D';

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'IPO' AS prod_cd, wca.scexh.exchgcd from wca.scexh
inner join wca.ipo on  wca.scexh.secid = wca.ipo.secid
Where wca.scexh.Actflag <> 'D' ;

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'FPO' AS prod_cd, wca.scexh.exchgcd from wca.scexh
inner join wca.fpo on  wca.scexh.secid = wca.fpo.secid
Where wca.scexh.Actflag <> 'D' ;

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'UTD' AS prod_cd, exchange_code from wca2.evf_dividend_unittrust;

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct 'PUBHOL' AS prod_cd, exchgcd from finhols.pubhol_exch_map
Where exchgcd !='';

INSERT IGNORE INTO prdxtra.prd_coverage (prod_cd,exchgcd)
select distinct  'ADJF' AS prod_cd, exchgcd from prdxtra.prd_coverage
where FIND_IN_SET(prod_cd ,'WCA,EODP') > 0;
  


