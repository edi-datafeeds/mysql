DROP TABLE IF EXISTS `prdxtra`.`test_coverage`;

CREATE TABLE  `prdxtra`.`test_coverage` (
  `region` varchar(200) DEFAULT NULL,
  `country` varchar(70) DEFAULT NULL,
  `exchgcd` varchar(20) DEFAULT NULL,
  `mic` varchar(6) DEFAULT NULL,
  `exchgname` varchar(100) DEFAULT NULL,
  `wca` varchar(1) NOT NULL DEFAULT 'N',
  `wdi` varchar(1) NOT NULL DEFAULT 'N',
  `wso` varchar(1) NOT NULL DEFAULT 'N',
  `wfi` varchar(1) NOT NULL DEFAULT 'N',
  `ipo` varchar(1) NOT NULL DEFAULT 'N',  
  `fpo` varchar(1) NOT NULL DEFAULT 'N',  
  `utd` varchar(1) NOT NULL DEFAULT 'N',
  `pubhol` varchar(1) NOT NULL DEFAULT 'N',
  `eodp` varchar(1) NOT NULL DEFAULT 'N',  
  `adjf` varchar(1) NOT NULL DEFAULT 'N',
  `etd` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



INSERT INTO prdxtra.test_coverage (exchgcd)
    select distinct exchgcd from wca.exchg
    where actflag <>'d'
    union
    SELECT distinct prices.markets.marketcode as exchgcd FROM prices.markets
    where prices.markets.viewonweb = 1
    and prices.markets.marketcode not like '%\\_%'
    and prices.markets.mktclosedate > date_sub(curdate(),interval 550 day)
 order by exchgcd;


update prdxtra.test_coverage
inner join wca.exchg on prdxtra.test_coverage.exchgcd = wca.exchg.exchgcd
set prdxtra.test_coverage.exchgname = wca.exchg.exchgname;


update prdxtra.test_coverage
inner join prices.markets on prdxtra.test_coverage.exchgcd = prices.markets.marketcode
set prdxtra.test_coverage.exchgname = prices.markets.description
where prdxtra.test_coverage.exchgname is null;


update prdxtra.test_coverage
inner join wca.exchg on prdxtra.test_coverage.exchgcd = wca.exchg.exchgcd
set prdxtra.test_coverage.mic = wca.exchg.MIC;


update prdxtra.test_coverage
inner join prices.markets on prdxtra.test_coverage.exchgcd = prices.markets.marketcode
set prdxtra.test_coverage.mic = prices.markets.mic
where prdxtra.test_coverage.mic is null;


update prdxtra.test_coverage
inner join prdxtra.country_region on substring(prdxtra.test_coverage.exchgcd,1,2) = prdxtra.country_region.country_code
set prdxtra.test_coverage.country = prdxtra.country_region.country,
prdxtra.test_coverage.region = prdxtra.country_region.region;


update prdxtra.test_coverage
inner join prices.markets on test_coverage.exchgcd = prices.markets.marketcode
set eodp = 'X'
where prices.markets.viewonweb = 1
and prices.markets.marketcode not like '%\\_%'
and prices.markets.mktclosedate > date_sub(curdate(),interval 550 day);



update prdxtra.test_coverage
inner join wca2.evf_dividend on prdxtra.test_coverage.exchgcd = wca2.evf_dividend.exchange_code
set wca = 'X', wdi = 'X'
where wca2.evf_dividend.changed>date_sub(curdate(),interval 550 day);


update prdxtra.test_coverage
inner join wca2.evf_shares_outstanding_change on prdxtra.test_coverage.exchgcd = wca2.evf_shares_outstanding_change.exchange_code
set wso = 'X'
where evf_shares_outstanding_change.changed>date_sub(curdate(),interval 550 day);


update  prdxtra.test_coverage 
inner join wca.scexh on prdxtra.test_coverage.exchgcd = wca.scexh.exchgcd
and wca.scexh.Actflag <> 'D' and wca.scexh.ListStatus <> 'D'
inner join wca.bond on  wca.scexh.secid = wca.bond.secid
and  liststatus <> 'D'
set wfi = 'X';


update prdxtra.test_coverage
inner join wca.scexh on prdxtra.test_coverage.exchgcd = wca.scexh.exchgcd
and wca.scexh.Actflag <> 'D' 
inner join wca.ipo on  wca.scexh.secid = wca.ipo.secid
set ipo = 'X';

update prdxtra.test_coverage
inner join wca.scexh on prdxtra.test_coverage.exchgcd = wca.scexh.exchgcd
and wca.scexh.Actflag <> 'D' 
inner join wca.fpo on  wca.scexh.secid = wca.fpo.secid
set fpo = 'X';

update prdxtra.test_coverage
set adjf = 'X'
where wca = 'X' and eodp='X';

update  prdxtra.test_coverage
inner join wca2.evf_dividend_unittrust on prdxtra.test_coverage.exchgcd = wca2.evf_dividend_unittrust.exchange_code
and wca2.evf_dividend_unittrust.changed >date_sub(curdate(),interval 550 day)
set utd= 'X';


update prdxtra.test_coverage
inner join finhols.pubhol_exch_map on prdxtra.test_coverage.exchgcd = finhols.pubhol_exch_map.exchgcd
set pubhol = 'X';

DROP TABLE IF EXISTS `prdxtra`.`mkt_coverage_bkp`;

ALTER TABLE `prdxtra`.`mkt_coverage` RENAME TO `prdxtra`.`mkt_coverage_bkp`;

ALTER TABLE `prdxtra`.`test_coverage` RENAME TO `prdxtra`.`mkt_coverage`;

ALTER TABLE `prdxtra`.`mkt_coverage` ADD INDEX `idx_exchangcd` (`exchgcd` ASC)  COMMENT '';
