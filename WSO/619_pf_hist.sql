-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.619
-- filenamesuffix=
-- headerprefix=EDI_WCA_Shares_Outstanding_619_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # BIG 1
select distinct
-- wca.scmst.SecID,
client.pfsecid.code as SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
'' as shochNotes
FROM wca.scmst
inner JOIN client.pfsecid on wca.scmst.secid = client.pfsecid.code
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where client.pfsecid.accid = 999 and client.pfsecid.actflag <> 'D'
and wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.issur.actflag<>'D'
and (shh.actflag<>'D' or shh.actflag is null)
and (shh.newsos<>'' or wca.scmst.sharesoutstanding<>'')
order by client.pfsecid.code;