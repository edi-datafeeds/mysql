-- arc=y
-- arp=n:\temp\
-- dfn=n
-- hpx=select ''
-- fex=.670
-- eof=EDI_ENDOFFILE
-- hdt=SELECT ''


-- # BIG 4
select
wca.scmst.Acttime,
wca.scmst.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
case when wca.bbc.cntrycd is null then substring(wca.scmst.primaryexchgcd,1,2) else wca.bbc.cntrycd end as listing_country,
wca.bbc.curencd as trading_currency,
wca.bbc.bbgcompid as composite_global_id,
wca.bbc.bbgcomptk as bloomberg_composite_ticker,
wca.bbe.exchgcd as listing_exchange,
wca.bbe.bbgexhid as bloomberg_global_id,
wca.bbe.bbgexhtk as bloomberg_exchange_ticker,
wca.scmst.secid as EventID,
'SCMST' as eventtype,
wca.scmst.sharesoutstandingdate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
'' as oldsos,
wca.scmst.sharesoutstanding as newsos
from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.shoch on wca.scmst.secid = wca.shoch.secid
left outer join wca.bbc on wca.scmst.secid = wca.bbc.secid and substring(wca.scmst.primaryexchgcd,1,2)=wca.bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scmst.secid = wca.bbe.secid and wca.bbc.cntrycd = substring(wca.bbe.exchgcd,1,2) and  wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
where
shoch.shochid is null
and scmst.sharesoutstanding<>''
and wca.scmst.actflag<>'D'

