-- arc=y
-- arp=n:\bespoke\limad\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_LimAd_WSO_Alert
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WSO_CALC_ALERT_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select
wca.dvst.acttime,
wca.dvst.actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.dvst.rdid as eventid,
'dvst' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.dvst.ratioold,
wca.dvst.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.dvst.rationew)/wca.dvst.ratioold as newsos
from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'dvst'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and (wca.scmst.secid = wca.dvst.ressecid or wca.dvst.ressecid is null)
and wca.scmst.sharesoutstanding <> ''
and wca.dvst.rationew <> ''
and wca.dvst.ratioold <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.dist.Acttime,
wca.dist.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.dist.rdid as eventid,
'DIST' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.mpay.ratioold,
wca.mpay.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.mpay.ratioold + wca.mpay.rationew)/wca.mpay.ratioold as newsos
from wca.dist
left outer join wca.mpay on dist.rdid = mpay.eventid and 1 = mpay.optionid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dist.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'DIST'=mpay.sevent
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DIST'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and mpayopt2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.dmrgr.Acttime,
wca.dmrgr.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.dmrgr.rdid as eventid,
'DMRGR' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.mpay.ratioold,
wca.mpay.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.mpay.rationew)/wca.mpay.ratioold as newsos
from wca.dmrgr
left outer join wca.mpay on dmrgr.rdid = mpay.eventid and 1 = mpay.optionid and 'DMRGR'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dmrgr.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'dmrgr'=mpay.sevent
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DMRGR'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and mpayopt2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.divpy.Acttime,
wca.divpy.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.div_my.divid as eventid,
'DIV' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.divpy.ratioold,
wca.divpy.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.divpy.ratioold + wca.divpy.rationew)/wca.divpy.ratioold as newsos
from wca.div_my
left outer join wca.v10s_divpy as divpy on div_my.divid = divpy.divid and 1 = divpy.optionid
left outer join wca.v10s_divpy as divopt2 on div_my.divid = divopt2.divid and 2 = divopt2.optionid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'div'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and divopt2.divid is null and divpy.divtype='S'
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.divpy.ratioold <> ''
and wca.divpy.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.caprd.Acttime,
wca.caprd.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.caprd.caprdid as eventid,
'CAPRD' as eventtype,
wca.caprd.effectivedate as effectivedate,
'F' as lateflag,
wca.caprd.oldratio as ratioold,
wca.caprd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.caprd.newratio)/wca.caprd.oldratio as newsos
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rd on wca.caprd.rdid = wca.rd.rdid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'SD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.caprd.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3)and wca.caprd.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.caprd.oldratio <> ''
and wca.caprd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.sd.acttime,
wca.sd.actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.sd.rdid as eventid,
'SD' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.sd.oldratio as ratioold,
wca.sd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.sd.newratio)/wca.sd.oldratio as newsos
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'SD'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.sd.oldratio <> ''
and wca.sd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.rts.Acttime,
wca.rts.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.rts.rdid as eventid,
'RTS' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.rts.ratioold,
wca.rts.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.rts.ratioold + wca.rts.rationew)/wca.rts.ratioold as newsos
from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'rts'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.rts.ratioold <> ''
and wca.rts.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.ent.Acttime,
wca.ent.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.ent.rdid as eventid,
'ENT' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.ent.ratioold,
wca.ent.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.ent.ratioold + wca.ent.rationew)/wca.ent.ratioold as newsos
from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'ent'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.ent.ratioold <> ''
and wca.ent.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.consd.Acttime,
wca.consd.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.consd.rdid as eventid,
'CONSD' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.consd.oldratio as ratioold,
wca.consd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.consd.newratio)/wca.consd.oldratio as newsos
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'consd'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'ENT'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.consd.oldratio <> ''
and wca.consd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.bon.Acttime,
wca.bon.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.bon.rdid as eventid,
'BON' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.bon.ratioold,
wca.bon.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.bon.ratioold + wca.bon.rationew)/wca.bon.ratioold as newsos
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'bon'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.scmst.sharesoutstanding <> ''
and wca.bon.ratioold <> ''
and wca.bon.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.scmst.secid not in (select wca.shoch.secid from wca.shoch where wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3))
union
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.shoch.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.shoch.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),6)
and wca.shoch.newsos <> ''
union
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'T' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.shoch.effectivedate <= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.shoch.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.shoch.newsos <> ''
union
select
wca.scmst.Acttime,
wca.scmst.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
substring(wca.scmst.primaryexchgcd,1,2) as listing_country,
wca.scmst.secid as EventID,
'SCMST' as eventtype,
wca.scmst.sharesoutstandingdate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.scmst.sharesoutstanding as newsos
from wca.scmst
left outer join wca.shoch on wca.scmst.secid=wca.shoch.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scmst.isin in (select code from client.pfisin where accid = 378)
or wca.scmst.secid in (select secid from client.pfisin where accid = 378))
and wca.shoch.secid is null
and wca.scmst.acttime> (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.scmst.sharesoutstanding <> '';
