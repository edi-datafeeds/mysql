-- arc=y
-- arp=n:\temp\
-- fex=_619.txt
-- fty=y
-- hpx=EDI_WCA_Shares_Outstanding_619_
-- fdt=select ''
-- fpx=Phoenix
-- eof=select ''

-- # 1
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else wca.scmst.AnnounceDate end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else wca.scmst.Acttime end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
client.pfsedol.code as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as Effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as Newsos,
case when wca.sedol.cntrycd <> wca.exchg.cntrycd then psedol.sedol else '' end as PrimarySedol,
'' as ShochNotes
FROM client.pfsedol
left outer join wca.sedol on client.pfsedol.code = wca.sedol.sedol
left outer join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.scexh ON wca.sedol.SecID = wca.scexh.SecID and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol as psedol on wca.sedol.secid = psedol.secid and wca.exchg.cntrycd = psedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where client.pfsedol.accid = 265
and client.pfsedol.actflag <> 'D'
and wca.scmst.secid is not null
and (wca.scmst.actflag<>'D' or wca.scmst.secid is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and (wca.issur.actflag<>'D' or wca.issur.actflag is null)
and (shh.actflag<>'D' or shh.actflag is null)
and shh.effectivedate=(select max(feeddate) from wca.tbl_opslog);
