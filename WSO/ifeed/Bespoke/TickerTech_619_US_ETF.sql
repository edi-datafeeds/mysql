-- arc=y
-- arp=n:\bespoke\TickerTech\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.619
-- fpx=
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Shares_Outstanding_619_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
'I' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
wca.shh.ShochNotes
FROM wca.scmst
left outer join wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where
wca.scmst.sectycd='ETF'
and wca.exchg.cntrycd='US'
and wca.scmst.actflag<>'D'
and ifnull(wca.scexh.actflag,'')<>'D'
and ifnull(wca.issur.actflag,'')<>'D'
and ifnull(wca.scexh.liststatus,'L')<>'D'
and wca.scmst.statusflag<>'I'
and wca.scexh.exchgcd = wca.scmst.primaryexchgcd
and ifnull(shh.actflag,'')<>'D'
and (ifnull(shh.newsos,'')<>'' or wca.scmst.sharesoutstanding<>'')
and ((shh.shochid is null and scmst.sharesoutstanding<>'') or
shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1));
