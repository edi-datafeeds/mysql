-- arc=y
-- arp=n:\No_Cull_Feeds\WAM\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.619
-- fpx=
-- fsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Shares_Outstanding_619_
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

select
wca.scmst.secid,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     else '1'
     end as BbgExhSeqnum,
case when wca.shoch.shochid is not null then wca.shoch.AnnounceDate else wca.scmst.Announcedate end as created,
case when wca.shoch.shochid is not null then date_format(wca.shoch.Acttime,'%Y/%m/%d') 
                                        else date_format(wca.scmst.Acttime,'%Y/%m/%d') 
                                        end as changed,
case when wca.shoch.Actflag='D' or wca.scmst.actflag='D' then 'D' 
     when wca.shoch.shochid is not null then wca.shoch.Actflag
     else wca.scmst.Actflag end as actflag,
wca.issur.CntryofIncorp as cntryofincorp,
wca.issur.IssuerName as issuerName, 
wca.scmst.SecurityDesc as securitydesc,
wca.scmst.Parvalue as parvalue,
wca.scmst.CurenCD as pvcurrency,
wca.scmst.ISIN as isin,
wca.scmst.USCode as uscode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as statusflag,
wca.scmst.PrimaryExchgCD as primaryexchgcd,
wca.scmst.SectyCD as sectycd,
wca.exchg.exchgname as exchange,
wca.scexh.exchgcd as exchgcd,
wca.exchg.mic as mic,
wca.exchg.cntrycd as excountry,
ifnull(bbg.curencd,'') as BbgTradingCurrency,
ifnull(bbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(bbg.bbgcomptk,'') as BbgCompositeTicker,
case when ifnull(bbg.bbgexhid,'') <> ''
     then bbg.bbgexhid
     else ''
     end as BbgGlobalID,
case when ifnull(bbg.bbgexhtk,'') <> ''
     then bbg.bbgexhtk
     else ''
     end as BbgExchangeTicker,
wca.scexh.LocalCode as localcode,
case when wca.scexh.liststatus='L' or wca.scexh.liststatus=''
     then 'Listed'
     when wca.scexh.liststatus='N'
     then 'New Listing'
     when wca.scexh.liststatus='D'
     then 'De-Listed'
     when wca.scexh.liststatus='R'
     then 'Restored'
     when wca.scexh.liststatus='S'
     then 'Suspended'
     end as listingstatus,
wca.scexh.listdate as listdate,
case when wca.shoch.shochid is not null then wca.shoch.shochid else 0 end as eventid,
case when wca.shoch.shochid is not null then wca.shoch.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shoch.OldSos as oldsos,
case when shoch.shochid is not null then wca.shoch.NewSos else wca.scmst.sharesoutstanding end as newsos,
wca.shoch.ShochNotes as shochnotes
from wca.scmst
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
INNER JOIN wca.SectyMap on wca.scmst.SectyCD = wca.SectyMap.SectyCD and (wca.SectyMap.Typegroup = 'Equity' or wca.SectyMap.Typegroup = 'Depository Receipt')
left outer JOIN wca.shoch ON wca.scmst.SecID = wca.shoch.secid
INNER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.secid and 'D' <> wca.scexh.actflag
INNER JOIN wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd 
                     and (wca.exchg.cntrycd='AU' or wca.exchg.cntrycd='CA' or wca.exchg.cntrycd='GB' or wca.exchg.cntrycd='NL')
left outer join wca2.bbgseq as bbg on wca.scexh.secid=bbg.secid and wca.scexh.exchgcd = bbg.exchgcd
left outer join wca2.bbcseq as bbcseq on bbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on bbg.bbeid=bbeseq.bbeid
where
(wca.shoch.shochid is not null
and wca.shoch.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(wca.shoch.effectivedate,'2000-01-01'))
and wca.shoch.NewSos<>wca.shoch.OldSos)
OR
(wca.shoch.shochid is not null
and (wca.shoch.eventtype = 'CONSD' or wca.shoch.eventtype = 'SD' or wca.shoch.eventtype = 'CAPRD' 
         or wca.shoch.eventtype = 'ISCHG' or wca.shoch.eventtype = 'ARR')
and wca.shoch.acttime > (select date_sub(max(feeddate), interval 31 day) from wca.tbl_Opslog)
and wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_Opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(wca.shoch.effectivedate,'2000-01-01'))
and wca.shoch.NewSos<>wca.shoch.OldSos)
OR
(wca.shoch.shochid is null and 
wca.scmst.sharesoutstanding <> ''
and wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_Opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(wca.scmst.sharesoutstandingdate,'2000-01-01')))