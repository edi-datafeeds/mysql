-- arc=y
-- arp=n:\upload\acc\117\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.619
-- fpx=
-- fsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Shares_Outstanding_619_
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else wca.scmst.AnnounceDate end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else wca.scmst.Acttime end as Changed,
case when shh.Actflag='D' or wca.scmst.actflag='D' then 'D' else '' end as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as Effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as Newsos,
'' as ShochNotes,
wca.scmst.UnlistedClassesSharesOutstanding,
wca.scmst.UnlistedClassesSharesOutstandingDate
from wca.scmst
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
INNER JOIN wca.sectygrp ON wca.scmst.sectycd = wca.sectygrp.sectycd AND 3>wca.sectygrp.secgrpid
inner join wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where
(shh.shochid is not null
and shh.acttime >= (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(shh.effectivedate,'2000-01-01'))
and shh.NewSos<>shh.OldSos)
OR
(shh.shochid is not null
and (shh.eventtype = 'CONSD' or shh.eventtype = 'SD' or shh.eventtype = 'CAPRD' 
         or shh.eventtype = 'ISCHG' or shh.eventtype = 'ARR')
and shh.acttime > (select date_sub(max(acttime), interval 31 day) from wca.tbl_opslog)
and wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(shh.effectivedate,'2000-01-01'))
and shh.NewSos<>shh.OldSos)
OR
(shh.shochid is null and 
wca.scmst.sharesoutstanding <> ''
and wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca.scexh.liststatus<>'D' or ifnull(wca.scexh.delistdate, wca.scexh.acttime)>ifnull(wca.scmst.sharesoutstandingdate,'2000-01-01')))