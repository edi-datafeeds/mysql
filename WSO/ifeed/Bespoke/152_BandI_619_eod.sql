-- arc=y
-- arp=n:\upload\acc\152\619\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_619
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Shares_Outstanding_619_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else wca.scmst.AnnounceDate end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else wca.scmst.Acttime end as Changed,
case when shh.Actflag='D' or wca.scmst.actflag='D' then 'D' else '' end as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
client.pfisin.code as Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as Effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as Newsos,
'' as ShochNotes
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where client.pfisin.accid = 152
and client.pfisin.actflag = 'U'
and (shh.shochid is not null or wca.scmst.sharesoutstanding <> '')
and (shh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
    or (wca.scmst.announcedate >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
    and wca.scmst.sharesoutstandingdate is not null and shh.shochid is null))
and (shh.shochid is null or scmst.secid is null or
shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1))
UNION
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else wca.scmst.AnnounceDate end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else wca.scmst.Acttime end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
client.pfisin.code as Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as Effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as Newsos,
'' as ShochNotes
FROM client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where client.pfisin.accid = 152
and client.pfisin.actflag = 'I'
and (wca.scmst.actflag<>'D' or wca.scmst.secid is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and (wca.issur.actflag<>'D' or wca.issur.actflag is null)
and (shh.actflag<>'D' or shh.actflag is null)
and (shh.shochid is null or scmst.secid is null or
shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1))
UNION
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else wca.scmst.AnnounceDate end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else wca.scmst.Acttime end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
client.pfisin.code as Isin,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as Effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as Newsos,
'' as ShochNotes
FROM client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where client.pfisin.accid = 152
and client.pfisin.actflag <> 'D'
and wca.scmst.secid is not null
and (wca.scmst.actflag<>'D' or wca.scmst.secid is null)
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null)
and (wca.issur.actflag<>'D' or wca.issur.actflag is null)
and (shh.actflag<>'D' or shh.actflag is null)
and shh.effectivedate=(select max(feeddate) from wca.tbl_opslog);
