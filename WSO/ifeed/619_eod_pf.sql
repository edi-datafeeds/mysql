-- arc=y
-- arp=n:\temp\
-- fex=.619
-- fty=y
-- hpx=EDI_WCA_Shares_Outstanding_619_

-- # 1
select distinct
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
client.pfisin.code as ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
'' as shochNotes
FROM client.pfisin
inner JOIN wca.scmst on client.pfisin.code = wca.scmst.isin
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where
(wca.shh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
    or (wca.shh.secid is null and wca.scmst.announcedate >= (select max(feeddate) from wca.tbl_opslog where seq = 3)))
and client.pfisin.accid = @accid and client.pfisin.actflag <> 'D'
and wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.issur.actflag<>'D'
and (shh.actflag<>'D' or shh.actflag is null)
and (shh.newsos<>'' or wca.scmst.sharesoutstanding<>'');
