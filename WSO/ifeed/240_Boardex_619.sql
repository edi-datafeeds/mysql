-- arp=n:\upload\acc\240\feed\
-- fex=.619
-- fty=y
-- hpx=EDI_WCA_Shares_Outstanding_619_

-- # 1
select
wca.scmst.SecID,
case when shh.shochid is not null then shh.AnnounceDate 
     else null
     end as Created,
case when shh.shochid is not null then shh.Acttime
     else null
     end as Changed,
case when shh.Actflag='D' or wca.scmst.actflag='D' then 'D'
     else ''
     end as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A' then 'A'
     when wca.scmst.Statusflag = '' then 'A' 
     when wca.scmst.Statusflag is null then 'A' 
     ELSE 'I' 
     END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus = '' then 'L'
     when wca.scexh.ListStatus = 'N' then 'L'
     else wca.scexh.ListStatus
     end as ListingStatus,
wca.scexh.ListDate,
shh.shochid as EventID,
shh.EffectiveDate,
shh.OldSos,
case when shh.shochid is not null then shh.NewSos 
     else wca.scmst.sharesoutstanding 
     end as NewSos,
shh.ShochNotes
FROM wca.scmst
inner JOIN wca.sectygrp on wca.scmst.SectyCD = wca.sectygrp.SectyCD and 3>secgrpid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>wca.scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.shoch as shh on wca.scmst.secid = shh.secid and 'D'<>shh.actflag
where
wca.sedol.sedol in (select code from client.pfsedol where accid = @accid and actflag<>'D')
and shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1)
and ((shh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
or (wca.sedol.sedol in (select code from client.pfsedol where accid = @accid and actflag='I')))
