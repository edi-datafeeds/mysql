--filepath=o:\datafeed\wso\670\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.670
--suffix=_ALERT
--fileheadertext=EDI_WSO_CALC_ALERT_670
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=c:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1 

select
wca.caprd.Acttime,
wca.caprd.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.caprd.caprdid as eventid,
'CAPRD' as eventtype,
wca.caprd.effectivedate as effectivedate,
'F' as lateflag,
wca.caprd.oldratio as ratioold,
wca.caprd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.caprd.newratio)/wca.caprd.oldratio as newsos
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rd on wca.caprd.rdid = wca.rd.rdid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'SD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.caprd.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3)and wca.caprd.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.caprd.oldratio <> ''
and wca.caprd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.sd.acttime,
wca.sd.actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.sd.rdid as eventid,
'SD' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.sd.oldratio as ratioold,
wca.sd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.sd.newratio)/wca.sd.oldratio as newsos
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'SD'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.sd.oldratio <> ''
and wca.sd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.rts.Acttime,
wca.rts.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.rts.rdid as eventid,
'RTS' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.rts.ratioold,
wca.rts.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.rts.ratioold + wca.rts.rationew)/wca.rts.ratioold as newsos
from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'rts'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.rts.ratioold <> ''
and wca.rts.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.ent.Acttime,
wca.ent.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.ent.rdid as eventid,
'ENT' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.ent.ratioold,
wca.ent.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.ent.ratioold + wca.ent.rationew)/wca.ent.ratioold as newsos
from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'ent'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.ent.ratioold <> ''
and wca.ent.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.consd.Acttime,
wca.consd.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.consd.rdid as eventid,
'CONSD' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.consd.oldratio as ratioold,
wca.consd.newratio as rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.consd.newratio)/wca.consd.oldratio as newsos
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'consd'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'ENT'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.consd.oldratio <> ''
and wca.consd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.bon.Acttime,
wca.bon.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.bon.rdid as eventid,
'BON' as eventtype,
wca.exdt.exdate as effectivedate,
'F' as lateflag,
wca.bon.ratioold,
wca.bon.rationew,
wca.scmst.sharesoutstanding as oldsos,
wca.scmst.sharesoutstanding *(wca.bon.ratioold + wca.bon.rationew)/wca.bon.ratioold as newsos
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'bon'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.scmst.sharesoutstanding <> ''
and wca.bon.ratioold <> ''
and wca.bon.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
union
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.newsos,
wca.shoch.oldsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.shoch.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.shoch.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),5)
and wca.shoch.newsos <> '';
