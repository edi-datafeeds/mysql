-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.619
-- filenamesuffix=_619
-- headerprefix=EDI_WCA_Shares_Outstanding_619_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # BIG 1
select
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
case when wca.shh.shochid is not null then wca.shh.EffectiveDate else wca.scmst.sharesoutstandingdate end as effectivedate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
'' as shochNotes
FROM wca.scmst
inner JOIN wca.sectygrp on wca.scmst.SectyCD = wca.sectygrp.SectyCD and 3>secgrpid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>wca.shh.actflag
where
wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.issur.actflag<>'D'
and (shh.actflag<>'D' or shh.actflag is null)
and (shh.newsos<>'' or wca.scmst.sharesoutstanding<>'')
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'AU'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BR'
or wca.exchg.cntrycd = 'CA'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CL'
or wca.exchg.cntrycd = 'CO'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EG'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'ID'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IL'
or wca.exchg.cntrycd = 'IN'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'JP'
or wca.exchg.cntrycd = 'KR'
or wca.exchg.cntrycd = 'MX'
or wca.exchg.cntrycd = 'MY'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'NZ'
or wca.exchg.cntrycd = 'PE'
or wca.exchg.cntrycd = 'PH'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RU'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SG'
or wca.exchg.cntrycd = 'TH'
or wca.exchg.cntrycd = 'TR'
or wca.exchg.cntrycd = 'US'
or wca.exchg.cntrycd = 'ZA')

