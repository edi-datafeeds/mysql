-- arc=y
-- arp=n:\temp\
-- dfn=n
-- hpx=select ''
-- fex=.670
-- eof=select ''
-- hdt=SELECT ''


-- # BIG 2
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
case when wca.bbc.cntrycd is null then substring(wca.scmst.primaryexchgcd,1,2) else wca.bbc.cntrycd end as listing_country,
wca.bbc.curencd as trading_currency,
wca.bbc.bbgcompid as composite_global_id,
wca.bbc.bbgcomptk as bloomberg_composite_ticker,
wca.bbe.exchgcd as listing_exchange,
wca.bbe.bbgexhid as bloomberg_global_id,
wca.bbe.bbgexhtk as bloomberg_exchange_ticker,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.bbc on wca.scmst.secid = wca.bbc.secid and substring(wca.scmst.primaryexchgcd,1,2)=wca.bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scmst.secid = wca.bbe.secid and wca.bbc.cntrycd = substring(wca.bbe.exchgcd,1,2) and  wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
where
shoch.shochid>500000 and shoch.shochid<=1000000
and wca.shoch.actflag<>'D'
and wca.shoch.newsos <> '';


