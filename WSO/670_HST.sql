-- arc=y
-- arp=n:\temp\
-- hpx=EDI_WSO_CALC_ALERT_670_
-- fex=.670


-- # BIG 1
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
'' as listing_country,
'' as trading_currency,
'' as composite_global_id,
'' as bloomberg_composite_ticker,
'' as listing_exchange,
'' as bloomberg_global_id,
'' as bloomberg_exchange_ticker,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
shoch.shochid<=500000
and wca.shoch.actflag<>'D'
and wca.shoch.newsos <> '';

-- # BIG 2
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
'' as listing_country,
'' as trading_currency,
'' as composite_global_id,
'' as bloomberg_composite_ticker,
'' as listing_exchange,
'' as bloomberg_global_id,
'' as bloomberg_exchange_ticker,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
shoch.shochid>500000 and shoch.shochid<=1000000
and wca.shoch.actflag<>'D'
and wca.shoch.newsos <> '';

-- # BIG 3
select
wca.shoch.Acttime,
wca.shoch.Actflag,
wca.scmst.secid,
wca.issur.issuername,
wca.scmst.isin,
wca.scmst.securitydesc,
wca.scmst.sectycd,
'' as listing_country,
'' as trading_currency,
'' as composite_global_id,
'' as bloomberg_composite_ticker,
'' as listing_exchange,
'' as bloomberg_global_id,
'' as bloomberg_exchange_ticker,
wca.shoch.shochid as EventID,
'SHOCH' as eventtype,
wca.shoch.effectivedate as effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
wca.shoch.oldsos,
wca.shoch.newsos
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
shoch.shochid>1000000
and wca.shoch.actflag<>'D'
and wca.shoch.newsos <> '';
