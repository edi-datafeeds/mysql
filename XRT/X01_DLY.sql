--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(xrates.rates.Feeddate) from xrates.rates
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n




-- arc=y
-- arp=N:\xrates\Daily\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(Feeddate)from xrates.rates where base = @base), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(Feeddate)from xrates.rates where base = @base), '%Y%m%d')
-- hpx=
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE




-- # 1
set @base = 'USD'
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate = (select max(xrates.rates.Feeddate) from xrates.rates where base = @base);
