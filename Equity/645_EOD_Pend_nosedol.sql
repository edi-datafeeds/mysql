--filepath=O:\Datafeed\Equity\645\645PEND\nosedol\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\645\nosedol\pend\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp
FROM wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE wca.issur.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.scmst.sectycd<>'BND'
and wca.sectygrp.secgrpid<3


--# 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid=wca.issur.issid
WHERE wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3
and wca.scmst.sectycd<>'BND'


--# 3
SELECT distinct
'' as Tablename,
'' as Actflag,
'' as Acttime,
'' as Announcedate,
'' as SecID,
'' as CntryCD,
'' as Sedol,
'' as Defunct,
'' as RcntryCD,
'' as SedolId


--# 4
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
case when ifnull(listdate,'2018-01-01')>now() then 'P'
     when ifnull(firsttradingdate,'2018-01-01')>now() then 'P'
     when ifnull(ipostatus,'HISTORY')<>'HISTORY' and listdate is null and firsttradingdate is null then 'P'
     else wca.scexh.ListStatus
     end as ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
case when ifnull(listdate,'2018-01-01')>now() then 'P'
     when ifnull(firsttradingdate,'2018-01-01')>now() then 'P'
     when ifnull(ipostatus,'HISTORY')<>'HISTORY' and listdate is null and firsttradingdate is null then 'P'
     else wca.scexh.ListStatus
     end as ListStatus,
wca.scexh.LocalCode
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid and 'D'<>wca.ipo.actflag
WHERE 
(wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or ifnull(wca.scexh.listdate,'2001/01/01') = (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.ipo.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
and wca.scmst.sectycd <> 'BND'
and wca.sectygrp.secgrpid<3;


--# 5
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
WHERE wca.exchg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
