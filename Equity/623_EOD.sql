--filepath=o:\Datafeed\Equity\623\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.623
--suffix=
--fileheadertext=EDI_STATIC_623_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\623\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
wca.scexh.ScexhID,
wca.scexh.Actflag,
wca.scexh.Acttime as Changed,
wca.scmst.IssID,
wca.scmst.SecID,
wca.issur.Issuername,
wca.issur.CntryofIncorp,
wca.issur.IndusID,
wca.issur.Financialyearend,
case when wca.scmst.Statusflag <> '' then wca.scmst.Statusflag else 'A' end as SecStatus,
wca.scmst.PrimaryExchgCD,
wca.scmst.Securitydesc,
wca.scmst.CurenCD as ParValueCurrency,
wca.scmst.Parvalue,
wca.scmst.SectyCD,
wca.scmst.Uscode,
wca.scmst.Isin,
case when wca.sedol.defunct <> 'T' then wca.sedol.sedol else '' end as Sedol,
case when wca.sedol.defunct <> 'T' then wca.sedol.rcntrycd else '' end as CountryOfRegister,
wca.scexh.exchgcd,
wca.scexh.localcode,
case when wca.scexh.liststatus is not null then wca.scexh.liststatus else 'L' end as ListStatus
FROM wca.scmst
INNER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.cntry ON wca.issur.CntryofIncorp = wca.cntry.cntryCD
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
LEFT OUTER JOIN wca.sedol ON wca.scmst.SecID = wca.sedol.SecID
                      AND wca.exchg.cntryCD = wca.sedol.CntryCD
where 
(wca.scmst.sectycd = 'EQS'
or wca.scmst.sectycd = 'PRF'
or wca.scmst.sectycd = 'DR')
and (wca.scmst.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.sedol.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.scexh.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.issur.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1))
