--filepath=o:\Datafeed\Equity\653\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.653
--suffix=
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\653\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
select
wca.exchg.cntrycd,
wca.exchg.mic,
wca.scmst.secid,
wca.scmst.isin,
wca.scexh.Localcode,
wca.issur.issuername, 
wca.scmst.issid,
wca.scmst.securitydesc,
wca.scexh.exchgcd,
wca.scmst.sectycd,
wca.scmst.primaryexchgcd,
case when wca.scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when wca.scexh.liststatus is null then 'U' when wca.scexh.liststatus='N' or wca.scexh.liststatus='' then 'L' else wca.scexh.liststatus end as wcaliststatus,
wca.scmst.acttime as changed,
wca.scmst.actflag as ScmstRecordStatus,
wca.scexh.actflag as ScexhRecordStatus
from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
(wca.scmst.acttime >= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.issur.acttime >= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.scexh.acttime >= (select max(acttime) from wca.tbl_opslog where seq = 1))
