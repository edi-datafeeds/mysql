--filepath=o:\Datafeed\Equity\664\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.664
--suffix=
--fileheadertext=EDI_DR_664_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\664\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
SELECT
upper('DPRCP') as Tablename,
wca.dprcp.Actflag,
wca.dprcp.Acttime,
wca.dprcp.Announcedate,
wca.dprcp.SecID,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio,
wca.dprcp.SpnFlag,
wca.dprcp.DRtype,
wca.dprcp.DepBank,
wca.dprcp.LevDesc,
wca.dprcp.OtherDepBank
from wca.dprcp
WHERE wca.dprcp.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 1)


--# 2
SELECT
upper('DRCHG') as Tablename,
wca.drchg.Actflag,
wca.drchg.Acttime,
wca.drchg.Announcedate,
wca.drchg.DrchgID,
wca.drchg.SecID,
wca.drchg.EffectiveDate,
wca.drchg.OldDRRatio,
wca.drchg.NewDRRatio,
wca.drchg.OldUNRatio,
wca.drchg.NewUNRatio,
wca.drchg.OldUNSecID,
wca.drchg.NewUNSecID,
wca.drchg.Eventtype as RelatedEvent,
wca.drchg.OldDepbank,
wca.drchg.NewDepbank,
wca.drchg.OldDRtype,
wca.drchg.NewDRtype,
wca.drchg.OldLevel,
wca.drchg.Newlevel,
wca.drchg.DrchgNotes
from wca.drchg
WHERE wca.drchg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 1)

