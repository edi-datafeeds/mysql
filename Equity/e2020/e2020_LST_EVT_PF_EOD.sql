-- arc=y
-- arp=n:\temp\
-- fsx=select '_LST_EVT'
-- hsx=select '_LST_EVT'
-- hdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fpx=e2020_
-- hpx=e2020_
-- dfn=l

-- # 1
select * from wca2.e2020_LST_EVT
where
(EventChangeDT > (select max(feeddate) from wca.tbl_opslog where seq=3)
and (secid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U')
  or secid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U')
  or secid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U')
  or secid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='U')
  or secid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))
OR
    (secid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I')
  or secid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I')
  or secid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I')
  or secid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='I')
  or secid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I'));
