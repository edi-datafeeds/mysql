-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_LST_REF_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_LST_REF_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fpx=e2020_
-- hpx=e2020_
-- dfn=l

-- # 1

select ltab.*,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.FISN,
wca.scmst.StructCD,
wca.scmst.Voting,
wca.scmst.VotePerSec,
wca.scmst.SharesOutstanding,
wca.scmst.SharesoutstandingDate as SharesOutstandingDT
from wca2.e2020_LST as ltab
inner join wca.issur on ltab.issid=wca.issur.issid
inner join wca.scmst on ltab.secid=wca.scmst.secid
where
ltab.ListChangeDT >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog);
