-- arc=y
-- arp=n:\temp\
-- fsx=select '_LST_REF'
-- hsx=select '_LST_REF'
-- hdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fpx=e2020_
-- hpx=e2020_
-- dfn=l

-- # 1

select ltab.*,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.FISN,
wca.scmst.StructCD,
wca.scmst.Voting,
wca.scmst.VotePerSec,
wca.scmst.SharesOutstanding,
wca.scmst.SharesoutstandingDate as SharesOutstandingDT
from wca2.e2020_LST as ltab
inner join wca.issur on ltab.issid=wca.issur.issid
inner join wca.scmst on ltab.secid=wca.scmst.secid
where
ltab.ListChangeDT >(select max(feeddate) from wca.tbl_opslog where seq=3);

