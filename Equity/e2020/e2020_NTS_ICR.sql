-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_NTS_',(select seq_new-1 from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_NTS_',(select seq_new-1 from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fpx=e2020_
-- hpx=e2020_
-- dfn=l

-- # 1

select * from wca2.e2020_NTS
where
EventChangeDT >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)