use wca2;
drop table if exists sedolccy;
CREATE TABLE `sedolccy` (
  `Acttime` datetime DEFAULT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `CntryCD` char(2) DEFAULT NULL,
  `Sedol` char(7) DEFAULT NULL,
  `Defunct` char(1) DEFAULT NULL,
  `RcntryCD` char(2) DEFAULT NULL,
  `SedolID` int(10) NOT NULL DEFAULT '0',
  `CurenCD` char(3) DEFAULT '',
  `Opol` char(4) DEFAULT '',
  `AnnounceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SedolId`),
  KEY `ix_acttime_SEDOL` (`Acttime`),
  KEY `IX_SEDOL` (`Sedol`),
  KEY `IX_SecID` (`SecID`,`CntryCD`,`RcntryCD`,`CurenCD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
