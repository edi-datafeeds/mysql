-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_LST_EVT_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_LST_EVT_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fpx=e2020_
-- hpx=e2020_
-- dfn=l

-- # 1

select * from wca2.e2020_LST_EVT
where
(EventChangeDT >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (secid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U')
  or secid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U')
  or secid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U')
  or secid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='U')
  or secid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))
OR
    (secid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I')
  or secid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I')
  or secid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I')
  or secid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='I')
  or secid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I'));
