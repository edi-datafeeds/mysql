-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_NTS_',(select seq_new-1 from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_NTS_',(select seq_new-1 from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime_new) from wca.tbl_opslog), '%Y%m%d')
-- hpx=e2020_
-- dfn=l

-- # 1

select
EventCD,
EventID,
EventActflag,
EventChangeDT,
NotesType,
NotesText
from wca2.e2020_NTS
where
(EventChangeDT >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='U'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pffigi where accid=@accid and client.pffigi.actflag='U'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='U')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))))
OR
((((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='I'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pffigi where accid=@accid and client.pffigi.actflag='I'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='I')))
OR
((eventcd in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I'))
or (eventcd not in ('ANN','BKRP','CLSAC','ISCHG','LIQ') and PfLinkID in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I')))));
