--filepath=o:\Datafeed\Equity\634_investt\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.634
--suffix=
--fileheadertext=EDI_STATIC_634_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\634_investt\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
wca.scexh.ScexhID,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
wca.scexh.Actflag,
case when wca.scmst.Actflag='D' then 'D' WHEN wca.scexh.AnnounceDate>now()-5 THEN 'I' ELSE 'U' END as Actflag,
wca.scmst.IssID,
wca.scmst.SecID,
wca.issur.CntryofIncorp,
wca.issur.Issuername,
wca.scmst.Securitydesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.Isin,
wca.scmst.Uscode,
case when wca.scmst.Statusflag is not null and wca.scmst.Statusflag <> '' then wca.scmst.Statusflag else 'A' end as SecStatus,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
substring(wca.scexh.Exchgcd,1,2) as ExCountry,
case when wca.sedol.actflag <> 'D' then wca.sedol.Sedol
else '' end as Sedol,
wca.scexh.Exchgcd,
wca.exchg.Mic,
wca.scexh.Localcode,
CASE WHEN (wca.scexh.ListStatus IS NULL) or (wca.scexh.ListStatus='') THEN 'L' ELSE wca.scexh.ListStatus END as ListStatus,
wca.scexh.ListDate,
wca.scexh.Lot
FROM wca.scmst
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.cntry ON wca.issur.CntryofIncorp = wca.cntry.cntryCD
LEFT OUTER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
LEFT OUTER JOIN wca.sedol ON wca.scmst.SecID = wca.sedol.SecID
                 AND wca.exchg.cntryCD = wca.sedol.CntryCD
LEFT OUTER JOIN wca.sectymap ON wca.scmst.SectyCD = wca.sectymap.SectyCD
where wca.issur.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.sectymap.typegroup='Equity'
and (wca.exchg.CntryCD = 'DE' or wca.exchg.CntryCD = 'DK' or wca.exchg.CntryCD = 'FI' or wca.exchg.CntryCD = 'NL' or wca.exchg.CntryCD = 'SE' or wca.exchg.CntryCD = 'NO')
