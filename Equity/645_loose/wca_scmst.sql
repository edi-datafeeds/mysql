--filepath=o:\Datafeed\Equity\645_loose\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_EQUITY_645_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\645_loose\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE
wca.sectygrp.secgrpid<3
and wca.scmst.sectycd<>'BND'
and wca.scmst.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3)
