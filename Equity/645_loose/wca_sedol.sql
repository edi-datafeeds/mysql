--filepath=o:\Datafeed\Equity\645_loose\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_EQUITY_645_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\645_loose\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.SedolId
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE
wca.scmst.sectycd <> 'BND'
and wca.sectygrp.secgrpid<3
and wca.sedol.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3)


