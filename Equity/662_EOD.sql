--filepath=o:\Datafeed\Equity\662\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.662
--suffix=
--fileheadertext=EDI_662_NOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\662\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n


--# 1
select
'sEvent' as dummy1,
'EventID' as dummy2,
'Notes' as dummy3

--# 2
SELECT
'CALL' as sEvent,
CallID,
CallNotes
FROM wca.call_my
WHERE
wca.call_my.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 3
SELECT
'LIQ' as sEvent,
LiqID,
LiquidationTerms
FROM wca.liq
WHERE
wca.liq.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 4
SELECT
'CTX' as sEvent,
CtxID,
CtxNotes
FROM wca.ctx
WHERE
wca.ctx.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)



--# 6
SELECT
'CONV' as sEvent,
ConvID,
ConvNotes
FROM wca.conv
WHERE
wca.conv.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 7
SELECT
'REDEM' as sEvent,
RedemID,
RedemNotes
FROM wca.redem
WHERE
wca.redem.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 8
SELECT
'SECRC' as sEvent,
SecrcID,
SecrcNotes
FROM wca.secrc
WHERE
wca.secrc.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)



--# 10
SELECT
'SDCHG' as sEvent,
SdchgID,
SdchgNotes
FROM wca.sdchg
WHERE
wca.sdchg.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 11
SELECT
'BB' as sEvent,
BBID,
BBNotes
FROM wca.bb
WHERE
wca.bb.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 12
SELECT
'CAPRD' as sEvent,
CaprdID,
CapRDNotes
FROM wca.caprd
WHERE
wca.caprd.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 13
SELECT
'TKOVR' as sEvent,
TkovrID,
TkovrNotes
FROM wca.tkovr
WHERE
wca.tkovr.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 14
SELECT
'ARR' as sEvent,
RdID,
ArrNotes
FROM wca.arr
WHERE
wca.arr.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 15
SELECT
'BON' as sEvent,
RdID,
BonNotes
FROM wca.bon
WHERE
wca.bon.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 17
SELECT
'CONSD' as sEvent,
RdID,
ConsdNotes
FROM wca.consd
WHERE
wca.consd.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 18
SELECT
'DMRGR' as sEvent,
RdID,
DmrgrNotes
FROM wca.dmrgr
WHERE
wca.dmrgr.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 19
SELECT
'DIST' as sEvent,
RdID,
DistNotes
FROM wca.dist
WHERE
wca.dist.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 20
SELECT
'DVST' as sEvent,
RdID,
DvstNotes
FROM wca.dvst
WHERE
wca.dvst.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 21
SELECT
'ENT' as sEvent,
RdID,
EntNotes
FROM wca.ent
WHERE
wca.ent.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 22
SELECT
'MRGR' as sEvent,
RdID,
MrgrTerms
FROM wca.mrgr
WHERE
wca.mrgr.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 23
SELECT
'PRF' as sEvent,
RdID,
PrfNotes
FROM wca.prf
WHERE
wca.prf.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 24
SELECT
'PO' as sEvent,
RdID,
PoNotes
FROM wca.po
WHERE
wca.po.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 25
SELECT
'RTS' as sEvent,
RdID,
RtsNotes
FROM wca.rts
WHERE
wca.rts.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 26
SELECT
'SCSWP' as sEvent,
RdID,
ScswpNotes
FROM wca.scswp
WHERE
wca.scswp.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 27
SELECT
'SD' as sEvent,
RdID,
SDNotes
FROM wca.sd
WHERE
wca.sd.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 28
SELECT
'BKRP' as sEvent,
BkrpID,
BkrpNotes
FROM wca.bkrp 
WHERE
wca.bkrp.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 31
SELECT
'ISCHG' as sEvent,
IschgID,
IschgNotes
FROM wca.ischg
WHERE
wca.ischg.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 32
SELECT
'LAWST' as sEvent,
LawstID,
LawstNotes
FROM wca.lawst
WHERE
wca.lawst.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 33
SELECT
'SCCHG' as sEvent,
ScchgID,
ScchgNotes
FROM wca.scchg
WHERE
wca.scchg.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 35
SELECT
'LSTAT' as sEvent,
LstatID,
Reason
FROM wca.lstat
WHERE
wca.lstat.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 38
SELECT
'ANN' as sEvent,
AnnID,
AnnNotes
FROM wca.ann
WHERE
wca.ann.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 39
SELECT
'PVRD' as sEvent,
PvrdID,
PvrdNotes
FROM wca.pvrd
WHERE
wca.pvrd.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 40
SELECT
'CURRD' as sEvent,
CurrdID,
CurrdNotes
FROM wca.currd
WHERE
wca.currd.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 41
SELECT
'RCAP' as sEvent,
RcapID,
RcapNotes
FROM wca.rcap
WHERE
wca.rcap.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 42
SELECT
'DIVPY' as sEvent,
RdID,
DivNotes
FROM wca.div_my
WHERE
wca.div_my.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 46
SELECT
'BR' as sEvent,
RdID,
BRNotes
FROM wca.br
WHERE
wca.br.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)
