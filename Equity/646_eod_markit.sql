--filepath=o:\Datafeed\Equity\646_Markit\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.646
--suffix=
--fileheadertext=EDI_REORG_646_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\646_Markit\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n


--# 0
select
'Levent' as f1,
'EventID' as f2,
'SecID' as f3,
'IssID' as f4,
'Created' as f5,
'Changed' as f6,
'Actflag' as f7,
'PrimaryExchgCD' as f8,
'Isin' as f9,
'PrimaryDate' as f10,
'Enddate1' as f11,
'SecondaryDate' as f12,
'Enddate2' as f13,
'TertiaryDate' as f14,
'Enddate3' as f15,
'RatioNew' as f16,
'RatioOld' as f17,
'Fractions' as f18,
'ResSecID' as f19,
'ResIsin' as f20,
'Addfld1' as f21,
'AddInfo1' as f22,
'Addfld2' as f23,
'AddInfo2' as f24,
'Addfld3' as f25,
'AddInfo3' as f26

--# 1
select distinct
'Bankruptcy' as Levent,
wca.bkrp.BkrpID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,
wca.bkrp.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.Isin,
'FilingDate' as PrimaryDate,
wca.bkrp.FilingDate as Enddate1,
'NotificationDate' as SecondaryDate,
wca.bkrp.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.bkrp
INNER JOIN wca.scmst ON wca.scmst.IssID = wca.bkrp.IssID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.bkrp.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1)
and (select max(acttime) from wca.tbl_opslog where seq = 3) 
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 2
select distinct
'Stock Dividend' as Levent,
wca.div_my.DivID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.div_my.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.div_my.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.div_my.Acttime) THEN wca.pexdt.Acttime ELSE wca.div_my.Acttime END as Changed,
wca.div_my.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
case when wca.pexdt.PayDate2 is not null then wca.pexdt.PayDate2 else wca.pexdt.PayDate end as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
wca.divpy.RatioNew,
wca.divpy.RatioOld,
wca.divpy.Fractions,
wca.divpy.ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.div_my
INNER JOIN wca.rd ON wca.rd.RdID = wca.div_my.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.divpy ON wca.div_my.DivID = wca.divpy.DivID
                     and ('B' = wca.divpy.DivType or 'S' = wca.divpy.DivType)
                      and wca.divpy.Actflag<>'D'
LEFT OUTER JOIN wca.scmst as scmstres ON wca.divpy.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID
     AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DIV' = wca.pexdt.EventType
LEFT OUTER join wca.v10s_divpy as dvop1 ON wca.div_my.DivID = wca.dvop1.DivID
                     and 1 = wca.dvop1.OptionID
LEFT OUTER join wca.v10s_divpy as dvop2 ON wca.div_my.DivID = wca.dvop2.DivID
                     and 2 = wca.dvop2.OptionID
LEFT OUTER join wca.v10s_divpy as dvop3 ON wca.div_my.DivID = wca.dvop3.DivID
                     and 3 = wca.dvop3.OptionID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.divpy.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3)
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.divpy.OptionID is not null
and (((wca.dvop1.actflag = 'D' or wca.dvop1.DivID is null) and (wca.dvop2.actflag = 'D' or wca.dvop2.DivID is null))
        OR
    ((wca.dvop1.actflag = 'D' or wca.dvop1.DivID is null) and (wca.dvop3.actflag = 'D' or wca.dvop3.DivID is null))
        OR
    ((wca.dvop2.actflag = 'D' or wca.dvop2.DivID is null) and (wca.dvop3.actflag = 'D' or wca.dvop3.DivID is null)))
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 3
select distinct
'Stock Option Alert' as Levent,
wca.div_my.DivID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.div_my.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.div_my.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.div_my.Acttime) THEN wca.pexdt.Acttime ELSE wca.div_my.Acttime END as Changed,
case when wca.div_my.ActFlag<>'U' then wca.div_my.ActFlag else 'I' end as Actflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
case when wca.pexdt.PayDate2 is not null then wca.pexdt.PayDate2 else wca.pexdt.PayDate end as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
wca.divpy.ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.div_my
INNER JOIN wca.rd ON wca.rd.RdID = wca.div_my.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.divpy ON wca.div_my.DivID = wca.divpy.DivID
                     and ('B' = wca.divpy.DivType or 'S' = wca.divpy.DivType)
                      and wca.divpy.Actflag<>'D'
LEFT OUTER JOIN wca.scmst as scmstres ON wca.divpy.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID
     AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DIV' = wca.pexdt.EventType
LEFT OUTER join wca.v10s_divpy as dvop1 ON wca.div_my.DivID = wca.dvop1.DivID
                     and 1 = wca.dvop1.OptionID
LEFT OUTER join wca.v10s_divpy as dvop2 ON wca.div_my.DivID = wca.dvop2.DivID
                     and 2 = wca.dvop2.OptionID
LEFT OUTER join wca.v10s_divpy as dvop3 ON wca.div_my.DivID = wca.dvop3.DivID
                     and 3 = wca.dvop3.OptionID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.divpy.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.divpy.OptionID is not null
and NOT (((wca.dvop1.actflag = 'D' or wca.dvop1.DivID is null) and (wca.dvop2.actflag = 'D' or wca.dvop2.DivID is null))
        OR
    ((wca.dvop1.actflag = 'D' or wca.dvop1.DivID is null) and (wca.dvop3.actflag = 'D' or wca.dvop3.DivID is null))
        OR
    ((wca.dvop2.actflag = 'D' or wca.dvop2.DivID is null) and (wca.dvop3.actflag = 'D' or wca.dvop3.DivID is null)))
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 4
select distinct
'Call' as Levent,
wca.call_my.CallID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.call_my.AnnounceDate as Created,
CASE WHEN wca.rd.Acttime > wca.call_my.Acttime THEN wca.rd.Acttime ELSE wca.call_my.Acttime END as Changed,
wca.call_my.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'DueDate' as PrimaryDate,
wca.call_my.DueDate as Enddate1,
'RecDate' as SecondaryDate,
wca.rd.RecDate as Endate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.call_my
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.call_my.SecID
LEFT OUTER JOIN wca.rd ON wca.call_my.RdID = wca.rd.RdID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.call_my.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 5
select distinct
'Capital Reduction' as Levent,
wca.caprd.CaprdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.caprd.AnnounceDate as Created,
CASE WHEN wca.rd.Acttime > wca.caprd.Acttime THEN wca.rd.Acttime ELSE wca.caprd.Acttime END as Changed,
wca.caprd.ActFlag,
wca.scmst.PrimaryExchgCD,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.OldISIN = '') THEN wca.scmst.ISIN ELSE wca.icc.OldISIN END as Isin,
'PayDate' as PrimaryDate,
wca.caprd.PayDate as Enddate1,
'EffectiveDate' as SecondaryDate,
wca.caprd.EffectiveDate as Endate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Endate3,
wca.caprd.NewRatio as RatioNew,
wca.caprd.OldRatio as RatioOld,
wca.caprd.Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.caprd
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.caprd.SecID
LEFT OUTER JOIN wca.rd ON wca.caprd.RdID = wca.rd.RdID
LEFT OUTER JOIN wca.icc ON wca.caprd.CaprdID = wca.icc.RelEventID AND 'CAPRD' = wca.icc.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.caprd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 6
select distinct
'Arrangement' as Levent,
wca.arr.RdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.arr.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.arr.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.arr.Acttime) THEN wca.pexdt.Acttime ELSE wca.arr.Acttime END as Changed,
wca.arr.ActFlag,
wca.scmst.PrimaryExchgCD,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.OldISIN = '') THEN wca.scmst.ISIN ELSE wca.icc.OldISIN END as Isin,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.arr
INNER JOIN wca.rd ON wca.rd.RdID = wca.arr.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'ARR' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.arr.RdID = wca.icc.RelEventID AND 'ARR' = wca.icc.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.arr.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 7
select distinct
'Bonus' as Levent,
wca.bon.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.bon.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.bon.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.bon.Acttime) THEN wca.pexdt.Acttime ELSE wca.bon.Acttime END as Changed,
wca.bon.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.bon
INNER JOIN wca.rd ON wca.rd.RdID = wca.bon.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.bon.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'BON' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.bon.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 8
select distinct
'Bonus Rights' as Levent,
wca.br.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.br.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.br.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.br.Acttime) THEN wca.pexdt.Acttime ELSE wca.br.Acttime END as Changed,
wca.br.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.br
INNER JOIN wca.rd ON wca.rd.RdID = wca.br.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.br.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'BR' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.br.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 9
select distinct
'Consolidation' as Levent,
wca.consd.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.consd.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.consd.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.consd.Acttime) THEN wca.pexdt.Acttime ELSE wca.consd.Acttime END as Changed,
wca.consd.ActFlag,
wca.scmst.PrimaryExchgCD,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.OldISIN = '') THEN wca.scmst.ISIN ELSE wca.icc.OldISIN END as Isin,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
wca.scmst.SecID as ResSecid,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.NewISIN = '') THEN '' ELSE wca.icc.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.consd
INNER JOIN wca.rd ON wca.rd.RdID = wca.consd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'CONSD' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.consd.RdId = wca.icc.RelEventID AND 'CONSD' = wca.icc.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.consd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3)
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3)
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 10
select distinct
'Demerger Alert' as Levent,
wca.dmrgr.RdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.dmrgr.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.dmrgr.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.dmrgr.Acttime) THEN wca.pexdt.Acttime ELSE wca.dmrgr.Acttime END as Changed,
wca.dmrgr.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.dmrgr
INNER JOIN wca.rd ON wca.rd.RdID = wca.dmrgr.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DMRGR' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.dmrgr.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 11
select distinct
'Distribution Alert' as Levent,
wca.dist.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.dist.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.dist.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.dist.Acttime) THEN wca.pexdt.Acttime ELSE wca.dist.Acttime END as Changed,
wca.dist.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.dist
INNER JOIN wca.rd ON wca.rd.RdID = wca.dist.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DIST' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.dist.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 12
select distinct
'Divestment' as Levent,
wca.dvst.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.dvst.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.dvst.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.dvst.Acttime) THEN wca.pexdt.Acttime ELSE wca.dvst.Acttime END as Changed,
wca.dvst.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.dvst
INNER JOIN wca.rd ON wca.rd.RdID = wca.dvst.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.dvst.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DVST' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.dvst.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 16
select distinct
'Rights' as Levent,
wca.rts.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.rts.AnnounceDate as Created,
wca.rd.Acttime Changed,
wca.rts.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'EndTrade' as PrimaryDate,
wca.rts.EndTrade as Enddate1,
'StartTrade' as SecondaryDate,
wca.rts.StartTrade as EndDate2,
'SplitDate' as TertiaryDate,
wca.rts.SplitDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.rts
INNER JOIN wca.rd ON wca.rd.RdID = wca.rts.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.rts.ResSecID = wca.scmstres.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.rts.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 13
select distinct
'Entitlement' as Levent,
wca.ent.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.ent.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.ent.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.ent.Acttime) THEN wca.pexdt.Acttime ELSE wca.ent.Acttime END as Changed,
wca.ent.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.ent
INNER JOIN wca.rd ON wca.rd.RdID = wca.ent.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.ent.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'ENT' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.ent.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 14
select distinct
'Sub-Division' as Levent,
wca.sd.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.sd.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.sd.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.sd.Acttime) THEN wca.pexdt.Acttime ELSE wca.sd.Acttime END as Changed,
wca.sd.ActFlag,
wca.scmst.PrimaryExchgCD,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.OldISIN = '') THEN wca.scmst.ISIN ELSE wca.icc.OldISIN END as Isin,
'NewCodeDate' as PrimaryDate,
wca.icc.Acttime as EndDate1,
'PayDate' as SecondaryDate,
wca.pexdt.PayDate as EndDate2,
'RecDate' as TertiaryDate,
wca.pexdt.ExDate as EndDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
wca.scmst.SecID as ResSecid,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.NewISIN = '') THEN '' ELSE wca.icc.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.sd
INNER JOIN wca.rd ON wca.rd.RdID = wca.sd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'SD' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.sd.RdId = wca.icc.RelEventID AND 'SD' = wca.icc.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.sd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.icc.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 15
select distinct
'Merger Alert' as Levent,
wca.mrgr.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.mrgr.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.mrgr.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.mrgr.Acttime) THEN wca.pexdt.Acttime ELSE wca.mrgr.Acttime END as Changed,
wca.mrgr.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'AppointedDate' as PrimaryDate,
wca.mrgr.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
wca.mrgr.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
wca.pexdt.PayDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.mrgr
INNER JOIN wca.rd ON wca.rd.RdID = wca.mrgr.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'MRGR' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.mrgr.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 17
select distinct
'Preferential Offer' as Levent,
wca.prf.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.prf.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.prf.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.prf.Acttime) THEN wca.pexdt.Acttime ELSE wca.prf.Acttime END as Changed,
wca.prf.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.prf
INNER JOIN wca.rd ON wca.rd.RdID = wca.prf.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.prf.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'PRF' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.prf.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 18
select distinct
'Assimilation' as Levent,
wca.assm.AssmId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.assm.AnnounceDate as Created,
wca.assm.Acttime as Changed,
wca.assm.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'AssimilationDate' as PrimaryDate,
AssimilationDate as EndDate1,
'' as SecondaryDate,
'' as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.assm
INNER JOIN wca.scmst ON wca.assm.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.assm.ResSecID = wca.scmstres.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.assm.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 19
select distinct
'Buy Back Alert' as Levent,
wca.bb.BBId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.bb.AnnounceDate as Created,
wca.bb.Acttime as Changed,
wca.bb.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'StartDate' as PrimaryDate,
wca.bb.startdate as EndDate1,
'StartDate' as SecondaryDate,
wca.bb.EndDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.bb
INNER JOIN wca.scmst ON wca.bb.SecID = wca.scmst.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.bb.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 20
select distinct
'Liquidation Alert' as Levent,
wca.liq.LiqID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.liq.AnnounceDate as Created,
wca.liq.Acttime as Changed,
wca.liq.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'RdDate' as PrimaryDate,
wca.liq.RdDate as EndDate1,
'' as SecondaryDate,
'' as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.liq
INNER JOIN wca.scmst ON wca.liq.IssID = wca.scmst.IssID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
wca.liq.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 21
select distinct
'Redemption' as Levent,
wca.redem.RedemID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'RedemDate' as PrimaryDate,
wca.redem.RedemDate as EndDate1,
'RecDate' as SecondaryDate,
wca.rd.RecDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.redem
LEFT OUTER JOIN wca.rd ON wca.redem.RdID = wca.rd.RdID
INNER JOIN wca.scmst ON wca.redem.SecID = wca.scmst.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.redem.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3))
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 22
select distinct
'Takeover Alert' as Levent,
wca.tkovr.TKOVRID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.tkovr.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.tkovr.Acttime) THEN wca.rd.Acttime ELSE wca.tkovr.Acttime END as Changed,
wca.tkovr.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'CloseDate' as PrimaryDate,
wca.tkovr.closedate as EndDate1,
'CmAcqDate' as SecondaryDate,
wca.tkovr.CmAcqDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.tkovr
LEFT OUTER JOIN wca.rd ON wca.tkovr.RdID = wca.rd.RdID
INNER JOIN wca.scmst ON wca.tkovr.SecID = wca.scmst.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.tkovr.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3))
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 23
select distinct
'Security Swap' as Levent,
wca.scswp.RdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scswp.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.scswp.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.scswp.Acttime) THEN wca.pexdt.Acttime ELSE wca.scswp.Acttime END as Changed,
wca.scswp.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as Enddate3,
wca.scswp.NewRatio as RatioNew,
wca.scswp.OldRatio as RatioOld,
wca.scswp.Fractions,
wca.scswp.ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.scswp
INNER JOIN wca.rd ON wca.rd.RdID = wca.scswp.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.scswp.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'SCSWP' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.scswp.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 24
select distinct
'Security Reclassification' as Levent,
wca.secrc.SecrcID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.secrc.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.secrc.Acttime) THEN wca.rd.Acttime ELSE wca.secrc.Acttime END as Changed,
wca.secrc.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'EffectiveDate' as PrimaryDate,
wca.secrc.EffectiveDate as EndDate1,
'RecDate' as SecondaryDate,
wca.rd.RecDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
wca.secrc.RatioNew,
wca.secrc.RatioOld,
'' as Fractions,
wca.secrc.ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.secrc
INNER JOIN wca.rd ON wca.rd.RdID = wca.secrc.SecRcID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.secrc.ResSecID = wca.scmstres.SecID
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.secrc.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3))
and wca.scexh.ListStatus<>'D'
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')

--# 25
select distinct
'DRIP' as Levent,
wca.drip.DivID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.drip.AnnounceDate as Created,
wca.drip.Acttime as Changed,
wca.drip.ActFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.ISIN,
'DripPayDate' as PrimaryDate,
wca.drip.DripPayDate as EndDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EndDate2,
'LastDate' as TertiaryDate,
wca.drip.DripLastDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.drip
LEFT OUTER JOIN wca.div_my ON wca.drip.DivID = wca.div_my.DivID
INNER JOIN wca.rd ON wca.div_my.RdID = wca.rd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DIV' = wca.pexdt.EventType
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
Where
(wca.drip.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(acttime) from wca.tbl_opslog where seq = 1) and (select max(acttime) from wca.tbl_opslog where seq = 3) )
and wca.scmst.sectycd <> 'DR'
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.sectygrp.secgrpid=1
and (wca.exchg.cntrycd = 'AT'
or wca.exchg.cntrycd = 'BE'
or wca.exchg.cntrycd = 'BG'
or wca.exchg.cntrycd = 'CH'
or wca.exchg.cntrycd = 'CY'
or wca.exchg.cntrycd = 'CZ'
or wca.exchg.cntrycd = 'DE'
or wca.exchg.cntrycd = 'DK'
or wca.exchg.cntrycd = 'EE'
or wca.exchg.cntrycd = 'ES'
or wca.exchg.cntrycd = 'FI'
or wca.exchg.cntrycd = 'FR'
or wca.exchg.cntrycd = 'GB'
or wca.exchg.cntrycd = 'GR'
or wca.exchg.cntrycd = 'HU'
or wca.exchg.cntrycd = 'IE'
or wca.exchg.cntrycd = 'IS'
or wca.exchg.cntrycd = 'IT'
or wca.exchg.cntrycd = 'LT'
or wca.exchg.cntrycd = 'LU'
or wca.exchg.cntrycd = 'LV'
or wca.exchg.cntrycd = 'MT'
or wca.exchg.cntrycd = 'NL'
or wca.exchg.cntrycd = 'NO'
or wca.exchg.cntrycd = 'PL'
or wca.exchg.cntrycd = 'PT'
or wca.exchg.cntrycd = 'RO'
or wca.exchg.cntrycd = 'SE'
or wca.exchg.cntrycd = 'SI'
or wca.exchg.cntrycd = 'SK')
