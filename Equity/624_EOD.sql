--filepath=o:\Datafeed\Equity\624\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.624
--suffix=
--fileheadertext=EDI_STATIC_624_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\624\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT 
wca.scexh.ScexhID,
CASE WHEN wca.scexh.Acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3) THEN wca.scexh.Acttime
WHEN wca.scmst.Acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3) THEN wca.scmst.Acttime
WHEN wca.issur.Acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3) THEN wca.issur.Acttime
ELSE wca.exchg.acttime END as Changed,
wca.scexh.AnnounceDate as Created,
wca.scexh.Actflag,
wca.scmst.IssID,
wca.scmst.SecID,
wca.issur.CntryofIncorp,
wca.issur.Issuername,
wca.scmst.Securitydesc,
CASE WHEN locate('.',wca.scmst.Parvalue) >0
                THEN substring(wca.scmst.Parvalue,1,locate('.',wca.scmst.Parvalue)+5)
                ELSE wca.scmst.Parvalue
                END AS Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.Isin,
wca.scmst.Uscode,
case when wca.scmst.Statusflag is not null then wca.scmst.Statusflag else 'A' end as SecStatus,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
substring(wca.scexh.Exchgcd,1,2) as ExCountry,
wca.scexh.Exchgcd,
wca.exchg.Mic,
wca.scexh.Localcode,
CASE WHEN (wca.scexh.ListStatus IS NULL) or (wca.scexh.ListStatus='') THEN 'L' ELSE wca.scexh.ListStatus END as ListStatus,
wca.scexh.ListDate,
wca.scexh.Lot
FROM wca.scmst
INNER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.cntry ON wca.issur.CntryofIncorp = wca.cntry.cntryCD
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
where
(wca.scmst.PrimaryExchgCD = wca.scexh.exchgcd
or wca.scmst.PrimaryExchgCD = ''
or wca.scmst.PrimaryExchgCD is null
or wca.scexh.exchgcd = 'DEFSX')
and (wca.scmst.sectycd = 'EQS'
or wca.scmst.sectycd = 'PRF'
or wca.scmst.sectycd = 'DR')
and (wca.scmst.acttime>= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.exchg.acttime>= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime>= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or (wca.issur.acttime>= (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.scmst.Statusflag<>'I'));
