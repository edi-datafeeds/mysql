--filepath=o:\Datafeed\Equity\625\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.625
--suffix=
--fileheadertext=EDI_LOOKUP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\Equity\625\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper(Actflag) as Actflag,
wca.lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
wca.lookup.Lookup
FROM wca.lookup
where 
wca.lookup.code <> 'BLANK'
and wca.lookup.code <> ''
and wca.lookup.code <> 'INTONMAT'
and wca.lookup.code <> 'INTONTRIG'
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('ACTION') as TypeGroup,
wca.iraction.Code,
wca.iraction.Lookup
from wca.iraction
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
wca.dvprd.DvprdCD,
wca.dvprd.Divperiod
from wca.dvprd
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('EXSTYLE') as TypeGroup,
wca.irexstyle.Code,
wca.irexstyle.Lookup
from wca.irexstyle
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('FRACTIONS') as TypeGroup,
wca.irfractions.Code,
wca.irfractions.Lookup
from wca.irfractions
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('LSTATSTAT') as TypeGroup,
wca.irlstatstat.Code,
wca.irlstatstat.Lookup
from wca.irlstatstat
union
SELECT
upper('I') as Actflag,
'2006/01/26' as AnnounceDate,
upper('MANDOPT') as TypeGroup,
wca.irmandopt.Code,
wca.irmandopt.Lookup
from wca.irmandopt
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('MRGRSTAT') as TypeGroup,
wca.irmrgrstat.Code,
wca.irmrgrstat.Lookup
from wca.irmrgrstat
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('ONOFFMARKET') as TypeGroup,
wca.ironoffmarket.Code,
wca.ironoffmarket.Lookup
from wca.ironoffmarket
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('PARTFINAL') as TypeGroup,
wca.irpartfinal.Code,
wca.irpartfinal.Lookup
from wca.irpartfinal
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('PAYTYPE') as TypeGroup,
wca.irpaytype.Code,
wca.irpaytype.Lookup
from wca.irpaytype
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('SCMSTSTAT') as TypeGroup,
wca.irscmststat.Code,
wca.irscmststat.Lookup
from wca.irscmststat
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('TKOVRSTAT') as TypeGroup,
wca.irtkovrstat.Code,
wca.irtkovrstat.Lookup
from wca.irtkovrstat
union
SELECT
upper('I') as Actflag,
secty.Acttime,
upper('SECTYPE') as TypeGroup,
wca.secty.SectyCD,
wca.secty.SecurityDescriptor
from wca.secty
union
SELECT
wca.exchg.Actflag,
wca.exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
wca.exchg.ExchgCD as Code,
wca.exchg.Exchgname as Lookup
from wca.exchg
union
SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('MEETING') as TypeGroup,
wca.irmeeting.Code,
wca.irmeeting.Lookup
from wca.irmeeting
union
SELECT
wca.exchg.Actflag,
wca.exchg.Acttime,
upper('MIC') as TypeGroup,
wca.exchg.ExchgCD as Code,
wca.exchg.MIC as Lookup
from wca.exchg
order by TypeGroup, Code