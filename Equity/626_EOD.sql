--filepath=o:\Datafeed\Equity\626\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.626
--suffix=
--fileheadertext=EDI_STATIC_626_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\626\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
wca.issur.Actflag,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp
FROM wca.issur
where wca.issur.acttime>(select max(acttime) from wca.tbl_opslog where seq = 1)

--# 2
SELECT
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.Statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.FYENPPDate,
wca.scmst.USCode,
wca.scmst.ISIN
from wca.scmst
where wca.scmst.acttime>(select max(acttime) from wca.tbl_opslog where seq = 1)
and (wca.scmst.sectycd = 'EQS'
or wca.scmst.sectycd = 'PRF'
or wca.scmst.sectycd = 'DR')

--# 3
SELECT
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.SedolId
FROM wca.sedol
LEFT OUTER JOIN wca.scmst ON wca.sedol.SECID = wca.scmst.SECID
where wca.sedol.acttime>(select max(acttime) from wca.tbl_opslog where seq = 1)
and (wca.scmst.sectycd = 'EQS'
or wca.scmst.sectycd = 'PRF'
or wca.scmst.sectycd = 'DR')


--# 4
SELECT
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.SecID,
wca.scexh.ExchgCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
'' as TradeStatus,
wca.scexh.LocalCode,
wca.scexh.ScExhID
FROM wca.scexh
LEFT OUTER JOIN wca.scmst ON wca.scexh.SECID = wca.scmst.SECID
where wca.scexh.acttime>(select max(acttime) from wca.tbl_opslog where seq = 1)
and (wca.scmst.sectycd = 'EQS'
or wca.scmst.sectycd = 'PRF'
or wca.scmst.sectycd = 'DR')
