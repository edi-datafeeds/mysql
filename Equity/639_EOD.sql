--filepath=o:\Datafeed\Equity\639\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.639
--suffix=
--fileheadertext=EDI_STATIC_639_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\639\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
SELECT
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp
FROM wca.issur
WHERE wca.issur.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)

--# 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.statusflag<>'I' OR wca.scmst.statusflag is null then 'A' else 'I' end as statusflag,
wca.scmst.StatusReason,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.X as CommonCode,
wca.scmst.SharesOutstanding
FROM wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
WHERE 
wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.scmst.PrimaryExchgCD=wca.scexh.ExchgCD
and (wca.scmst.statusflag <> 'I'  or wca.scmst.statusflag is null)
and (wca.scmst.secid in (select secid from portfolio.mklist)
or ((wca.dprcp.drtype <> 'CDI' or wca.dprcp.drtype is null)
and (wca.dprcp.drtype <> 'NDR' or wca.dprcp.drtype is null)
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.scmst.sectycd <> 'SP'
and wca.scmst.sectycd <> 'STP'
and wca.scmst.sectycd <> 'SS'
and wca.sectygrp.secgrpid<3
and (
substring(wca.scmst.primaryexchgcd,1,2) in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East')
or wca.issur.cntryofincorp  in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East'))
))

--# 3
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SecID,
wca.sedol.cntrycd,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.SedolId
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
inner join wca,issur on wca.scmst.issid = wca.issur.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
WHERE
wca.sedol.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and substring(wca.scmst.PrimaryExchgCD,1,2) = wca.sedol.CntryCD
and (wca.scmst.statusflag <> 'I'  or wca.scmst.statusflag is null)
and (wca.scmst.secid in (select wca.secid from portfolio.dbo.mklist)
or ((wca.dprcp.drtype <> 'CDI' or wca.dprcp.drtype is null)
and (wca.dprcp.drtype <> 'NDR' or wca.dprcp.drtype is null)
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.scmst.sectycd <> 'SP'
and wca.scmst.sectycd <> 'STP'
and wca.scmst.sectycd <> 'SS'
and wca.sectygrp.secgrpid<3
and (
substring(wca.scmst.primaryexchgcd,1,2) in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East')
or wca.issur.cntryofincorp  in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East'))
))



--# 4
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
case when wca.scexh.ListStatus='S' OR wca.scexh.ListStatus ='R' OR wca.scexh.ListStatus ='D' then wca.scexh.ListStatus else 'L' end as ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scmst.ISIN,
wca.scmst.CurenCD as ScmstCurenCD,
wca.exchg.MIC
FROM wca.scexh
left outer join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
WHERE 
wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.scmst.PrimaryExchgCD=wca.scexh.ExchgCD
and (wca.scmst.statusflag <> 'I'  or wca.scmst.statusflag is null)
and (wca.scmst.secid in (select secid from portfolio.mklist)
or ((wca.dprcp.drtype <> 'CDI' or wca.dprcp.drtype is null)
and (wca.dprcp.drtype <> 'NDR' or wca.dprcp.drtype is null)
and wca.scmst.sectycd <> 'UNT'
and wca.scmst.sectycd <> 'CDI'
and wca.scmst.sectycd <> 'TRT'
and wca.scmst.sectycd <> 'DRT'
and wca.scmst.sectycd <> 'SP'
and wca.scmst.sectycd <> 'STP'
and wca.scmst.sectycd <> 'SS'
and wca.sectygrp.secgrpid<3
and (
substring(wca.scmst.primaryexchgcd,1,2) in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East')
or wca.issur.cntryofincorp  in (select cntrycd from wca.continent where wca.continent='Europe' or wca.continent = 'Africa' or wca.continent = 'Middle East'))
))


--# 5
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.cntrycd,
wca.exchg.MIC
from wca.exchg
WHERE wca.exchg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)

--# 6
SELECT
upper('DPRCP') as Tablename,
wca.dprcp.Actflag,
wca.dprcp.Acttime,
wca.dprcp.Announcedate,
wca.dprcp.SecID,
wca.dprcp.UnSecID,
wca.dprcp.DRRatio,
wca.dprcp.USRatio,
wca.dprcp.SpnFlag,
wca.dprcp.DRtype,
wca.dprcp.DepBank,
wca.dprcp.LevDesc,
wca.dprcp.OtherDepBank
from wca.dprcp
WHERE wca.dprcp.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.dprcp.drtype <> 'CDI'
and wca.dprcp.drtype <> 'NDR'
