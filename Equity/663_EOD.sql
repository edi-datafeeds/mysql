--filepath=o:\Datafeed\Equity\663\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.663
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_663_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\663\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
select
wca.sdchg.SdchgID as EventID,
'Sedol Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.sdchg.Actflag,
wca.sdchg.eventtype as Reason,
case when wca.sdchg.effectivedate is null
     then wca.sdchg.announcedate
     else wca.sdchg.effectivedate
     end as EffectiveDate,
wca.sdchg.OldSedol as OldStatic,
wca.sdchg.NewSedol as NewStatic
FROM wca.sdchg
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.sdchg.SecID
where
wca.sdchg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 3
select
wca.icc.ICCID as EventID,
'ISIN Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.icc.Actflag,
wca.icc.eventtype as Reason,
case when wca.icc.effectivedate is null
     then wca.icc.announcedate
     else wca.icc.effectivedate
     end as Effectivedate,
wca.icc.OldIsin as OldStatic,
wca.icc.NewIsin as NewStatic
FROM wca.icc
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.icc.SecID
where
wca.icc.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.icc.OldIsin <> '' or wca.icc.NewIsin <> '')


--# 4
select
wca.inchg.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.inchg.Actflag,
wca.inchg.eventtype as Reason,
CASE WHEN wca.inchg.inchgdate IS NULL
     THEN wca.inchg.ANNOUNCEDATE
     ELSE wca.inchg.inchgdate
     END AS EffectiveDate,
wca.inchg.OldCntryCD as OldStatic,
wca.inchg.NewCntryCD as NewStatic
FROM wca.inchg
INNER JOIN wca.scmst ON wca.scmst.issid = inchg.issid
where
wca.inchg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 5
select
wca.ischg.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.ischg.Actflag,
wca.ischg.eventtype as Reason,
case when wca.ischg.namechangedate is null
     then wca.ischg.announcedate
     else wca.ischg.namechangedate
     end as EffectiveDate,
wca.ischg.IssOldName as OldStatic,
wca.ischg.IssNewName as NewStatic
FROM wca.ischg
INNER JOIN wca.scmst ON wca.scmst.issid = wca.ischg.issid
where 
wca.ischg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.scmst.statusflag<>'I'


--# 6
select
wca.lcc.LCCID as EventID,
'Local Code Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.lcc.Actflag,
wca.lcc.eventtype as Reason,
case when wca.lcc.effectivedate is null
     then wca.lcc.announcedate
     else wca.lcc.effectivedate
     end as EffectiveDate,
wca.lcc.OldLocalCode as OldStatic,
wca.lcc. NewLocalCode as NewStatic
FROM wca.lcc
INNER JOIN wca.scmst ON wca.scmst.secid = wca.lcc.secid
where 
wca.lcc.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 7
select
wca.scchg.SCCHGID as EventID,
'Security Description Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.scchg.Actflag,
wca.scchg.eventtype as Reason,
case when wca.scchg.dateofchange is null
     then wca.scchg.announcedate
     else wca.scchg.dateofchange
     end as EffectiveDate,
wca.scchg.SecOldName as OldStatic,
wca.scchg.SecNewName as NewStatic
FROM wca.scchg
INNER JOIN wca.scmst ON wca.scmst.secid = wca.scchg.secid
where 
wca.scchg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)


--# 8
select
wca.ltchg.LTCHGID as EventID,
'Lot Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.ltchg.Actflag,
'' as Reason,
case when wca.ltchg.effectivedate is null
     then wca.ltchg.announcedate
     else wca.ltchg.effectivedate
     end as EffectiveDate,
wca.ltchg.OldLot as OldStatic,
wca.ltchg.NewLot as NewStatic
FROM wca.ltchg
INNER JOIN wca.scmst ON wca.scmst.secid = wca.ltchg.secid
where 
wca.ltchg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.ltchg.OldLot <> ''
or wca.ltchg.NewLot <> '')


--# 9
select
wca.ltchg.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.ltchg.Actflag,
'' as Reason,
case when wca.ltchg.effectivedate is null
     then wca.ltchg.announcedate
     else wca.ltchg.effectivedate
     end as EffectiveDate,
wca.ltchg.OldMinTrdQty as OldStatic,
wca.ltchg.NewMinTrdgQty as NewStatic
FROM wca.ltchg
INNER JOIN wca.scmst ON wca.scmst.secid = wca.ltchg.secid
where
wca.ltchg.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.ltchg.OldMinTrdQty <> ''
or wca.ltchg.NewMinTrdgQty <> '')


--# 10
select
wca.lstat.LSTATID as EventID,
'Listing Status Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.lstat.Actflag,
wca.lstat.eventtype as Reason,
case when wca.lstat.effectivedate is null
     then wca.lstat.announcedate
     else wca.lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
wca.lstat.LStatStatus as NewStatic
FROM wca.lstat
INNER JOIN wca.scmst ON wca.scmst.secid = wca.lstat.secid
where
wca.lstat.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)



--# 12
select
wca.icc.ICCID as EventID,
'USCode Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.icc.Actflag,
wca.icc.eventtype as Reason,
case when wca.icc.EffectiveDate is null
     then wca.icc.announcedate
     else wca.icc.effectivedate
     end as EffectiveDate,
wca.icc.OldUSCode as OldStatic,
wca.icc.NewUSCode as NewStatic
FROM wca.icc
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.icc.SecID
where
wca.icc.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.icc.OldUSCode <> ''
or wca.icc.NewUSCode <> '')


--# 13
select
wca.currd.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
wca.scmst.IssID,
wca.scmst.SecID,
wca.currd.Actflag,
wca.currd.eventtype as Reason,
case when wca.currd.EffectiveDate is null
     then wca.currd.announcedate
     else wca.currd.effectivedate
     end as EffectiveDate,
wca.currd.OldCurenCD as OldStatic,
wca.currd.NewCurenCD as NewStatic
FROM wca.currd
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.currd.SecID
where
wca.currd.Acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)

