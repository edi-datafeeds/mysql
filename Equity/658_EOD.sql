--filepath=o:\Datafeed\Equity\658\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.658
--suffix=
--fileheadertext=EDI_658_LISTDATA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\658\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
select
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
case when wca.sedol.secid is not null and wca.sedol.actflag<>'D' and wca.sedol.defunct<>'T' then wca.sedol.sedol else '' end as Sedol,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.scmst.SecurityDesc,
wca.scmst.SectyCD,
wca.scmst.PrimaryExchgCD,
wca.scmst.SharesOutstanding,
wca.scexh.ExchgCD,
wca.scexh.LocalCode,
wca.scexh.listDate,
wca.scexh.ListStatus,
case when wca.scexh.actflag is not null then wca.scexh.actflag else wca.scmst.actflag end as RecordStatus
FROM wca.scexh
INNER JOIN wca.scmst ON wca.scexh.SecID = wca.scmst.SecID
INNER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and substring(wca.scexh.exchgcd,1,2) = wca.sedol.cntrycd
where
wca.scmst.sectycd<>'CW'
and (wca.scexh.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1)
or wca.scmst.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1)
or wca.issur.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1))
