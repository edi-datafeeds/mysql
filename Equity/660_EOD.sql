--filepath=o:\Datafeed\Equity\660\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.660
--suffix=
--fileheadertext=EDI_REORG_660_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\660\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 0
select
'Levent' as f1,
'EventID' as f2,
'SecID' as f3,
'IssID' as f4,
'Created' as f5,
'Changed' as f6,
'Actflag' as f7,
'PrimaryDate' as f12,
'EventDate1' as f11,
'SecondaryDate' as f12,
'EventDate2' as f13,
'TertiaryDate' as f14,
'EventDate3' as f15,
'RatioNew' as f16,
'RatioOld' as f17,
'Fractions' as f18,
'ResSecID' as f19,
'ResIsin' as f20,
'Addfld1' as f21,
'AddInfo1' as f22,
'Addfld2' as f23,
'AddInfo2' as f24,
'Addfld3' as f25,
'AddInfo3' as f26


--# 1
select distinct
'Bankruptcy' as Levent,
wca.bkrp.BkrpID as EventID,
'' as SecID,
wca.bkrp.IssID,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,
wca.bkrp.ActFlag,
'FilingDate' as PrimaryDate,
wca.bkrp.FilingDate as EventDate1,
'NotificationDate' as SecondaryDate,
wca.bkrp.NotificationDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.bkrp
Where
wca.bkrp.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 


--# 2
select distinct
'Lawsuit' as Levent,
wca.lawst.LAWSTID as EventID,
'' as SecID,
wca.lawst.IssID,
wca.lawst.AnnounceDate as Created,
wca.lawst.Acttime as Changed,
wca.lawst.ActFlag,
'EffectiveDate' as PrimaryDate,
wca.lawst.EffectiveDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.lawst
Where
wca.lawst.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 


--# 3
select distinct
'Company Meeting' as Levent,
wca.agm.AGMID as EventID,
'' as SecID,
wca.agm.IssID,
wca.agm.AnnounceDate as Created,
wca.agm.Acttime as Changed,
wca.agm.ActFlag,
'AGMDate' as PrimaryDate,
wca.agm.AGMDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.agm
Where
wca.agm.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 


--# 4
select distinct
'Call' as Levent,
wca.call_my.CallID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.call_my.AnnounceDate as Created,
CASE WHEN wca.rd.Acttime > wca.call_my.Acttime THEN wca.rd.Acttime ELSE wca.call_my.Acttime END as Changed,
wca.call_my.ActFlag,
'DueDate' as PrimaryDate,
wca.call_my.DueDate as EventDate1,
'RecDate' as SecondaryDate,
wca.rd.RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.call_my
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.call_my.SecID
LEFT OUTER JOIN wca.rd ON wca.call_my.RdID = wca.rd.RdID
Where
(wca.call_my.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3)
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 5
select distinct
'Capital Reduction' as Levent,
wca.caprd.CaprdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.caprd.AnnounceDate as Created,
CASE WHEN wca.rd.Acttime > wca.caprd.Acttime THEN wca.rd.Acttime ELSE wca.caprd.Acttime END as Changed,
wca.caprd.ActFlag,
'PayDate' as PrimaryDate,
wca.caprd.PayDate as EventDate1,
'EffectiveDate' as SecondaryDate,
wca.caprd.EffectiveDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
wca.caprd.NewRatio as RatioNew,
wca.caprd.OldRatio as RatioOld,
wca.caprd.Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.caprd
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.caprd.SecID
LEFT OUTER JOIN wca.rd ON wca.caprd.RdID = wca.rd.RdID
LEFT OUTER JOIN wca.icc ON wca.caprd.CaprdID = wca.icc.RelEventID AND 'CAPRD' = wca.icc.EventType
Where
(wca.caprd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3))


--# 6
select distinct
'Arrangement' as Levent,
wca.arr.RdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.arr.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.arr.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.arr.Acttime) THEN wca.pexdt.Acttime ELSE wca.arr.Acttime END as Changed,
wca.arr.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.arr
INNER JOIN wca.rd ON wca.rd.RdID = wca.arr.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'ARR' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.arr.RdID = wca.icc.RelEventID AND 'ARR' = wca.icc.EventType
Where
(wca.arr.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 7
select distinct
'Bonus' as Levent,
wca.bon.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.bon.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.bon.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.bon.Acttime) THEN wca.pexdt.Acttime ELSE wca.bon.Acttime END as Changed,
wca.bon.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.bon
INNER JOIN wca.rd ON wca.rd.RdID = wca.bon.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.bon.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'BON' = wca.pexdt.EventType
Where
(wca.bon.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 8
select distinct
'Bonus Rights' as Levent,
wca.br.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.br.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.br.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.br.Acttime) THEN wca.pexdt.Acttime ELSE wca.br.Acttime END as Changed,
wca.br.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.br
INNER JOIN wca.rd ON wca.rd.RdID = wca.br.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.br.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'BR' = wca.pexdt.EventType
Where
(wca.br.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 9
select distinct
'Consolidation' as Levent,
wca.consd.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.consd.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.consd.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.consd.Acttime) THEN wca.pexdt.Acttime ELSE wca.consd.Acttime END as Changed,
wca.consd.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
wca.scmst.SecID as ResSecid,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.NewISIN = '') THEN '' ELSE wca.icc.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.consd
INNER JOIN wca.rd ON wca.rd.RdID = wca.consd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'CONSD' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.consd.RdId = wca.icc.RelEventID AND 'CONSD' = wca.icc.EventType
Where
(wca.consd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )



--# 12
select distinct
'Divestment' as Levent,
wca.dvst.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.dvst.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.dvst.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.dvst.Acttime) THEN wca.pexdt.Acttime ELSE wca.dvst.Acttime END as Changed,
wca.dvst.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.dvst
INNER JOIN wca.rd ON wca.rd.RdID = wca.dvst.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.dvst.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DVST' = wca.pexdt.EventType
Where
(wca.dvst.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 16
select distinct
'Rights' as Levent,
wca.rts.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.rts.AnnounceDate as Created,
wca.rd.Acttime Changed,
wca.rts.ActFlag,
'EndTrade' as PrimaryDate,
wca.rts.EndTrade as EventDate1,
'StartTrade' as SecondaryDate,
wca.rts.StartTrade as EventDate2,
'SplitDate' as TertiaryDate,
wca.rts.SplitDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.rts
INNER JOIN wca.rd ON wca.rd.RdID = wca.rts.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.rts.ResSecID = wca.scmstres.SecID
Where
wca.rts.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 


--# 13
select distinct
'Entitlement' as Levent,
wca.ent.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.ent.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.ent.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.ent.Acttime) THEN wca.pexdt.Acttime ELSE wca.ent.Acttime END as Changed,
wca.ent.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.ent
INNER JOIN wca.rd ON wca.rd.RdID = wca.ent.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.ent.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'ENT' = wca.pexdt.EventType
Where
(wca.ent.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 14
select distinct
'Sub-Division' as Levent,
wca.sd.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.sd.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.sd.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.sd.Acttime) THEN wca.pexdt.Acttime ELSE wca.sd.Acttime END as Changed,
wca.sd.ActFlag,
'NewCodeDate' as PrimaryDate,
wca.icc.Acttime as EventDate1,
'PayDate' as SecondaryDate,
wca.pexdt.PayDate as EventDate2,
'RecDate' as TertiaryDate,
wca.pexdt.ExDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
wca.scmst.SecID as ResSecid,
CASE WHEN (wca.icc.RelEventID is null OR wca.icc.NewISIN = '') THEN '' ELSE wca.icc.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.sd
INNER JOIN wca.rd ON wca.rd.RdID = wca.sd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'SD' = wca.pexdt.EventType
LEFT OUTER JOIN wca.icc ON wca.sd.RdId = wca.icc.RelEventID AND 'SD' = wca.icc.EventType
Where
(wca.sd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.icc.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )



--# 17
select distinct
'Preferential Offer' as Levent,
wca.prf.RdId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.prf.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.prf.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.prf.Acttime) THEN wca.pexdt.Acttime ELSE wca.prf.Acttime END as Changed,
wca.prf.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.prf
INNER JOIN wca.rd ON wca.rd.RdID = wca.prf.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.prf.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'PRF' = wca.pexdt.EventType
Where
(wca.prf.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )



--# 18
select distinct
'Assimilation' as Levent,
wca.assm.AssmId as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.assm.AnnounceDate as Created,
wca.assm.Acttime as Changed,
wca.assm.ActFlag,
'AssimilationDate' as PrimaryDate,
AssimilationDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.assm
INNER JOIN wca.scmst ON wca.assm.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.assm.ResSecID = wca.scmstres.SecID
Where
wca.assm.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
and wca.assm.Actflag <> 'D' 


--# 21
select distinct
'Preference Redemption' as Levent,
wca.redem.RedemID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.ActFlag,
'RedemDate' as PrimaryDate,
wca.redem.RedemDate as EventDate1,
'RecDate' as SecondaryDate,
wca.rd.RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.redem
LEFT OUTER JOIN wca.rd ON wca.redem.RdID = wca.rd.RdID
INNER JOIN wca.scmst ON wca.redem.SecID = wca.scmst.SecID
Where
(wca.redem.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3))
and wca.scmst.sectycd<>'BND'


--# 22
select distinct
'Preference Conversion' as Levent,
wca.conv.CONVID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.conv.AnnounceDate as Created,
wca.conv.Acttime as Changed,
wca.conv.ActFlag,
'FromDate' as PrimaryDate,
wca.conv.FromDate as EventDate1,
'ToDate' as SecondaryDate,
wca.conv.ToDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
wca.conv.RatioNew as RatioNew,
wca.conv.RatioOld as RatioOld,
wca.conv.Fractions as Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.conv
INNER JOIN wca.scmst ON wca.conv.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.conv.ResSecID = wca.scmstres.SecID
Where
wca.conv.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3)
and wca.scmst.sectycd<>'BND'


--# 23
select distinct
'Security Swap' as Levent,
wca.scswp.RdID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scswp.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.scswp.Acttime) and (wca.rd.Acttime > wca.pexdt.Acttime) THEN wca.rd.Acttime WHEN (wca.pexdt.Acttime is not null) and (wca.pexdt.Acttime > wca.scswp.Acttime) THEN wca.pexdt.Acttime ELSE wca.scswp.Acttime END as Changed,
wca.scswp.ActFlag,
'PayDate' as PrimaryDate,
wca.pexdt.PayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'RecDate' as TertiaryDate,
wca.rd.RecDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.scswp
INNER JOIN wca.rd ON wca.rd.RdID = wca.scswp.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.scswp.ResSecID = wca.scmstres.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'SCSWP' = wca.pexdt.EventType
Where
(wca.scswp.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 24
select distinct
'Security Reclassification' as Levent,
wca.secrc.SecrcID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.secrc.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.secrc.Acttime) THEN wca.rd.Acttime ELSE wca.secrc.Acttime END as Changed,
wca.secrc.ActFlag,
'EffectiveDate' as PrimaryDate,
EffectiveDate as EventDate1,
'RecDate' as SecondaryDate,
RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
RatioNew,
RatioOld,
'' as Fractions,
ResSecID,
wca.scmstres.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.secrc
INNER JOIN wca.rd ON wca.rd.RdID = wca.secrc.SecRcID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.scmst as scmstres ON wca.secrc.ResSecID = wca.scmstres.SecID
Where
(wca.secrc.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3))


--# 25
select distinct
'DRIP' as Levent,
wca.drip.DivID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.drip.AnnounceDate as Created,
wca.drip.Acttime as Changed,
wca.drip.ActFlag,
'DripPayDate' as PrimaryDate,
wca.drip.DripPayDate as EventDate1,
'ExDate' as SecondaryDate,
wca.pexdt.ExDate as EventDate2,
'LastDate' as TertiaryDate,
wca.drip.DripLastDate as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.drip
LEFT OUTER JOIN wca.div_my ON wca.drip.DivID = wca.div_my.DivID
INNER JOIN wca.rd ON wca.div_my.RdID = wca.rd.RdID
INNER JOIN wca.scmst ON wca.rd.SecID = wca.scmst.SecID
LEFT OUTER JOIN wca.exdt as pexdt ON wca.rd.RdID = wca.pexdt.RdID AND wca.scmst.PrimaryExchgCD = wca.pexdt.ExchgCD AND 'DIV' = wca.pexdt.EventType
Where
(wca.drip.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.rd.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) 
or wca.pexdt.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3) )


--# 26
select distinct
'Return of Capital' as Levent,
wca.rcap.RCAPID as EventID,
wca.scmst.SecID,
wca.scmst.IssID,
wca.rcap.AnnounceDate as Created,
wca.rcap.Acttime as Changed,
wca.rcap.ActFlag,
'PayDate' as PrimaryDate,
wca.rcap.CSPYDate as EventDate1,
'EffectiveDate' as SecondaryDate,
wca.rcap.EffectiveDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM wca.rcap
INNER JOIN wca.scmst ON wca.scmst.SecID = wca.rcap.SecID
Where
wca.rcap.Acttime between (select max(feeddate) from wca.tbl_opslog where seq = 3) and (select max(acttime)+0.05 from wca.tbl_opslog where seq = 3)
