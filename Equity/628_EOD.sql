--filepath=o:\Datafeed\Equity\628\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.628
--suffix=
--fileheadertext=EDI_NONDEBT_628_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\628\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
case when wca.scmst.Actflag<>'D' then '' ELSE wca.scmst.Actflag END as Actflag,
wca.scmst.SectyCD,
case 
when wca.scexh.Liststatus = 'R' then upper('A')
when wca.scexh.Liststatus = 'S' then upper('S')
when wca.scmst.statusflag = '' then upper('A')
when wca.scmst.statusflag = 'I' then upper('I')
when wca.scmst.statusflag = 'A' then upper('A')
ELSE upper('A')
end as statusflag,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ParValue,
wca.issur.Issuername,
wca.scmst.Securitydesc,
wca.scmst.IssID,
wca.issur.CntryofIncorp,
case
when wca.sedol.actflag <> 'D' then wca.sedol.Sedol
else '' end as Sedol,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.scmst.SecID,
'1' as DENOM1,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.wartm.wtexersecid is not null then wca.wartm.wtexersecid
     ELSE '' END as UnSecID,
case when wca.unscmst.secid is not null then wca.unscmst.curencd
     when wca.exscmst.secid is not null then wca.exscmst.curencd
     ELSE '' END as UnCurrency,
wca.exchg.mic as Mic,
substring(wca.scexh.Localcode,1,20) as Localcode,
wca.sedol.RcntryCD
FROM wca.scmst
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
                 and wca.scexh.exchgcd = wca.scmst.primaryexchgcd
left outer join wca.dprcp on wca.scmst.secid = wca.dprcp.secid
                 and wca.dprcp.actflag<>'D'
left outer join wca.wartm on wca.scmst.secid = wca.wartm.secid
                 and wca.wartm.actflag<>'D'
left outer join wca.scmst as unscmst on wca.dprcp.secid = wca.unscmst.secid
left outer join wca.scmst as exscmst on wca.wartm.wtexersecid = wca.exscmst.secid
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
LEFT OUTER JOIN wca.sedol ON wca.scmst.SecID = wca.sedol.SecID
                 AND wca.exchg.cntryCD = wca.sedol.CntryCD
where
wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.scmst.sectycd <> 'BND'
and (wca.scmst.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.exchg.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.scexh.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.sedol.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.dprcp.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.wartm.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1)
or wca.issur.acttime>= (select max(acttime) from wca.tbl_opslog where seq = 1))
