-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_689','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_689_
-- dfn=l

-- # 1

select
EventCD,
EventID,
Changed,
Fieldname,
NotesText
from wca2.t689_temp
where
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pffigi where accid=@accid and client.pffigi.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))))
OR
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pffigi where accid=@accid and client.pffigi.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I') and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)))))
OR
((((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfcomptk where accid=@accid and client.pfcomptk.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pffigi where accid=@accid and client.pffigi.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pffigi where accid=@accid and client.pffigi.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I') and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))));
