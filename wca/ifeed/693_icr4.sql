-- arp=n:\no_cull_feeds\693i4\
-- fsx=select concat('_693','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_693_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t693_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)

