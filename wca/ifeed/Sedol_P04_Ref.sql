-- arc=y
-- arp=n:\temp\
-- fsx=_SedolRef
-- hpx=EDI_SedolRef_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')

-- # 1
select distinct
wca.scmst.secid,
wca.scmst.primaryexchgcd,
wca.scexh.exchgcd,
case when gbp04.Sedol is null then wca.scexh.localcode
     else replace(gbp04.TIDM,'.L','')
     end as localcode,
wca.sedol.sedol,
case when gbp04.sedol is not null
     then gbp04.Currency
     when wca.sedol.curencd <> ''
     then wca.sedol.curencd
     when (wca.scmst.pvcurencd <> '' and wca.sedol.cntrycd=substring(wca.scmst.primaryexchgcd,1,2))
     then wca.scmst.pvcurencd
     else ''
     end as sedoltradingcurrency,
wca.sedol.rcntrycd as sedolregistercountry
from wca.scmst
inner join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
inner join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on wca.sedol.sedol=gbp04.Sedol
where
wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.sedol.actflag<>'D'
and (wca.scmst.statusflag<>'I' or gbp04.Sedol is not null)
and (wca.scexh.liststatus<>'D' or gbp04.Sedol is not null)
and (wca.sedol.defunct<>'T' or gbp04.Sedol is not null)
and wca.sedol.sedol<>'';
