-- arp=n:\no_cull_feeds\690i4\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
union
select * from wca2.t690_temp_prf
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;
