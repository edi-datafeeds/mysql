-- arc=y
-- arp=n:\temp\
-- fsx=_690
-- hpx=EDI_WCA_690_
-- dfn=l

-- # 1

select wca2.t690_temp.*
from wca2.t690_temp
left outer join wca.sdchg on wca2.t690_temp.eventcd = wca.sdchg.eventtype and wca2.t690_temp.eventid = wca.sdchg.releventid
left outer join wca.icc on wca2.t690_temp.eventcd = wca.icc.eventtype and wca2.t690_temp.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca.icc.oldisin in (select code from client.pfisin where accid=240 and actflag='U')
or wca.icc.olduscode in (select code from client.pfuscode where accid=240 and actflag='U')
or wca.sdchg.oldsedol in (select code from client.pfsedol where accid=240 and actflag='U')
or wca2.t690_temp.field2 in (select code from client.pfsedol where accid=240 and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca.icc.oldisin in (select code from client.pfisin where accid=240 and actflag='I')
or wca.icc.olduscode in (select code from client.pfuscode where accid=240 and actflag='I')
or wca2.t690_temp.field2 in (select code from client.pfsedol where accid=240 and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');
