-- arc=y
-- arp=n:\temp\
-- fsx=_695
-- hpx=EDI_WCA_695_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select * from wca2.t695_hist
where eventcd='BON'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='CONSD'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='BKRP'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='DIV'
and paytype ='S'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='DMRGR'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='ENT'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='LIQ'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='RTS'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='SD'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='TKOVR'
limit 10;

-- # 2
select * from wca2.t695_hist
where eventcd='SECRC'
limit 10;
