-- arc=y
-- arp=n:\temp\
-- fsx=_689
-- hpx=EDI_WCA_689_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fty=n

-- # 1

select * from wca2.t689_hist
where eventcd<>'SHOCH'
order by eventcd, eventid
