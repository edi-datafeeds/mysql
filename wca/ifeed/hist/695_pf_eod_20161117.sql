-- arc=y
-- arp=n:\temp\
-- fsx=_695
-- hpx=EDI_WCA_695_
-- dfn=l

-- # 1

select * from wca2.t695_hist
left outer join wca.icc on wca2.t695_hist.eventcd = wca.icc.eventtype and wca2.t695_hist.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca2.t695_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t695_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or wca2.t695_hist.BbgCompositeTicker in (select code from client.pfcomptk where accid=@accid and actflag='U')
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca2.t695_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t695_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or wca2.t695_hist.BbgCompositeTicker in (select code from client.pfcomptk where accid=@accid and actflag='I')
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');