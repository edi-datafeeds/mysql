-- arc=y
-- arp=n:\temp\
-- fsx=_690
-- hpx=EDI_WCA_690_
-- dfn=l

-- # 1

select distinct wca2.t690_hist.*
from wca2.t690_hist
left outer join wca.sdchg on wca2.t690_hist.eventcd = wca.sdchg.eventtype and wca2.t690_hist.eventid = wca.sdchg.releventid
left outer join wca.icc on wca2.t690_hist.eventcd = wca.icc.eventtype and wca2.t690_hist.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca2.t690_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t690_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or sedol in (select code from client.pfsedol where accid=@accid and actflag='U')
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U')
or wca.sdchg.newsedol in (select code from client.pfsedol where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca2.t690_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t690_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or sedol in (select code from client.pfsedol where accid=@accid and actflag='I') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I')
or wca.sdchg.newsedol in (select code from client.pfsedol where accid=@accid and actflag='I'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch');
