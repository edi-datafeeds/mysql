-- arc=y
-- arp=n:\temp\
-- fsx=_680
-- hpx=EDI_WCA_680_
-- dfn=l

-- # 1

select distinct wca2.t680_hist_prf.*
from wca2.t680_hist_prf
where
wca2.t680_hist_prf.secid in (select secid from client.pfsecid where accid=@accid and actflag='I')
and exchgcntry='US'
and exchgcd<>'USBND';

-- # 2

select distinct wca2.t680_temp_prf.*
from wca2.t680_temp_prf
where
wca2.t680_temp_prf.secid in (select secid from client.pfsecid where accid=@accid and actflag='I');

