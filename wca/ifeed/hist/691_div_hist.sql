-- arc=y
-- arp=n:\temp\
-- fsx=_691
-- hpx=EDI_WCA_691_
-- dfn=l

-- # 1

select * from wca2.t690_hist
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
and (sedol in (select code from client.pfsedol where accid=@accid and actflag='U')
    or uscode in (select code from client.pfuscode where accid=@accid and actflag='U')))
or (sedol in (select code from client.pfsedol where accid=@accid and actflag='I')
    or uscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
order by eventid desc;