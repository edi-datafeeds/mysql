-- arc=y
-- arp=n:\temp\
-- fsx=_695
-- hpx=EDI_WCA_695_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')

-- # 1

select * from wca2.t695_hist
where exchgcntry<>'US'
union
select * from wca2.t695_hist_fi
where exchgcntry<>'US'
order by eventcd, eventid;
