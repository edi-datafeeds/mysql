-- arc=y
-- arp=n:\temp\
-- hpx=EDI_WCA_685_
-- dfn=l
-- fex=_685.txt

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.CFI,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scmst.sharesoutstanding,
wca.scmst.sharesoutstandingdate,
wca.sedol.sedol,
wca.sedol.defunct as SedolDefunct
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.sedol on bbe.secid = wca.sedol.secid
    and wca.exchg.cntrycd=wca.sedol.cntrycd
    and bbe.curencd=wca.sedol.curencd
    and 'D'<>wca.sedol.actflag
where
(scmst.statusflag<>'I')
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'
and (cntrycd='BW'
  or cntrycd='CI'
  or cntrycd='GH'
  or cntrycd='KE'
  or cntrycd='MU'
  or cntrycd='MW'
  or cntrycd='NG'
  or cntrycd='TZ'
  or cntrycd='UG'
  or cntrycd='ZM'
  or cntrycd='ZW')
