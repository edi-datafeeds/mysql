-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_680
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'agm' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 2
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ann'
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 3
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'arr' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 4
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'assm' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 5
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'bb' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 6
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'bkrp' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 7
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'bon' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 8
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'br' 
#and date6 is not null
and ifnull(date6,'2000-01-01')>=@fromdate and ifnull(date6,'2000-01-01') <=@todate;

-- # 9
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'call' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 10
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'caprd' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 11
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'consd' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 12
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'conv' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 13
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ctx' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 14
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'currd' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 15
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'dist' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 16
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'div' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 17
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'dmrgr' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 18
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'drip' 
#and date4 is not null
and ifnull(date4,'2000-01-01')>=@fromdate and ifnull(date4,'2000-01-01') <=@todate;

-- # 19
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'dvst' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 20
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ent' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 21
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'frank' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 22
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ftt' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 23
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'fychg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 24
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'icc' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 25
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'inchg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 26
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ischg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 27
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'lawst' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 28
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'lcc' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 29
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'liq' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 30
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'lstat' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 31
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'ltchg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 32
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'mkchg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 33
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'mrgr' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 34
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'nlist' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 35
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'oddlt' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 36
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'pid' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 37
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'po' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 38
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'prchg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 39
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'prf' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 40
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'pvrd' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 41
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'rcap' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 42
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'redem' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 43
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'rts' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 44
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'scchg' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 45
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'scswp' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 46
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'sd' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 47
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'secrc' 
#and date1 is not null
and ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate;

-- # 48
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'tkovr' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;

-- # 49
SELECT * from wca2.t680_hist
where
isin in (select mycode from portfolio.scott_isin)
and 
eventcd = 'idiv' 
#and date2 is not null
and ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate;
