-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.695
-- filenamesuffix=select '_2014_DIV_DRIP_'
-- headerprefix=EDI_WCA_695_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\temp\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # big 1
select
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
case when wca.divpy.OptionSerialNo is null then '1' else wca.divpy.OptionSerialNo end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (divpy.acttime > vtab.acttime) 
      and (divpy.acttime > rd.acttime) 
      and (divpy.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (divpy.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then divpy.acttime 
     when (rd.acttime > vtab.acttime) 
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Paydate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Paydate2 
     ELSE null END as Date4,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'Exdate2' as Date11Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN wca.exdt.Exdate2 
     WHEN pexdt.rdid IS NOT NULL and wca.divpy.divtype='B'
     THEN pexdt.Exdate2 
     ELSE null END as Date11,
'FXDate' as Date12Type,
wca.divpy.FXDate as Date12,
ifnull(wca.divpy.Divtype,'') as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
substring(wca.divpy.ratioold,1,instr(wca.divpy.ratioold,'.')+7) as RatioOld,
substring(wca.divpy.rationew,1,instr(wca.divpy.rationew,'.')+7) as RatioNew,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'Dapflag' as Field12Name,
case when wca.prvsc.secid is not null then 'Y' else 'N' end as Dapflag,
'InstallmentPayDate' as Field13Name,
wca.divpy.InstallmentPayDate as Field13,
'DeclCurenCD' as Field14Name,
vtab.declcurencd as Field14,
'DeclGrossAmt' as Field15Name,
vtab.declgrossamt as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
Where
stab.isin in (select code from client.pfisin where accid=995)
and vtab.acttime>'2015/01/01'
order by vtab.EventID;

-- # big 2
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
case when (div_my.acttime > vtab.acttime)
      and (div_my.acttime > rd.acttime)
      and (div_my.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (div_my.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then div_my.acttime 
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'DripLastdate' as Date1Type,
vtab.DripLastdate Date1,
'DripPaydate' as Date2Type,
vtab.DripPaydate as Date2,
'CrestDate' as Date3Type,
vtab.CrestDate as Date3,
'ExDate' as Date4Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'DripReinvPrice' as Field1Name,
vtab.DripReinvPrice as Field1,
'CntryCD' as Field2Name,
vtab.CntryCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_drip as vtab
inner join wca.div_my on vtab.divid=wca.div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on wca.div_my.rdid=wca.rdprt.rdid and 'DIV' = wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.CntryCD and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.ExchgCD
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and 'DIV' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
Where
stab.isin in (select code from client.pfisin where accid=995)
and wca.div_my.acttime>'2015/01/01'
order by vtab.EventID;
