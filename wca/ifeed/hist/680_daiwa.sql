-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_680
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE


-- # 16
SELECT * from wca2.t680_hist
where 
eventcd = 'idiv' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or ifnull(date11,'2001-01-01')>=@fromdate 
     or (date4 >=@fromdate and date4<=@todate)
     or (date3 >=@fromdate and date3<=@todate)
     or (date2 >=@fromdate and date2<=@todate))

-- # 1
SELECT * from wca2.t680_hist
where 
eventcd = 'agm' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and date1 is not null
and date1>=@fromdate and date1 <='2017-12-24'

-- # 3
SELECT * from wca2.t680_hist
where 
eventcd = 'arr' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))
     
-- # 4
SELECT * from wca2.t680_hist
where 
eventcd = 'assm' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate

-- # 5
SELECT * from wca2.t680_hist
where 
eventcd = 'bb' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date2,'2001-01-01')>=@fromdate 
     or (date3 >=@fromdate and date3<=@todate)
     or (date4 >=@fromdate and date4<=@todate))

-- # 6
SELECT * from wca2.t680_hist
where 
eventcd = 'bkrp' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 7
SELECT * from wca2.t680_hist
where 
eventcd = 'bon' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))
     
-- # 8
SELECT * from wca2.t680_hist
where 
eventcd = 'br' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date6,'2001-01-01')>=@fromdate 
     or (date7 >=@fromdate and date7<=@todate)
     or (date8 >=@fromdate and date8<=@todate))

-- # 9
SELECT * from wca2.t680_hist
where 
eventcd = 'call' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 10
SELECT * from wca2.t680_hist
where 
eventcd = 'caprd' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 11
SELECT * from wca2.t680_hist
where 
eventcd = 'consd' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 12
SELECT * from wca2.t680_hist
where 
eventcd = 'conv' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date2,'2001-01-01')>=@fromdate 

-- # 13
SELECT * from wca2.t680_hist
where 
eventcd = 'ctx' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date2,'2001-01-01')>=@fromdate 

-- # 14
SELECT * from wca2.t680_hist
where 
eventcd = 'currd' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 15
SELECT * from wca2.t680_hist
where 
eventcd = 'dist' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 16
SELECT * from wca2.t680_hist
where 
eventcd = 'div' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or ifnull(date11,'2001-01-01')>=@fromdate 
     or (date4 >=@fromdate and date2<=@todate)
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 17
SELECT * from wca2.t680_hist
where 
eventcd = 'dmrgr' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 18
SELECT * from wca2.t680_hist
where 
eventcd = 'drip' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date4,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date1 >=@fromdate and date1<=@todate))

-- # 19
SELECT * from wca2.t680_hist
where 
eventcd = 'dvst' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 20
SELECT * from wca2.t680_hist
where 
eventcd = 'ent' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 21
SELECT * from wca2.t680_hist
where 
eventcd = 'frank' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 22
SELECT * from wca2.t680_hist
where 
eventcd = 'ftt' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date2,'2001-01-01')>=@fromdate 

-- # 23
SELECT * from wca2.t680_hist
where 
eventcd = 'fychg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 24
SELECT * from wca2.t680_hist
where 
eventcd = 'icc' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 25
SELECT * from wca2.t680_hist
where 
eventcd = 'inchg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 26
SELECT * from wca2.t680_hist
where 
eventcd = 'ischg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 27
SELECT * from wca2.t680_hist
where 
eventcd = 'lawst' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 28
SELECT * from wca2.t680_hist
where 
eventcd = 'lcc' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 29
SELECT * from wca2.t680_hist
where 
eventcd = 'liq' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 30
SELECT * from wca2.t680_hist
where 
eventcd = 'lstat' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 31
SELECT * from wca2.t680_hist
where 
eventcd = 'ltchg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 32
SELECT * from wca2.t680_hist
where 
eventcd = 'mkchg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 33
SELECT * from wca2.t680_hist
where 
eventcd = 'mrgr' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 34
SELECT * from wca2.t680_hist
where 
eventcd = 'nlist' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 35
SELECT * from wca2.t680_hist
where 
eventcd = 'oddlt' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date2,'2001-01-01')>=@fromdate 

-- # 36
SELECT * from wca2.t680_hist
where 
eventcd = 'pid' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 37
SELECT * from wca2.t680_hist
where 
eventcd = 'po' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 38
SELECT * from wca2.t680_hist
where 
eventcd = 'prchg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 39
SELECT * from wca2.t680_hist
where 
eventcd = 'prf' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 40
SELECT * from wca2.t680_hist
where 
eventcd = 'pvrd' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 41
SELECT * from wca2.t680_hist
where 
eventcd = 'rcap' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 42
SELECT * from wca2.t680_hist
where 
eventcd = 'redem' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 43
SELECT * from wca2.t680_hist
where 
eventcd = 'rts' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 44
SELECT * from wca2.t680_hist
where 
eventcd = 'scchg' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 45
SELECT * from wca2.t680_hist
where 
eventcd = 'scswp' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 46
SELECT * from wca2.t680_hist
where 
eventcd = 'sd' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and (ifnull(date1,'2001-01-01')>=@fromdate 
     or (date2 >=@fromdate and date2<=@todate)
     or (date3 >=@fromdate and date3<=@todate))

-- # 47
SELECT * from wca2.t680_hist
where 
eventcd = 'secrc' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date1,'2001-01-01')>=@fromdate 

-- # 48
SELECT * from wca2.t680_hist
where 
eventcd = 'tkovr' 
and isin in (select code from client.pfisin where accid = 995 and client.pfisin.actflag<>'D')
and ifnull(date2,'2001-01-01')>=@fromdate 
