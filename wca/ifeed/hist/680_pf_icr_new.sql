-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- fty=y

-- # 1

select * from wca2.t680_hist
left outer join wca.icc on wca2.t680_hist.eventcd = wca.icc.eventtype and wca2.t680_hist.eventid = wca.icc.releventid
where
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca2.t680_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t680_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca2.t680_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t680_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');
