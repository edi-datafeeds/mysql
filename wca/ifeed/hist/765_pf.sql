--filepath=o:\prodman\salesfeeds\wca\765\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_765
--fileheadertext=EDI_WCA_765_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=HH:MM:SS
--ForceTime=n

--# 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scmst.acttime, '%Y/%m/%d %H:%i:%s')
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scexh.acttime, '%Y/%m/%d %H:%i:%s')
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(issur.acttime, '%Y/%m/%d %H:%i:%s')
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(mktsg.acttime, '%Y/%m/%d %H:%i:%s')
     else date_format(exchg.acttime, '%Y/%m/%d %H:%i:%s')
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
client.pfisin.code as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scmst.SharesOutstanding,
wca.scmst.sharesoutstandingdate as EffectiveDate
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
where client.pfisin.accid = 999
and wca.scmst.sectycd<>'BND'
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D';