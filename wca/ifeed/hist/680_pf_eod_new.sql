-- arc=y
-- arp=n:\temp\
-- fsx=_680
-- hpx=EDI_WCA_680_
-- dfn=l

-- # 1

select distinct wca2.t680_hist.*
from wca2.t680_hist
left outer join wca.icc on wca2.t680_hist.eventcd = wca.icc.eventtype and wca2.t680_hist.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca2.t680_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t680_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca2.t680_hist.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t680_hist.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or concat(wca2.t680_hist.localcode, wca2.t680_hist.mic) in (select concat(client.pftickmic.code, client.pftickmic.mic) from client.pftickmic where accid=@accid and actflag='I')
or concat(wca2.t680_hist.isin, wca2.t680_hist.mic) in (select concat(client.pfisinmic.code, client.pfisinmic.mic) from client.pfisinmic where accid=@accid and actflag='I')
or wca2.t680_hist.secid in (select secid from client.pfsecid where accid=@accid and actflag='I') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');