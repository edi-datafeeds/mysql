-- arc=y
-- arp=n:\temp\
-- fsx=_689
-- hpx=EDI_WCA_689_
-- dfn=l

-- # 1

select eventcd,
eventid,
changed,
fieldname,
notestext
from wca2.t689_hist
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))))
OR
((((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfisin where accid=@accid and client.pfisin.actflag='I'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I')))
OR
((eventcd in ('BKRP','LAWST','LIQ') and pfid in (select issid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I'))
or (eventcd not in ('BKRP','LAWST','LIQ') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I')))))
and eventcd = 'div'
order by eventid desc;