-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_680
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT * from wca2.t680_hist
where 
eventcd = 'agm' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'agm' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 2
SELECT * from wca2.t680_hist
where 
eventcd = 'ann' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ann' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 3
SELECT * from wca2.t680_hist
where 
eventcd = 'arr' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'arr' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 4
SELECT * from wca2.t680_hist
where 
eventcd = 'assm'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UBION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'assm'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 5
SELECT * from wca2.t680_hist
where 
eventcd = 'bb' 
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'bb' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 6
SELECT * from wca2.t680_hist
where 
eventcd = 'bkrp'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'bkrp'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 7
SELECT * from wca2.t680_hist
where 
eventcd = 'bon' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'bon' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 8
SELECT * from wca2.t680_hist
where 
eventcd = 'br' 
and exchgcntry='US'
and date6 is not null
and date6>=@fromdate and date6 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'br' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date6 is not null
and date6>=@fromdate and date6 <=@todate;

-- # 9
SELECT * from wca2.t680_hist
where 
eventcd = 'call' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'call' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 10
SELECT * from wca2.t680_hist
where 
eventcd = 'caprd' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'caprd' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 11
SELECT * from wca2.t680_hist
where 
eventcd = 'consd'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'consd'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 12
SELECT * from wca2.t680_hist
where 
eventcd = 'conv' 
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'conv' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 13
SELECT * from wca2.t680_hist
where 
eventcd = 'ctx' 
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ctx' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 14
SELECT * from wca2.t680_hist
where 
eventcd = 'currd' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'currd' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 15
SELECT * from wca2.t680_hist
where 
eventcd = 'dist'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'dist'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 16
SELECT * from wca2.t680_hist
where 
eventcd = 'div'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'div'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 17
SELECT * from wca2.t680_hist
where 
eventcd = 'dmrgr' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'dmrgr' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 18
SELECT * from wca2.t680_hist
where 
eventcd = 'drip' 
and exchgcntry='US'
and date4 is not null
and date4>=@fromdate and date4 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'drip' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date4 is not null
and date4>=@fromdate and date4 <=@todate;

-- # 19
SELECT * from wca2.t680_hist
where 
eventcd = 'dvst' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'dvst' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 20
SELECT * from wca2.t680_hist
where 
eventcd = 'ent'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ent'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 21
SELECT * from wca2.t680_hist
where 
eventcd = 'frank' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'frank' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 22
SELECT * from wca2.t680_hist
where 
eventcd = 'ftt' 
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ftt' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 23
SELECT * from wca2.t680_hist
where 
eventcd = 'fychg'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'fychg'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 24
SELECT * from wca2.t680_hist
where 
eventcd = 'icc' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'icc' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 25
SELECT * from wca2.t680_hist
where 
eventcd = 'inchg' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'inchg' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 26
SELECT * from wca2.t680_hist
where 
eventcd = 'ischg'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ischg'
and exchgcntry='US
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 27
SELECT * from wca2.t680_hist
where 
eventcd = 'lawst'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'lawst'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 28
SELECT * from wca2.t680_hist
where 
eventcd = 'lcc'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'lcc'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 29
SELECT * from wca2.t680_hist
where 
eventcd = 'liq'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'liq'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 30
SELECT * from wca2.t680_hist
where 
eventcd = 'lstat' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'lstat' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 31
SELECT * from wca2.t680_hist
where 
eventcd = 'ltchg' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'ltchg' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 32
SELECT * from wca2.t680_hist
where 
eventcd = 'mkchg'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'mkchg'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 33
SELECT * from wca2.t680_hist
where 
eventcd = 'mrgr'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'mrgr'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 34
SELECT * from wca2.t680_hist
where 
eventcd = 'nlist' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'nlist' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 35
SELECT * from wca2.t680_hist
where 
eventcd = 'oddlt' 
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'oddlt' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 36
SELECT * from wca2.t680_hist
where 
eventcd = 'pid' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'pid' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 37
SELECT * from wca2.t680_hist
where 
eventcd = 'po' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'po' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 38
SELECT * from wca2.t680_hist
where 
eventcd = 'prchg'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'prchg'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 39
SELECT * from wca2.t680_hist
where 
eventcd = 'prf' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNIOM
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'prf' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 40
SELECT * from wca2.t680_hist
where 
eventcd = 'pvrd'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'pvrd'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 41
SELECT * from wca2.t680_hist
where 
eventcd = 'rcap' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'rcap' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 42
SELECT * from wca2.t680_hist
where 
eventcd = 'redem'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'redem'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 43
SELECT * from wca2.t680_hist
where 
eventcd = 'rts'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'rts'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 44
SELECT * from wca2.t680_hist
where 
eventcd = 'scchg'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'scchg'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 45
SELECT * from wca2.t680_hist
where 
eventcd = 'scswp'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'scswp'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 46
SELECT * from wca2.t680_hist
where 
eventcd = 'sd' 
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'sd' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 47
SELECT * from wca2.t680_hist
where 
eventcd = 'secrc'
and exchgcntry='US'
and date1 is not null
and date1>=@fromdate and date1 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'secrc'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date1 is not null
and date1>=@fromdate and date1 <=@todate;

-- # 48
SELECT * from wca2.t680_hist
where 
eventcd = 'tkovr'
and exchgcntry='US'
and date2 is not null
and date2>=@fromdate and date2 <=@todate
UNION
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'tkovr'
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

-- # 49
SELECT * from wca2.t680_hist_fi
where 
eventcd = 'idiv' 
and exchgcntry='US'
and exchgcd<>'USTRCE'
and exchgcd<>'USBND'
and date2 is not null
and date2>=@fromdate and date2 <=@todate;

