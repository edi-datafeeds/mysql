use prices;
drop table if exists prices.indus_intel_temp;
CREATE TABLE `prices`.`indus_intel_temp`(
  `MIC` varchar(6) DEFAULT NULL,
  `LocalCode` varchar(80) NOT NULL DEFAULT '',
  `Isin` varchar(12) DEFAULT NULL,
  `Currency` char(3) NOT NULL DEFAULT '',
  `PriceDate` date DEFAULT NULL,
  `Open` varchar(20) DEFAULT NULL,
  `High` varchar(20) DEFAULT NULL,
  `Low` varchar(20) DEFAULT NULL,
  `Close` varchar(20) DEFAULT NULL,
  `Mid` varchar(20) DEFAULT NULL,
  `Ask` varchar(20) DEFAULT NULL,
  `Last` varchar(20) DEFAULT NULL,
  `Bid` varchar(20) DEFAULT NULL,
  `BidSize` varchar(20) DEFAULT NULL,
  `AskSize` varchar(20) DEFAULT NULL,
  `TradedVolume` varchar(50) DEFAULT NULL,
  `SecID` int(11) NOT NULL DEFAULT '0',
  `MktCloseDate` date DEFAULT NULL,
  `Volflag` char(1) DEFAULT NULL,
  `Issuername` varchar(70) DEFAULT NULL,
  `SectyCD` char(3) DEFAULT NULL,
  `SecurityDesc` varchar(100) DEFAULT NULL,
  `Sedol` varchar(7) DEFAULT NULL,
  `uscode` varchar(9) DEFAULT NULL,
  `PrimaryExchgCD` varchar(6) DEFAULT NULL,
  `ExchgCD` varchar(9) NOT NULL,
  `TradedValue` varchar(50) DEFAULT NULL,
  `TotalTrades` varchar(50) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ExchgCD`,`SecID`,`Currency`,`LocalCode`) USING BTREE,
  KEY `IX_LocalCode` (`MIC`,`LocalCode`),
  KEY `IX_SECID` (`SecID`),
  KEY `IX_Sedol` (`Sedol`),
  KEY `IX_ISIN` (`Isin`) USING BTREE,
  KEY `IX_Localcode_exch` (`LocalCode`,`ExchgCD`),
  KEY `IX_Uscode` (`uscode`),
  KEY `IX_PrimaryExchgCD` (`PrimaryExchgCD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
insert ignore into prices.indus_intel_temp
select distinct
client.pftickmic.mic as MIC,
client.pftickmic.code as LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode and client.pftickmic.mic = prices.lasttrade.mic
where 
client.pftickmic.accid=267
and client.pftickmic.actflag<>'D'
and (comment not like 'terms%' or comment is null)
and substring(prices.lasttrade.exchgcd,1,2)<>'US'
and concat(client.pftickmic.code, client.pftickmic.mic) not in (select concat(localcode, mic) from prices.indus_intel_temp)
and (prices.lasttrade.secid is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1))
and (prices.lasttrade.secid is null or prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1))
and (prices.lasttrade.localcode is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1));
insert ignore into prices.indus_intel_temp
select distinct
client.pftickmic.mic as MIC,
client.pftickmic.code as LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
prices.lasttrade.Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode and client.pftickmic.mic = prices.lasttrade.mic
where 
client.pftickmic.accid=267
and client.pftickmic.actflag<>'D'
and (comment not like 'terms%' or comment is null)
and substring(prices.lasttrade.exchgcd,1,2)='US'
and concat(client.pftickmic.code, client.pftickmic.mic) not in (select concat(localcode, mic) from prices.indus_intel_temp)
and (prices.lasttrade.secid is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency desc limit 1))
and (prices.lasttrade.secid is null or prices.lasttrade.secid=
(select secid from prices.lasttrade as latest
where
latest.isin=prices.lasttrade.isin
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency desc limit 1))
and (prices.lasttrade.localcode is null or prices.lasttrade.currency=
(select currency from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
order by pricedate desc, latest.currency limit 1));
insert ignore into prices.indus_intel_temp
select distinct
client.pftickmic.mic as MIC,
client.pftickmic.code as LocalCode,
wca.scmst.Isin,
'' as Currency,
null as PriceDate,
'' as Open,
'' as High,
'' as Low,
'' as Close,
'' as Mid,
'' as Ask,
'' as Last,
'' as Bid,
'' as BidSize,
'' as AskSize,
'' as TradedVolume,
wca.scmst.SecID,
null as MktCloseDate,
'' as Volflag,
wca.issur.Issuername,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
'' as Sedol,
wca.scmst.USCode,
wca.scmst.PrimaryExchgCD,
'' as ExchgCD,
'' as TradedValue,
'' as TotalTrades,
case when wca.scexh.localcode<>''
     then concat('Possible New Ticker:', wca.scexh.localcode)
	 else ''
     end as Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode and client.pftickmic.mic = prices.lasttrade.mic
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
left outer join wca.scexh on prices.lasttrade.secid = wca.scexh.secid and prices.lasttrade.exchgcd = wca.scexh.exchgcd
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
where 
client.pftickmic.accid=267
and client.pftickmic.actflag<>'D'
and (comment not like 'terms%' or comment is null)
and concat(client.pftickmic.code, client.pftickmic.mic) not in (select concat(localcode, mic) from prices.indus_intel_temp);
