-- arc=y
-- arp=n:\upload\acc\306\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fsx=_P04
-- fty=y
-- fex=.txt
-- dfn=L
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=
-- hsx=_ShareCompany_P04

-- # 1
select distinct
client.pfisinmic.mic as MIC,
prices.liveprices.localcode as LocalCode,
client.pfisinmic.code as Isin,
prices.liveprices.Currency,
prices.liveprices.PriceDate,
prices.liveprices.Open,
prices.liveprices.High,
prices.liveprices.Low,
prices.liveprices.Close,
prices.liveprices.Mid,
prices.liveprices.Ask,
prices.liveprices.Last,
prices.liveprices.Bid,
prices.liveprices.BidSize,
prices.liveprices.AskSize,
prices.liveprices.TradedVolume,
prices.liveprices.SecID,
prices.liveprices.MktCloseDate,
prices.liveprices.Volflag,
prices.liveprices.Issuername,
prices.liveprices.SectyCD,
prices.liveprices.SecurityDesc,
prices.liveprices.Sedol,
prices.liveprices.uscode,
prices.liveprices.PrimaryExchgCD,
prices.liveprices.ExchgCD,
prices.liveprices.TradedValue,
prices.liveprices.TotalTrades,
prices.liveprices.Comment
from client.pfisinmic
left outer join prices.liveprices on client.pfisinmic.code=prices.liveprices.isin and client.pfisinmic.mic = prices.liveprices.mic
where
((pricedate is null or
(prices.liveprices.pricedate>(select(adddate(prices.liveprices.mktclosedate, interval -365 day)))))
and accid=306
and client.pfisinmic.actflag<>'D'
and (comment not like 'terms%' or comment is null))