-- arc=y
-- arp=n:\temp\
-- fsx=_Xignite_SRF_HST
-- hpx=EDI_Xignite_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
'' as SedolDefunct,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
case when ifnull(prices.lasttrade.localcode,'')<>''
     then prices.lasttrade.localcode
     when wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scexh.listdate,
wca.ipo.IPOStatus,
wca.ipo.FirstTradingDate,
smf4.security.OPOL
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join prices.lasttrade on wca.scexh.secid=prices.lasttrade.secid 
                        and sedolbbg.exchgcd=prices.lasttrade.exchgcd
						and substring(sedolbbg.curencd,1,2)=substring(prices.lasttrade.currency,1,2)
                        and sedolbbg.sedol=prices.lasttrade.sedol
                        and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 750 day))
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 92 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
where 
wca.scexh.exchgcd='GBLSE'
and wca.scexh.liststatus<>'D'
and wca.scmst.statusflag<>'I'
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'
and (prices.lasttrade.currency is null or prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latestprice
where
latestprice.exchgcd=wca.scexh.exchgcd
and wca.scexh.liststatus<>'D'
and latestprice.secid=prices.lasttrade.secid
and latestprice.currency=prices.lasttrade.currency
and (latestprice.sedol=prices.lasttrade.sedol)
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 750 day))
order by latestprice.currency, pricedate desc limit 1))
UNION
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
'' as SedolDefunct,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
CASE when (wca.exchg.cntrycd<>'US' or (wca.scexh.exchgcd='USOTC' or wca.scexh.exchgcd='USTRCE' or wca.scexh.exchgcd='USBND')) and wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     when wca.scexh.LocalCode='BPRAP'
     then wca.scexh.LocalCode
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2),'PR','/PR'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),'/PR/CL')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3),'PR','/PR/'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4),'PR','/PR/')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-5),'/PR/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,1),'/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WS/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/WS')
     when wca.scmst.sectycd='TRT' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/RT')
     when wca.scmst.sectycd='TRT' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/RT')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and securitydesc like '%Class A%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'A'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/A')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and securitydesc like '%Class B%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'B'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/B')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WI')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WD')
     else wca.scexh.LocalCode
     end as LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scexh.listdate,
wca.ipo.IPOStatus,
wca.ipo.FirstTradingDate,
smf4.security.OPOL
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 92 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
where 
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and wca.exchg.cntrycd='US' and wca.exchg.exchgcd<>'USOTC')
                           or wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
                           or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and wca.exchg.cntrycd<>'GB'
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D' and exchg.actflag<>'D'
and wca.scexh.liststatus<>'D' and wca.scmst.statusflag<>'I'
and substring(wca.exchg.exchgcd,3,3)<>'BND'
and wca.exchg.exchgcd<>'ALTSE'
and wca.exchg.exchgcd<>'AOASE'
and wca.exchg.exchgcd<>'AROEX'
and wca.exchg.exchgcd<>'ARRCX'
and wca.exchg.exchgcd<>'AUSIMV'
and wca.exchg.exchgcd<>'CHSCH'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'CNSZHK'
and wca.exchg.exchgcd<>'DESCH'
and wca.exchg.exchgcd<>'DODRSX'
and wca.exchg.exchgcd<>'FINDX'
and wca.exchg.exchgcd<>'FOFSM'
and wca.exchg.exchgcd<>'GABVCA'
and wca.exchg.exchgcd<>'GIGSX'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'HNCASE'
and wca.exchg.exchgcd<>'HTHTSE'
and wca.exchg.exchgcd<>'JPNSE'
and wca.exchg.exchgcd<>'KNECSE'
and wca.exchg.exchgcd<>'LSMSM'
and wca.exchg.exchgcd<>'LUUL'
and wca.exchg.exchgcd<>'MVMSX'
and wca.exchg.exchgcd<>'MYLFX'
and wca.exchg.exchgcd<>'MZMSX'
and wca.exchg.exchgcd<>'NOOTC'
and wca.exchg.exchgcd<>'PLOTC'
and wca.exchg.exchgcd<>'SENDX'
and wca.exchg.exchgcd<>'TJCASE'
and wca.exchg.exchgcd<>'UAINX'
and wca.exchg.exchgcd<>'UAKISE'
and wca.exchg.exchgcd<>'USTRCE'
and wca.exchg.exchgcd<>'UYUEX';
