-- arp=n:\no_cull_feeds\689\
-- hpx=EDI_WCA_689_
-- dfn=l
-- fex=.689
-- fty=y

-- # 1

select EventCD,
EventID,
Fieldname,
Notestext 
from wca2.t689_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'