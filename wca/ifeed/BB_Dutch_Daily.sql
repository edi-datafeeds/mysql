-- arp=n:\temp
-- fsx=select '_BB_Dutch'
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- hpx=EDI_WCA_BB_Dutch_
-- dfn=l
-- fty=y

-- # 1
select 
wca.bb.acttime,
wca.bb.actflag,
'BB' as EventCD,
wca.bb.bbid as EventID,
wca.mpay.dutchauction
from wca.bb
inner join wca.mpay on bb.bbid=wca.mpay.eventid and 'BB'=wca.mpay.sevent
where
wca.mpay.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3);

