-- arp=n:\no_cull_feeds\696\
-- hpx=EDI_WCA_696_
-- dfn=l
-- fex=_696.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'DIV'