-- arp=n:\no_cull_feeds\692\
-- hpx=EDI_WCA_692_
-- dfn=l
-- fex=_692.txt
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'SHOCH'
