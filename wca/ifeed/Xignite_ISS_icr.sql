-- arc=n
-- arp=n:\temp\
-- hpx=EDI_Xignite_ISS_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_Xignite_ISS_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
select distinct
wca.issur.IssID,
wca.issur.Actflag,
wca.issur.acttime as Changed,
wca.issur.AnnounceDate as Created,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK
from wca.issur
where 
wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog);

