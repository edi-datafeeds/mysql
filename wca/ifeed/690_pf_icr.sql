-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select wca2.t690_temp.*
from wca2.t690_temp
left outer join wca.sdchg on wca2.t690_temp.eventcd = wca.sdchg.eventtype and wca2.t690_temp.eventid = wca.sdchg.releventid
left outer join wca.icc on wca2.t690_temp.eventcd = wca.icc.eventtype and wca2.t690_temp.eventid = wca.icc.releventid
where
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca2.t690_temp.isin in (select code from client.pfisin where accid=@accid and actflag='U') 
or wca2.t690_temp.uscode in (select code from client.pfuscode where accid=@accid and actflag='U') 
or sedol in (select code from client.pfsedol where accid=@accid and actflag='U')
or wca.icc.oldisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.olduscode in (select code from client.pfuscode where accid=@accid and actflag='U')
or wca.sdchg.oldsedol in (select code from client.pfsedol where accid=@accid and actflag='U')
or wca2.t690_temp.field2 in (select code from client.pfsedol where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca2.t690_temp.isin in (select code from client.pfisin where accid=@accid and actflag='I') 
or wca2.t690_temp.uscode in (select code from client.pfuscode where accid=@accid and actflag='I') 
or sedol in (select code from client.pfsedol where accid=@accid and actflag='I')
or wca.icc.oldisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.olduscode in (select code from client.pfuscode where accid=@accid and actflag='I')
or wca.sdchg.oldsedol in (select code from client.pfsedol where accid=@accid and actflag='I')
or wca2.t690_temp.field2 in (select code from client.pfsedol where accid=@accid and actflag='I'))
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1) and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((wca2.t690_temp.isin in (select code from client.pfisin where accid=@accid and actflag='I') 
or wca2.t690_temp.uscode in (select code from client.pfuscode where accid=@accid and actflag='I') 
or sedol in (select code from client.pfsedol where accid=@accid and actflag='I') 
or wca.icc.oldisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.olduscode in (select code from client.pfuscode where accid=@accid and actflag='I')
or wca2.t690_temp.field2 in (select code from client.pfsedol where accid=@accid and actflag='I'))
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1) and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');
