-- arc=y
-- arp=n:\temp\
-- fsx=_XTX_SRF_HST
-- hpx=EDI_XTX_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select distinct
wca.scmst.SecID,
wca.scexh.ExchgCD,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when (wca.scmst.acttime > scexh.acttime)
      and (wca.scmst.acttime > issur.acttime)
      and (wca.scmst.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (wca.scexh.acttime > issur.acttime)
      and (wca.scexh.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (wca.issur.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(wca.mktsg.acttime,'2000-01-01') > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.mktsg.acttime
     else wca.exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as USCode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.LEI,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(bbg.curencd,'') as BbgTradingCurrency,
ifnull(bbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(bbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(bbg.bbgexhid,'') as BbgGlobalID,
ifnull(bbg.bbgexhtk,'') as BbgExchangeTicker,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
wca.scexh.localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.bbgseq as bbg on wca.scexh.secid=bbg.secid and wca.scexh.exchgcd = bbg.exchgcd
left outer join wca2.bbcseq as bbcseq on bbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on bbg.bbeid=bbeseq.bbeid
where 
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and wca.exchg.cntrycd='US' and (wca.scexh.exchgcd='USNYSE' or scexh.exchgcd='USNASD' or scexh.exchgcd='USAMEX' or scexh.exchgcd='USPAC' or scexh.exchgcd='USBATS')) or wca.sectygrp.secgrpid is not null)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>'2019/01/29')
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D';
