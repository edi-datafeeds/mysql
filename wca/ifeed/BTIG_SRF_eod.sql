-- arc=y
-- arp=n:\temp\
-- fsx=_SEDOLBBG2_SRF_EOD
-- hpx=EDI_SEDOLBBG2_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')

-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (sedolbbg.bbeacttime)
     and ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (sedolbbg.bbcacttime)
     and ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.sedolacttime
     when ifnull(sedolbbg.bbeacttime,'2001/01/01') >= (sedolbbg.bbeacttime)
     and ifnull(sedolbbg.bbeacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.bbeacttime
     when ifnull(sedolbbg.bbcacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.bbeacttime
     else wca.scexh.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as SedolRegCntry,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when ifnull(gbp04.TIDM,'')<>''
     then replace(gbp04.TIDM,'.L','')
     when wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.scexh.LocalCode as Localcode680,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L')
     else wca.scexh.ListStatus
     end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join prices.lse_gb as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
where
(3>wca.sectygrp.secgrpid
or ((wca.scmst.sectycd='BND'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and (wca.exchg.cntrycd='US' and wca.scexh.exchgcd<>'USBND')))
and (ifnull(sedolbbg.sedolacttime,'2001/01/01') > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or ifnull(sedolbbg.bbeacttime,'2001/01/01') > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or ifnull(sedolbbg.bbcacttime,'2001/01/01') > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog where seq=3)
           or wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog where seq=3));
     