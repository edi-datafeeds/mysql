-- arc=y
-- arp=n:\wca\divrc\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_DIVRC
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_Dividend_Reclassification_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select
UPPER('DIVRC') AS EventCD,
vtab.EventID,
ifnull(wca2.sedolseq.Seqnumreg,1) as Seqnumreg,
ifnull(wca2.sedolseq.Seqnumcur,1) as Seqnumcur,
wca.scexh.ScexhID,
wca2.sedolseq.SedolID,
vtab.ActFlag,
case when (wca.frank.acttime > vtab.acttime) 
      and (wca.frank.acttime > wca.rd.acttime) 
      and (wca.frank.acttime > ifnull(wca.exdt.acttime,'2000-01-01')) 
      and (wca.frank.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then wca.frank.acttime 
     when (wca.rd.acttime > vtab.acttime) 
      and (wca.rd.acttime > ifnull(wca.exdt.acttime,'2000-01-01')) 
      and (wca.rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then wca.rd.acttime 
     when (ifnull(wca.exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(wca.exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(wca.exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.USCode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when ifnull(wca.sedol.defunct, '')=''
     then ''
     else ifnull(wca.sedol.Sedol,'')
     end as Sedol,
case when ifnull(wca.sedol.defunct, 'T')
     then ''
     else ifnull(wca.sedol.CurenCD,'')
     end as SedolTraCurrency,
ifnull(wca.sedol.defunct, '') as Defunct,
case when ifnull(wca.sedol.defunct, 'T')
     then ''
     else ifnull(wca.sedol.rcntrycd,'')
     end as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.ExDate
     ELSE pexdt.exdate END as Exdate,
rd.Recdate as Recdate,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN wca.exdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and (wca.divpy.divtype='B' or wca.divpy.divtype='C')
     THEN pexdt.Paydate
     ELSE pexdt.Paydate END as Paydate,
vtab.FYEDate,
vtab.PeriodEndDate,
wca.divpy.OptElectionDate,
rd.ToDate,
rd.RegistrationDate,
vtab.DeclarationDate,
wca.divpy.FXDate,
case when wca.exdt.rdid is not null 
     then wca.exdt.DueBillsRedemDate
     else wca.pexdt.DueBillsRedemDate
     end as DueBillsRedemDate,
wca.divpy.InstallmentPayDate,
wca.frank.TaxYearEndDate,
wca.divpy.CurenCD as DivCurenCD,
wca.divpy.GrossDividend,
wca.divpy.NetDividend,
vtab.Marker,
vtab.Frequency,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
wca.divpy.TaxRate,
wca.divpy.Depfees,
wca.divpy.Coupon,
case when wca.prvsc.secid is not null then 'Y' else 'N' end as Dapflag,
vtab.DeclCurenCD,
vtab.DeclGrossamt,
wca.frank.DivAllocTaxYear,
wca.frank.IncomeDivs,
wca.frank.QualIncomeDivs,
wca.frank.STCapGains,
wca.frank.LTCapGains,
wca.frank.TotalCapGains,
wca.frank.QualSTCapGains,
wca.frank.QualLTCapGains,
wca.frank.QualTotalCapGains,
wca.frank.ForeignTaxPaid,
wca.frank.QualForeignTaxPaid,
wca.frank.Section1202Gain,
wca.frank.Collec28PctGain,
wca.frank.CashLiqDist,
wca.frank.NoncashLiqDist,
wca.frank.ExemptInterestDivs,
wca.frank.PercentageOfAMT,
wca.frank.ReturnOfCapital,
wca.frank.Under1250Section
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
INNER JOIN wca.frank on vtab.divid = wca.frank.divid and wca.exchg.cntrycd = wca.frank.cntrycd
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
left outer join wca.prvsc on stab.secid = wca.prvsc.secid and wca.exchg.cntrycd='GB' and 'DAP'=wca.prvsc.privilege
left outer join wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca2.sedolseq on wca.sedol.sedolid = wca2.sedolseq.sedolid
Where
wca.frank.cntrycd='US'
and wca.divpy.curencd='USD'
and wca.divpy.divtype='C'
and (wca.frank.acttime='20190211' 
     or wca.rd.acttime='20190211' 
     or wca.exdt.acttime='20190211' 
     or pexdt.acttime='20190211' 
     or vtab.acttime='20190211' 
     );
