-- arc=n
-- arp=n:\temp\
-- fsx=_Xignite_ISS_HST
-- hpx=EDI_Xignite_ISS_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select distinct
wca.issur.IssID,
wca.issur.Actflag,
wca.issur.acttime as Changed,
wca.issur.AnnounceDate as Created,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK
from wca.issur
where 
wca.issur.actflag<>'D';
