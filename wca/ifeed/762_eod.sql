-- arc=y
-- arp=n:\no_cull_feeds\762\
-- fsx=_762
-- hpx=EDI_WCA_762_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbebnd.acttime,'2000-01-01'))
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbebnd.acttime,'2000-01-01'))
      and (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (ifnull(wca.bbc.acttime,'2000-01-01') > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01') > ifnull(bbebnd.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01') > issur.acttime)
      and (ifnull(wca.bbc.acttime,'2000-01-01') > ifnull(mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then wca.bbc.acttime
     when (ifnull(wca.bbe.acttime,'2000-01-01') > ifnull(bbebnd.acttime,'2000-01-01'))
      and (ifnull(wca.bbe.acttime,'2000-01-01') > issur.acttime)
      and (ifnull(wca.bbe.acttime,'2000-01-01') > ifnull(mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbe.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then wca.bbe.acttime
     when (ifnull(bbebnd.acttime,'2000-01-01') > issur.acttime)
      and (ifnull(bbebnd.acttime,'2000-01-01') > ifnull(mktsg.acttime,'2000-01-01'))
      and (ifnull(bbebnd.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then bbebnd.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
case when ifnull(wca.bbc.curencd,'') <> '' then wca.bbc.curencd
     when ifnull(wca.bbe.curencd,'') <> '' then wca.bbe.curencd
     when ifnull(bbebnd.curencd,'') <> '' then bbebnd.curencd
     else ''
     end as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' 
     then wca.bbe.bbgexhid
     when ifnull(bbebnd.bbgexhid,'')<>''
     then bbebnd.bbgexhid
     else ''
     end as BbgGlobalID,
case when ifnull(wca.bbe.bbgexhtk,'')<>''
     then wca.bbe.bbgexhtk
     when ifnull(bbebnd.bbgexhtk,'')<>''
     then bbebnd.bbgexhtk
     else ''
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbe as bbebnd on wca.scmst.secid = bbebnd.secid and ''=bbebnd.exchgcd and 'D'<> bbebnd.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
where 
(wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.bbc.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.bbe.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or bbebnd.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.issur.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.mktsg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.exchg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3));

