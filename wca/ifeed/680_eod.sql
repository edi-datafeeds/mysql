-- arp=n:\no_cull_feeds\680\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'