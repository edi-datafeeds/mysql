-- # 1
-- arc=y
-- arp=n:\temp\
-- fpx=edi_
-- hpx=edi_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')
-- fsx=select concat('_SedolRef_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_SedolRef_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y

-- # 1
select distinct
wca.scmst.secid,
wca.exchg.cntrycd as listcntrycd,
wca.scmst.primaryexchgcd,
wca.scexh.exchgcd,
case when ifnull(gbp04.TIDM,'')<>'' then replace(gbp04.TIDM,'.L','')
     when ifnull(wca.scxtc.localcode,'')<>'' then wca.scxtc.localcode
     else wca.scexh.localcode
     end as localcode,
wca_other.sedol.sedol,
case when gbp04.sedol is not null
     then gbp04.Currency
     when smf4.security.unitofqcurrcode <> ''
     then smf4.security.unitofqcurrcode
     when wca_other.sedol.curencd <> ''
     then wca_other.sedol.curencd
     else ''
     end as sedoltradingcurrency,
wca_other.sedol.rcntrycd as sedolregistercountry
from wca.scmst
inner join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
inner join wca_other.sedol on wca.scmst.secid=wca_other.sedol.secid and wca.exchg.cntrycd=wca_other.sedol.cntrycd
left outer join smf4.security on wca_other.sedol.sedol=smf4.security.sedol
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on wca_other.sedol.sedol=gbp04.Sedol
left outer join wca.scxtc on wca.scexh.scexhid=wca.scxtc.scexhid and wca_other.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
where
(wca.scexh.exchgcd<>'GBLSE' or gbp04.sedol is not null)
and wca_other.sedol.sedol<>''
and (wca_other.sedol.defunct<>'T' OR smf4.security.statusflag<>'D')
and wca.exchg.actflag<>'D'
and wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca_other.sedol.actflag<>'D'
and wca_other.sedol.sedol<>''
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XZ'
and wca_other.sedol.rcntrycd<>'XH'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'CNSZHK';
