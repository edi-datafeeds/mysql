-- arp=n:\no_cull_feeds\695\mf\
-- fsx=_695
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
union
select * from wca2.t695_mf_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
and exchgcd='USNASD'
order by EventCD;
