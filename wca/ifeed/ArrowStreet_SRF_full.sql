-- arc=n
-- fty=n
-- arp=n:\Bespoke\ArrowStreet\
-- hpx=EDI_ArrowStreet_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select '_ArrowStreet_SRF'
-- hsx=select '_ArrowStreet_SRF'

-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(sedolseq.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scmst.acttime
      when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(sedolseq.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(sedolseq.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(sedolseq.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then mktsg.acttime
     when (ifnull(exchg.acttime,'2000-01-01') > ifnull(sedolseq.acttime,'2000-01-01'))
      and (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then exchg.acttime
     when (ifnull(sedolseq.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(sedolseq.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then sedolseq.acttime
     when (ifnull(bbcseq.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then bbcseq.acttime
     else bbeseq.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
ifnull(sedolbbg.Sedol,'') as Sedol,
ifnull(sedolbbg.SedolDefunct,'') as SedolDefunct,
ifnull(sedolbbg.RcntryCD,'') as RegCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
CASE when wca.exchg.cntrycd<>'US' or (wca.scexh.exchgcd='USOTC' or wca.scexh.exchgcd='USTRCE' or wca.scexh.exchgcd='USBND')
     then wca.scexh.localcode
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2),'PR','/PR'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),'/PR/CL')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3),'PR','/PR/'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4),'PR','/PR/')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-5),'/PR/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,1),'/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WS/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/WS')
     when wca.scmst.sectycd='TRT' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/RT')
     when wca.scmst.sectycd='TRT' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/RT')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WI')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WD')
     else wca.scexh.localcode
     end as localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
where 
(wca.exchg.mic= 'XNYS' or wca.exchg.mic = 'XNAS' or wca.exchg.mic = 'XKRX' or wca.exchg.mic = 'XKOS')
and wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D';
