-- arc=y
-- arp=n:\temp\
-- fsx=select concat('_683','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_683_

-- # 1

select * from wca2.t683_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (isin in (select code from client.pfisin where accid=@accid and actflag='U')
    or uscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
or (isin in (select code from client.pfisin where accid=@accid and actflag='I')
    or uscode in (select code from client.pfuscode where accid=@accid and actflag='I'));
