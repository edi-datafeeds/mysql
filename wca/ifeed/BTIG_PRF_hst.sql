-- arc=y
-- arp=n:\temp\
-- fsx=_BTIG_PRF_HST
-- hpx=EDI_BTIG_PRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
case when ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (sedolbbg.bbeacttime)
     and ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (sedolbbg.bbcacttime)
     and ifnull(sedolbbg.sedolacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.sedolacttime
     when ifnull(sedolbbg.bbeacttime,'2001/01/01') >= (sedolbbg.bbeacttime)
     and ifnull(sedolbbg.bbeacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.bbeacttime
     when ifnull(sedolbbg.bbcacttime,'2001/01/01') >= (wca.scexh.acttime)
     then sedolbbg.bbeacttime
     else wca.scexh.acttime
     end as Changed,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
substring(wca.scexh.ExchgCD,1,2) as ExchgCntry,
wca.scexh.ExchgCD,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
ifnull(sedolbbg.bbgexhid,'') as BbgGlobalID,
ifnull(sedolbbg.bbgexhtk,'') as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
case when ifnull(gbp04.TIDM,'')<>''
     then replace(gbp04.TIDM,'.L','')
     when wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.scexh.LocalCode as Localcode680
from wca.scexh
inner join wca2.t680_hist_prf on wca.scexh.scexhid=wca2.t680_hist_prf.scexhid
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join prices.lse_gb as gbp04 on sedolbbg.sedol=gbp04.Sedol
where
wca.scexh.exchgcd<>'USBND'
and wca.scexh.exchgcd<>'USTRCE';
