-- arc=y
-- arp=n:\temp\
-- hpx=EDI_WCA_685_
-- dfn=l
-- fex=_685.txt
-- fty=y

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (ifnull(bbc.acttime,'2000-01-01') > scmst.acttime)
      and (ifnull(bbc.acttime,'2000-01-01') > issur.acttime)
      and (ifnull(bbc.acttime,'2000-01-01') > scexh.acttime)
      and (ifnull(bbc.acttime,'2000-01-01') > ifnull(sedol.acttime,'2000-01-01'))
      and (ifnull(bbc.acttime,'2000-01-01') > ifnull(bbe.acttime,'2000-01-01'))
     then bbc.acttime
	 when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(bbe.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(sedol.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(bbe.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(sedol.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(bbe.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(sedol.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(bbe.acttime,'2000-01-01') > ifnull(sedol.acttime,'2000-01-01'))
     then bbe.acttime
     else sedol.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.GICS,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.CFI,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scmst.sharesoutstanding,
wca.scmst.sharesoutstandingdate,
wca.sedol.sedol,
wca.sedol.defunct as SedolDefunct
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.sedol on bbe.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and bbe.curencd=wca.sedol.curencd and 'D'<>wca.sedol.actflag
where
(wca.scmst.acttime >= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime >= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or wca.issur.acttime >= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or ifnull(bbc.acttime,'2000-01-01')>= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or ifnull(bbe.acttime,'2000-01-01')>= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or ifnull(sedol.acttime,'2000-01-01')>= (select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or ifnull(wca.bbc.acttime,'2000-01-01')>=(select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3)
or ifnull(wca.bbe.acttime,'2000-01-01')>=(select date_sub(max(feeddate), interval 0 day) from wca.tbl_opslog where seq = 3))
and (primaryexchgcd=scexh.exchgcd or primaryexchgcd='');


