-- arp=n:\no_cull_feeds\682\
-- hpx=EDI_WCA_682_
-- dfn=l
-- fex=_682.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'SHOCH'
