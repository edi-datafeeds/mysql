-- arc=y
-- arp=n:\temp\
-- fsx=_681
-- hpx=EDI_WCA_681_
-- dfn=l
-- fty=y

-- # 1

select wca2.t680_temp.*
from wca2.t680_temp
left outer join wca.icc on wca2.t680_temp.eventcd = wca.icc.eventtype and wca2.t680_temp.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (wca2.t680_temp.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t680_temp.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and eventcd = 'DIV')
or
((wca2.t680_temp.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t680_temp.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and eventcd = 'DIV');
