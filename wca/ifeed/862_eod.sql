-- arp=n:\no_cull_feeds\862\
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_862_
-- dfn=l
-- fty=y
-- fex=_862.txt
-- fty=y

-- # 1

select * from wca2.t862_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3);