-- arp=n:\bespoke\siacharts\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and exchgcntry<>'US' and exchgcntry<>'CA'
and sectycd='ETF'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
