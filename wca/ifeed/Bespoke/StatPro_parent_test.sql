-- arc=y
-- arp=n:\bespoke\statpro\
-- hpx=EDI_WCA_690_
-- dfn=l
-- fex=_690.txt
-- fty=y

-- # 1
select distinct
'NLIST' AS EventCD,
wca.scexh.scexhid AS EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
wca.scexh.ActFlag,
wca.scexh.acttime as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'ParentSecID' as Field1Name,
wca.secrc.secid as Field1,
'ParentEventCD' as Field2Name,
'SECRC' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.secrc on wca.scmst.secid=wca.secrc.ressecid 
             and ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(secrc.effectivedate, interval 7 day) and ifnull(wca.scexh.listdate,wca.scexh.announcedate)>date_sub(secrc.effectivedate, interval 7 day)
             and 'D'<>wca.secrc.actflag
Where
wca.secrc.effectivedate is not null
and (3>wca.sectygrp.secgrpid
or (wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and wca.scexh.announcedate>(select date_sub(max(feeddate), interval 150 day) from wca.tbl_opslog where seq = 3)
and ((wca.scexh.acttime>=(select date_sub(max(acttime), interval '200000' minute) from wca.tbl_opslog))
or (wca.scmst.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (wca.sedol.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)))
and wca.scexh.liststatus<>'D' and wca.scexh.liststatus<>'S' and wca.scmst.statusflag<>'I'

union

select distinct
'NLIST' AS EventCD,
wca.scexh.scexhid AS EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
wca.scexh.ActFlag,
wca.scexh.acttime as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.curencd as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'ParentSecID' as Field1Name,
wca.rd.secid as Field1,
'ParentEventCD' as Field2Name,
'SCSWP' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.rd on wca.scmst.secid=wca.rd.secid 
             and 'D'<>wca.rd.actflag
inner join wca.scswp on wca.rd.secid=wca.scswp.ressecid 
             and ifnull(wca.scexh.listdate,wca.scexh.announcedate)<date_add(rd.recdate, interval 7 day) and ifnull(wca.scexh.listdate,wca.scexh.announcedate)>date_sub(rd.recdate, interval 7 day)
             and 'D'<>wca.scswp.actflag
Where
wca.rd.recdate is not null
and (3>wca.sectygrp.secgrpid
or (wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and wca.scexh.announcedate>(select date_sub(max(feeddate), interval 150 day) from wca.tbl_opslog where seq = 3)
and ((wca.scexh.acttime>=(select date_sub(max(acttime), interval '200000' minute) from wca.tbl_opslog))
or (wca.scmst.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (wca.sedol.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)))
and wca.scexh.liststatus<>'D' and wca.scexh.liststatus<>'S' and wca.scmst.statusflag<>'I';

