-- arp=n:\bespoke\spglobal\
-- fsx=select concat('_690_Alert','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fty=y

-- # 1
SELECT * from wca2.t690_temp
where
eventcd = 'arr' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'arr' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 2
SELECT * from wca2.t690_temp
where
eventcd = 'assm'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'assm'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 3
SELECT * from wca2.t690_temp
where
eventcd = 'bb' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'bb' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 4
SELECT * from wca2.t690_temp
where
eventcd = 'bkrp'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'bkrp'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 5
SELECT * from wca2.t690_temp
where
eventcd = 'bon' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'bon' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 6
SELECT * from wca2.t690_temp
where
eventcd = 'br' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date6 < now()
and date6 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'br' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date6 < now()
and date6 > date_sub(now(), interval 3 day))));

-- # 7
SELECT * from wca2.t690_temp
where
eventcd = 'call' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'call' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 8
SELECT * from wca2.t690_temp
where
eventcd = 'caprd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'caprd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 9
SELECT * from wca2.t690_temp
where
eventcd = 'consd'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'consd'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 10
SELECT * from wca2.t690_temp
where
eventcd = 'conv' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'conv' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 11
SELECT * from wca2.t690_temp
where
eventcd = 'ctx' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'ctx' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 12
SELECT * from wca2.t690_temp
where
eventcd = 'currd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'currd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 13
SELECT * from wca2.t690_temp
where
eventcd = 'dist'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'dist'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 14
SELECT * from wca2.t690_temp
where
eventcd = 'div'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'div'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 15
SELECT * from wca2.t690_temp
where
eventcd = 'dmrgr' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'dmrgr' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 16
SELECT * from wca2.t690_temp
where
eventcd = 'drip' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date4 < now()
and date4 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'drip' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date4 < now()
and date4 > date_sub(now(), interval 3 day))));

-- # 17
SELECT * from wca2.t690_temp
where
eventcd = 'dvst' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'dvst' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 18
SELECT * from wca2.t690_temp
where
eventcd = 'ent'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'ent'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 19
SELECT * from wca2.t690_temp
where
eventcd = 'frank' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'frank' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 20
SELECT * from wca2.t690_temp
where
eventcd = 'ftt' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'ftt' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 21
SELECT * from wca2.t690_temp
where
eventcd = 'fychg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'fychg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 22
SELECT * from wca2.t690_temp
where
eventcd = 'icc' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'icc' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 23
SELECT * from wca2.t690_temp
where
eventcd = 'inchg' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'inchg' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 24
SELECT * from wca2.t690_temp
where
eventcd = 'ischg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'ischg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 25
SELECT * from wca2.t690_temp
where
eventcd = 'lawst'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'lawst'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 26
SELECT * from wca2.t690_temp
where
eventcd = 'lcc'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'lcc'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 27
SELECT * from wca2.t690_temp
where
eventcd = 'liq'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'liq'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 28
SELECT * from wca2.t690_temp
where
eventcd = 'lstat' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'lstat' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 29
SELECT * from wca2.t690_temp
where
eventcd = 'ltchg' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'ltchg' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 30
SELECT * from wca2.t690_temp
where
eventcd = 'mkchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'mkchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 31
SELECT * from wca2.t690_temp
where
eventcd = 'mrgr'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'mrgr'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 32
SELECT * from wca2.t690_temp
where
eventcd = 'nlist' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'nlist' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 33
SELECT * from wca2.t690_temp
where
eventcd = 'oddlt' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'oddlt' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 34
SELECT * from wca2.t690_temp
where
eventcd = 'pid' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'pid' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 35
SELECT * from wca2.t690_temp
where
eventcd = 'po' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'po' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 36
SELECT * from wca2.t690_temp
where
eventcd = 'prchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'prchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 37
SELECT * from wca2.t690_temp
where
eventcd = 'prf' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'prf' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 38
SELECT * from wca2.t690_temp
where
eventcd = 'pvrd'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'pvrd'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 39
SELECT * from wca2.t690_temp
where
eventcd = 'rcap' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'rcap' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 40
SELECT * from wca2.t690_temp
where
eventcd = 'redem'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'redem'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 41
SELECT * from wca2.t690_temp
where
eventcd = 'rts'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'rts'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 42
SELECT * from wca2.t690_temp
where
eventcd = 'scchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'scchg'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 43
SELECT * from wca2.t690_temp
where
eventcd = 'scswp'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'scswp'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 44
SELECT * from wca2.t690_temp
where
eventcd = 'sd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'sd' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 45
SELECT * from wca2.t690_temp
where
eventcd = 'secrc'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'secrc'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date1 < now()
and date1 > date_sub(now(), interval 3 day))));

-- # 46
SELECT * from wca2.t690_temp
where
eventcd = 'tkovr'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))))
UNION
SELECT * from wca2.t690_temp_prf
where
eventcd = 'tkovr'
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 47
SELECT * from wca2.t690_temp_prf
where
eventcd = 'pxoff' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date3 is not null
and ((date3>=@fromdate and date3 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date3 < now()
and date3 > date_sub(now(), interval 3 day))));

-- # 48
SELECT * from wca2.t690_temp_prf
where
eventcd = 'pred' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));

-- # 49
SELECT * from wca2.t690_temp_prf
where
eventcd = 'idiv' 
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and secid in (select secid from client.pfisin where accid = 368 and client.pfisin.actflag <> 'D')
and date2 < now()
and date2 > date_sub(now(), interval 3 day))));