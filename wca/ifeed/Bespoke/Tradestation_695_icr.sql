-- arp=n:\bespoke\tradestation\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch' and eventcd <> 'div'
and eventcd <> 'agm' and eventcd <> 'ann' and eventcd <> 'bkrp' and eventcd <> 'liq'
union
select * from wca2.t695_temp_fi
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch' and eventcd <> 'div'
and eventcd <> 'agm' and eventcd <> 'ann' and eventcd <> 'bkrp' and eventcd <> 'liq'
and eventcd <> 'poff' and eventcd <> 'pcon' and eventcd <> 'pamo'

-- # 2

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd = 'div'
and ((paytype='S' or paytype='B')
or (paytype='C' and (Field1='SPL' or Field1='IRG' or Field1='SUP')))
and exchgcntry='US';


