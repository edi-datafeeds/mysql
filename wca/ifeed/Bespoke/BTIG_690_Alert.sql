-- arc=y
-- arp=n:\bespoke\btig\
-- fsx=_Alert_690
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1

select * from wca2.t690_temp
where
((eventcd='caprd' and date1>@fromdate and date1<=@todate)
or (eventcd='consd' and date1>@fromdate and date1<=@todate)
or (eventcd='div' and date1>@fromdate and date1<=@todate)
or (eventcd='sd' and date1>@fromdate and date1<=@todate));
