-- arc=y
-- arp=n:\upload\acc\165\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1
select wca2.t695_temp.*
from wca2.t695_temp
left outer join wca.icc on wca2.t695_temp.eventcd = wca.icc.eventtype and wca2.t695_temp.eventid = wca.icc.releventid
where
(
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (concat(wca2.t695_temp.secid,wca2.t695_temp.exchgcd) in (select concat(secid,exchgcd) from client.pffigi where accid=@accid and actflag='U') 
or wca2.t695_temp.BbgGlobalID in (select code from client.pffigi where accid=@accid and actflag='U')))
OR
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (concat(wca2.t695_temp.secid,wca2.t695_temp.exchgcd) in 
     (select concat(secid,exchgcd) from client.pffigi 
      where acttime>(select date_sub(max(acttime), interval '14' day) from wca.tbl_opslog)
      and accid=@accid and actflag='D') 
or wca2.t695_temp.BbgGlobalID in (select code from client.pffigi where accid=@accid and actflag='U')))
OR
(concat(wca2.t695_temp.secid,wca2.t695_temp.exchgcd) in (select concat(secid,exchgcd) from client.pffigi where accid=@accid and actflag='I') 
or wca2.t695_temp.BbgGlobalID in (select code from client.pffigi where accid=@accid and actflag='I'))
)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch';