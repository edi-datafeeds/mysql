-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=Daily_AGM_Cancellations_
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=Daily_AGM_Cancellations_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select wca2.evf_company_meeting.event,
wca2.evf_company_meeting.eventid,
wca2.evf_company_meeting.created,
wca2.evf_company_meeting.changed,
wca2.evf_company_meeting.actflag,
wca2.evf_company_meeting.meeting_date,
wca2.evf_dividend.*
from wca2.evf_dividend
inner join wca2.evf_company_meeting on wca2.evf_dividend.issid = wca2.evf_company_meeting.issid
where 
wca2.evf_company_meeting.actflag='Cancelled'
and wca2.evf_company_meeting.changed >= (select max(feeddate) from wca.tbl_opslog)
and 
ifnull(wca2.evf_dividend.record_date,'2000-01-01') >=ifnull(wca2.evf_company_meeting.meeting_date,'2000-01-01')
and (wca2.evf_dividend.actflag<>'Cancelled'
or wca2.evf_dividend.cash_dividend_rescinded not like '%yes%')
