-- arc=y
-- arp=n:\bespoke\fundapps\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'CONSD' or eventcd = 'DRIP' or eventcd = 'RTS' or eventcd = 'SD' or eventcd = 'NLIST')
UNION
select * from wca2.t680_temp_prf
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'CONSD' or eventcd = 'DRIP' or eventcd = 'RTS' or eventcd = 'SD' or eventcd = 'NLIST')
UNION
SELECT distinct maintab.* from wca2.t680_temp as maintab
left outer join wca.divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join wca.divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join wca.divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
left outer join wca.drip on maintab.eventid=wca.drip.divid and maintab.exchgcntry=drip.cntrycd
where
eventcd = 'DIV'
and mainoptn.divid is not null
and mainoptn.divtype='S'
and divopt2.divid is null
and divopt3.divid is null
and changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
UNION
SELECT distinct maintab.* from wca2.t680_temp as maintab
left outer join wca.divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 2=mainoptn.optionid
left outer join wca.divpy as divopt1 on maintab.eventid = divopt1.divid
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag and 'C'<>divopt1.actflag
left outer join wca.divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
left outer join wca.drip on maintab.eventid=wca.drip.divid and maintab.exchgcntry=drip.cntrycd
where
eventcd = 'DIV'
and mainoptn.divid is not null
and mainoptn.divtype='S'
and divopt1.divid is null
and divopt3.divid is null
and changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
order by eventcd, eventid;