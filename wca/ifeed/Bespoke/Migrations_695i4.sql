-- arc=y
-- arp=n:\bespoke\Migrations\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US' and exchgcd<>'USBND'
and eventcd<>'SHOCH'
UNION
select * from wca2.t695_temp_fi
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US' and exchgcd<>'USBND'
and eventcd<>'SHOCH'
order by eventcd, eventid;