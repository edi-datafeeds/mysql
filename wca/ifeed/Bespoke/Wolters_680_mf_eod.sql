-- arp=n:\bespoke\wolters\
-- fsx=_680_MF
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t680_mf_temp
inner join wca.mf on wca2.t680_mf_temp.secid = wca.mf.secid and wca.mf.uttype <> 'T'
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
and exchgcd='USNASD'
order by EventCD;
