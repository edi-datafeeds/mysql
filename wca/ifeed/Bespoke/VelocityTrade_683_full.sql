-- arp=n:\bespoke\velocitytrade\
-- fsx=_Full_683
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_683_
-- dfn=l
-- fty=n

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.ipo on wca.scmst.secid = wca.ipo.secid
where
(3>wca.sectygrp.secgrpid
or ((wca.scmst.sectycd='BND' 
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and (wca.exchg.cntrycd='US' and wca.scexh.exchgcd<>'USBND')))
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'
and (wca.scexh.exchgcd='ATVSE'
or wca.scexh.exchgcd='AUASX'
or wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='CATSE'
or wca.scexh.exchgcd='CHSSX'
or wca.scexh.exchgcd='CZPSE'
or wca.scexh.exchgcd='DEXETR'
or wca.scexh.exchgcd='DKCSE'
or wca.scexh.exchgcd='EETSE'
or wca.scexh.exchgcd='ESBBSE'
or wca.scexh.exchgcd='ESBSE'
or wca.scexh.exchgcd='ESMSE'
or wca.scexh.exchgcd='ESVSE'
or wca.scexh.exchgcd='FIHSE'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='GBLSE'
or wca.scexh.exchgcd='HKSEHK'
or wca.scexh.exchgcd='IEISE'
or wca.scexh.exchgcd='ILTSE'
or wca.scexh.exchgcd='ITMSE'
or wca.scexh.exchgcd='JPTSE'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='NOOB'
or wca.scexh.exchgcd='NZSE'
or wca.scexh.exchgcd='PLWSE'
or wca.scexh.exchgcd='PTBVL'
or wca.scexh.exchgcd='SESSE'
or wca.scexh.exchgcd='SGSSE'
or wca.scexh.exchgcd='TRIMKB'
or wca.scexh.exchgcd='USNASD'
or wca.scexh.exchgcd='USNYSE'
or wca.scexh.exchgcd='BABLSX'
or wca.scexh.exchgcd='BGBSE'
or wca.scexh.exchgcd='BYBCSE'
or wca.scexh.exchgcd='CACNQ'
or wca.scexh.exchgcd='CATSE'
or wca.scexh.exchgcd='CYCSE'
or wca.scexh.exchgcd='CZPSE'
or wca.scexh.exchgcd='DEBSE'
or wca.scexh.exchgcd='DEDSE'
or wca.scexh.exchgcd='DEFSX'
or wca.scexh.exchgcd='DEHNSE'
or wca.scexh.exchgcd='DEHSE'
or wca.scexh.exchgcd='DEMSE'
or wca.scexh.exchgcd='DESSE'
or wca.scexh.exchgcd='EETSE'
or wca.scexh.exchgcd='GGCISX'
or wca.scexh.exchgcd='GRASE'
or wca.scexh.exchgcd='HRZSE'
or wca.scexh.exchgcd='HUBSE'
or wca.scexh.exchgcd='ISISE'
or wca.scexh.exchgcd='LTNSE'
or wca.scexh.exchgcd='LULSE'
or wca.scexh.exchgcd='MKMSE'
or wca.scexh.exchgcd='MTMSE'
or wca.scexh.exchgcd='ROBSE'
or wca.scexh.exchgcd='RSBEL'
or wca.scexh.exchgcd='RUMICX'
or wca.scexh.exchgcd='SILSE'
or wca.scexh.exchgcd='SKBSE'
or wca.scexh.exchgcd='UAPFTS'
or wca.scexh.exchgcd='UAUX'
or wca.scexh.exchgcd='USAMEX'
or wca.scexh.exchgcd='USOTC'
or wca.scexh.exchgcd='USPAC'
or wca.scexh.exchgcd='USTRCE')