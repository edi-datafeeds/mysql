-- arc=y
-- arp=n:\bespoke\fincontent\
-- fsx=_689
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_689_
-- dfn=l
-- fty=n
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t689_temp
where changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='CONSD' or eventcd='DIV' or eventcd='SD')
order by eventcd, eventid;