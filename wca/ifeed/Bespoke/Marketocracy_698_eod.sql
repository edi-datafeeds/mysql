-- arp=n:\bespoke\marketocracy\
-- hpx=EDI_WCA_698_
-- dfn=l
-- fex=_698.txt
-- fty=y

-- # 1

select * from wca2.t698_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and exchgcntry='US';