-- arp=N:\Bespoke\William_Oneil\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (eventcd='bb'
or eventcd='consd'
or eventcd='dmrgr'
or eventcd='icc'
or eventcd='ischg'
or eventcd='lcc'
or eventcd='liq'
or eventcd='lstat'
or eventcd='mrgr'
or eventcd='nlist'
or eventcd='prchg'
or eventcd='prf'
or eventcd='scchg'
or eventcd='sd'
or eventcd='shoch'
or eventcd='tkovr')
and exchgcntry='US';