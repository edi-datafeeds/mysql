-- arc=y
-- arp=n:\bespoke\ftse\
-- fsx=_690
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp_prf
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd='IDIV'