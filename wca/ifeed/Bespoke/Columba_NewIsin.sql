-- arc=y
-- arp=n:\upload\acc\109\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Columba_NewIsin
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=
-- hsx=_Columba_NewIsin
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select distinct wca.issur.Issuername,
wca.scmst.Isin,
wca.scmst.SectyCD,
case when wca.scmst.Statusflag = '' then 'A' else wca.scmst.statusflag end as Statusflag,
wca.scexh.ExchgCD,
case when wca.scexh.Liststatus = '' or wca.scexh.liststatus='N'
	 then 'L'
     else wca.scexh.liststatus
     end as Liststatus,
wca.scexh.ListDate
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid     
where 
wca.scmst.sectycd='EQS'
and wca.scmst.actflag<>'D'
and wca.scmst.statusflag<>'I'
and wca.scexh.actflag<>'D'
and wca.scexh.liststatus<>'D'
and (wca.scmst.isin = 'BE' or wca.scmst.isin='FR')
and wca.scmst.isin not in (select code from client.pfisin where accid = 109 and client.pfisin.actflag<>'D');