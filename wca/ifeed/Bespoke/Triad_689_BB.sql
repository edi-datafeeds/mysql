-- arp=n:\bespoke\triad\
-- hpx=EDI_WCA_689_
-- dfn=l
-- fex=_689.txt
-- fty=y

-- # 1

select * from wca2.t689_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'bb'