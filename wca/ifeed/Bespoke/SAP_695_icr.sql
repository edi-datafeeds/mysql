-- arc=y
-- arp=n:\bespoke\sap\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t695_temp_fi
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcd='USNYSE' or exchgcd='USAMEX' or exchgcd='USNASD' or exchgcd='USTRCE')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch' and eventcd <> 'idiv'
order by eventcd, eventid;

-- # 2

select
'IDIV' as EventCD,
vtab.rdid as EventID,
case when wca.intpy.OptionID is null then '1' else wca.intpy.OptionID end as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.intpy.actflag IS NOT NULL 
     then wca.intpy.actflag 
     else vtab.Actflag 
     END as Actflag,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > vtab.Acttime) and (wca.rd.acttime > wca.exdt.acttime) and (wca.rd.acttime > pexdt.acttime)
     then wca.rd.acttime 
     when (wca.exdt.acttime is not null) and (wca.exdt.Acttime > vtab.Acttime) and (wca.exdt.acttime > pexdt.acttime)
     then wca.exdt.acttime 
     when (pexdt.acttime is not null) and (pexdt.acttime > vtab.acttime)
     then pexdt.acttime
     else vtab.Acttime 
     END as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.USCode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
case when stab.SectyCD = 'BND' then 'PRF' else stab.SectyCD end as SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
case when wca.scexh.ExchgCD = 'USTRCE' then 'USNYSE' else wca.scexh.ExchgCD end as ExchgCD,
case when wca.exchg.Mic = 'FINR' then 'XNYS' else wca.exchg.Mic end as Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.ExDate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN wca.exdt.Paydate
     WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN wca.exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (wca.intpy.inttype='B' or wca.intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN wca.exdt.Paydate2 
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN pexdt.Paydate2 
     ELSE null END as Date4,
'FYEDate' as Date5Type,
null as Date5,
'PeriodEndDate' as Date6Type,
null as Date6,
'OptElectionDate' as Date7Type,
wca.intpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
null as Date8,
'RegistrationDate' as Date9Type,
null as Date9,
'DeclarationDate' as Date10Type,
null as Date10,
'Exdate2' as Date11Type,
CASE WHEN wca.exdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN wca.exdt.Exdate2 
     WHEN pexdt.rdid IS NOT NULL and wca.intpy.inttype='B'
     THEN pexdt.Exdate2 
     ELSE null END as Date11,
'FXDate' as Date12Type,
null as Date12,
wca.intpy.inttype as Paytype,
wca.rd.RdID,
'' as priority,
wca.intpy.DefaultOpt,
wca.intpy.ResSecID as OutturnSecID,
resscmst.isin as OutturnIsin,
substring(wca.intpy.ratioold,1,instr(wca.intpy.ratioold,'.')+7) as RatioOld,
substring(wca.intpy.rationew,1,instr(wca.intpy.rationew,'.')+7) as RatioNew,
wca.intpy.Fractions as Fractions,
wca.intpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.intpy.GrossInterest*stab.ParValue as Rate1,
'NetDividend' as Rate2Type,
wca.intpy.NetInterest as  Rate2,
'Marker' as Field1Name,
'' as Field1,
'Frequency' as Field2Name,
wca.bond.InterestPaymentFrequency as Field2,
'Tbaflag' as Field3Name,
'' as Field3,
'NilDividend' as Field4Name,
vtab.nilint as Field4,
'DivRescind' as Field5Name,
wca.intpy.RescindInterest as Field5,
'RecindCashDiv' as Field6Name,
'' as Field6,
'RescindStockDiv' as Field7Name,
wca.intpy.RescindStockInterest as Field7,
'Approxflag' as Field8Name,
'' as Field8,
'TaxRate' as Field9Name,
wca.intpy.DomesticTaxRate as Field9,
'Depfees' as Field10Name,
'' as Field10,
'Coupon' as Field11Name,
wca.intpy.CouponNo as Field11,
'Dapflag' as Field12Name,
'' as Field12,
'InstallmentPayDate' as Field13Name,
'' as Field13,
'DeclCurenCD' as Field14Name,
'' as Field14,
'DeclGrossAmt' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_intpy as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.v20c_680_scmst_fi as stab on wca.rd.secid = stab.secid
LEFT OUTER join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd 
left outer join wca.intpy on vtab.rdid=intpy.rdid
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.intpy.ressecid = resscmst.secid
Where
((wca.rd.acttime >= (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.exdt.acttime,'2000-01-01') >= (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(pexdt.acttime,'2000-01-01') >= (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(vtab.Acttime ,'2000-01-01') >= (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D') 
and (wca.scexh.exchgcd='USNYSE' or wca.scexh.exchgcd='USAMEX' or wca.scexh.exchgcd='USNASD' or wca.scexh.exchgcd='USTRCE');