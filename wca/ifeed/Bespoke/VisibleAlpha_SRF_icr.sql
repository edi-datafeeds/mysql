-- arc=y
-- arp=n:\bespoke\VisibleAlpha\
-- hpx=EDI_WCA_VisibleAlpha_SRF_
-- dfn=l
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_VisibleAlpha_SRF','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))


-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scmst.acttime, '%Y/%m/%d %H:%i:%s')
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scexh.acttime, '%Y/%m/%d %H:%i:%s')
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(issur.acttime, '%Y/%m/%d %H:%i:%s')
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(mktsg.acttime, '%Y/%m/%d %H:%i:%s')
     else date_format(exchg.acttime, '%Y/%m/%d %H:%i:%s')
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
'' as CIN,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scmst.SharesOutstanding,
wca.scmst.Sharesoutstandingdate as EffectiveDate
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where 
(wca.exchg.cntrycd='BR' or wca.exchg.cntrycd='IN' or wca.exchg.cntrycd='US')
and (wca.scmst.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) 
     or wca.scexh.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.mktsg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.exchg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     );


