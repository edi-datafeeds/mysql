-- arp=n:\bespoke\dividendmax\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'AGM'
or eventcd = 'BON'
or eventcd = 'CALL'
or eventcd = 'CONSD'
or eventcd = 'DIST'
or eventcd = 'DIV'
or eventcd = 'DMRGR'
or eventcd = 'ENT'
or eventcd = 'ICC'
or eventcd = 'ISCHG'
or eventcd = 'LCC'
or eventcd = 'LSTAT'
or eventcd = 'MRGR'
or eventcd = 'NLIST'
or eventcd = 'PRCHG'
or eventcd = 'RCAP'
or eventcd = 'RTS'
or eventcd = 'SCSWP'
or eventcd = 'SD'
or eventcd = 'SECRC'
or eventcd = 'TKOVR')

