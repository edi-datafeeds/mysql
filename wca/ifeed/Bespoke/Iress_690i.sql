-- arc=y
-- arp=n:\bespoke\iress\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd <> 'ctx' and eventcd <> 'secrc' and eventcd <> 'assm' and eventcd <> 'tkovr'
and eventcd <> 'bon' and eventcd <> 'br' and eventcd <> 'dmrgr' and eventcd <> 'dist' and eventcd <> 'dvst'
and eventcd <> 'div' and eventcd <> 'ent' and eventcd <> 'mrgr' and eventcd <> 'prf' and eventcd <> 'scswp'
and eventcd <> 'caprd' and eventcd <> 'consd' and eventcd <> 'sd' and eventcd <> 'rts'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch' and eventcd <> 'frank' and eventcd <> 'drip')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
wca2.t690_temp.IssID,
wca2.t690_temp.Isin,
wca2.t690_temp.uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
wca2.t690_temp.SectyCD,
wca2.t690_temp.SecurityDesc,
wca2.t690_temp.ParValue,
PVCurrency,
wca2.t690_temp.StatusFlag,
wca2.t690_temp.PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
wca2.t690_temp.StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
Field17Name,
Field17,
Field18Name,
Field18,
Field19Name,
Field19,
Field20Name,
Field20,
Field21Name,
Field21,
'OutturnUSCode' as Field22Name,
resscmst.uscode as Field22,
'OutturnSedol' as Field23Name,
ressedol.sedol as Field23,
'OutturnLocalCode' as Field24Name,
resscexh.localcode as Field24
from wca2.t690_temp
left outer join wca.sedol as ressedol on wca2.t690_temp.outturnsecid = ressedol.secid
                         and wca2.t690_temp.exchgcntry = ressedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = ressedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = ressedol.curencd 
left outer join wca.scexh as resscexh on wca2.t690_temp.outturnsecid = resscexh.secid
                                     and wca2.t690_temp.exchgcd = resscexh.exchgcd
left outer join wca.scmst as resscmst on wca2.t690_temp.outturnsecid = resscmst.secid
where
(changed>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(ressedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'ctx' or eventcd = 'secrc' or eventcd = 'assm' or eventcd = 'tkovr'
or eventcd = 'bon' or eventcd = 'dmrgr' or eventcd = 'dist' or eventcd = 'dvst'
or eventcd = 'ent' or eventcd = 'mrgr' or eventcd = 'prf' or eventcd = 'scswp')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
wca2.t690_temp.IssID,
wca2.t690_temp.Isin,
wca2.t690_temp.uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
wca2.t690_temp.SectyCD,
wca2.t690_temp.SecurityDesc,
wca2.t690_temp.ParValue,
PVCurrency,
wca2.t690_temp.StatusFlag,
wca2.t690_temp.PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
wca2.t690_temp.StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
Field17Name,
Field17,
'TradingIsin' as Field18Name,
trascmst.isin as Field18,
'TradingUSCode' as Field19Name ,
trascmst.uscode as Field19,
'TradingSedol' as Field20Name,
trasedol.sedol as Field20,
'TradingLocalCode' as Field21Name,
trascexh.localcode as Field21,
'OutturnUSCode' as Field22Name,
resscmst.uscode as Field22,
'OutturnSedol' as Field23Name,
ressedol.sedol as Field23,
'OutturnLocalCode' as Field24Name,
resscexh.localcode as Field24
from wca2.t690_temp
left outer join wca.sedol as ressedol on wca2.t690_temp.outturnsecid = ressedol.secid
                         and wca2.t690_temp.exchgcntry = ressedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = ressedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = ressedol.curencd 
left outer join wca.sedol as trasedol on wca2.t690_temp.field1 = trasedol.secid
                         and wca2.t690_temp.exchgcntry = trasedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = trasedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = trasedol.curencd 
left outer join wca.scexh as resscexh on wca2.t690_temp.outturnsecid = resscexh.secid
                                     and wca2.t690_temp.exchgcd = resscexh.exchgcd
left outer join wca.scexh as trascexh on wca2.t690_temp.field1 = trascexh.secid
                                     and wca2.t690_temp.exchgcd = trascexh.exchgcd
left outer join wca.scmst as resscmst on wca2.t690_temp.outturnsecid = resscmst.secid
left outer join wca.scmst as trascmst on wca2.t690_temp.field1 = trascmst.secid
where
(changed>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(ressedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(trasedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(trascexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(trascmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'rts' or eventcd = 'br')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
wca2.t690_temp.IssID,
wca2.t690_temp.Isin,
wca2.t690_temp.uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
wca2.t690_temp.SectyCD,
wca2.t690_temp.SecurityDesc,
wca2.t690_temp.ParValue,
PVCurrency,
wca2.t690_temp.StatusFlag,
wca2.t690_temp.PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
wca2.t690_temp.StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
wca2.t690_temp.RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
'DivPeriodCD' as Field17Name,
wca.div_my.DivPeriodCD as Field17,
Field18Name,
Field18,
Field19Name,
Field19,
Field20Name,
Field20,
Field21Name,
Field21,
'OutturnUSCode' as Field22Name,
resscmst.uscode as Field22,
'OutturnSedol' as Field23Name,
ressedol.sedol as Field23,
'OutturnLocalCode' as Field24Name,
resscexh.localcode as Field24
from wca2.t690_temp
left outer join wca.sedol as ressedol on wca2.t690_temp.outturnsecid = ressedol.secid
                         and wca2.t690_temp.exchgcntry = ressedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = ressedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = ressedol.curencd 
left outer join wca.scexh as resscexh on wca2.t690_temp.outturnsecid = resscexh.secid
                                     and wca2.t690_temp.exchgcd = resscexh.exchgcd
left outer join wca.scmst as resscmst on wca2.t690_temp.outturnsecid = resscmst.secid
left outer join wca.div_my on wca2.t690_temp.eventid = wca.div_my.divid
where
(changed>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(ressedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(resscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'div' or eventcd = 'drip' or eventcd = 'frank')
union
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'ParallelTradingEnd' as Date12Type,
case when wca.lstat.effectivedate is not null 
     then date_sub(wca.lstat.effectivedate, interval '1' day) 
     else null 
     end as EffectiveDate,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'Currency' as Field3Name,
vtab.CurenCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'TradingIsin' as Field18Name,
tlscmst.isin as Field18,
'TradingUSCode' as Field19Name,
tlscmst.uscode as Field19,
'TradingSedol' as Field20Name,
tlsedol.sedol as Field20,
'TradingLocalcode' as Field21Name,
tlscexh.localcode as Field21,
'OutturnUSCode' as Field22Name,
wca.icc.newuscode as Field22,
'OutturnSedol' as Field23Name,
wca.sdchg.newsedol as Field23,
'OutturnLocalCode' as Field24Name,
wca.lcc.newlocalcode as Field24
from wca.v10s_consd as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype
                                                                 and wca.exchg.CntryCD<>wca.sdchg.cntrycd
                                                                 and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.tlelk on vtab.eventid = wca.tlelk.eventid
                                 and vtab.eventcd=wca.tlelk.eventtype
                                 and 'D'<>wca.tlelk.actflag
left outer join wca.scmst as tlscmst on wca.tlelk.secid = tlscmst.secid
left outer join wca.scexh as tlscexh on wca.tlelk.secid = tlscexh.secid
                                 and wca.scexh.ExchgCD=tlscexh.exchgcd
left outer join wca.sedol as tlsedol on tlelk.secid = tlsedol.secid
                                 and wca.exchg.CntryCD=tlsedol.cntrycd
left outer join wca.lstat on tlscexh.secid=wca.lstat.secid
                                 and tlscexh.exchgcd = wca.lstat.exchgcd
where
(vtab.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.icc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lcc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.sdchg.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.exdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(pexdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.tlelk.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlsedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lstat.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.exchg.CntryCD = 'AU'
  or wca.exchg.CntryCD = 'BE'
  or wca.exchg.CntryCD = 'CA'
  or wca.exchg.CntryCD = 'CN'
  or wca.exchg.CntryCD = 'DE'
  or wca.exchg.CntryCD = 'FR'
  or wca.exchg.CntryCD = 'GB'
  or wca.exchg.CntryCD = 'HK'
  or wca.exchg.CntryCD = 'ID'
  or wca.exchg.CntryCD = 'IN'
  or wca.exchg.CntryCD = 'JP'
  or wca.exchg.CntryCD = 'KR'
  or wca.exchg.CntryCD = 'MY'
  or wca.exchg.CntryCD = 'NL'
  or wca.exchg.CntryCD = 'NZ'
  or wca.exchg.CntryCD = 'PH'
  or wca.exchg.CntryCD = 'PT'
  or wca.exchg.CntryCD = 'SG'
  or wca.exchg.CntryCD = 'TH'
  or wca.exchg.CntryCD = 'TW'
  or wca.exchg.CntryCD = 'US'
  or wca.exchg.CntryCD = 'ZA')
union
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'ParallelTradingEnd' as Date12Type,
case when wca.lstat.effectivedate is not null 
     then date_sub(wca.lstat.effectivedate, interval '1' day) 
     else null 
     end as EffectiveDate,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'OldCurrency' as Field3Name,
vtab.OldCurenCD as Field3,
'NewCurrency' as Field4Name,
vtab.NewCurenCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'TradingIsin' as Field18Name,
tlscmst.isin as Field18,
'TradingUSCode' as Field19Name,
tlscmst.uscode as Field19,
'TradingSedol' as Field20Name,
tlsedol.sedol as Field20,
'TradingLocalcode' as Field21Name,
tlscexh.localcode as Field21,
'OutturnUSCode' as Field22Name,
wca.icc.newuscode as Field22,
'OutturnSedol' as Field23Name,
wca.sdchg.newsedol as Field23,
'OutturnLocalCode' as Field24Name,
wca.lcc.newlocalcode as Field24
from wca.v10s_sd as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype
                                                                 and wca.exchg.CntryCD<>wca.sdchg.cntrycd
                                                                 and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.tlelk on vtab.eventid = wca.tlelk.eventid
                                 and vtab.eventcd=wca.tlelk.eventtype
                                 and 'D'<>wca.tlelk.actflag
left outer join wca.scmst as tlscmst on wca.tlelk.secid = tlscmst.secid
left outer join wca.scexh as tlscexh on wca.tlelk.secid = tlscexh.secid
                                 and wca.scexh.ExchgCD=tlscexh.exchgcd
left outer join wca.sedol as tlsedol on tlelk.secid = tlsedol.secid
                                 and wca.exchg.CntryCD=tlsedol.cntrycd
left outer join wca.lstat on tlscexh.secid=wca.lstat.secid
                                 and tlscexh.exchgcd = wca.lstat.exchgcd
where
(vtab.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.icc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lcc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.sdchg.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.exdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(pexdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.tlelk.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlsedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lstat.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.exchg.CntryCD = 'AU'
  or wca.exchg.CntryCD = 'BE'
  or wca.exchg.CntryCD = 'CA'
  or wca.exchg.CntryCD = 'CN'
  or wca.exchg.CntryCD = 'DE'
  or wca.exchg.CntryCD = 'FR'
  or wca.exchg.CntryCD = 'GB'
  or wca.exchg.CntryCD = 'HK'
  or wca.exchg.CntryCD = 'ID'
  or wca.exchg.CntryCD = 'IN'
  or wca.exchg.CntryCD = 'JP'
  or wca.exchg.CntryCD = 'KR'
  or wca.exchg.CntryCD = 'MY'
  or wca.exchg.CntryCD = 'NL'
  or wca.exchg.CntryCD = 'NZ'
  or wca.exchg.CntryCD = 'PH'
  or wca.exchg.CntryCD = 'PT'
  or wca.exchg.CntryCD = 'SG'
  or wca.exchg.CntryCD = 'TH'
  or wca.exchg.CntryCD = 'TW'
  or wca.exchg.CntryCD = 'US'
  or wca.exchg.CntryCD = 'ZA')
union
select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'ParallelTradingEnd' as Date12Type,
case when wca.lstat.effectivedate is not null 
     then date_sub(wca.lstat.effectivedate, interval '1' day) 
     else null 
     end as EffectiveDate,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'TradingIsin' as Field18Name,
tlscmst.isin as Field18,
'TradingUSCode' as Field19Name,
tlscmst.uscode as Field19,
'TradingSedol' as Field20Name,
tlsedol.sedol as Field20,
'TradingLocalcode' as Field21Name,
tlscexh.localcode as Field21,
'OutturnUSCode' as Field22Name,
wca.icc.newuscode as Field22,
'OutturnSedol' as Field23Name,
wca.sdchg.newsedol as Field23,
'OutturnLocalCode' as Field24Name,
wca.lcc.newlocalcode as Field24
from wca.v10s_caprd as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype 
                                                                 and wca.exchg.CntryCD<>wca.sdchg.cntrycd
                                                                 and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.tlelk on vtab.eventid = wca.tlelk.eventid
                                 and vtab.eventcd=wca.tlelk.eventtype
                                 and 'D'<>wca.tlelk.actflag
left outer join wca.scmst as tlscmst on wca.tlelk.secid = tlscmst.secid
left outer join wca.scexh as tlscexh on wca.tlelk.secid = tlscexh.secid
                                 and wca.scexh.ExchgCD=tlscexh.exchgcd
left outer join wca.sedol as tlsedol on tlelk.secid = tlsedol.secid
                                 and wca.exchg.CntryCD=tlsedol.cntrycd
left outer join wca.lstat on tlscexh.secid=wca.lstat.secid
                                 and tlscexh.exchgcd = wca.lstat.exchgcd
where
(vtab.acttime>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.icc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lcc.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.sdchg.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.exdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(pexdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.tlelk.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscmst.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlscexh.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(tlsedol.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or ifnull(wca.lstat.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.exchg.CntryCD = 'AU'
  or wca.exchg.CntryCD = 'BE'
  or wca.exchg.CntryCD = 'CA'
  or wca.exchg.CntryCD = 'CN'
  or wca.exchg.CntryCD = 'DE'
  or wca.exchg.CntryCD = 'FR'
  or wca.exchg.CntryCD = 'GB'
  or wca.exchg.CntryCD = 'HK'
  or wca.exchg.CntryCD = 'ID'
  or wca.exchg.CntryCD = 'IN'
  or wca.exchg.CntryCD = 'JP'
  or wca.exchg.CntryCD = 'KR'
  or wca.exchg.CntryCD = 'MY'
  or wca.exchg.CntryCD = 'NL'
  or wca.exchg.CntryCD = 'NZ'
  or wca.exchg.CntryCD = 'PH'
  or wca.exchg.CntryCD = 'PT'
  or wca.exchg.CntryCD = 'SG'
  or wca.exchg.CntryCD = 'TH'
  or wca.exchg.CntryCD = 'TW'
  or wca.exchg.CntryCD = 'US'
  or wca.exchg.CntryCD = 'ZA');
