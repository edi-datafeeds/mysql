-- arp=n:\bespoke\vms\
-- fsx=select concat('_689','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=VMSData_WCA_689_
-- dfn=l
-- fty=n
-- eof=VMSData_ENDOFFILE

-- # 1

select * from wca2.t689_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
union
select * from wca2.t689_mf_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
order by EventCD;