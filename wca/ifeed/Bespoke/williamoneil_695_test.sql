-- arp=N:\Bespoke\William_Oneil\test\
-- hpx=EDI_WCA_695_
-- dfn=l
-- fex=_695.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='bb'
or eventcd='consd'
or eventcd='dmrgr'
or eventcd='icc'
or eventcd='ischg'
or eventcd='lcc'
or eventcd='liq'
or eventcd='lstat'
or eventcd='mrgr'
or eventcd='nlist'
or eventcd='prchg'
or eventcd='prf'
or eventcd='scchg'
or eventcd='sd'
or eventcd='shoch'
or eventcd='tkovr')
and (exchgcntry='CA'
or exchgcntry='CN'
or exchgcntry='DE'
or exchgcntry='HK'
or exchgcntry='IN'
or exchgcntry='JP'
or exchgcntry='SG'
or exchgcntry='GB')
union
select * from wca2.t695_etn
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='icc'
or eventcd='intpy'
or eventcd='ischg'
or eventcd='lcc'
or eventcd='liq'
or eventcd='lstat'
or eventcd='nlist'
or eventcd='prchg'
or eventcd='redm'
or eventcd='scchg')
and (exchgcntry='CA'
or exchgcntry='CN'
or exchgcntry='DE'
or exchgcntry='HK'
or exchgcntry='IN'
or exchgcntry='JP'
or exchgcntry='SG'
or exchgcntry='GB')
order by eventcd;