-- fty=y
-- fsx=_680
-- hpx=EDI_WCA_680_
-- dfn=l
-- arp=n:\upload\acc\253\

-- # 1

select * from wca2.t680_temp
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (isin in (select code from client.pfisin where accid=253 and actflag='U'))
or (isin in (select code from client.pfisin where accid=253 and actflag='I')))
and (exchgcd = primaryexchgcd or primaryexchgcd = '');
