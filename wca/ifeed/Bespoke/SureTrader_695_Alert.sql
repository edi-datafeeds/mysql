-- arc=y
-- arp=n:\bespoke\suretrader\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=select concat('_Alert_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_Alert_
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'agm' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 2
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ann' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 3
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'arr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 4
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'assm' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 5
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'bb' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))

-- # 6
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'bkrp' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 7
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'bon' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 8
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'br' 
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date6 < now()
      and date6 > date_sub(now(), interval 7 day)))

-- # 9
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'call' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 10
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'caprd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 11
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'consd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 12
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'conv' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))

-- # 13
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ctx' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))

-- # 14
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'currd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 15
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'dist' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 16
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'div' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 17
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'dmrgr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 18
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'drip' 
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date4 < now()
      and date4 > date_sub(now(), interval 7 day)))

-- # 19
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'dvst' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 20
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ent' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 21
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'frank' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 22
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ftt' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))

-- # 23
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'fychg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 24
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'icc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 25
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'inchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 26
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ischg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 27
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'lawst' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 28
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'lcc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 29
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'liq' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 30
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'lstat' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 31
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'ltchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 32
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'mkchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 33
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'mrgr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 34
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'nlist' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 35
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'oddlt' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))

-- # 36
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'pid' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 37
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'po' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 38
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'prchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 39
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'prf' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 40
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'pvrd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 41
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'rcap' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 42
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'redem' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 43
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'rts' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 44
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'scchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 45
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'scswp' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 46
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'sd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 47
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'secrc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)))

-- # 48
SELECT * from wca2.t695_temp
where 
exchgcntry = 'US'
and eventcd = 'tkovr' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date2 < now()
      and date2 > date_sub(now(), interval 7 day)))
