-- arc=y
-- arp=n:\bespoke\vtbcapital\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_StampDutyExmp
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_SRF_VTBCapital_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
automate_tasks.growthmarketstemp.Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
case when automate_tasks.growthmarketstemp.exemption_end_date = '0000-00-00' then 'Y' else 'N' end as StampDutyExmpt,
case when automate_tasks.growthmarketstemp.exemption_start_date is not null then automate_tasks.growthmarketstemp.exemption_start_date else '' end as ExemptionStartDate,
case when automate_tasks.growthmarketstemp.exemption_end_date <> '0000-00-00' then automate_tasks.growthmarketstemp.exemption_end_date else '' end as ExemptionEndDate
from automate_tasks.growthmarketstemp
left outer join wca.scmst on automate_tasks.growthmarketstemp.isin = wca.scmst.isin
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where (wca.exchg.cntrycd='GB' or wca.exchg.cntrycd='IE' or wca.scmst.secid is null) 
and (wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D' or wca.scmst.secid is null)
and (wca.scexh.liststatus<>'D' or wca.scexh.secid is null)
and (automate_tasks.growthmarketstemp.exemption_end_date='0000-00-00'
or automate_tasks.growthmarketstemp.exemption_end_date>=date_sub(now(),interval 7 day))
order by sectycd, isin