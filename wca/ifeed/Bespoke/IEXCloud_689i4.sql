-- arc=y
-- arp=n:\bespoke\iex_cloud\
-- fsx=select concat('_689','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_689_
-- dfn=l
-- fty=n

-- # 1

select * from wca2.t689_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'BON'
or eventcd = 'CALL'
or eventcd = 'RCAP'
or eventcd = 'CONSD'
or eventcd = 'DMRGR'
or eventcd = 'DIST'
or eventcd = 'DIV'
or eventcd = 'ENT'
or eventcd = 'SECRC'
or eventcd = 'RTS'
or eventcd = 'SCSWP'
or eventcd = 'SD'
or eventcd = 'IDIV')
union
select * from wca2.t689_mf_temp
where
changed >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'BON'
or eventcd = 'CALL'
or eventcd = 'RCAP'
or eventcd = 'CONSD'
or eventcd = 'DMRGR'
or eventcd = 'DIST'
or eventcd = 'DIV'
or eventcd = 'ENT'
or eventcd = 'SECRC'
or eventcd = 'RTS'
or eventcd = 'SCSWP'
or eventcd = 'SD')
order by EventCD, EventID;