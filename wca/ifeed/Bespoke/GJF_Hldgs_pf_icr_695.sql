-- arc=y
-- arp=n:\upload\acc\893\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- fty=y

-- # 1

select * from wca2.t695_temp
left outer join wca.icc on wca2.t695_temp.eventcd = wca.icc.eventtype and wca2.t695_temp.eventid = wca.icc.releventid
where
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (wca2.t695_temp.secid in (select secid from client.pfisin where accid=@accid and actflag='U') 
or wca2.t695_temp.secid in (select secid from client.pfuscode where accid=@accid and actflag='U') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='U')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and (eventcd = 'consd' or eventcd = 'icc' or eventcd = 'ischg' or eventcd = 'lcc' or eventcd = 'prchg' or eventcd = 'sd'))
or
((wca2.t695_temp.secid in (select secid from client.pfisin where accid=@accid and actflag='I') 
or wca2.t695_temp.secid in (select secid from client.pfuscode where accid=@accid and actflag='I') 
or wca.icc.newisin in (select code from client.pfisin where accid=@accid and actflag='I')
or wca.icc.newuscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and (eventcd = 'consd' or eventcd = 'icc' or eventcd = 'ischg' or eventcd = 'lcc' or eventcd = 'prchg' or eventcd = 'sd'));