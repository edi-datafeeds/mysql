-- arp=N:\Bespoke\ValueLine\
-- hpx=EDI_WCA_695_
-- dfn=l
-- fex=_695.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='bkrp'
or eventcd='consd'
or eventcd='dmrgr'
or eventcd='fychg'
or eventcd='icc'
or eventcd='ischg'
or eventcd='lcc'
or eventcd='liq'
or eventcd='lstat'
or eventcd='mrgr'
or eventcd='nlist'
or eventcd='prchg'
or eventcd='scchg'
or eventcd='sd'
or eventcd='tkovr')
and exchgcntry='US'