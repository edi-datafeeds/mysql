-- arc=y
-- arp=n:\bespoke\bellpotter\
-- fsx=_690
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcd='ATVSE'
or exchgcd='AUASX'
or exchgcd='BEENB'
or exchgcd='CACVE'
or exchgcd='CATSE'
or exchgcd='CHSSX'
or exchgcd='CNSGSE'
or exchgcd='CNSSE'
or exchgcd='DEDSE'
or exchgcd='DEFSX'
or exchgcd='DEMSE'
or exchgcd='DEXETR'
or exchgcd='DKCSE'
or exchgcd='ESMSE'
or exchgcd='FIHSE'
or exchgcd='FRPEN'
or exchgcd='GBLSE'
or exchgcd='GRASE'
or exchgcd='HKSEHK'
or exchgcd='HUBSE'
or exchgcd='IDJSE'
or exchgcd='IEISE'
or exchgcd='ITMSE'
or exchgcd='JPTSE'
or exchgcd='KRKSE'
or exchgcd='NLENA'
or exchgcd='NOOB'
or exchgcd='NZSE'
or exchgcd='PHPSE'
or exchgcd='PTBVL'
or exchgcd='SESSE'
or exchgcd='SGSSE'
or exchgcd='THSET'
or exchgcd='USAMEX'
or exchgcd='USBATS'
or exchgcd='USFNBB'
or exchgcd='USNASD'
or exchgcd='USNYSE'
or exchgcd='USOTC'
or exchgcd='USPAC'
or exchgcd='ZAJSE')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t690_temp_prf
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcd='ATVSE'
or exchgcd='AUASX'
or exchgcd='BEENB'
or exchgcd='CACVE'
or exchgcd='CATSE'
or exchgcd='CHSSX'
or exchgcd='CNSGSE'
or exchgcd='CNSSE'
or exchgcd='DEDSE'
or exchgcd='DEFSX'
or exchgcd='DEMSE'
or exchgcd='DEXETR'
or exchgcd='DKCSE'
or exchgcd='ESMSE'
or exchgcd='FIHSE'
or exchgcd='FRPEN'
or exchgcd='GBLSE'
or exchgcd='GRASE'
or exchgcd='HKSEHK'
or exchgcd='HUBSE'
or exchgcd='IDJSE'
or exchgcd='IEISE'
or exchgcd='ITMSE'
or exchgcd='JPTSE'
or exchgcd='KRKSE'
or exchgcd='NLENA'
or exchgcd='NOOB'
or exchgcd='NZSE'
or exchgcd='PHPSE'
or exchgcd='PTBVL'
or exchgcd='SESSE'
or exchgcd='SGSSE'
or exchgcd='THSET'
or exchgcd='USAMEX'
or exchgcd='USBATS'
or exchgcd='USFNBB'
or exchgcd='USNASD'
or exchgcd='USNYSE'
or exchgcd='USOTC'
or exchgcd='USPAC'
or exchgcd='ZAJSE')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;