-- arc=y
-- arp=n:\bespoke\atreyu\
-- fsx=select concat('_862','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_862_
-- dfn=l
-- fty=n

-- # 1

select * from wca2.t862_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US';
