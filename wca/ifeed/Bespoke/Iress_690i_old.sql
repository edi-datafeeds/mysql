-- arc=y
-- arp=n:\bespoke\iress\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd <> 'ctx' and eventcd <> 'secrc' and eventcd <> 'assm' and eventcd <> 'tkovr'
and eventcd <> 'bon' and eventcd <> 'br' and eventcd <> 'dmrgr' and eventcd <> 'dist' and eventcd <> 'dvst'
and eventcd <> 'div' and eventcd <> 'ent' and eventcd <> 'mrgr' and eventcd <> 'prf' and eventcd <> 'scswp'
and eventcd <> 'caprd' and eventcd <> 'consd' and eventcd <> 'sd' and eventcd <> 'arr' and eventcd <> 'rts'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
IssID,
Isin,
uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
SectyCD,
SecurityDesc,
ParValue,
PVCurrency,
StatusFlag,
PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
Field17Name,
Field17,
Field18Name,
Field18,
Field19Name,
Field19,
Field20Name,
Field20,
'TradingSedol' as Field21Name,
trasedol.sedol as Field21,
'TradingLocalCode' as Field22Name,
trascexh.localcode as Field22,
'OutturnSedol' as Field23Name,
ressedol.sedol as Field23,
'OutturnLocalCode' as Field24Name,
resscexh.localcode as Field24
from wca2.t690_temp
left outer join wca.sedol as ressedol on wca2.t690_temp.outturnsecid = ressedol.secid
                         and wca2.t690_temp.exchgcntry = ressedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = ressedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = ressedol.curencd 
left outer join wca.sedol as trasedol on wca2.t690_temp.secid = trasedol.secid
                         and wca2.t690_temp.exchgcntry = trasedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = trasedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = trasedol.curencd 
left outer join wca.scexh as resscexh on wca2.t690_temp.outturnsecid = resscexh.secid
                                     and wca2.t690_temp.exchgcd = resscexh.exchgcd
left outer join wca.scexh as trascexh on wca2.t690_temp.secid = trascexh.secid
                                     and wca2.t690_temp.exchgcd = trascexh.exchgcd

where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'ctx' or eventcd = 'secrc' or eventcd = 'assm' or eventcd = 'tkovr'
or eventcd = 'bon' or eventcd = 'dmrgr' or eventcd = 'dist' or eventcd = 'dvst'
or eventcd = 'div' or eventcd = 'ent' or eventcd = 'mrgr' or eventcd = 'prf' or eventcd = 'scswp')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
IssID,
Isin,
uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
SectyCD,
SecurityDesc,
ParValue,
PVCurrency,
StatusFlag,
PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
Field17Name,
Field17,
Field18Name,
Field18,
Field19Name,
Field19,
Field20Name,
Field20,
'TradingSedol' as Field21Name,
trasedol.sedol as Field21,
'TradingLocalCode' as Field22Name,
trascexh.localcode as Field22,
'OutturnSedol' as Field23Name,
ressedol.sedol as Field23,
'OutturnLocalCode' as Field24Name,
resscexh.localcode as Field24
from wca2.t690_temp
left outer join wca.sedol as ressedol on wca2.t690_temp.outturnsecid = ressedol.secid
                         and wca2.t690_temp.exchgcntry = ressedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = ressedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = ressedol.curencd 
left outer join wca.sedol as trasedol on wca2.t690_temp.field1 = trasedol.secid
                         and wca2.t690_temp.exchgcntry = trasedol.cntrycd
                         and wca2.t690_temp.sedolregcntry = trasedol.rcntrycd
                         and wca2.t690_temp.sedolcurrency = trasedol.curencd 
left outer join wca.scexh as resscexh on wca2.t690_temp.outturnsecid = resscexh.secid
                                     and wca2.t690_temp.exchgcd = resscexh.exchgcd
left outer join wca.scexh as trascexh on wca2.t690_temp.field1 = trascexh.secid
                                     and wca2.t690_temp.exchgcd = trascexh.exchgcd

where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'rts' or eventcd = 'br')
union
select
EventCD,
EventID,
OptionID,
SerialID,
wca2.t690_temp.ScexhID,
wca2.t690_temp.sedolid,
wca2.t690_temp.ActFlag,
Changed,
Created,
wca2.t690_temp.SecID,
IssID,
Isin,
uscode,
IssuerName,
CntryofIncorp,
SIC,
CIK,
IndusID,
SectyCD,
SecurityDesc,
ParValue,
PVCurrency,
StatusFlag,
PrimaryExchgCD,
wca2.t690_temp.Sedol,
SedolCurrency,
wca2.t690_temp.Defunct,
SedolRegCntry,
StructCD,
ExchgCntry,
wca2.t690_temp.ExchgCD,
Mic,
Micseg,
wca2.t690_temp.LocalCode,
wca2.t690_temp.ListStatus,
Date1Type,
Date1,
Date2Type,
Date2,
Date3Type,
Date3,
Date4Type,
Date4,
Date5Type,
Date5,
Date6Type,
Date6,
Date7Type,
Date7,
Date8Type,
Date8,
Date9Type,
Date9,
Date10Type,
Date10,
Date11Type,
Date11,
Date12Type,
Date12,
Paytype,
RdID,
priority,
DefaultOpt,
OutturnSecID,
OutturnIsin,
RatioOld,
RatioNew,
Fractions,
Currency,
Rate1Type,
Rate1,
Rate2Type,
Rate2,
Field1Name,
Field1,
Field2Name,
Field2,
Field3Name,
Field3,
Field4Name,
Field4,
Field5Name,
Field5,
Field6Name,
Field6,
Field7Name,
Field7,
Field8Name,
Field8,
Field9Name,
Field9,
Field10Name,
Field10,
Field11Name,
Field11,
Field12Name,
Field12,
Field13Name,
Field13,
Field14Name,
Field14,
Field15Name,
Field15,
Field16Name,
Field16,
Field17Name,
Field17,
Field18Name,
Field18,
Field19Name,
Field19,
Field20Name,
Field20,
'TradingSedol' as Field21Name,
wca2.t690_temp.sedol as Field21,
'TradingLocalCode' as Field22Name,
wca2.t690_temp.localcode as Field22,
'OutturnSedol' as Field23Name,
wca.sdchg.newsedol as Field23,
'OutturnLocalCode' as Field24Name,
wca.lcc.newlocalcode as Field24
from wca2.t690_temp
left outer join wca.sdchg on wca2.t690_temp.secid = wca.sdchg.secid
                         and wca2.t690_temp.eventid = wca.sdchg.releventid
left outer join wca.lcc on wca2.t690_temp.secid = wca.lcc.secid
                       and wca2.t690_temp.eventid = wca.lcc.releventid
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry = 'AU'
or exchgcntry = 'BE'
or exchgcntry = 'CA'
or exchgcntry = 'CN'
or exchgcntry = 'DE'
or exchgcntry = 'FR'
or exchgcntry = 'GB'
or exchgcntry = 'HK'
or exchgcntry = 'ID'
or exchgcntry = 'IN'
or exchgcntry = 'JP'
or exchgcntry = 'KR'
or exchgcntry = 'MY'
or exchgcntry = 'NL'
or exchgcntry = 'NZ'
or exchgcntry = 'PH'
or exchgcntry = 'PT'
or exchgcntry = 'SG'
or exchgcntry = 'TH'
or exchgcntry = 'TW'
or exchgcntry = 'US'
or exchgcntry = 'ZA')
and
(eventcd = 'caprd' or eventcd = 'consd' or eventcd = 'sd' or eventcd = 'arr')
