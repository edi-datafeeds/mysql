-- arp=N:\Bespoke\GJF_Hldgs\
-- hpx=EDI_WCA_689_
-- dfn=l
-- fex=_689.txt
-- fty=n

-- # 1

select * from wca2.t689_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='consd'
or eventcd='icc'
or eventcd='ischg'
or eventcd='prchg'
or eventcd='sd')
order by eventcd;