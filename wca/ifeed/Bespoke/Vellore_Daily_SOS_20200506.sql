-- arc=y
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Daily_SOS
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_Daily_SOS_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select wca.issur.IssID,
wca.scmst.SecID,
wca.issur.Issuername,
wca.scmst.Isin,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.cntry.Country,
wca.exchg.CntryCD,
wca.scmst.PrimaryExchgCD,
wca.exchg.ExchgName,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate
from wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
                          and 'D'<>wca.scmst.actflag
                          and 'I'<>wca.scmst.statusflag
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.exchg on wca.scmst.primaryexchgcd = wca.exchg.exchgcd
left outer join wca.cntry on wca.exchg.cntrycd = wca.cntry.cntrycd
where 
(wca.scmst.primaryexchgcd='HRZSE'
or wca.scmst.primaryexchgcd='IRTSE'
or wca.scmst.primaryexchgcd='PHPSE'
or wca.scmst.primaryexchgcd='JOASE'
or wca.scmst.primaryexchgcd='NOOB'
or wca.scmst.primaryexchgcd='CLBCS'
or wca.scmst.primaryexchgcd='ATVSE'
or wca.scmst.primaryexchgcd='ILTSE'
or wca.scmst.primaryexchgcd='CZPSE'
or wca.scmst.primaryexchgcd='BRBVSP'
or wca.scmst.primaryexchgcd='ROBSE'
or wca.scmst.primaryexchgcd='KZKASE'
or wca.scmst.primaryexchgcd='NANSE'
or wca.scmst.primaryexchgcd='TNTSE'
or wca.scmst.primaryexchgcd='SGSSE'
or wca.scmst.primaryexchgcd='SILSE'
or wca.scmst.primaryexchgcd='MXMSE'
or wca.scmst.primaryexchgcd='ESMSE'
or wca.scmst.primaryexchgcd='DEDSE'
or wca.scmst.primaryexchgcd='QADSM'
or wca.scmst.primaryexchgcd='RUMICX'
or wca.scmst.primaryexchgcd='COCSX'
or wca.scmst.primaryexchgcd='FJSPSX')
order by wca.exchg.cntrycd, wca.issur.Issuername;