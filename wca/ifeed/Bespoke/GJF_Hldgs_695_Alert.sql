-- arc=y
-- arp=n:\upload\acc\893
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Alert_695
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_Alert_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE


-- # 1

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'consd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));


-- # 2

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'icc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));


-- # 3

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'ischg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));


-- # 4

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'lcc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));


-- # 5

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'prchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));


-- # 6

SELECT * from wca2.t695_temp
where 
wca2.t695_temp.secid in (select secid from client.pfisin where accid = 893 and actflag <> 'D')
and eventcd = 'sd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));
