-- arp=n:\bespoke\tradestation\
-- fsx=_Alert_695
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_Alert_
-- dfn=l
-- fty=y


-- # 1
SELECT * from wca2.t695_temp
where
eventcd = 'arr' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'arr' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 2
SELECT * from wca2.t695_temp
where
eventcd = 'assm'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'assm'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 3
SELECT * from wca2.t695_temp
where
eventcd = 'bb' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'bb' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 4
SELECT * from wca2.t695_temp
where
eventcd = 'bon' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'bon' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 5
SELECT * from wca2.t695_temp
where
eventcd = 'br' 
and exchgcntry='US'
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date6 < now()
and date6 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'br' 
and exchgcntry='US'
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date6 < now()
and date6 > date_sub(now(), interval 7 day))));

-- # 6
SELECT * from wca2.t695_temp
where
eventcd = 'call' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'call' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 7
SELECT * from wca2.t695_temp
where
eventcd = 'caprd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'caprd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 8
SELECT * from wca2.t695_temp
where
eventcd = 'consd'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'consd'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 9
SELECT * from wca2.t695_temp
where
eventcd = 'conv' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'conv' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 10
SELECT * from wca2.t695_temp
where
eventcd = 'ctx' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'ctx' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 11
SELECT * from wca2.t695_temp
where
eventcd = 'currd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'currd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 12
SELECT * from wca2.t695_temp
where
eventcd = 'dist'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'dist'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 13
SELECT * from wca2.t695_temp
where
eventcd = 'div'
and ((paytype='S' or paytype='B')
or (paytype='C' and (Field1='SPL' or Field1='IRG' or Field1='SUP')))
and exchgcntry='US'
and (((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01')<=@todate)
or (ifnull(date11,'2000-01-01')>=@fromdate and ifnull(date11,'2000-01-01')<=@todate))
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and (ifnull(date1,'2099-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day)
or (ifnull(date11,'2099-01-01') < now()
and ifnull(date11,'2000-01-01') > date_sub(now(), interval 7 day)))));

-- # 14
SELECT * from wca2.t695_temp
where
eventcd = 'dmrgr' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'dmrgr' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 15
SELECT * from wca2.t695_temp
where
eventcd = 'drip' 
and exchgcntry='US'
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date4 < now()
and date4 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'drip' 
and exchgcntry='US'
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date4 < now()
and date4 > date_sub(now(), interval 7 day))));

-- # 16
SELECT * from wca2.t695_temp
where
eventcd = 'dvst' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'dvst' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 17
SELECT * from wca2.t695_temp
where
eventcd = 'ent'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'ent'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 18
SELECT * from wca2.t695_temp
where
eventcd = 'frank' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'frank' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 19
SELECT * from wca2.t695_temp
where
eventcd = 'ftt' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'ftt' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 20
SELECT * from wca2.t695_temp
where
eventcd = 'fychg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'fychg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 21
SELECT * from wca2.t695_temp
where
eventcd = 'icc' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'icc' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 22
SELECT * from wca2.t695_temp
where
eventcd = 'inchg' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'inchg' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 23
SELECT * from wca2.t695_temp
where
eventcd = 'ischg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'ischg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 24
SELECT * from wca2.t695_temp
where
eventcd = 'lawst'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'lawst'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 25
SELECT * from wca2.t695_temp
where
eventcd = 'lcc'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'lcc'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 26
SELECT * from wca2.t695_temp
where
eventcd = 'lstat' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'lstat' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 27
SELECT * from wca2.t695_temp
where
eventcd = 'ltchg' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'ltchg' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 28
SELECT * from wca2.t695_temp
where
eventcd = 'mkchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'mkchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 29
SELECT * from wca2.t695_temp
where
eventcd = 'mrgr'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'mrgr'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 30
SELECT * from wca2.t695_temp
where
eventcd = 'nlist' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'nlist' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 31
SELECT * from wca2.t695_temp
where
eventcd = 'oddlt' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'oddlt' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 32
SELECT * from wca2.t695_temp
where
eventcd = 'pid' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'pid' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 33
SELECT * from wca2.t695_temp
where
eventcd = 'po' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'po' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 34
SELECT * from wca2.t695_temp
where
eventcd = 'prchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'prchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 35
SELECT * from wca2.t695_temp
where
eventcd = 'prf' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'prf' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 36
SELECT * from wca2.t695_temp
where
eventcd = 'pvrd'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'pvrd'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 37
SELECT * from wca2.t695_temp
where
eventcd = 'rcap' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'rcap' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 38
SELECT * from wca2.t695_temp
where
eventcd = 'redem'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'redem'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 39
SELECT * from wca2.t695_temp
where
eventcd = 'rts'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'rts'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 40
SELECT * from wca2.t695_temp
where
eventcd = 'scchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'scchg'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 41
SELECT * from wca2.t695_temp
where
eventcd = 'scswp'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'scswp'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 42
SELECT * from wca2.t695_temp
where
eventcd = 'sd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'sd' 
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 43
SELECT * from wca2.t695_temp
where
eventcd = 'secrc'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'secrc'
and exchgcntry='US'
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day))));

-- # 44
SELECT * from wca2.t695_temp
where
eventcd = 'tkovr'
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))))
UNION
SELECT * from wca2.t695_temp_fi
where
eventcd = 'tkovr'
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 45
SELECT * from wca2.t695_temp_fi
where
eventcd = 'pxoff' 
and exchgcntry='US'
and date3 is not null
and ((date3>=@fromdate and date3 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date3 < now()
and date3 > date_sub(now(), interval 7 day))));

-- # 46
SELECT * from wca2.t695_temp_fi
where
eventcd = 'pred' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));

-- # 47
SELECT * from wca2.t695_temp_fi
where
eventcd = 'idiv' 
and exchgcntry='US'
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date2 < now()
and date2 > date_sub(now(), interval 7 day))));
