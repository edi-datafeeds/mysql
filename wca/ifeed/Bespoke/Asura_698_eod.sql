-- arc=y
-- arp=n:\bespoke\asurafintech\
-- fsx=_698
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_698_
-- dfn=l
-- fty=n

-- # 1

select * from wca2.t698_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and exchgcntry='GB'
