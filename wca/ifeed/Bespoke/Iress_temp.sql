select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01')) 
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime) 
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_arr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.sedol.sedol in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and (vtab.acttime>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or
(vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sedol.sedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));


select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01')) 
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
     then rd.acttime
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_caprd as vtab
INNER join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype and 'D'<>wca.sdchg.actflag
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.sedol.sedol in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and (vtab.acttime>=@fromdate
 or ifnull(icc.acttime,'2000-01-01')>=@fromdate
 or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
 or rd.acttime>=@fromdate
 or vtab.effectivedate>=@fromdate and vtab.effectivedate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sedol.sedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sdchg.oldsedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));


select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'Currency' as Field3Name,
vtab.CurenCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_consd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.sedol.sedol in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and (vtab.acttime>=@fromdate
 or ifnull(icc.acttime,'2000-01-01')>=@fromdate
 or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
 or rd.acttime>=@fromdate
 or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
 or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sedol.sedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sdchg.oldsedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));



select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime 
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime 
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01')) 
     then rd.acttime 
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01')) 
     then ifnull(exdt.acttime,'2000-01-01') 
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime) 
     then ifnull(pexdt.acttime,'2000-01-01') 
     else vtab.acttime 
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'OldCurrency' as Field3Name,
vtab.OldCurenCD as Field3,
'NewCurrency' as Field4Name,
vtab.NewCurenCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from wca.v10s_sd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = wca.stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@runmode='E' and concat(eventcd, vtab.eventid) in (select concat(eventcd, eventid) from client.pfevent)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
or (@runmode='H'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
and (vtab.acttime>@fromdate and vtab.acttime < @todate and vtab.actflag<>'D'
and (@cntrycd is null or wca.exchg.cntrycd = @cntrycd)
  or (stab.isin in (select code from client.pfisin where actflag<>'D' and accid=@accid)
    or stab.uscode in (select code from client.pfuscode where actflag<>'D' and accid=@accid)
    or wca.sedol.sedol in (select code from client.pfsedol where actflag<>'D' and accid=@accid)
    or stab.secid in (select code from client.pfsecid where actflag<>'D' and accid=@accid)))
or (@runmode is null and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and (vtab.acttime>=@fromdate
or ifnull(icc.acttime,'2000-01-01')>=@fromdate
or ifnull(lcc.acttime,'2000-01-01')>=@fromdate
or rd.acttime>=@fromdate
or ifnull(exdt.acttime,'2000-01-01')>=@fromdate
or ifnull(pexdt.acttime,'2000-01-01')>=@fromdate
 or wca.exdt.exdate>=@fromdate and wca.exdt.exdate<=@caldate
 or wca.pexdt.exdate>=@fromdate and wca.pexdt.exdate<=@caldate
 or (vtab.acttime>@histdate and vtab.actflag<>'D'
  and (stab.isin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.uscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sedol.sedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or stab.secid in (select code from client.pfsecid where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.oldisin in (select code from client.pfisin where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.icc.olduscode in (select code from client.pfuscode where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))
    or wca.sdchg.oldsedol in (select code from client.pfsedol where actflag='I' and accid in (select accid from client.accfs where feedseriesid=@feedseriesid))))));
