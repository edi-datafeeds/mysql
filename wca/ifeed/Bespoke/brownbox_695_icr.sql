-- arp=n:\bespoke\brownbox\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and exchgcntry='US'
and (exchgcd<>'USOTC' and exchgcd<>'USFNBB')
and (eventcd = 'consd' or eventcd = 'dist' or eventcd = 'div' or eventcd = 'lcc'
or eventcd = 'rcap' or eventcd = 'sd' ) 
union
select * from wca2.t695_mf_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (eventcd = 'consd' or eventcd = 'dist' or eventcd = 'div' or eventcd = 'lcc'
or eventcd = 'rcap' or eventcd = 'sd' )
and exchgcd='USNASD'
order by EventCD;
