-- arc=y
-- arp=n:\bespoke\spglobal\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Alert_690
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_Alert_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT * from wca2.t690_temp
where 
eventcd = 'ICC' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));

-- # 2
SELECT * from wca2.t690_temp
where 
eventcd = 'LCC' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));
	
-- # 3
SELECT * from wca2.t690_temp
where 
eventcd = 'SDCHG' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
      and date1 < now()
      and date1 > date_sub(now(), interval 7 day)));
      
set @fromdate=(select max(feeddate) from wca.tbl_opslog);
set @todate=case when dayofweek(now())=4 or dayofweek(now())=5 or dayofweek(now())=6 then date_add(now(), interval 7 day) else date_add(now(), interval 5 day) end;
SELECT dayofweek(now());
