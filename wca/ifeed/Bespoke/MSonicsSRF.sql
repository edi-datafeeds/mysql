-- arc=y
-- arp=n:\temp\
-- hpx=EDI_MSonic_SRF_
-- dfn=l
-- fex=_MSonic_SRF.txt
-- fty=y

-- # 1
select
prices.lasttrade.pricedate,
wca.scmst.Isin,
prices.lasttrade.Localcode,
wca.issur.Gics,
wca.issur.Cik,
wca.issur.IndusID,
wca.indus.Indusname,
wca.scmst.SecurityDesc,
wca.issur.Issuername,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.secty.SecurityDescriptor,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid=wca.scmst.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.indus on wca.issur.indusid=wca.indus.indusid
left outer join wca.secty on wca.scmst.sectycd=wca.secty.sectycd
where
prices.lasttrade.exchgcd='USCOMP'
and prices.lasttrade.pricedate>'2016-05-31';
