-- arp=N:\Bespoke\Accutech\MF\
-- fsx=select concat('_698','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_698_
-- dfn=l
-- fty=n

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (wca.scmst.acttime > scexh.acttime)
      and (wca.scmst.acttime > issur.acttime)
      and (wca.scmst.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.scmst.acttime
     when (wca.scexh.acttime > issur.acttime)
      and (wca.scexh.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.scexh.acttime
     when (wca.issur.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.issur.acttime
     when (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.bbe.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.bbc.acttime
     when (ifnull(wca.bbe.acttime,'2000-01-01')>ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbe.acttime,'2000-01-01')>ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.bbe.acttime
     when (ifnull(wca.mktsg.acttime,'2000-01-01') > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.mktsg.acttime
     else wca.exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.scmst.sectycd='MF'
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>wca.bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd 
                        and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
where 
(wca.scmst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
or (wca.scexh.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (wca.issur.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.bbc.acttime,'2000-01-01') >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.bbe.acttime,'2000-01-01') >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.mktsg.acttime,'2000-01-01') >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.exchg.acttime,'2000-01-01') >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)))
and wca.scexh.exchgcd='USNASD';