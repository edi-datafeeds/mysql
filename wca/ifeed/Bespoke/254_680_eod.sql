-- arc=y
-- arp=n:\upload\acc\254\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
isin in (select code from client.pfisin where accid = 254 and actflag<>'D')
and changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='ischg'
or eventcd='icc')
