-- arc=y
-- arp=n:\upload\acc\358\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=Unmatched_Securities_Report_
-- fsx=
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=Crabel_Unmatched_Securities_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1

select client.pftickmic.code as Ticker, 
client.pftickmic.MIC,
case when wca.scexh.secid is not null then 'Yes' else 'No' end as Available_In_EDI_Database
from client.pftickmic
left outer join wca.exchg on client.pftickmic.mic = wca.exchg.mic
left outer join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd and wca.scexh.localcode = client.pftickmic.code
where
client.pftickmic.accid = 358
and client.pftickmic.actflag<>'D'
and wca.scexh.secid is null
order by Available_In_EDI_Database;