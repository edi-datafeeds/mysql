-- arc=y
-- arp=n:\bespoke\accutech\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcd='USAMEX' or exchgcd='USNYSE' or exchgcd='USPAC')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t695_temp_fi
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcd='USAMEX' or exchgcd='USNYSE' or exchgcd='USPAC')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;