-- arc=y
-- arp=n:\upload\acc\260\695\
-- fsx=_Alert_695
-- hpx=EDI_WCA_695_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=n

-- # 1

select * from wca2.t695_temp
where
isin in (select code from client.pfisin where accid = 260 and actflag<>'D')
and ((eventcd='agm' and date1>=@fromdate and date1<=@todate)
or (eventcd='ann' and date1>=@fromdate and date1<=@todate)
or (eventcd='arr' and date1>=@fromdate and date1<=@todate)
or (eventcd='assm' and date1>=@fromdate and date1<=@todate)
or (eventcd='bb' and date2>=@fromdate and date2<=@todate)
or (eventcd='bkrp' and date1>=@fromdate and date1<=@todate)
or (eventcd='bon' and date1>=@fromdate and date1<=@todate)
or (eventcd='br' and date6>=@fromdate and date6<=@todate)
or (eventcd='call' and date1>=@fromdate and date1<=@todate)
or (eventcd='caprd' and date1>=@fromdate and date1<=@todate)
or (eventcd='consd' and date1>=@fromdate and date1<=@todate)
or (eventcd='conv' and date2>=@fromdate and date2<=@todate)
or (eventcd='ctx' and date2>=@fromdate and date2<=@todate)
or (eventcd='currd' and date1>=@fromdate and date1<=@todate)
or (eventcd='dist' and date1>=@fromdate and date1<=@todate)
or (eventcd='div' and date1>=@fromdate and date1<=@todate)
or (eventcd='dmrgr' and date1>=@fromdate and date1<=@todate)
or (eventcd='drip' and date4>=@fromdate and date4<=@todate)
or (eventcd='dvst' and date1>=@fromdate and date1<=@todate)
or (eventcd='ent' and date1>=@fromdate and date1<=@todate)
or (eventcd='frank' and date1>=@fromdate and date1<=@todate)
or (eventcd='ftt' and date2>=@fromdate and date2<=@todate)
or (eventcd='fychg' and date1>=@fromdate and date1<=@todate)
or (eventcd='icc' and date1>=@fromdate and date1<=@todate)
or (eventcd='inchg' and date1>=@fromdate and date1<=@todate)
or (eventcd='ischg' and date1>=@fromdate and date1<=@todate)
or (eventcd='lawst' and date1>=@fromdate and date1<=@todate)
or (eventcd='lcc' and date1>=@fromdate and date1<=@todate)
or (eventcd='liq' and date1>=@fromdate and date1<=@todate)
or (eventcd='lstat' and date1>=@fromdate and date1<=@todate)
or (eventcd='ltchg' and date1>=@fromdate and date1<=@todate)
or (eventcd='mkchg' and date1>=@fromdate and date1<=@todate)
or (eventcd='mrgr' and date1>=@fromdate and date1<=@todate)
or (eventcd='nlist' and date1>=@fromdate and date1<=@todate)
or (eventcd='oddlt' and date2>=@fromdate and date2<=@todate)
or (eventcd='pid' and date1>=@fromdate and date1<=@todate)
or (eventcd='po' and date1>=@fromdate and date1<=@todate)
or (eventcd='prchg' and date1>=@fromdate and date1<=@todate)
or (eventcd='prf' and date1>=@fromdate and date1<=@todate)
or (eventcd='pvrd' and date1>=@fromdate and date1<=@todate)
or (eventcd='rcap' and date1>=@fromdate and date1<=@todate)
or (eventcd='redem' and date1>=@fromdate and date1<=@todate)
or (eventcd='rts' and date1>=@fromdate and date1<=@todate)
or (eventcd='scchg' and date1>=@fromdate and date1<=@todate)
or (eventcd='scswp' and date1>=@fromdate and date1<=@todate)
or (eventcd='sd' and date1>=@fromdate and date1<=@todate)
or (eventcd='secrc' and date1>=@fromdate and date1<=@todate)
or (eventcd='tkovr' and date2>=@fromdate and date2<=@todate));
