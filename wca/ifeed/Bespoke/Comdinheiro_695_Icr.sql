-- arc=y
-- arp=n:\bespoke\comdinheiro\
-- fsx=select concat('_695','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcntry='AD'
or exchgcntry='AL'
or exchgcntry='AM'
or exchgcntry='AT'
or exchgcntry='AZ'
or exchgcntry='BA'
or exchgcntry='BE'
or exchgcntry='BG'
or exchgcntry='BY'
or exchgcntry='CH'
or exchgcntry='CY'
or exchgcntry='CZ'
or exchgcntry='DE'
or exchgcntry='DK'
or exchgcntry='EE'
or exchgcntry='ES'
or exchgcntry='FI'
or exchgcntry='FO'
or exchgcntry='FR'
or exchgcntry='GB'
or exchgcntry='GE'
or exchgcntry='GG'
or exchgcntry='GI'
or exchgcntry='GR'
or exchgcntry='HR'
or exchgcntry='HU'
or exchgcntry='IE'
or exchgcntry='IM'
or exchgcntry='IS'
or exchgcntry='IT'
or exchgcntry='JE'
or exchgcntry='LI'
or exchgcntry='LT'
or exchgcntry='LU'
or exchgcntry='LV'
or exchgcntry='MC'
or exchgcntry='MD'
or exchgcntry='MK'
or exchgcntry='MT'
or exchgcntry='NL'
or exchgcntry='NO'
or exchgcntry='PL'
or exchgcntry='PT'
or exchgcntry='RO'
or exchgcntry='RU'
or exchgcntry='SE'
or exchgcntry='SI'
or exchgcntry='SJ'
or exchgcntry='SK'
or exchgcntry='SM'
or exchgcntry='UA'
or exchgcntry='VA'
or exchgcntry='RS'
or exchgcntry='EU'
or exchgcntry='ME')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;