-- arc=y
-- arp=n:\bespoke\kelmar\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Alert_695
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_695_Alert_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'agm' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 2
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ann' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 3
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'arr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 4
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'assm' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 5
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'bb' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 6
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'bkrp' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 7
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'bon' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 8
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'br' 
and date6 is not null
and ((date6>=@fromdate and date6 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 9
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'call' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 10
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'caprd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 11
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'consd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 12
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'conv' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 13
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ctx' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 14
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'currd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 15
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'dist' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 16
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'div' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 17
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'dmrgr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 18
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'drip' 
and date4 is not null
and ((date4>=@fromdate and date4 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 19
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'dvst' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 20
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ent' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 21
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'frank' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 22
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ftt' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 23
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'fychg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 24
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'icc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 25
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'inchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 26
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ischg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 27
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'lawst' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 28
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'lcc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 29
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'liq' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 30
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'lstat' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 31
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'ltchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 32
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'mkchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 33
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'mrgr' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 34
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'nlist' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 35
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'oddlt' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 36
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'pid' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 37
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'po' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 38
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'prchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 39
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'prf' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 40
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'pvrd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 41
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'rcap' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 42
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'redem' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 43
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'rts' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 44
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'scchg' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 45
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'scswp' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 46
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'sd' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 47
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'secrc' 
and date1 is not null
and ((date1>=@fromdate and date1 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))

-- # 48
SELECT * from wca2.t695_Kelmar
where 
eventcd = 'tkovr' 
and date2 is not null
and ((date2>=@fromdate and date2 <=@todate)
      OR
     (changed>'2017/11/20' and changed<'2017/12/15'))
