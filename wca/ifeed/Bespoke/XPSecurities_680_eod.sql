-- arp=n:\no_cull_feeds\xpsecurities\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where 
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'FTT'
and (exchgcntry='FR' or exchgcntry='IT');