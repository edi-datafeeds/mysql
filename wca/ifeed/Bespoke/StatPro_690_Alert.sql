-- arc=y
-- arp=n:\bespoke\statpro\
-- fsx=_Alert_690
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')

-- # 1

select * from wca2.t690_temp
where
((eventcd='assm' and date1>now() and date1<@todate)
or (eventcd='bb' and date2>now() and date2<@todate)
or (eventcd='bkrp' and date1>now() and date1<@todate)
or (eventcd='bon' and date1>now() and date1<@todate)
or (eventcd='br' and date6>now() and date6<@todate)
or (eventcd='caprd' and date1>now() and date1<@todate)
or (eventcd='consd' and date1>now() and date1<@todate)
or (eventcd='currd' and date1>now() and date1<@todate)
or (eventcd='dist' and date1>now() and date1<@todate)
or (eventcd='div' and date1>now() and date1<@todate)
or (eventcd='dmrgr' and date1>now() and date1<@todate)
or (eventcd='drip' and date2>now() and date2<@todate)
or (eventcd='ent' and date1>now() and date1<@todate)
or (eventcd='frank' and date1>now() and date1<@todate)
or (eventcd='icc' and date1>now() and date1<@todate)
or (eventcd='inchg' and date1>now() and date1<@todate)
or (eventcd='ischg' and date1>now() and date1<@todate)
or (eventcd='lcc' and date1>now() and date1<@todate)
or (eventcd='liq' and date1>now() and date1<@todate)
or (eventcd='mrgr' and date1>now() and date1<@todate)
or (eventcd='nlist' and date1>now() and date1<@todate)
or (eventcd='po' and date1>now() and date1<@todate)
or (eventcd='pvrd' and date1>now() and date1<@todate)
or (eventcd='rcap' and date1>now() and date1<@todate)
or (eventcd='rts' and date1>now() and date1<@todate)
or (eventcd='sd' and date1>now() and date1<@todate)
or (eventcd='sdchg' and date1>now() and date1<@todate)
or (eventcd='secrc' and date1>now() and date1<@todate)
or (eventcd='shoch' and date1>@fromdate and date1<=@todate)
or (eventcd='tkovr' and date2>now() and date2<@todate));