-- arc=y
-- arp=n:\bespoke\lmr\
-- fsx=_690
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and exchgcntry = 'US'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t690_temp_prf
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and exchgcntry = 'US'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;