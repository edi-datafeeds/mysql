-- arp=N:\Bespoke\Taniscott\
-- hpx=EDI_WCA_695_
-- dfn=l
-- fex=_695.txt
-- fty=y

-- # 1
select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
and (exchgcd='AEADSM' or exchgcd='AEDFM' or exchgcd='ARMVB' or exchgcd='ATVSE' or exchgcd='AUASX'
or exchgcd='BEENB' or exchgcd='BGBSE' or exchgcd='BHBSE' or exchgcd='BRBVSP' or exchgcd='BWBSE'
or exchgcd='CACVE' or exchgcd='CATSE' or exchgcd='CHSSX' or exchgcd='CIBVA' or exchgcd='CLBCS'
or exchgcd='CNSGSE' or exchgcd='CNSSE' or exchgcd='COCSX' or exchgcd='CYCSE' or exchgcd='CZPSE'
or exchgcd='DEFSX' or exchgcd='DEXETR' or exchgcd='DKCSE' or exchgcd='EETSE' or exchgcd='EGCASE'
or exchgcd='ESMSE' or exchgcd='FIHSE' or exchgcd='FRPEN' or exchgcd='GBLSE' or exchgcd='GHGSE'
or exchgcd='GRASE' or exchgcd='HKSEHK' or exchgcd='HRZSE' or exchgcd='HUBSE' or exchgcd='IDJSE'
or exchgcd='IEISE' or exchgcd='ILTSE' or exchgcd='INNSE' or exchgcd='IQISX' or exchgcd='ISISE'
or exchgcd='ITMSE' or exchgcd='JOASE' or exchgcd='JPJASD' or exchgcd='JPOSE' or exchgcd='JPTSE'
or exchgcd='KENSE' or exchgcd='KRKSDQ' or exchgcd='KRKSE' or exchgcd='KWKSE' or exchgcd='LBBSE'
or exchgcd='LKCSE' or exchgcd='LTNSE' or exchgcd='LULSE' or exchgcd='LVRSE' or exchgcd='MABVC'
or exchgcd='MTMSE' or exchgcd='MUSEM' or exchgcd='MXMSE' or exchgcd='MYKLSE' or exchgcd='NANSE'
or exchgcd='NGLSE' or exchgcd='NLENA' or exchgcd='NOOB' or exchgcd='NZSE' or exchgcd='OMMSM'
or exchgcd='PEBVL' or exchgcd='PHPSE' or exchgcd='PKKSE' or exchgcd='PLWSE' or exchgcd='PSAFM'
or exchgcd='PTBVL' or exchgcd='QADSM' or exchgcd='ROBSE' or exchgcd='RUMICX' or exchgcd='SARSE'
or exchgcd='SESSE' or exchgcd='SGSSE' or exchgcd='SILSE' or exchgcd='SKBSE' or exchgcd='THSET'
or exchgcd='TNTSE' or exchgcd='TRIMKB' or exchgcd='TWTSE' or exchgcd='UAPFTS' or exchgcd='USAMEX'
or exchgcd='USNASD' or exchgcd='USNYSE' or exchgcd='USPAC' or exchgcd='VECSE' or exchgcd='VNSTC' 
or exchgcd='ZAJSE' or exchgcd='ZMZSE' or exchgcd='ZWZSE');