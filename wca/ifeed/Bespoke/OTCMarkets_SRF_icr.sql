-- arc=y
-- arp=n:\bespoke\OTCMarkets_SRF\
-- hpx=EDI_WCA_OTCMarkets_SRF_
-- dfn=l
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_OTCMarkets_SRF','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))


-- # 1

select distinct
wca.scexh.ScexhID,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     else '1'
     end as BbgExhSeqnum,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
wca.scexh.Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scmst.acttime
      when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then mktsg.acttime
      when (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then exchg.acttime
     when (ifnull(bbcseq.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then bbcseq.acttime
     else bbeseq.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
wca.scexh.ListDate,
wca.scmst.SharesOutstanding,
wca.scmst.Sharesoutstandingdate as EffectiveDate
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca2.bbcseq as bbcseq on wca.bbc.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on wca.bbe.bbeid=bbeseq.bbeid
left outer join wca.ipo on wca.scmst.secid = wca.ipo.secid
where 
wca.exchg.cntrycd='CA'
and (wca.ipo.ipostatus<>'PENDING' or wca.ipo.secid is null)
and wca.scexh.exchgcd not like '%BND'
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.issur.actflag<>'D' or wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.mktsg.mktsgid is null or (wca.mktsg.actflag<>'D' or ifnull(wca.mktsg.acttime,'2000-01-01')>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog))
and (wca.scmst.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) 
     or wca.scexh.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca2.bbcseq.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca2.bbeseq.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.mktsg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.exchg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     );

