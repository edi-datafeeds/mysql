-- hpx=EDI_IPO_103_
-- fex=.103
-- fty=y
-- eor=Select concat(char(13),char(10))
-- dtm=select '';
-- arp=N:\bespoke\william_oneil\test\ipo\
-- dfn=n

-- # 0

select 
'Created' as Created,
'Modified' as Modified,
'ID' as ID,
'Status' as Status,
'Actflag' as Actflag,
'IssuerName' as IssuerName,
'CntryInc' as CntryInc,
'ExchgCd' as ExchgCd,
'ISIN' as ISIN,
'Sedol' as Sedol,
'USCode' as USCode,
'Symbol' as Symbol,
'SecDescription' as SecDescription,
'PVCurrency' as PVCurrency,
'ParValue' as ParValue,
'Sectype' as Sectype,
'LotSize' as LotSize,
'SubPeriodFrom' as SubPeriodFrom,
'SubperiodTo' as SubperiodTo,
'MinSharesOffered' as MinSharesOffered,
'MaxSharesOffered' as MaxSharesOffered,
'SharesOutstanding' as SharesOutstanding,
'Currency' as Currency,
'SharePriceLowest' as SharePriceLowest,
'SharePriceHighest' as SharePriceHighest,
'ProposedPrice' as ProposedPrice,
'InitialPrice' as InitialPrice,
'InitialTradedVolume' as InitialTradedVolume,
'Underwriter' as Underwriter,
'DealType' as DealType,
'LawFirm' as LawFirm,
'TransferAgent' as TransferAgent,
'IndusCode' as IndusCode,
'FirstTradingDate' as FirstTradingDate,
'SecLevel' as SecLevel,
'TOFCurrency' as TOFCurrency,
'TotalOfferSize' as TotalOfferSize,
'MIC' as MIC,
'Notes' as Notes


-- # 1

select 
AnnounceDate as Created,
DATE_FORMAT(@time, '%Y/%m/%d') as Modified,
RumID as ID,
'R' as Status,
Actflag as Actflag,
IssuerName,
'' as CntryofIncorp,
ExchgCD,
'' as ISIN,
'' as Sedol,
'' as USCode,
LocalCode as Symbol,
'' as SecDescription,
'' as PVCurrency,
'' as ParValue,
SectyCD as SecType,
'' as LotSize,
'' as SubPeriodFrom,
'' as SubperiodTo,
'' as MinSharesOffered,
NoOfShares as MaxSharesOffered,
'' as SharesOutstanding,
CurenCD as Currency,
'' as SharePriceLowest,
IssPrice as SharePriceHighest,
'' as ProposedPrice,
'' as InitialPrice,
'' as InitialTradedVolume,
'' as Underwriter,
'' as DealType,
'' as LawFirm,
'' as TransferAgent,
'' as IndusCode,
'' as FirstTradingDate,
'' as SecLevel,
'' as TOFCurrency,
case when IssValue <>'' then (IssValue * 1000000 ) else null end as TotalOfferSize,
'' as MIC,
Notes as Notes
from wca.rum
where 
wca.rum.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.rum.IPOID is NULL
and (substring(exchgcd,1,2)='CA'
or substring(exchgcd,1,2)='CN'
or substring(exchgcd,1,2)='DE'
or substring(exchgcd,1,2)='HK'
or substring(exchgcd,1,2)='IN'
or substring(exchgcd,1,2)='JP'
or substring(exchgcd,1,2)='SG'
or substring(exchgcd,1,2)='GB')

-- # 2

select 
wca.ipo.AnnounceDate as Created,
DATE_FORMAT(@time, '%Y/%m/%d') as Modified,
wca.ipo.IpoID+100000 as ID,
substring(wca.ipo.IPOStatus,1,1) as Status,
wca.ipo.Actflag,
wca.issur.Issuername as Issuer,
wca.issur.CntryofIncorp as CntryInc,
wca.scexh.ExchgCd,
wca.scmst.ISIN,
wca.sedol.Sedol,
wca.scmst.USCode,
wca.scexh.Localcode as Symbol,
wca.scmst.SecurityDesc as SecDescription,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ParValue,
wca.scmst.SecTyCD as Sectype,
wca.scexh.Lot as LotSize,
wca.ipo.SubPeriodFrom,
wca.ipo.SubperiodTo,
wca.ipo.MinSharesOffered,
wca.ipo.MaxSharesOffered,
wca.ipo.SharesOutstanding,
wca.ipo.TOFCurrency as Currency,
wca.ipo.SharePriceLowest,
wca.ipo.SharePriceHighest,
wca.ipo.ProposedPrice,
wca.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.ipo.Underwriter,
wca.ipo.DealType,
wca.ipo.LawFirm,
wca.ipo.TransferAgent,
'' as IndusCode,
wca.ipo.FirstTradingDate,
'' as SecLevel,
wca.ipo.TOFCurrency,
case when wca.ipo.TotalOfferSize<>'' then ((wca.ipo.TotalOfferSize )*1000000 ) else null end as TotalOfferSize,
wca.exchg.mic as MIC,
wca.ipo.Notes
from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.ipo.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
where
(wca.ipo.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or 
(wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
or 
(wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
or 
(wca.sedol.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
     and DATE_SUB(CURDATE(),INTERVAL 60 day)<wca.ipo.AnnounceDate)
)
and (wca.exchg.cntrycd='CA'
or wca.exchg.cntrycd='CN'
or wca.exchg.cntrycd='DE'
or wca.exchg.cntrycd='HK'
or wca.exchg.cntrycd='IN'
or wca.exchg.cntrycd='JP'
or wca.exchg.cntrycd='SG'
or wca.exchg.cntrycd='GB')
and wca.scexh.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scmst.primaryexchgcd = wca.scexh.exchgcd