-- arc=y
-- arp=n:\bespoke\iress\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
vtab.ActFlag,
case when (ifnull(icc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(lcc.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(icc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then icc.acttime
     when (ifnull(lcc.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > rd.acttime)
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(exdt.acttime,'2000-01-01'))
      and (ifnull(lcc.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then lcc.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > ifnull(exdt.acttime,'2000-01-01'))
      and (rd.acttime > ifnull(pexdt.acttime,'2000-01-01'))
     then rd.acttime
     when (ifnull(exdt.acttime,'2000-01-01') > vtab.acttime)
      and (ifnull(exdt.acttime,'2000-01-01') > ifnull(pexdt.acttime,'2000-01-01'))
     then ifnull(exdt.acttime,'2000-01-01')
     when (ifnull(pexdt.acttime,'2000-01-01') > vtab.acttime)
     then ifnull(pexdt.acttime,'2000-01-01')
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
case when (wca.sdchg.sdchgid is null or wca.sdchg.newsedol = '') then wca.sedol.sedol else wca.sdchg.oldsedol end as Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'ParallelTradingEnd' as Date12Type,
case when wca.lstat.effectivedate is not null 
     then date_sub(max(wca.lstat.effectivedate), interval '1' day) 
     else null 
     end as EffectiveDate,
'S' as Paytype,
wca.rd.RdID,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.OldRatio as RatioOld,
vtab.NewRatio as RatioNew,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'Currency' as Field3Name,
vtab.CurenCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'TradingIsin' as Field18Name,
tlscmst.isin as Field18,
'TradingUSCode' as Field19Name,
tlscmst.uscode as Field19,
'TradingSedol' as Field20Name,
tlsedol.sedol as Field20,
'TradingLocalcode' as Field21Name,
tlscexh.localcode as Field21,
'OutturnUSCode' as Field22Name,
wca.icc.newuscode as Field22,
'OutturnSedol' as Field23Name,
wca.sdchg.newsedol as Field23,
'OutturnLocalCode' as Field24Name,
wca.lcc.newlocalcode as Field24
from wca.v10s_consd as vtab
inner wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol ON wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER join wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.ExchgCD = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.sdchg on vtab.eventid = wca.sdchg.RelEventID and vtab.eventcd = wca.sdchg.eventtype and 'D'<>wca.sdchg.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.ExchgCD = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.tlelk on vtab.eventid = wca.tlelk.eventid
                                 and vtab.eventcd=wca.tlelk.eventtype
                                 and 'D'<>wca.tlelk.actflag
left outer join wca.scmst as tlscmst on wca.tlelk.secid = tlscmst.secid
left outer join wca.scexh as tlscexh on wca.tlelk.secid = tlscexh.secid
                                 and wca.scexh.ExchgCD=tlscexh.exchgcd
left outer join wca.sedol as tlsedol on tlelk.secid = tlsedol.secid
                                 and wca.exchg.CntryCD=tlsedol.cntrycd
left outer join wca.lstat on tlscexh.secid=wca.lstat.secid
                                 and tlscexh.exchgcd = wca.lstat.exchgcd
where
(vtab.acttime>=(select date_sub(max(acttime), interval 20000 minute) from wca.tbl_opslog)
 or wca.rd.acttime>=(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(wca.exdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(pexdt.acttime,'2000-01-01')>=(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(wca.icc.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(wca.sdchg.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(wca.lcc.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(wca.lstat.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(tlscmst.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(tlscexh.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
 or ifnull(tlsedol.acttime,'2000-01-01')>(select date_sub(max(acttime), interval 20 minute) from wca.tbl_opslog)
)
and (wca.exchg.CntryCD = 'AU'
  or wca.exchg.CntryCD = 'BE'
  or wca.exchg.CntryCD = 'CA'
  or wca.exchg.CntryCD = 'CN'
  or wca.exchg.CntryCD = 'DE'
  or wca.exchg.CntryCD = 'FR'
  or wca.exchg.CntryCD = 'GB'
  or wca.exchg.CntryCD = 'HK'
  or wca.exchg.CntryCD = 'ID'
  or wca.exchg.CntryCD = 'IN'
  or wca.exchg.CntryCD = 'JP'
  or wca.exchg.CntryCD = 'KR'
  or wca.exchg.CntryCD = 'MY'
  or wca.exchg.CntryCD = 'NL'
  or wca.exchg.CntryCD = 'NZ'
  or wca.exchg.CntryCD = 'PH'
  or wca.exchg.CntryCD = 'PT'
  or wca.exchg.CntryCD = 'SG'
  or wca.exchg.CntryCD = 'TH'
  or wca.exchg.CntryCD = 'TW'
  or wca.exchg.CntryCD = 'US'
  or wca.exchg.CntryCD = 'ZA');
