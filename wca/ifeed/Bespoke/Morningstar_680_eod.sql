-- arp=n:\bespoke\morningstar\
-- fsx=_680
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_mornstar_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
order by eventcd, eventid
