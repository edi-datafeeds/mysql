-- arp=n:\bespoke\msci\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680i_dtcc_temp
where (exchgcd='USAMEX'
or exchgcd='USNYSE'
or exchgcd='USPAC');
