-- arp=n:\bespoke\spiderrock\
-- fsx=select concat('_SRF','_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_SpiderRock_SRF_
-- dfn=l
-- fty=n

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (wca.scmst.acttime > scexh.acttime)
      and (wca.scmst.acttime > issur.acttime)
      and (wca.scmst.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(bbebnd.acttime, '2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scmst.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.scmst.acttime
     when (wca.scexh.acttime > issur.acttime)
      and (wca.scexh.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(bbebnd.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.scexh.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.scexh.acttime
     when (wca.issur.acttime > ifnull(wca.bbc.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.bbe.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(bbebnd.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (wca.issur.acttime > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.issur.acttime
     when (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.bbe.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(bbebnd.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbc.acttime,'2000-01-01')>ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.bbc.acttime
     when (ifnull(wca.bbe.acttime,'2000-01-01')>ifnull(bbebnd.acttime,'2000-01-01'))
      and (ifnull(wca.bbe.acttime,'2000-01-01')>ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (ifnull(wca.bbe.acttime,'2000-01-01')>ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.bbe.acttime
     when (ifnull(bbebnd.acttime,'2000-01-01')>ifnull(wca.mktsg.acttime,'2000-01-01'))
      and (ifnull(bbebnd.acttime,'2000-01-01')>ifnull(wca.exchg.acttime,'2000-01-01'))
     then bbebnd.acttime
     when (ifnull(wca.mktsg.acttime,'2000-01-01') > ifnull(wca.exchg.acttime,'2000-01-01'))
     then wca.mktsg.acttime
     else wca.exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
case when ifnull(wca.bbc.curencd,'') <> '' then wca.bbc.curencd
     when ifnull(wca.bbe.curencd,'') <> '' then wca.bbe.curencd
     when ifnull(bbebnd.curencd,'') <> '' then bbebnd.curencd
     else ''
     end as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' 
     then wca.bbe.bbgexhid
     when ifnull(bbebnd.bbgexhid,'')<>''
     then bbebnd.bbgexhid
     else ''
     end as BbgGlobalID,
case when ifnull(wca.bbe.bbgexhtk,'')<>''
     then wca.bbe.bbgexhtk
     when ifnull(bbebnd.bbgexhtk,'')<>''
     then bbebnd.bbgexhtk
     else ''
     end as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbe as bbebnd on wca.scmst.secid = bbebnd.secid and ''=bbebnd.exchgcd and 'D'<> bbebnd.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
WHERE
(3>wca.sectygrp.secgrpid
or ((wca.scmst.sectycd='BND'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and (wca.exchg.cntrycd='US' and wca.scexh.exchgcd<>'USBND')))
and (wca.scmst.acttime >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
or (wca.scexh.acttime >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (wca.issur.acttime >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.bbc.acttime,'2000-01-01') >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.bbe.acttime,'2000-01-01') >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (ifnull(bbebnd.acttime,'2000-01-01') >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.mktsg.acttime,'2000-01-01') >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog))
or (ifnull(wca.exchg.acttime,'2000-01-01') >(select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)))
and wca.exchg.cntrycd='US';