-- arc=y
-- arp=n:\upload\acc\254\
-- fsx=_Alert_680
-- hpx=EDI_WCA_680_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')

-- # 1

select * from wca2.t680_temp
where
isin in (select code from client.pfisin where accid = 254 and actflag<>'D')
and ((eventcd='div' and date1>=(select max(feeddate) from wca.tbl_opslog where seq=3) and date1<@todate)
or (eventcd='div' and changed>(select max(feeddate) from wca.tbl_opslog where seq=3)));