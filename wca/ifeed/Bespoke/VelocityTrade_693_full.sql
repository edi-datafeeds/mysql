-- arp=n:\bespoke\velocitytrade\
-- fsx=_Full_693
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_693_
-- dfn=l
-- fty=n

-- # 1

select distinct
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as SedolID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
where
wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'
and (wca.scexh.exchgcd='ATVSE'
or wca.scexh.exchgcd='AUASX'
or wca.scexh.exchgcd='BEENB'
or wca.scexh.exchgcd='CATSE'
or wca.scexh.exchgcd='CHSSX'
or wca.scexh.exchgcd='CZPSE'
or wca.scexh.exchgcd='DEXETR'
or wca.scexh.exchgcd='DKCSE'
or wca.scexh.exchgcd='EETSE'
or wca.scexh.exchgcd='ESBBSE'
or wca.scexh.exchgcd='ESBSE'
or wca.scexh.exchgcd='ESMSE'
or wca.scexh.exchgcd='ESVSE'
or wca.scexh.exchgcd='FIHSE'
or wca.scexh.exchgcd='FRPEN'
or wca.scexh.exchgcd='GBLSE'
or wca.scexh.exchgcd='HKSEHK'
or wca.scexh.exchgcd='IEISE'
or wca.scexh.exchgcd='ILTSE'
or wca.scexh.exchgcd='ITMSE'
or wca.scexh.exchgcd='JPTSE'
or wca.scexh.exchgcd='NLENA'
or wca.scexh.exchgcd='NOOB'
or wca.scexh.exchgcd='NZSE'
or wca.scexh.exchgcd='PLWSE'
or wca.scexh.exchgcd='PTBVL'
or wca.scexh.exchgcd='SESSE'
or wca.scexh.exchgcd='SGSSE'
or wca.scexh.exchgcd='TRIMKB'
or wca.scexh.exchgcd='USNASD'
or wca.scexh.exchgcd='USNYSE'
or wca.scexh.exchgcd='ZAJSE')