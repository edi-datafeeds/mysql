-- arc=y
-- arp=n:\bespoke\tradesmith\
-- fsx=select concat('_683','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_683_
-- dfn=l
-- fty=n

-- # 1

select * from wca2.t683_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcd='USAMEX' or exchgcd='USNYSE' or exchgcd='USPAC');
