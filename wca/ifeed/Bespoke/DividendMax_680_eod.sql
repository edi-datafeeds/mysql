-- arp=n:\bespoke\dividendmax\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'AGM'
or eventcd = 'BON'
or eventcd = 'CALL'
or eventcd = 'CONSD'
or eventcd = 'DIST'
or eventcd = 'DIV'
or eventcd = 'DMRGR'
or eventcd = 'ENT'
or eventcd = 'ICC'
or eventcd = 'ISCHG'
or eventcd = 'LCC'
or eventcd = 'LSTAT'
or eventcd = 'MRGR'
or eventcd = 'NLIST'
or eventcd = 'PRCHG'
or eventcd = 'RCAP'
or eventcd = 'RTS'
or eventcd = 'SCSWP'
or eventcd = 'SD'
or eventcd = 'SECRC'
or eventcd = 'TKOVR')

