-- arc=y
-- arp=n:\upload\acc\109\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Columba_Reference
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=
-- hsx=_Columba_Reference
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select distinct wca.issur.Issuername,
client.pfisin.code as Isin,
wca.scmst.SectyCD,
case when wca.scmst.Statusflag = '' then 'A' else wca.scmst.statusflag end as Statusflag,
wca.scexh.ExchgCD,
case when wca.scexh.Liststatus = '' or wca.scexh.liststatus='R' or wca.scexh.liststatus='N'
     then 'L'
     else wca.scexh.liststatus
     end as Liststatus,
'' as SuccessorIsin,
'' as IsinChangeDate
from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where client.pfisin.accid = 109 and client.pfisin.actflag<>'D'
and (substring(wca.scexh.exchgcd,1,2)='BE' or substring(wca.scexh.exchgcd,1,2)='FR') 
UNION
select distinct 
'' as Issuername,
client.pfisin.code as Isin,
wca.scmst.SectyCD,
case when wca.scmst.Statusflag = '' then 'A' else wca.scmst.statusflag end as Statusflag,
'' as ExchgCD,
'' as Liststatus,
si.newisin as SuccessorIsin,
si.effectivedate as IsinChangeDate
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.icc as si on client.pfisin.code = si.oldisin and 'D' <> si.actflag
where 
client.pfisin.accid = 109 and client.pfisin.actflag<>'D'
and wca.scmst.secid is null
and (si.iccid is null 
or si.iccid = (select subicc.iccid from wca.icc as subicc where si.secid = subicc.secid
and subicc.actflag<>'D'
and subicc.eventtype <> 'CORR' and subicc.eventtype <> 'CLEAN'
order by effectivedate desc limit 1))