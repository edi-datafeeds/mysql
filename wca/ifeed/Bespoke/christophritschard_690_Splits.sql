-- arp=n:\bespoke\Christoph_Ritschard\
-- hpx=EDI_WCA_690_
-- dfn=l
-- fex=_690.txt
-- fty=y

-- # 1

select * from wca2.t690_temp
where
exchgcntry='CH'
and changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'BON' or eventcd = 'CAPRD' or eventcd = 'CONSD' or eventcd = 'SD')