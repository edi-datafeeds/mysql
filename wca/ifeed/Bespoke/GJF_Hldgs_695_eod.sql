-- arp=N:\Bespoke\GJF_Hldgs\
-- hpx=EDI_WCA_695_
-- dfn=l
-- fex=_695.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and exchgcntry='US'
and (eventcd='consd'
or eventcd='icc'
or eventcd='ischg'
or eventcd='prchg'
or eventcd='sd')
order by eventcd;