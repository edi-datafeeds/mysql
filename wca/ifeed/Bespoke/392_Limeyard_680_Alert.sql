-- arp=n:\bespoke\limeyard\
-- fsx=_Alert_680
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_Alert_
-- dfn=l
-- fty=y

-- # 1 
SELECT * from wca2.t680_temp
where
eventcd = 'agm' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 2 
SELECT * from wca2.t680_temp
where
eventcd = 'ann' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 3
SELECT * from wca2.t680_temp
where
eventcd = 'arr' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 4
SELECT * from wca2.t680_temp
where
eventcd = 'assm'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 5
SELECT * from wca2.t680_temp
where
eventcd = 'bb' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 6 
SELECT * from wca2.t680_temp
where
eventcd = 'bkrp' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 7
SELECT * from wca2.t680_temp
where
eventcd = 'bon' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 8
SELECT * from wca2.t680_temp
where
eventcd = 'br' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date6,'2000-01-01')>=@fromdate and ifnull(date6,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date6,'2000-01-01') < now()
and ifnull(date6,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 9
SELECT * from wca2.t680_temp
where
eventcd = 'call' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 10
SELECT * from wca2.t680_temp
where
eventcd = 'caprd' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 11
SELECT * from wca2.t680_temp
where
eventcd = 'consd'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 12
SELECT * from wca2.t680_temp
where
eventcd = 'conv' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 13
SELECT * from wca2.t680_temp
where
eventcd = 'ctx' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 14
SELECT * from wca2.t680_temp
where
eventcd = 'currd' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 15
SELECT * from wca2.t680_temp
where
eventcd = 'dist'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 16
SELECT * from wca2.t680_temp
where
eventcd = 'div'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 17
SELECT * from wca2.t680_temp
where
eventcd = 'dmrgr' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 18
SELECT * from wca2.t680_temp
where
eventcd = 'drip' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date4,'2000-01-01')>=@fromdate and ifnull(date4,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date4,'2000-01-01') < now()
and ifnull(date4,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 19
SELECT * from wca2.t680_temp
where
eventcd = 'dvst' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 20
SELECT * from wca2.t680_temp
where
eventcd = 'ent'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 21
SELECT * from wca2.t680_temp
where
eventcd = 'frank' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 22
SELECT * from wca2.t680_temp
where
eventcd = 'ftt' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 23
SELECT * from wca2.t680_temp
where
eventcd = 'fychg'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 24
SELECT * from wca2.t680_temp
where
eventcd = 'icc' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 25
SELECT * from wca2.t680_temp
where
eventcd = 'inchg' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 26
SELECT * from wca2.t680_temp
where
eventcd = 'ischg'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 27
SELECT * from wca2.t680_temp
where
eventcd = 'lawst'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 28
SELECT * from wca2.t680_temp
where
eventcd = 'lcc'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 29
SELECT * from wca2.t680_temp
where
eventcd = 'liq' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 30
SELECT * from wca2.t680_temp
where
eventcd = 'lstat' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 31
SELECT * from wca2.t680_temp
where
eventcd = 'ltchg' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 32
SELECT * from wca2.t680_temp
where
eventcd = 'mkchg'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 33
SELECT * from wca2.t680_temp
where
eventcd = 'mrgr'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 34
SELECT * from wca2.t680_temp
where
eventcd = 'nlist' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 35
SELECT * from wca2.t680_temp
where
eventcd = 'oddlt' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 36
SELECT * from wca2.t680_temp
where
eventcd = 'pid' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 37
SELECT * from wca2.t680_temp
where
eventcd = 'po' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 38
SELECT * from wca2.t680_temp
where
eventcd = 'prchg'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 39
SELECT * from wca2.t680_temp
where
eventcd = 'prf' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 40
SELECT * from wca2.t680_temp
where
eventcd = 'pvrd'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 41
SELECT * from wca2.t680_temp
where
eventcd = 'rcap' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 42
SELECT * from wca2.t680_temp
where
eventcd = 'redem'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 43
SELECT * from wca2.t680_temp
where
eventcd = 'rts'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 44
SELECT * from wca2.t680_temp
where
eventcd = 'scchg'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 45
SELECT * from wca2.t680_temp
where
eventcd = 'scswp'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 46
SELECT * from wca2.t680_temp
where
eventcd = 'sd' 
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 47
SELECT * from wca2.t680_temp
where
eventcd = 'secrc'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date1,'2000-01-01')>=@fromdate and ifnull(date1,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date1,'2000-01-01') < now()
and ifnull(date1,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 48
SELECT * from wca2.t680_temp
where
eventcd = 'tkovr'
and secid in (select secid from client.pfisin where accid = 392 and client.pfisin.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));