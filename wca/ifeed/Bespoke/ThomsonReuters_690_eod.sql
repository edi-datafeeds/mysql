-- arc=y
-- arp=n:\bespoke\thomsonreuters\
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- fsx=_690
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcd='USNYSE' or exchgcd='USAMEX' or exchgcd='USPAC')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
