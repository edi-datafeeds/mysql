-- arp=n:\bespoke\iress\
-- fsx=select concat('_Iress','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_SRF_Iress_
-- dfn=l
-- fty=y

-- #
select distinct
concat('1',sce.ScexhID) as ListID,
case when exc.actflag = 'D' then 'D'
     when scm.actflag = 'D' then 'D'
     when sce.actflag = 'D' then 'D'
     else sce.actflag end as ListActFlag,
case when sce.acttime > @fromdate then sce.acttime
     when scm.acttime > @fromdate then scm.acttime
     when iss.acttime > @fromdate then iss.acttime
     when exc.acttime > @fromdate then exc.acttime
     when sed.acttime > @fromdate then sed.acttime
     else sce.acttime
     end as ListChangeDT,
sce.AnnounceDate as ListCreateDT,
case when scm.Statusflag = '' then 'A'
     else scm.Statusflag end as GlobalActiveFlag,
scm.SecID,
scm.IssID,
iss.IssuerName,
scm.SecurityDesc as SecurityDescription,
scm.Parvalue,
scm.CurenCD as ParvalueCurenCD,
scm.isin,
scm.uscode,
scm.SectyCD,
scm.PrimaryExchgCD as PrimaryExchgCD,
exc.CntryCD as ListCntryCD,
ifnull(sed.RcntryCD,'') as RegisterCntryCD,
case when ifnull(pri.currency,'')<>'' 
      and ifnull(pri.currency,'')<>ifnull(sed.curencd,'')
      and ifnull(pri.currency,'')<>'KWF'
     then ifnull(pri.currency,'')
     when ifnull(sed.curencd,'')<>''
     then sed.curencd
     else scm.curencd
     end as TradingCurenCD,
ifnull(sed.Sedol,'') as Sedol,
ifnull(sed.defunct,'') as SedolDefunct,
sce.ExchgCD,
ifnull(exc.Mic,'') as Mic,
case when sce.ListStatus='D' or sce.ListStatus='D' 
     then sce.ListStatus
     else 'L'
     end as ListStatus,
sce.LocalCode,
sce.listdate as ListDT,
sce.delistdate as DelistDT
from wca.scexh as sce
inner join wca.exchg as exc on sce.exchgcd=exc.exchgcd
inner join wca.scmst as scm on sce.secid=scm.secid
inner join wca.issur as iss on scm.issid=iss.issid
left outer join wca2.e2020_bbg as bbg on sce.secid=bbg.secid and sce.exchgcd=bbg.exchgcd
left outer join prices.lasttrade as pri on sce.secid=pri.secid 
               and sce.exchgcd=pri.exchgcd 
			   and pri.pricedate>'2020/06/01'
left outer join wca2.e2020_sedol as sed on sce.secid=sed.secid and exc.cntrycd=sed.cntrycd
where
(sce.exchgcd='MABVC'
or sce.exchgcd='GHGSE'
or sce.exchgcd='CIBVA'
or sce.exchgcd='TTTTSE'
or sce.exchgcd='TNTSE'
or sce.exchgcd='UGKSE'
or sce.exchgcd='LKCSE'
or sce.exchgcd='BDDSE'
or sce.exchgcd='AEADSM'
or sce.exchgcd='JOASE'
or sce.exchgcd='BHBSE'
or sce.exchgcd='AEDFM'
or sce.exchgcd='KWKSE'
or sce.exchgcd='OMMSM'
or sce.exchgcd='AEDIFX'
or sce.exchgcd='QADSM')
and sce.actflag<>'D'
and scm.actflag<>'D'
and iss.actflag<>'D'
and exc.actflag<>'D'
order by sce.exchgcd, bbg.secid;
