-- arc=y
-- arp=n:\bespoke\vtfintech\
-- fsx=_689
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_689_
-- dfn=l
-- fty=n

-- # 1

select * from wca2.t689_temp
where changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='CONSD' or eventcd='SD')
order by eventcd, eventid;