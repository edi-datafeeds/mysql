-- arp=n:\bespoke\fisaffin\
-- hpx=EDI_WCA_690_
-- dfn=l
-- fex=_690.txt
-- fty=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')

-- # 1
select * from wca2.t690_temp
where
changed>(select date_sub(acttime, interval '20' minute) from wca.tbl_opslog
order by acttime desc
limit 2, 1)
and exchgcntry='MY'
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch';