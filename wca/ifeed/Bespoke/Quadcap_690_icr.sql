-- arc=y
-- arp=n:\bespoke\quadcap\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd<>'SHOCH' and eventcd<>'DPRCP' and eventcd<>'DRCHG'
and (exchgcntry='AU'
or exchgcntry='BE'
or exchgcntry='BR'
or exchgcntry='CA'
or exchgcntry='CH'
or exchgcntry='CL'
or exchgcntry='CN'
or exchgcntry='CO'
or exchgcntry='DE'
or exchgcntry='DK'
or exchgcntry='ES'
or exchgcntry='FI'
or exchgcntry='FR'
or exchgcntry='GB'
or exchgcntry='HK'
or exchgcntry='ID'
or exchgcntry='IE'
or exchgcntry='IL'
or exchgcntry='IN'
or exchgcntry='IT'
or exchgcntry='JP'
or exchgcntry='KR'
or exchgcntry='LU'
or exchgcntry='MY'
or exchgcntry='MX'
or exchgcntry='NL'
or exchgcntry='NO'
or exchgcntry='PL'
or exchgcntry='PT'
or exchgcntry='RU'
or exchgcntry='SA'
or exchgcntry='SE'
or exchgcntry='SG'
or exchgcntry='TH'
or exchgcntry='TR'
or exchgcntry='TW'
or exchgcntry='US'
or exchgcntry='ZA')
