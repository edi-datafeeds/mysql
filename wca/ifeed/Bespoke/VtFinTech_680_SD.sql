-- arc=y
-- arp=n:\bespoke\vtfintech\SD\
-- fsx=_680
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

SELECT * from wca2.t680_temp
where
eventcd = 'SD' 
and (date1 = (select max(feeddate) from wca.tbl_opslog)
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and date1 < now()
and date1 > date_sub(now(), interval 7 day)));