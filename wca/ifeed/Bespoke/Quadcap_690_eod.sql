-- arc=y
-- arp=n:\Bespoke\quadcap\
-- hpx=EDI_WCA_690_
-- dfn=l
-- fex=_690.txt
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd<>'SHOCH' and eventcd<>'DPRCP' and eventcd<>'DRCHG'
and (exchgcntry='AU'
or exchgcntry='BE'
or exchgcntry='BR'
or exchgcntry='CA'
or exchgcntry='CH'
or exchgcntry='CL'
or exchgcntry='CN'
or exchgcntry='CO'
or exchgcntry='DE'
or exchgcntry='DK'
or exchgcntry='ES'
or exchgcntry='FI'
or exchgcntry='FR'
or exchgcntry='GB'
or exchgcntry='HK'
or exchgcntry='ID'
or exchgcntry='IE'
or exchgcntry='IL'
or exchgcntry='IN'
or exchgcntry='IT'
or exchgcntry='JP'
or exchgcntry='KR'
or exchgcntry='LU'
or exchgcntry='MY'
or exchgcntry='MX'
or exchgcntry='NL'
or exchgcntry='NO'
or exchgcntry='PL'
or exchgcntry='PT'
or exchgcntry='RU'
or exchgcntry='SA'
or exchgcntry='SE'
or exchgcntry='SG'
or exchgcntry='TH'
or exchgcntry='TR'
or exchgcntry='TW'
or exchgcntry='US'
or exchgcntry='ZA')
