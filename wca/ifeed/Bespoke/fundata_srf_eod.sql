-- arp=n:\bespoke\fundata\
-- hpx=EDI_WCA_SRF_
-- dfn=l
-- fex=.txt
-- fsx=_Fundata
-- fty=y

-- # 1

select wca2.t683_temp.*,
issurxtra.acttime as issuerchanged,
issurxtra.announcedate as issuercreated,
scmstxtra.acttime as securitychanged,
scmstxtra.announcedate as securitycreated,
scexhxtra.acttime as securityexchangechanged,
scexhxtra.announcedate as securityexchangecreated,
exchgxtra.acttime as exchangegchanged,
scmstxtra.parentsecid,
scmstxtra.issueprice,
scmstxtra.paidupvalue,
scmstxtra.statusreason,
scmstxtra.voting,
scmstxtra.sharesoutstanding,
scexhxtra.lot,
scexhxtra.mintrdgqty,
scexhxtra.listdate,
exchgxtra.exchgname
from wca2.t683_temp
inner join wca.scmst as scmstxtra on wca2.t683_temp.secid = scmstxtra.secid
inner join wca.issur as issurxtra on wca2.t683_temp.issid = issurxtra.issid
inner join wca.scexh as scexhxtra on wca2.t683_temp.scexhid = scexhxtra.secid
inner join wca.exchg as exchgxtra on wca2.t683_temp.ExchgCd = exchgxtra.exchgcd
where wca2.t683_temp.changed >(select max(feeddate) from wca.tbl_opslog where seq = 3);