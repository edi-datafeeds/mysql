-- arc=y
-- arp=n:\upload\acc\303\
-- fsx=_695
-- hpx=EDI_WCA_695_
-- dfn=l

-- # 1

select wca2.t695_temp.*
from wca2.t695_temp
left outer join wca.icc on wca2.t695_temp.eventcd = wca.icc.eventtype and wca2.t695_temp.eventid = wca.icc.releventid
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and concat(wca2.t695_temp.isin, wca2.t695_temp.mic) in (select concat(client.pfisinmic.code, client.pfisinmic.mic) from client.pfisinmic where accid=@accid and actflag='U') 
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
(concat(wca2.t695_temp.isin, wca2.t695_temp.mic) in (select concat(client.pfisinmic.code, client.pfisinmic.mic) from client.pfisinmic where accid=@accid and actflag='I') 
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');
