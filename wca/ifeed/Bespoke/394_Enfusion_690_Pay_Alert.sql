-- arp=n:\bespoke\enfusion\
-- fsx=_Alert_Pay_690
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fty=y

-- # 1 
SELECT * from wca2.t690_temp
where
eventcd = 'arr' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 2
SELECT * from wca2.t690_temp
where
eventcd = 'bb' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date4,'2000-01-01')>=@fromdate and ifnull(date4,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date4,'2000-01-01') < now()
and ifnull(date4,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 3
SELECT * from wca2.t690_temp
where
eventcd = 'bon' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 4
SELECT * from wca2.t690_temp
where
eventcd = 'br' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date8,'2000-01-01')>=@fromdate and ifnull(date8,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date8,'2000-01-01') < now()
and ifnull(date8,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 5
SELECT * from wca2.t690_temp
where
eventcd = 'caprd' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 6
SELECT * from wca2.t690_temp
where
eventcd = 'consd'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 7
SELECT * from wca2.t690_temp
where
eventcd = 'dist'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 8
SELECT * from wca2.t690_temp
where
eventcd = 'div'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and (((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate)
	or (ifnull(date4,'2000-01-01')>=@fromdate and ifnull(date4, '2000-01-01') <=@todate))
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ((ifnull(date3,'2000-01-01') < now() and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))
	or (ifnull(date4,'2000-01-01') < now() and ifnull(date4,'2000-01-01') > date_sub(now(), interval 7 day))
)));
     
-- # 9
SELECT * from wca2.t690_temp
where
eventcd = 'dmrgr' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 10
SELECT * from wca2.t690_temp
where
eventcd = 'drip' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 11
SELECT * from wca2.t690_temp
where
eventcd = 'dvst' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 12
SELECT * from wca2.t690_temp
where
eventcd = 'ent'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 13
SELECT * from wca2.t690_temp
where
eventcd = 'frank' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 14
SELECT * from wca2.t690_temp
where
eventcd = 'liq' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date2,'2000-01-01')>=@fromdate and ifnull(date2,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date2,'2000-01-01') < now()
and ifnull(date2,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 15
SELECT * from wca2.t690_temp
where
eventcd = 'mrgr'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 16
SELECT * from wca2.t690_temp
where
eventcd = 'oddlt' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date4,'2000-01-01')>=@fromdate and ifnull(date4,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date4,'2000-01-01') < now()
and ifnull(date4,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 17
SELECT * from wca2.t690_temp
where
eventcd = 'po' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 18
SELECT * from wca2.t690_temp
where
eventcd = 'prf' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 19
SELECT * from wca2.t690_temp
where
eventcd = 'rcap' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 20
SELECT * from wca2.t690_temp
where
eventcd = 'rts'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 21
SELECT * from wca2.t690_temp
where
eventcd = 'scswp'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 22
SELECT * from wca2.t690_temp
where
eventcd = 'sd' 
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date3,'2000-01-01')>=@fromdate and ifnull(date3,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date3,'2000-01-01') < now()
and ifnull(date3,'2000-01-01') > date_sub(now(), interval 7 day))));

-- # 23
SELECT * from wca2.t690_temp
where
eventcd = 'tkovr'
and sedol in (select code from client.pfsedol where accid = 394 and client.pfsedol.actflag <> 'D')
and ((ifnull(date5,'2000-01-01')>=@fromdate and ifnull(date5,'2000-01-01') <=@todate
OR
(changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(date5,'2000-01-01') < now()
and ifnull(date5,'2000-01-01') > date_sub(now(), interval 7 day))));


