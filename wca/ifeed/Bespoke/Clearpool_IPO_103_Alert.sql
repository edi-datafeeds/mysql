-- arp=n:\bespoke\Clearpool\
-- fsx=_IPO_Alert
-- fex=.103
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_IPO_103_Alert_
-- dfn=l
-- fty=y

-- # 1

select 
wca.ipo.AnnounceDate as Created,
DATE_FORMAT(@time, '%Y/%m/%d') as Modified,
wca.ipo.IpoID+100000 as ID,
wca.ipo.IPOStatus as Status,
wca.ipo.Actflag,
wca.issur.Issuername,
wca.issur.CntryofIncorp as CntryInc,
wca.scexh.ExchgCd,
wca.scmst.ISIN,
wca.sedol.Sedol,
wca.scmst.USCode,
wca.scexh.Localcode as Symbol,
wca.scmst.SecurityDesc as SecDescription,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ParValue,
wca.scmst.SecTyCD as Sectype,
wca.scexh.Lot as LotSize,
wca.ipo.SubPeriodFrom,
wca.ipo.SubperiodTo,
wca.ipo.MinSharesOffered,
wca.ipo.MaxSharesOffered,
wca.ipo.SharesOutstanding,
wca.ipo.TOFCurrency as Currency,
wca.ipo.SharePriceLowest,
wca.ipo.SharePriceHighest,
wca.ipo.ProposedPrice,
wca.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.ipo.Underwriter,
wca.ipo.DealType,
wca.ipo.LawFirm,
wca.ipo.TransferAgent,
'' as IndusCode,
wca.ipo.FirstTradingDate,
'' as SecLevel,
wca.ipo.TOFCurrency,
case when wca.ipo.TotalOfferSize<>'' then ((wca.ipo.TotalOfferSize )*1000000 ) else null end as TotalOfferSize,
wca.exchg.mic as MIC,
'' as Notes
from wca.ipo
inner join wca.scmst on wca.ipo.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.ipo.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
where
(wca.ipo.acttime>=(select date_format(max(acttime), '%Y-%m-%d') from wca.tbl_opslog)
or (wca.scmst.acttime>=(select date_format(max(acttime), '%Y-%m-%d') from wca.tbl_opslog) and date_sub(curdate(), interval 60 day)<wca.ipo.announcedate)
or (wca.scexh.acttime>=(select date_format(max(acttime), '%Y-%m-%d') from wca.tbl_opslog) and date_sub(curdate(), interval 60 day)<wca.ipo.announcedate)
or (wca.sedol.acttime>=(select date_format(max(acttime), '%Y-%m-%d') from wca.tbl_opslog) and date_sub(curdate(), interval 60 day)<wca.ipo.announcedate)
or (wca.ipo.firsttradingdate>=@fromdate and wca.ipo.firsttradingdate<=@todate
or (wca.ipo.acttime>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
and ifnull(wca.ipo.firsttradingdate,'2000-01-01') < now()
and ifnull(wca.ipo.firsttradingdate,'2000-01-01') > date_sub(now(), interval 5 day))))
and (substring(wca.scexh.exchgcd,1,2)='US' or substring(wca.scexh.exchgcd,1,2)='CA')
and wca.scexh.actflag<>'D'
and wca.scexh.liststatus<>'D'
and wca.scmst.primaryexchgcd = wca.scexh.exchgcd;