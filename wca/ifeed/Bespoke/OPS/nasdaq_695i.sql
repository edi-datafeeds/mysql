-- fpx=select 'edi_corporate_action_events_'
-- fsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=
-- hdt=SELECT ''
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=
-- eof=SELECT ''
-- dfn=l
-- fty=y
-- arc=n
-- arp=n:\temp\

-- # 1

SELECT * FROM wca2.t695_temp
WHERE
changed >(SELECT DATE_SUB(MAX(acttime), INTERVAL '20' MINUTE) FROM wca.tbl_opslog)
AND eventcd <> 'dprcp' AND eventcd <> 'drchg' AND eventcd <> 'shoch' AND exchgcntry = 'US'
UNION
SELECT * FROM wca2.t695_temp_fi
WHERE
changed >(SELECT DATE_SUB(MAX(acttime), INTERVAL '20' MINUTE) FROM wca.tbl_opslog)
AND eventcd <> 'dprcp' AND eventcd <> 'drchg' AND eventcd <> 'shoch' AND exchgcntry = 'US'
order by eventcd, eventid;
