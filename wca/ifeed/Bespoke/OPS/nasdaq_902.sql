--filepath=O:\Datafeed\Bespoke\Nasdaq_OMX_US\
--filenameprefix=edi_lookup_table_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=edi_lookup_table_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup AS Description
FROM LOOKUPEXTRA
where
(typegroup<>'FRNINDXBEN' and typegroup<>'SENJUN')

union

SELECT
Actflag,
Acttime,
case when upper(TypeGroup) = 'LIQPRI' then 'SENJUN'
     else upper(TypeGroup)
     end as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

SELECT
case when Exchg.Actflag='D' then 'C' else Exchg.Actflag end as Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG

union

SELECT
case when Exchg.Actflag='D' then 'C' else Exchg.Actflag end as Actflag,
Exchg.Acttime,
upper('MICCODE') as TypeGroup,
Exchg.MIC as Code,
Exchg.Exchgname as Lookup
from EXCHG
where MIC <> '' and MIC is not null

union

SELECT distinct
mktsg.Actflag,
mktsg.Acttime,
upper('MICSEG') as TypeGroup,
mktsg.MIC as Code,
mktsg.MKtSegment as Lookup
from mktsg
inner join scexh on mktsg.mktsgid=scexh.mktsgid
inner join exchg on scexh.exchgcd=exchg.exchgcd
where mktsg.MIC <> '' and mktsg.MIC is not null
and mktsg.Actflag<>'D'

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MICMAP') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG

union

SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS

union

SELECT
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY

union

SELECT
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN

union

SELECT
case when event.Actflag<>'D' then 'I' else event.Actflag end as Actflag,
'2005/01/01 15:00:00' as Acttime,
upper('EVENT') as TypeGroup,
event.EventType as Code,
event.EventName as Lookup
from event

union

SELECT
case when lookupextra.Actflag<>'D' then 'I' else lookupextra.Actflag end as Actflag,
'2005/01/01 15:00:00' as Acttime,
upper('EVENT') as TypeGroup,
lookupextra.code as Code,
lookupextra.lookup as Lookup
from lookupextra
where
typegroup='TABLENAME'
and lookupextra.code<>'BOCHG'
and lookupextra.code<>'FRNFX'
and lookupextra.code<>'INCHG'
and lookupextra.code<>'SCCHG'
and lookupextra.code<>'SD'
and lookupextra.code<>'CRCHG'
and lookupextra.code<>'LSTAT'
and lookupextra.code<>'WKN'

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2

order by TypeGroup, Code
