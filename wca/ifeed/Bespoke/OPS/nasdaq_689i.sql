-- arp=n:\no_cull_feeds\689\
-- fpx=select 'edi_event_notes_'
-- fsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=
-- hdt=SELECT ''
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=
-- eof=SELECT ''
-- dfn=l
-- fty=n
-- arc=n

-- # 1

select * from wca2.t689_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
