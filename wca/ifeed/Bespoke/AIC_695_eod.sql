-- arc=y
-- arp=n:\bespoke\aic\
-- fsx=_695
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_695_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcntry='CI'
or exchgcntry='TZ'
or exchgcntry='UG'
or exchgcntry='MW'
or exchgcntry='ZW'
or exchgcntry='ZM'
or exchgcntry='BW'
or exchgcntry='GH'
or exchgcntry='KE'
or exchgcntry='MU'
or exchgcntry='NG')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t695_temp_fi
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcntry='CI'
or exchgcntry='TZ'
or exchgcntry='UG'
or exchgcntry='MW'
or exchgcntry='ZW'
or exchgcntry='ZM'
or exchgcntry='BW'
or exchgcntry='GH'
or exchgcntry='KE'
or exchgcntry='MU'
or exchgcntry='NG')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;