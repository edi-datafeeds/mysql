-- arc=y
-- arp=n:\bespoke\cinnober\
-- fsx=_Alert_690
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1

select * from wca2.t690_temp
where
(isin in (select code from client.pfisin where accid = 278 and actflag<>'D')
and (eventcd='bon' or eventcd='caprd' or eventcd='consd' or eventcd='currd' 
or eventcd='dmrgr' or eventcd='ent' or eventcd='icc' or eventcd='ischg' 
or eventcd='lcc' or eventcd='lstat' or eventcd='nlist' or eventcd='rts'
or eventcd='scswp' or eventcd='sd' or eventcd='secrc' or eventcd='shoch')
and date1>@fromdate and date1<=@todate)
OR
(isin in (select code from client.pfisin where accid = 278 and actflag<>'D')
and (eventcd='bon' or eventcd='caprd' or eventcd='consd' or eventcd='currd' 
or eventcd='dmrgr' or eventcd='ent' or eventcd='icc' or eventcd='ischg' 
or eventcd='lcc' or eventcd='lstat' or eventcd='nlist' or eventcd='rts'
or eventcd='scswp' or eventcd='sd' or eventcd='secrc' or eventcd='shoch')
and changed>date_sub(now(), interval 30 day) and date1 is not null and date1<date_add(now(), interval 1 day));