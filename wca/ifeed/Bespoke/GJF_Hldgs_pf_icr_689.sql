-- arc=y
-- arp=n:\upload\acc\893\
-- fsx=select concat('_689','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_689_
-- dfn=l
-- fty=n

-- # 1

select
EventCD,
EventID,
Changed,
Fieldname,
NotesText
from wca2.t689_temp
where
(changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
AND (eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U'))
OR
(eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='U'))
OR
(eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='U')))
OR
((eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I'))
OR
(eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfuscode where accid=@accid and client.pfuscode.actflag='I'))
OR
(eventcd in ('CONSD','ICC','ISCHG','PRCHG','SD') and pfid in (select secid from client.pfsedol where accid=@accid and client.pfsedol.actflag='I')));
