-- arc=y
-- arp=n:\upload\acc\109\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Columba_Meeting
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=
-- hsx=_Columba_Meeting
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select client.pfisin.code as Isin,
latestagm.agmegm as MeetingType,
latestagm.agmdate as MeetingDate
from client.pfisin
left outer join wca.scmst on client.pfisin.code = wca.scmst.isin
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.agm as latestagm on wca.issur.issid = latestagm.issid 
                        and (latestagm.agmegm='AGM' or latestagm.agmegm='EGM' or latestagm.agmegm='GM')
where
client.pfisin.accid=109 and client.pfisin.actflag<>'D'
and (latestagm.agmid is null
or latestagm.agmid = (select subagm.agmid from wca.agm as subagm where latestagm.issid = subagm.issid
and subagm.actflag<>'D'
and (subagm.agmegm='AGM' or subagm.agmegm='EGM' or subagm.agmegm='GM') ORDER BY subagm.agmdate desc limit 1));