-- arc=y
-- arp=n:\bespoke\smartkarma\
-- fsx=_680
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcntry='HK' or exchgcntry='SG' or exchgcntry='GB' or exchgcntry='US'
or exchgcntry='AU' or exchgcntry='JP' or exchgcntry='CN' or exchgcntry='IN')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t680_temp_prf
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcntry='HK' or exchgcntry='SG' or exchgcntry='GB' or exchgcntry='US'
or exchgcntry='AU' or exchgcntry='JP' or exchgcntry='CN' or exchgcntry='IN')
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;