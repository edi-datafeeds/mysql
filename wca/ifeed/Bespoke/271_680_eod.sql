-- arc=y
-- arp=n:\upload\acc\271\680\
-- fsx=_680
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t680_temp
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (isin in (select code from client.pfisin where accid=@accid and actflag='U') or uscode in (select code from client.pfuscode where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch' and exchgcd = primaryexchgcd)
or
((isin in (select code from client.pfisin where accid=@accid and actflag='I') or uscode in (select code from client.pfuscode where accid=@accid and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH' and exchgcd = primaryexchgcd);
