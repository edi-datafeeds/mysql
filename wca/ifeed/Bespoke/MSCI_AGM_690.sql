-- arc=y
-- arp=n:\bespoke\msciinc\
-- hpx=EDI_WCA_690_
-- fpx=AGM_
-- dfn=l
-- fex=_690.txt
-- fty=n

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd = 'AGM'