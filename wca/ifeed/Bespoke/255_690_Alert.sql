-- arc=y
-- arp=n:\upload\acc\255\
-- fsx=_Alert_690
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fty=y

-- # 1

select * from wca2.t690_temp
where
sedol in (select code from client.pfsedol where accid = 255 and actflag<>'D')
and ((eventcd='agm' and date1>@fromdate and date1<=@todate)
or (eventcd='arr' and date1>@fromdate and date1<=@todate)
or (eventcd='assm' and date1>@fromdate and date1<=@todate)
or (eventcd='bb' and date1>@fromdate and date1<=@todate)
or (eventcd='bkrp' and date1>@fromdate and date1<=@todate)
or (eventcd='bon' and date1>@fromdate and date1<=@todate)
or (eventcd='br' and date6>@fromdate and date6<=@todate)
or (eventcd='call' and date1>@fromdate and date1<=@todate)
or (eventcd='caprd' and date1>@fromdate and date1<=@todate)
or (eventcd='consd' and date1>@fromdate and date1<=@todate)
or (eventcd='conv' and date3>@fromdate and date3<=@todate)
or (eventcd='ctx' and date2>@fromdate and date2<=@todate)
or (eventcd='currd' and date1>@fromdate and date1<=@todate)
or (eventcd='dist' and date1>@fromdate and date1<=@todate)
or (eventcd='div' and date1>@fromdate and date1<=@todate)
or (eventcd='dmrgr' and date1>@fromdate and date1<=@todate)
or (eventcd='dmrgr' and date7>@fromdate and date7<=@todate)
or (eventcd='drchg' and date1>@fromdate and date1<=@todate)
or (eventcd='drip' and date4>@fromdate and date4<=@todate)
or (eventcd='dvst' and date1>@fromdate and date1<=@todate)
or (eventcd='ent' and date1>@fromdate and date1<=@todate)
or (eventcd='frank' and date1>@fromdate and date1<=@todate)
or (eventcd='icc' and date1>@fromdate and date1<=@todate)
or (eventcd='inchg' and date1>@fromdate and date1<=@todate)
or (eventcd='ischg' and date1>@fromdate and date1<=@todate)
or (eventcd='lawst' and date1>@fromdate and date1<=@todate)
or (eventcd='lcc' and date1>@fromdate and date1<=@todate)
or (eventcd='liq' and date2>@fromdate and date2<=@todate)
or (eventcd='lstat' and date1>@fromdate and date1<=@todate)
or (eventcd='ltchg' and date1>@fromdate and date1<=@todate)
or (eventcd='mrgr' and date1>@fromdate and date1<=@todate)
or (eventcd='mrgr' and date5>@fromdate and date5<=@todate)
or (eventcd='nlist' and date1>@fromdate and date1<=@todate)
or (eventcd='po' and date1>@fromdate and date1<=@todate)
or (eventcd='prf' and date1>@fromdate and date1<=@todate)
or (eventcd='pvrd' and date1>@fromdate and date1<=@todate)
or (eventcd='rcap' and date1>@fromdate and date1<=@todate)
or (eventcd='rts' and date1>@fromdate and date1<=@todate)
or (eventcd='scchg' and date1>@fromdate and date1<=@todate)
or (eventcd='scswp' and date1>@fromdate and date1<=@todate)
or (eventcd='sd' and date1>@fromdate and date1<=@todate)
or (eventcd='sdchg' and date1>@fromdate and date1<=@todate)
or (eventcd='secrc' and date1>@fromdate and date1<=@todate)
or (eventcd='shoch' and date1>@fromdate and date1<=@todate)
or (eventcd='tkovr' and date5>@fromdate and date5<=@todate));