-- arp=n:\upload\acc\125\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select distinct
vtab.EventCD,
vtab.EventID,
1 as OptionID,
1 as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
client.pfisin.code as Isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SIC,
stab.CIK,
stab.IndusID,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
stab.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AGMDate' as Date1Type,
vtab.AGMDate as Date1,
'FYEDate' as Date2Type,
vtab.FYEDate as Date2,
'RecDate' as Date3Type,
vtab.RecDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
null as RdID,
null as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'AGMEGM' as Field1Name,
vtab.AGMEGM as Field1,
'AGMNo' as Field2Name,
vtab.AGMNo as Field2,
'AGMTime' as Field3Name,
vtab.AGMTime as Field3,
'Add1' as Field4Name,
vtab.Add1 as Field4,
'Add2' as Field5Name,
vtab.Add2 as Field5,
'Add3' as Field6Name,
vtab.Add3 as Field6,
'Add4' as Field7Name,
vtab.Add4 as Field7,
'Add5' as Field8Name,
vtab.Add5 as Field8,
'Add6' as Field9Name,
vtab.Add6 as Field9,
'City' as Field10Name,
vtab.City as Field10,
'CntryCD' as Field11Name,
vtab.CntryCD as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22,
'' as Field23Name,
'' as Field23,
'' as Field24Name,
'' as Field24
from client.pfisin
left outer join wca.v20c_680_scmst as stab on client.pfisin.code = stab.isin
left outer join wca.issur on stab.issid = wca.issur.issid
left outer join wca.v10s_agm as vtab on wca.issur.issid = vtab.issid
left outer join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER join wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER join wca.exchg ON wca.scexh.ExchgCD = wca.exchg.exchgcd
Where
(vtab.acttime >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and client.pfisin.accid=125 and client.pfisin.actflag='U'
and (wca.exchg.exchgcd = stab.primaryexchgcd or stab.primaryexchgcd = ''))
OR
(client.pfisin.accid=125 and client.pfisin.actflag='I'
and (vtab.agmid is null
or vtab.agmid = (select subagm.agmid from wca.agm as subagm where vtab.issid = subagm.issid
and subagm.actflag<>'D'
and (wca.exchg.exchgcd = stab.primaryexchgcd or stab.primaryexchgcd = '') ORDER BY subagm.agmdate desc limit 1)));


