-- arp=n:\bespoke\tmx\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog where seq=3 order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog where seq=3 order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog where seq=3), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y

-- # 1
DROP TABLE IF EXISTS `wca2`.`t690_TMX_temp`;

-- # Create t690_TMX_temp
CREATE TABLE wca2.t690_TMX_temp like wca2.t690_temp;

-- # Insert into t690_TMX_temp everything from wca2.t690_temp
insert ignore into wca2.t690_TMX_temp select * from wca2.t690_temp 
where
changed >'2020/07/01 16:00:00'
and changed <'2020/07/02 00:00:00'
and eventcd<>'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH'
and (exchgcntry='AD'
or exchgcntry='AL'
or exchgcntry='AM'
or exchgcntry='AT'
or exchgcntry='AZ'
or exchgcntry='BA'
or exchgcntry='BE'
or exchgcntry='BG'
or exchgcntry='BY'
or exchgcntry='CA'
or exchgcntry='CH'
or exchgcntry='CY'
or exchgcntry='CZ'
or exchgcntry='DE'
or exchgcntry='DK'
or exchgcntry='EE'
or exchgcntry='ES'
or exchgcntry='FI'
or exchgcntry='FR'
or exchgcntry='GB'
or exchgcntry='GE'
or exchgcntry='GR'
or exchgcntry='HR'
or exchgcntry='HU'
or exchgcntry='IE'
or exchgcntry='IS'
or exchgcntry='IT'
or exchgcntry='LI'
or exchgcntry='LT'
or exchgcntry='LU'
or exchgcntry='LV'
or exchgcntry='MD'
or exchgcntry='ME'
or exchgcntry='MK'
or exchgcntry='MT'
or exchgcntry='NL'
or exchgcntry='NO'
or exchgcntry='PL'
or exchgcntry='PT'
or exchgcntry='RO'
or exchgcntry='RS'
or exchgcntry='SE'
or exchgcntry='SI'
or exchgcntry='SK'
or exchgcntry='SM'
or exchgcntry='UA'
or exchgcntry='US'
or exchgcntry='VA');

-- #
update wca2.t690_TMX_temp
set localcode=
case when LocalCode='BPRAP' or LocalCode='SJIU'
     then LocalCode
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),
                        replace(substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2),'PR','.PR'))
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-4),'.PR.CL')
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),
                        replace(substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3),'PR','.PR.'))
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-4),
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-3,4),'PR','.PR.')
     when sectycd='PRF' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-5),'.PR.',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,1),'.',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2))
     when sectycd='WAR' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'.WS.',
                        substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1))
     when sectycd='WAR' and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),'.WS')
     when (sectycd='CVR' or sectycd='TRT') and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'.RT')
     when (sectycd='CVR' or sectycd='TRT') and substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-2),'.RT')
     when (sectycd='EQS' or sectycd='DR') and securitydesc like '%Class A%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'A'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'.A')
     when (sectycd='EQS' or sectycd='DR') and securitydesc like '%Class B%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'B'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'.B')
     when substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'.WI')
     when substring(replace(localcode,' ',''),length(replace(localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-3),'.WD')
     when securitydesc like 'Unit%' and substring(replace(localcode,' ',''),length(replace(localcode,' ','')),1) = 'U'
          and sectycd='STP'
     then concat(substring(replace(localcode,' ',''),1,length(replace(localcode,' ',''))-1),'.U')
     else LocalCode
     end
where
exchgCD='USNYSE';

-- #
select * from wca2.t690_TMX_temp;
