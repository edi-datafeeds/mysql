-- arc=y
-- fty=y
-- arp=n:\bespoke\moodys\
-- hpx=EDI_Moodys_SRF_
-- dfn=l
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=_Moodys_SRF

-- # 1
select distinct
wca.scexh.ScexhID,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     else '1'
     end as BbgExhSeqnum,
bbcseq.BbcID,
bbeseq.BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scmst.acttime
      when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbcseq.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(bbeseq.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(mktsg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then mktsg.acttime
      when (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbcseq.acttime,'2000-01-01'))
      and (ifnull(exchg.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then exchg.acttime
     when (ifnull(bbcseq.acttime,'2000-01-01') > ifnull(bbeseq.acttime,'2000-01-01'))
     then bbcseq.acttime
     else bbeseq.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.Isin,
wca.scmst.Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.LEI,
wca.issur.NAICS,
wca.scmst.CFI,
wca.issur.shellcompany as ShellComp,
wca.scmst.CIC,
wca.scmst.FISN,
wca.issur.IndusID,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca2.bbcseq as bbcseq on wca.bbc.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on wca.bbe.bbeid=bbeseq.bbeid
where
(3>wca.sectygrp.secgrpid
or (wca.scmst.sectycd='BND' and wca.exchg.cntrycd='US' 
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)))
and (wca.scmst.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     or wca.scexh.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)
     or wca.issur.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     or wca2.bbcseq.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     or wca2.bbeseq.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     or wca.mktsg.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     or wca.exchg.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3) 
     );