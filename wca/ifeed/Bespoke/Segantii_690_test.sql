-- arp=n:\upload\acc\388\
-- fsx=select concat('_690_Alert','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_Alert_
-- dfn=l
-- fty=y

-- # 1
SELECT * from wca2.t690_hist
where
eventcd = 'agm' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 2
SELECT * from wca2.t690_hist
where
eventcd = 'arr' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 3
SELECT * from wca2.t690_hist
where
eventcd = 'assm'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 4
SELECT * from wca2.t690_hist
where
eventcd = 'bb' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date4,'2001-01-01')>=@fromdate and ifnull(date4,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date4,'2001-01-01')<@fromdate and ifnull(date4,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 6
SELECT * from wca2.t690_hist
where
eventcd = 'bon' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 7
SELECT * from wca2.t690_hist
where
eventcd = 'br' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
       or (ifnull(date8,'2001-01-01')>=@fromdate and ifnull(date8,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date8,'2001-01-01')<@fromdate and ifnull(date8,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 8
SELECT * from wca2.t690_hist
where
eventcd = 'call' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 9
SELECT * from wca2.t690_hist
where
eventcd = 'caprd' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 10
SELECT * from wca2.t690_hist
where
eventcd = 'consd'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 11
SELECT * from wca2.t690_hist
where
eventcd = 'conv' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 12
SELECT * from wca2.t690_hist
where
eventcd = 'ctx' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 13
SELECT * from wca2.t690_hist
where
eventcd = 'currd' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 14
SELECT * from wca2.t690_hist
where
eventcd = 'dist'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
       or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 15
SELECT distinct maintab.* from wca2.t690_hist as maintab
left outer join wca.divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join wca.divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join wca.divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
left outer join wca.drip on maintab.eventid=wca.drip.divid and maintab.exchgcntry=drip.cntrycd

where
eventcd = 'div'
and not (mainoptn.divid is not null 
and mainoptn.divtype='C'
and divopt2.divid is null 
and divopt3.divid is null
and wca.drip.divid is null)
and mainoptn.divid is not null
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
      or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
      or (ifnull(date4,'2001-01-01')>=@fromdate and ifnull(date4,'2001-01-01')<=@todate)
      or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
      or (ifnull(date7,'2001-01-01')>=@fromdate and ifnull(date7,'2001-01-01')<=@todate)
      or (ifnull(date11,'2001-01-01')>=@fromdate and ifnull(date11,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
		  (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
		  or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day)) 
		  or (ifnull(date4,'2001-01-01')<@fromdate and ifnull(date4,'2001-01-01')>date_sub(@fromdate, interval 3 day)) 
		  or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day)) 
		  or (ifnull(date7,'2001-01-01')<@fromdate and ifnull(date7,'2001-01-01')>date_sub(@fromdate, interval 3 day)) 
		  or (ifnull(date11,'2001-01-01')<@fromdate and ifnull(date11,'2001-01-01')>date_sub(@fromdate, interval 3 day)) 
         )
     )
    );

-- # 16
SELECT * from wca2.t690_hist
where
eventcd = 'dmrgr' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
       or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
       or (ifnull(date7,'2001-01-01')>=@fromdate and ifnull(date7,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date7,'2001-01-01')<@fromdate and ifnull(date7,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 17
SELECT * from wca2.t690_hist
where
eventcd = 'drip' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date4,'2001-01-01')>=@fromdate and ifnull(date4,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date4,'2001-01-01')<@fromdate and ifnull(date4,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 19
SELECT * from wca2.t690_hist
where
eventcd = 'ent'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 20
SELECT * from wca2.t690_hist
where
eventcd = 'frank' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 21
SELECT * from wca2.t690_hist
where
eventcd = 'ftt' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 23
SELECT * from wca2.t690_hist
where
eventcd = 'icc' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 25
SELECT * from wca2.t690_hist
where
eventcd = 'ischg'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 28
SELECT * from wca2.t690_hist
where
eventcd = 'liq'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 30
SELECT * from wca2.t690_hist
where
eventcd = 'ltchg' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 32
SELECT * from wca2.t690_hist
where
eventcd = 'mrgr'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
       or (ifnull(date7,'2001-01-01')>=@fromdate and ifnull(date7,'2001-01-01')<=@todate)
       or (ifnull(date8,'2001-01-01')>=@fromdate and ifnull(date8,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date7,'2001-01-01')<@fromdate and ifnull(date7,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date8,'2001-01-01')<@fromdate and ifnull(date8,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 34
SELECT * from wca2.t690_hist
where
eventcd = 'oddlt' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date4,'2001-01-01')>=@fromdate and ifnull(date4,'2001-01-01')<=@todate)
       or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
       or (ifnull(date7,'2001-01-01')>=@fromdate and ifnull(date7,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date4,'2001-01-01')<@fromdate and ifnull(date4,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date7,'2001-01-01')<@fromdate and ifnull(date7,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 35
SELECT * from wca2.t690_hist
where
eventcd = 'pid' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 36
SELECT * from wca2.t690_hist
where
eventcd = 'po' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 37
SELECT * from wca2.t690_hist
where
eventcd = 'prchg'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 38
SELECT * from wca2.t690_hist
where
eventcd = 'prf' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 39
SELECT * from wca2.t690_hist
where
eventcd = 'pvrd'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 41
SELECT * from wca2.t690_hist
where
eventcd = 'redem'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 42
SELECT * from wca2.t690_hist
where
eventcd = 'rts'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
       or (ifnull(date7,'2001-01-01')>=@fromdate and ifnull(date7,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date7,'2001-01-01')<@fromdate and ifnull(date7,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 43
SELECT * from wca2.t690_hist
where
eventcd = 'scchg'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 44
SELECT * from wca2.t690_hist
where
eventcd = 'scswp'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 45
SELECT * from wca2.t690_hist
where
eventcd = 'sd' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
       or (ifnull(date3,'2001-01-01')>=@fromdate and ifnull(date3,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date3,'2001-01-01')<@fromdate and ifnull(date3,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 46
SELECT * from wca2.t690_hist
where
eventcd = 'sdchg' 
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date1,'2001-01-01')>=@fromdate and ifnull(date1,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date1,'2001-01-01')<@fromdate and ifnull(date1,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );

-- # 48
SELECT * from wca2.t690_hist
where
eventcd = 'tkovr'
and secid in (select secid from client.pfisin where accid = 388 and client.pfisin.actflag <> 'D')
and ((
      (ifnull(date2,'2001-01-01')>=@fromdate and ifnull(date2,'2001-01-01')<=@todate)
       or (ifnull(date4,'2001-01-01')>=@fromdate and ifnull(date4,'2001-01-01')<=@todate)
       or (ifnull(date5,'2001-01-01')>=@fromdate and ifnull(date5,'2001-01-01')<=@todate)
       or (ifnull(date6,'2001-01-01')>=@fromdate and ifnull(date6,'2001-01-01')<=@todate)
       or (ifnull(date8,'2001-01-01')>=@fromdate and ifnull(date8,'2001-01-01')<=@todate)
       or (ifnull(date10,'2001-01-01')>=@fromdate and ifnull(date10,'2001-01-01')<=@todate)
     )
	OR
	 (
     changed>date_sub((select max(acttime) from wca.tbl_opslog), interval 20 minute)
     and (
          (ifnull(date2,'2001-01-01')<@fromdate and ifnull(date2,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date4,'2001-01-01')<@fromdate and ifnull(date4,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date5,'2001-01-01')<@fromdate and ifnull(date5,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date6,'2001-01-01')<@fromdate and ifnull(date6,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date8,'2001-01-01')<@fromdate and ifnull(date8,'2001-01-01')>date_sub(@fromdate, interval 3 day))
          or (ifnull(date10,'2001-01-01')<@fromdate and ifnull(date10,'2001-01-01')>date_sub(@fromdate, interval 3 day))
         )
     )
    );
