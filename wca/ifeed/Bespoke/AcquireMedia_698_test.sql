--filepath=n:\temp\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=AcquireMedia_SRF
--fileheadertext=EDI_WCA_AcquireMedia_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bespoke\acquiremedia
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=HH:MM:SS
--ForceTime=n


--# 1
select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as bbcid,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as bbeid,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scmst.acttime, '%Y/%m/%d %H:%i:%s')
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(scexh.acttime, '%Y/%m/%d %H:%i:%s')
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(issur.acttime, '%Y/%m/%d %H:%i:%s')
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then date_format(mktsg.acttime, '%Y/%m/%d %H:%i:%s')
     else date_format(exchg.acttime, '%Y/%m/%d %H:%i:%s')
     end as changed,
wca.scexh.AnnounceDate as created,
wca.scmst.SecID AS secid,
wca.scmst.IssID AS issid,
wca.scmst.isin as isin,
wca.scmst.uscode as uscode,
wca.issur.IssuerName AS issuername,
wca.issur.CntryofIncorp AS cntryofincorp,
wca.issur.SIC AS sic,
wca.issur.CIK AS cik,
wca.issur.IndusID AS indusid,
wca.scmst.SectyCD AS sectycd,
wca.scmst.SecurityDesc AS securitydesc,
wca.scmst.ParValue AS parvalue,
wca.scmst.CurenCD as pvcurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as statusflag,
wca.scmst.PrimaryExchgCD AS primaryexchgcd,
wca.bbc.curencd as bbgcurrency,
wca.bbc.bbgcompid as bbgcompositeglobalid,
wca.bbc.bbgcomptk as bbgcompositeticker,
wca.bbe.bbgexhid as bbgglobalid,
wca.bbe.bbgexhtk as bbgexchangeticker,
wca.scmst.StructCD AS structcd,
wca.exchg.CntryCD as exchgcntry,
wca.scexh.ExchgCD AS exchgcd,
wca.exchg.Mic AS mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as micseg,
wca.scexh.LocalCode AS localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as liststatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>bbc.actflag
where 
wca.scmst.actflag<>'D' 
and wca.scexh.actflag<>'D'



