-- arc=y
-- arp=n:\bespoke\fincontent\
-- fsx=_680
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t680_temp
where changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='CONSD' or eventcd='DIV' or eventcd='SD')
and (exchgcd='BEENB'
or exchgcd='DEXETR'
or exchgcd='FRPEN'
or exchgcd='GBENLN'
or exchgcd='IEISE'
or exchgcd='NLENA'
or exchgcd='PTBVL')
order by eventcd, eventid;