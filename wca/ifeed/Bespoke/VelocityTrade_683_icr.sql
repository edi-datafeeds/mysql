-- arp=n:\bespoke\velocitytrade\
-- fsx=select concat('_683','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_683_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t683_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and (exchgcd='ATVSE'
or exchgcd='AUASX'
or exchgcd='BEENB'
or exchgcd='CATSE'
or exchgcd='CHSSX'
or exchgcd='CZPSE'
or exchgcd='DEXETR'
or exchgcd='DKCSE'
or exchgcd='EETSE'
or exchgcd='ESBBSE'
or exchgcd='ESBSE'
or exchgcd='ESMSE'
or exchgcd='ESVSE'
or exchgcd='FIHSE'
or exchgcd='FRPEN'
or exchgcd='GBLSE'
or exchgcd='HKSEHK'
or exchgcd='IEISE'
or exchgcd='ILTSE'
or exchgcd='ITMSE'
or exchgcd='JPTSE'
or exchgcd='NLENA'
or exchgcd='NOOB'
or exchgcd='NZSE'
or exchgcd='PLWSE'
or exchgcd='PTBVL'
or exchgcd='SESSE'
or exchgcd='SGSSE'
or exchgcd='TRIMKB'
or exchgcd='USNASD'
or exchgcd='USNYSE')