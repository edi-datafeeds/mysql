-- arp=n:\bespoke\iress_2020\
-- fsx=_683
-- hsx=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_WCA_683_
-- dfn=l
-- fty=y

-- # 1

select * from wca2.t683_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (exchgcntry = 'TT'
or exchgcntry = 'TN'
or exchgcntry = 'RS'
or exchgcntry = 'BG'
or exchgcntry = 'HR'
or exchgcntry = 'AE'
or exchgcntry = 'JO'
or exchgcntry = 'BH'
or exchgcntry = 'KW'
or exchgcntry = 'OM'
or exchgcntry = 'IN'
or exchgcntry = 'CN'
or exchgcntry = 'PH'
or exchgcntry = 'SA'
or exchgcntry = 'PL')