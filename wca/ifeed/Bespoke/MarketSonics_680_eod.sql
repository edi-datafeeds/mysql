-- arp=n:\bespoke\marketsonics\
-- hpx=EDI_WCA_680_
-- dfn=l
-- fex=_680.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'BON'
or eventcd = 'CALL'
or eventcd = 'CONSD'
or eventcd = 'DIST'
or eventcd = 'DMRGR'
or eventcd = 'ENT'
or eventcd = 'RCAP'
or eventcd = 'RTS'
or eventcd = 'SCSWP'
or eventcd = 'SD'
or eventcd = 'SECRC')

