-- arc=y
-- arp=n:\Bespoke\ArrowStreet\
-- hpx=EDI_WCA_689_
-- dfn=l
-- fex=.txt
-- fsx=select concat('_689','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- dfn=l
-- fty=n

-- # 1
select * from wca2.t689_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (pfid in (select secid from wca.scexh
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
mic= 'XNYS' or mic = 'XNAS' or mic = 'XKRX' or mic = 'XKOS')
or pfid in (select issid from wca.scexh
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
inner join wca.scmst on wca.scexh.secid=wca.scmst.secid
where
mic= 'XNYS' or mic = 'XNAS' or mic = 'XKRX' or mic = 'XKOS'));
