-- arp=N:\Bespoke\William_Oneil\
-- hpx=EDI_WCA_689_
-- dfn=l
-- fex=_689.txt
-- fty=y

-- # 1

select * from wca2.t689_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd='bb'
or eventcd='consd'
or eventcd='dmrgr'
or eventcd='icc'
or eventcd='ischg'
or eventcd='lcc'
or eventcd='liq'
or eventcd='lstat'
or eventcd='mrgr'
or eventcd='nlist'
or eventcd='prchg'
or eventcd='prf'
or eventcd='scchg'
or eventcd='sd'
or eventcd='shoch'
or eventcd='tkovr');