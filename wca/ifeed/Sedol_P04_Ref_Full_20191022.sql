-- # 1
-- arc=y
-- arp=n:\temp\
-- fpx=edi_
-- hpx=edi_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog), interval 0 day), '%Y%m%d')
-- fsx=select concat('_SedolRef_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_SedolRef_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y

-- # 1
select distinct
wca.scmst.secid,
wca.exchg.cntrycd as listcntrycd,
wca.scmst.primaryexchgcd,
wca.scexh.exchgcd,
case when ifnull(gbp04.TIDM,'')<>'' then replace(gbp04.TIDM,'.L','')
     when ifnull(wca.scxtc.localcode,'')<>'' then wca.scxtc.localcode
     else wca.scexh.localcode
     end as localcode,
wca.sedol.sedol,
case when gbp04.sedol is not null
     then gbp04.Currency
     when wca.sedol.curencd <> ''
     then wca.sedol.curencd
     else ''
     end as sedoltradingcurrency,
wca.sedol.rcntrycd as sedolregistercountry
from wca.scmst
inner join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
inner join wca.sedol on wca.scmst.secid=wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on wca.sedol.sedol=gbp04.Sedol
left outer join wca.scxtc on wca.scexh.scexhid=wca.scxtc.scexhid and wca.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
where
wca.scmst.actflag<>'D'
and wca.scexh.actflag<>'D'
and wca.sedol.actflag<>'D'
and (wca.scmst.statusflag<>'I' or gbp04.Sedol is not null)
and (wca.scexh.liststatus<>'D' or gbp04.Sedol is not null)
and (wca.sedol.defunct<>'T' or gbp04.Sedol is not null)
and wca.sedol.sedol<>''
and wca.sedol.rcntrycd<>'XG'
and wca.sedol.rcntrycd<>'XZ'
and wca.sedol.rcntrycd<>'XH'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'CNSZHK';
