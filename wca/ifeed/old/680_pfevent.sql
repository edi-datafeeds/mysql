-- arc=y
-- arp=n:\temp\
-- hpx=EDI_WCA_680_
-- fsx=_680
-- dfn=l

-- # 1

select * from wca2.t680_pfevent
where (eventcd = 'BON'
or eventcd = 'SD'
or eventcd = 'CONSD'
or eventcd = 'MRGR'
or eventcd = 'TKOVR'
or eventcd = 'DMRGR'
or eventcd = 'LCC'
or eventcd = 'LSTAT'
or eventcd = 'DIV'
or eventcd = 'RTS'
or eventcd = 'DRIP'
or eventcd = 'LIQ'
or eventcd = 'RCAP')
order by eventcd asc;