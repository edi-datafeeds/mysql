-- arc=y
-- arp=n:\temp\
-- hpx=EDI_PRICES_P04_
-- fsx=_P04
-- dfn=l
-- fdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=select date_format((select max(acttime) from wca.tbl_opslog), '%Y%m%d')

-- # 1

select * from prices.lasttradedate
where prices.lasttradedate.uscode in (select cusip from portfolio.scott_cusip)
and prices.lasttradedate.primaryexchgcd = prices.lasttradedate.exchgcd 