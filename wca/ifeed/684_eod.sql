-- arp=n:\no_cull_feeds\684\
-- hpx=EDI_WCA_684_
-- dfn=l
-- fex=_684.txt
-- fty=y

-- # 1

select * from wca2.t680_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'dprcp' or eventcd = 'drchg');
