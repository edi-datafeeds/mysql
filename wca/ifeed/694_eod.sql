-- arp=n:\no_cull_feeds\694\
-- hpx=EDI_WCA_694_
-- dfn=l
-- fex=_694.txt
-- fty=y

-- # 1

select * from wca2.t690_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'dprcp' or eventcd = 'drchg');
