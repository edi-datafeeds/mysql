-- arp=n:\no_cull_feeds\680\mf\
-- fsx=select concat('_680','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_680_
-- dfn=l
-- fty=y
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t680_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
union
select * from wca2.t680_mf_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
and exchgcd='USNASD'
order by EventCD;
