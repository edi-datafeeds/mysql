-- arc=y
-- arp=n:\No_Cull_Feeds\690\IDIV\
-- fsx=select concat('_690','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_690_
-- dfn=l
-- fty=y
-- eof=EDI_ENDOFFILE

-- # 1

select * from wca2.t690_temp
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
UNION
select * from wca2.t690_temp_prf
where
changed >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
order by eventcd, eventid;