-- arc=y
-- arp=n:\temp\
-- fsx=_Ronin_SRF
-- hpx=EDI_Ronin_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate) from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate) from wca.tbl_opslog where seq = 3), interval 1 day), '%Y%m%d')

-- # 1
select distinct
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when issur.acttime > ifnull(exchg.acttime,'2000-01-01')
     then issur.acttime
     else exchg.acttime
     end as Changed,
wca.scmst.SecID,
wca.scmst.SharesOutstanding,
wca.issur.IssuerName,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.scexh.Lot,
wca.exchg.Mic,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and wca.exchg.cntrycd='US' 
                          and (wca.scexh.exchgcd='USNYSE' or wca.scexh.exchgcd='USNASD' or wca.scexh.exchgcd='USAMEX' or wca.scexh.exchgcd='USPAC' or wca.scexh.exchgcd='USBATS')) or wca.sectygrp.secgrpid is not null)
and wca.scmst.statusflag<>'I'
and wca.scexh.liststatus<>'D'
and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D';
