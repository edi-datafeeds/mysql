-- arc=y
-- arp=N:\No_Cull_Feeds\CLSAC\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_CLSAC
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_WCA_CLSAC_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select 
wca.clsac.Actflag,
case when ifnull(wca.clsac.acttime,'2000-01-01') > (select max(feeddate) from wca.tbl_opslog where seq = 3) 
     then wca.clsac.acttime
     when ifnull(wca.clssc.acttime,'2000-01-01') > (select max(feeddate) from wca.tbl_opslog where seq = 3) 
     then wca.clssc.acttime
     when ifnull(wca.hear.acttime,'2000-01-01') > (select max(feeddate) from wca.tbl_opslog where seq = 3) 
     then wca.hear.acttime
     else wca.clsst.acttime  
     end as Changed,
wca.clsac.ClsacID,
wca.scmst.SecID,
wca.sedol.SedolID,
wca.hear.HearID,
wca.clsst.ClsstID,
wca.issur.Issuername,
wca.scexh.Localcode,
wca.scexh.ExchgCD,
wca.scmst.Isin,
wca.scmst.USCode,
wca.sedol.Sedol,
case when wca.scexh.liststatus='' or wca.scexh.liststatus='R' or wca.scexh.liststatus='N'
     then 'L'
     else wca.scexh.liststatus
     end as Liststatus,
wca.clsac.CourtID,
wca.clsac.CaseNo,
wca.clsac.FilingDate,
wca.clsac.Judge,
wca.clsac.CaseName,
wca.clsac.Allegation,
wca.clsac.Eligsec,
wca.clsac.Investcls,
wca.clsac.OnBehalf,
wca.clsac.PeriodStartDate,
wca.clsac.PeriodEndDate,
wca.clsac.LeadPDate,
wca.clsac.PrffDate,
wca.clsac.ClaimfDate,
wca.clsac.ObjDate,
wca.clsac.ExclDate,
wca.clsac.LeadPtif,
wca.clsac.CaseStatus,
wca.clsac.Setcurcd,
wca.clsac.Setamount,
wca.hear.HearingDate,
wca.hear.Add1,
wca.hear.Add2,
wca.hear.Add3,
wca.hear.Add4,
wca.hear.Add5,
wca.hear.Add6,
wca.hear.City,
wca.hear.State,
wca.hear.CntryCD,
wca.hear.Website,
wca.hear.Contact1,
wca.hear.Tel1,
wca.hear.Fax1,
wca.hear.Email1,
wca.hear.Contact2,
wca.hear.Tel2,
wca.hear.Fax2,
wca.hear.Email2,
wca.issur.SIC,
wca.issur.NAICS,
wca.isscn.HOAdd1,
wca.isscn.HOAdd2,
wca.isscn.HOAdd3,
wca.isscn.HOAdd4,
wca.isscn.HOAdd5,
wca.isscn.HOAdd6,
wca.isscn.HOCity,
wca.isscn.HOState,
wca.isscn.HOCntryCD,
wca.isscn.HOPostcode,
wca.isscn.HOTel,
wca.isscn.HOFax,
wca.isscn.HOEmail,
wca.clsst.Status
from wca.clsac
left outer join wca.clssc on wca.clsac.clsacid = wca.clssc.clsacid and wca.clssc.actflag<>'D'
left outer join wca.scmst on wca.clssc.secid = wca.scmst.secid and wca.scmst.actflag<>'D'
left outer join wca.issur on wca.clsac.issid = wca.issur.issid and wca.issur.actflag<>'D'
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid 
                          and 'D'<>wca.scexh.liststatus 
                          and substring(wca.scexh.exchgcd,1,2)='US'
                          and wca.scexh.actflag<>'D'
left outer join wca.sedol on wca.scmst.secid = wca.sedol.secid 
                          and substring(wca.scexh.exchgcd,1,2)=wca.sedol.cntrycd
                          and wca.sedol.actflag<>'D'
left outer join wca.hear on wca.clsac.clsacid = wca.hear.clsacid and wca.hear.actflag<>'D'
left outer join wca.isscn on wca.clsac.issid = wca.isscn.issid and wca.isscn.actflag<>'D'
left outer join wca.clsst on wca.clsac.clsacid = wca.clsst.clsacid and wca.clsst.actflag<>'D'
WHERE
(wca.clsac.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.clssc.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.hear.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.clsst.acttime>=(select max(feeddate) from wca.tbl_opslog where seq = 3))