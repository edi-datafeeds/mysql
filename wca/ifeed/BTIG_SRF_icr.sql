-- arc=y
-- fty=n
-- arp=n:\temp\
-- hpx=EDI_SEDOLBBG2_SRF_ICR_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(acttime_new)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime_new)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_SEDOLBBG2_SRF_ICR_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime_new desc limit 1))


-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when sedolseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then sedolseq.acttime
     when bbeseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbeseq.acttime
     when bbcseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbcseq.acttime
     when bbebnd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbebnd.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as BbgGlobalID,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as SedolRegCntry,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when ifnull(gbp04.TIDM,'')<>''
     then replace(gbp04.TIDM,'.L','')
     when wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
wca.scexh.LocalCode as Localcode680,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L')
     else wca.scexh.ListStatus
     end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join prices.lse_gb as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
where
(3>wca.sectygrp.secgrpid
or ((wca.scmst.sectycd='BND'
and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and (wca.exchg.cntrycd='US' and wca.scexh.exchgcd<>'USBND')))
and (sedolbbg.sedolacttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or sedolbbg.bbeacttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or sedolbbg.bbcacttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.bbebnd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog)
           or wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog));
