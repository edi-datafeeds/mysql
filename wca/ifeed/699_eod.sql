-- arp=n:\no_cull_feeds\699\
-- hpx=EDI_WCA_699_
-- dfn=l
-- fex=_699.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (eventcd = 'dprcp' or eventcd = 'drchg');
