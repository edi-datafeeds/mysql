-- arp=n:\no_cull_feeds\695\
-- hpx=EDI_WCA_695_
-- dfn=l
-- fex=_695.txt
-- fty=y

-- # 1

select * from wca2.t695_temp
where
changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch'
