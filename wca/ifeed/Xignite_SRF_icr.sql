-- arc=n
-- fty=n
-- arp=n:\temp\
-- hpx=EDI_Xignite_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_Xignite_SRF_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when wca.scexh.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolbbg.sedolacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.sedolacttime
     when sedolbbg.bbeacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.bbeacttime
     when sedolbbg.bbcacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.bbcacttime
     when bbe.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then bbe.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
'' as SedolDefunct,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
case when ifnull(gbp04.TIDM,'')<>'' and ifnull(sedolbbg.sedoldefunct,'')<>'T' and ifnull(smf4.security.statusflag,'')='T'
     then replace(gbp04.TIDM,'.L','')
     when ifnull(wca.scxtc.localCode,'')<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L')
     else wca.scexh.ListStatus
     end as ListStatus,
wca.scexh.listdate,
wca.ipo.IPOStatus,
wca.ipo.FirstTradingDate,
smf4.security.OPOL,
'' as DailyAccrual
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe on wca.scmst.secid =wca.bbe.secid and ''=ifnull(wca.bbe.exchgcd,'X') and ''<>ifnull(wca.bbe.bbgexhid,'') and 'D'<>ifnull(wca.bbe.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
where 
wca.scexh.exchgcd='GBLSE'
and ((wca.scexh.liststatus<>'D' and wca.scexh.actflag<>'D') or wca.scexh.acttime>(select max(feeddate) from wca.tbl_opslog))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and (wca.scmst.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) 
     or wca.scexh.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.sedolacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.bbcacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.bbeacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.bbe.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.mktsg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.scxtc.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.ipo.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.exchg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or ifnull(wca.scexh.listdate,'2000-01-01')=(select max(feeddate) from wca.tbl_opslog)
     )
UNION
select distinct
wca.scexh.ScexhID,
case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumReg,
case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end as SedolSeqnumCur,
case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgCompSeqnum,
case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end as BbgExhSeqnum,
wca.scexh.Actflag,
case when wca.scexh.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolbbg.sedolacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.sedolacttime
     when sedolbbg.bbeacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.bbeacttime
     when sedolbbg.bbcacttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then sedolbbg.bbcacttime
     when bbe.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then bbe.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else wca.scmst.Isin
     end as Isin,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     when wca.scxtc.Isin<>''
     then ''
     else wca.scmst.uscode
     end as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
ifnull(sedolbbg.curencd,'') as TradingCurrency,
ifnull(sedolbbg.bbgcompid,'') as BbgCompositeGlobalID,
ifnull(sedolbbg.bbgcomptk,'') as BbgCompositeTicker,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else wca.bbe.bbgexhid
     end as BbgGlobalID,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else wca.bbe.bbgexhtk
     end as BbgExchangeTicker,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.Sedol else '' end as Sedol,
'' as SedolDefunct,
case when ifnull(sedolbbg.SedolDefunct,'')='F' then sedolbbg.RcntryCD else '' end as RegCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
ifnull(wca.mktsg.mktsgid,'') as Micseg,
CASE when wca.scexh.exchgcd<>'USNYSE' and wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     when wca.scexh.LocalCode='BPRAP'
     then wca.scexh.LocalCode
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2),'PR','/PR'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),'/PR/CL')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3),'PR','/PR/'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4),'PR','/PR/')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-5),'/PR/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,1),'/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WS/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/WS')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/RT')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/RT')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and securitydesc like '%Class A%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'A'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/A')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and securitydesc like '%Class B%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'B'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/B')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WI')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WD')
     when wca.scmst.securitydesc like 'Unit%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'U'
          and wca.scmst.sectycd='STP'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/U')
     else wca.scexh.LocalCode
     end as LocalCode,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'P'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'P'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L')
     else wca.scexh.ListStatus
     end as ListStatus,
wca.scexh.listdate,
wca.ipo.IPOStatus,
wca.ipo.FirstTradingDate,
smf4.security.OPOL,
'' as DailyAccrual
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd
                   and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe on wca.scmst.secid =wca.bbe.secid and ''=ifnull(wca.bbe.exchgcd,'X') and ''<>ifnull(wca.bbe.bbgexhid,'') and 'D'<>ifnull(wca.bbe.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
where 
wca.exchg.cntrycd<>'GB'
and (wca.exchg.mic<>'' or wca.scmst.sectycd<>'BND')
and ((wca.scexh.liststatus<>'D' and wca.scexh.actflag<>'D') or wca.scexh.acttime>(select max(feeddate) from wca.tbl_opslog))
and (wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and (wca.scmst.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog) 
     or wca.scexh.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.issur.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.sedolacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.bbcacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or sedolbbg.bbeacttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.bbe.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.mktsg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.exchg.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.scxtc.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or wca.ipo.acttime>(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
     or smf4.security.actdate>(select date_sub(max(acttime), interval '1500' minute) from wca.tbl_opslog)
     or ifnull(wca.scexh.listdate,'2000-01-01')=(select max(feeddate) from wca.tbl_opslog)
     )
and substring(wca.exchg.exchgcd,3,3)<>'BND'
and wca.exchg.exchgcd<>'ALTSE'
and wca.exchg.exchgcd<>'AOASE'
and wca.exchg.exchgcd<>'AROEX'
and wca.exchg.exchgcd<>'ARRCX'
and wca.exchg.exchgcd<>'AUSIMV'
and wca.exchg.exchgcd<>'CHSCH'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'CNSZHK'
and wca.exchg.exchgcd<>'DESCH'
and wca.exchg.exchgcd<>'DODRSX'
and wca.exchg.exchgcd<>'FINDX'
and wca.exchg.exchgcd<>'FOFSM'
and wca.exchg.exchgcd<>'GABVCA'
and wca.exchg.exchgcd<>'GIGSX'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'HNCASE'
and wca.exchg.exchgcd<>'HTHTSE'
and wca.exchg.exchgcd<>'JPNSE'
and wca.exchg.exchgcd<>'KNECSE'
and wca.exchg.exchgcd<>'LSMSM'
and wca.exchg.exchgcd<>'LUUL'
and wca.exchg.exchgcd<>'MVMSX'
and wca.exchg.exchgcd<>'MYLFX'
and wca.exchg.exchgcd<>'MZMSX'
and wca.exchg.exchgcd<>'NOOTC'
and wca.exchg.exchgcd<>'PLOTC'
and wca.exchg.exchgcd<>'SENDX'
and wca.exchg.exchgcd<>'TJCASE'
and wca.exchg.exchgcd<>'UAINX'
and wca.exchg.exchgcd<>'UAKISE'
and wca.exchg.exchgcd<>'USTRCE'
and wca.exchg.exchgcd<>'UYUEX';
