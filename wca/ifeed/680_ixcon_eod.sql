-- arc=y
-- arp=n:\temp\
-- fsx=_680
-- hpx=EDI_WCA_680_IXCON
-- dfn=l

-- # 1

select * from wca2.t680_temp
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and (secid in (select code from client.pfsecid where accid=@accid and actflag='U'))
and eventcd <> 'dprcp' and eventcd <> 'drchg' and eventcd <> 'shoch')
or
((secid in (select code from client.pfsecid where accid=@accid and actflag='I'))
and eventcd <> 'DPRCP' and eventcd <> 'DRCHG' and eventcd <> 'SHOCH');

