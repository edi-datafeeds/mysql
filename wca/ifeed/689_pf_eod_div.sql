-- arc=y
-- arp=n:\temp\
-- fsx=_689
-- hpx=EDI_WCA_689_
-- dfn=l
-- fty=n

-- # 1

select eventcd,
eventid,
changed,
fieldname,
notestext
from wca2.t689_temp
where
(changed >(select max(feeddate) from wca.tbl_opslog where seq = 3)
and eventcd='DIV' and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='U'))
or 
(eventcd='DIV' and pfid in (select secid from client.pfisin where accid=@accid and client.pfisin.actflag='I'));