-- arc=y
-- arp=n:\temp\
-- fsx=_Xignite_SRF
-- hpx=EDI_Xignite_SRF_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')

-- # 1
select distinct
wca.scexh.ScexhID,
wca.scexh.Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.CFI,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca2.sedolbbg.bbgcompid as BbgCompositeGlobalID,
wca2.sedolbbg.bbgcomptk as BbgCompositeTicker,
wca2.sedolbbg.bbgexhid as BbgGlobalID,
wca2.sedolbbg.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca2.sedolbbg.Sedol,
wca2.sedolbbg.RcntryCD as SedolRegCntry,
prices.lasttrade.LocalCode,
prices.lasttrade.Currency as TradingCurrency,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
prices.lasttrade.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid=wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and prices.lasttrade.exchgcd=wca.scexh.exchgcd
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca2.sedolbbg on wca.scexh.exchgcd = wca2.sedolbbg.exchgcd 
              and wca.scexh.secid=wca2.sedolbbg.secid
              and wca.exchg.cntrycd=wca2.sedolbbg.cntrycd
              and prices.lasttrade.currency=(wca2.sedolbbg.curencd or wca2.sedolbbg.curencd='')
where
wca.scexh.liststatus<>'D'
and prices.lasttrade.exchgcd='GBLSE'
and ifnull(wca2.sedolbbg.sedol,'')<>''
and prices.lasttrade.localcode=
(select localcode from prices.lasttrade as latest
where
latest.secid=prices.lasttrade.secid
and latest.exchgcd=prices.lasttrade.exchgcd
and prices.lasttrade.pricedate>(date_sub(date_format(now(),'%Y-%m-%d'), interval 366 day))
order by pricedate desc limit 1)
and wca.scmst.actflag<>'D'
and (wca.scexh.actflag<>'D' or wca.scexh.actflag is null);
