--filepath=o:\Datafeed\WCA\512_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.512
--suffix=
--fileheadertext=Records = NNNNNN Security Data
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\512_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n




--# 1
SELECT
wca.scexh.ScexhID as SecID,
concat(date_format(now(), '%d/%m/%Y'),' ',substring(now(),12,8)) as scmsttime,
wca.scmst.Actflag,
substring(wca.scexh.ExchgCD,1,2) as ExCountry,
wca.scexh.ExchgCD,
wca.scmst.IssID,
wca.scexh.Localcode,
wca.scmst.Isin,
wca.scmst.SecurityDesc,
wca.scmst.SectyCD as Typecode,
sedolseq1.Sedol,
wca.scmst.USCode,
wca.scmst.AnnounceDate as Sourcedate,
wca.scmst.AnnounceDate as Creation,
wca.scmst.IssID as Globissid,
'' as trailingTAB
FROM wca.scmst
LEFT OUTER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.sedolseq1 ON wca.scmst.SecID = wca.sedolseq1.SecID
                   and substring(wca.scexh.ExchgCD,1,2) = wca.sedolseq1.cntryCD
where (wca.scmst.sectyCD='EQS' or wca.scmst.sectyCD='DR' or wca.scmst.sectyCD='PRF')
and (wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
and wca.scexh.scexhid is not null
and wca.sedolseq1.seqnum = 1
order by scexhid, sedolseq1.sedol desc