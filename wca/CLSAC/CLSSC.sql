--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CLSSC
--fileheadertext=EDI_WCA_CLSSC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\CLSSC\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('CLSSC') as tablename,
wca.main.Actflag,
wca.main.Acttime,
wca.main.Announcedate,
wca.main.SecID,
wca.main.ClsacID,
wca.main.ClsscID,
wca.main.Notes
from wca.clssc as main
inner join wca.clsac on main.clsacid = wca.clsac.clsacid 
WHERE
wca.main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
and (wca.clsac.periodenddate>(select date_sub(now(),interval 31 day) or (wca.clsac.periodenddate is null and wca.clsac.announcedate>(select date_sub(now(),interval 365 day)))))
