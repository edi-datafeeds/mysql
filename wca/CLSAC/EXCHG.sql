--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_EXCHG
--fileheadertext=EDI_WCA_EXCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\EXCHG\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
WHERE
wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
