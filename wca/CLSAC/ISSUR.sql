--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_WCA_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\ISSUR\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Acttime,
wca.issur.Announcedate,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
inner join wca.clsac on wca.issur.issid = wca.clsac.issid
WHERE
(wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.clsac.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
and (wca.clsac.periodenddate>(select date_sub(now(),interval 31 day) or (wca.clsac.periodenddate is null and wca.clsac.announcedate>(select date_sub(now(),interval 365 day)))))
