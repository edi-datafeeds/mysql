--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CLSST
--fileheadertext=EDI_WCA_CLSST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\CLSST\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('CLSST') as tablename,
wca.main.Actflag,
wca.main.Acttime,
wca.main.Announcedate,
wca.main.ClsacID,
wca.main.StatusDate,
wca.main.Status,
wca.main.ClsstID,
wca.main.Notes
from wca.clsst as main
inner join wca.clsac on wca.main.clsacid = wca.clsac.clsacid
WHERE
wca.main.actflag<>'D'
