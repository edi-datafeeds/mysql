--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_HEAR
--fileheadertext=EDI_WCA_HEAR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\HEAR\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('HEAR') as tablename,
wca.main.Actflag,
wca.main.Acttime,
wca.main.Announcedate,
wca.main.ClsacID,
wca.main.HearingDate,
wca.main.ADD1,
wca.main.ADD2,
wca.main.ADD3,
wca.main.ADD4,
wca.main.ADD5,
wca.main.ADD6,
wca.main.City,
wca.main.State,
wca.main.CntryCD,
wca.main.Website,
wca.main.Contact1,
wca.main.Tel1,
wca.main.Fax1,
wca.main.Email1,
wca.main.Contact2,
wca.main.Tel2,
wca.main.Fax2,
wca.main.Email2,
wca.main.Hearid,
wca.main.Notes
from wca.hear as main
inner join wca.clsac on wca.main.clsacid = wca.clsac.clsacid
WHERE
wca.main.actflag<>'D'
