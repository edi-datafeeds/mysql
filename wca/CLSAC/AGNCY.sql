--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_WCA_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\AGNCY\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
select
upper('AGNCY')as Tablename,
wca.agncy.Actflag,
wca.agncy.Acttime,
wca.agncy.Announcedate,
wca.agncy.AgncyID,
wca.agncy.RegistrarName,
wca.agncy.Add1,
wca.agncy.Add2,
wca.agncy.Add3,
wca.agncy.Add4,
wca.agncy.Add5,
wca.agncy.Add6,
wca.agncy.City,
wca.agncy.CntryCD,
wca.agncy.WebSite,
wca.agncy.Contact1,
wca.agncy.Tel1,
wca.agncy.Fax1,
wca.agncy.email1,
wca.agncy.Contact2,
wca.agncy.Tel2,
wca.agncy.Fax2,
wca.agncy.email2,
wca.agncy.Depository,
wca.agncy.State
from wca.agncy
inner join wca.clsag on wca.agncy.agncyid = wca.clsag.agncyid
inner join wca.clsac on wca.clsag.clsacid = wca.clsac.clsacid
where
(wca.agncy.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.clsag.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
and (wca.clsac.periodenddate>(select date_sub(now(),interval 31 day) or (wca.clsac.periodenddate is null and wca.clsac.announcedate>(select date_sub(now(),interval 365 day)))))
