--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CLSAC_SHORT
--fileheadertext=EDI_CLSAC_SHORT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
select courtid, caseno, casename, cntrycd, status, statusdate, uscode as cusip, isin from wca.clsac
left outer join wca.clsst on wca.clsac.clsacid=wca.clsst.clsacid
left outer join wca.hear on wca.clsac.clsacid=wca.hear.clsacid
left outer join wca.clssc on wca.clsac.clsacid=wca.clssc.clsacid
left outer join wca.scmst on wca.clssc.secid=wca.scmst.secid
where statusdate>'2013/12/31'
order by statusdate;