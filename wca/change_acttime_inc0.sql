SET @v_acttime = ( SELECT DATE_SUB( MAX( acttime_new ), INTERVAL '4' MINUTE) FROM wca.tbl_opslog WHERE seq = 0 ORDER BY acttime DESC );

UPDATE `wca`.`tbl_opslog` SET `acttime` = @v_acttime
WHERE (`file_name` =  CONCAT( DATE_FORMAT( CURDATE() - INTERVAL 0 DAY , '%Y%m%d' ), '.z01' ) );