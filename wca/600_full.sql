--filepath=o:\datafeed\wca\600_full\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.600
--suffix=
--fileheadertext=EDI_WCA_600_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\600\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT 
wca.issur.Issuername,
wca.issur.CntryofIncorp,
wca.scmst.IssID,
wca.scmst.SecID,
wca.scmst.Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.Securitydesc,
wca.scmst.CurenCD as ParValueCurrency,
wca.scmst.Parvalue,
wca.scmst.SectyCD,
wca.scmst.Uscode,
wca.scmst.Isin,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.scexh.exchgcd,
wca.scexh.localcode,
wca.scexh.liststatus,
wca.cntry.curenCD as DomicileCurrency
FROM wca.scmst
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.cntry ON wca.issur.CntryofIncorp = wca.cntry.cntryCD
LEFT OUTER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
LEFT OUTER JOIN wca.sedol ON wca.scmst.SecID = wca.sedol.SecID
                      AND wca.exchg.cntryCD = wca.sedol.CntryCD
where
wca.scmst.sectycd<>'CW'
and (wca.scexh.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1)
or wca.scmst.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1)
or wca.sedol.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1)
or wca.issur.acttime>=(select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 1))
