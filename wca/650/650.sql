--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.650
--suffix=
--fileheadertext=EDI_WCA_650_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\650\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n

--# 1
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

--# 2
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
case when (div_my.acttime > vtab.acttime)
      and (div_my.acttime > rd.acttime)
      and (div_my.acttime > exdt.acttime)
      and (div_my.acttime > pexdt.acttime)
     then div_my.acttime
     when (rd.acttime > vtab.acttime)
      and (rd.acttime > exdt.acttime)
      and (rd.acttime > pexdt.acttime)
     then rd.acttime
     when (exdt.acttime > vtab.acttime)
      and (exdt.acttime > pexdt.acttime)
     then exdt.acttime
     when (pexdt.acttime > vtab.acttime)
     then pexdt.acttime
     else vtab.acttime
     end as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then 'A' else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then 'L' else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'NonPID' as Field1Name,
vtab.NonPID as Field1,
'PID' as Field2Name,
vtab.PID as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_pid as vtab
inner join wca.div_my on vtab.divid=div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on div_my.rdid=wca.rdprt.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.cntrycd and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.cntrycd = wca.exchg.cntrycd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null
or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (div_my.acttime>@ffdate and (@ftdate is null or div_my.acttime<@ftdate))
or (rd.acttime>@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>@fcfdate and exdt.exdate<@ftdate)
or (exdt.exdate is null and (pexdt.exdate>@fcfdate and pexdt.exdate<@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null or stab.secid in (select code from client.pfsecid where accid=@faccid))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.cntrycd = 'GB';
