drop table if exists prices_aux.splitseq1;
use wca;
create table prices_aux.splitseq1(
select
wca.rd.secid,
wca.scexh.exchgcd,
case when wca.exdt.exdate is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.rdid as eventid,
'CONSD' as eventtype
from wca.consd as maintab
inner join wca.rd on maintab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'CONSD' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'CONSD' = pexdt.eventtype
where (maintab.actflag<>'D')
and (wca.exdt.exdate is not null or pexdt.exdate is not null)
and maintab.newratio is not null and  maintab.newratio<>''
and maintab.oldratio is not null and  maintab.oldratio<>''
);
alter table `prices_aux`.`splitseq1`
add primary key (`ExchgCD`, `EventID`, `EventType`),
add index `ix_secid`(`Seqnum`, `ExchgCD`,`SecID`);
insert into prices_aux.splitseq1
select
wca.rd.secid,
wca.scexh.exchgcd,
case when wca.exdt.exdate is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.rdid as eventid,
'SD' as eventtype
from wca.sd as maintab
inner join wca.rd on maintab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'sd' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'sd' = pexdt.eventtype
where (maintab.actflag<>'D')
and (wca.exdt.exdate is not null or pexdt.exdate is not null)
and maintab.newratio is not null and  maintab.newratio<>''
and maintab.oldratio is not null and  maintab.oldratio<>'';
insert into prices_aux.splitseq1 
select
maintab.secid,
wca.scexh.exchgcd,
maintab.effectivedate as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.caprdid as eventid,
'CAPRD' as eventtype
from wca.caprd as maintab
inner join wca.scmst on maintab.secid = wca.scmst.secid
inner join wca.scexh on maintab.secid = wca.scexh.secid
where (maintab.actflag<>'D')
and maintab.effectivedate is not null
and maintab.newratio<>''
and maintab.oldratio<>''
and maintab.newratio<>maintab.oldratio;
call prices_aux.build_splitseq(@Ssecid, @Sexchgcd, @Tsecid, @Texchgcd, @Tsplitdate, @Teventid, @Teventtype, @Tseqnum, @cnt, @finished);

