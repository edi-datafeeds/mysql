drop table if exists prices_aux.cashdivregseq1;
use wca;
create table prices_aux.cashdivregseq1(
select distinct
wca.div_my.DivID,
wca.rd.SecID,
wca.exdt.exdate,
wca.div_my.frequency,
wca.divpy.CurenCD,
wca.divpy.GrossDividend,
1 as seqnum
from wca.div_my
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.v10s_divpy as divpy on wca.div_my.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and 'DIV' = wca.exdt.eventtype
where
wca.exdt.exdate is not null
and wca.div_my.actflag<>'D'
and wca.div_my.actflag<>'C'
and wca.divpy.actflag<>'D'
and wca.divpy.actflag<>'C'
and (wca.divpy.optionserialno<2 or wca.divpy.optionserialno is null)
and (wca.div_my.frequency='ANL' or wca.div_my.frequency='SMA' or wca.div_my.frequency='TRM' or wca.div_my.frequency='QTR' or wca.div_my.frequency='MNT' or wca.div_my.frequency='BIM')
and wca.exdt.exdate>date_sub(now(), interval 400 day)
and wca.exdt.exdate<now()
and wca.divpy.grossdividend is not null
and wca.divpy.grossdividend<>'0'
and wca.divpy.CurenCD<>''
);
alter table `prices_aux`.`cashdivregseq1`
add primary key (`DivID`, `CurenCD`, `exdate`,`GrossDividend`),
add index `ix_seqnum`(`seqnum`, `secid`, `curencd`);
call prices_aux.build_cashdivregseq1(@Ssecid, @Tsecid, @Texdate, @Tdivid, @Tcurencd, @Tseqnum, @cnt, @Tgrossdividend, @finished);
