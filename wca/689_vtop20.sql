--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.689
--suffix=
--fileheadertext=EDI_WCA_689_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\689\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n
--zerorowchk=n

--# 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

--# 1
select vtab.* from wca.v689_arr as vtab
limit 20;

--# 2
select vtab.* from wca.v689_bb as vtab
limit 20;

--# 3
select vtab.* from wca.v689_bkrp as vtab
limit 20;

--# 4
select vtab.* from wca.v689_bon as vtab
limit 20;

--# 5
select vtab.* from wca.v689_call as vtab
limit 20;

--# 6
select vtab.* from wca.v689_caprd as vtab
limit 20;

--# 7
select vtab.* from wca.v689_consd as vtab
limit 20;

--# 8
select vtab.* from wca.v689_conv as vtab
limit 20;

--# 9
select vtab.* from wca.v689_ctx as vtab
limit 20;

--# 10
select vtab.* from wca.v689_currd as vtab
limit 20;

--# 11
select vtab.* from wca.v689_dist as vtab
limit 20;

--# 12
select vtab.* from wca.v689_div as vtab
limit 20;

--# 13
select vtab.* from wca.v689_dmrgr as vtab
limit 20;

--# 14
select vtab.* from wca.v689_dvst as vtab
limit 20;

--# 15
select vtab.* from wca.v689_ent as vtab
limit 20;

--# 16
select vtab.* from wca.v689_lawst as vtab
limit 20;

--# 17
select vtab.* from wca.v689_liq as vtab
limit 20;

--# 18
select vtab.* from wca.v689_mrgr_approval_status as vtab
limit 20;

--# 19
select vtab.* from wca.v689_mrgr_companies as vtab
limit 20;

--# 20
select vtab.* from wca.v689_mrgr_mrgrterms as vtab
limit 20;

--# 21
select vtab.* from wca.v689_oddlt as vtab
limit 20;

--# 22
select vtab.* from wca.v689_po as vtab
limit 20;

--# 23
select vtab.* from wca.v689_prf as vtab
limit 20;

--# 24
select vtab.* from wca.v689_pvrd as vtab
limit 20;

--# 25
select vtab.* from wca.v689_rcap as vtab
limit 20;

--# 26
select vtab.* from wca.v689_redem as vtab
limit 20;

--# 27
select vtab.* from wca.v689_rts as vtab
limit 20;

--# 28
select vtab.* from wca.v689_scchg as vtab
limit 20;

--# 29
select vtab.* from wca.v689_scswp as vtab
limit 20;

--# 30
select vtab.* from wca.v689_sd as vtab
limit 20;

--# 31
select vtab.* from wca.v689_secrc as vtab
limit 20;

--# 32
select vtab.* from wca.v689_tkovr as vtab
limit 20;
