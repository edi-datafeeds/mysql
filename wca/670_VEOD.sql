--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.670
--suffix=
--fileheadertext=EDI_WCA_670_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\670\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'Sedol' as f3,
'SedolCurrency' as f3,
'Defunct' as f3,
'SedolRegCntry' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioNew' as f25,
'RatioOld' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

--# 1
select vtab.* from wca.v670_ann as vtab;

--# 2
select vtab.* from wca.v670_arr as vtab;

--# 3
select vtab.* from wca.v670_assm as vtab;

--# 4
select vtab.* from wca.v670_bb as vtab;

--# 5
select vtab.* from wca.v670_bkrp as vtab;

--# 6
select vtab.* from wca.v670_bon as vtab;

--# 7
select vtab.* from wca.v670_br as vtab;

--# 8
select vtab.* from wca.v670_call as vtab;

--# 9
select vtab.* from wca.v670_caprd as vtab;

--# 10
select vtab.* from wca.v670_agm as vtab;

--# 11
select vtab.* from wca.v670_consd as vtab;

--# 12
select vtab.* from wca.v670_conv as vtab;

--# 13
select vtab.* from wca.v670_ctx as vtab;

--# 14
select vtab.* from wca.v670_currd as vtab;

--# 15
select vtab.* from wca.v670_dist as vtab;

--# 16
select vtab.* from wca.v670_div as vtab;

--# 17
select vtab.* from wca.v670_dmrgr as vtab;

--# 18
select vtab.* from wca.v670_drip as vtab;

--# 19
select vtab.* from wca.v670_dvst as vtab;

--# 20
select vtab.* from wca.v670_ent as vtab;

--# 21
select vtab.* from wca.v670_frank as vtab;

--# 22
select vtab.* from wca.v670_fychg as vtab;

--# 23
select vtab.* from wca.v670_icc as vtab;

--# 24
select vtab.* from wca.v670_inchg as vtab;

--# 25
select vtab.* from wca.v670_ischg as vtab;

--# 26
select vtab.* from wca.v670_lawst as vtab;

--# 27
select vtab.* from wca.v670_lcc as vtab;

--# 28
select vtab.* from wca.v670_liq as vtab;

--# 29
select vtab.* from wca.v670_lstat as vtab;

--# 30
select vtab.* from wca.v670_ltchg as vtab;

--# 31
select vtab.* from wca.v670_mkchg as vtab;

--# 32
select vtab.* from wca.v670_mrgr as vtab;

--# 33
select vtab.* from wca.v670_nlist as vtab;

--# 34
select vtab.* from wca.v670_oddlt as vtab;

--# 35
select vtab.* from wca.v670_po as vtab;

--# 36
select vtab.* from wca.v670_prf as vtab;

--# 37
select vtab.* from wca.v670_pvrd as vtab;

--# 38
select vtab.* from wca.v670_rcap as vtab;

--# 39
select vtab.* from wca.v670_redem as vtab;

--# 40
select vtab.* from wca.v670_rts as vtab;

--# 41
select vtab.* from wca.v670_scchg as vtab;

--# 42
select vtab.* from wca.v670_scswp as vtab;

--# 43
select vtab.* from wca.v670_sd as vtab;

--# 44
select vtab.* from wca.v670_sdchg as vtab;

--# 45
select vtab.* from wca.v670_secrc as vtab;

--# 46
select vtab.* from wca.v670_tkovr as vtab;
