--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.680
--suffix=
--fileheadertext=EDI_WCA_680_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\680\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n
--zerorowchk=n

--# 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

--# 1
select vtab.* from wca.v680_ann as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 2
select vtab.* from wca.v680_arr as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 3
select vtab.* from wca.v680_assm as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 4
select vtab.* from wca.v680_bb as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 5
select vtab.* from wca.v680_bkrp as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 6
select vtab.* from wca.v680_bon as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 7
select vtab.* from wca.v680_br as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 8
select vtab.* from wca.v680_call as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 9
select vtab.* from wca.v680_caprd as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 10
select vtab.* from wca.v680_agm as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 11
select vtab.* from wca.v680_consd as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 12
select vtab.* from wca.v680_conv as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 13
select vtab.* from wca.v680_ctx as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 14
select vtab.* from wca.v680_currd as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 15
select vtab.* from wca.v680_dist as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 16
select vtab.* from wca.v680_div as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 17
select vtab.* from wca.v680_dmrgr as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 18
select vtab.* from wca.v680_drip as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 19
select vtab.* from wca.v680_dvst as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 20
select vtab.* from wca.v680_ent as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 21
select vtab.* from wca.v680_frank as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 22
select vtab.* from wca.v680_fychg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 23
select vtab.* from wca.v680_icc as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 24
select vtab.* from wca.v680_inchg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 25
select vtab.* from wca.v680_ischg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 26
select vtab.* from wca.v680_lawst as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 27
select vtab.* from wca.v680_lcc as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 28
select vtab.* from wca.v680_liq as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 29
select vtab.* from wca.v680_lstat as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 30
select vtab.* from wca.v680_ltchg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 31
select vtab.* from wca.v680_mkchg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 32
select vtab.* from wca.v680_mrgr as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 33
select vtab.* from wca.v680_nlist as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 34
select vtab.* from wca.v680_oddlt as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 35
select vtab.* from wca.v680_po as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 36
select vtab.* from wca.v680_prf as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 37
select vtab.* from wca.v680_pvrd as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 38
select vtab.* from wca.v680_rcap as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 39
select vtab.* from wca.v680_redem as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 40
select vtab.* from wca.v680_rts as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 41
select vtab.* from wca.v680_scchg as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 42
select vtab.* from wca.v680_scswp as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 43
select vtab.* from wca.v680_sd as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 44
select vtab.* from wca.v680_secrc as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;

--# 45
select vtab.* from wca.v680_tkovr as vtab
where exchgcd = primaryexchgcd
order by eventid desc
limit 20;
