-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.698
-- filenamesuffix=
-- headerprefix=EDI_WCA_698_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\no_cull_feeds\698\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
select
'ScexhID' as f3,
'BbcID' as f3,
'BbeID' as f3,
'Actflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'BbgCurrency' as f3,
'BbgCompositeGlobalID' as f3,
'BbgCompositeTicker' as f3,
'BbgGlobalID' as f3,
'BbgExchangeTicker' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,
'Mic' as f3,
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3;

-- # 2;
select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null
  or (issur.acttime>@ffdate)
  or (scmst.acttime>@ffdate)
  or (scexh.acttime>@ffdate)
  or (exchg.acttime>@ffdate)
  or (mktsg.acttime>@ffdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or wca.scmst.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or wca.scmst.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D'))
and (@inactive is null or wca.scmst.statusflag<>'I')
and (@delist is null or wca.scexh.liststatus<>'D')
and (@allactflag is null or (wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'));
