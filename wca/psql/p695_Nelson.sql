-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.695
-- filenamesuffix=
-- headerprefix=EDI_WCA_695_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\no_cull_feeds\695\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # big 1
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'BbcID' as f3,
'BbeID' as f3,
'Actflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'BbgCurrency' as f3,
'BbgCompositeGlobalID' as f3,
'BbgCompositeTicker' as f3,
'BbgGlobalID' as f3,
'BbgExchangeTicker' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

-- # big 2
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AGMDate' as Date1Type,
vtab.AGMDate as Date1,
'FYEDate' as Date2Type,
vtab.FYEDate as Date2,
'RecDate' as Date3Type,
vtab.RecDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'AGMEGM' as Field1Name,
vtab.AGMEGM as Field1,
'AGMNo' as Field2Name,
vtab.AGMNo as Field2,
'AGMTime' as Field3Name,
vtab.AGMTime as Field3,
'Add1' as Field4Name,
vtab.Add1 as Field4,	
'Add2' as Field5Name,
vtab.Add2 as Field5,
'Add3' as Field6Name,
vtab.Add3 as Field6,
'Add4' as Field7Name,
vtab.Add4 as Field7,
'Add5' as Field8Name,
vtab.Add5 as Field8,
'Add6' as Field9Name,
vtab.Add6 as Field9,
'City' as Field10Name,
vtab.City as Field10,
'CntryCD' as Field11Name,
vtab.CntryCD as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_agm as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on wca.issur.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.agmdate>=@fcfdate and vtab.agmdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 3
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_ann as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on vtab.issid = wca.stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.notificationdate>=@fcfdate and vtab.notificationdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 4
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_arr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 5
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'AssimilationDate' as Date1Type,
vtab.AssimilationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_assm as vtab
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd = wca.scexh.exchgcd
inner join wca.v20c_680_scmst as stab on wca.scexh.secid = stab.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.assimilationdate>=@fcfdate and vtab.assimilationdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 6
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Startdate' as Date1Type,
vtab.Startdate as Date1,
'Enddate' as Date2Type,
vtab.Enddate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Paydate' as Date4Type,
mpay.Paydate as Date4,
'Withdrawalfromdate' as Date5Type,
mpay.Withdrawalfromdate as Date5,
'Withdrawaltodate' as Date6Type,
mpay.Withdrawaltodate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'OnOffFlag' as Field1Name,
vtab.OnOffFlag as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_bb as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.bbid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.enddate>=@fcfdate and vtab.enddate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 7
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'Filing Date' as Date2Type,
FilingDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_bkrp as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.notificationdate>=@fcfdate and vtab.notificationdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 8
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
vtab.ressecid as OutturnSecID,
'' as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Ressectycd' as Field1Name,
vtab.SectyCD as Field1,
'LaspsedPremium' as Field2Name,
vtab.LapsedPremium as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_bon as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 9
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartSubscription' as Date1Type,
vtab.StartSubscription Date1,
'EndSubscription' as Date2Type,
vtab.EndSubscription as Date2,
'SplitDate' as Date3Type,
vtab.SplitDate as Date3,
'StartTrade' as Date4Type,
vtab.StartTrade as Date4,
'EndTrade' as Date5Type,
vtab.Endtrade as Date5,
'ExDate' as Date6Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date6,
'Recdate' as Date7Type,
wca.rd.Recdate as Date7,
'PayDate' as Date8Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
'' as Priority,
'F' as DefaultOpt,
vtab.ResSecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.IssuePrice Rate1,
'' as Rate2Type,
'' Rate2,
'TradingSecID' as Field1Name,
vtab.TraSecID as Field1,
'TradingIsin' as Field2Name,
trascmst.Isin as Field2,
'OverSubscription' as Field3Name,
case when wca.vtab.oversubscription='T' then 'T' else 'F' end as OverSubscription,
'ResSectyCD' as Field4Name,
vtab.SectyCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_br as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 10
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'DueDate' as Date1Type,
vtab.DueDate Date1,
'Recdate' as Date2Type,
wca.rd.recdate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'CallNumber' as Field1Name,
vtab.CallNumber as Field1,
'CurenCD' as Field2Name,
vtab.CurenCD as Field2,
'ToFacevalue' as Field3Name,
vtab.ToFacevalue as Field3,
'ToPremium' as Field4Name,
vtab.ToPremium as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_call as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.duedate>=@fcfdate and vtab.duedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 11
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.oldisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.olduscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_caprd as vtab
INNER JOIN wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 12
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.newisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.newuscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'Currency' as Field3Name,
vtab.CurenCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_consd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 13
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'FromDate' as Date1Type,
vtab.FromDate Date1,
'ToDate' as Date2Type,
vtab.ToDate as Date2,
'RecDate' as Date3Type,
rd.Recdate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew as RatioNew,
vtab.RatioOld as RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'Price' as Rate1Type,
vtab.Price as Rate1,
'' as Rate2Type,
'' Rate2,
'MandOptFlag' as Field1Name,
vtab.MandOptFlag as Field1,
'FXrate' as Field2Name,
vtab.FXrate as Field2,
'PartFinalFlag' as Field3Name,
vtab.PartFinalFlag as Field3,	
'ResSectyCD' as Field4Name,
vtab.ResSectyCD as Field4,
'ConvType' as Field5Name,
vtab.ConvType as Field5,
'PriceAsPercent' as Field6Name,
vtab.PriceAsPercent as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_conv as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
left outer join wca.rd on vtab.rdid = wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.todate>=@fcfdate and vtab.todate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 14
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartDate' as Date1Type,
vtab.StartDate Date1,
'EndDate' as Date2Type,
EndDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.eventtype as Field1,
'ResSectyCD' as Field2Name,
vtab.SectyCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_ctx as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.enddate>=@fcfdate and vtab.enddate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 15
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldCurenCD' as Field2Name,
vtab.OldCurenCD as Field2,
'NewCurenCD' as Field3Name,
vtab.NewCurenCD as Field3,
'OldParValue' as Field4Name,
vtab.OldParValue as Field4,	
'NewParValue' as Field5Name,
vtab.NewParValue as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_currd as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = wca.stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.OldCurenCD <> '' or vtab.NewCurenCD <> '' or vtab.OldParValue <> '' or vtab.NewParValue <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 16
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
wca.mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
wca.mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
wca.mpay.OptElectionDate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then mpay.Paytype
     when wca.mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_dist as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 17
select
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.PayDate2 IS NOT NULL then wca.exdt.PayDate2 ELSE pexdt.paydate2 END as Date4,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
wca.divpy.Divtype as Paytype,
wca.rdprt.Priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
wca.divpy.RatioNew,
wca.divpy.RatioOld,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when wca.vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when wca.vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when wca.vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.divpy on wca.vtab.divid = wca.divpy.divid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 18
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
wca.mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
wca.mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
wca.mpay.OptElectionDate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_dmrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 19
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'DripLastdate' as Date1Type,
vtab.DripLastdate Date1,
'DripPaydate' as Date2Type,
vtab.DripPaydate as Date2,
'CrestDate' as Date3Type,
vtab.CrestDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'DripReinvPrice' as Field1Name,
vtab.DripReinvPrice as Field1,
'CntryCD' as Field2Name,
vtab.CntryCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_drip as vtab
inner join wca.div_my on vtab.divid=wca.div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on wca.div_my.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.cntrycd and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.driplastdate>=@fcfdate and vtab.driplastdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 20
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when vtab.rationew<>'' and vtab.curencd<>'' then 'B'
     when vtab.curencd<>'' then 'C'
     when vtab.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
vtab.Minprice Rate1,
'MaxPrice' as Rate2Type,
vtab.MaxPrice Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'MaxQlyQty' as Field2Name,
vtab.MaxQlyQty as Field2,
'MaxAcpQty' as Field3Name,
vtab.MaxAcpQty as Field3,
'TradingSecID' as Field4Name,
vtab.TraSecID as Field4,
'TradingIsin' as Field5Name,
trascmst.Isin as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_dvst as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 21
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.EntIssuePrice Rate1,
'' as Rate2Type,
'' Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'OverSubscription' as Field2Name,
case when wca.vtab.OverSubscription='T' then 'T' else 'F' end as OverSubscription,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_ent as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 22
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Frankflag' as Field1Name,
vtab.Frankflag as Field1,
'FrankDiv' as Field2Name,
vtab.FrankDiv as Field2,
'UnFrankDiv' as Field3Name,
vtab.UnFrankDiv as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_frank as vtab
inner join wca.div_my on vtab.divid=div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.cntrycd and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.cntrycd <> 'GB'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 23
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'NotificationDate' as Date1Type,
vtab.NotificationDate Date1,
'OldFYStartDate' as Date2Type,
vtab.OldFYStartDate as Date2,
'OldFYEndDate' as Date3Type,
vtab.OldFYEndDate as Date3,
'NewFYStartDate' as Date4Type,
vtab.NewFYStartDate as Date4,
'NewFYEndDate' as Date5Type,
vtab.NewFYEndDate as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.Eventtype as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_fychg as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.notificationdate>=@fcfdate and vtab.notificationdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 24
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldIsin' as Field2Name,
vtab.OldIsin as Field2,
'NewIsin' as Field3Name,
vtab.NewIsin as Field3,
'OldUscode' as Field4Name,
vtab.OldUscode as Field4,
'NewUscode' as Field5Name,
vtab.NewUscode as Field5,	
'RelEventID' as Field6Name,
vtab.RelEventID as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_icc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.OldIsin <> '' or vtab.NewIsin <> '' or vtab.OldUscode <> '' or vtab.NewUscode <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 25
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.InChgDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldCntryCD' as Field2Name,
vtab.OldCntryCD as Field2,
'NewCntryCD' as Field3Name,
vtab.NewCntryCD as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_inchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.inchgdate>=@fcfdate and vtab.inchgdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.OldCntryCD <> '' or vtab.NewCntryCD <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 26
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.NameChangeDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'IssOldName' as Field2Name,
vtab.IssOldName as Field2,
'IssNewName' as Field3Name,
vtab.IssNewName as Field3,
'LegalName' as Field4Name,
case when wca.vtab.LegalName='T' then 'T' else 'F' end as LegalName,
'MeetingDateFlag' as Field5Name,
case when wca.vtab.MeetingDateFlag='T' then 'T' else 'F' end as MeetingDateFlag,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_ischg as vtab
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.namechangedate>=@fcfdate and vtab.namechangedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.IssOldName <> '' or vtab.IssNewName <> '' or vtab.LegalName <> '' or vtab.MeetingDateFlag <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 27
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'Regdate' as Date2Type,
vtab.Regdate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'LaType' as Field1Name,
vtab.LaType as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_lawst as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 28
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLocalCode' as Field2Name,
vtab.OldLocalCode as Field2,
'NewLocalCode' as Field3Name,
vtab.NewLocalCode as Field3,
'RelEventID' as Field4Name,
vtab.RelEventID as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_lcc as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.OldLocalCode <> '' or vtab.NewLocalCode <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 29
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Recdate' as Date1Type,
vtab.Rddate as Date1,
'Paydate' as Date2Type,
wca.mpay.paydate as Date2,
'OptElectionDate' as Date3Type,
wca.mpay.OptElectionDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
'' as priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'ResSectyCD' as Field1Name,
mpay.SectyCD as Field1,
'Liquidator' as Field2Name,
vtab.Liquidator as Field2,
'LiqAdd1' as Field3Name,
vtab.LiqAdd1 as Field3,
'LiqAdd2' as Field4Name,
vtab.LiqAdd2 as Field4,
'LiqAdd3' as Field5Name,
vtab.LiqAdd3 as Field5,
'LiqAdd4' as Field6Name,
vtab.LiqAdd4 as Field6,
'LiqAdd5' as Field7Name,
vtab.LiqAdd5 as Field7,
'LiqAdd6' as Field8Name,
vtab.LiqAdd6 as Field8,
'LiqCity' as Field9Name,
vtab.LiqCity as Field9,
'LiqCntryCD' as Field10Name,
vtab.LiqCntryCD as Field10,
'LiqTel' as Field11Name,
vtab.LiqTel as Field11,
'LiqFax' as Field12Name,
vtab.LiqFax as Field12,
'LiqEmail' as Field13Name,
vtab.LiqEmail as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_liq as vtab
inner join wca.issur on vtab.issid = wca.issur.issid
inner join wca.v20c_680_scmst as stab on vtab.issid = stab.issid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.mpay on vtab.liqid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.rddate>=@fcfdate and vtab.rddate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 30
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_lstat as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on vtab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 31
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'OldLot' as Field2Name,
vtab.OldLot as Field2,
'OldMinTrdQty' as Field3Name,
vtab.OldMinTrdQty as Field3,
'NewLot' as Field4Name,
vtab.NewLot as Field4,
'NewMinTrdgQty' as Field5Name,
vtab.NewMinTrdgQty as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_ltchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid and vtab.exchgcd=wca.scexh.exchgcd
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.OldLot <> '' or vtab.NewLot <> '' or vtab.OldMinTrdQty <> '' or vtab.NewMinTrdgQty <> '' )
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 32
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'' as DefaultOpt,
'' as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'EventType' as Field1Name,
vtab.EventType as Field1,
'OldMIC' as Field2Name,
oldseg.MIC as Field2,
'NewMIC' as Field3Name,
newseg.MIC as Field3,
'OldMktsegment' as Field4Name,
oldseg.Mktsegment as Field4,
'NewMktsegment' as Field5Name,
newseg.Mktsegment as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_mkchg as vtab
inner join wca.scexh on vtab.secid = wca.scexh.secid
inner join wca.v20c_680_scmst as stab on wca.scexh.secid = wca.stab.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.mktsg as oldseg on vtab.oldmktsgid = oldseg.mktsgid
left outer join wca.mktsg as newseg on vtab.newmktsgid = newseg.mktsgid
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 33
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'AppointedDate' as Date4Type,
vtab.AppointedDate as Date4,
'EffectiveDate' as Date5Type,
vtab.EffectiveDate as Date5,
'WithdrawalFromdate' as Date6Type,
wca.mpay.WithdrawalFromdate as Date6,
'WithdrawalTodate' as Date7Type,
wca.mpay.WithdrawalTodate as Date7,
'OptElectionDate' as Date8Type,
mpay.OptElectionDate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MrgrStatus' as Field1Name,
vtab.MrgrStatus as Field1,
'WithdrawalRights' as Field2Name,
case when wca.mpay.WithdrawalRights='T' then 'T' else 'F' end as WithdrawalRights,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_mrgr as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 34
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
wca.scexh.ListDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_nlist as vtab
inner join wca.scexh on wca.vtab.scexhid = wca.scexh.scexhid
inner join wca.v20c_680_scmst as stab on wca.scexh.secid = wca.stab.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (wca.scexh.listdate>=@fcfdate and wca.scexh.listdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 35
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'StartDate' as Date1Type,
vtab.StartDate Date1,
'EndDate' as Date2Type,
vtab.EndDate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Paydate' as Date4Type,
mpay.Paydate as Date4,
'Withdrawalfromdate' as Date5Type,
mpay.Withdrawalfromdate as Date5,
'Withdrawaltodate' as Date6Type,
mpay.Withdrawaltodate as Date6,
'OptElectionDate' as Date7Type,
wca.mpay.OptElectionDate as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'MinQlyQty' as Field1Name,
wca.mpay.MinQlyQty as Field1,
'MaxQlyQty' as Field2Name,
wca.mpay.MaxQlyQty as Field2,
'WithdrawalRights' as Field3Name,
case when wca.mpay.withdrawalrights='T' then 'T' else 'F' end as WithdrawalRights,
'MinOfrQty' as Field4Name,
wca.mpay.MinOfrQty as Field4,
'MaxOfrQty' as Field5Name,
wca.mpay.MaxOfrQty as Field5,
'MinAcpQty' as Field6Name,
wca.vtab.MinAcpQty as Field6,
'MaxAcpQty' as Field7Name,
wca.vtab.MaxAcpQty as Field7,
'Buyin' as Field8Name,
wca.vtab.Buyin as Field8,
'BuyInCurenCD' as Field9Name,
wca.vtab.BuyInCurenCD as Field9,
'BuyInPrice' as Field10Name,
wca.vtab.BuyInPrice as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_oddlt as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.eventid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.enddate>=@fcfdate and vtab.enddate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 36
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.Isin,
stab.Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'NonPID' as Field1Name,
vtab.NonPID as Field1,
'PID' as Field2Name,
vtab.PID as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,	
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_pid as vtab
inner join wca.div_my on vtab.divid=div_my.divid
inner join wca.rd on wca.div_my.rdid=wca.rd.rdid
left outer join wca.rdprt on rd.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.exchg on vtab.cntrycd = wca.exchg.cntrycd and 'D' <> wca.exchg.actflag
inner join wca.scexh on stab.secid = wca.scexh.secid and exchg.exchgcd=wca.scexh.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.cntrycd = 'GB'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 37
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'OfferOpens' as Date4Type,
vtab.OfferOpens as Date4,
'OfferCloses' as Date5Type,
vtab.OfferCloses as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
MinPrice Rate1,
'MaxPrice' as Rate2Type,
vtab.MaxPrice Rate2,
'POMinPercent' as Field1Name,
POMinPercent as Field1,
'POMaxPercent' as Field2Name,
POMaxPercent as Field2,
'MinOfrQty' as Field3Name,
vtab.MinOfrQty as Field3,
'MaxOfrQty' as Field4Name,
vtab.MaxOfrQty as Field4,
'TndrStrkPrice' as Field5Name,
vtab.TndrStrkPrice as Field5,
'TndrPriceStep' as Field6Name,
vtab.TndrPriceStep as Field6,
'MinQlyQty' as Field7Name,
vtab.MinQlyQty as Field7,
'MaxQlyQty' as Field8Name,
vtab.MaxQlyQty as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'SealedBid' as Field11Name,
case when wca.vtab.SealedBid='T' then 'T' else 'F' end as SealedBid,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_po as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 38
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'' as Date6Type,
Null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
case when vtab.rationew<>'' and vtab.curencd<>'' then 'B'
     when vtab.curencd<>'' then 'C'
     when vtab.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'MinPrice' as Rate1Type,
vtab.MinPrice Rate1,
'MaxPrice' as Rate2Type,
MaxPrice Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'OffereeIssID' as Field2Name,
vtab.OffereeIssID as Field2,
'OffereeName' as Field3Name,
vtab.OffereeName as Field3,
'TndrStrkPrice' as Field4Name,
vtab.TndrStrkPrice as Field4,
'TndrPriceStep' as Field5Name,
vtab.TndrPriceStep as Field5,
'MinQlyQty' as Field6Name,
vtab.MinQlyQty as Field6,
'MaxQlyQty' as Field7Name,
vtab.MaxQlyQty as Field7,
'MinAcpQty' as Field8Name,
vtab.MinAcpQty as Field8,
'MaxAcpQty' as Field9Name,
vtab.MaxAcpQty as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_prf as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 39
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
vtab.eventtype as Field1,
'PVCurency' as Field2Name,
vtab.Curencd as Field2,
'OldParValue' as Field3Name,
vtab.OldParValue as Field3,
'NewParValue' as Field4Name,
vtab.NewParValue as Field4,
'' as Field5Name,
'' as Field5,	
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_pvrd as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 40
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'RecDate' as Date2Type,
wca.rd.RecDate as Date2,
'PayDate' as Date3Type,
vtab.Cspydate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioOld,
'' as RatioNew,
'' as Fractions,
vtab.CurenCD as Currency,
'CashBak' as Rate1Type,
vtab.CashBak Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_rcap as vtab
INNER JOIN wca.v20c_680_scmst as stab ON stab.SecID = vtab.SecID
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 41
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'RedemDate' as Date1Type,
RedemDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'C' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
vtab.CurenCD as Currency,
'RedemPrice' as Rate1Type,
vtab.RedemPrice Rate1,
'' as Rate2Type,
'' Rate2,
'MandOptFlag' as Field1Name,
vtab.MandOptFlag as Field1,
'PartFinal' as Field2Name,
vtab.PartFinal as Field2,
'Redemtype' as Field3Name,
vtab.Redemtype as Field3,
'AmountRedeemed' as Field4Name,
vtab.AmountRedeemed as Field4,
'RedemPremium' as Field5Name,
vtab.RedemPremium as Field5,
'PriceAsPercent' as Field6Name,
vtab.PriceAsPercent as Field6,
'PremiumAsPercent' as Field7Name,
vtab.PremiumAsPercent as Field7,
'PoolFactor' as Field8Name,
vtab.PoolFactor as Field8,
'RedemPercent' as Field9Name,
vtab.RedemPercent as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_redem as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.redemdate>=@fcfdate and vtab.redemdate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 42
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
vtab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
vtab.EndSubscription as Date5,
'StartTrade' as Date6Type,
vtab.StartTrade as Date6,
'EndTrade' as Date7Type,
vtab.EndTrade as Date7,
'Splitdate' as Date8Type,
Splitdate as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
vtab.Fractions,
vtab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
vtab.IssuePrice Rate1,
'LapsedPremium' as Rate2Type,
LapsedPremium Rate2,
'TradingSecID' as Field1Name,
vtab.TraSecID as Field1,
'TradingIsin' as Field2Name,
trascmst.Isin as Field2,
'PPSecID' as Field3Name,
vtab.PPSecID as Field3,
'PPIsin' as Field4Name,
ppscmst.isin as Field4,
'OverSubscription' as Field5Name,
case when wca.vtab.OverSubscription='T' then 'T' else 'F' end as OverSubscription,
'ResSectyCD' as Field6Name,
vtab.SectyCD as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_rts as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on vtab.ressecid = resscmst.secid
left outer join wca.scmst as trascmst on vtab.trasecid = trascmst.secid
left outer join wca.scmst as ppscmst on vtab.ppsecid = ppscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 43
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.DateofChange as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'' as Paytype,
'' as Priority,
'F' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'eventtype' as Field1Name,
vtab.eventtype as Field1,
'SecOldName' as Field2Name,
vtab.SecOldName as Field2,
'SecNewName' as Field3Name,
vtab.SecNewName as Field3,
'OldSectyCD' as Field4Name,
vtab.OldSectyCD as Field4,
'NewSectyCD' as Field5Name,
vtab.NewSectyCD as Field5,	
'OldRegS144A' as Field6Name,
vtab.OldRegS144A as Field6,
'NewRegS144A' as Field7Name,
vtab.NewRegS144A as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_scchg as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
WHERE
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.dateofchange>@fcfdate and vtab.dateofchange<@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (vtab.SecOldName <> '' or vtab.SecNewName <> '' or vtab.OldSectyCD <> '' or vtab.NewSectyCD <> '' or vtab.OldRegS144A <> '' or vtab.NewRegS144A <> '')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 44
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'ResSectyCD' as Field1Name,
vtab.SectyCD as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_scswp as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.scmst as resscmst on ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 45
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then stab.isin else wca.icc.newisin end as Isin,
case when (wca.icc.releventid is null or wca.icc.olduscode = '') then stab.uscode else wca.icc.newuscode end as Uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else wca.stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
case when (wca.lcc.releventid is null or wca.lcc.oldlocalcode = '') then wca.scexh.localcode else wca.lcc.oldlocalcode end as Localcode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
wca.rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
wca.rdprt.priority,
'F' as DefaultOpt,
case when (wca.icc.releventid is null or wca.icc.oldisin = '')  then null else stab.secid end as OutturnSecID,
case when (wca.icc.releventid is null or wca.icc.oldisin = '') then null else wca.icc.newisin end as OutturnIsin,
vtab.NewRatio as RatioNew,
vtab.OldRatio as RatioOld,
vtab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
vtab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
vtab.NewParvalue as Field2,
'OldCurrency' as Field3Name,
vtab.OldCurenCD as Field3,
'NewCurrency' as Field4Name,
vtab.NewCurenCD as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_sd as vtab
left outer join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.v20c_680_scmst as stab on wca.rd.secid = wca.stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.icc on vtab.eventid = wca.icc.releventid and vtab.eventcd = wca.icc.eventtype and 'D'<>wca.icc.actflag
LEFT OUTER JOIN wca.lcc ON vtab.eventid = wca.lcc.RelEventID AND vtab.eventcd = wca.lcc.EventType and wca.scexh.exchgcd = wca.lcc.exchgcd and 'D'<>wca.lcc.actflag
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and stab.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null
or (exdt.exdate>=@fcfdate and exdt.exdate<=@ftdate)
or (exdt.exdate is null and (pexdt.exdate>=@fcfdate and pexdt.exdate<=@ftdate)))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 46
select
vtab.EventCD,
vtab.EventID,
'1' as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'EffectiveDate' as Date1Type,
vtab.EffectiveDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Date9Type,
null as Date9,
'' as Date10Type,
null as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
'S' as Paytype,
'' as Priority,
'F' as DefaultOpt,
resscmst.secid as OutturnSecID,
resscmst.isin as OutturnIsin,
vtab.RatioNew,
vtab.RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Relevent' as Field1Name,
vtab.Eventtype as Field1,
'ResSectyCD' as Field2Name,
vtab.SectyCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6,
'' as Field7Name,
'' as Field7,
'' as Field8Name,
'' as Field8,
'' as Field9Name,
'' as Field9,
'' as Field10Name,
'' as Field10,
'' as Field11Name,
'' as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
FROM wca.v10s_secrc as vtab
INNER JOIN wca.v20c_680_scmst as stab ON stab.SecID = vtab.SecID
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer JOIN wca.scmst as resscmst ON vtab.ressecID = resscmst.SecID
Where
(@ffdate is null 
  or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.effectivedate>=@fcfdate and vtab.effectivedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;

-- # big 47
select
vtab.EventCD,
vtab.EventID,
case when wca.mpay.OptionID is null then 1 else wca.mpay.OptionID end as OptionID,
case when wca.mpay.SerialID is null then 1 else wca.mpay.SerialID end as SerialID,
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
stab.SecID,
stab.IssID,
stab.isin,
stab.uscode,
stab.IssuerName,
stab.CntryofIncorp,
stab.SectyCD,
stab.SecurityDesc,
stab.ParValue,
stab.PVCurrency,
case when stab.StatusFlag='' then upper('A') else stab.StatusFlag end as StatusFlag,
stab.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end AS Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
'Opendate' as Date1Type,
vtab.opendate as Date1,
'Closedate' as Date2Type,
vtab.closedate as Date2,
'Recdate' as Date3Type,
wca.rd.Recdate as Date3,
'Cmacqdate' as Date4Type,
vtab.Cmacqdate as Date4,
'Paydate' as Date5Type,
wca.mpay.Paydate as Date5,
'Optelectiondate' as Date6Type,
wca.mpay.Optelectiondate as Date6,
'Withdrawalfromdate' as Date7Type,
wca.mpay.Withdrawalfromdate as Date7,
'Withdrawaltodate' as Date8Type,
wca.mpay.Withdrawaltodate as Date8,
'UnconditionalDate' as Date9Type,
vtab.UnconditionalDate as Date9,
'ToDate' as Date10Type,
wca.rd.ToDate as Date10,
'RegistrationDate' as Date11Type,
wca.rd.RegistrationDate as Date11,
'' as Date12Type,
null as Date12,
case when wca.mpay.Paytype<>'' then wca.mpay.Paytype
     when wca.mpay.rationew<>'' and wca.mpay.curencd<>'' then 'B'
     when wca.mpay.curencd<>'' then 'C'
     when wca.mpay.rationew<>'' then 'S'
     else '' end as Paytype,
wca.rdprt.priority,
case when wca.mpay.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
wca.mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
wca.mpay.Maxprice Rate2,
'TkovrStatus' as Field1Name,
vtab.TkovrStatus as Field1,
'OfferorIssID' as Field2Name,
vtab.OfferorIssID as Field2,
'OfferorName' as Field3Name,
vtab.OfferorName as Field3,
'Hostile' as Field4Name,
case when wca.vtab.Hostile='T' then 'T' else 'F' end as Hostile,
'MiniTkovr' as Field5Name,
case when wca.vtab.MiniTkovr='T' then 'T' else 'F' end as MiniTkovr,
'ResSecTyCD' as Field6Name,
mpay.SecTyCD as Field6,
'TargetQuantity' as Field7Name,
vtab.TargetQuantity as Field7,
'TargetPercent' as Field8Name,
vtab.TargetPercent as Field8,
'MinAcpQty' as Field9Name,
vtab.MinAcpQty as Field9,
'MaxAcpQty' as Field10Name,
vtab.MaxAcpQty as Field10,
'PreOfferQty' as Field11Name,
vtab.PreOfferQty as Field11,
'PreOfferPercent' as Field12Name,
vtab.PreOfferPercent as Field12,
'TndrStrkPrice' as Field13Name,
mpay.TndrStrkPrice as Field13,
'TndrStrkStep' as Field14Name,
mpay.TndrStrkStep as Field14,
'MinQlyQty' as Field15Name,
mpay.MinQlyQty as Field15,
'MaxQlyQty' as Field16Name,
mpay.MaxQlyQty as Field16,
'MinOfrQty' as Field17Name,
mpay.MinOfrQty as Field17,
'MaxOfrQty' as Field18Name,
mpay.MaxOfrQty as Field18,
'DutchAuction' as Field19Name,
case when wca.mpay.DutchAuction='T' then 'T' else 'F' end as DutchAuction,
'WithdrawalRights' as Field20Name,
mpay.WithdrawalRights as Field20,
'' as Field21Name,
'' as Field21,
'RdID' as Field22Name,
wca.rd.RdID as Field22
from wca.v10s_tkovr as vtab
inner join wca.v20c_680_scmst as stab on vtab.secid = stab.secid
inner join wca.scexh on stab.secid = wca.scexh.secid
LEFT OUTER JOIN wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid and vtab.eventcd= wca.rdprt.eventtype
left outer join wca.mpay on vtab.tkovrid = wca.mpay.eventid and vtab.eventcd = wca.mpay.sEvent
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
Where
(@ffdate is null 
  or (vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate)))
and (@fcfdate is null or (vtab.closedate>=@fcfdate and vtab.closedate<=@ftdate))
and (@fexchgcd is null or wca.scexh.exchgcd = @fexchgcd)
and (@fcntrycd is null or wca.exchg.cntrycd = @fcntrycd)
and (@faccid is null
    or stab.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or stab.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or stab.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or bbc.bbgcomptk in (select code from client.pfcomptk where accid=@faccid and actflag <> 'D'))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
and vtab.actflag<>'D'
order by vtab.EventID desc;
