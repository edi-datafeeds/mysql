-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.689
-- filenamesuffix=
-- headerprefix=EDI_WCA_689_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\no_cull_feeds\689\
-- fieldheaders=n
-- filetidy=n
-- incremental=
-- shownulls=n
-- zerorowchk=n

-- # 0
SELECT
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'Notestext' as f4

-- # big 1
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ArrNotes as NotesText
FROM wca.v10s_arr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ArrNotes<>''
and ArrNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 2
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BBNotes as NotesText
FROM wca.v10s_bb as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and BBNotes<>''
and BBNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.enddate>@fcfdate and vtab.enddate<@ftdate))
order by vtab.EventID desc;

-- # big 3
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BkrpNotes as NotesText
FROM wca.v10s_bkrp as vtab
WHERE
vtab.actflag<>'D'
and BkrpNotes<>''
and BkrpNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or vtab.issid in (select issid from client.pfsecid where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfisin where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfuscode where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfsedol where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select client.pfcomptk.issid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    where ExchgCD = @fexchgcd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.notificationdate>@fcfdate and vtab.notificationdate<@ftdate))                    
order by vtab.EventID desc;

-- # big 4
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BonNotes as NotesText
FROM wca.v10s_bon as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and BonNotes<>''
and BonNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or scmst.secid in (select secid from client.pfsecid where actflag<>'D' and accid=@faccid)
    or scmst.secid in (select client.pfisin.secid from client.pfisin where actflag<>'D' and accid=@faccid)
    or scmst.secid in (select client.pfuscode.secid from client.pfuscode where actflag<>'D' and accid=@faccid)
    or scmst.secid in (select client.pfsedol.secid from client.pfsedol where actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or scmst.secid in
    (select scmst.secid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    where ExchgCD = @fexchgcd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or scmst.secid in
    (select scmst.secid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 5
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CallNotes as NotesText
FROM wca.v10s_call as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and CallNotes<>''
and CallNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.duedate>@fcfdate and vtab.duedate<@ftdate))
order by vtab.EventID desc;

-- # big 6
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM wca.v10s_caprd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and CapRdNotes<>''
and CapRdNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))
order by vtab.EventID desc;

-- # big 7
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConsdNotes as NotesText
FROM wca.v10s_consd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ConsdNotes<>''
and ConsdNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 8
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConvNotes as NotesText
FROM wca.v10s_conv as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ConvNotes<>''
and ConvNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.todate>@fcfdate and vtab.todate<@ftdate))
order by vtab.EventID desc;

-- # big 9
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CtxNotes as NotesText
FROM wca.v10s_ctx as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and CtxNotes<>''
and CtxNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.enddate>@fcfdate and vtab.enddate<@ftdate))
order by vtab.EventID desc;

-- # big 10
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CurRdNotes as NotesText
FROM wca.v10s_currd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and CurRdNotes<>''
and CurRdNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))
order by vtab.EventID desc;

-- # big 11
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DistNotes as NotesText
FROM wca.v10s_dist as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and DistNotes<>''
and DistNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 12
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DIVNotes
FROM wca.v10s_div as vtab
inner join wca.rd on vtab.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and DIVNotes<>''
and DIVNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 13
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DmrgrNotes as NotesText
FROM wca.v10s_dmrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and DmrgrNotes<>''
and DmrgrNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 14
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DvstNotes as NotesText
FROM wca.v10s_dvst as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and DvstNotes<>''
and DvstNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 15
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
EntNotes as NotesText
FROM wca.v10s_ent as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and EntNotes<>''
and EntNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 16
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
LawstNotes as NotesText
FROM wca.v10s_lawst as vtab
WHERE
vtab.actflag<>'D'
and LawstNotes<>''
and LawstNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or vtab.issid in (select issid from client.pfsecid where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfisin where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfuscode where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfsedol where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select client.pfcomptk.issid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    where ExchgCD = @fexchgcd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))                    
order by vtab.EventID desc;

-- # big 17
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
WHERE
vtab.actflag<>'D'
and LiquidationTerms<>''
and LiquidationTerms<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or vtab.issid in (select issid from client.pfsecid where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfisin where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfuscode where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select issid from client.pfsedol where actflag<>'D' and accid=@faccid)
    or vtab.issid in (select client.pfcomptk.issid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    where ExchgCD = @fexchgcd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or vtab.issid in
    (select issid from wca.scmst
    inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.rddate>@fcfdate and vtab.rddate<@ftdate))
order by vtab.EventID desc;

-- # big 18
SELECT
vtab.EventCD,
vtab.EventID,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and MrgrTerms<>''
and MrgrTerms<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 19
SELECT
vtab.EventCD,
vtab.EventID,
'Companies' as Fieldname,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and Companies<>''
and Companies<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 20
SELECT
vtab.EventCD,
vtab.EventID,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ApprovalStatus<>''
and ApprovalStatus<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 21
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PONotes
FROM wca.v10s_po as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and PONotes<>''
and PONotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 22
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PrfNotes
FROM wca.v10s_prf as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and PrfNotes<>''
and PrfNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 23
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PvRdNotes as NotesText
FROM wca.v10s_pvrd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and PvRdNotes<>''
and PvRdNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))
order by vtab.EventID desc;

-- # big 24
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RcapNotes as NotesText
FROM wca.v10s_rcap as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and RcapNotes<>''
and RcapNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))
order by vtab.EventID desc;

-- # big 25
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redem as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and RedemNotes<>''
and RedemNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.redemdate>@fcfdate and vtab.redemdate<@ftdate))
order by vtab.EventID desc;

-- # big 26
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RtsNotes
FROM wca.v10s_rts as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and RtsNotes<>''
and RtsNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 27
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ScChgNotes<>''
and ScChgNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.dateofchange>@fcfdate and vtab.dateofchange<@ftdate))
order by vtab.EventID desc;

-- # big 28
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ScSwpNotes
FROM wca.v10s_scswp as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and ScSwpNotes<>''
and ScSwpNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 29
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SDNotes
FROM wca.v10s_sd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and SDNotes<>''
and SDNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or vtab.rdid in (select rdid from wca.exdt where exdt.rdid=vtab.rdid and eventtype=vtab.eventcd
                  and exdate>=@fcfdate and exdate<=@ftdate))
order by vtab.EventID desc;

-- # big 30
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM wca.v10s_secrc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and SecRcNotes<>''
and SecRcNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.effectivedate>@fcfdate and vtab.effectivedate<@ftdate))
order by vtab.EventID desc;

-- # big 31
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and TkovrNotes<>''
and TkovrNotes<>'No further information'
and (@ffdate is null or vtab.acttime>@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
and (@faccid is null
    or wca.scmst.secid in (select code from client.pfsecid where client.pfsecid.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfisin.secid from client.pfisin where client.pfisin.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfuscode.secid from client.pfuscode where client.pfuscode.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfsedol.secid from client.pfsedol where client.pfsedol.actflag<>'D' and accid=@faccid)
    or wca.scmst.secid in (select client.pfcomptk.secid from client.pfcomptk where client.pfcomptk.actflag<>'D' and accid=@faccid))
and (@fexchgcd is null or wca.scmst.secid in (select wca.scexh.secid from wca.scexh where wca.scexh.ExchgCD = @fexchgcd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcntrycd is null or wca.scmst.secid in
    (select wca.scexh.secid from wca.scexh
    inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd where wca.exchg.CntryCD = @fcntrycd
    and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'))
and (@fcfdate is null or (vtab.closedate>@fcfdate and vtab.closedate<@ftdate))
order by vtab.EventID desc;
