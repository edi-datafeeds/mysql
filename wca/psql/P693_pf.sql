-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.693
-- filenamesuffix=
-- headerprefix=EDI_WCA_693_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\no_cull_feeds\693\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n


-- # 1
select
'ScexhID' as f3,
'SedolID' as f3,
'Actflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'Sedol' as f3,
'SedolCurrency' as f3,
'Defunct' as f3,
'SedolRegCntry' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3

-- # 2
select distinct
wca.scexh.ScexhID,
case when wca.sedol.sedolid is null then 0 else wca.sedol.sedolid end as sedolid,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
wca.scexh.Acttime as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.sedol.Sedol,
wca.sedol.curencd as SedolCurrency,
wca.sedol.Defunct,
wca.sedol.rcntrycd as SedolRegCntry,
wca.scexh.ExchgCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd=wca.sedol.cntrycd and 'D'<>wca.sedol.actflag
WHERE
(wca.scmst.isin in (select code from client.pfisin where accid=@faccid and actflag <> 'D')
    or wca.scmst.uscode in (select code from client.pfuscode where accid=@faccid and actflag <> 'D')
    or wca.scmst.secid in (select code from client.pfsecid where accid=@faccid and actflag <> 'D')
    or wca.sedol.sedol in (select code from client.pfsedol where accid=@faccid and actflag <> 'D'))
and ((issur.acttime>@ffdate
      or scmst.acttime>@ffdate
      or scexh.acttime>@ffdate
      or exchg.acttime>@ffdate
      or mktsg.acttime>@ffdate)
OR
 (wca.scmst.isin in (select code from client.pfisin where accid=@faccid and actflag = 'I')
  or wca.scmst.uscode in (select code from client.pfuscode where accid=@faccid and actflag = 'I')
  or wca.scmst.secid in (select code from client.pfsecid where accid=@faccid and actflag = 'I')
  or wca.sedol.sedol in (select code from client.pfsedol where accid=@faccid and actflag = 'I')))
and wca.scexh.actflag<>'D'
and wca.sedol.actflag<>'D';