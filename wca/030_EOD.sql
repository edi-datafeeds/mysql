-- arp=n:\EDIUK\030
-- ddt=dd/mm/yyyy
-- dfn=n
-- dtm=select ''
-- fex=.030
-- fty=y
-- hdt=SELECT ''
-- eof=select ''

-- # 1
SELECT
smf4.security.Sedol,
vtab.eventid as DistribId,
wca.divpy.optionid AS ElementID,
concat(issuer.issuername,' ',smf4.security.longdesc) AS SecurityDescription,
smf4.security.Isin,
wca.divpy.curencd as Currcode,
now() As FeedDate,
case when wca.exdt.exdate is not null then wca.exdt.exdate else pexdt.exdate end as exdate,
wca.rd.recdate AS Recdate,
case when wca.exdt.paydate is not null then wca.exdt.paydate else pexdt.paydate end as paydate,
wca.divpy.netdividend AS Payrate,
case when wca.divpy.divtype='C' then 'Net' else '' end AS Taxcode,
wca.divpy.taxrate as uktaxrate,
'' as Comment,
case when wca.divpy.divtype='C' then 'CASH' when wca.divpy.divtype='B' then 'STOCK OPTION' else 'STOCK DIVIDEND' End As Divtype,
substring(smf4.security.sectype,1,1) As Stocktype,
wca.divpy.rationew as Numerat,
wca.divpy.ratioold as Denomin,
wca.divpy.announcedate as CreationDate,
wca.divpy.actflag AS Actflag
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'GBLSE' = wca.scexh.exchgcd and 'D'<>wca.scexh.actflag
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid and 'GB' = wca.sedol.cntrycd
inner join smf4.security on wca.sedol.sedol = smf4.security.sedol
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.v10s_divpy as divpy on vtab.eventid = wca.divpy.divid
                   and 1=wca.divpy.optionid
left outer join wca.v10s_divpy as divopt2 on vtab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join wca.v10s_divpy as divopt3 on vtab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag

Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (divpy.acttime>=@ffdate and (@ftdate is null or divpy.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and wca.divpy.divid is not null
and wca.divpy.actflag <> 'D'
and wca.divpy.actflag <> 'C'
and (wca.divpy.divtype='B' or wca.divpy.divtype='C' or wca.divpy.divtype='S')
and divopt2.divid is null
and divopt3.divid is null
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
union
SELECT
smf4.security.Sedol,
vtab.eventid as DistribId,
wca.divpy.optionid AS ElementID,
concat(issuer.issuername,' ',smf4.security.longdesc) AS SecurityDescription,
smf4.security.Isin,
wca.divpy.curencd as Currcode,
now() As FeedDate,
case when wca.exdt.exdate is not null then wca.exdt.exdate else pexdt.exdate end as exdate,
wca.rd.recdate AS Recdate,
case when wca.exdt.paydate is not null then wca.exdt.paydate else pexdt.paydate end as paydate,
wca.divpy.netdividend AS Payrate,
case when wca.divpy.divtype='C' then 'Net' else '' end AS Taxcode,
wca.divpy.taxrate as uktaxrate,
'' as Comment,
'CURRENCY ELECTION' As Divtype,
substring(smf4.security.sectype,1,1) As Stocktype,
wca.divpy.rationew as Numerat,
wca.divpy.ratioold as Denomin,
wca.divpy.announcedate as CreationDate,
wca.divpy.actflag AS Actflag
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'GBLSE' = wca.scexh.exchgcd and 'D'<>wca.scexh.actflag
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid and 'GB' = wca.sedol.cntrycd
inner join smf4.security on wca.sedol.sedol = smf4.security.sedol
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.v10s_divpy as divpy on vtab.eventid = wca.divpy.divid
left outer join wca.v10s_divpy as divopt1 on vtab.eventid = divopt1.divid
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join wca.v10s_divpy as divopt2 on vtab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join wca.v10s_divpy as divopt3 on vtab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join wca.v10s_divpy as divopt4 on vtab.eventid = divopt4.divid
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join wca.v10s_divpy as divopt5 on vtab.eventid = divopt5.divid
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join wca.v10s_divpy as divopt6 on vtab.eventid = divopt6.divid
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (divpy.acttime>=@ffdate and (@ftdate is null or divpy.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and wca.divpy.divid is not null
and wca.divpy.actflag<>'C'
and wca.divpy.actflag<>'D'
and not ((wca.divpy.divtype='B' or wca.divpy.divtype='C' or wca.divpy.divtype='S')
          and divopt2.divid is null
          and divopt3.divid is null)
and (divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null)
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
union
SELECT
smf4.security.Sedol,
vtab.eventid as DistribId,
wca.divpy.optionid AS ElementID,
concat(issuer.issuername,' ',smf4.security.longdesc) AS SecurityDescription,
smf4.security.Isin,
wca.divpy.curencd as Currcode,
now() As FeedDate,
case when wca.exdt.exdate is not null then wca.exdt.exdate else pexdt.exdate end as exdate,
wca.rd.recdate AS Recdate,
case when wca.exdt.paydate is not null then wca.exdt.paydate else pexdt.paydate end as paydate,
wca.divpy.netdividend AS Payrate,
case when wca.divpy.divtype='C' then 'Net' else '' end AS Taxcode,
wca.divpy.taxrate as uktaxrate,
'' as Comment,
'DIVIDEND OPTION' As Divtype,
substring(smf4.security.sectype,1,1) As Stocktype,
wca.divpy.rationew as Numerat,
wca.divpy.ratioold as Denomin,
wca.divpy.announcedate as CreationDate,
wca.divpy.actflag AS Actflag
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'GBLSE' = wca.scexh.exchgcd and 'D'<>wca.scexh.actflag
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid and 'GB' = wca.sedol.cntrycd
inner join smf4.security on wca.sedol.sedol = smf4.security.sedol
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.v10s_divpy as divpy on vtab.eventid = wca.divpy.divid
left outer join wca.v10s_divpy as divopt1 on vtab.eventid = divopt1.divid
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join wca.v10s_divpy as divopt2 on vtab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join wca.v10s_divpy as divopt3 on vtab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join wca.v10s_divpy as divopt4 on vtab.eventid = divopt4.divid
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join wca.v10s_divpy as divopt5 on vtab.eventid = divopt5.divid
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join wca.v10s_divpy as divopt6 on vtab.eventid = divopt6.divid
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (divpy.acttime>=@ffdate and (@ftdate is null or divpy.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and wca.divpy.divid is not null
and wca.divpy.actflag<>'C'
and wca.divpy.actflag<>'D'
and not ((wca.divpy.divtype='B' or wca.divpy.divtype='C' or wca.divpy.divtype='S')
          and divopt2.divid is null
          and divopt3.divid is null)
and not ((divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null))
and not ((divopt1.divtype = 'C' or divopt1.divtype = 'S')
     and (divopt2.divtype = 'C' or divopt2.divtype = 'S')
     and (divopt3.actflag is null))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
union
SELECT
smf4.security.Sedol,
vtab.eventid as DistribId,
wca.divpy.optionid AS ElementID,
concat(issuer.issuername,' ',smf4.security.longdesc) AS SecurityDescription,
smf4.security.Isin,
wca.divpy.curencd as Currcode,
now() As FeedDate,
case when wca.exdt.exdate is not null then wca.exdt.exdate else pexdt.exdate end as exdate,
wca.rd.recdate AS Recdate,
case when wca.exdt.paydate is not null then wca.exdt.paydate else pexdt.paydate end as paydate,
wca.divpy.netdividend AS Payrate,
case when wca.divpy.divtype='C' then 'Net' else '' end AS Taxcode,
wca.divpy.taxrate as uktaxrate,
'' as Comment,
'SCRIPT DIVIDEND' As Divtype,
substring(smf4.security.sectype,1,1) As Stocktype,
wca.divpy.rationew as Numerat,
wca.divpy.ratioold as Denomin,
wca.divpy.announcedate as CreationDate,
wca.divpy.actflag AS Actflag
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'GBLSE' = wca.scexh.exchgcd and 'D'<>wca.scexh.actflag
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid and 'GB' = wca.sedol.cntrycd
inner join smf4.security on wca.sedol.sedol = smf4.security.sedol
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.v10s_divpy as divpy on vtab.eventid = wca.divpy.divid
left outer join wca.v10s_divpy as divopt1 on vtab.eventid = divopt1.divid
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join wca.v10s_divpy as divopt2 on vtab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join wca.v10s_divpy as divopt3 on vtab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join wca.v10s_divpy as divopt4 on vtab.eventid = divopt4.divid
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join wca.v10s_divpy as divopt5 on vtab.eventid = divopt5.divid
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join wca.v10s_divpy as divopt6 on vtab.eventid = divopt6.divid
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (divpy.acttime>=@ffdate and (@ftdate is null or divpy.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and wca.divpy.divid is not null
and wca.divpy.actflag<>'C'
and wca.divpy.actflag<>'D'
and not ((wca.divpy.divtype='B' or wca.divpy.divtype='C' or wca.divpy.divtype='S')
          and divopt2.divid is null
          and divopt3.divid is null)
and not ((divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null))
and ((divopt1.divtype = 'C' or divopt1.divtype = 'S')
     and (divopt2.divtype = 'C' or divopt2.divtype = 'S')
     and (divopt3.actflag is null))
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D'
union
SELECT
smf4.security.Sedol,
vtab.eventid as DistribId,
wca.divpy.optionid AS ElementID,
concat(issuer.issuername,' ',smf4.security.longdesc) AS SecurityDescription,
smf4.security.Isin,
wca.divpy.curencd as Currcode,
now() As FeedDate,
case when wca.exdt.exdate is not null then wca.exdt.exdate else pexdt.exdate end as exdate,
wca.rd.recdate AS Recdate,
case when wca.exdt.paydate is not null then wca.exdt.paydate else pexdt.paydate end as paydate,
wca.divpy.netdividend AS Payrate,
case when wca.divpy.divtype='C' then 'Net' else '' end AS Taxcode,
wca.divpy.taxrate as uktaxrate,
'' as Comment,
case when wca.divpy.divtype ='C' then 'CASH'
     when wca.divpy.divtype ='B' then 'DIVIDEND OPTION'
     else 'STOCK DIVIDEND'
     end As Divtype,
substring(smf4.security.sectype,1,1) As Stocktype,
wca.divpy.rationew as Numerat,
wca.divpy.ratioold as Denomin,
wca.divpy.announcedate as CreationDate,
wca.divpy.actflag AS Actflag
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
inner join wca.scmst on wca.rd.secid = scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'GBLSE' = wca.scexh.exchgcd and 'D'<>wca.scexh.actflag
inner join wca.sedol on wca.scmst.secid = wca.sedol.secid and 'GB' = wca.sedol.cntrycd
inner join smf4.security on wca.sedol.sedol = smf4.security.sedol
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and vtab.eventcd = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and vtab.eventcd = pexdt.eventtype
left outer join wca.v10s_divpy as divpy on vtab.eventid = wca.divpy.divid
Where
(@ffdate is null
or (vtab.acttime>=@ffdate and (@ftdate is null or vtab.acttime<@ftdate))
or (divpy.acttime>=@ffdate and (@ftdate is null or divpy.acttime<@ftdate))
or (rd.acttime>=@ffdate and (@ftdate is null or rd.acttime<@ftdate))
or (exdt.acttime>=@ffdate and (@ftdate is null or exdt.acttime<@ftdate))
or (pexdt.acttime>=@ffdate and (@ftdate is null or pexdt.acttime<@ftdate)))
and wca.divpy.divid is not null
and (wca.divpy.actflag='C' or wca.divpy.actflag='D')
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D';
