--filepath=o:\Datafeed\Equity\682\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.682
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_682_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\682\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventCD' as f1,
'EventID' as f2,
'ScexhID' as f2,
'Actflag' as f9,
'Changed' as f8,
'Created' as f7,
'SecID' as f2,
'IssID' as f2,
'Isin' as f12,
'Uscode' as f3,
'Issuername' as f10,
'CntryofIncorp' as f3,
'SectyCD' as f11,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f11,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f4,
'Mic' as f3,	
'Micseg' as f3,		
'Localcode' as f13,
'ListStatus' as f3,


'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2

use wca
select distinct
'SDCHG' as EventCD,
stab.SdchgID as EventID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,


use wca
select
'SDCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldSedol as OldStatic,
stab.NewSedol as NewStatic
FROM SDCHG as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldSedol <> '' or stab.NewSedol <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))


--# 3
use wca
select
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldIsin as OldStatic,
stab.NewIsin as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldIsin <> '' or stab.NewIsin <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 3
use wca
select
stab.IccID as EventID,
'USCC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldUscode <> '' or stab.NewUscode <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))



--# 4
use wca
select
stab.InchgID as EventID,
'INCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.inchgdate is null
     then stab.announcedate
     ELSE stab.inchgdate
     END as EffectiveDate,
stab.OldCntryCD as OldStatic,
stab.NewCntryCD as NewStatic
FROM INCHG as stab
INNER JOIN SCMST ON stab.IssID = scmst.Issid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON SCMST.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldCntryCD <> '' or stab.NewCntryCD <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 5
use wca
select
stab.IschgID as EventID,
'ISCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.namechangedate is null
     then stab.announcedate
     else stab.namechangedate
     end as EffectiveDate,
stab.IssOldName as OldStatic,
stab.IssNewName as NewStatic
FROM ischg as stab
INNER JOIN SCMST ON stab.IssID = scmst.Issid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON SCMST.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
(stab.IssOldName <> '' or stab.IssNewName <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 6
use wca
select
stab.LccID as EventID,
'LCC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldLocalCode as OldStatic,
stab.NewLocalCode as NewStatic
FROM lcc as stab
inner join SCEXH ON stab.SecID = SCEXH.SecID and stab.exchgcd= scexh.exchgcd
INNER JOIN SCMST ON scexh.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
(stab.OldLocalCode <> '' or stab.NewLocalCode <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 7
use wca
select
stab.SCCHGID as EventID,
'SCCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.dateofchange is null
     then stab.announcedate
     else stab.dateofchange
     end as EffectiveDate,
stab.SecOldName as OldStatic,
stab.SecNewName as NewStatic
FROM scchg as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
(stab.SecOldName <> '' or stab.SecNewName <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 8
use wca
select
stab.LTCHGID as EventID,
'LTCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
'' as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldLot as OldStatic,
stab.NewLot as NewStatic
FROM ltchg as stab
inner join SCEXH ON stab.SecID = SCEXH.SecID and stab.exchgcd= scexh.exchgcd
INNER JOIN SCMST ON scexh.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
(stab.OldLot <> '' or stab.NewLot <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 10
use wca
select
stab.LstatID as EventID,
'LSTAT' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
'' as OldStatic,
stab.LStatStatus as NewStatic
FROM lstat as stab
inner join SCEXH ON stab.SecID = SCEXH.SecID and stab.exchgcd= scexh.exchgcd
INNER JOIN SCMST ON scexh.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))

--# 11
use wca
select
stab.scexhID as EventID,
'NLIST' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
case when SCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when SCEXH.Listdate is not null 
     then SCEXH.Listdate 
     else stab.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist as stab
INNER JOIN SCEXH ON stab.scexhid = SCEXH.scexhid
INNER JOIN SCMST ON SCEXH.secid = SCMST.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))


--# 13
use wca
select
stab.CurrdID as EventID,
'CURRD' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldCurenCD as OldStatic,
stab.NewCurenCD as NewStatic
FROM CURRD as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldCurenCD <> '' or stab.NewCurenCD <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))
