--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)FROM wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_689_Ratio_Created
--fileheadertext=EDI_WCA_689_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\689\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n


--# 0
SELECT
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'Notestext' as f4


--# 2
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConvNotes as NotesText
FROM wca.v10s_conv as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.ConvNotes is not null
and vtab.announcedate>'2012-11-13'

--# 5
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redem as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.RedemNotes is not null
and vtab.announcedate>'2012-11-13'

--# 7
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM wca.v10s_secrc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.SecRcNotes is not null
and vtab.announcedate>'2012-11-13'

--# 9
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BBNotes as NotesText
FROM wca.v10s_bb as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.BBNotes is not null
and vtab.announcedate>'2012-11-13'

--# 11
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM wca.v10s_caprd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.CapRdNotes is not null
and vtab.announcedate>'2012-11-13'

--# 13
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.TkovrNotes is not null
and vtab.announcedate>'2012-11-13'

--# 15
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BonNotes as NotesText
FROM wca.v10s_bon as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.BonNotes is not null
and vtab.announcedate>'2012-11-13'

--# 16
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConsdNotes as NotesText
FROM wca.v10s_consd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.ConsdNotes is not null
and vtab.announcedate>'2012-11-13'

--# 17
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DmrgrNotes as NotesText
FROM wca.v10s_dmrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.DmrgrNotes is not null
and vtab.announcedate>'2012-11-13'

--# 18
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DistNotes as NotesText
FROM wca.v10s_dist as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.DistNotes is not null
and vtab.announcedate>'2012-11-13'

--# 19
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DvstNotes as NotesText
FROM wca.v10s_dvst as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.DvstNotes is not null
and vtab.announcedate>'2012-11-13'

--# 20
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
EntNotes as NotesText
FROM wca.v10s_ent as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.EntNotes is not null
and vtab.announcedate>'2012-11-13'

--# 21
SELECT
vtab.EventCD,
vtab.EventID,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.MrgrTerms is not null
and vtab.announcedate>'2012-11-13'

--# 22
SELECT
vtab.EventCD,
vtab.EventID,
'Companies' as Fieldname,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.Companies is not null
and vtab.announcedate>'2012-11-13'

--# 23
SELECT
vtab.EventCD,
vtab.EventID,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.ApprovalStatus is not null
and vtab.announcedate>'2012-11-13'

--# 24
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PrfNotes
FROM wca.v10s_prf as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.PrfNotes is not null
and vtab.announcedate>'2012-11-13'


--# 26
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RtsNotes
FROM wca.v10s_rts as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.RtsNotes is not null
and vtab.announcedate>'2012-11-13'

--# 27
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ScSwpNotes
FROM wca.v10s_scswp as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.ScSwpNotes is not null
and vtab.announcedate>'2012-11-13'

--# 28
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SDNotes
FROM wca.v10s_sd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.SDNotes is not null
and vtab.announcedate>'2012-11-13'

--# 29
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DIVNotes
FROM wca.v10s_div as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.DIVnotes is not null
and vtab.announcedate>'2012-11-13'

--# 29
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
Notes
FROM wca.v10s_oddlt as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.notes is not null
and vtab.announcedate>'2012-11-13'

