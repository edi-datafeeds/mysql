--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)FROM wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.689
--suffix=_sample
--fileheadertext=EDI_WCA_689_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\689\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n


--# 0
SELECT
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'Notestext' as f4

--# 1
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CtxNotes as NotesText
FROM wca.v10s_ctx as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 2
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConvNotes as NotesText
FROM wca.v10s_conv as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 3
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CurRdNotes as NotesText
FROM wca.v10s_currd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 4
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PvRdNotes as NotesText
FROM wca.v10s_pvrd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 5
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.v10s_redem as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 6
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.v10s_scchg as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 7
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM wca.v10s_secrc as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 9
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BBNotes as NotesText
FROM wca.v10s_bb as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 10
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CallNotes as NotesText
FROM wca.v10s_call as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

 
--# 11
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM wca.v10s_caprd as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 12
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RcapNotes as NotesText
FROM wca.v10s_rcap as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 13
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.v10s_tkovr as vtab
inner join wca.scmst on vtab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 14
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ArrNotes as NotesText
FROM wca.v10s_arr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 15
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BonNotes as NotesText
FROM wca.v10s_bon as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 16
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ConsdNotes as NotesText
FROM wca.v10s_consd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 17
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DmrgrNotes as NotesText
FROM wca.v10s_dmrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 18
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DistNotes as NotesText
FROM wca.v10s_dist as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 19
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DvstNotes as NotesText
FROM wca.v10s_dvst as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 20
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
EntNotes as NotesText
FROM wca.v10s_ent as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 21
SELECT
vtab.EventCD,
vtab.EventID,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 22
SELECT
vtab.EventCD,
vtab.EventID,
'Companies' as Fieldname,
Companies
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 23
SELECT
vtab.EventCD,
vtab.EventID,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.v10s_mrgr as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 24
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PrfNotes
FROM wca.v10s_prf as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 25
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
PONotes
FROM wca.v10s_po as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 26
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
RtsNotes
FROM wca.v10s_rts as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 27
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
ScSwpNotes
FROM wca.v10s_scswp as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 28
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
SDNotes
FROM wca.v10s_sd as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 29
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
DIVNotes
FROM wca.v10s_div as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 30
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
Notes
FROM wca.v10s_oddlt as vtab
inner join wca.rd on vtab.eventid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and wca.scmst.secid in (select wca.scexh.secid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')


--# 31
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
LawstNotes as NotesText
FROM wca.v10s_lawst as vtab
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and vtab.issid in (select wca.scmst.issid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')

--# 32
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
BkrpNotes as NotesText
FROM wca.v10s_bkrp as vtab
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and vtab.issid in (select wca.scmst.issid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
                    
--# 33
SELECT
vtab.EventCD,
vtab.EventID,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.v10s_liq as vtab
WHERE
vtab.actflag<>'D'
and vtab.announcedate >='2013-01-01' and vtab.announcedate < '2013-03-08'
and vtab.issid in (select wca.scmst.issid from wca.scexh
               inner join client.pfsecid on wca.scexh.scexhid = client.pfsecid.code and 999= client.pfsecid.accid
               inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
               where (wca.scexh.liststatus<>'D' or wca.scexh.acttime>vtab.announcedate) and wca.scexh.actflag<>'D')
