--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_WCA_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\SCEXH\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.clssc on wca.scexh.secid = wca.clssc.secid
inner join wca.clsac on wca.clssc.clsacid = wca.clsac.clsacid
WHERE 
wca.clsac.clsacid in (select code from client.pfissid where accid=999 and client.pfissid.actflag='I')
