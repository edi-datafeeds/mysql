--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_WCA_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\ISSUR\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Acttime,
wca.issur.Announcedate,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
inner join wca.clsac on wca.issur.issid = wca.clsac.issid
WHERE
wca.clsac.clsacid in (select code from client.pfissid where accid=999 and client.pfissid.actflag='I')
