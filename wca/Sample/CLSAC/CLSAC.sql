--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CLSAC
--fileheadertext=EDI_WCA_CLSAC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\CLSAC\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('CLSAC') as tablename,
wca.main.Actflag,
wca.main.Acttime,
wca.main.Announcedate,
wca.main.IssID,
wca.main.CourtID,
wca.main.CaseNo,
wca.main.FilingDate,
wca.main.Judge,
wca.main.CaseName,
wca.main.Allegation,
wca.main.Eligsec,
wca.main.Investcls,
wca.main.OnBehalf,
wca.main.PeriodStartDate,
wca.main.PeriodEndDate,
wca.main.LeadPDate,
wca.main.PrffDate,
wca.main.ClaimfDate,
wca.main.ObjDate,
wca.main.ExclDate,
wca.main.LeadPtif,
wca.main.CaseStatus,
wca.main.Setcurcd,
wca.main.Setamount,
wca.main.PClsacID,
wca.main.ClsacID,
wca.main.Notes
from wca.clsac as main
WHERE
wca.main.clsacid in (select code from client.pfissid where accid=999 and client.pfissid.actflag='I')
