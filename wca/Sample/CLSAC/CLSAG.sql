--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CLSAG
--fileheadertext=EDI_WCA_CLSAG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\CLSAG\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('CLSAG') as tablename,
wca.main.Actflag,
wca.main.Acttime,
wca.main.Announcedate,
wca.main.ClsacID,
wca.main.ClsagID,
wca.main.Relate,
wca.main.AgncyID,
wca.main.Notes
from wca.clsag as main
inner join wca.clsac on wca.main.clsacid = wca.clsac.clsacid
WHERE
wca.clsac.clsacid in (select code from client.pfissid where accid=999 and client.pfissid.actflag='I')
