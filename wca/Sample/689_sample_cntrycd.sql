--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)FROM wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.689
--suffix=
--fileheadertext=EDI_WCA_689_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\689\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n


--# 0
SELECT
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'Notestext' as f4

--# 1
SELECT
'CTX' as EventCD,
etab.CtxID as EventID,
'Notes' as Fieldname,
CtxNotes as NotesText
FROM wca.ctx as etab
WHERE
CtxNotes is not null
and etab.ctxid = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.ctxid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 2
SELECT
'CONV' as EventCD,
etab.ConvID as EventID,
'Notes' as Fieldname,
ConvNotes as NotesText
FROM wca.conv as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
ConvNotes is not null
and etab.ConvID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.ConvID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 3
SELECT
'CURRD' as EventCD,
etab.CurrdID as EventID,
'Notes' as Fieldname,
CurRdNotes as NotesText
FROM wca.currd as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
CurRdNotes is not null
and etab.CurrdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.CurrdID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 4
SELECT
'PVRD' as EventCD,
etab.PvRdID as EventID,
'Notes' as Fieldname,
PvRdNotes as NotesText
FROM wca.pvrd as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
PvRdNotes is not null
and etab.PvRdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.PvRdID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 5
SELECT
'REDEM' as EventCD,
etab.RedemID as EventID,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM wca.redem as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
RedemNotes is not null
and etab.RedemID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.RedemID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 6
SELECT
'SCCHG' as EventCD,
etab.ScChgID as EventID,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM wca.scchg as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
ScChgNotes is not null
and etab.ScChgID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.ScChgID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 7
SELECT
'SECRC' as EventCD,
etab.SecRcID as EventID,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM wca.secrc as etab
WHERE
SecRcNotes is not null
and etab.SecRcID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.SecRcID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 8
SELECT
'SDCHG' as EventCD,
etab.SdChgID as EventID,
'Notes' as Fieldname,
SdChgNotes as NotesText
FROM wca.sdchg as etab
inner join wca.scmst on etab.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
SdChgNotes is not null
and etab.SdChgID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.SdChgID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 9
SELECT
'BB' as EventCD,
etab.BBID as EventID,
'Notes' as Fieldname,
BBNotes as NotesText
FROM wca.bb as etab
WHERE
BBNotes is not null
and etab.BBID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.BBID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 10
SELECT
'CALL' as EventCD,
etab.CallID as EventID,
'Notes' as Fieldname,
CallNotes as NotesText
FROM wca.call_my as etab
WHERE
CallNotes is not null
and etab.CallID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.CallID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;
 
--# 11
SELECT
'CAPRD' as EventCD,
etab.CaprdID as EventID,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM wca.caprd as etab
WHERE
CapRdNotes is not null
and etab.CaprdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.CaprdID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 12
SELECT
'RCAP' as EventCD,
etab.RcapID as EventID,
'Notes' as Fieldname,
RcapNotes as NotesText
FROM wca.rcap as etab
WHERE
RcapNotes is not null
and etab.RcapID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.RcapID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 13
SELECT
'TKOVR' as EventCD,
etab.TkovrID as EventID,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM wca.tkovr as etab
WHERE
TkovrNotes is not null
and etab.TkovrID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.TkovrID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 14
SELECT
'ARR' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ArrNotes as NotesText
FROM wca.arr as etab
WHERE
ArrNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.RdID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 15
SELECT
'BON' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
BonNotes as NotesText
FROM wca.bon as etab
WHERE
BonNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 16
SELECT
'CONSD' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ConsdNotes as NotesText
FROM wca.consd as etab
WHERE
ConsdNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 17
SELECT
'DMRGR' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DmrgrNotes as NotesText
FROM wca.dmrgr as etab
WHERE
DmrgrNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 18
SELECT
'DIST' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DistNotes as NotesText
FROM wca.dist as etab
WHERE
DistNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 19
SELECT
'DVST' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DvstNotes as NotesText
FROM wca.dvst as etab
WHERE
DvstNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 20
SELECT
'ENT' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
EntNotes as NotesText
FROM wca.ent as etab
WHERE
EntNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 21
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'MrgrTerms' as Fieldname,
MrgrTerms
FROM wca.mrgr as etab
WHERE
MrgrTerms is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 22
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'Companies' as Fieldname,
Companies
FROM wca.mrgr as etab
WHERE
Companies is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 23
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'ApprovalStatus' as Fieldname,
ApprovalStatus
FROM wca.mrgr as etab
WHERE
ApprovalStatus is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 24
SELECT
'PRF' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
PrfNotes
FROM wca.prf as etab
WHERE
PrfNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 25
SELECT
'PO' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
PONotes
FROM wca.po as etab
WHERE
PONotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 26
SELECT
'RTS' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
RtsNotes
FROM wca.rts as etab
WHERE
RtsNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 27
SELECT
'SCSWP' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ScSwpNotes
FROM wca.scswp as etab
WHERE
ScSwpNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 28
SELECT
'SD' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
SDNotes
FROM wca.sd as etab
WHERE
SDNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 29
SELECT
'DIV' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DIVNotes
FROM wca.div_my as etab
WHERE
DIVNotes is not null
and etab.RdID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.rdid=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 30
SELECT
'LAWST' as EventCD,
etab.LawstID as EventID,
'Notes' as Fieldname,
LawstNotes as NotesText
FROM wca.lawst as etab
WHERE
LawstNotes is not null
and etab.LawstID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.LawstID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 31
SELECT
'BKRP' as EventCD,
etab.BkrpID as EventID,
'Notes' as Fieldname,
BkrpNotes as NotesText
FROM wca.bkrp as etab
WHERE
BkrpNotes is not null
and etab.BkrpID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.BkrpID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;

--# 32
SELECT
'LIQ' as EventCD,
etab.LiqID as EventID,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM wca.liq as etab
WHERE
LiquidationTerms is not null
and etab.LiqID = (
select distinct rdid from wca.rd
inner join wca.scexh on wca.rd.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
where etab.LiqID=wca.rd.rdid and
wca.exchg.cntrycd='DE' and wca.scexh.liststatus<>'D')
order by etab.announcedate desc
limit 20;
