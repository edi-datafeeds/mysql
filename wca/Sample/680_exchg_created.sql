--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_680_XXXXX
--fileheadertext=EDI_WCA_680_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\680\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n

--# 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43

--# 1
select vtab.* from wca.v680_ann as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 2
select vtab.* from wca.v680_arr as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 3
select vtab.* from wca.v680_assm as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 4
select vtab.* from wca.v680_bb as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 5
select vtab.* from wca.v680_bkrp as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 6
select vtab.* from wca.v680_bon as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 7
select vtab.* from wca.v680_br as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 8
select vtab.* from wca.v680_call as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 9
select vtab.* from wca.v680_caprd as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 10
select vtab.* from wca.v680_agm as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 11
select vtab.* from wca.v680_consd as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 12
select vtab.* from wca.v680_conv as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 13
select vtab.* from wca.v680_ctx as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 14
select vtab.* from wca.v680_currd as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 15
select vtab.* from wca.v680_dist as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 16
select vtab.* from wca.v680_div as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 17
select vtab.* from wca.v680_dmrgr as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 18
select vtab.* from wca.v680_drip as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 19
select vtab.* from wca.v680_dvst as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 20
select vtab.* from wca.v680_ent as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 21
select vtab.* from wca.v680_frank as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 22
select vtab.* from wca.v680_fychg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 23
select vtab.* from wca.v680_icc as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 24
select vtab.* from wca.v680_inchg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 25
select vtab.* from wca.v680_ischg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 26
select vtab.* from wca.v680_lawst as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 27
select vtab.* from wca.v680_lcc as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 28
select vtab.* from wca.v680_liq as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 29
select vtab.* from wca.v680_lstat as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 30
select vtab.* from wca.v680_ltchg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 31
select vtab.* from wca.v680_mkchg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 32
select vtab.* from wca.v680_mrgr as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 33
select vtab.* from wca.v680_nlist as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 34
select vtab.* from wca.v680_oddlt as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 35
select vtab.* from wca.v680_po as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 36
select vtab.* from wca.v680_prf as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 37
select vtab.* from wca.v680_pvrd as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 38
select vtab.* from wca.v680_rcap as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 39
select vtab.* from wca.v680_redem as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 40
select vtab.* from wca.v680_rts as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 41
select vtab.* from wca.v680_scchg as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 42
select vtab.* from wca.v680_scswp as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 43
select vtab.* from wca.v680_sd as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 44
select vtab.* from wca.v680_secrc as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');

--# 45
select vtab.* from wca.v680_tkovr as vtab
inner join client.exchgloop on vtab.exchgcd = client.exchgloop.code
where (vtab.announcedate >='2012-01-01' and vtab.announcedate < '2013-03-06');
