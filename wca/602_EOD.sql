--filepath=o:\Datafeed\wca\602\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.602
--suffix=
--fileheadertext=EDI_STATIC_602_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\602\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--zerorowchk=n


--# 1
SELECT 
wca.issur.Issuername,
wca.issur.CntryofIncorp,
wca.scmst.IssID,
wca.scmst.SecID,
wca.scmst.Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.Securitydesc,
wca.scmst.CurenCD as ParValueCurrency,
wca.scmst.Parvalue,
wca.scmst.SectyCD,
wca.scmst.Uscode,
wca.scmst.Isin,
wca.scmst.StructCD,
wca.sedol.Sedol,
wca.sedol.RCntryCD as RegCountry,
wca.sedol.Defunct,
wca.scexh.exchgcd,
wca.scexh.localcode,
wca.scexh.liststatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty
FROM wca.scmst
LEFT OUTER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID
LEFT OUTER JOIN wca.sectygrp ON wca.scmst.sectycd = wca.sectygrp.sectycd
LEFT OUTER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.exchg ON wca.scexh.ExchgCD = wca.exchg.ExchgCD
LEFT OUTER JOIN wca.sedol ON wca.scmst.SecID = wca.sedol.SecID
                      AND wca.exchg.cntryCD = wca.sedol.CntryCD
where
(wca.issur.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.sedol.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))
AND wca.sectygrp.secgrpid <3