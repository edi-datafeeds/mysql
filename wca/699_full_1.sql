-- arc=y
-- arp=n:\temp\
-- fsx=_699
-- hpx=EDI_WCA_699_
-- dfn=l
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')

-- # BIG 1
select distinct
wca.scexh.ScexhID,
case when wca.bbc.BbcID is null then 0 else wca.bbc.BbcID end as BbcID,
case when wca.bbe.BbeID is null then 0 else wca.bbe.BbeID end as BbeID,
case when wca.scmst.ActFlag = 'D' then 'D' else wca.scexh.ActFlag end as Actflag,
case when (scmst.acttime > scexh.acttime)
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,
wca.scexh.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IndusID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.bbc.curencd as BbgCurrency,
wca.bbc.bbgcompid as BbgCompositeGlobalID,
wca.bbc.bbgcomptk as BbgCompositeTicker,
wca.bbe.bbgexhid as BbgGlobalID,
wca.bbe.bbgexhtk as BbgExchangeTicker,
wca.scmst.StructCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
case when wca.mktsg.mktsgid is null then '' else wca.mktsg.mic end as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as ListStatus,
case when wca.warex.WarexID is not null 
     then wca.warex.ExerSecID
     when wca.rts.rtsid is not null 
     then wca.rd.SecID
     else null end as UnSecID,
case when wca.warex.WarexID is not null
     then 'C'
     else '' end as CallPut,
case when wca.warex.WarexID is not null
     then wca.warex.StrikePrice
     when wca.rts.rtsid is not null
     then wca.rts.IssuePrice
     else '' end as Price,
case when wca.warex.WarexID is not null
     then wca.warex.CurenCD
     when wca.rts.rtsid is not null
     then wca.rts.CurenCD
     else '' end as PriceCurrency,
case when wca.warex.WarexID is not null
     then wca.wartm.ExpirationDate
     when wca.rts.rtsid is not null
     then wca.rts.EndSubscription
     else null end as ExpirationDate,
wca.wartm.ExerciseStyle,
case when wca.warex.WarexID is not null
     then wca.warex.Ratioold
     when wca.rts.rtsid is not null
     then wca.rts.OldExerciseRatio
     else '' end as ExerRatioOld,
case when wca.warex.WarexID is not null
     then wca.warex.Rationew
     when wca.rts.rtsid is not null
     then wca.rts.RatioNew
     else '' end as ExerRatioNew,
wca.rts.OldDistRatio as DistRatioOld,
wca.rts.NewDistRatio as DistRatioNew
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.bbe on wca.scexh.secid = wca.bbe.secid and wca.scexh.exchgcd = bbe.exchgcd and wca.bbc.curencd=wca.bbe.curencd and 'D'<>wca.bbe.actflag
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag
left outer join wca.wartm on wca.scmst.secid = wca.wartm.secid
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
WHERE
wca.scexh.scexhid is not null
and substring(wca.scexh.exchgcd,3,3)<>'BND'
and (wca.scexh.Exchgcd = wca.scmst.PrimaryExchgCD or wca.scmst.PrimaryExchgCd = '')
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
and wca.warex.todate>now()
order by wca.warex.todate limit 1));
