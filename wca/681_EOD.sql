--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.681
--suffix=
--fileheadertext=EDI_WCA_681_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\681\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n

--# 0
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'ScexhID' as f3,
'Acflag' as f3,
'Changed' as f3,
'Created' as f3,
'SecID' as f2,
'IssID' as f3,
'Isin' as f3,
'Uscode' as f3,
'IssuerName' as f3,
'CntryofIncorp' as f3,
'SectyCD' as f3,
'SecurityDesc' as f3,
'ParValue' as f3,
'PVCurrency' as f3,
'StatusFlag' as f3,
'PrimaryExchgCD' as f3,
'ExchgCntry' as f3,
'ExchgCD' as f3,	
'Mic' as f3,	
'Micseg' as f3,		
'LocalCode' as f3,	
'ListStatus' as f3,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Date9type' as f18,
'Date9' as f19,
'Date10type' as f18,
'Date10' as f19,
'Date11type' as f18,
'Date11' as f19,
'Date12type' as f18,
'Date12' as f19,
'Paytype' as f22,
'Priority' as dummy,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43,
'Field7name' as f42,
'Field7' as f43,
'Field8name' as f42,
'Field8' as f43,
'Field9name' as f42,
'Field9' as f43,
'Field10name' as f42,
'Field10' as f43,
'Field11name' as f42,
'Field11' as f43,
'Field12name' as f42,
'Field12' as f43,
'Field13name' as f40,
'Field13' as f41,
'Field14name' as f42,
'Field14' as f43,
'Field15name' as f42,
'Field15' as f43,
'Field16name' as f42,
'Field16' as f43,
'Field17name' as f42,
'Field17' as f43,
'Field18name' as f42,
'Field18' as f43,
'Field19name' as f42,
'Field19' as f43,
'Field20name' as f42,
'Field20' as f43,
'Field21name' as f42,
'Field21' as f43,
'Field22name' as f42,
'Field22' as f43


--# 1
select distinct
vtab.EventCD,
vtab.EventID,
case when wca.divpy.OptionID is null then '1' else wca.divpy.OptionID end as OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
vtab.ActFlag,
vtab.Acttime as Changed,
vtab.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.exchg.CntryCD as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
wca.mktsg.Mic as Micseg,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then 'L' else wca.scexh.ListStatus end as ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.PayDate2 IS NOT NULL then wca.exdt.PayDate2 ELSE pexdt.paydate2 END as Date3,
'FYEDate' as Date5Type,
vtab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
vtab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
vtab.DeclarationDate as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
wca.divpy.Divtype as Paytype,
wca.rdprt.Priority,
case when wca.divpy.defaultopt='T' then 'T' else 'F' end as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
wca.divpy.RatioNew,
wca.divpy.RatioOld,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
vtab.Marker as Field1,
'Frequency' as Field2Name,
vtab.Frequency as Field2,
'Tbaflag' as Field3Name,
case when vtab.Tbaflag='T' then 'T' else 'F' end as Tbaflag,
'NilDividend' as Field4Name,
case when vtab.NilDividend='T' then 'T' else 'F' end as NilDividend,
'DivRescind' as Field5Name,
case when vtab.DivRescind='T' then 'T' else 'F' end as DivRescind,
'RecindCashDiv' as Field6Name,
case when wca.divpy.RecindCashDiv='T' then 'T' else 'F' end as RecindCashDiv,
'RecindStockDiv' as Field7Name,
case when wca.divpy.RecindStockDiv='T' then 'T' else 'F' end as RecindStockDiv,
'Approxflag' as Field8Name,
case when wca.divpy.Approxflag='T' then 'T' else 'F' end as Approxflag,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'' as Field12Name,
'' as Field12,
'' as Field13Name,
'' as Field13,
'' as Field14Name,
'' as Field14,
'' as Field15Name,
'' as Field15,
'' as Field16Name,
'' as Field16,
'' as Field17Name,
'' as Field17,
'' as Field18Name,
'' as Field18,
'' as Field19Name,
'' as Field19,
'' as Field20Name,
'' as Field20,
'' as Field21Name,
'' as Field21,
'' as Field22Name,
'' as Field22
from wca.v10s_div as vtab
inner join wca.rd on vtab.rdid=wca.rd.rdid
left outer join wca.rdprt on vtab.rdid=wca.rdprt.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
left outer join wca.divpy on vtab.divid = wca.divpy.divid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'div' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'div' = pexdt.eventtype
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
WHERE
vtab.acttime>(select max(feeddate) from wca.tbl_opslog where seq = 3)