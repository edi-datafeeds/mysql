--filepath=o:\prodman\otherfeeds\SMF\121\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.121
--suffix=
--fileheadertext=EDI_SMF_121_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\121i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select 2 as seq
--sevent=n
--shownulls=n

--#
SELECT
smf4.security.Sedol as SEDOL,
smf4.security.Isin as ISIN,
smf4.issuer.Issuername as ISSUERNAME,
replace(smf4.security.Longdesc,'|',' ') as LONGDESC,
smf4.security.Cregcode as CREGCODE,
smf4.security.Statusflag as STATUSFLAG,
smf4.security.OPOL as OPOL,
smf4.security.Sectype as SECTYPE,
wca.dprcp.DRtype as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.UnitofQcurrcode)<>3 then '' else smf4.security.UnitofQcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION,
smf4.oldopol.oldopol
from smf4.security
left outer join smf4.oldopol on smf4.security.securityid = smf4.oldopol.securityid
LEFT OUTER JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
LEFT OUTER JOIN wca.sedol ON smf4.security.Sedol = wca.sedol.Sedol
LEFT OUTER JOIN wca.dprcp ON wca.sedol.secid = wca.dprcp.secid
where
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.issuer.issuerid is not null
and smf4.security.longdesc not like '%STOCK CONNECT%'
union
SELECT
smf4.security.Sedol as SEDOL,
smf4.security.Isin as ISIN,
smf4.issuer.Issuername as ISSUERNAME,
replace(smf4.security.Longdesc,'|',' ') as LONGDESC,
smf4.security.Cregcode as CREGCODE,
smf4.security.Statusflag as STATUSFLAG,
smf4.security.OPOL as OPOL,
smf4.security.Sectype as SECTYPE,
wca.dprcp.DRtype as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.UnitofQcurrcode)<>3 then '' else smf4.security.UnitofQcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION,
smf4.oldopol.oldopol
FROM smf4.issuer
LEFT OUTER JOIN smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join smf4.oldopol on smf4.security.securityid = smf4.oldopol.securityid
LEFT OUTER JOIN wca.sedol ON smf4.security.Sedol = wca.sedol.Sedol
LEFT OUTER JOIN wca.dprcp ON wca.sedol.secid = wca.dprcp.secid
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
where
smf4.issuer.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.security.securityid is not null
and smf4.security.longdesc not like '%STOCK CONNECT%'
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
);
