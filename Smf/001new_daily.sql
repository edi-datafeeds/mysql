--filepath=o:\prodman\otherfeeds\smf\001\new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.001
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\001\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
select
smf4.issuer.actdate,
smf4.issuer.actflag,
smf4.issuer.issuerid,
smf4.issuer.cinccode,
smf4.issuer.ICBSuperSector as businessid,
'' as dummy,
smf4.issuer.issuername
from smf4.issuer
where
(smf4.issuer.actdate >= (select max(fromdate) from smf4.feedlog where feedfreq='smfdaily')
and smf4.issuer.actdate<=(select max(todate) from smf4.feedlog where feedfreq='smfdaily')
)
or
(smf4.issuer.headerdate >= (select max(fromdate) from smf4.feedlog where feedfreq='smfdaily')
and smf4.issuer.headerdate <= (select max(todate) from smf4.feedlog where feedfreq='smfdaily')
)

