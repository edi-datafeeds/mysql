--filepath=o:\prodman\otherfeeds\SMF\ml\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.ml
--suffix=
--fileheadertext=
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\SMF\ml\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--#
SELECT
smf4.security.SecurityID,
smf4.security.Actflag,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
'' as dummy1,
smf4.security.Cficode,
smf4.security.OPOL,
smf4.security.SecType,
smf4.security.Isin,
smf4.security.IssuerID,
' ' as Tidisplay,
replace(smf4.security.Longdesc,'|',' ') as longdesc,
smf4.security.Statusflag,
smf4.security.Sedol,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
smf4.security.Formflag,
'' as dummy2
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and sectype <> 'SP'
union
SELECT
smf4.security.SecurityID,
smf4.security.Actflag,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
'' as dummy1,
smf4.security.Cficode,
smf4.security.OPOL,
smf4.security.SecType,
smf4.security.Isin,
smf4.security.IssuerID,
' ' as Tidisplay,
replace(smf4.security.Longdesc,'|',' ') as longdesc,
smf4.security.Statusflag,
smf4.security.Sedol,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
smf4.security.Formflag,
'' as dummy2
FROM smf4.issuer
inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
where
smf4.issuer.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and sectype <> 'SP'
and smf4.security.securityid is not null
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
or smf4.issuer_prev.cinccode<>smf4.issuer.cinccode
or smf4.issuer_prev.ICBIndustry<>smf4.issuer.ICBIndustry
);

