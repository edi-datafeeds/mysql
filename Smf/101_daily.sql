--filepath=o:\prodman\otherfeeds\SMF\101\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_SMF_101_yyyymmdd
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\smf\101\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
SELECT
smf4.security.Actdate AS Actdate,
smf4.security.Actflag AS Actflag,
smf4.security.SecurityID,
smf4.security.Cregcode AS CountryofReg,
' '  as newInfosource,
smf4.security.OPOL,
smf4.security.Isin,
smf4.security.IssuerID,
smf4.issuer.Issuername,
smf4.security.Longdesc AS SecurityDescription,
smf4.security.Sedol,
smf4.security.Statusflag,
smf4.security.Eventcode,
smf4.issuer.Cinccode AS CountryofInc,
smf4.security.Sectype,
smf4.security.DomesticListingIndicator as PrimaryListing
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
union
SELECT
smf4.security.Actdate AS Actdate,
smf4.security.Actflag AS Actflag,
smf4.security.SecurityID,
smf4.security.Cregcode AS CountryofReg,
' '  as newInfosource,
smf4.security.OPOL,
smf4.security.Isin,
smf4.security.IssuerID,
smf4.issuer.Issuername,
smf4.security.Longdesc AS SecurityDescription,
smf4.security.Sedol,
smf4.security.Statusflag,
smf4.security.Eventcode,
smf4.issuer.Cinccode AS CountryofInc,
smf4.security.Sectype,
smf4.security.DomesticListingIndicator as PrimaryListing
FROM smf4.issuer
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
or smf4.issuer_prev.cinccode<>smf4.issuer.cinccode
or smf4.issuer_prev.ICBIndustry<>smf4.issuer.ICBIndustry
);


/*Format
field		Datatype(maxwidth)	Description
Acttime,	Date(10)		Action time (Last modified)
Actflag,	Char(1)			Action (I=Insert, U=Update, D=Delete, F=Full Alignment)
SecurityID,	Int(10)			Unique Global internal number at the sedol level
Cregcode,	Char(2)			ISO country of Reg Code (*)
Newinfosource,	Char(4)			New infosource
OPOL,		Char(3)			Official place of listing (MIC or UNL or FMQ - fund manager quote)
Isin,		Char(12)		ISIN
IssuerID,	Int(10)			Unique Global internal number for LSE issuer
Issuername,	Varchar(35)		Issuername
Longdesc,	Varchar(40)		Security Description
Sedol,		Char(7)		        Sedol code
Statusflag,	Char(1)		        Trading, Coded, Defunct
Eventcode,	Char(2)			Security Event Code (*)
Cinccode,	Char(2)			ISO country of Incorporation Code (*)
Sectype,	Char(2)			New style Security Type (*)
PrimaryListing	Char(1)			1 = Primary

(*) See SMF feed documentation for all possible values and meanings
Date format = YYYY/MM/DD
*/ 

