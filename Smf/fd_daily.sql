--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=FD
--filename=yymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
select
concat('FD ',(SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')),' ',(SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')));

--# 2
select
concat('"',smf4.security.Sedol,'"'),
concat('"',substring(concat(replace(smf4.issuer.issuername,'"','`'),'                                  '),1,35),'"'),
concat('"',substring(concat(replace(smf4.security.Longdesc,'"','`'),'                                       '),1,40),'"'),
concat('"',substring(concat(replace(ifnull(smf4.security.background,''),'"','`'),'                           '),1,27),'"'),
concat('"',(SELECT DATE_FORMAT((smf4.security.eventdate), '%y%m%d')),'"')
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and smf4.security.statusflag = 'D' or smf4.security.actflag='D';

--# 3
select 
concat('0000000','ENDOFFILE');

