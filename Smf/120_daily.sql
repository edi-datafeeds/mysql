--filepath=o:\Datafeed\SMF\120\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\120\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--#
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
replace(smf4.security.Longdesc,'|',' ') as longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
wca.dprcp.DRtype,
smf4.security.Restrictions
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
left outer join wca.dprcp ON wca.sedol.secid = wca.dprcp.secid and wca.dprcp.actflag <> 'D'
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
union
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
replace(smf4.security.Longdesc,'|',' ') as longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
wca.dprcp.DRtype,
smf4.security.Restrictions
FROM smf4.issuer
inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
left outer join wca.dprcp ON wca.sedol.secid = wca.dprcp.secid and wca.dprcp.actflag <> 'D'
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
ORDER BY Sedol