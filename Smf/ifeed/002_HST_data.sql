-- arc=y
-- arp=n:\Temp\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.002
-- fty=select 'n'
-- hdt=select ''
-- hpx=select ''
-- eof=select ''
-- dfo=n

-- #1 BIG
SELECT
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
' ' as Assentflag,
' ' as fundflag,
smf4.security.Cficode,
smf4.security.Cregcode,
' ' as cregstat,
' ' as dealflag,
smf4.security.Background,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
' ' as Localcode,
smf4.security.Closedate,
' ' as classflag,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Eventcode,
CASE WHEN (smf4.security.EventDate < '1950/01/01') THEN NULL ELSE smf4.security.EventDate END AS EvntDate,
smf4.security.Formflag,
smf4.security.Statusflag,
smf4.security.SecType,
smf4.security.Sedol,
' ' as sysindic,
' ' as TiDisplay,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
' ' as Accrualflag
FROM smf4.security
where
smf4.security.securityid > @gthanpid
and smf4.security.actflag<>'D'
and smf4.security.longdesc not like '%STOCK CONNECT%'
order by smf4.security.securityid 
limit 300000;

