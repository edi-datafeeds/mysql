-- arc=y
-- arp=n:\SMF\002\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
-- fex=.002
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
-- hpx=SELECT CONCAT('H11',DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d'), '0000001684S');
-- eof=T110000000000
-- dfo=n
-- fsx=select case when(substring(cast(max(feeddate) as char),12,2) ='12') then '_1' else '_2' end from smf4.tbl_opslog
-- hsx=select case when(substring(cast(max(feeddate) as char),12,2) ='12') then '_1' else '_2' end from smf4.tbl_opslog

-- # 1
select
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
' ' as Assentflag,
' ' as fundflag,
smf4.security.Cficode,
smf4.security.Cregcode,
' ' as cregstat,
' ' as dealflag,
smf4.security.Background,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
' ' as Localcode,
smf4.security.Closedate,
' ' as classflag,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Eventcode,
case when (smf4.security.eventdate < '1950/01/01') then null else smf4.security.eventdate end as EvntDate,
smf4.security.Formflag,
smf4.security.Statusflag,
smf4.security.SecType,
smf4.security.Sedol,
' ' as sysindic,
' ' as TiDisplay,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
' ' as Accrualflag
from smf4.security
where
smf4.security.longdesc not like '%stock connect%'
and
(smf4.security.headerdate >= (select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and (smf4.security.confirmation is not null)
and (smf4.security.longdesc not like '%stock connect%'));
