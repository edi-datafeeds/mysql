-- arc=y
-- arp=n:\Temp\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.001
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=SELECT CONCAT('H11',DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d'), '0000001684I');
-- eof=T110000000000
-- dfo=n

-- # 1
SELECT
smf4.issuer.Actdate,
smf4.issuer.Actflag,
smf4.issuer.IssuerID,
smf4.issuer.Cinccode,
smf4.issuer.ICBIndustry as Induscode,
' ' as Dummy,
smf4.issuer.Issuername
FROM smf4.issuer
where
smf4.issuer.actflag<>'D'

