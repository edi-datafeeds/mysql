-- arc=y
-- arp=n:\SMF\001\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.001
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=SELECT CONCAT('H11',DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d'), '0000001684I');
-- eof=T110000000000
-- dfo=n

-- # 1
SELECT
smf4.issuer.Actdate,
smf4.issuer.Actflag,
smf4.issuer.IssuerID,
smf4.issuer.Cinccode,
smf4.issuer.ICBIndustry as Induscode,
' ' as Dummy,
smf4.issuer.Issuername
FROM smf4.issuer
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
where
smf4.issuer.headerdate >= (select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
or smf4.issuer_prev.cinccode<>smf4.issuer.cinccode
or smf4.issuer_prev.ICBIndustry<>smf4.issuer.ICBIndustry
);
