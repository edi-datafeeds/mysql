-- arc=y
-- arp=n:\Temp\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=SELECT CONCAT('H11',DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d'), '0000001684S');
-- eof=select ''
-- dfo=n

-- #1 BIG
SELECT
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
smf4.issuer.ICBIndustry as Induscode,
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
' ' as Assentflag,
smf4.security.Cficode,
smf4.security.Cregcode,
smf4.security.Background,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
smf4.security.Closedate,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Eventcode,
CASE WHEN (smf4.security.EventDate < '1950/01/01') THEN NULL ELSE smf4.security.EventDate END AS EventDate,
smf4.security.Formflag,
smf4.security.Statusflag,
smf4.security.SecType,
smf4.security.Sedol,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
' ' as Accrualflag
FROM smf4.security
inner join smf4.issuer on smf4.security.issuerid=smf4.issuer.issuerid
where
smf4.security.actflag<>'D'
and smf4.issuer.actflag<>'D'
and (smf4.security.sectype='CJ'
or smf4.security.sectype='CK'
or smf4.security.sectype='CL'
or smf4.security.sectype='DL')
limit 200000, 200000;
