-- arc=y
-- arp=n:\SMF\002\
-- ddt=yyyymmdd
-- dfn=n
-- dtm=select ' '
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.002
-- fty=
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=SELECT CONCAT('H11',DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d'), '0000001684S');
-- eof=T110000000000
-- dfo=n

-- #
SELECT
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
' ' as Assentflag,
' ' as fundflag,
smf4.security.Cficode,
smf4.security.Cregcode,
' ' as cregstat,
' ' as dealflag,
smf4.security.Background,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
' ' as Localcode,
smf4.security.Closedate,
' ' as classflag,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Eventcode,
CASE WHEN (smf4.security.EventDate < '1950/01/01') THEN NULL ELSE smf4.security.EventDate END AS EvntDate,
smf4.security.Formflag,
smf4.security.Statusflag,
smf4.security.SecType,
smf4.security.Sedol,
' ' as sysindic,
' ' as TiDisplay,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
' ' as Accrualflag
FROM smf4.security
where
(smf4.security.headerdate >= (select max(feeddate) from wca.tbl_opslog)
or (sedol='B5B7CM0'
or sedol='B093XC2'
or sedol='B1FSCS7'
or sedol='B0YWFV7'
or sedol='B19FKV3'
or sedol='B19FL98'
or sedol='B0RP6N2'
or sedol='B0RP7N9'
or sedol='B0VTRV5'
or sedol='B0VTRY8'
or sedol='B0JLMP2'
or sedol='B0MML66'
or sedol='B59HTY3'
or sedol='B18QDB8'
or sedol='B0P9M83'
or sedol='B17KR19'
or sedol='B15VBW3'
or sedol='B16LKQ9'
or sedol='B1ZBZV0'
or sedol='B1L7Z78'
or sedol='B23VTG2'
or sedol='B16MV03'
or sedol='B17KQX4'
or sedol='B1FKKW3'
or sedol='B59HTW1'
or sedol='B1FLQM8'
or sedol='7651649'
or sedol='B1CZ980'
or sedol='7517138'
or sedol='B1FLQN9'
or sedol='B1FPMQ6'
or sedol='B61L4W3'
or sedol='B1HN1X8'
or sedol='B28Y892'
or sedol='B1WVW01'
or sedol='B1YCSB5'
or sedol='B3NPTY9'
or sedol='B2Q95M1'
or sedol='B2Q94J1'
or sedol='B2QW315'
or sedol='B2R2WQ2'
or sedol='B2QN9S7'
or sedol='B2R62Y2'
or sedol='B2R8CS2'
or sedol='B2QPCJ5'
or sedol='B2R92H4'
or sedol='B3DMXG0'
or sedol='B3CCJD0'
or sedol='B3CCJJ6'
or sedol='B3C9GQ3'
or sedol='B5MTJ79'
or sedol='B17KR42'
or sedol='B39H2S4'
or sedol='B61F4N6'
or sedol='B3M90M7'
or sedol='B67JDZ7'
or sedol='B41KPV2'
or sedol='B4TR0K9'
or sedol='BRGBY58'
or sedol='7093315'
or sedol='B048XK0'
or sedol='5046957'
or sedol='5255719'
or sedol='4458326'
or sedol='4537957'
or sedol='5273142'
or sedol='B1BZ389'
or sedol='B031399'
or sedol='B23Z4S1'
or sedol='B298GT9'
or sedol='B1FRPG3'
or sedol='B1LDTW9'
or sedol='B3KJSG9'
or sedol='B3F2M94'
or sedol='B232SL3'
or sedol='B3CY0T9'
or sedol='B5N6VD9'))
and smf4.security.longdesc not like '%STOCK CONNECT%';
