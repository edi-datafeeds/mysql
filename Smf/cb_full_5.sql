--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=CB
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_5
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=FW
--archive=n
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=LS7,LS12,LS35,LS40,LS4,LS2,LS2,LS2,LS1,LS14
--incremental=n
--sevent=n
--shownulls=n

--#
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.OPOL,
'  ' as dummy1,
smf4.security.Cregcode,
'  ' as dummy2,
smf4.security.Statusflag,
smf4.security.Eventcode as dummy3
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.actflag<>'D'
limit 4000000, 1000000;
