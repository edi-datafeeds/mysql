--filepath=o:\Datafeed\Smf\131\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.131
--suffix=
--fileheadertext=EDI_SMF_131_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\131\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
SELECT
smf4.security.Sedol,
smf4.security.PrevSedol,
smf4.security.Actflag,
smf4.security.Actdate,
smf4.security.Confirmation,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
smf4.security.Longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
smf4.security.Eventdate,
smf4.eventtype.Description,
smf4.security.Sectype,
smf4.security.Background
FROM smf4.security
Inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
LEFT OUTER JOIN smf4.eventtype ON smf4.security.Eventcode = smf4.eventtype.code
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and (smf4.security.sectype = 'CL'
or smf4.security.sectype = 'CK')
and cinccode = 'GB'
union
SELECT
smf4.security.Sedol,
smf4.security.PrevSedol,
smf4.security.Actflag,
smf4.security.Actdate,
smf4.security.Confirmation,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
smf4.security.Longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
smf4.security.Eventdate,
smf4.eventtype.Description,
smf4.security.Sectype,
smf4.security.Background
from smf4.issuer
Inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
LEFT OUTER JOIN smf4.eventtype ON smf4.security.Eventcode = smf4.eventtype.code
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and (smf4.security.sectype = 'CL'
or smf4.security.sectype = 'CK')
and cinccode = 'GB'
