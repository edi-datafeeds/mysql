--filepath=o:\datafeed\bahar\122i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.122
--suffix=
--fileheadertext=EDI_SMF_122_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\122i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 0 as seq
--sevent=n
--shownulls=n

--# 1
SELECT distinct
smf4.security.SecurityID,
smf4.security.Actdate as tempActtime,
smf4.security.Actflag as tempActflag,
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.PrevSedol,
smf4.security.IssuerID,
smf4.security.Sedol,
smf4.security.Isin,
smf4.security.Cregcode,
smf4.security.CouponIR,
smf4.security.Parvalue,
smf4.security.PvCurrenCD,
smf4.security.UnderlyingInstr,
smf4.security.UnderlyingIssuer,
smf4.security.StrikePrice,
smf4.security.StrikePriceCurrency,
smf4.security.Formflag,
smf4.security.SecType,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
smf4.security.Cficode,
smf4.security.CloseDate,
smf4.security.ClosingDateType,
smf4.security.Restrictions,
smf4.security.OPOL,
smf4.security.DomesticListingIndicator as PrimaryListing,
' ' as NewInfosource,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Background,
' ' as Accrualflag,
' ' as Assentflag,
smf4.security.Eventcode,
smf4.security.Eventdate,
smf4.security.Statusflag,
smf4.security.Confirmation,
'' as CAID,
'' as CAType,
smf4.security.CreDate,
smf4.security.HeaderDate,
' ' as fundflag,
' ' as cregstat,
' ' as dealflag,
' ' as Localcode,
' ' as classflag,
' ' as sysindic,
smf4.market.Tidm as Tidisplay,
' ' as Previssname,
smf4.security.OPOL as MIC,
substring(smf4.security.OPOL,2,3) as Infosource,
' ' as typeflag,
smf4.issuer.Actflag,
smf4.issuer.Actdate,
smf4.issuer.IssuerID,
smf4.issuer.Issuername,
smf4.issuer.AlternateIssuername,
smf4.issuer.Cinccode,
smf4.issuer.ICBIndustry as Induscode,
smf4.issuer.ICBSuperSector as BusinessID,
'' as ParentID,
'' as SectorCode,
smf4.issuer.Comment,
smf4.issuer.IssuerStatus,
'C' as Confirmation,
'' as CAID,
'' as CAType,
smf4.issuer.CreDate,
smf4.issuer.HeaderDate
FROM smf4.security
INNER JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
left outer join smf4.market on smf4.security.securityid = smf4.market.securityid
            and (smf4.security.opol = 'XLON' or smf4.security.opol = 'AIMX')
where
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.issuer.issuerid is not null
union
SELECT
smf4.security.SecurityID,
smf4.security.Actdate as tempActtime,
smf4.security.Actflag as tempActflag,
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.PrevSedol,
smf4.security.IssuerID,
smf4.security.Sedol,
smf4.security.Isin,
smf4.security.Cregcode,
smf4.security.CouponIR,
smf4.security.Parvalue,
smf4.security.PvCurrenCD,
smf4.security.UnderlyingInstr,
smf4.security.UnderlyingIssuer,
smf4.security.StrikePrice,
smf4.security.StrikePriceCurrency,
smf4.security.Formflag,
smf4.security.SecType,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
smf4.security.Cficode,
smf4.security.CloseDate,
smf4.security.ClosingDateType,
smf4.security.Restrictions,
smf4.security.OPOL,
smf4.security.DomesticListingIndicator as PrimaryListing,
' ' as NewInfosource,
smf4.security.Longdesc,
smf4.security.Shortdesc,
smf4.security.Background,
' ' as Accrualflag,
' ' as Assentflag,
smf4.security.Eventcode,
smf4.security.Eventdate,
smf4.security.Statusflag,
smf4.security.Confirmation,
'' as CAID,
'' as CAType,
smf4.security.CreDate,
smf4.security.HeaderDate,
' ' as fundflag,
' ' as cregstat,
' ' as dealflag,
' ' as Localcode,
' ' as classflag,
' ' as sysindic,
smf4.market.Tidm as Tidisplay,
' ' as Previssname,
smf4.security.OPOL as MIC,
substring(smf4.security.OPOL,2,3) as Infosource,
' ' as typeflag,
smf4.issuer.Actflag,
smf4.issuer.Actdate,
smf4.issuer.IssuerID,
smf4.issuer.Issuername,
smf4.issuer.AlternateIssuername,
smf4.issuer.Cinccode,
smf4.issuer.ICBIndustry as Induscode,
smf4.issuer.ICBSuperSector as BusinessID,
'' as ParentID,
'' as SectorCode,
smf4.issuer.Comment,
smf4.issuer.IssuerStatus,
smf4.security.Confirmation,
'' as CAID,
'' as CAType,
smf4.issuer.CreDate,
smf4.issuer.HeaderDate
FROM smf4.issuer
INNER JOIN smf4.security ON smf4.issuer.IssuerID = smf4.security.issuerID
left outer join smf4.market on smf4.security.securityid = smf4.market.securityid
            and (smf4.security.opol = 'XLON' or smf4.security.opol = 'AIMX')
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
where
smf4.issuer.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.cinccode<>smf4.issuer.cinccode
);
