--filepath=c:\Datafeed\smf\001new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.001
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\001\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4;
select
issuer.actdate,
issuer.actflag,
issuer.issuerid,
issuer.cinccode,
issuer.businessid,
'' as dummy,
issuer.issuername
from issuer
where
(issuer.actdate >= (select max(fromdate) from feedlog where feedfreq='smfdaily')
and issuer.actdate<=(select max(todate) from feedlog where feedfreq='smfdaily')
and (issuer.confirmation='c')
and (issuer.confirmation is not null))
or
(issuer.headerdate >= (select max(fromdate) from feedlog where feedfreq='smfdaily')
and issuer.headerdate <= (select max(todate) from feedlog where feedfreq='smfdaily')
and (issuer.confirmation='c')
and (issuer.confirmation is not null))