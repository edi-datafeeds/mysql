--filepath=c:\Datafeed\SMF\001\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.002
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyymmdd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\002\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
select
smf4.security.actdate,
smf4.security.actflag,
smf4.security.securityid,
smf4.security.amtrans1,
smf4.security.amtrans2,
smf4.security.amtrans3,
smf4.security.amtrans4,
' ' as assentflag,
' ' as fundflag,
smf4.security.cficode,
smf4.security.cregcode,
' ' as cregstat,
' ' as dealflag,
smf4.security.background,
upper(smf4.security.opol),
smf4.security.isin,
smf4.security.issuerid,
' ' as localcode,
smf4.security.closedate,
' ' as classflag,
smf4.security.longdesc,
smf4.security.shortdesc,
smf4.security.eventcode,
eventdate = case when (smf4.security.eventdate < '1950/01/01') then null
             else smf4.security.eventdate end,
smf4.security.formflag,
smf4.security.statusflag,
smf4.security.sectype,
smf4.security.sedol,
' ' as sysindic,
' ' as tidisplay,
smf4.security.unitofq,
smf4.security.unitofqcurrcode,
' ' as accrualflag
from smf4.security
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and smf4.security.confirmation='C'
and smf4.security.longdesc not like '%STOCK CONNECT%';
