--filepath=c:\Datafeed\Smf\130\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.130
--suffix=
--fileheadertext=EDI_SMF_130_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\130\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use smf4;
select
dailyallsmf.tempacttime as acttime,
case when security.credate = security.headerdate then upper('i') else security.actflag end as actflag,
issuer.issuerid,
replace(replace(replace(issuer.issuername,char(39),'`'),'"','`'),',',';') as issuername,
issuer.cinccode,
issuer.businessid,
replace(replace(replace(issuer.comment,char(39),'`'),'"','`'),',',';') as comment,
issuer.issuerstatus,
security.sedol,
security.isin,
replace(replace(replace(security.longdesc,char(39),'`'),'"','`'),',',';') as longdesc,
security.cregcode,
security.opol,
security.sectype,
security.statusflag,
security.eventcode,
security.eventdate,
security.prevsedol,
security.closedate,
security.closingdatetype,
security.primarylisting,
replace(replace(replace(security.background,char(39),'`'),'"','`'),',',';') as background,
security.assentflag,
security.confirmation
from dailyallsmf
inner join security on security.securityid = dailyallsmf.securityid
left outer join issuer on security.issuerid = issuer.issuerid 
where security.sectype<>'cm'
and  security.sectype<>'cn'
order by security.sedol