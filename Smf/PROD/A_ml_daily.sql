--filepath=c:\Datafeed\SMF\ml\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.ml
--suffix=
--fileheadertext=
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\ml\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
use smf4;
select
dailyconf.securityid,
dailyconf.tempactflag,
issuer.issuername,
issuer.cinccode,
'' as dummy1,
security.cficode,
security.opol,
security.sectype,
security.isin,
security.issuerid,
' ' as tidisplay,
replace(security.longdesc,'|',' ') as longdesc,
security.statusflag,
security.sedol,
security.unitofq,
security.unitofqcurrcode,
security.formflag,
'' as dummy2
from dailyconf
inner join security on security.securityid = dailyconf.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where sectype <> 'sp'
