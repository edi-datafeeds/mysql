--filepath=c:\Datafeed\SMF\010\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.010
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\010\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4;
select
issuer.actdate,
issuer.actflag,
issuer.issuerid,
issuer.issuername,
issuer.cinccode,
'' as dummytab
from issuer
where (issuer.headerdate > curdate()-1)
and (issuer.confirmation='c')
and (issuer.confirmation is not null)
