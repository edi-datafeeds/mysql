--filepath=c:\Datafeed\SMF\CB\
--filenameprefix=NC
--filename=yymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\SMF\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
select
concat('NC ',(SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')),' ',(SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%y%m%d')));

--# 2
SELECT
concat('"',wca.sedol.Sedol,'"'),
concat('"',substring(concat(replace(wca.ischg.issoldname,'"','`'),'                                  '),1,35),'"'),
concat('"',substring(concat(replace(wca.ischg.issnewname,'"','`'),'                                  '),1,35),'"')
from wca.sedol
inner join wca.scmst on sedol.secid = scmst.secid
inner join wca.issur on scmst.issid = issur.issid
inner join wca.ischg on issur.issid = ischg.issid
where namechangedate>(select DATE_FORMAT((curdate()), '%Y-%m-%d'))
and sedol.sedol <>''
and ischg.eventtype<>'CLEAN'
ORDER BY sedol.Sedol;


--# 3
select 
concat('0000000','ENDOFFILE');
