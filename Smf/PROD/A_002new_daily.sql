--filepath=c:\Datafeed\smf\001new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.002
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\002new\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4;
select
security.actdate,
security.actflag,
security.securityid,
security.amtrans1,
security.amtrans2,
security.amtrans3,
security.amtrans4,
security.assentflag,
security.primarylisting,
security.cficode,
security.cregcode,
security.restrictions,
security.confirmation,
security.background,
upper(security.opol),
security.isin,
security.issuerid,
'' as strikepricecurrency,
security.closedate,
substring(security.closingdatetype,1,10) as closingdatetype,
security.longdesc,
'' as shortdesc,
security.eventcode,
security.eventdate = case when (security.eventdate < '1950/01/01') then null
             else security.eventdate end,
security.formflag,
security.statusflag,
security.sectype,
security.sedol,
'' as parvalue,
market.tidm as tidisplay,
security.unitofq,
security.unitofqcurrcode,
security.accrualflag
from security
inner join sectype on security.sectype=sectype.code
left outer join market on security.securityid = market.securityid
            and 'xlon' = market.mic
where
(security.actdate >= (select max(fromdate) from feedlog where feedfreq='smfdaily')
and security.actdate<=(select max(todate) from feedlog where feedfreq='smfdaily')
and security.confirmation='c'
and security.confirmation is not null
and (sectype.typegroup <> 'other' or
 security.sectype = 'cg' or
 security.sectype = 'cj' or
 security.sectype = 'cl'))
or
(security.headerdate >= (select max(fromdate) from feedlog where feedfreq='smfdaily')
and security.headerdate <= (select max(todate) from feedlog where feedfreq='smfdaily')
and (security.confirmation='c')
and (security.confirmation is not null)
and (sectype.typegroup <> 'other' or
 security.sectype = 'cg' or
 security.sectype = 'cj' or
 security.sectype = 'cl')
 )