--filepath=c:\Datafeed\SMF\CB\
--filenameprefix=CB
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=FW
--archive=y
--archivepath=n:\SMF\CB\
--fieldheaders=n
--filetidy=y
--fwoffsets=LS7,LS12,LS35,LS40,LS4,LS2,LS2,LS2,LS1,LS14
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4;
select
security.sedol,
security.isin,
issuer.issuername,
security.longdesc,
security.opol,
'  ' as dummy1,
security.cregcode,
'  ' as dummy2,
security.statusflag,
security.eventcode as dummy3
from dailyconf
inner join security on security.securityid = dailyconf.securityid
left outer join issuer on security.issuerid = issuer.issuerid 
order by security.sedol
