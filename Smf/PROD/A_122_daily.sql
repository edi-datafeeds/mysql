--filepath=c:\Datafeed\Smf\122\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.122
--suffix=
--fileheadertext=EDI_SMF_122_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\122\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4;
select
dailyconf.*,
smf4.security.actflag,
smf4.security.actdate,
smf4.security.securityid,
smf4.security.prevsedol,
smf4.security.issuerid,
smf4.security.sedol,
smf4.security.isin,
smf4.security.cregcode,
smf4.security.couponir,
smf4.security.parvalue,
smf4.security.pvcurrencd,
smf4.security.underlyinginstr,
smf4.security.underlyingissuer,
smf4.security.strikeprice,
smf4.security.strikepricecurrency,
smf4.security.formflag,
smf4.security.sectype,
smf4.security.amtrans1,
smf4.security.amtrans2,
smf4.security.amtrans3,
smf4.security.amtrans4,
smf4.security.unitofq,
smf4.security.unitofqcurrcode,
smf4.security.cficode,
smf4.security.closedate,
smf4.security.closingdatetype,
smf4.security.restrictions,
smf4.security.opol,
smf4.security.primarylisting,
smf4.security.newinfosource,
smf4.security.longdesc,
smf4.security.shortdesc,
smf4.security.background,
smf4.security.accrualflag,
smf4.security.assentflag,
smf4.security.eventcode,
smf4.security.eventdate,
smf4.security.statusflag,
smf4.security.confirmation,
smf4.security.caid,
smf4.security.catype,
smf4.security.credate,
smf4.security.headerdate,
' ' as fundflag,
' ' as cregstat,
' ' as dealflag,
' ' as localcode,
' ' as classflag,
' ' as sysindic,
smf4.market.tidm as tidisplay,
' ' as previssname,
smf4.security.opol as mic,
substring(smf4.security.opol,2,3) as infosource,
' ' as typeflag,
smf4.issuer.*
from dailyconf
inner join security on security.securityid = dailyconf.securityid
inner join issuer on security.issuerid = issuer.issuerid
left outer join market on security.securityid = market.securityid
            and security.opol = market.mic
            and (security.opol = 'xlon' or security.opol = 'aimx')
