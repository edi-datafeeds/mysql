--filepath=c:\Datafeed\SMF\101\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_SMF_101_yyyymmdd
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\101\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n




--# 1
use smf4;
select
dailyconf.tempacttime as actdate,
dailyconf.tempactflag as actflag,
security.securityid,
security.cregcode as countryofreg,
security.newinfosource,
security.opol,
security.isin,
security.issuerid,
issuer.issuername,
security.longdesc as securitydescription,
security.sedol,
security.statusflag,
security.eventcode,
issuer.cinccode as countryofinc,
security.sectype,
security.primarylisting
from dailyconf
inner join security on security.securityid = dailyconf.securityid
left outer join issuer on security.issuerid = issuer.issuerid 
order by security.securityid;
