--filepath=c:\Datafeed\Smf\131\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.131
--suffix=
--fileheadertext=EDI_SMF_131_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\131\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use smf4;
select
security.sedol,
security.prevsedol,
security.actflag,
security.actdate,
security.confirmation,
security.isin, 
issuer.issuername,
issuer.cinccode, 
security.longdesc,
security.cregcode,
security.statusflag,
security.opol, 
security.sectype,
security.eventdate,
eventtype.description,
security.sectype,
security.background
from dailyconf
inner join security on security.securityid = dailyconf.securityid
left outer join issuer on security.issuerid = issuer.issuerid
left outer join eventtype on security.eventcode = eventtype.code
where
(security.sectype = 'cl'
or security.sectype = 'ck')
and cinccode = 'gb'
