--filepath=c:\Datafeed\SMF\021\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.021
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\021\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
use smf4;
select
security.actdate,
security.actflag,
security.securityid,
security.cregcode,
upper(security.opol),
security.isin,
security.issuerid,
security.longdesc,
security.eventcode,
security.statusflag,
security.sedol,
security.sectype,
'' as dummytab
from security
where 
(security.actdate >= (select max(fromdate) from feedlog where feedfreq='smfdaily')
and security.actdate<=(select max(todate) from feedlog where feedfreq='smfdaily'
and (security.confirmation='c')
and (security.confirmation is not null))
or
(security.headerdate >= '2009/09/08'
and security.actdate<'2009/09/09'
and (security.confirmation='c')
and (security.confirmation is not null)))
