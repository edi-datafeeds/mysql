--filepath=c:\Datafeed\SMF\120i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\120i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 1 as seq
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Security.Sedol as SEDOL,
Security.Isin as ISIN, 
Issuer.Issuername as ISSUERNAME, 
replace(Security.Longdesc,'|',' ') as LONGDESC,
Security.Cregcode as CREGCODE,
Security.Statusflag as STATUSFLAG, 
Security.OPOL as OPOL, 
Security.Sectype as SECTYPE,
udr.dbo.udrnew.DRtype as DRTYPE,
Security.Restrictions as RESTRICTIONS
FROM Dailyconf
INNER JOIN Security ON Security.SecurityID = Dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN Udr.dbo.Udrnew ON Security.Sedol = udr.dbo.udrnew.DRSedol
/*Where Dailyconf.tempacttime<(select convert(varchar(30),getdate(),102)+' 01:00:00')*/
Where Dailyconf.tempacttime>'2010/02/26 12:00:00' and Dailyconf.tempacttime<'2010/03/01 00:00:00'
ORDER BY Security.Sedol
