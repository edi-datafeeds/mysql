--filepath=o:\Datafeed\SMF\021\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.021
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\021\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
SELECT
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Cregcode,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
smf4.security.Longdesc,
smf4.security.Eventcode,
smf4.security.Statusflag,
smf4.security.Sedol,
smf4.security.Sectype,
'' as dummytab
FROM smf4.security
Where
(smf4.security.Actdate >= (SELECT MAX(FROMDATE) FROM smf4.feedlog WHERE FEEDFREQ='SMFDAILY')
and smf4.security.Actdate<=(SELECT MAX(TODATE) FROM smf4.feedlog WHERE FEEDFREQ='SMFDAILY'
AND (smf4.security.Confirmation='C')
and (smf4.security.Confirmation is not null))
or
(smf4.security.headerdate >= '2009/09/08'
and smf4.security.Actdate<'2009/09/09'
AND (smf4.security.Confirmation='C')
and (smf4.security.Confirmation is not null)))