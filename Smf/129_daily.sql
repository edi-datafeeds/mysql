--filepath=o:\prodman\otherfeeds\Smf\129\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.129
--suffix=
--fileheadertext=EDI_SMF_129_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Smf\129\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--#
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
wca.dprcp.DRtype,
smf4.security.Restrictions
FROM smf4.security
inner JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
LEFT OUTER JOIN wca.dprcp ON wca.sedol.Secid = wca.dprcp.secid and wca.dprcp.actflag <> 'D'
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and (security.confirmation = 'C')
AND (security.confirmation is not null)
union
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.Cregcode,
smf4.security.Statusflag,
smf4.security.OPOL,
smf4.security.Sectype,
wca.dprcp.DRtype,
smf4.security.Restrictions
FROM smf4.issuer
LEFT OUTER JOIN smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
LEFT OUTER JOIN wca.dprcp ON wca.sedol.Secid = wca.dprcp.secid and wca.dprcp.actflag <> 'D'
WHERE
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and (security.confirmation = 'C')
AND (security.confirmation is not null)
ORDER BY Sedol