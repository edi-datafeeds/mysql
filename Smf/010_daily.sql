--filepath=o:\Datafeed\SMF\010\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.010
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\010\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
SELECT
smf4.issuer.Actdate,
smf4.issuer.Actflag,
smf4.issuer.IssuerID,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
'' as dummytab
FROM smf4.issuer
Where smf4.issuer.headerdate > curdate()-1
