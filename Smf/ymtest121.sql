SELECT
smf4.security.Sedol as SEDOL,
smf4.security.Isin as ISIN,
smf4.issuer.Issuername as ISSUERNAME,
replace(smf4.security.Longdesc,'|',' ') as LONGDESC,
smf4.security.Cregcode as CREGCODE,
smf4.security.Statusflag as STATUSFLAG,
smf4.security.OPOL as OPOL,
smf4.security.Sectype as SECTYPE,
wca.dprcp.DRtype as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.UnitofQcurrcode)<>3 then '' else smf4.security.UnitofQcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION,
smf4.oldopol.oldopol
from smf4.security
left outer join smf4.oldopol on smf4.security.securityid = smf4.oldopol.securityid
LEFT OUTER JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
LEFT OUTER JOIN wca.sedol ON smf4.security.Sedol = wca.sedol.Sedol
LEFT OUTER JOIN wca.dprcp ON wca.sedol.secid = wca.dprcp.secid
where
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.issuer.issuerid is not null