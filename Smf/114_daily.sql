--filepath=o:\Datafeed\SMF\114\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.114
--suffix=
--fileheadertext=EDI_SMF_114_YYYYMMDD
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\114\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
SELECT
smf4.security.Actflag,
smf4.security.Actdate,
smf4.issuer.Issuername,
smf4.security.Sedol,
smf4.security.Isin,
smf4.security.Longdesc,
smf4.security.CloseDate,
smf4.security.CouponIR,
smf4.security.PVCurrenCD,
smf4.security.Parvalue,
' ' as Classflag,
smf4.security.Sectype,
smf4.security.EventDate,
smf4.security.Eventcode,
smf4.security.Statusflag,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4
FROM smf4.security
LEFT OUTER JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
left outer join smf4.sectype on smf4.security.sectype = smf4.sectype.code
where (smf4.sectype.typegroup = 'Debt' or smf4.sectype.typegroup is null)
and closedate between curdate()+1 and curdate()+5
and closedate is not null
ORDER BY smf4.security.Sedol;