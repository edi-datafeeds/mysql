--filepath=o:\prodman\otherfeeds\smf\119\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.119
--suffix=
--fileheadertext=EDI_SMF_119_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\SMF\119i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 1 as seq
--sevent=n
--shownulls=n

--# 1
select 
code as SEDOL,
mic as MIC,
marketstatus as MARKETSTATUS,
smf4.market.statusflag as STATUSFLAG,
smf4.market.confirmation as CONFIRMATION,
smf4.market.actdate as ACTDATE,
smf4.market.actflag as ACTFLAG
from client.pfsedol
left outer join smf4.security on code =sedol
left outer join smf4.market on smf4.security.securityid = smf4.market.securityid
where
accid=998
and smf4.market.actflag<>'D';
