use smf4
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'SMFDAILY')
set @tdate = (select convert(varchar(30),getdate(),102)+' 12:00:00')


if @fdate<@tdate-0.6
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'SMFDAILY', @fdate, @tdate)
    print "SMF4 SMFDAILY Log record creation successful"
  end
else
  print "Already run today !!!"
  go
