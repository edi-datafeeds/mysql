print " Generating dailyALLSMF, please wait..."
go
USE SMF4
DECLARE @FROMDATE AS DATETIME
SET @FROMDATE=(SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
DECLARE @TODATE AS DATETIME
SET @TODATE=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
if exists (select * from sysobjects where name = 'DailyALLSMF')
	drop table DailyALLSMF
SELECT Security.SecurityID, 
tempActtime =
case
  WHEN issuer.actdate < security.actdate THEN security.actdate
else
  issuer.actdate
END, 
tempActflag =
case
  WHEN issuer.actdate < security.actdate THEN security.actflag
else
  issuer.actflag
END

into DailyALLSMF
FROM Security
INNER JOIN Issuer ON Security.issuerID = Issuer.IssuerID
WHERE Security.Actdate >= @FROMDATE and Security.Actdate<=@TODATE
UNION
SELECT Security.SecurityID,
tempActtime =
case
  WHEN issuer.actdate < security.actdate THEN security.actdate
else
  issuer.actdate
END, 
tempActflag =
case
  WHEN issuer.actdate < security.actdate THEN security.actflag
else
  issuer.actflag
END
FROM  Issuer
INNER JOIN Security ON Security.issuerID = Issuer.IssuerID
WHERE Issuer.Actdate >= @FROMDATE and Issuer.Actdate<=@TODATE
go
