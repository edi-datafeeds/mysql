--filepath=c:\Datafeed\SMF\001\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.001
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyymmdd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\001\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
select
issuer.actdate,
issuer.actflag,
issuer.issuerid,
issuer.cinccode,
issuer.ICBIndustry as induscode,
'' as dummy,
issuer.issuername
from smf4.issuer
where
smf4.issuer.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog);



