--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=CB
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=FW
--archive=y
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=y
--fwoffsets=LS7,LS12,LS35,LS40,LS4,LS2,LS2,LS2,LS1,LS14
--incremental=n
--sevent=n
--shownulls=n

--#
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.OPOL,
'  ' as dummy1,
smf4.security.Cregcode,
'  ' as dummy2,
smf4.security.Statusflag,
smf4.security.Eventcode as dummy3
FROM smf4.security
inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
union
SELECT
smf4.security.Sedol,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.OPOL,
'  ' as dummy1,
smf4.security.Cregcode,
'  ' as dummy2,
smf4.security.Statusflag,
smf4.security.Eventcode as dummy3
FROM smf4.issuer
inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
where
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and smf4.security.securityid is not null
and smf4.security.longdesc not like '%STOCK CONNECT%'
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
) ORDER BY Sedol;
