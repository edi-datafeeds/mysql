--filepath=o:\datafeed\smf\133\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate) from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_133
--fileheadertext=EDI_SMF_133_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\SMF\133i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
SELECT
smf4.security.Sedol,
smf4.security.Actflag,
smf4.security.Actdate,
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.issuer.Cinccode,
smf4.security.Cregcode,
smf4.security.CouponIR,
smf4.security.Parvalue,
smf4.security.PvCurrenCD,
smf4.security.StrikePrice,
smf4.security.StrikePriceCurrency,
smf4.security.SecType,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
smf4.security.Cficode,
smf4.security.OPOL as MIC,
smf4.security.Longdesc,
smf4.security.Statusflag,
smf4.security.Eventcode,
smf4.security.CloseDate,
smf4.security.ClosingDateType,
smf4.security.Eventdate
FROM smf4.security
INNER JOIN smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
where
smf4.security.confirmation='P'
and smf4.security.actflag<>'D';
