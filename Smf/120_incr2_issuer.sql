--filepath=o:\prodman\otherfeeds\SMF\120\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from smf4.tbl_opslog), '%Y%m%d')
--fileextension=.120
--suffix=
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\120i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select 2 as seq
--sevent=n
--shownulls=n

--#
select
smf4.security.sedol as SEDOL,
smf4.security.isin as ISIN,
smf4.issuer.issuername as ISSUERNAME,
replace(smf4.security.longdesc,'|',' ') as LONGDESC,
smf4.security.cregcode as CREGCODE,
smf4.security.statusflag as STATUSFLAG,
smf4.security.opol as OPOL,
smf4.security.sectype as SECTYPE,
wca.dprcp.DRTYPE as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.unitofqcurrcode)<>3 then '' else smf4.security.unitofqcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION,
smf4.oldopol.oldopol
from smf4.security
left outer join smf4.oldopol on smf4.security.securityid = smf4.oldopol.securityid
left outer join smf4.issuer on smf4.security.issuerid = smf4.issuer.issuerid
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
left outer join wca.dprcp on wca.sedol.secid = wca.dprcp.secid
where
smf4.security.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.issuer.issuerid is not null
and smf4.security.longdesc not like '%STOCK CONNECT%'
union
select
smf4.security.sedol as SEDOL,
smf4.security.isin as ISIN,
smf4.issuer.issuername as ISSUERNAME,
replace(smf4.security.longdesc,'|',' ') as LONGDESC,
smf4.security.cregcode as CREGCODE,
smf4.security.statusflag as STATUSFLAG,
smf4.security.opol as OPOL,
smf4.security.sectype as SECTYPE,
wca.dprcp.DRTYPE as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.unitofqcurrcode)<>3 then '' else smf4.security.unitofqcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION,
smf4.oldopol.oldopol
from smf4.issuer
left outer join smf4.security on smf4.issuer.issuerid = smf4.security.issuerid
left outer join smf4.oldopol on smf4.security.securityid = smf4.oldopol.securityid
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
left outer join wca.dprcp on wca.sedol.secid = wca.dprcp.secid
where
smf4.issuer.headerdate>(select max(feeddate) from smf4.tbl_opslog)
and smf4.security.securityid is not null
and smf4.security.longdesc not like '%STOCK CONNECT%';
