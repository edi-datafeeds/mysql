--filepath=o:\prodman\otherfeeds\smf\002\new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.002
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\002new\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--#
SELECT
smf4.security.Actdate,
smf4.security.Actflag,
smf4.security.SecurityID,
smf4.security.Amtrans1,
smf4.security.Amtrans2,
smf4.security.Amtrans3,
smf4.security.Amtrans4,
' ' as Assentflag,
smf4.security.DomesticListingIndicator as PrimaryListing,
smf4.security.Cficode,
smf4.security.Cregcode,
smf4.security.Restrictions,
smf4.security.Confirmation,
smf4.security.Background,
upper(smf4.security.OPOL),
smf4.security.Isin,
smf4.security.IssuerID,
'' as StrikePriceCurrency,
smf4.security.Closedate,
substring(smf4.security.ClosingDateType,1,10) as ClosingDateType,
smf4.security.Longdesc,
'' as Shortdesc,
smf4.security.Eventcode,
CASE WHEN (smf4.security.EventDate < '1950/01/01') THEN NULL ELSE smf4.security.EventDate END AS EvntDate,
smf4.security.Formflag,
smf4.security.Statusflag,
smf4.security.SecType,
smf4.security.Sedol,
'' as ParValue,
smf4.market.Tidm as Tidisplay,
smf4.security.Unitofq,
smf4.security.UnitofqCurrCode,
' ' as Accrualflag
FROM smf4.security
inner join smf4.sectype on smf4.security.Sectype=smf4.sectype.code
left outer join smf4.market on smf4.security.securityid = smf4.market.securityid
            and 'XLON' = smf4.market.mic
where
(smf4.security.Actdate >= (SELECT MAX(FROMDATE) FROM smf4.feedlog WHERE smf4.feedlog.feedfreq='SMFDAILY')
and smf4.security.Actdate<=(SELECT MAX(TODATE) FROM smf4.feedlog WHERE smf4.feedlog.feedfreq='SMFDAILY')
AND smf4.security.Confirmation='C'
and smf4.security.Confirmation is not null
and (smf4.sectype.typegroup <> 'Other' or
 smf4.security.sectype = 'CG' or
 smf4.security.sectype = 'CJ' or
 smf4.security.sectype = 'CL'))
or
(smf4.security.headerdate >= (SELECT MAX(FROMDATE) FROM smf4.feedlog WHERE smf4.feedlog.feedfreq='SMFDAILY')
and security.headerdate <= (SELECT MAX(TODATE) FROM smf4.feedlog WHERE smf4.feedlog.feedfreq='SMFDAILY')
AND (smf4.security.Confirmation='C')
and (smf4.security.Confirmation is not null)
and (smf4.sectype.typegroup <> 'Other' or
 smf4.security.sectype = 'CG' or
 smf4.security.sectype = 'CJ' or
 smf4.security.sectype = 'CL')
 )