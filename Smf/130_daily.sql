--filepath=o:\Datafeed\Smf\130\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.130
--suffix=
--fileheadertext=EDI_SMF_130_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\130\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
SELECT
smf4.security.actdate as Acttime,
case when smf4.security.credate = smf4.security.headerdate then upper('I') else smf4.security.Actflag end as Actflag,
smf4.issuer.IssuerID,
replace(replace(replace(smf4.issuer.Issuername,char(39),'`'),'"','`'),',',';') as Issuername,
smf4.issuer.Cinccode,
smf4.issuer.ICBSuperSector as businessid,
replace(replace(replace(smf4.issuer.Comment,char(39),'`'),'"','`'),',',';') as Comment,
smf4.issuer.IssuerStatus,
smf4.security.Sedol,
smf4.security.Isin,
replace(replace(replace(smf4.security.Longdesc,char(39),'`'),'"','`'),',',';') as Longdesc,
smf4.security.Cregcode,
smf4.security.OPOL,
smf4.security.Sectype,
smf4.security.Statusflag,
smf4.security.Eventcode,
smf4.security.Eventdate,
smf4.security.Prevsedol,
smf4.security.CloseDate,
smf4.security.ClosingDateType,
smf4.security.DomesticListingIndicator as PrimaryListing,
replace(replace(replace(smf4.security.Background,char(39),'`'),'"','`'),',',';') as Background,
' ' as Assentflag,
smf4.security.Confirmation
FROM smf4.security
Inner join smf4.issuer ON smf4.security.IssuerID = smf4.issuer.IssuerID
WHERE
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and smf4.security.sectype<>'CM'
and  smf4.security.sectype<>'CN'
union
SELECT
smf4.security.actdate as Acttime,
case when smf4.security.credate = smf4.security.headerdate then upper('I') else smf4.security.Actflag end as Actflag,
smf4.issuer.IssuerID,
replace(replace(replace(smf4.issuer.Issuername,char(39),'`'),'"','`'),',',';') as Issuername,
smf4.issuer.Cinccode,
smf4.issuer.ICBSuperSector as businessid,
replace(replace(replace(smf4.issuer.Comment,char(39),'`'),'"','`'),',',';') as Comment,
smf4.issuer.IssuerStatus,
smf4.security.Sedol,
smf4.security.Isin,
replace(replace(replace(smf4.security.Longdesc,char(39),'`'),'"','`'),',',';') as Longdesc,
smf4.security.Cregcode,
smf4.security.OPOL,
smf4.security.Sectype,
smf4.security.Statusflag,
smf4.security.Eventcode,
smf4.security.Eventdate,
smf4.security.Prevsedol,
smf4.security.CloseDate,
smf4.security.ClosingDateType,
smf4.security.DomesticListingIndicator as PrimaryListing,
replace(replace(replace(smf4.security.Background,char(39),'`'),'"','`'),',',';') as Background,
' ' as Assentflag,
smf4.security.Confirmation
FROM smf4.issuer
Inner join smf4.security ON smf4.issuer.IssuerID = smf4.security.IssuerID
left outer join smf4.issuer_prev on smf4.issuer.issuerid = smf4.issuer_prev.issuerid
WHERE
smf4.security.headerdate>(select substring(max(feeddate),1,10) from smf4.tbl_opslog)
and smf4.security.sectype<>'CM'
and  smf4.security.sectype<>'CN'
and (smf4.issuer_prev.issuername<>smf4.issuer.issuername or smf4.issuer_prev.issuername is null
or smf4.issuer_prev.issuername<>smf4.issuer.issuername
or smf4.issuer_prev.cinccode<>smf4.issuer.cinccode
or smf4.issuer_prev.ICBIndustry<>smf4.issuer.ICBIndustry
)
ORDER BY sedol;
