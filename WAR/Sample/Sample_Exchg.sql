--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Sample_Exchg
--fileheadertext=EDI_WCA_Exchg_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
inner join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd
WHERE
wca.scexh.secid in (select code from client.pfsecid where accid = 999)
union
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.warex
inner join wca.scexh on wca.warex.exersecid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
WHERE
wca.warex.secid in (select code from client.pfsecid where accid = 999)