--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Sample_Issur
--fileheadertext=EDI_WCA_Issur_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=n
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.wartm
inner join wca.scmst on wca.wartm.Secid = wca.scmst.secid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
where
wca.scmst.secid in (select code from client.pfsecid where accid = 999)
union
SELECT
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.warex
inner join wca.scmst on wca.warex.exerSecid = wca.scmst.secid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
where
wca.warex.secid in (select code from client.pfsecid where accid = 999)
