select
upper('AGCHG') as Tablename,
wca.agchg.Actflag,
wca.agchg.Acttime,
wca.agchg.Announcedate,
wca.agchg.AgchgID,
wca.agchg.SecID,
wca.agchg.CntryCD,
wca.agchg.EffectiveDate,
wca.agchg.OldAgncyID,
wca.agchg.NewAgncyID,
wca.agchg.EventType as RelatedEvent
from wca.agchg
where wca.agchg.acttime>(select max(feeddate)from wca.tbl_opslog where seq = 3);