--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_WCA_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\BBEC\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('BBEC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbecid,
wca.main.secid,
wca.main.bbeid,
wca.main.oldexchgcd,
wca.main.oldcurencd,
wca.main.effectivedate,
wca.main.newexchgcd,
wca.main.newcurencd,
wca.main.oldbbgexhid,
wca.main.newbbgexhid,
wca.main.oldbbgexhtk,
wca.main.newbbgexhtk,
wca.main.releventid,
wca.main.eventtype,
wca.main.notes
FROM wca.bbec as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
WHERE
wca.scmst.actflag<>'D'
and main.actflag<>'D'