drop table if exists wca2.bbcseq;
use wca2;
CREATE TABLE `bbcseq` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `CurenCD` char(3) NOT NULL DEFAULT '',
  `BbcID` int(11) NOT NULL,
  `Seqnum` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BbcID`),
  KEY `ix_sort` (`SecID`,`CntryCD`,`BbcID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

use wca;
insert into wca2.bbcseq
select
bbc.acttime,
bbc.secid,
bbc.cntrycd,
case when bbc.curencd<>'' then bbc.curencd else 'XXX' end,
bbc.bbcid,
1 as seqnum
from bbc
inner join scmst on bbc.secid=scmst.secid
                and 'D'<>scmst.actflag
where
bbc.actflag<>'D'
and bbc.bbgcompid<>''
and bbc.cntrycd<>''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ';

call wca2.sp_bbcseq(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Tbbcid, @Tseqnum, @cnt, @finished);
