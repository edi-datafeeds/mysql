-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- filenameextension=.667
-- filenamesuffix=
-- headerprefix=EDI_STATIC_667i_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\No_Cull_Feeds\667i\
-- fieldheaders=y
-- filetidy=y
-- incremental=select wca.tbl_opslog.seq from wca.tbl_opslog order by wca.tbl_opslog.acttime desc limit 1
-- sevent=n
-- shownulls=n
-- zerorowchk=n

-- # 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
WHERE issur.acttime >= "2019-06-19 13:56:23" AND issur.acttime <= "2019-06-19 18:07:38" 
order by acttime;


-- # 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.NoParValue,
wca.scmst.Voting,
wca.scmst.Holding,
wca.scmst.RegS144A,
wca.scmst.VotePerSec,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
WHERE scmst.acttime >= "2019-06-19 13:56:23" AND scmst.acttime <= "2019-06-19 18:07:38" 
order by acttime;


-- # 3
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
WHERE sedol.acttime >= "2019-06-19 13:56:23" AND sedol.acttime <= "2019-06-19 18:07:38" 
order by acttime;


-- # 4
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
WHERE scexh.acttime >= "2019-06-19 13:56:23" AND scexh.acttime <= "2019-06-19 18:07:38" 
order by acttime;


-- # 5
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
WHERE exchg.acttime >= "2019-06-19 13:56:23" AND exchg.acttime <= "2019-06-19 18:07:38" 
order by acttime;

