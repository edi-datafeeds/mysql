-- arc=n
-- fty=y
-- dfn=n
-- arp=n:\temp\
-- hpx=EDI_WCA_BBC_
-- dfn=l
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_BBC_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
select distinct
upper('BBC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbcid,
wca.main.secid,
wca.main.cntrycd,
wca.main.curencd,
wca.main.bbgcompid,
wca.main.bbgcomptk
from wca.bbc as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.main.acttime>(select date_sub(max(wca.tbl_opslog.acttime), interval 20 minute) from wca.tbl_opslog);

