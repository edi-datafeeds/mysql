--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_WCA_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\BBG\
--fieldheaders=n
--filetidy=y
--incremental=n
--shownulls=n
--zerorowchk=n


--# 1
select distinct
upper('BBC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbcid,
wca.main.secid,
wca.main.cntrycd,
wca.main.curencd,
wca.main.bbgcompid,
wca.main.bbgcomptk
from wca.bbc as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
WHERE
wca.scmst.actflag<>'D'
and main.actflag<>'D'
