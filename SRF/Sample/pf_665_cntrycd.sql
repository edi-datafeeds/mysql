-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- filenameextension=.665
-- filenamesuffix=_Sample
-- headerprefix=EDI_STATIC_665_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\No_Cull_Feeds\665\
-- fieldheaders=y
-- filetidy=n
-- fwoffsets=
-- incremental=
-- sevent=n
-- shownulls=n
-- zerorowchk=n

-- # 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
WHERE
wca.scmst.secid in (select secid from client.pfsecid where accid=997)
and wca.scexh.liststatus <> 'D'

-- # 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.NoParValue,
wca.scmst.Voting,
wca.scmst.Holding,
wca.scmst.RegS144A,
wca.scmst.VotePerSec,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
WHERE
wca.scmst.secid in (select secid from client.pfsecid where accid=997)
and wca.scexh.liststatus <> 'D'

-- # 3
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.scexh on wca.sedol.secid = wca.scexh.secid and sedol.cntrycd = substring(wca.scexh.exchgcd,1,2)
WHERE
wca.scmst.secid in (select secid from client.pfsecid where accid=997)
and (wca.sedol.cntrycd = 'JP' or wca.sedol.cntrycd = 'CA')
and wca.scexh.liststatus <>'D'

-- # 4
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
WHERE
wca.scmst.secid in (select secid from client.pfsecid where accid=997)
and (wca.scexh.exchgcd = 'JPJASD' or wca.scexh.exchgcd = 'JPTSE' or wca.scexh.exchgcd = 'CATSE')
and wca.scexh.liststatus <> 'D'

-- # 5
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
WHERE
wca.exchg.exchgcd = 'JPJASD' or wca.exchg.exchgcd = 'JPTSE' or wca.exchg.exchgcd = 'CATSE'





