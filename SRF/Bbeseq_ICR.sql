drop table if exists wca2.bbeseq;
use wca2;
CREATE TABLE `bbeseq` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `ExchgCD` char(6) NOT NULL DEFAULT '',
  `CurenCD` char(3) NOT NULL DEFAULT '',
  `BbeID` int(11) NOT NULL,
  `Seqnum` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BbeID`),
  KEY `ix_sort` (`SecID`,`ExchgCD`,`BbeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

use wca;
insert into wca2.bbeseq
select
bbe.acttime,
bbe.secid,
bbe.exchgcd,
case when bbe.curencd<>'' then bbe.curencd else 'XXX' end,
bbe.bbeid,
1 as seqnum
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
where
bbe.actflag<>'D'
and bbe.bbgexhid<>''
and (bbe.exchgcd<>'')
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ';

call wca2.sp_bbeseq(@Ssecid, @Tsecid, @Sexchgcd, @Texchgcd, @Tbbeid, @Tseqnum, @cnt, @finished);
