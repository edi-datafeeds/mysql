--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_WCA_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\BBE\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('BBE') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbeid,
wca.main.secid,
wca.main.exchgcd,
wca.main.curencd,
wca.main.bbgexhid,
wca.main.bbgexhtk
from wca.bbe as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
WHERE 
wca.scmst.actflag<>'D'
and main.actflag<>'D'
