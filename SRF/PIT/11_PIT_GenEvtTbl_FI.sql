delete from wca2.pitevt;

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'ISCHG', ctab.ischgid,'GeneralNotes', ctab.IschgNotes 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.actflag<>'D' and ctab.IschgNotes is not null
and length(IschgNotes)>2
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
                          
insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'SCCHG', ctab.Scchgid, 'GeneralNotes', ctab.ScchgNotes 
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.actflag<>'D' and ctab.ScchgNotes is not null
and length(ScchgNotes)>2
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'PRCHG', ctab.prchgid,'GeneralNotes', ctab.Notes 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.actflag<>'D' and ctab.Notes is not null
and ctab.effectivedate is not null
and length(Notes)>2
and ctab.oldexchgcd<>ctab.newexchgcd

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'LSTAT', ctab.lstatid, 'GeneralNotes', ctab.Reason
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.actflag<>'D' and ctab.Reason is not null
and length(Reason)>2
and ctab.effectivedate is not null
and ctab.oldlstatstatus<>ctab.lstatstatus

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'BOCHG', ctab.bochgid, 'GeneralNotes', ctab.bochgNotes
from wca.bochg as ctab
where
ctab.actflag<>'D'
and length(bochgnotes)>2
and ctab.effectivedate is not null
and ctab.newsos<>ctab.oldsos;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, 'GeneralNotes', etab.BKRPNotes 
from wca.icc as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, 'GeneralNotes', etab.CONVNotes 
from wca.icc as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, 'GeneralNotes', etab.CURRDNotes 
from wca.icc as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, 'GeneralNotes', etab.ISCHGNotes 
from wca.icc as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, 'GeneralNotes', etab.PVRDNotes 
from wca.icc as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, 'GeneralNotes', etab.REDEMNotes 
from wca.icc as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, 'GeneralNotes', etab.SECRCNotes 
from wca.icc as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, 'GeneralNotes', etab.scchgNotes 
from wca.icc as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCSWP', ctab.releventid, 'GeneralNotes', etab.SCSWPNotes 
from wca.lcc as ctab
inner join wca.scswp as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='SCSWP' and etab.actflag<>'D' and etab.SCSWPNotes is not null
and ctab.actflag<>'D'
and length(ScswpNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, 'GeneralNotes', etab.BKRPNotes 
from wca.lcc as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, 'GeneralNotes', etab.CONVNotes 
from wca.lcc as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, 'GeneralNotes', etab.CURRDNotes 
from wca.lcc as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, 'GeneralNotes', etab.ISCHGNotes 
from wca.lcc as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, 'GeneralNotes', etab.PVRDNotes 
from wca.lcc as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, 'GeneralNotes', etab.REDEMNotes 
from wca.lcc as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, 'GeneralNotes', etab.SECRCNotes 
from wca.lcc as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, 'GeneralNotes', etab.scchgNotes 
from wca.lcc as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCSWP', ctab.releventid, 'GeneralNotes', etab.SCSWPNotes 
from wca.bochg as ctab
inner join wca.scswp as etab on ctab.releventid=etab.rdid
where
ctab.eventtype='SCSWP' and etab.actflag<>'D' and etab.SCSWPNotes is not null
and ctab.actflag<>'D'
and length(ScswpNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, 'GeneralNotes', etab.BKRPNotes 
from wca.bochg as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, 'GeneralNotes', etab.CONVNotes 
from wca.bochg as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, 'GeneralNotes', etab.CURRDNotes 
from wca.bochg as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, 'GeneralNotes', etab.ISCHGNotes 
from wca.bochg as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, 'GeneralNotes', etab.PVRDNotes 
from wca.bochg as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, 'GeneralNotes', etab.REDEMNotes 
from wca.bochg as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, 'GeneralNotes', etab.SECRCNotes 
from wca.bochg as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, 'GeneralNotes', etab.scchgNotes 
from wca.bochg as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
