use wca2;
drop PROCEDURE PIT_Parse_Forward;

DELIMITER $$
CREATE DEFINER=`sa`@`%` PROCEDURE `PIT_Parse_Forward`(IN
  Sscexhid integer,
  Sstartdt datetime,
  SCntryofIncorp char(2),
  SIssuerName varchar(70),
  SSecurityDesc varchar(70),
  SPrimaryExchgCD char(6),
  SISIN char(12),
  SUSCode char(9),
  SLocalCode varchar(50),
  SListStatus char(1),
  Tscexhid integer,
  Tstartdt datetime,
  TCntryofIncorp char(2),
  TIssuerName varchar(70),
  TSecurityDesc varchar(70),
  TPrimaryExchgCD char(6),
  TISIN char(12),
  TUSCode char(9),
  TLocalCode varchar(50),
  TListStatus char(1),
  finished integer
 )
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT scexhid,startdt,CntryofIncorp,IssuerName,SecurityDesc,PrimaryExchgCD,ISIN,USCode,LocalCode,ListStatus
  FROM pitsrfsave 
  ORDER BY scexhid,startdt;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Sscexhid = 0;
  set Sstartdt = '2005-01-01';
  set SCntryofIncorp = '';
  set SIssuerName = '';
  set SSecurityDesc = '';
  set SPrimaryExchgCD = '';
  set SISIN = '';
  set SUSCode = '';
  set SLocalCode = '';
  set SListStatus = '';
  set Tscexhid = 0;
  set Tstartdt = '2005-01-01';
  set TCntryofIncorp = '';
  set TIssuerName = '';
  set TSecurityDesc = '';
  set TPrimaryExchgCD = '';
  set TISIN = '';
  set TUSCode = '';
  set TLocalCode = '';
  set TListStatus = '';
  set finished = 0;
 
  FETCH MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                      TLocalCode,TListStatus;
  
  get_pitsrf: LOOP
    IF finished = 1 THEN
      LEAVE get_pitsrf;
    END IF;
    IF Sscexhid = Tscexhid THEN 
      IF TCntryofIncorp    ='~' THEN set TCntryofIncorp    = SCntryofIncorp;    END IF;
      IF TIssuerName     ='~' THEN set TIssuerName     = SIssuerName;     END IF;
      IF TSecurityDesc   ='~' THEN set TSecurityDesc   = SSecurityDesc;   END IF;
      IF TPrimaryExchgCD ='~' THEN set TPrimaryExchgCD = SPrimaryExchgCD; END IF;
      IF TISIN           ='~' THEN set TISIN           = SISIN;           END IF;
      IF TUSCode         ='~' THEN set TUSCode         = SUSCode;         END IF;
      IF TLocalCode      ='~' THEN set TLocalCode      = SLocalCode;      END IF;
      IF TListStatus     ='~' THEN set TListStatus     = SListStatus;     END IF;

      UPDATE wca2.pitsrf 
      SET wca2.pitsrf.CntryofIncorp     = TCntryofIncorp,   
          wca2.pitsrf.IssuerName      = TIssuerName,    
          wca2.pitsrf.SecurityDesc    = TSecurityDesc,  
          wca2.pitsrf.PrimaryExchgCD  = TPrimaryExchgCD,
          wca2.pitsrf.ISIN            = TISIN,          
          wca2.pitsrf.USCode          = TUSCode,        
          wca2.pitsrf.LocalCode       = TLocalCode,     
          wca2.pitsrf.ListStatus      = TListStatus
      WHERE wca2.pitsrf.scexhid = Tscexhid and wca2.pitsrf.startdt=Tstartdt;

    END IF;

    SET Sscexhid        = Tscexhid;
    SET SCntryofIncorp    = TCntryofIncorp;
    SET SIssuerName     = TIssuerName;
    SET SSecurityDesc   = TSecurityDesc;
    SET SPrimaryExchgCD = TPrimaryExchgCD;
    SET SISIN           = TISIN;
    SET SUSCode         = TUSCode;
    SET SLocalCode      = TLocalCode;
    SET SListStatus     = TListStatus;

    FETCH NEXT FROM MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                                  TLocalCode,TListStatus;
  END LOOP get_pitsrf;
  
  CLOSE MyCursor;
  
END$$
DELIMITER ;
