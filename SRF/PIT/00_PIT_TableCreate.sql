use wca2; 
DROP TABLE pitsrf;
CREATE TABLE `pitsrf` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `CntryofIncorp` char(2) NOT NULL DEFAULT '',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ISIN` char(12) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) NOT NULL DEFAULT '',
  `ListStatus` char(1) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL,
  `ListCntryCD` char(2) NOT NULL,
  `SecID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`),
  KEY `ix_secid_startdt` (`SecID`,`StartDT`),
  KEY `ix_secid_enddt` (`SecID`,`EndDT`)
) ENGINE=InnoDB;
DROP TABLE pitsrfsave;
CREATE TABLE `pitsrfsave` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `CntryofIncorp` char(2) NOT NULL DEFAULT '',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ISIN` char(12) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) NOT NULL DEFAULT '',
  `ListStatus` char(1) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL,
  `ListCntryCD` char(2) NOT NULL,
  `SecID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`),
  KEY `ix_secid_startdt` (`SecID`,`StartDT`),
  KEY `ix_secid_enddt` (`SecID`,`EndDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE pitsrfdupe;
CREATE TABLE `pitsrfdupe` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `CntryofIncorp` char(2) NOT NULL DEFAULT '',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ISIN` char(12) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) NOT NULL DEFAULT '',
  `ListStatus` char(1) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL,
  `ListCntryCD` char(2) NOT NULL,
  `SecID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`),
  KEY `ix_secid_startdt` (`SecID`,`StartDT`),
  KEY `ix_secid_enddt` (`SecID`,`EndDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE pitfld;
CREATE TABLE `pitfld` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `FldName` char(20) NOT NULL,
  `EvtCD` char(10) NOT NULL,
  `EvtID` int(11) DEFAULT NULL,
  `FldChangeReason` varchar(10) DEFAULT '',
  PRIMARY KEY (`ScexhID`,`StartDT`,`FldName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE pitevt;
CREATE TABLE `pitevt` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `EvtCD` char(10) NOT NULL,
  `EvtID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  `NotesName` char(20) NOT NULL DEFAULT '',
  `Notes` longtext,
  PRIMARY KEY (`EvtCD`,`EvtID`,`NotesName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE pitlnk;
CREATE TABLE `pitlnk` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `PrevSecID` int(11) NOT NULL,
  `NextSecID` int(11) NOT NULL,
  `EffectiveDT` datetime NOT NULL,
  `EvtCD` varchar(20) NOT NULL,
  `PrevSeCntryCD` char(3) NOT NULL,
  `NextSeCntryCD` char(3) NOT NULL,
  `EvtID` int(11) NOT NULL,
  `EvtReason` varchar(10) NOT NULL DEFAULT '',
  `LnkType` char(1) NOT NULL,
  PRIMARY KEY (`PrevSecID`,`NextSecID`,`EffectiveDT`,`EvtCD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE pitsos;
CREATE TABLE `pitsos` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `SOSAmount` varchar(25) NOT NULL,
  `EvtCD` varchar(20) NOT NULL,
  `EvtID` int(11) NULL,
  `SOSChangeReason` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`SecID`,`StartDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
