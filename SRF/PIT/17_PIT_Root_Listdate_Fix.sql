update wca2.pitsrf as pit
inner join wca.scexh on pit.scexhid=wca.scexh.scexhid
set pit.startdt=wca.scexh.listdate,
    pit.RecTM='2019/02/20 15:00:00'
where
reccd='R'
and pit.startdt>'2005/01/01'
and listdate>pit.startdt
and listdate<=pit.enddt;

update wca2.pitsrf as pit
inner join wca.scexh on pit.scexhid=wca.scexh.scexhid
set pit.startdt=wca.scexh.listdate,
    pit.RecTM='2019/02/20 16:00:00'
where
reccd='R'
and pit.startdt>'2005/01/01'
and listdate<pit.startdt
and listdate>'2005/01/01';


update wca2.pitsrf as pit
inner join wca.scexh on pit.scexhid=wca.scexh.scexhid
set pit.startdt='2005/01/01',
    pit.RecTM='2019/02/20 17:00:00'
where
reccd='R'
and pit.startdt>'2005/01/01'
and listdate<='2005/01/01';
