-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_FLD_Sample
-- fty=n
-- hpx=edi_
-- hsx=_PIT_FLD_Sample

-- # 1
select wca2.pitfld.* from wca2.pitfld
inner join wca.scexh on wca2.pitfld.scexhid=wca.scexh.scexhid
inner join wca.scmst on wca.scexh.secid=wca.scmst.secid
where
issid=3199
or issid=6455
or issid=9850
or issid=18451
or issid=23291
or issid=29994
or issid=40149
or issid=76937
or issid=112882
or issid=152315
order by scexhid, startdt, fldname;
