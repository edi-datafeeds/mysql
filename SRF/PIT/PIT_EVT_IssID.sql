-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_EVT_Sample
-- fty=n
-- hpx=edi_
-- hsx=_PIT_EVT_Sample

-- # 1
select 
wca2.pitevt.RecCD,
wca2.pitevt.RecTM,
wca2.pitevt.EvtCD,
wca2.pitevt.EvtID,
wca2.pitevt.NotesName,
wca2.pitevt.Notes
from wca2.pitevt
where
issid=3199
or issid=6455
or issid=9850
or issid=18451
or issid=23291
or issid=29994
or issid=40149
or issid=76937
or issid=112882
or issid=152315
order by evtcd, evtid, notesname;
