-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_EVT_Sample_XSTU
-- fty=n
-- hpx=edi_
-- hsx=_PIT_EVT_Sample

-- # 1
select distinct
wca2.pitevt.RecCD,
wca2.pitevt.RecTM,
wca2.pitevt.EvtCD,
wca2.pitevt.EvtID,
wca2.pitevt.NotesName,
wca2.pitevt.Notes
from wca2.pitevt
inner join wca.scmst on wca2.pitevt.issid=wca.scmst.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd='DESSE' and wca.scmst.actflag<>'D' and wca.scexh.actflag<>'D'
order by evtcd, evtid, notesname;
