use wca2;
drop PROCEDURE PIT_Dedupe;

DELIMITER $$
CREATE DEFINER=`sa`@`%` PROCEDURE `PIT_Dedupe`(IN
  Sscexhid integer,
  Sstartdt datetime,
  SCntryofIncorp char(2),
  SIssuerName varchar(70),
  SSecurityDesc varchar(70),
  SPrimaryExchgCD char(6),
  SISIN char(12),
  SUSCode char(9),
  SLocalCode varchar(50),
  SListStatus char(1),
  Tscexhid integer,
  Tstartdt datetime,
  TCntryofIncorp char(2),
  TIssuerName varchar(70),
  TSecurityDesc varchar(70),
  TPrimaryExchgCD char(6),
  TISIN char(12),
  TUSCode char(9),
  TLocalCode varchar(50),
  TListStatus char(1),
  finished integer
 )
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT scexhid,startdt,CntryofIncorp,IssuerName,SecurityDesc,PrimaryExchgCD,ISIN,USCode,LocalCode,ListStatus
  FROM pitsrf 
  ORDER BY scexhid,startdt;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Sscexhid = 0;
  set Sstartdt = '2005-01-01';
  set SCntryofIncorp = '';
  set SIssuerName = '';
  set SSecurityDesc = '';
  set SPrimaryExchgCD = '';
  set SISIN = '';
  set SUSCode = '';
  set SLocalCode = '';
  set SListStatus = '';
  set Tscexhid = 0;
  set Tstartdt = '2005-01-01';
  set TCntryofIncorp = '';
  set TIssuerName = '';
  set TSecurityDesc = '';
  set TPrimaryExchgCD = '';
  set TISIN = '';
  set TUSCode = '';
  set TLocalCode = '';
  set TListStatus = '';
  set finished = 0;
 
  FETCH MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                      TLocalCode,TListStatus;
  
  get_pitsrf: LOOP
    IF finished = 1 THEN
      LEAVE get_pitsrf;
    END IF;
    IF Sscexhid = Tscexhid and 
         TCntryofIncorp  = SCntryofIncorp AND
         TIssuerName     = SIssuerName     AND
         TSecurityDesc   = SSecurityDesc   AND
         TPrimaryExchgCD = SPrimaryExchgCD AND
         TISIN           = SISIN           AND
         TUSCode         = SUSCode         AND
         TLocalCode      = SLocalCode      AND
         TListStatus     = SListStatus     THEN
      insert into wca2.pitsrfdupe select * from wca2.pitsrf
      WHERE wca2.pitsrf.scexhid = Tscexhid and wca2.pitsrf.startdt=Tstartdt;
      delete from wca2.pitsrf 
      WHERE wca2.pitsrf.scexhid = Tscexhid and wca2.pitsrf.startdt=Tstartdt;
    
    ELSE

      SET Sscexhid        = Tscexhid;
      SET SCntryofIncorp  = TCntryofIncorp;
      SET SIssuerName     = TIssuerName;
      SET SSecurityDesc   = TSecurityDesc;
      SET SPrimaryExchgCD = TPrimaryExchgCD;
      SET SISIN           = TISIN;
      SET SUSCode         = TUSCode;
      SET SLocalCode      = TLocalCode;
      SET SListStatus     = TListStatus;
    END IF;

    FETCH NEXT FROM MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                                  TLocalCode,TListStatus;
  END LOOP get_pitsrf;
  
  CLOSE MyCursor;
  
END$$
DELIMITER ;
