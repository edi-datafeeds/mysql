use wca2;
drop PROCEDURE PIT_BBE_Rootflag;

DELIMITER $$
CREATE DEFINER=`sa`@`%` PROCEDURE `PIT_BBE_Rootflag`(IN Sbbeid integer, IN Tbbeid integer, Sstartdt datetime,
                  IN Tstartdt datetime, IN finished integer)
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT bbeid,startdt FROM pitbbe
  ORDER BY bbeid, startdt desc;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Sbbeid = 0;
  set Tbbeid = 0;
  set Sstartdt = '2000-01-01';
  set Tstartdt = '2000-01-01';
  set finished = 0;
 
  FETCH MyCursor INTO Tbbeid, Tstartdt;
  
  get_pitBBE: LOOP
    IF finished = 1 THEN
      LEAVE get_pitBBE;
    END IF;
    IF Sbbeid = Tbbeid THEN 
      SET Sstartdt = Tstartdt;
    ELSE 
      UPDATE wca2.pitbbe SET wca2.pitbbe.reccd = 'R'
      WHERE wca2.pitbbe.bbeid = Sbbeid and wca2.pitbbe.startdt=Sstartdt;
      SET Sbbeid = Tbbeid;
      SET Sstartdt = Tstartdt;
    END IF;
    FETCH NEXT FROM MyCursor INTO Tbbeid, Tstartdt;
  END LOOP get_pitBBE;
  
  CLOSE MyCursor;
  
END$$

DELIMITER ;
