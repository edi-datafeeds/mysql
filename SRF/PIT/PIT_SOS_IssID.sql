-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_SOS_Sample
-- fty=n
-- hpx=edi_
-- hsx=_PIT_SOS_Sample

-- # 1
select distinct wca2.pitsos.* from wca2.pitsos
inner join wca.scmst on wca2.pitsos.secid=wca.scmst.secid
where
issid=3199
or issid=6455
or issid=9850
or issid=18451
or issid=23291
or issid=29994
or issid=40149
or issid=76937
or issid=112882
or issid=152315
order by secid, startdt;
