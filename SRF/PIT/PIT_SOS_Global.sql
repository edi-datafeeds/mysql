-- arp=n:\temp\
-- hpx=select 'edi_'
-- fpx=select 'edi_'
-- dfn=l
-- fty=n
-- ddt=yyyy-mm-dd
-- eof=EDI_ENDOFFILE

-- # 1
select distinct wca2.pitsos.* from wca2.pitsos
inner join wca.scexh on wca2.pitsos.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
(cntrycd=@ctycd or @ctycd is null)
order by secid, startdt;
