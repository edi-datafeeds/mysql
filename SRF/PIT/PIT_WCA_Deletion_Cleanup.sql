select badevt.lstatid from wca.lstat as badevt
inner join wca.lstat as goodevt on badevt.secid=goodevt.secid 
                               and badevt.exchgcd=goodevt.exchgcd
                               and badevt.lstatid<goodevt.lstatid
                               and badevt.effectivedate=goodevt.effectivedate
where 
badevt.actflag<>'D' and goodevt.actflag<>'D'
and badevt.lstatid=(
select lstatid from wca.lstat as sublstat
where
sublstat.secid=goodevt.secid and sublstat.exchgcd=goodevt.exchgcd and sublstat.lstatid<goodevt.lstatid and sublstat.effectivedate=goodevt.effectivedate
order by sublstat.lstatid limit 1)
order by badevt.lstatid;

select badevt.ischgid from wca.ischg as badevt
inner join wca.ischg as goodevt on badevt.issid=goodevt.issid 
                               and badevt.ischgid<goodevt.ischgid
                               and badevt.namechangedate=goodevt.namechangedate
where 
badevt.actflag<>'D' and goodevt.actflag<>'D'
and badevt.ischgid=(
select ischgid from wca.ischg as sublstat
where
sublstat.issid=goodevt.issid and sublstat.ischgid<goodevt.ischgid and sublstat.namechangedate=goodevt.namechangedate
order by sublstat.ischgid limit 1)
order by badevt.ischgid;

select badevt.iccid from wca.icc as badevt
inner join wca.icc as goodevt on badevt.secid=goodevt.secid 
                               and badevt.iccid<goodevt.iccid
                               and badevt.effectivedate=goodevt.effectivedate
where 
badevt.actflag<>'D' and goodevt.actflag<>'D'
and badevt.iccid=(
select iccid from wca.icc as sublstat
where
sublstat.secid=goodevt.secid and sublstat.iccid<goodevt.iccid and sublstat.effectivedate=goodevt.effectivedate
order by sublstat.iccid limit 1)
order by badevt.iccid;

select badevt.lccid from wca.lcc as badevt
inner join wca.lcc as goodevt on badevt.secid=goodevt.secid 
                               and badevt.exchgcd=goodevt.exchgcd
                               and badevt.lccid<goodevt.lccid
                               and badevt.effectivedate=goodevt.effectivedate
where 
badevt.actflag<>'D' and goodevt.actflag<>'D'
and badevt.lccid=(
select lccid from wca.lcc as sublstat
where
sublstat.secid=goodevt.secid and sublstat.exchgcd=goodevt.exchgcd and sublstat.lccid<goodevt.lccid and sublstat.effectivedate=goodevt.effectivedate
order by sublstat.lccid limit 1)
order by badevt.lccid;
