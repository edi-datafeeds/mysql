-- arp=n:\temp\
-- hpx=select 'edi_'
-- fpx=select 'edi_'
-- dfn=l
-- fty=n
-- ddt=yyyy-mm-dd
-- eof=EDI_ENDOFFILE

-- # 1
select distinct 
wca2.pitevt.RecCD,
wca2.pitevt.RecTM,
wca2.pitevt.EvtCD,
wca2.pitevt.EvtID,
wca2.pitevt.NotesName,
wca2.pitevt.Notes
from wca2.pitevt
inner join wca.scmst on wca2.pitevt.issid=wca.scmst.issid
inner join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
wca.exchg.cntrycd=@ctycd
order by EvtCD, EvtID;
