use wca2;
drop PROCEDURE PIT_SOS_EndDT;

DELIMITER $$
CREATE DEFINER=`sa`@`%` PROCEDURE `PIT_SOS_EndDT`(IN Ssecid integer, IN Tsecid integer, Sstartdt datetime,
                  IN Tstartdt datetime, IN finished integer)
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT secid,startdt FROM pitsos 
  ORDER BY secid, startdt desc;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Ssecid = 0;
  set Tsecid = 0;
  set Sstartdt = '2000-01-01';
  set Tstartdt = '2000-01-01';
  set finished = 0;
 
  FETCH MyCursor INTO Tsecid, Tstartdt;
  
  get_pitsos: LOOP
    IF finished = 1 THEN
      LEAVE get_pitsos;
    END IF;
    IF Ssecid = Tsecid THEN 
      UPDATE wca2.pitsos SET wca2.pitsos.enddt = date_sub(Sstartdt, interval 1 day)
      WHERE wca2.pitsos.secid = Tsecid and wca2.pitsos.startdt=Tstartdt;
      SET Sstartdt = Tstartdt;
    ELSE 
      SET Ssecid = Tsecid;
      SET Sstartdt = Tstartdt;
    END IF;
    FETCH NEXT FROM MyCursor INTO Tsecid, Tstartdt;
  END LOOP get_pitsos;
  
  CLOSE MyCursor;
  
END$$

DELIMITER ;

