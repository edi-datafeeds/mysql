delete from wca2.pitsos;

insert ignore into wca2.pitsos
select 'I',
ctab.acttime, 
ctab.SecID, 
ctab.effectivedate,
'2099-01-01',
ctab.newsos,
case when ctab.releventid is not null and ctab.eventtype <>'' then ctab.eventtype
     when (ctab.releventid is null or ctab.eventtype ='') and length(ShochNotes)>2 then 'SHOCH'
     else '' end,
case when ctab.releventid is not null and ctab.eventtype <>'' then ctab.releventid
     when (ctab.releventid is null or ctab.eventtype ='') and length(ShochNotes)>2 then ctab.shochid
     else null end,
case when ctab.eventtype <>'' then ctab.eventtype
     else 'SHOCH' end
from wca.shoch as ctab
inner join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.effectivedate<now()
and wca.scmst.statusflag<>'I'
and wca.scmst.actflag<>'D'
and ctab.actflag<>'D'
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
insert ignore into wca2.pitsos
select 'I',
ctab.acttime, 
ctab.SecID, 
ctab.sharesoutstandingdate,
'2099-01-01',
ctab.sharesoutstanding,
'',
null,
'SCMST'
from wca.scmst as ctab
left outer join wca.sectygrp on ctab.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.sharesoutstandingdate is not null
and ctab.statusflag<>'I'
and ctab.sharesoutstanding<>''
and ctab.actflag<>'D'
and wca.sectygrp.secgrpid is not null;