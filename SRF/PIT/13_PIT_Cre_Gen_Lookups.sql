use wca2;
DROP TABLE pitcty;
CREATE TABLE `pitcty` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(2) NOT NULL,
  `Lookup` char(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitcty
select distinct 
case when actflag='D' then 'H' else actflag end,
acttime, listcntrycd, country
from wca2.pitsrf as pit
inner join wca.cntry as lup on lup.cntrycd=pit.listcntrycd;
insert ignore into wca2.pitcty
select distinct
case when actflag='D' then 'H' else actflag end,
acttime, CntryofIncorp, country
from wca2.pitsrf as pit
inner join wca.cntry as lup on lup.cntrycd=pit.CntryofIncorp;

DROP TABLE pitevn;
CREATE TABLE `pitevn` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitevn
select distinct 
case when actflag='D' then 'H' else actflag end,
acttime, lup.eventtype, eventname
from wca2.pitevt as pit
inner join wca.event as lup on lup.eventtype=pit.evtcd;

insert ignore into wca2.pitevn
select distinct 
case when actflag='D' then 'H' else actflag end,
acttime, lup.eventtype, eventname
from wca2.pitfld as pit
inner join wca.event as lup on lup.eventtype=pit.fldchangereason;

DROP TABLE pitexn;
CREATE TABLE `pitexn` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(6) NOT NULL,
  `Lookup` char(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitexn
select distinct 
case when actflag='D' then 'H' else actflag end,
acttime, lup.exchgcd, exchgname
from wca2.pitsrf as pit
inner join wca.exchg as lup on lup.exchgcd=pit.exchgcd;

DROP TABLE pitlst;
CREATE TABLE `pitlst` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(2) NOT NULL,
  `Lookup` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitlst select 'I', '2018-01-01 15:00:00', 'L', 'Listed';
insert ignore into wca2.pitlst select 'I', '2018-01-01 15:00:00', 'D', 'Delisted';
insert ignore into wca2.pitlst select 'I', '2018-01-01 15:00:00', 'S', 'Suspended';

DROP TABLE pitrec;
CREATE TABLE `pitrec` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(2) NOT NULL,
  `Lookup` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitrec select 'I', '2018-01-01 15:00:00', 'R', 'Root';
insert ignore into wca2.pitrec select 'I', '2018-01-01 15:00:00', 'I', 'Inserted';
insert ignore into wca2.pitrec select 'I', '2018-01-01 15:00:00', 'U', 'Updated';
insert ignore into wca2.pitrec select 'I', '2018-01-01 15:00:00', 'D', 'Deleted';
insert ignore into wca2.pitrec select 'I', '2018-01-01 15:00:00', 'H', 'Historical';

DROP TABLE pitmic;
CREATE TABLE `pitmic` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(6) NOT NULL,
  `Lookup` char(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitmic
select distinct
case when actflag='D' then 'H' else actflag end,
wca.exchg.Acttime,
wca.exchg.ExchgCD as Code,
wca.exchg.MIC as Lookup
from wca.exchg
where
substring(wca.exchg.exchgcd,3,3)<>'BND';

use wca2;
DROP TABLE pitsty;
CREATE TABLE `pitsty` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(3) NOT NULL,
  `Lookup` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitsty
select distinct
wca.secty.Actflag,
wca.secty.Acttime,
wca.secty.SectyCD,
wca.secty.SecurityDescriptor
from wca.secty;

use wca2;
DROP TABLE pitstr;
CREATE TABLE `pitstr` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitstr
select distinct
wca.lookup.Actflag,
wca.lookup.Acttime,
wca.lookup.code,
wca.lookup.lookup
from wca.lookup where typegroup='STRUCTCD';

use wca2;
DROP TABLE pitvot;
CREATE TABLE `pitvot` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into wca2.pitvot
select distinct
wca.lookup.Actflag,
wca.lookup.Acttime,
wca.lookup.code,
wca.lookup.lookup
from wca.lookup where typegroup='VOTING';
