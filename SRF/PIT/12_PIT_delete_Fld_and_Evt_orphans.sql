ALTER TABLE `wca2`.`pitfld` 
ADD INDEX `ix_evt` (`evtcd` ASC, `EvtID` ASC);

delete wca2.pitfld from wca2.pitfld
left outer join wca2.pitsrf on wca2.pitfld.scexhid=wca2.pitsrf.scexhid and wca2.pitfld.startdt=wca2.pitsrf.startdt
where
wca2.pitsrf.scexhid is null;

delete wca2.pitevt from wca2.pitevt
left outer join wca2.pitfld on wca2.pitevt.evtcd=wca2.pitfld.evtcd and wca2.pitevt.evtid=wca2.pitfld.evtid
where
wca2.pitfld.evtid is null;
