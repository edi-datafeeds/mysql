delete from wca2.pitfld;

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.inchgdate, 'CntryofIncorp', '', null, ctab.eventtype 
from wca.inchg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and (ctab.inchgdate>=wca.scexh.listdate or ctab.inchgdate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.namechangedate, 'Issuername', 'ISCHG', ctab.ischgid, ctab.eventtype 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and (ctab.namechangedate>=wca.scexh.listdate or ctab.namechangedate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
 'Isin', ctab.eventtype
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.oldisin<>ctab.newisin)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'UScode',
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.dateofchange, 'SecurityDesc', 'SCCHG', ctab.scchgid, ctab.eventtype 
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and (ctab.dateofchange>=wca.scexh.listdate or ctab.dateofchange>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'PrimaryExchgCD', 'PRCHG', ctab.prchgid, ctab.eventtype 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'Localcode',
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
from wca.lcc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'ListStatus', 'LSTAT', ctab.lstatid, ctab.eventtype 
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
