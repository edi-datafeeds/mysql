delete from wca2.pitevt;

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'ISCHG', ctab.ischgid, wca.scmst.issid, 'GeneralNotes', ctab.IschgNotes 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.actflag<>'D' and ctab.IschgNotes is not null
and length(IschgNotes)>2
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          ) or wca.sectygrp.secgrpid is not null);
                          
insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'SCCHG', ctab.Scchgid, wca.scmst.issid, 'GeneralNotes', ctab.ScchgNotes 
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.actflag<>'D' and ctab.ScchgNotes is not null
and length(ScchgNotes)>2
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          ) or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'PRCHG', ctab.prchgid, wca.scmst.issid, 'GeneralNotes', ctab.Notes 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.actflag<>'D' and ctab.Notes is not null
and ctab.effectivedate is not null
and length(Notes)>2
and ctab.oldexchgcd<>ctab.newexchgcd
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          ) or wca.sectygrp.secgrpid is not null);


insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'LSTAT', ctab.lstatid, wca.scmst.issid, 'GeneralNotes', ctab.Reason
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.actflag<>'D' and ctab.Reason is not null
and length(Reason)>2
and ctab.effectivedate is not null
and ctab.oldlstatstatus<>ctab.lstatstatus
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          ) or wca.sectygrp.secgrpid is not null);


insert ignore into wca2.pitevt
select 'I', ctab.acttime, 'SHOCH', ctab.shochid, wca.scmst.issid, 'GeneralNotes', ctab.ShochNotes
from wca.shoch as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.actflag<>'D'
and length(shochnotes)>2
and ctab.effectivedate is not null
and ctab.newsos<>ctab.oldsos;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ARR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ArrNotes 
from wca.icc as ctab
inner join wca.arr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ARR' and etab.actflag<>'D' and etab.ArrNotes is not null
and ctab.actflag<>'D'
and length(ArrNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONSD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ConsdNotes 
from wca.icc as ctab
inner join wca.consd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CONSD' and etab.actflag<>'D' and etab.ConsdNotes is not null
and ctab.actflag<>'D'
and length(ConsdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SDNotes 
from wca.icc as ctab
inner join wca.sd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SD' and etab.actflag<>'D' and etab.SDNotes is not null
and ctab.actflag<>'D'
and length(SDNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DMRGR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DMRGRNotes 
from wca.icc as ctab
inner join wca.dmrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='DMRGR' and etab.actflag<>'D' and etab.DMRGRNotes is not null
and ctab.actflag<>'D'
and length(DmrgrNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ENT', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ENTNotes 
from wca.icc as ctab
inner join wca.ent as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ENT' and etab.actflag<>'D' and etab.EntNotes is not null
and ctab.actflag<>'D'
and length(EntNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'RTS', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.RtsNotes 
from wca.icc as ctab
inner join wca.rts as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='RTS' and etab.actflag<>'D' and etab.RTSNotes is not null
and ctab.actflag<>'D'
and length(RtsNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCSWP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SCSWPNotes 
from wca.icc as ctab
inner join wca.scswp as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SCSWP' and etab.actflag<>'D' and etab.SCSWPNotes is not null
and ctab.actflag<>'D'
and length(ScswpNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'MRGRTerms', etab.MRGRTerms 
from wca.icc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.MRGRTerms is not null
and ctab.actflag<>'D'
and length(MrgrTerms)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'ApprovalStatus', etab.ApprovalStatus 
from wca.icc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.ApprovalStatus is not null
and ctab.actflag<>'D'
and length(ApprovalStatus)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'Companies', etab.Companies 
from wca.icc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.Companies is not null
and ctab.actflag<>'D'
and length(Companies)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BB', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BBNotes 
from wca.icc as ctab
inner join wca.bb as etab on ctab.releventid=etab.bbid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='BB' and etab.actflag<>'D' and etab.BBNotes is not null
and ctab.actflag<>'D'
and length(BbNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BKRPNotes 
from wca.icc as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BON', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BonNotes 
from wca.icc as ctab
inner join wca.bon as etab on ctab.releventid=etab.Bonid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='Bon' and etab.actflag<>'D' and etab.BonNotes is not null
and ctab.actflag<>'D'
and length(BonNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CALL', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CALLNotes 
from wca.icc as ctab
inner join wca.call_my as etab on ctab.releventid=etab.CALLid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CALL' and etab.actflag<>'D' and etab.CALLNotes is not null
and ctab.actflag<>'D'
and length(CallNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CAPRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CAPRDNotes 
from wca.icc as ctab
inner join wca.caprd as etab on ctab.releventid=etab.CAPRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CAPRD' and etab.actflag<>'D' and etab.CAPRDNotes is not null
and ctab.actflag<>'D'
and length(CaprdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CONVNotes 
from wca.icc as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CTX', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CTXNotes 
from wca.icc as ctab
inner join wca.ctx as etab on ctab.releventid=etab.CTXid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CTX' and etab.actflag<>'D' and etab.CTXNotes is not null
and ctab.actflag<>'D'
and length(CtxNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CURRDNotes 
from wca.icc as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DRCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DRCHGNotes 
from wca.icc as ctab
inner join wca.drchg as etab on ctab.releventid=etab.DRCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='DRCHG' and etab.actflag<>'D' and etab.DRCHGNotes is not null
and ctab.actflag<>'D'
and length(DrchgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ISCHGNotes 
from wca.icc as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.PVRDNotes 
from wca.icc as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.REDEMNotes 
from wca.icc as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SECRCNotes 
from wca.icc as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.scchgNotes 
from wca.icc as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'TKOVR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.TKOVRNotes 
from wca.icc as ctab
inner join wca.tkovr as etab on ctab.releventid=etab.TKOVRid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='TKOVR' and etab.actflag<>'D' and etab.TKOVRNotes is not null
and ctab.actflag<>'D'
and length(TkovrNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'WTCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.WTCHGNotes 
from wca.icc as ctab
inner join wca.wtchg as etab on ctab.releventid=etab.Wtchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='WTCHG' and etab.actflag<>'D' and etab.WtchgNotes is not null
and ctab.actflag<>'D'
and length(WtchgNotes)>2
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ARR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ArrNotes 
from wca.lcc as ctab
inner join wca.arr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ARR' and etab.actflag<>'D' and etab.ArrNotes is not null
and ctab.actflag<>'D'
and length(ArrNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONSD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ConsdNotes 
from wca.lcc as ctab
inner join wca.consd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CONSD' and etab.actflag<>'D' and etab.ConsdNotes is not null
and ctab.actflag<>'D'
and length(ConsdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SDNotes 
from wca.lcc as ctab
inner join wca.sd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SD' and etab.actflag<>'D' and etab.SDNotes is not null
and ctab.actflag<>'D'
and length(SDNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DMRGR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DMRGRNotes 
from wca.lcc as ctab
inner join wca.dmrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='DMRGR' and etab.actflag<>'D' and etab.DMRGRNotes is not null
and ctab.actflag<>'D'
and length(DmrgrNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ENT', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ENTNotes 
from wca.lcc as ctab
inner join wca.ent as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ENT' and etab.actflag<>'D' and etab.EntNotes is not null
and ctab.actflag<>'D'
and length(EntNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'RTS', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.RtsNotes 
from wca.lcc as ctab
inner join wca.rts as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='RTS' and etab.actflag<>'D' and etab.RTSNotes is not null
and ctab.actflag<>'D'
and length(RtsNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCSWP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SCSWPNotes 
from wca.lcc as ctab
inner join wca.scswp as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SCSWP' and etab.actflag<>'D' and etab.SCSWPNotes is not null
and ctab.actflag<>'D'
and length(ScswpNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'MRGRTerms', etab.MRGRTerms 
from wca.lcc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.MRGRTerms is not null
and ctab.actflag<>'D'
and length(MrgrTerms)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'ApprovalStatus', etab.ApprovalStatus 
from wca.lcc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.ApprovalStatus is not null
and ctab.actflag<>'D'
and length(ApprovalStatus)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'Companies', etab.Companies 
from wca.lcc as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.Companies is not null
and ctab.actflag<>'D'
and length(Companies)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BB', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BBNotes 
from wca.lcc as ctab
inner join wca.bb as etab on ctab.releventid=etab.bbid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='BB' and etab.actflag<>'D' and etab.BBNotes is not null
and ctab.actflag<>'D'
and length(BbNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BKRPNotes 
from wca.lcc as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BON', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BonNotes 
from wca.lcc as ctab
inner join wca.bon as etab on ctab.releventid=etab.Bonid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='Bon' and etab.actflag<>'D' and etab.BonNotes is not null
and ctab.actflag<>'D'
and length(BonNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CALL', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CALLNotes 
from wca.lcc as ctab
inner join wca.call_my as etab on ctab.releventid=etab.CALLid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CALL' and etab.actflag<>'D' and etab.CALLNotes is not null
and ctab.actflag<>'D'
and length(CallNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CAPRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CAPRDNotes 
from wca.lcc as ctab
inner join wca.caprd as etab on ctab.releventid=etab.CAPRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CAPRD' and etab.actflag<>'D' and etab.CAPRDNotes is not null
and ctab.actflag<>'D'
and length(CaprdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CONVNotes 
from wca.lcc as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CTX', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CTXNotes 
from wca.lcc as ctab
inner join wca.ctx as etab on ctab.releventid=etab.CTXid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CTX' and etab.actflag<>'D' and etab.CTXNotes is not null
and ctab.actflag<>'D'
and length(CtxNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CURRDNotes 
from wca.lcc as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DRCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DRCHGNotes 
from wca.lcc as ctab
inner join wca.drchg as etab on ctab.releventid=etab.DRCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='DRCHG' and etab.actflag<>'D' and etab.DRCHGNotes is not null
and ctab.actflag<>'D'
and length(DrchgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ISCHGNotes 
from wca.lcc as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.PVRDNotes 
from wca.lcc as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.REDEMNotes 
from wca.lcc as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SECRCNotes 
from wca.lcc as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.scchgNotes 
from wca.lcc as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'TKOVR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.TKOVRNotes 
from wca.lcc as ctab
inner join wca.tkovr as etab on ctab.releventid=etab.TKOVRid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='TKOVR' and etab.actflag<>'D' and etab.TKOVRNotes is not null
and ctab.actflag<>'D'
and length(TkovrNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
     

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'WTCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.WTCHGNotes 
from wca.lcc as ctab
inner join wca.wtchg as etab on ctab.releventid=etab.Wtchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
ctab.eventtype='WTCHG' and etab.actflag<>'D' and etab.WtchgNotes is not null
and ctab.actflag<>'D'
and length(WtchgNotes)>2
and (ctab.oldlocalcode<>ctab.newlocalcode)
and ctab.effectivedate is not null
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ARR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ArrNotes 
from wca.shoch as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.arr as etab on ctab.releventid=etab.rdid
where
ctab.eventtype='ARR' and etab.actflag<>'D' and etab.ArrNotes is not null
and ctab.actflag<>'D'
and length(ArrNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONSD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ConsdNotes 
from wca.shoch as ctab
inner join wca.consd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CONSD' and etab.actflag<>'D' and etab.ConsdNotes is not null
and ctab.actflag<>'D'
and length(ConsdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SDNotes 
from wca.shoch as ctab
inner join wca.sd as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='SD' and etab.actflag<>'D' and etab.SDNotes is not null
and ctab.actflag<>'D'
and length(SDNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DMRGR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DMRGRNotes 
from wca.shoch as ctab
inner join wca.dmrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='DMRGR' and etab.actflag<>'D' and etab.DMRGRNotes is not null
and ctab.actflag<>'D'
and length(DmrgrNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ENT', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ENTNotes 
from wca.shoch as ctab
inner join wca.ent as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='ENT' and etab.actflag<>'D' and etab.EntNotes is not null
and ctab.actflag<>'D'
and length(EntNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'RTS', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.RtsNotes 
from wca.shoch as ctab
inner join wca.rts as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='RTS' and etab.actflag<>'D' and etab.RTSNotes is not null
and ctab.actflag<>'D'
and length(RtsNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCSWP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SCSWPNotes 
from wca.shoch as ctab
inner join wca.scswp as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='SCSWP' and etab.actflag<>'D' and etab.SCSWPNotes is not null
and ctab.actflag<>'D'
and length(ScswpNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'MRGRTerms', etab.MRGRTerms 
from wca.shoch as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.MRGRTerms is not null
and ctab.actflag<>'D'
and length(MrgrTerms)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'ApprovalStatus', etab.ApprovalStatus 
from wca.shoch as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.ApprovalStatus is not null
and ctab.actflag<>'D'
and length(ApprovalStatus)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'MRGR', ctab.releventid, wca.scmst.issid, 'Companies', etab.Companies 
from wca.shoch as ctab
inner join wca.mrgr as etab on ctab.releventid=etab.rdid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='MRGR' and etab.actflag<>'D' and etab.Companies is not null
and ctab.actflag<>'D'
and length(Companies)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BB', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BBNotes 
from wca.shoch as ctab
inner join wca.bb as etab on ctab.releventid=etab.bbid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='BB' and etab.actflag<>'D' and etab.BBNotes is not null
and ctab.actflag<>'D'
and length(BbNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BKRP', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BKRPNotes 
from wca.shoch as ctab
inner join wca.bkrp as etab on ctab.releventid=etab.BKRPid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='BKRP' and etab.actflag<>'D' and etab.BKRPNotes is not null
and ctab.actflag<>'D'
and length(BkrpNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'BON', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.BonNotes 
from wca.shoch as ctab
inner join wca.bon as etab on ctab.releventid=etab.Bonid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='BON' and etab.actflag<>'D' and etab.BonNotes is not null
and ctab.actflag<>'D'
and length(BonNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CALL', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CALLNotes 
from wca.shoch as ctab
inner join wca.call_my as etab on ctab.releventid=etab.CALLid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CALL' and etab.actflag<>'D' and etab.CALLNotes is not null
and ctab.actflag<>'D'
and length(CallNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CAPRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CAPRDNotes 
from wca.shoch as ctab
inner join wca.caprd as etab on ctab.releventid=etab.CAPRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CAPRD' and etab.actflag<>'D' and etab.CAPRDNotes is not null
and ctab.actflag<>'D'
and length(CaprdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CONV', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CONVNotes 
from wca.shoch as ctab
inner join wca.conv as etab on ctab.releventid=etab.CONVid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CONV' and etab.actflag<>'D' and etab.CONVNotes is not null
and ctab.actflag<>'D'
and length(ConvNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CTX', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CTXNotes 
from wca.shoch as ctab
inner join wca.ctx as etab on ctab.releventid=etab.CTXid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CTX' and etab.actflag<>'D' and etab.CTXNotes is not null
and ctab.actflag<>'D'
and length(CtxNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'CURRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.CURRDNotes 
from wca.shoch as ctab
inner join wca.currd as etab on ctab.releventid=etab.CURRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='CURRD' and etab.actflag<>'D' and etab.CURRDNotes is not null
and ctab.actflag<>'D'
and length(CurrdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'DRCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.DRCHGNotes 
from wca.shoch as ctab
inner join wca.drchg as etab on ctab.releventid=etab.DRCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='DRCHG' and etab.actflag<>'D' and etab.DRCHGNotes is not null
and ctab.actflag<>'D'
and length(DrchgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'ISCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.ISCHGNotes 
from wca.shoch as ctab
inner join wca.ischg as etab on ctab.releventid=etab.ISCHGid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='ISCHG' and etab.actflag<>'D' and etab.ISCHGNotes is not null
and ctab.actflag<>'D'
and length(IschgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'PVRD', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.PVRDNotes 
from wca.shoch as ctab
inner join wca.pvrd as etab on ctab.releventid=etab.PVRDid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='PVRD' and etab.actflag<>'D' and etab.PVRDNotes is not null
and ctab.actflag<>'D'
and length(PvrdNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'REDEM', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.REDEMNotes 
from wca.shoch as ctab
inner join wca.redem as etab on ctab.releventid=etab.REDEMid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='REDEM' and etab.actflag<>'D' and etab.REDEMNotes is not null
and ctab.actflag<>'D'
and length(RedemNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SECRC', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.SECRCNotes 
from wca.shoch as ctab
inner join wca.secrc as etab on ctab.releventid=etab.SECRCid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='SECRC' and etab.actflag<>'D' and etab.SECRCNotes is not null
and ctab.actflag<>'D'
and length(SecrcNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;

insert ignore into wca2.pitevt
select 'I', etab.acttime, 'SCCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.scchgNotes 
from wca.shoch as ctab
inner join wca.scchg as etab on ctab.releventid=etab.scchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='SCCHG' and etab.actflag<>'D' and etab.scchgNotes is not null
and ctab.actflag<>'D'
and length(scchgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'TKOVR', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.TKOVRNotes 
from wca.shoch as ctab
inner join wca.tkovr as etab on ctab.releventid=etab.TKOVRid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='TKOVR' and etab.actflag<>'D' and etab.TKOVRNotes is not null
and ctab.actflag<>'D'
and length(TkovrNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;
     
insert ignore into wca2.pitevt
select 'I', etab.acttime, 'WTCHG', ctab.releventid, wca.scmst.issid, 'GeneralNotes', etab.WTCHGNotes 
from wca.shoch as ctab
inner join wca.wtchg as etab on ctab.releventid=etab.Wtchgid
left outer join wca.scmst on ctab.secid=wca.scmst.secid
where
ctab.eventtype='WTCHG' and etab.actflag<>'D' and etab.WtchgNotes is not null
and ctab.actflag<>'D'
and length(WtchgNotes)>2
and ctab.oldsos<>ctab.newsos
and ctab.effectivedate is not null;

