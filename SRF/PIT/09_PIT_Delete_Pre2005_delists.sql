delete wca2.pitsrf from wca2.pitsrf
inner join wca.lstat as l on wca2.pitsrf.secid=l.secid and wca2.pitsrf.exchgcd=l.exchgcd
where
l.effectivedate<'2005/01/01' and l.lstatstatus='D' and l.actflag<>'D'
and l.lstatid=(
select lstatid from wca.lstat as sublstat
where
wca2.pitsrf.secid=sublstat.secid
and wca2.pitsrf.exchgcd=sublstat.exchgcd
order by sublstat.effectivedate desc limit 1);

delete wca2.pitsrf from wca2.pitsrf
inner join wca.scexh on wca2.pitsrf.scexhid=wca.scexh.scexhid
where
wca.scexh.actflag='D';

delete wca2.pitsrf from wca2.pitsrf
inner join wca.scmst on wca2.pitsrf.secid=wca.scmst.secid
where
wca.scmst.actflag='D';

delete wca2.pitsrf from wca2.pitsrf
inner join wca.issur on wca2.pitsrf.issid=wca.issur.issid
where
wca.issur.actflag='D';

update wca2.pitsrf as pit
inner join wca.issur as wtab on pit.issid=wtab.issid
set pit.cntryofincorp=wtab.cntryofincorp
where
pit.enddt='2099/01/01'
and pit.cntryofincorp<>wtab.cntryofincorp;

update wca2.pitsrf as pit
inner join wca.issur as wtab on pit.issid=wtab.issid
set pit.issuername=wtab.issuername
where
pit.enddt='2099/01/01'
and pit.issuername<>wtab.issuername;

update wca2.pitsrf as pit
inner join wca.scmst as wtab on pit.secid=wtab.secid
set pit.isin=wtab.isin
where
pit.enddt='2099/01/01'
and pit.isin<>wtab.isin;

update wca2.pitsrf as pit
inner join wca.scmst as wtab on pit.secid=wtab.secid
set pit.uscode=wtab.uscode
where
pit.enddt='2099/01/01'
and pit.uscode<>wtab.uscode;

update wca2.pitsrf as pit
inner join wca.scmst as wtab on pit.secid=wtab.secid
set pit.primaryexchgcd=wtab.primaryexchgcd
where
pit.enddt='2099/01/01'
and pit.primaryexchgcd<>wtab.primaryexchgcd;

update wca2.pitsrf as pit
inner join wca.scmst as wtab on pit.secid=wtab.secid
set pit.securitydesc=wtab.securitydesc
where
pit.enddt='2099/01/01'
and pit.securitydesc<>wtab.securitydesc;

update wca2.pitsrf as pit
inner join wca.scexh as wtab on pit.scexhid=wtab.scexhid
set pit.localcode=wtab.localcode
where
pit.enddt='2099/01/01'
and pit.localcode<>wtab.localcode;

update wca2.pitsrf as pit
inner join wca.scexh as wtab on pit.scexhid=wtab.scexhid
set pit.ListStatus=wtab.ListStatus
where
pit.enddt='2099/01/01'
and pit.ListStatus<>wtab.ListStatus
and (wtab.ListStatus='D' or wtab.ListStatus='S');

update wca2.pitsrf as pit
inner join wca.scmst as wtab on pit.secid=wtab.secid
set pit.ListStatus='D'
where
pit.enddt='2099/01/01'
and pit.ListStatus<>'D' and wtab.statusflag='I';

ALTER TABLE `wca2`.`pitsrf` 
ADD INDEX `ix_exchgcd_localcode` (`ExchgCD` ASC, `LocalCode` ASC);
 
