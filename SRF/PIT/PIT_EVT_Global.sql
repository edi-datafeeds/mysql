-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_EVT_Global
-- fty=n
-- hpx=edi_
-- hsx=_PIT_EVT_Global

-- # 1
select 
wca2.pitevt.RecCD,
wca2.pitevt.RecTM,
wca2.pitevt.EvtCD,
wca2.pitevt.EvtID,
wca2.pitevt.NotesName,
wca2.pitevt.Notes
from wca2.pitevt
order by evtcd, evtid, notesname;
