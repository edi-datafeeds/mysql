use wca2;
DROP TABLE pitbbe;
CREATE TABLE `pitbbe` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `BbeID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `ExchgCD` char(6) NOT NULL,
  `ListCntryCD` char(2) NOT NULL,
  `BbgExhID` char(12) NOT NULL,
  `BbgExhTk` varchar(40) NOT NULL,
  `CurenCD` char(3) NOT NULL,
  `EvtCD` varchar(20) NOT NULL,
  `EvtID` int(11) DEFAULT NULL,
  `SecID` int(11) NOT NULL,
  PRIMARY KEY (`BbeID`,`StartDT`),
  KEY `ix_secid_enddt` (`BbeID`,`EndDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
