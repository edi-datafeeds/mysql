DEAD

delete from wca2.pitchg;

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.inchgdate, 'CntryofIncorp', 'inchg', ctab.inchgid, ctab.eventtype 
from wca.inchg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and (ctab.inchgdate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.inchgdate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.inchgdate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.namechangedate, 'Issuername', 'ischg', ctab.ischgid, ctab.eventtype 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and (ctab.namechangedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.namechangedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.namechangedate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'Isin', 'icc', ctab.iccid, ctab.eventtype 
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.oldisin<>ctab.newisin)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'UScode', 'icc', ctab.iccid, ctab.eventtype 
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.dateofchange, 'SecurityDesc', 'scchg', ctab.scchgid, ctab.eventtype 
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and (ctab.dateofchange>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.dateofchange>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.dateofchange<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'PrimaryExchgCD', 'prchg', ctab.prchgid, ctab.eventtype 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'Localcode', 'lcc', ctab.lccid, ctab.eventtype 
from wca.lcc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitchg
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'ListStatus', 'lstat', ctab.lstatid, ctab.eventtype 
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

