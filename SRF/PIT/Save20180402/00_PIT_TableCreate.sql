CREATE TABLE `pitsrf` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `CntryofIncorp` char(2) NOT NULL DEFAULT '',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ISIN` char(12) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) NOT NULL DEFAULT '',
  `ListStatus` char(1) NOT NULL DEFAULT '',
  `Lot` varchar(12) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL,
  `ListCtyCD` char(2) NOT NULL,
  `SecID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`),
  KEY `ix_secid_startdt` (`SecID`,`StartDT`),
  KEY `ix_secid_enddt` (`SecID`,`EndDT`)
) ENGINE=InnoDB;

ALTER TABLE `wca2`.`pitsrf` 
ADD INDEX `ix_secid_enddt` (`SecID` ASC, `EndDT` ASC),
ADD INDEX `ix_secid_startdt` (`SecID` ASC, `StartDT` ASC);


CREATE TABLE `pitsrfsave` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `EndDT` datetime NOT NULL,
  `CntryofIncorp` char(2) NOT NULL DEFAULT '',
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) NOT NULL DEFAULT '',
  `PrimaryExchgCD` char(6) NOT NULL DEFAULT '',
  `ISIN` char(12) NOT NULL DEFAULT '',
  `USCode` char(9) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) NOT NULL DEFAULT '',
  `ListStatus` char(1) NOT NULL DEFAULT '',
  `Lot` varchar(12) NOT NULL DEFAULT '',
  `ExchgCD` char(6) NOT NULL,
  `ListCtyCD` char(2) NOT NULL,
  `SecID` int(11) NOT NULL,
  `IssID` int(11) NOT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`),
  KEY `ix_secid_startdt` (`SecID`,`StartDT`),
  KEY `ix_secid_enddt` (`SecID`,`EndDT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pitchg` (
  `RecTM` datetime NOT NULL,
  `ScexhID` int(11) NOT NULL,
  `StartDT` datetime NOT NULL,
  `ChgTbl` varchar(20) DEFAULT NULL,
  `ChgTblID` int(11) NOT NULL,
  `ChgFld` char(20) NOT NULL DEFAULT '',
  `Eventtype` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ScexhID`,`StartDT`,`ChgFld`)
) ENGINE=InnoDB;

