update wca2.pitsrf as pit
inner join wca.inchg as ctab on pit.issid=ctab.issid and pit.startdt=ctab.inchgdate
set pit.CntryofIncorp=ctab.NewCntryCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.CntryofIncorp='~'
and ctab.newcntrycd<>'';

update wca2.pitsrf as pit
inner join wca.ischg as ctab on pit.issid=ctab.issid and pit.startdt=ctab.namechangedate
set pit.Issuername=ctab.IssNewName,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.Issuername='~'
and ctab.issnewname<>'';

update wca2.pitsrf as pit
inner join wca.scchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.dateofchange
set pit.securitydesc=ctab.SecNewName,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.securitydesc='~'
and ctab.secnewname<>'';

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate
set pit.Isin=ctab.NewIsin,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.Isin='~'
and ctab.newisin<>'';

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate
set pit.Uscode=ctab.NewUscode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.Uscode='~'
and ctab.newuscode<>'';

update wca2.pitsrf as pit
inner join wca.prchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate
set pit.PrimaryExchgCD=ctab.NewExchgCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.PrimaryExchgCD='~'
and ctab.newexchgcd<>'';

update wca2.pitsrf as pit
inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.NewLocalcode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.Localcode='~'
and ctab.newlocalcode<>'';

update wca2.pitsrf as pit
inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=(select case when (ctab.LstatStatus='R' or ctab.LstatStatus='N') then 'L' else ctab.LstatStatus end),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.ListStatus='~';
and ctab.lstatstatus<>'';

update wca2.pitsrf as pit
inner join wca.ltchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.exchgcd=ctab.exchgcd 
set pit.Lot=ifnull(ctab.Newlot,''),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D'
and pit.Lot='~'
and ifnull(ctab.Newlot,'')<>'';
