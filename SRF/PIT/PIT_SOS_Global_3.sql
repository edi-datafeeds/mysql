-- arc=n
-- arp=n:\temp\
-- ddt=yyyy/mm/dd
-- dfn=n
-- dfo=n
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate) from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=edi_
-- fsx=_PIT_SOS_Global
-- fty=n
-- hdt=select ''
-- hpx=
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=select ''

-- # 1
select distinct wca2.pitsos.* from wca2.pitsos
order by secid, startdt
limit 1000000, 500000;
