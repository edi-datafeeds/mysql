delete from wca2.pitfld;

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.inchgdate, 'CntryofIncorp', '', null, case when ctab.eventtype='' then 'NS' else ctab.eventtype end 
from wca.inchg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and (ctab.inchgdate>=wca.scexh.listdate or ctab.inchgdate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.namechangedate, 'Issuername', 'ISCHG', ctab.ischgid, case when ctab.eventtype='' then 'NS' else ctab.eventtype end 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and (ctab.namechangedate>=wca.scexh.listdate or ctab.namechangedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'Isin',
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype else '' end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.oldisin<>ctab.newisin)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'UScode',
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype else '' end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.dateofchange, 'SecurityDesc', 'SCCHG', ctab.scchgid, case when ctab.eventtype='' then 'NS' else ctab.eventtype end 
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and (ctab.dateofchange>=wca.scexh.listdate or ctab.dateofchange>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'PrimaryExchgCD', 'PRCHG', ctab.prchgid, case when ctab.eventtype='' then 'NS' else ctab.eventtype end 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'Localcode',
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.eventtype else '' end as EvtCD,
case when ctab.eventtype not in ('CLEAN','CORR','INCHG','MKCHG') and ctab.releventid is not null then ctab.releventid end as EvtID,
ctab.eventtype
from wca.lcc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitfld
select 'I', ctab.acttime, wca.scexh.ScexhID, ctab.effectivedate, 'ListStatus', 'LSTAT', ctab.lstatid, case when ctab.eventtype='' then 'NS' else ctab.eventtype end 
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlstatstatus<>ctab.lstatstatus
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or ctab.effectivedate>=wca.scexh.announcedate)
and ((substring(wca.scexh.exchgcd,1,2)='US' and wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
