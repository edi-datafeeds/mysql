use wca2;
drop PROCEDURE PIT_EndDT;

DELIMITER $$
CREATE DEFINER=`sa`@`%` PROCEDURE `PIT_EndDT`(IN Sscexhid integer, IN Tscexhid integer, Sstartdt datetime,
                  IN Tstartdt datetime, IN finished integer)
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT scexhid,startdt FROM pitsrf 
  ORDER BY scexhid,startdt desc;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Sscexhid = 0;
  set Tscexhid = 0;
  set Sstartdt = '2005-01-01';
  set Tstartdt = '2005-01-01';
  set finished = 0;
 
  FETCH MyCursor INTO Tscexhid, Tstartdt;
  
  get_pitsrf: LOOP
    IF finished = 1 THEN
      LEAVE get_pitsrf;
    END IF;
    IF Sscexhid = Tscexhid THEN 
      UPDATE wca2.pitsrf SET wca2.pitsrf.enddt = date_sub(Sstartdt, interval 1 day)
      WHERE wca2.pitsrf.scexhid = Tscexhid and wca2.pitsrf.startdt=Tstartdt;
      SET Sstartdt = Tstartdt;
    ELSE 
      SET Sscexhid = Tscexhid;
      SET Sstartdt = Tstartdt;
    END IF;
    FETCH NEXT FROM MyCursor INTO Tscexhid, Tstartdt;
  END LOOP get_pitsrf;
  
  CLOSE MyCursor;
  
END$$
DELIMITER ;
