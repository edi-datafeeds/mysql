delete from wca2.pitsrf;

insert ignore into wca2.pitsrf
select 'R', '2000-01-01 15:00:00',
scx.ScexhID,
(case when ifnull(scx.listdate,'2099-01-01') < scx.announcedate and ifnull(scx.listdate,'2001-01-01') > '2005-01-01' then scx.listdate
      when scx.announcedate > '2005-01-01' then scx.announcedate 
      else '2005-01-01' end),
'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.scexh as scx
inner  join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3 and 
inner join wca.exchg on scx.exchgcd=wca.exchg.exchgcd
where
scx.actflag<>'D';

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.inchgdate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.inchg as ctab
inner  join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
ctab.inchgdate<now()
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and (ctab.inchgdate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.namechangedate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.ischg as ctab
inner  join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.namechangedate<now()
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and (ctab.namechangedate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));
     
insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.icc as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate<now()
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and (ctab.effectivedate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.dateofchange,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.scchg as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.dateofchange<now()
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and (ctab.dateofchange>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.prchg as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate<now()
and ctab.effectivedate is not null
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.lcc as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
inner join wca.exchg on ctab.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate<now()
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and (ctab.effectivedate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));
     
insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~', '~', '~', '~', '~', '~', '~', '~', wca.exchg.exchgcd, wca.exchg.cntrycd, wca.scmst.secid, wca.scmst.issid
from wca.lstat as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid>3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
inner join wca.exchg on ctab.exchgcd=wca.exchg.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate<now()
and ctab.effectivedate is not null
and (ctab.effectivedate>
    (select 
     case when ifnull(subscexh.listdate,'2099-01-01') < subscexh.announcedate and ifnull(subscexh.listdate,'2001-01-01') > '2005-01-01' 
     then subscexh.listdate
     when subscexh.announcedate > '2005-01-01' then subscexh.announcedate 
     else '2005-01-01' end as effdate from wca.scexh as subscexh where wca.scexh.actflag<>'D' and wca.scexh.scexhid=subscexh.scexhid));
