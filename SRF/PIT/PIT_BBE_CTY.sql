-- arp=n:\temp\
-- hpx=select 'edi_'
-- fpx=select 'edi_'
-- dfn=l
-- fty=n
-- ddt=yyyy-mm-dd
-- eof=EDI_ENDOFFILE

-- # 1
select distinct wca2.pitbbe.* from wca2.pitbbe
where
(listcntrycd=@ctycd or @ctycd is null)
order by secid, startdt;
