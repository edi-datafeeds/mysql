-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_SRF_Sample
-- fty=n
-- hpx=edi_
-- hsx=_PIT_SRF_Sample

-- # 1
select * from wca2.pitsrf
where
(secid=33449 and exchgcd='USNASD')
or (secid=158503 and exchgcd='USNASD')
or (secid=41543 and exchgcd='USNASD')
or (secid=5073358 and exchgcd='USNYSE')
or (secid=4980481 and exchgcd='USNYSE')
or (secid=4445967 and exchgcd='USNYSE')
or (secid=45857 and exchgcd='CATSE')
or (secid=3896729 and exchgcd='USNYSE')
order by secid, scexhid, startdt;
