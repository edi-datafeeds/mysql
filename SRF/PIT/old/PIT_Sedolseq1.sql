drop table if exists wca2.sedolseq1;
use wca2;
CREATE TABLE `sedolseq1` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `SEDOL` char(7) DEFAULT NULL,
  `CntryCD` char(2) NOT NULL,
  `RCntryCD` char(2) DEFAULT NULL,
  `CurenCD` char(3) DEFAULT NULL,
  `SedolID` int(11) NOT NULL,
  `Seqnum` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SedolID`),
  KEY `ix_sort` (`SecID`,`CntryCD`,`SedolID`)
  KEY `ix_sedol` (`Sedol`)
) ENGINE=Aria DEFAULT CHARSET=latin1;

use wca;
insert into wca2.sedolseq1
select
SEDOL.acttime,
SEDOL.secid,
SEDOL.sedol,
SEDOL.cntrycd,
SEDOL.rcntrycd,
SEDOL.curencd,
SEDOL.sedolid,
1 as seqnum
from SEDOL
where SEDOL.actflag<>'D'
and SEDOL.defunct='F'
and SEDOL.sedol<>''
and SEDOL.sedol is not null
and SEDOL.cntrycd<>'ZZ'
and SEDOL.cntrycd<>''
and SEDOL.cntrycd is not null;

call wca2.build_sedolseq1(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Tsedolid, @Tseqnum, @cnt, @finished);
