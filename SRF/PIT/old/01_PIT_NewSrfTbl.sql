delete from wca2.pitsrf;

insert ignore into wca2.pitsrf
select 'I', scx.Announcedate,
scx.ScexhID,
(select case when scx.listdate is null and scx.announcedate<'2005-01-01' then '2005-01-01' 
             when scx.listdate is null and scx.announcedate>'2005-01-01'then scx.announcedate
             when scx.listdate <'2005-01-01' then '2005-01-01' 
             else scx.listdate end),'2099-12-31',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
scx.ExchgCD, substring(scx.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.scexh as scx
inner  join wca.scmst on scx.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(scx.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and (scx.liststatus<>'D' or scx.announcedate>'2004-12-31' or (scx.liststatus='D' and  scx.acttime>'2005-01-01'))
and scx.actflag<>'D';

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.inchgdate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.inchg as ctab
inner  join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and ctab.inchgdate>'2004-12-31'
and (ctab.inchgdate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.inchgdate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.inchgdate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.namechangedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.ischg as ctab
inner  join wca.scmst on ctab.issid=wca.scmst.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and ctab.namechangedate>'2004-12-31'
and (ctab.namechangedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.namechangedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.namechangedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.icc as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.dateofchange,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.scchg as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and ctab.dateofchange>'2004-12-31'
and (ctab.dateofchange>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.dateofchange>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.dateofchange<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00', 
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.prchg as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.sdchg as ctab
inner join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca2.sedolseq1 on ctab.secid = wca2.sedolseq1.secid and ctab.newcntrycd=wca2.sedolseq1.cntrycd and 1=wca2.sedolseq1.seqnum
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.newcntrycd=substring(wca.scexh.exchgcd,1,2)
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and (ctab.oldsedol<>ctab.newsedol or ctab.eventtype='DEFUNCT' or ctab.eventtype='RESTORE')
and (ctab.cntrycd=ctab.newcntrycd)
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.lcc as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.lstat as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.lstatstatus<>ctab.oldlstatstatus
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

insert ignore into wca2.pitsrf
select 'I', '2000-01-01 15:00:00',
wca.scexh.ScexhID, 
ctab.effectivedate,'2099-01-01',
'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~' ,'~','~',
wca.scexh.ExchgCD, substring(wca.scexh.ExchgCD,1,2),wca.scmst.SecID, wca.scmst.issid
from wca.ltchg as ctab
inner  join wca.scmst on ctab.secid=wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 
                          and substring(wca.scexh.exchgcd,1,2)='US') or wca.sectygrp.secgrpid is not null)
and ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ifnull(ctab.oldlot,0)<>ifnull(ctab.newlot,0)
and ctab.effectivedate is not null
and ctab.effectivedate>'2004-12-31'
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));

