select
scx.scexhid,
case when scx.listdate is null then scx.announcedate else scx.listdate end as startdate
from wca.scexh as scx
inner join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
scx.actflag<>'D';

-- list scexh with no change histories
select
count(scx.scexhid)
from wca.scexh as scx
inner join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
scx.actflag<>'D'
and scx.scexhid not in (select scexhid from wca2.srfpit);

-- list scexh which need start chain records added
select
count(distinct scx.scexhid)
from wca.scexh as scx
inner join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca2.srfpit on scx.scexhid=wca2.srfpit.scexhid
where
scx.actflag<>'D'
and (ifnull(scx.listdate,'2000-01-01') <> wca2.srfpit.startdt and scx.announcedate <> wca2.srfpit.startdt);

select * from wca2.srfpit where startdt is null;

-- list scexh which don't need start chain records added
select
count(distinct scx.scexhid)
from wca.scexh as scx
inner join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca2.srfpit on scx.scexhid=wca2.srfpit.scexhid
where
scx.actflag<>'D'
and (ifnull(scx.listdate,'2000-01-01')  = wca2.srfpit.startdt or scx.announcedate = wca2.srfpit.startdt);

insert ignore into wca2.srfpit
select scx.Acttime, wca.scmst.SectyCD, scx.ExchgCD, substring(scx.ExchgCD,1,2),
scx.ScexhID, wca.scmst.SecID, wca.scmst.issid, '2000-01-01', (select case when scx.listdate is null then scx.announcedate else scx.listdate end),'2099-01-01',
'', '' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'1999-01-01' ,''
from wca.scexh as scx
left outer join wca.scmst on scx.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
scx.actflag<>'D';


