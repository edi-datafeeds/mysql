DELIMITER $$

DROP PROCEDURE IF EXISTS `wca2`.`PIT_Parse_Forward` $$
CREATE DEFINER=`sa`@`%` PROCEDURE `wca2`.`PIT_Parse_Forward`
 (IN
  Sscexhid integer,
  Sstartdt datetime,
  SCntryofIncorp char(2),
  SIssuerName varchar(70),
  SSecurityDesc varchar(70),
  SPrimaryExchgCD char(6),
  SISIN char(12),
  SUSCode char(9),
  SSedol char(7),
  SSedolDefunct char(1),
  SLocalCode varchar(50),
  SListStatus char(1),
  SLot integer,
  Tscexhid integer,
  Tstartdt datetime,
  TCntryofIncorp char(2),
  TIssuerName varchar(70),
  TSecurityDesc varchar(70),
  TPrimaryExchgCD char(6),
  TISIN char(12),
  TUSCode char(9),
  TSedol char(7),
  TSedolDefunct char(1),
  TLocalCode varchar(50),
  TListStatus char(1),
  TLot integer,
  finished integer
 )
  
BEGIN

  DECLARE MyCursor CURSOR FOR
  SELECT scexhid,startdt,CntryofIncorp,IssuerName,SecurityDesc,PrimaryExchgCD,ISIN,USCode,Sedol,SedolDefunct,LocalCode,ListStatus,Lot
  FROM pitsrf 
  ORDER BY scexhid,startdt;

  DECLARE CONTINUE HANDLER
         FOR NOT FOUND SET finished = 1;
         
  OPEN MyCursor;
  
  set Sscexhid = 0;
  set Sstartdt = '2001-01-01';
  set SCntryofIncorp = '';
  set SIssuerName = '';
  set SSecurityDesc = '';
  set SPrimaryExchgCD = '';
  set SISIN = '';
  set SUSCode = '';
  set SSedol = '';
  set SSedolDefunct = '';
  set SLocalCode = '';
  set SListStatus = '';
  set SLot = 0;
  set Tscexhid = 0;
  set Tstartdt = '2001-01-01';
  set TCntryofIncorp = '';
  set TIssuerName = '';
  set TSecurityDesc = '';
  set TPrimaryExchgCD = '';
  set TISIN = '';
  set TUSCode = '';
  set TSedol = '';
  set TSedolDefunct = '';
  set TLocalCode = '';
  set TListStatus = '';
  set TLot = '';
  set finished = 0;
 
  FETCH MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                      TSedol,TSedolDefunct,TLocalCode,TListStatus,TLot;
  
  get_pitsrf: LOOP
    IF finished = 1 THEN
      LEAVE get_pitsrf;
    END IF;
    IF Sscexhid = Tscexhid THEN 
      UPDATE wca2.pitsrf 
      SET wca2.pitsrf.CntryofIncorp     = (SELECT CASE WHEN TCntryofIncorp      ='~' THEN SCntryofIncorp    ELSE TCntryofIncorp     END),
          wca2.pitsrf.IssuerName      = (SELECT CASE WHEN TIssuerName       ='~' THEN SIssuerName     ELSE TIssuerName      END),
          wca2.pitsrf.SecurityDesc    = (SELECT CASE WHEN TSecurityDesc     ='~' THEN SSecurityDesc   ELSE TSecurityDesc    END),
          wca2.pitsrf.PrimaryExchgCD  = (SELECT CASE WHEN TPrimaryExchgCD   ='~' THEN SPrimaryExchgCD ELSE TPrimaryExchgCD  END),
          wca2.pitsrf.ISIN            = (SELECT CASE WHEN TISIN             ='~' THEN SISIN           ELSE TISIN            END),
          wca2.pitsrf.USCode          = (SELECT CASE WHEN TUSCode           ='~' THEN SUSCode         ELSE TUSCode          END),
          wca2.pitsrf.Sedol           = (SELECT CASE WHEN TSedol            ='~' THEN SSedol          ELSE TSedol           END),
          wca2.pitsrf.SedolDefunct    = (SELECT CASE WHEN TSedolDefunct     ='~' THEN SSedolDefunct   ELSE TSedolDefunct    END),
          wca2.pitsrf.LocalCode       = (SELECT CASE WHEN TLocalCode        ='~' THEN SLocalCode      ELSE TLocalCode       END),
          wca2.pitsrf.ListStatus      = (SELECT CASE WHEN TListStatus       ='~' THEN SListStatus     ELSE TListStatus      END),
          wca2.pitsrf.Lot             = (SELECT CASE WHEN TLot              ='~' THEN SLot            ELSE TLot             END)
      WHERE wca2.pitsrf.scexhid = Tscexhid and wca2.pitsrf.startdt=Tstartdt;
      SET Sstartdt = Tstartdt;
    ELSE 
      SET Sscexhid        = Tscexhid;
      SET SCntryofIncorp    = TCntryofIncorp;
      SET SIssuerName     = TIssuerName;
      SET SSecurityDesc   = TSecurityDesc;
      SET SPrimaryExchgCD = TPrimaryExchgCD;
      SET SISIN           = TISIN;
      SET SUSCode         = TUSCode;
      SET SSedol          = TSedol;
      SET SSedolDefunct   = TSedolDefunct;
      SET SLocalCode      = TLocalCode;
      SET SListStatus     = TListStatus;
      SET SLot            = TLot;

    END IF;
    FETCH NEXT FROM MyCursor INTO Tscexhid,Tstartdt,TCntryofIncorp,TIssuerName,TSecurityDesc,TPrimaryExchgCD,TISIN,TUSCode,
                      TSedol,TSedolDefunct,TLocalCode,TListStatus,TLot;
  END LOOP get_pitsrf;
  
  CLOSE MyCursor;
  
END $$

DELIMITER ;
