
CREATE TABLE `wca2`.`popoldstat` (
  `oldlstatid` INT NOT NULL,
  `newlstatid` INT NOT NULL,
  `oldlstatstatus` CHAR(1) NULL,
  `newlstatstatus` CHAR(1) NULL,
  PRIMARY KEY (`newlstatid`));


insert into wca2.popoldstat
select
oldlstat.lstatid as OldID,
newlstat.lstatid as NewID,
oldlstat.lstatstatus as OldNewlstat,
newlstat.lstatstatus as NewNewlstat
from wca.lstat as newlstat
left outer join wca.lstat as oldlstat on newlstat.secid = oldlstat.secid
                                  and newlstat.exchgcd = oldlstat.exchgcd
                                  and oldlstat.actflag <> 'D'
                                  and ifnull(oldlstat.effectivedate,'2030-01-01')<newlstat.effectivedate
where
oldlstat.lstatid is not null
and newlstat.actflag<>'D'
and newlstat.lstatstatus <> '' 
and ifnull(newlstat.oldlstatstatus,'') = '' 
and oldlstat.lstatstatus <> ''
and oldlstat.lstatid = 
(select lstatid from wca.lstat 
where newlstat.secid = wca.lstat.secid
and newlstat.exchgcd = wca.lstat.exchgcd
and newlstat.effectivedate>ifnull(wca.lstat.effectivedate,'2030-01-01')
and wca.lstat.lstatstatus <> ''  
and wca.lstat.actflag <> 'D' 
order by effectivedate desc limit 1);

update wca.lstat as uplstat
inner join wca2.popoldstat as prevlstat on uplstat.lstatid=prevlstat.newlstatid
set uplstat.oldLstatStatus=prevlstat.oldlstatstatus;