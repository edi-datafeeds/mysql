update wca2.pitsrf as pit
inner join wca.inchg as ctab on pit.issid=ctab.issid
set pit.CntryofIncorp=ctab.oldCntryCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.inchgdate is not null and pit.actflag='R'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgid=(select sub.inchgid from wca.inchg as sub 
                  where pit.issid=sub.issid and pit.startdt<sub.inchgdate and sub.oldcntrycd<>sub.newcntrycd
                  order by sub.inchgdate limit 1);

update wca2.pitsrf as pit
inner join wca.inchg as ctab on pit.issid=ctab.issid
set pit.CntryofIncorp=ctab.newCntryCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.inchgdate is not null and pit.actflag='R' and pit.CntryofIncorp='~'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgid=(select sub.inchgid from wca.inchg as sub 
                  where pit.issid=sub.issid and pit.startdt>sub.inchgdate and sub.oldcntrycd<>sub.newcntrycd
                  order by sub.inchgdate desc limit 1);

update wca2.pitsrf as pit
inner join wca.ischg as ctab on pit.issid=ctab.issid
set pit.Issuername=ctab.IssOldName,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.namechangedate is not null and pit.actflag='R' 
and ctab.issoldname<>ctab.issnewname
and ctab.ischgid=(select sub.ischgid from wca.ischg as sub 
                  where pit.issid=sub.issid and pit.startdt<sub.namechangedate and sub.issoldname<>sub.issnewname
                  order by sub.namechangedate limit 1);

update wca2.pitsrf as pit
inner join wca.ischg as ctab on pit.issid=ctab.issid
set pit.Issuername=ctab.IssNewName,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.namechangedate is not null and pit.actflag='R' and pit.Issuername='~'
and ctab.issoldname<>ctab.issnewname
and ctab.ischgid=(select sub.ischgid from wca.ischg as sub 
                  where pit.issid=sub.issid and pit.startdt>sub.namechangedate and sub.issoldname<>sub.issnewname
                  order by sub.namechangedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.Isin=ctab.OldIsin,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ctab.oldisin<>ctab.newisin
and ctab.iccid=(select sub.iccid from wca.icc as sub 
                where pit.secid=sub.secid and pit.startdt<sub.effectivedate and sub.oldisin<>sub.newisin
                order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.Isin=ctab.NewIsin,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.isin='~' or pit.isin='')
and ctab.oldisin<>ctab.newisin
and ctab.iccid=(select sub.iccid from wca.icc as sub 
                where pit.secid=sub.secid and pit.startdt>sub.effectivedate and sub.oldisin<>sub.newisin
                order by sub.effectivedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.UScode=ctab.OldUScode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ctab.oldUScode<>ctab.newUScode
and ctab.iccid=(select sub.iccid from wca.icc as sub 
                where pit.secid=sub.secid and pit.startdt<sub.effectivedate and sub.oldUScode<>sub.newUScode
                order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.UScode=ctab.NewUScode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.UScode='~' or pit.UScode='')
and ctab.oldUScode<>ctab.newUScode
and ctab.iccid=(select sub.iccid from wca.icc as sub 
                where pit.secid=sub.secid and pit.startdt>sub.effectivedate and sub.oldUScode<>sub.newUScode
                order by sub.effectivedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.scchg as ctab on pit.secid=ctab.secid
set pit.SecurityDesc=ctab.SecOldName
where ctab.actflag<>'D' and ctab.dateofchange is not null and pit.actflag='R' 
and ctab.secoldname<>ctab.secnewname
and ctab.scchgid=(select sub.scchgid from wca.scchg as sub 
                  where pit.secid=sub.secid and pit.startdt<sub.dateofchange and sub.secoldname<>sub.secnewname
                  order by sub.dateofchange limit 1);

update wca2.pitsrf as pit
inner join wca.scchg as ctab on pit.secid=ctab.secid
set pit.SecurityDesc=ctab.SecNewName
where ctab.actflag<>'D' and ctab.dateofchange is not null and pit.actflag='R' and (pit.SecurityDesc='~' or pit.SecurityDesc='')
and ctab.secoldname<>ctab.secnewname
and ctab.scchgid=(select sub.scchgid from wca.scchg as sub 
                  where pit.secid=sub.secid and pit.startdt>sub.dateofchange 
                  order by sub.dateofchange desc limit 1);

update wca2.pitsrf as pit
inner join wca.prchg as ctab on pit.secid=ctab.secid
set pit.PrimaryExchgCD=ctab.OldExchgCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ctab.oldexchgcd<>ctab.newexchgcd
and ctab.prchgid=(select sub.prchgid from wca.prchg as sub 
                  where pit.secid=sub.secid and pit.startdt<sub.effectivedate and sub.oldexchgcd<>sub.newexchgcd
                  order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.prchg as ctab on pit.secid=ctab.secid
set pit.PrimaryExchgCD=ctab.NewExchgCD,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.PrimaryExchgCD='~' or pit.PrimaryExchgCD='')
and ctab.oldexchgcd<>ctab.newexchgcd
and ctab.prchgid=(select sub.prchgid from wca.prchg as sub 
                  where pit.secid=sub.secid and pit.startdt>sub.effectivedate and sub.oldexchgcd<>sub.newexchgcd
                  order by sub.effectivedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.OldLocalcode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ctab.newlocalcode<>ctab.oldlocalcode
and ctab.lccid=(select sub.lccid from wca.lcc as sub 
                where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt<sub.effectivedate and sub.newlocalcode<>sub.oldlocalcode
                order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.NewLocalcode,
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.Localcode='~' or pit.Localcode='')
and ctab.newlocalcode<>ctab.oldlocalcode
and ctab.lccid=(select sub.lccid from wca.lcc as sub 
                where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt>sub.effectivedate and sub.newlocalcode<>sub.oldlocalcode
                order by sub.effectivedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=(select case when (ctab.OldLstatStatus='' or ctab.OldLstatStatus='R' or ctab.OldLstatStatus='N') then 'L' else ctab.OldLstatStatus end),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ctab.oldlstatstatus<>''
and ctab.lstatid=(select sub.lstatid from wca.lstat as sub 
                  where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt<sub.effectivedate and sub.oldlstatstatus<>''
                  order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=(select case when (ctab.LstatStatus='' or ctab.LstatStatus='R' or ctab.LstatStatus='N') then 'L' else ctab.LstatStatus end),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.ListStatus='~' or pit.ListStatus='')
and ctab.lstatstatus<>''
and ctab.lstatid=(select sub.lstatid from wca.lstat as sub 
                  where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt>sub.effectivedate and sub.lstatstatus<>''
                  order by sub.effectivedate desc limit 1);

update wca2.pitsrf as pit
inner join wca.ltchg as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Lot=ifnull(ctab.OldLot,''),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' 
and ifnull(ctab.oldlot,0)<>ifnull(ctab.newlot,0)
and ctab.ltchgid=(select sub.ltchgid from wca.ltchg as sub 
                  where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt<sub.effectivedate and ifnull(sub.oldlot,0)<>ifnull(sub.newlot,0)
                  order by sub.effectivedate limit 1);

update wca2.pitsrf as pit
inner join wca.ltchg as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Lot=ifnull(ctab.NewLot,''),
    pit.RecTM=(select case when ctab.Acttime>pit.RecTM then ctab.Acttime else pit.RecTM end)
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.actflag='R' and (pit.Lot='~' or pit.Lot='')
and ifnull(ctab.oldlot,0)<>ifnull(ctab.newlot,0)
and ctab.ltchgid=(select sub.ltchgid from wca.ltchg as sub 
                  where pit.secid=sub.secid and pit.exchgcd=sub.exchgcd and pit.startdt>sub.effectivedate and  ifnull(sub.oldlot,0)<>ifnull(sub.newlot,0)
                  order by sub.effectivedate desc limit 1);
