update wca2.pitsrf as pit
inner join wca.inchg as ctab on pit.issid=ctab.issid and pit.startdt=ctab.inchgdate
set pit.CntryofIncorp=ctab.NewCntryCD, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.ischg as ctab on pit.issid=ctab.issid and pit.startdt=ctab.namechangedate
set pit.Issuername=ctab.IssNewName, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.scchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.dateofchange
set pit.isin=ctab.SecNewName, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.icc as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate
set pit.isin=ctab.NewIsin, pit.isin=ctab.NewUscode, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.prchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate
set pit.PrimaryExchgCD=ctab.NewExchgCD, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.NewLocalcode, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=ctab.LStatStatus, pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';

update wca2.pitsrf as pit
inner join wca.sdchg as ctab on pit.secid=ctab.secid and pit.startdt=ctab.effectivedate and pit.listcntrycd=ctab.NewCntryCD 
set pit.Sedol=ctab.NewSedol, pit.SRegisterCntryCD=ctab.NewRCntryCD, pit.STradeCurenCD=ctab.NewCurenCD,
    pit.FldChgReason=(select case when EventType='DEFUNCT' then 'T' else 'F' end), pit.RecTM=ctab.Acttime
where ctab.actflag<>'D';
