select issid from wca.scmst where secid=4321;
select * from wca.icc where effectivedate is null;
select * from wca.sdchg where effectivedate is null;
select * from wca.lcc where effectivedate is null;
select * from wca.ischg where namechangedate is null;
select * from wca.lstat where effectivedate is null;
select * from wca.prchg where effectivedate is null;
select * from wca.scchg where dateofchange is null;


select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.inchgdate as startdt 
from wca.inchg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldcntrycd<>ctab.newcntrycd
and ctab.inchgdate is not null
and (ctab.inchgdate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.inchgdate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.inchgdate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.namechangedate as startdt 
from wca.ischg as ctab
left outer join wca.scmst on ctab.issid=wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.issoldname<>ctab.issnewname
and ctab.namechangedate is not null
and (ctab.namechangedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.namechangedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.namechangedate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.effectivedate as startdt 
from wca.icc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and (ctab.oldisin<>ctab.newisin or ctab.olduscode<>ctab.newuscode)
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.dateofchange as startdt
from wca.scchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.secoldname<>ctab.secnewname
and ctab.dateofchange is not null
and (ctab.dateofchange>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.dateofchange>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.dateofchange<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.effectivedate as startdt 
from wca.prchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and ctab.oldexchgcd<>ctab.newexchgcd
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.effectivedate as startdt 
from wca.lcc as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.oldlocalcode<>ctab.newlocalcode
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.effectivedate as startdt 
from wca.lstat as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.exchgcd=wca.scexh.exchgcd
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime))
union
select wca.scexh.scexhid, wca.scmst.secid, wca.scmst.issid, wca.scexh.exchgcd, ctab.effectivedate as startdt 
from wca.sdchg as ctab
left outer join wca.scmst on ctab.secid=wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
left outer join wca.scexh on wca.scmst.secid=wca.scexh.secid and ctab.newcntrycd=substring(wca.scexh.exchgcd,1,2)
where
ctab.actflag<>'D' and wca.scexh.actflag<>'D'
and ctab.effectivedate is not null
and (ctab.oldsedol<>ctab.newsedol or ctab.rcntrycd<>ctab.newrcntrycd or ctab.oldcurencd<>ctab.newcurencd or ctab.Eventtype='DEFUNCT' or ctab.Eventtype='RESTORE')
and (ctab.effectivedate>=wca.scexh.listdate or (wca.scexh.listdate is null and ctab.effectivedate>=wca.scexh.announcedate))
and (wca.scexh.liststatus<>'D' or (wca.scexh.liststatus='D' and ctab.effectivedate<wca.scexh.acttime));
