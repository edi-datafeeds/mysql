update wca2.srfpit as pit inner join wca.inchg as ctab on pit.issid=ctab.issid
set pit.CntryofIncorp=ctab.OldCntryCD
where ctab.actflag<>'D' and ctab.inchgdate is not null and pit.delistdt='1999-01-01' 
and ctab.inchgid=(select sub.inchgid from wca.inchg as sub where pit.issid=sub.issid and pit.startdt<sub.inchgdate order by sub.inchgdate limit 1);

update wca2.srfpit as pit inner join wca.inchg as ctab on pit.issid=ctab.issid
set pit.CntryofIncorp=ctab.newCntryCD
where ctab.actflag<>'D' and ctab.inchgdate is not null and pit.delistdt='1999-01-01' and pit.CntryofIncorp=''
and ctab.inchgid=(select sub.inchgid from wca.inchg as sub where pit.issid=sub.issid and pit.startdt>sub.inchgdate order by sub.inchgdate desc limit 1);

update wca2.srfpit as pit inner join wca.ischg as ctab on pit.issid=ctab.issid
set pit.Issuername=ctab.IssOldName
where ctab.actflag<>'D' and ctab.namechangedate is not null and pit.delistdt='1999-01-01' 
and ctab.ischgid=(select sub.ischgid from wca.ischg as sub where pit.issid=sub.issid and pit.startdt<sub.namechangedate order by sub.namechangedate limit 1);

update wca2.srfpit as pit inner join wca.ischg as ctab on pit.issid=ctab.issid
set pit.Issuername=ctab.IssNewName
where ctab.actflag<>'D' and ctab.namechangedate is not null and pit.delistdt='1999-01-01' and pit.Issuername=''
and ctab.ischgid=(select sub.ischgid from wca.ischg as sub where pit.issid=sub.issid and pit.startdt>sub.namechangedate order by sub.namechangedate desc limit 1);

update wca2.srfpit as pit inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.Isin=ctab.NewIsin, pit.Uscode=ctab.NewUscode
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' 
and ctab.iccid=(select sub.iccid from wca.icc as sub where pit.secid=sub.secid and pit.startdt<sub.effectivedate order by sub.effectivedate limit 1);

update wca2.srfpit as pit inner join wca.icc as ctab on pit.secid=ctab.secid
set pit.Isin=ctab.OldIsin, pit.Uscode=ctab.OldUscode
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' and pit.isin=''
and ctab.iccid=(select sub.iccid from wca.icc as sub where pit.secid=sub.secid and pit.startdt>sub.effectivedate order by sub.effectivedate desc limit 1);

update wca2.srfpit as pit inner join wca.scchg as ctab on pit.secid=ctab.secid
set pit.SecurityDesc=ctab.SecNewName
where ctab.actflag<>'D' and ctab.dateofchange is not null and pit.delistdt='1999-01-01' 
and ctab.scchgid=(select sub.scchgid from wca.scchg as sub where pit.secid=sub.secid and pit.startdt<sub.dateofchange order by sub.dateofchange limit 1);

update wca2.srfpit as pit inner join wca.scchg as ctab on pit.secid=ctab.secid
set pit.SecurityDesc=ctab.SecOldName
where ctab.actflag<>'D' and ctab.dateofchange is not null and pit.delistdt='1999-01-01' and pit.SecurityDesc=''
and ctab.scchgid=(select sub.scchgid from wca.scchg as sub where pit.secid=sub.secid and pit.startdt>sub.dateofchange order by sub.dateofchange desc limit 1);

update wca2.srfpit as pit inner join wca.prchg as ctab on pit.secid=ctab.secid
set pit.PrimaryExchgCD=ctab.OldExchgCD
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' 
and ctab.prchgid=(select sub.prchgid from wca.prchg as sub where pit.secid=sub.secid and pit.startdt<sub.effectivedate order by sub.effectivedate limit 1);

update wca2.srfpit as pit inner join wca.prchg as ctab on pit.secid=ctab.secid
set pit.PrimaryExchgCD=ctab.NewExchgCD
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' and pit.PrimaryExchgCD=''
and ctab.prchgid=(select sub.prchgid from wca.prchg as sub where pit.secid=sub.secid and pit.startdt>sub.effectivedate order by sub.effectivedate desc limit 1);

update wca2.srfpit as pit inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.OldLocalcode
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' 
and ctab.lccid=(select sub.lccid from wca.lcc as sub where pit.secid=sub.secid and pit.startdt<sub.effectivedate order by sub.effectivedate limit 1);

update wca2.srfpit as pit inner join wca.lcc as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.Localcode=ctab.NewLocalcode
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' and pit.Localcode=''
and ctab.lccid=(select sub.lccid from wca.lcc as sub where pit.secid=sub.secid and pit.startdt>sub.effectivedate order by sub.effectivedate desc limit 1);

update wca2.srfpit as pit inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=ctab.OldLstatStatus
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' 
and ctab.lstatid=(select sub.lstatid from wca.lstat as sub where pit.secid=sub.secid and pit.startdt<sub.effectivedate order by sub.effectivedate limit 1);

update wca2.srfpit as pit inner join wca.lstat as ctab on pit.secid=ctab.secid and pit.exchgcd=ctab.exchgcd 
set pit.ListStatus=ctab.LstatStatus
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' and pit.ListStatus=''
and ctab.lstatid=(select sub.lstatid from wca.lstat as sub where pit.secid=sub.secid and pit.startdt>sub.effectivedate order by sub.effectivedate desc limit 1);

update wca2.srfpit as pit inner join wca.sdchg as ctab on pit.secid=ctab.secid and pit.listcntrycd=ctab.NewCntryCD  
set pit.Sedol=ctab.OldSedol, pit.SRegisterCntryCD=ctab.RCntryCD, pit.STradeCurenCD=ctab.OldCurenCD,
    pit.SDefunct=(select case when EventType='DEFUNCT' then 'T' else 'F' end), 
    pit.FldChgReason=(select case when EventType='DEFUNCT' then 'T' else 'F' end), pit.RecTM=ctab.Acttime
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' 
and ctab.sdchgid=(select sub.sdchgid from wca.sdchg as sub where pit.secid=sub.secid and pit.startdt<sub.effectivedate order by sub.effectivedate limit 1);

update wca2.srfpit as pit inner join wca.sdchg as ctab on pit.secid=ctab.secid and pit.listcntrycd=ctab.NewCntryCD 
set pit.Sedol=ctab.NewSedol, pit.SRegisterCntryCD=ctab.NewRCntryCD, pit.STradeCurenCD=ctab.NewCurenCD,
    pit.SDefunct=(select case when EventType='DEFUNCT' then 'T' else 'F' end), 
    pit.FldChgReason=(select case when EventType='DEFUNCT' then 'T' else 'F' end), pit.RecTM=ctab.Acttime
where ctab.actflag<>'D' and ctab.effectivedate is not null and pit.delistdt='1999-01-01' and pit.ListStatus=''
and ctab.sdchgid=(select sub.sdchgid from wca.sdchg as sub where pit.secid=sub.secid and pit.startdt>sub.effectivedate order by sub.effectivedate desc limit 1);
