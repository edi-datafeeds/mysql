delete from wca2.pitbbe;

insert ignore into wca2.pitbbe
select 'I',
ctab.acttime, 
wca.bbe.BbeID,
ctab.effectivedate,
'2099-01-01',
bbe.exchgcd,
substring(bbe.exchgcd,1,2) as CntryCD,
bbe.BbgExhID,
ctab.newbbgexhtk,
bbe.CurenCD,
'BBEC',
ctab.BbecID,
ctab.SecID
from wca.bbec as ctab
inner join wca.bbe on ctab.bbeid=wca.bbe.bbeid
where
ctab.effectivedate<now()
and wca.bbe.exchgcd<>''
and wca.bbe.actflag<>'D'
and ctab.actflag<>'D'
and ctab.newbbgexhtk<>''
and ctab.oldbbgexhtk<>ctab.newbbgexhtk
and ctab.effectivedate is not null;
insert ignore into wca2.pitbbe
select 'I',
ctab.acttime, 
ctab.BbeID,
ctab.announcedate,
'2099-01-01',
ctab.exchgcd,
substring(ctab.exchgcd,1,2) as CntryCD,
ctab.BbgExhID,
ctab.bbgexhtk,
ctab.CurenCD,
'',
null,
ctab.SecID
from wca.bbe as ctab
where
ctab.exchgcd<>''
and ctab.actflag<>'D'
and ctab.bbgexhtk<>''
and ctab.bbeid not in 
(select bbeid from wca2.pitbbe
where
wca2.pitbbe.secid=ctab.secid);
