delete from wca2.pitlnk;

insert ignore into wca2.pitlnk
select distinct 'I', etab.acttime, etab.SecID, etab.ResSecID, etab.EffectiveDate, 'secrc', prevsec.sectycd, nextsec.sectycd, 
       etab.SecrcID, etab.EventType, 'R' 
from wca.secrc as etab
inner join wca.scmst as prevsec on etab.secid=prevsec.secid
inner join wca.scmst as nextsec on etab.ressecid=nextsec.secid
left outer join wca.sectygrp on prevsec.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
where
etab.actflag<>'D' and etab.actflag<>'C'
and etab.secid is not null
and etab.ressecid is not null
and etab.secid<>etab.ressecid
and prevsec.actflag<>'D'
and nextsec.actflag<>'D'
and ifnull(etab.effectivedate,'2001-01-01')>'2004-01-01'
and eventtype not in ('CORR','CLEAN')
and ifnull(etab.effectivedate,'2001-01-01')>'2004-01-01'
and ((prevsec.sectycd='BND' and ord(substring(prevsec.uscode,7,1)) between 48 and 57 
                          and ord(substring(prevsec.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitlnk
select 'I', etab.acttime, wca.rd.SecID, etab.ResSecID, wca.rd.RecDate, 'scswp', prevsec.sectycd, nextsec.sectycd, 
       etab.RdID, '' as EventType, 'R' 
from wca.scswp as etab
inner join wca.rd on etab.rdid=wca.rd.rdid
inner join wca.scmst as prevsec on wca.rd.secid=prevsec.secid
inner join wca.scmst as nextsec on etab.ressecid=nextsec.secid
left outer join wca.sectygrp on prevsec.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
where
etab.actflag<>'D' and etab.actflag<>'C'
and rd.secid is not null
and etab.ressecid is not null
and wca.rd.secid<>etab.ressecid
and prevsec.actflag<>'D'
and nextsec.actflag<>'D'
and ifnull(wca.rd.Recdate,'2001-01-01')>'2004-01-01'
and ((prevsec.sectycd='BND' and ord(substring(prevsec.uscode,7,1)) between 48 and 57 
                          and ord(substring(prevsec.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);

insert ignore into wca2.pitlnk
select 'I', etab.acttime, etab.SecID, etab.ResSecID, etab.Enddate, 'ctx', prevsec.sectycd, nextsec.sectycd,
       etab.CtxID, etab.EventType, 'R' 
from wca.ctx as etab
inner join wca.scmst as prevsec on etab.secid=prevsec.secid
inner join wca.scmst as nextsec on etab.ressecid=nextsec.secid
left outer join wca.sectygrp on prevsec.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
where
etab.actflag<>'D' and etab.actflag<>'C'
and etab.secid is not null
and etab.ressecid is not null
-- and etab.secid<>etab.ressecid
and prevsec.actflag<>'D'
and nextsec.actflag<>'D'
and ifnull(etab.enddate,'2001-01-01')>'2004-01-01'
and eventtype not in ('CORR','CLEAN');
