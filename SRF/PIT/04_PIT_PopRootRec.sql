update wca2.pitsrf as pit
inner join wca.issur as iss on pit.issid=iss.issid
inner join wca.scmst as scm on pit.secid=scm.secid
inner join wca.scexh as scx on pit.scexhid=scx.scexhid
set pit.CntryofIncorp=(select case when pit.CntryofIncorp='~' then iss.cntryofincorp else pit.CntryofIncorp end), 
    pit.issuername=(select case when pit.issuername='~' then iss.issuername else pit.issuername end), 
    pit.securitydesc=(select case when pit.securitydesc='~' then scm.securitydesc else pit.securitydesc end), 
    pit.primaryexchgcd=(select case when pit.primaryexchgcd='~' then scm.primaryexchgcd else pit.primaryexchgcd end), 
    pit.ISIN=(select case when pit.ISIN='~' then scm.ISIN else pit.ISIN end), 
    pit.USCode=(select case when pit.USCode='~' then scm.USCode else pit.USCode end), 
    pit.localcode=(select case when pit.localcode='~' then scx.localcode else pit.localcode end),
    pit.ListStatus=(select case when pit.ListStatus='~' and (scx.ListStatus='R' or scx.ListStatus='N' or scx.ListStatus='') then 'L'
                                when pit.ListStatus='~' and (scx.ListStatus<>'R' and scx.ListStatus<>'N' and scx.ListStatus<>'') then scx.ListStatus
                                else pit.ListStatus end)
where pit.RecCD='R';
