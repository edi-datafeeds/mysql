-- arp=n:\temp\
-- fpx=edi_
-- fsx=_PIT_SXR_Sample
-- fty=n
-- hpx=edi_
-- hsx=_PIT_SXR_Sample

-- # 1
select distinct
wca.scmst.ActFlag,
scmst.acttime as Changed,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.scmst.FISN,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.CntryIncorpNumber,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.LEI,
wca.issur.FinancialYearEnd,
wca.issur.ShellCompany as ShellComp,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.NAICS,
wca.scmst.CIC,
wca.scmst.CFI as CFIcode,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.StructCD,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
case when wca.scmst.StatusFlag='' then upper('A') else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.Voting,
wca.scmst.VotePerSec,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate as EffectiveDate
from wca.scmst
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
where
wca.scmst.actflag<>'D'
where
issid=3199
and (issid=6455
or issid=9850
or issid=18451
or issid=23291
or issid=29994
or issid=40149
or issid=76937
or issid=112882
or issid=152315)
and wca.issur.actflag<>'D'
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 
                          and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
     or wca.sectygrp.secgrpid is not null);
