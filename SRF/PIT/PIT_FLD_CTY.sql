-- arp=n:\temp\
-- hpx=select 'edi_'
-- fpx=select 'edi_'
-- dfn=l
-- fty=n
-- ddt=yyyy-mm-dd
-- eof=EDI_ENDOFFILE

-- # 1
select distinct wca2.pitfld.* from wca2.pitfld
inner join wca.scexh on wca2.pitfld.scexhid=wca.scexh.scexhid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
where
cntrycd=@ctycd
order by scexhid, startdt;
