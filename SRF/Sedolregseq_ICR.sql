drop table if exists wca2.sedolregseq;
use wca2;
CREATE TABLE `sedolregseq` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `RCntryCD` char(2) NOT NULL DEFAULT '',
  `SedolID` int(11) NOT NULL,
  `Seqnum` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SedolID`),
  KEY `ix_sort` (`SecID`,`CntryCD`,`SedolID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

use wca;
insert into wca2.sedolregseq
select
sedol.acttime,
sedol.secid,
sedol.cntrycd,
sedol.rcntrycd,
sedol.sedolid,
1 as seqnum
from sedol
inner join scmst on sedol.secid=scmst.secid
where
ifnull(sedol.sedol,'')<>''
and ifnull(sedol.cntrycd,'')<>''
and ifnull(sedol.cntrycd,'')<>'XS'
and ifnull(sedol.cntrycd,'')<>'ZZ'
and (scmst.sectycd<>'BND' 
     or (sedol.cntrycd='US'
     and ord(substring(scmst.uscode,7,1)) between 48 and 57 and ord(substring(scmst.uscode,8,1)) between 48 and 57)
	);

call wca2.sp_sedolregseq(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Tsedolid, @Tseqnum, @cnt, @finished);
