-- arc=n
-- fty=n
-- arp=n:\temp\
-- hpx=EDI_WCA_BBE_
-- dfn=n
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_BBE_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
SELECT distinct
upper('BBE') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbeid,
wca.main.secid,
wca.main.exchgcd,
wca.main.curencd,
wca.main.bbgexhid,
wca.main.bbgexhtk
from wca.bbe as main
inner join wca.bbc on wca.main.secid = wca.bbc.secid and substring(wca.main.exchgcd,1,2) = wca.bbc.cntrycd and wca.main.curencd=wca.bbc.curencd
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE 
wca.main.acttime>(select date_sub(max(wca.tbl_opslog.acttime), interval 20 minute) from wca.tbl_opslog);


