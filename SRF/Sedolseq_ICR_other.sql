drop table if exists wca2.sedolseq;
use wca2;
CREATE TABLE `sedolseq` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `RcntryCD` char(2) NOT NULL DEFAULT '',
  `CurenCD` char(3) NOT NULL DEFAULT '',
  `SedolID` int(11) NOT NULL,
  `Seqnumreg` tinyint(4) NOT NULL DEFAULT '1',
  `Seqnumcur` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SedolID`),
  KEY `ix_sortreg` (`SecID`,`CntryCD`,`RcntryCD`,`SedolID`),
  KEY `ix_sortcur` (`SecID`,`CntryCD`,`CurenCD`,`SedolID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

use wca;
insert into wca2.sedolseq
select
wca_other.sedol.acttime,
wca_other.sedol.secid,
wca_other.sedol.cntrycd,
case when wca_other.sedol.rcntrycd<>'' then wca_other.sedol.rcntrycd else 'XX' end,
case when wca_other.sedol.curencd<>'' then wca_other.sedol.curencd else 'XXX' end,
wca_other.sedol.sedolid,
1 as seqnumreg,
1 as seqnumcur
from wca_other.sedol
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
where
wca_other.sedol.actflag<>'D'
and wca_other.sedol.sedol<>''
and ifnull(wca_other.sedol.cntrycd,'')<>''
and ifnull(wca_other.sedol.cntrycd,'')<>'XS'
and ifnull(wca_other.sedol.cntrycd,'')<>'ZZ'
and ifnull(wca_other.sedol.rcntrycd,'')<>'XG'
and ifnull(wca_other.sedol.rcntrycd,'')<>'XH'
and ifnull(wca_other.sedol.rcntrycd,'')<>'XZ';


call wca2.sp_sedolseqreg(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Srcntrycd, @Trcntrycd, @Tsedolid, @Tseqnumreg, @cnt, @finished);
call wca2.sp_sedolseqcur(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Scurencd, @Tcurencd, @Tsedolid, @Tseqnumcur, @cnt, @finished);

