--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_WCA_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\BBCC\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT
upper('BBCC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbccid,
wca.main.secid,
wca.main.bbcid,
wca.main.oldcntrycd,
wca.main.oldcurencd,
wca.main.effectivedate,
wca.main.newcntrycd,
wca.main.newcurencd,
wca.main.oldbbgcompid,
wca.main.newbbgcompid,
wca.main.oldbbgcomptk,
wca.main.newbbgcomptk,
wca.main.releventid,
wca.main.eventtype,
wca.main.notes
FROM wca.bbcc as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.main.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
