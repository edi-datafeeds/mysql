--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Issur
--fileheadertext=EDI_WCA_Issur_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
WHERE
wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3