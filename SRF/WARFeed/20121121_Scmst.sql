--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Scmst
--fileheadertext=EDI_WCA_Scmst_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.NoParValue,
wca.scmst.Voting,
wca.scmst.Holding,
wca.scmst.RegS144A,
wca.scmst.VotePerSec,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
WHERE
wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and wca.sectygrp.secgrpid<3
