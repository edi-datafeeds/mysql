--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Warex
--fileheadertext=EDI_WCA_Warex_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
select
upper('WAREX') as TableName,
wca.warex.Actflag,
wca.warex.AnnounceDate,
wca.warex.Acttime,
wca.warex.WarexID,
wca.warex.SecID,
wca.warex.FromDate,
wca.warex.ToDate,
wca.warex.RatioNew,
wca.warex.RatioOld,
wca.warex.CurenCD,
wca.warex.StrikePrice,
wca.warex.PricePerShare,
wca.warex.ExerSecID
from wca.warex
WHERE
wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
