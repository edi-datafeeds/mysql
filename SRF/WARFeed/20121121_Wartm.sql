--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_Wartm
--fileheadertext=EDI_WCA_Wartm_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\WARFeed\
--fieldheaders=n
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
select
upper('WARTM') as TableName,
wca.wartm.Actflag,
wca.wartm.AnnounceDate,
wca.wartm.Acttime,
wca.wartm.SecID,
wca.wartm.IssueDate,
wca.wartm.ExpirationDate,
wca.wartm.RedemptionDate,
wca.wartm.ExerciseStyle,
wca.wartm.wartmnotes as Notes
from wca.wartm
WHERE
wca.wartm.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
