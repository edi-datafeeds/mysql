-- arc=y
-- arp=n:\srf\sedolbbg\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=select concat('_SedolBBG','_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_SRF_SedolBBG_
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y

-- # 1

select * from wca2.sedolbbg;