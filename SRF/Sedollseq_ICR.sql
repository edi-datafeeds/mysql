drop table if exists wca.serialseq;
use wca;
CREATE TABLE `serialseq` (
	`EventCD` VARCHAR(3) CHARACTER SET latin1 NOT NULL DEFAULT '',
	`RdID` INT(10) NOT NULL DEFAULT '0',
	`UniqueID` INT(10) NOT NULL DEFAULT '0',
	`Seqnum` INT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`EventCD`, `RdID`, `UniqueID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
insert into wca.serialseq
select distinct
etab.EventCD,
etab.eventid as RdID,
etab.bonid as UniqueID,
1 as Seqnum
from v10s_bon as etab;
insert into wca.serialseq
select
etab.EventCD,
etab.eventid as RdID,
etab.prfid as UniqueID,
1 as seqnum
from v10s_prf as etab;
insert into wca.serialseq
select
etab.EventCD,
etab.eventid as RdID,
etab.rtsid as UniqueID,
1 as seqnum
from v10s_rts as etab;
call wca2.build_serialseq(@Srdid, @Trdid, @Seventcd, @Teventcd, @Tuniqueid, @Tseqnum, @cnt, @finished);