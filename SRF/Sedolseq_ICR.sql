drop table if exists wca2.sedolseq;
use wca2;
CREATE TABLE `sedolseq` (
  `Acttime` datetime NOT NULL,
  `SecID` int(11) NOT NULL,
  `CntryCD` char(2) NOT NULL DEFAULT '',
  `RcntryCD` char(2) NOT NULL DEFAULT '',
  `CurenCD` char(3) NOT NULL DEFAULT '',
  `SedolID` int(11) NOT NULL,
  `Seqnumreg` tinyint(4) NOT NULL DEFAULT '1',
  `Seqnumcur` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SedolID`),
  KEY `ix_sortreg` (`SecID`,`CntryCD`,`RcntryCD`,`SedolID`),
  KEY `ix_sortcur` (`SecID`,`CntryCD`,`CurenCD`,`SedolID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

use wca;
insert into wca2.sedolseq
select
wca.sedol.acttime,
wca.sedol.secid,
wca.sedol.cntrycd,
case when wca.sedol.rcntrycd<>'' then wca.sedol.rcntrycd else 'XX' end,
case when wca.sedol.curencd<>'' then wca.sedol.curencd else 'XXX' end,
wca.sedol.sedolid,
1 as seqnumreg,
1 as seqnumcur
from wca.sedol
inner join scmst on wca.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
where
wca.sedol.actflag<>'D'
and wca.sedol.sedol<>''
and ifnull(wca.sedol.cntrycd,'')<>''
and ifnull(wca.sedol.cntrycd,'')<>'XS'
and ifnull(wca.sedol.cntrycd,'')<>'ZZ'
and ifnull(wca.sedol.rcntrycd,'')<>'XG'
and ifnull(wca.sedol.rcntrycd,'')<>'XH'
and ifnull(wca.sedol.rcntrycd,'')<>'XZ';


call wca2.sp_sedolseqreg(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Srcntrycd, @Trcntrycd, @Tsedolid, @Tseqnumreg, @cnt, @finished);
call wca2.sp_sedolseqcur(@Ssecid, @Tsecid, @Scntrycd, @Tcntrycd, @Scurencd, @Tcurencd, @Tsedolid, @Tseqnumcur, @cnt, @finished);

