-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- filenameextension=.669
-- filenamesuffix=
-- headerprefix=EDI_INDUSTRY_SECTOR_669_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=
-- fieldheaders=y
-- filetidy=y
-- incremental=y
-- shownulls=n
-- zerorowchk=n

-- # 1
SELECT distinct
wca.issur.IssID,
wca.exchg.CntryCD AS ExchgCntry,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.issur.SIC,
wca.issur.CIK
FROM wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
WHERE
wca.issur.acttime >= "2019-06-19 13:56:23" AND wca.issur.acttime <= "2019-06-19 18:07:38" 
and wca.scmst.statusflag <> 'I'
and wca.scexh.liststatus <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'