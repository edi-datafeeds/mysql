-- arc=n
-- fty=n
-- arp=n:\temp\
-- hpx=EDI_WCA_BBEC_
-- dfn=n
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_BBEC_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
SELECT
upper('BBEC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbecid,
wca.main.secid,
wca.main.bbeid,
wca.main.oldexchgcd,
wca.main.oldcurencd,
wca.main.effectivedate,
wca.main.newexchgcd,
wca.main.newcurencd,
wca.main.oldbbgexhid,
wca.main.newbbgexhid,
wca.main.oldbbgexhtk,
wca.main.newbbgexhtk,
wca.main.releventid,
wca.main.eventtype,
wca.main.notes
FROM wca.bbec as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.main.acttime>(select date_sub(max(wca.tbl_opslog.acttime), interval 20 minute) from wca.tbl_opslog);
