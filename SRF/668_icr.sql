-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=SELECT DATE_FORMAT((select max(acttime) from wca.tbl_opslog), '%Y%m%d')
-- filenameextension=.668
-- filenamesuffix=
-- headerprefix=EDI_SEDOL_668_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=
-- fieldheaders=y
-- filetidy=y
-- incremental=y
-- shownulls=n
-- zerorowchk=n

-- # 1
SELECT distinct
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.sedol.secid = wca.scexh.secid
WHERE
wca.sedol.acttime > (select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.sectygrp.secgrpid<3
