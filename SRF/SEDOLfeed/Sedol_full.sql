-- arc=n
-- fsx=_SEDOL
-- hpx=EDI_WCA_SEDOL_
-- dfn=l
-- fty=y
-- fdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT(date_add((select max(feeddate)from wca.tbl_opslog where seq = 3), interval 0 day), '%Y%m%d')

-- # 1

SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.sedol.secid = wca.scexh.secid
WHERE
wca.sectygrp.secgrpid<3
and wca.sedol.actflag<>'D'
and wca.sedol.defunct<>'T'
and wca.sedol.cntrycd<>'ZZ'
and wca.sedol.cntrycd<>'XS'
