-- arc=n
-- fty=n
-- arp=n:\temp\
-- hpx=EDI_WCA_BBCC_
-- dfn=n
-- fdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- hdt=SELECT DATE_FORMAT((select max(acttime)from wca.tbl_opslog), '%Y%m%d')
-- fsx=select concat('_BBCC_',(select seq from wca.tbl_opslog order by acttime desc limit 1))
-- hsx=select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1))

-- # 1
SELECT
upper('BBCC') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbccid,
wca.main.secid,
wca.main.bbcid,
wca.main.oldcntrycd,
wca.main.oldcurencd,
wca.main.effectivedate,
wca.main.newcntrycd,
wca.main.newcurencd,
wca.main.oldbbgcompid,
wca.main.newbbgcompid,
wca.main.oldbbgcomptk,
wca.main.newbbgcomptk,
wca.main.releventid,
wca.main.eventtype,
wca.main.notes
FROM wca.bbcc as main
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.main.acttime>(select date_sub(max(wca.tbl_opslog.acttime), interval 20 minute) from wca.tbl_opslog);
