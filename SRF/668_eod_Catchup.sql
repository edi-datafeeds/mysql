-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- filenameextension=.668
-- filenamesuffix=
-- headerprefix=EDI_SEDOL_668_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1
SELECT distinct
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
left outer join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd
left outer join wca.scexh on wca.sedol.secid = wca.scexh.secid
WHERE
wca.sedol.acttime >= "2019-06-19 13:56:23" AND wca.sedol.acttime <= "2019-06-19 18:07:38" 
and wca.sectygrp.secgrpid<3
