--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.170
--suffix=
--fileheadertext=EDI_SRF_170_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\170_GB\
--fieldheaders=y
--filetidy=y
--incremental=n
--shownulls=n


--# 1
select wca.scmst.ISIN,
wca.sedol.Sedol,
wca.issur.IssuerName,
wca.agncy.RegistrarName,
wca.agncy.Add1,
wca.agncy.Add2,
wca.agncy.Add3,
wca.agncy.Add4,
wca.agncy.Add5,
wca.agncy.Add6,
wca.agncy.City,
wca.agncy.CntryCD,
wca.agncy.Website,
wca.agncy.Contact1,
wca.agncy.Tel1,
wca.agncy.Fax1,
wca.agncy.email1,
wca.agncy.Contact2,
wca.agncy.Tel2,
wca.agncy.Fax2,
wca.agncy.email2,
wca.agncy.Depository,
wca.agncy.State
from wca.cntrg
inner join wca.agncy on wca.cntrg.regid = wca.agncy.agncyid
inner join wca.scmst on wca.cntrg.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.sedol on wca.cntrg.secid = wca.sedol.secid and wca.cntrg.cntrycd = wca.sedol.cntrycd
where
wca.sedol.cntrycd='GB'
and wca.agncy.actflag<>'D'
and wca.scmst.actflag<>'D'
and wca.cntrg.actflag<>'D';