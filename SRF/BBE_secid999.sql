--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_WCA_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--archive=y
--archivepath=n:\no_cull_feeds\BBE\
--fieldheaders=y
--filetidy=n
--incremental=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT distinct
upper('BBE') as tablename,
wca.main.actflag,
wca.main.announcedate,
wca.main.acttime,
wca.main.bbeid,
wca.main.secid,
wca.main.exchgcd,
wca.main.curencd,
wca.main.bbgexhid,
wca.main.bbgexhtk
from wca.bbe as main
inner join wca.bbc on wca.main.secid = wca.bbc.secid and substring(wca.main.exchgcd,1,2) = wca.bbc.cntrycd and wca.main.curencd=wca.bbc.curencd
inner join wca.scmst on wca.main.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
WHERE
wca.scmst.secid in (select code from client.pfsecid where accid = 998 and actflag<>'D')
and wca.scmst.actflag<>'D'
and wca.main.exchgcd<>''
and wca.main.curencd<>''
