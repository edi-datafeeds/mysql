-- filenameprefix=EDI_STATIC_665_
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.665
-- filenamesuffix=
-- headerprefix=EDI_STATIC_665_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=n:\temp\
-- fieldheaders=y
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # BIG 1
SELECT distinct
upper('ISSUER') as Tablename,
wca.issur.Actflag,
wca.issur.Announcedate,
wca.issur.Acttime,
wca.issur.IssID,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.CntryofDom
FROM wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
WHERE
wca.issur.actflag<>'D'

-- # BIG 2
SELECT distinct
upper('SCMST') as Tablename,
wca.scmst.Actflag,
wca.scmst.Acttime,
wca.scmst.Announcedate,
wca.scmst.SecID,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.StructCD,
wca.scmst.SecurityDesc,
case when wca.scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.NoParValue,
wca.scmst.Voting,
wca.scmst.Holding,
wca.scmst.RegS144A,
wca.scmst.VotePerSec,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.scmst.SharesOutstanding
FROM wca.scmst
inner join wca.issur on wca.scmst.issid= wca.issur.issid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
WHERE
wca.scmst.actflag<>'D'
and wca.issur.actflag<>'D'

-- # BIG 3
SELECT distinct
upper('SEDOL') as Tablename,
wca.sedol.Actflag,
wca.sedol.Acttime,
wca.sedol.Announcedate,
wca.sedol.SedolId,
wca.sedol.SecID,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD,
wca.sedol.CurenCD
FROM wca.sedol
inner join wca.scmst on wca.sedol.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca.issur on wca.scmst.issid= wca.issur.issid
WHERE
wca.sedol.actflag<>'D'
and wca.scmst.actflag<>'D'

-- # BIG 4 
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca.issur on wca.scmst.issid= wca.issur.issid
WHERE 
wca.scexh.actflag<>'D'
and wca.scmst.actflag<>'D'
and wca.scexh.scexhid<800000

-- # BIG 5 
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca.issur on wca.scmst.issid= wca.issur.issid
WHERE 
wca.scexh.actflag<>'D'
and wca.scmst.actflag<>'D'
and wca.scexh.scexhid>=800000 and wca.scexh.scexhid<1700000

-- # BIG 6 
SELECT distinct
upper('SCEXH') as Tablename,
wca.scexh.Actflag,
wca.scexh.Acttime,
wca.scexh.Announcedate,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scexh.ExchgCD,
substring(wca.scexh.ExchgCD,1,2) as CntryCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.LocalCode,
wca.scexh.MktsgID
FROM wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and wca.sectygrp.secgrpid<3
inner join wca.issur on wca.scmst.issid= wca.issur.issid
WHERE 
wca.scexh.actflag<>'D'
and wca.scmst.actflag<>'D'
and wca.scexh.scexhid>=1700000

-- # BIG 7
SELECT
upper('EXCHG') as Tablename,
wca.exchg.Actflag,
wca.exchg.Acttime,
wca.exchg.Announcedate,
wca.exchg.ExchgCD,
wca.exchg.Exchgname,
wca.exchg.CntryCD,
wca.exchg.MIC
from wca.exchg
