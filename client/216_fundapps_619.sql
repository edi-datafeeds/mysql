--filepath=o:\upload\acc\216\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.619
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\216\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--#
select
wca.scmst.SecID,
case when wca.shoch.shochid is not null then wca.shoch.AnnounceDate 
     else null
     end as Created,
case when wca.shoch.shochid is not null then wca.shoch.Acttime
     else null
     end as Changed,
case when wca.shoch.Actflag='D' or scmst.actflag='D' then 'D'
     else ''
     end as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A' then 'A'
     when wca.scmst.Statusflag = '' then 'A' 
     when wca.scmst.Statusflag is null then 'A' 
     ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
wca.sedol.rcntrycd as RegCountry,
wca.sedol.Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus = '' then 'L'
     when wca.scexh.ListStatus = 'N' then 'L'
     else wca.scexh.ListStatus
     end as ListingStatus,
wca.scexh.ListDate,
wca.shoch.shochid as EventID,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
case when wca.shoch.shochid is not null then wca.shoch.NewSos 
     else wca.scmst.sharesoutstanding 
     end as NewSos,
shoch.ShochNotes
from wca.scmst
LEFT OUTER JOIN wca.shoch ON wca.scmst.SecID = wca.shoch.secid
inner join wca.scexh on wca.scmst.secid=wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd=wca.exchg.exchgcd
inner join wca.sedol on wca.scexh.secid = wca.sedol.secid
                    and wca.exchg.cntrycd = wca.sedol.cntrycd
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.shoch.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
    or (wca.shoch.secid is null and wca.scmst.announcedate >= (select max(feeddate) from wca.tbl_opslog where seq = 3)))
and isin in (select code from client.pfisin where accid = 216 and actflag<>'D')
and (wca.shoch.shochid is not null or wca.scmst.sharesoutstanding <> '')
and wca.sedol.actflag<>'D'
