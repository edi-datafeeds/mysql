-- filenameprefix=
-- filenamedate=yyyymmdd
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=_Prices
-- headerprefix=EDI_Prices_P04_
-- headerdate=yyyymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=n
-- archivepath=n:\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # big 1
select * from prices.lasttrade
where
isin in (select code from client.pfisin where accid=980 and actflag<>'D');