--filepath=o:\Upload\Acc\185\feed\
--filenameprefix=STANDING_FULL_
--filename=
--filenamesql=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Sutherlands_185_STANDING_FULL_yyyy-mm-dd
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\185\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select distinct
client.pfisin.code as ISIN,
wca.issur.Issuername as Issuer_Name,
wca.scmst.SecurityDesc as Description,
wca.bond.OutstandingAmount as Amount_Outstanding,
wca.bond.InterestBasis as Coupon_Type,
wca.bond.InterestRate as Coupon_Amount,
wca.bond.curencd as Currency,
case when wca.redem.redemtype = 'AMT' then 'Y'
     when wca.bond.sinkingfund<>'' then wca.bond.sinkingfund
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Sinkable,
case when wca.bond.Callable<>'' and wca.bond.Callable is not null then wca.bond.Callable 
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Callable,
case when wca.bond.Puttable<>'' and wca.bond.Puttable is not null then wca.bond.Puttable 
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Puttable,
case when wca.bond.subordinate<>'' and wca.bond.subordinate is not null then wca.bond.subordinate 
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Subordinated,
case when wca.bond.Perpetual<>'' and wca.bond.Perpetual is not null then wca.bond.Perpetual 
     when wca.bond.MaturityDate is not null then 'N'
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Perpetual,
case when wca.bond.MaturityExtendible<>'' and wca.bond.MaturityExtendible is not null then wca.bond.MaturityExtendible 
     when wca.bond.bondsrc='OC' or wca.bond.bondsrc='PE' or wca.bond.bondsrc='PR'
          or wca.bond.bondsrc='PS' or wca.bond.bondsrc='TS' then 'N'
     else '' end as Extendible,
wca.bond.InterestPaymentFrequency as Coupon_Frequency,
wca.bond.IssueAmount as Issuing_Amount,
wca.issur.cntryofincorp as Issuer_Country,
wca.bond.InterestAccrualConvention as Day_Count_Convention,
wca.bond.MaturityBenchmark as Index_Linked,
'' as Calculation_Type,
wca.bond.MaturityDate as Maturity_Date,
wca.bond.IssueDate as Issue_Date,
wca.bond.AnnounceDate as Announcement_Date,
wca.bond.SeniorJunior as Debt_Type_1,
wca.bond.Subordinate as Debt_Type_2,
wca.issur.IssID as Asset_Type_Link 
from client.pfisin
left outer join wca.scmst on client.pfisin.code = Isin
left outer join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.redem on wca.bond.secid = wca.redem.secid and 'AMT'= wca.redem.redemtype
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
where 
client.pfisin.accid  = 185
and client.pfisin.Actflag<>'D'
