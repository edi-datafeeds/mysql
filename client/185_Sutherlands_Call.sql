--filepath=o:\Upload\Acc\185\feed\
--filenameprefix=CALL_FULL_
--filename=
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Sutherlands_185_CALL_FULL_yyyy-mm-dd
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\185\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
client.pfisin.code as ISIN,
case when wca.cpopt.CPType='KO' then null
     else wca.cpopt.CPType
     end as Call_Style,
case when wca.cpopt.CPType='KO' then null
     when wca.cpopt.todate=wca.cpopt.fromdate then wca.cpopt.fromdate
     when wca.cpopt.todate is null then wca.cpopt.fromdate
     else null
     end as Next_Call_Date,
case when wca.cpopt.CPType='KO' then null
     when wca.cpopt.todate<> wca.cpopt.fromdate and wca.cpopt.todate is not null then wca.cpopt.fromdate
     else null
     end as From_Date,
case when wca.cpopt.CPType='KO' then null
     when wca.cpopt.todate<> wca.cpopt.fromdate and wca.cpopt.todate is not null then wca.cpopt.todate
     else null
     end as To_Date,
case when wca.cpopt.CPType='KO' then wca.cpopt.CPType
     else null
     end as KO_Call_Style,
case when wca.cpopt.CPType<>'KO' then null
     when wca.cpopt.todate=wca.cpopt.fromdate then wca.cpopt.fromdate
     when wca.cpopt.todate is null then wca.cpopt.fromdate
     else null
     end as KO_Next_Call_Date,
case when wca.cpopt.CPType<>'KO' then null
     when wca.cpopt.todate<> wca.cpopt.fromdate and wca.cpopt.todate is not null then wca.cpopt.fromdate
     else null
     end as KO_From_Date,
case when wca.cpopt.CPType<>'KO' then null
     when wca.cpopt.todate<> wca.cpopt.fromdate and wca.cpopt.todate is not null then wca.cpopt.todate
     else null
     end as KO_To_Date
from client.pfisin
left outer join wca.scmst on client.pfisin.code = Isin
left outer join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.cpopt on wca.scmst.secid = wca.cpopt.secid
where 
client.pfisin.accid  = 185
and client.pfisin.Actflag<>'D'
and wca.cpopt.cpoptid is not null
and wca.scmst.secid is not null
and wca.bond.callable<>'N'
order by isin
