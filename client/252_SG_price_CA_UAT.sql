-- arc=y
-- arp=n:\upload\acc\252\
-- ddt=
-- dfn=y
-- dft=n
-- dtm=select ''
-- fpx=
-- fsx=_sg_pricediv_UAT
-- fty=y
-- fex=.txt
-- dfn=L
-- hdt=
-- hpx=
-- hsx=_sg_pricediv

-- # 1
select
client.pftickmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pftickmic.MIC,
prices_aux.lasttradedate_20141231.LocalCode,
prices_aux.lasttradedate_20141231.Isin,
prices_aux.lasttradedate_20141231.Currency,
prices_aux.lasttradedate_20141231.PriceDate,
prices_aux.lasttradedate_20141231.Open,
prices_aux.lasttradedate_20141231.High,
prices_aux.lasttradedate_20141231.Low,
prices_aux.lasttradedate_20141231.Close,
prices_aux.lasttradedate_20141231.Mid,
prices_aux.lasttradedate_20141231.Ask,
prices_aux.lasttradedate_20141231.Last,
prices_aux.lasttradedate_20141231.Bid,
prices_aux.lasttradedate_20141231.BidSize,
prices_aux.lasttradedate_20141231.AskSize,
prices_aux.lasttradedate_20141231.TradedVolume,
prices_aux.lasttradedate_20141231.SecID,
prices_aux.lasttradedate_20141231.MktCloseDate,
prices_aux.lasttradedate_20141231.Volflag,
prices_aux.lasttradedate_20141231.Issuername,
prices_aux.lasttradedate_20141231.SectyCD,
prices_aux.lasttradedate_20141231.SecurityDesc,
'' as Sedol,
prices_aux.lasttradedate_20141231.uscode,
prices_aux.lasttradedate_20141231.PrimaryExchgCD,
prices_aux.lasttradedate_20141231.ExchgCD,
prices_aux.lasttradedate_20141231.TradedValue,
prices_aux.lasttradedate_20141231.TotalTrades,
prices_aux.lasttradedate_20141231.Comment
from client.pftickmic
left outer join prices_aux.lasttradedate_20141231 on client.pftickmic.code=prices_aux.lasttradedate_20141231.localcode
                                      and client.pftickmic.mic=prices_aux.lasttradedate_20141231.mic
where
((pricedate is null or
(prices_aux.lasttradedate_20141231.pricedate>(select(adddate(prices_aux.lasttradedate_20141231.mktclosedate, interval -365 day)))))
and client.pftickmic.accid=252
and client.pftickmic.actflag<>'D'
and (comment not like 'terms%' or comment is null))
union
select
client.pfisinmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pfisinmic.MIC,
prices_aux.lasttradedate_20141231.LocalCode,
prices_aux.lasttradedate_20141231.Isin,
prices_aux.lasttradedate_20141231.Currency,
prices_aux.lasttradedate_20141231.PriceDate,
prices_aux.lasttradedate_20141231.Open,
prices_aux.lasttradedate_20141231.High,
prices_aux.lasttradedate_20141231.Low,
prices_aux.lasttradedate_20141231.Close,
prices_aux.lasttradedate_20141231.Mid,
prices_aux.lasttradedate_20141231.Ask,
prices_aux.lasttradedate_20141231.Last,
prices_aux.lasttradedate_20141231.Bid,
prices_aux.lasttradedate_20141231.BidSize,
prices_aux.lasttradedate_20141231.AskSize,
prices_aux.lasttradedate_20141231.TradedVolume,
prices_aux.lasttradedate_20141231.SecID,
prices_aux.lasttradedate_20141231.MktCloseDate,
prices_aux.lasttradedate_20141231.Volflag,
prices_aux.lasttradedate_20141231.Issuername,
prices_aux.lasttradedate_20141231.SectyCD,
prices_aux.lasttradedate_20141231.SecurityDesc,
'' as Sedol,
prices_aux.lasttradedate_20141231.uscode,
prices_aux.lasttradedate_20141231.PrimaryExchgCD,
prices_aux.lasttradedate_20141231.ExchgCD,
prices_aux.lasttradedate_20141231.TradedValue,
prices_aux.lasttradedate_20141231.TotalTrades,
prices_aux.lasttradedate_20141231.Comment
from client.pfisinmic
left outer join wca.scmst on client.pfisinmic.code=wca.scmst.isin
left outer join prices_aux.lasttradedate_20141231 on wca.scmst.secid=prices_aux.lasttradedate_20141231.secid
                                      and client.pfisinmic.mic=prices_aux.lasttradedate_20141231.mic
where
((pricedate is null or
(prices_aux.lasttradedate_20141231.pricedate>(select(adddate(prices_aux.lasttradedate_20141231.mktclosedate, interval -365 day)))))
and client.pfisinmic.accid=252
and client.pfisinmic.actflag<>'D'
and (comment not like 'terms%' or comment is null))
union
select
client.pftickmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pftickmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode
                                      and client.pftickmic.mic=prices.lasttrade.mic
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and client.pftickmic.accid=252
and client.pftickmic.actflag<>'D'
and (comment not like 'terms%' or comment is null))
union
select
client.pfisinmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,
prices.lasttrade.Volflag,
prices.lasttrade.Issuername,
prices.lasttrade.SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
prices.lasttrade.PrimaryExchgCD,
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,
prices.lasttrade.TotalTrades,
prices.lasttrade.Comment
from client.pfisinmic
left outer join wca.scmst on client.pfisinmic.code=wca.scmst.isin
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.secid
                                      and client.pfisinmic.mic=prices.lasttrade.mic
where
((pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=252
and (comment not like 'terms%' or comment is null))
union
select
client.pftickmic.code as PortCode,
wca.div_my.actflag as DivActflag,
rd.recdate as DivRecdate,
wca.div_my.marker,
wca.div_my.frequency,
wca.div_my.nildividend,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as DivExdate,
case when wca.exdt.rdid is not null then wca.exdt.paydate
     else pexdt.paydate
     end as DivPaydate,
wca.divpy.divtype,
wca.divpy.curencd as divcurrency,
wca.divpy.netdividend,
wca.divpy.grossdividend,
wca.divpy.divid,
wca.divpy.optionid,
wca.divpy.optionserialno as serialid,
wca.divpy.defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pftickmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode
                                      and client.pftickmic.mic=prices.lasttrade.mic
inner join wca.rd on prices.lasttrade.secid=wca.rd.secid
inner join wca.div_my on wca.rd.rdid=wca.div_my.rdid
inner join wca.divpy on wca.div_my.divid=wca.divpy.divid and 1=wca.divpy.optionid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
where
accid=252
and (pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and (comment not like 'terms%' or comment is null)
and (grossdividend<>'' or netdividend<>'')
and approxflag<>'T'
and wca.rd.recdate>(select(adddate(now(), interval -930 day)))
union
select
client.pftickmic.code as Portcode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
wca.sd.Actflag as SplitActflag,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitExdate,
wca.sd.NewRatio as SplitRatioNew,
wca.sd.OldRatio as SplitRatioOld,
'Forward' as SplitDirection,
client.pftickmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode
                                      and client.pftickmic.mic=prices.lasttrade.mic
inner join wca.rd on prices.lasttrade.secid=wca.rd.secid
inner join wca.sd on wca.rd.rdid=wca.sd.rdid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'SD' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'SD' = pexdt.eventtype
where
(pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=252
and (comment not like 'terms%' or comment is null)
and wca.sd.rdid is not null
and wca.sd.newratio<>''
and wca.rd.recdate>(select(adddate(now(), interval -930 day)))
union
select
client.pftickmic.code as Portcode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
wca.consd.Actflag as SplitActflag,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitExdate,
wca.consd.NewRatio as SplitRatioNew,
wca.consd.OldRatio as SplitRatioOld,
'Reverse' as SplitDirection,
client.pftickmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pftickmic
left outer join prices.lasttrade on client.pftickmic.code=prices.lasttrade.localcode
                                      and client.pftickmic.mic=prices.lasttrade.mic
inner join wca.rd on prices.lasttrade.secid=wca.rd.secid
inner join wca.consd on wca.rd.rdid=wca.consd.rdid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'CONSD' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'CONSD' = pexdt.eventtype
where
(pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=252
and (comment not like 'terms%' or comment is null)
and wca.consd.rdid is not null
and wca.consd.newratio<>''
and wca.rd.recdate>(select(adddate(now(), interval -930 day)))
union
select
client.pfisinmic.code as PortCode,
wca.div_my.actflag as DivActflag,
rd.recdate as DivRecdate,
wca.div_my.marker,
wca.div_my.frequency,
wca.div_my.nildividend,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as DivExdate,
case when wca.exdt.rdid is not null then wca.exdt.paydate
     else pexdt.paydate
     end as DivPaydate,
wca.divpy.divtype,
wca.divpy.curencd as divcurrency,
wca.divpy.netdividend,
wca.divpy.grossdividend,
wca.divpy.divid,
wca.divpy.optionid,
wca.divpy.optionserialno as serialid,
wca.divpy.defaultopt,
'' as SplitActflag,
null as SplitExdate,
'' as SplitRatioNew,
'' as SplitRatioOld,
'' as SplitDirection,
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pfisinmic
left outer join wca.scmst on client.pfisinmic.code=wca.scmst.isin
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.secid
                                      and client.pfisinmic.mic=prices.lasttrade.mic
inner join wca.rd on wca.scmst.secid=wca.rd.secid
inner join wca.div_my on wca.rd.rdid=wca.div_my.rdid
inner join wca.divpy on wca.div_my.divid=wca.divpy.divid and 1=wca.divpy.optionid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
where
accid=252
and (pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and (comment not like 'terms%' or comment is null)
and (grossdividend<>'' or netdividend<>'')
and approxflag<>'T'
and wca.rd.recdate>(select(adddate(now(), interval -930 day)))
union
select
client.pfisinmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
wca.sd.Actflag as SplitActflag,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitExdate,
wca.sd.NewRatio as SplitRatioNew,
wca.sd.OldRatio as SplitRatioOld,
'Forward' as SplitDirection,
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pfisinmic
left outer join wca.scmst on client.pfisinmic.code=wca.scmst.isin
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.secid
                                      and client.pfisinmic.mic=prices.lasttrade.mic
left outer join wca.rd on wca.scmst.secid=wca.rd.secid
inner join wca.sd on wca.rd.rdid=wca.sd.rdid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'SD' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'SD' = pexdt.eventtype
where
(pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=252
and (comment not like 'terms%' or comment is null)
and wca.sd.rdid is not null
and wca.sd.newratio<>''
and wca.rd.recdate>(select(adddate(now(), interval -930 day)))
union
select
client.pfisinmic.code as PortCode,
'' as DivActflag,
null as DivRecdate,
'' as marker,
'' as frequency,
'' as nildividend,
null as DivExdate,
null as DivPaydate,
'' as divtype,
'' as divcurrency,
'' as netdividend,
'' as grossdividend,
null as divid,
null as optionid,
null as serialid,
'' as defaultopt,
wca.consd.Actflag as SplitActflag,
case when wca.exdt.rdid is not null then wca.exdt.exdate
     else pexdt.exdate
     end as SplitExdate,
wca.consd.NewRatio as SplitRatioNew,
wca.consd.OldRatio as SplitRatioOld,
'Reverse' as SplitDirection,
client.pfisinmic.MIC,
prices.lasttrade.LocalCode,
prices.lasttrade.Isin,
null as Currency,
null as PriceDate,
null as Open,
null as High,
null as Low,
null as Close,
null as Mid,
null as Ask,
null as Last,
null as Bid,
null as BidSize,
null as AskSize,
null as TradedVolume,
null as SecID,
null as MktCloseDate,
'' as Volflag,
prices.lasttrade.Issuername,
null as SectyCD,
prices.lasttrade.SecurityDesc,
'' as Sedol,
prices.lasttrade.uscode,
null as PrimaryExchgCD,
prices.lasttrade.ExchgCD,
null as TradedValue,
null as TotalTrades,
'' as Comment
from client.pfisinmic
left outer join wca.scmst on client.pfisinmic.code=wca.scmst.isin
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.secid
                                      and client.pfisinmic.mic=prices.lasttrade.mic
left outer join wca.rd on wca.scmst.secid=wca.rd.secid
inner join wca.consd on wca.rd.rdid=wca.consd.rdid
left outer join wca.exdt on rd.rdid = wca.exdt.rdid and prices.lasttrade.exchgcd = wca.exdt.exchgcd and 'CONSD' = exdt.eventtype
left outer join wca.exdt as pexdt on rd.rdid = pexdt.rdid and prices.lasttrade.exchgcd = pexdt.exchgcd and 'CONSD' = pexdt.eventtype
where
(pricedate is null or
(prices.lasttrade.pricedate>(select(adddate(prices.lasttrade.mktclosedate, interval -365 day)))))
and accid=252
and (comment not like 'terms%' or comment is null)
and wca.consd.rdid is not null
and wca.consd.newratio<>''
and wca.rd.recdate>(select(adddate(now(), interval -930 day)));
