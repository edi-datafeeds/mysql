insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USPAC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USPAC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USPAC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USPAC');
