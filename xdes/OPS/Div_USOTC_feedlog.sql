insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USOTC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USOTC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USOTC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USOTC');
