insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SARSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_SARSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'SARSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'SARSE');