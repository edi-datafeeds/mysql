insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USOTC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USOTC') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USOTC' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USOTC');