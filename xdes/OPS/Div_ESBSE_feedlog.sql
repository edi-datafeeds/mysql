insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ESBSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ESBSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ESBSE');
