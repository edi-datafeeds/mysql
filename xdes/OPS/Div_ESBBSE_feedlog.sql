insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESBBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ESBBSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ESBBSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ESBBSE');
