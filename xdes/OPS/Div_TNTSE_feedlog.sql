insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'TNTSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_TNTSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'TNTSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'TNTSE');