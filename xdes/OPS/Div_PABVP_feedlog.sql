insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PABVP' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_PABVP') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'PABVP' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'PABVP');
