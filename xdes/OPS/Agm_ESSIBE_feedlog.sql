insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESSIBE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESSIBE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESSIBE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESSIBE');