insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'EGCASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_EGCASE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'EGCASE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'EGCASE');