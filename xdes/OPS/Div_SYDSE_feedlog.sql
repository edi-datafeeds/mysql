insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SYDSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_SYDSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'SYDSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'SYDSE');