insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESMSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESMSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESMSE');