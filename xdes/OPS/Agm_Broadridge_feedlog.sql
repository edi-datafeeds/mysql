insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'BE_Broadridge' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_BE_Broadridge') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'BE_Broadridge' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'BE_Broadridge');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'FR_Broadridge' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_FR_Broadridge') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'FR_Broadridge' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'FR_Broadridge');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'NL_Broadridge' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_NL_Broadridge') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'NL_Broadridge' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'NL_Broadridge');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DE_Broadridge' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DE_Broadridge') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DE_Broadridge' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DE_Broadridge');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'RU_Broadridge' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_RU_Broadridge') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'RU_Broadridge' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'RU_Broadridge');