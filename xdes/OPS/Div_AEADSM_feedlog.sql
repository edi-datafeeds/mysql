insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEADSM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_AEADSM') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'AEADSM' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'AEADSM');