insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PSAFM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_PSAFM') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'PSAFM' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'PSAFM');