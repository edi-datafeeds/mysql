insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USAMEX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USAMEX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USAMEX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USAMEX');