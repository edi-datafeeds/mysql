insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESAIAF' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ESAIAF') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ESAIAF' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ESAIAF');
