insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PEBVL' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_PEBVL') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'PEBVL' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'PEBVL');
