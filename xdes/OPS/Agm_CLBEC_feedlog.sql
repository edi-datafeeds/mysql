insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CLBEC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_CLBEC') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'CLBEC' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'CLBEC');