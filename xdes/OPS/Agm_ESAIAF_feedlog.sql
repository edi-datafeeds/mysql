insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESAIAF' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESAIAF') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESAIAF' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESAIAF');