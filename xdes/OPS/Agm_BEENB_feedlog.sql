insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'BEENB' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_BEENB') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'BEENB' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'BEENB');