insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ARMVB' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ARMVB') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ARMVB' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ARMVB');
