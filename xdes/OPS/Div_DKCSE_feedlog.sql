insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DKCSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DKCSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DKCSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DKCSE');