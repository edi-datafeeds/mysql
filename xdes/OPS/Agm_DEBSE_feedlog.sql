insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEBSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEBSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEBSE');