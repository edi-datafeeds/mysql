insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PSAFM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_PSAFM') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'PSAFM' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'PSAFM');