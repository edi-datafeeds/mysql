insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'NLENA' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_NLENA') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'NLENA' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'NLENA');