insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEDIFX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_AEDIFX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'AEDIFX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'AEDIFX');