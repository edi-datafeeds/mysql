insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'KZKASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_KZKASE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'KZKASE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'KZKASE');