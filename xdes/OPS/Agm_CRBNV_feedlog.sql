insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CRBNV' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_CRBNV') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'CRBNV' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'CRBNV');