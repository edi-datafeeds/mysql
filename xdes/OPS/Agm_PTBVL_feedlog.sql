insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PTBVL' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_PTBVL') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'PTBVL' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'PTBVL');