insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USBATS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USBATS') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USBATS' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USBATS');

insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USNASD' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USNASD') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USNASD' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USNASD');

insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USOTC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USOTC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USOTC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USOTC');

insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USPAC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USPAC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USPAC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USPAC');

insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USNYSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USNYSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USNYSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USNYSE');