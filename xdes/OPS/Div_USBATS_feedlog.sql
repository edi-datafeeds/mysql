insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USBATS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USBATS') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USBATS' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USBATS');
