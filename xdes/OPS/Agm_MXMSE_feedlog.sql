insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'MXMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_MXMSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'MXMSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'MXMSE');