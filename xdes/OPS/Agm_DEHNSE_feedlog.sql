insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEHNSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEHNSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEHNSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEHNSE');