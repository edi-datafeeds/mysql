insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'UAPFTS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_UAPFTS') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'UAPFTS' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'UAPFTS');