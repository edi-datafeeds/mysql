insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'MABVC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_MABVC') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'MABVC' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'MABVC');