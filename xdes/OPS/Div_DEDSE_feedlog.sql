insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEDSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEDSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEDSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEDSE');