insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'UAUX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_UAUX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'UAUX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'UAUX');