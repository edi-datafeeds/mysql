insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESBSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESBSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESBSE');