insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ECQSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ECQSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ECQSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ECQSE');