insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ESMSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ESMSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ESMSE');
