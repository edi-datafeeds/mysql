insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'RUMICX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_RUMICX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'RUMICX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'RUMICX');