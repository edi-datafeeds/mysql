insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CLBEC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_CLBEC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'CLBEC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'CLBEC');
