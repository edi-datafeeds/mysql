insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESVSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESVSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESVSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESVSE');