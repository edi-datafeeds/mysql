insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'BYBCSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_BYBCSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'BYBCSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'BYBCSE');