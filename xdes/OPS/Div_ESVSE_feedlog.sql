insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESVSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ESVSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ESVSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ESVSE');
