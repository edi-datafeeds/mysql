insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'TNTSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_TNTSE') as filename,
now()
from xdes.divfeedlog
where ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'TNTSE');


insert into xdes.xdeslog(exchgcd,type,fromdate,todate,epoc,user,filename )
select 'GBLSE' as exchgcd,'Div'as type,ToDate as FRomDate,now() as ToDate,UNIX_TIMESTAMP(NOW()) as epoch,6 as userid,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_Div_GBLSE') as filename
from xdes.xdeslog
where ToDate = 
(select max(ToDate) from xdes.xdeslog where exchgcd = 'GBLSE' and type = 'div' );