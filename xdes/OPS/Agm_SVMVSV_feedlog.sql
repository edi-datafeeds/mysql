insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SVMVSV' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_SVMVSV') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'SVMVSV' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'SVMVSV');