insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'RURTS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_RURTS') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'RURTS' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'RURTS');