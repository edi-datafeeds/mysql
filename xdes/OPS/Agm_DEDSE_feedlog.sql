insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEDSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEDSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEDSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEDSE');