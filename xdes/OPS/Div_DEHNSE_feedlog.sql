insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEHNSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEHNSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEHNSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEHNSE');