insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'GBLSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_GBLSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'GBLSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'GBLSE');
