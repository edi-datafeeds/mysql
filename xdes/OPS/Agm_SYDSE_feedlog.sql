insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SYDSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_SYDSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'SYDSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'SYDSE');