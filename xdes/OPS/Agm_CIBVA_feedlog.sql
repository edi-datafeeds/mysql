insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CIBVA' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_CIBVA') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'CIBVA' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'CIBVA');