insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'GBOFX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_GBOFX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'GBOFX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'GBOFX');
