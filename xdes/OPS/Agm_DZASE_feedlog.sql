insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DZASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DZASE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DZASE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DZASE');