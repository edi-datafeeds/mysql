insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'FRPEN' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_FRPEN') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'FRPEN' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'FRPEN');