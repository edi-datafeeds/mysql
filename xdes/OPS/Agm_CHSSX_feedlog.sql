insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CHSSX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_CHSSX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'CHSSX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'CHSSX');