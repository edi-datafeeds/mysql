insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USAMEX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USAMEX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USAMEX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USAMEX');
