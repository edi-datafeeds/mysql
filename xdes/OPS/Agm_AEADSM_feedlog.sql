insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEADSM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_AEADSM') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'AEADSM' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'AEADSM');