insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'RURTS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_RURTS') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'RURTS' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'RURTS');
