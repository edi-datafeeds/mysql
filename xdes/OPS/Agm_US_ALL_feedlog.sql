insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USOTC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USOTC') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USOTC' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USOTC');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USNYSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USNYSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USNYSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USNYSE');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USPAC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USPAC') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USPAC' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USPAC');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USBATS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USBATS') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USBATS' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USBATS');

insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USNASD' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_USNASD') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'USNASD' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'USNASD');