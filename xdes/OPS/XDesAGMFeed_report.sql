-- filenameprefix=agm_full_report_
-- filenamedate=YYYYMMDD
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_AGM_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0
SELECT 
'sendflag' as f3, 'Actflag' as f3, 'AGM_ID' as f3, 'IssID' as f3, 'SecID' as f3, 
'ISIN' as f3, 'USCode' as f3, 'Sedol' as f3, 'CommonCode' as f3, 'WKN' as f3, 
'ExchgCD' as f3, 'LocalCode' as f3, 'IssuerName' as f3, 'SecurityDesc' as f3,
'AgmDate' as f3, 'AGMEGM' as f3, 'AGMNo' as f3, 'FYEDate' as f3, 'AGMTime' as f3, 
'Add1' as f3, 'Add2' as f3, 'Add3' as f3, 'Add4' as f3, 'Add5' as f3, 'Add6' as f3, 
'city' as f3, 'CntryCD' as f3, 'RdDate' as f3, 'Remarks' as f3;

-- # 1

SELECT 
	sendflag, Actflag, AGM_ID, IssID, SecID, ISIN, USCode, Sedol, CommonCode, WKN, ExchgCD, LocalCode, 
	IssuerName, SecurityDesc,AgmDate, AGMEGM, AGMNo, FYEDate, AGMTime, Add1, Add2, Add3, Add4, Add5, Add6, 
	city, CntryCD, RdDate, Remarks 
FROM xdes.agm
	WHERE acttime BETWEEN CURDATE() - INTERVAL 0 DAY  AND CURDATE() + INTERVAL 1 DAY
ORDER BY ExchgCD
