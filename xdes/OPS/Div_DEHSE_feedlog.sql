insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEHSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEHSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEHSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEHSE');