insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ECGSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ECGSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ECGSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ECGSE');