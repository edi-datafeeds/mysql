insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'KZKASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_KZKASE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'KZKASE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'KZKASE');