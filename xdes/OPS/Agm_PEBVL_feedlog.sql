insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PEBVL' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_PEBVL') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'PEBVL' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'PEBVL');