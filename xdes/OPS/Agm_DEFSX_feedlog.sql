insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEFSX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEFSX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEFSX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEFSX');