insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CLBCS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_CLBCS') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'CLBCS' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'CLBCS');