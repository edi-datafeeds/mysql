insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SARSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_SARSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'SARSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'SARSE');