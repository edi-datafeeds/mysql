insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SILSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_SILSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'SILSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'SILSE');