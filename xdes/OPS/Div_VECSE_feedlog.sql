insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'VECSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_VECSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'VECSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'VECSE');
