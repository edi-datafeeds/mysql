insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'UAUX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_UAUX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'UAUX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'UAUX');