insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'VECSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_VECSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'VECSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'VECSE');