insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ITMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ITMSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ITMSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ITMSE');
