insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'UAPFTS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_UAPFTS') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'UAPFTS' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'UAPFTS');