insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ARMVB' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ARMVB') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ARMVB' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ARMVB');