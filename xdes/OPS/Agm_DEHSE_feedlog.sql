insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEHSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEHSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEHSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEHSE');