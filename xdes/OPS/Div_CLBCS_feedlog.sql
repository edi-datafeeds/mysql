insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CLBCS' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_CLBCS') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'CLBCS' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'CLBCS');
