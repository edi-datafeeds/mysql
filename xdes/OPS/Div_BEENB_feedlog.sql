insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'BEENB' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_BEENB') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'BEENB' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'BEENB');