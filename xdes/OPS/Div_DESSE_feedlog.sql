insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DESSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DESSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DESSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DESSE');