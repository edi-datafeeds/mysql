insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'COCSX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_COCSX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'COCSX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'COCSX');