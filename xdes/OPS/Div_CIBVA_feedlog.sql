insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CIBVA' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_CIBVA') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'CIBVA' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'CIBVA');