insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEFSX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEFSX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEFSX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEFSX');