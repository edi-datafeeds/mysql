insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'USNYSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_USNYSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'USNYSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'USNYSE');