INSERT INTO xdes.tbl_feedlog(fromdate,todate,acttime, types )
SELECT ToDate AS FRomDate, now() AS ToDate, now(), 'eod'
FROM xdes.tbl_feedlog
WHERE ToDate = ( SELECT MAX(ToDate) FROM xdes.tbl_feedlog WHERE types = 'eod' ); 