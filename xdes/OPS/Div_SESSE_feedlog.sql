insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SESSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_SESSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'SESSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'SESSE');