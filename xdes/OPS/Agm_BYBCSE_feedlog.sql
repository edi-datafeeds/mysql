insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'BYBCSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_BYBCSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'BYBCSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'BYBCSE');