insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DZASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DZASE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DZASE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DZASE');