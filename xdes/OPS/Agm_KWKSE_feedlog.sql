insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'KWKSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_KWKSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'KWKSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'KWKSE');