insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEDFM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_AEDFM') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'AEDFM' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'AEDFM');