insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ECGSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_ECGSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'ECGSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'ECGSE');
