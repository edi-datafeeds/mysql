insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'NLENA' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_NLENA') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'NLENA' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'NLENA');