insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEMSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEMSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEMSE');