insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_DEBSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'DEBSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'DEBSE');