insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DESSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DESSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DESSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DESSE');