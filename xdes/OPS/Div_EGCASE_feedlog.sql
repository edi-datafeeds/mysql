insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'EGCASE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_EGCASE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'EGCASE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'EGCASE');