insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'SVMVSV' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_SVMVSV') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'SVMVSV' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'SVMVSV');
