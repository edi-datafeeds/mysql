insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEDFM' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_AEDFM') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'AEDFM' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'AEDFM');