insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'MXMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_MXMSE') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'MXMSE' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'MXMSE');
