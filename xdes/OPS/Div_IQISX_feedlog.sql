insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'IQISX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_IQISX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'IQISX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'IQISX');