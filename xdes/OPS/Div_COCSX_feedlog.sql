insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'COCSX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_COCSX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'COCSX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'COCSX');
