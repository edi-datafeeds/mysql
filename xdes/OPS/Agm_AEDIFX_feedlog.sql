insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'AEDIFX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_AEDIFX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'AEDIFX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'AEDIFX');