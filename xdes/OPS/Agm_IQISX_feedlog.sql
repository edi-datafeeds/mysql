insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'IQISX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_IQISX') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'IQISX' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'IQISX');