insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'MABVC' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_MABVC') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'MABVC' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'MABVC');