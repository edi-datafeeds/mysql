INSERT INTO xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
SELECT 'GBLSE' AS exchgcd,ToDate AS FRomDate, now() AS ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_GBLSE') AS filename,
now()
FROM xdes.agmfeedlog
WHERE exchgcd = 'GBLSE' AND ToDate = 
(select max(ToDate) FROM xdes.agmfeedlog WHERE exchgcd = 'GBLSE');
