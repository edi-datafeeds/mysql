insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'PABVP' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_PABVP') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'PABVP' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'PABVP');