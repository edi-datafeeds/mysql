insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'RUMICX' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_RUMICX') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'RUMICX' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'RUMICX');
