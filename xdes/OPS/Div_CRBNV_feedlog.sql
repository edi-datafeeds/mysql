insert into xdes.divfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'CRBNV' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_DIV_CRBNV') as filename,
now()
from xdes.divfeedlog
where exchgcd = 'CRBNV' and ToDate = 
(select max(ToDate) from xdes.divfeedlog where exchgcd = 'CRBNV');
