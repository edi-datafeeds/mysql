insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'DEMSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_DEMSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'DEMSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'DEMSE');