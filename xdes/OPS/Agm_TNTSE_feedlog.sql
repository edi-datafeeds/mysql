insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'TNTSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_TNTSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'TNTSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'TNTSE');