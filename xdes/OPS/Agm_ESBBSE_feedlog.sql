insert into xdes.agmfeedlog(exchgcd,fromdate,todate,filename,acttime )
select 'ESBBSE' as exchgcd,ToDate as FRomDate,now() as ToDate,
CONCAT(replace(CURDATE(),'-',''),'_',replace(CURTIME(),':',''),'_AGM_ESBBSE') as filename,
now()
from xdes.agmfeedlog
where exchgcd = 'ESBBSE' and ToDate = 
(select max(ToDate) from xdes.agmfeedlog where exchgcd = 'ESBBSE');