-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

Select 
uit_id,
actflag,
acttime,
isin,
us_code,
local_code,
sedol,
exchg_cd,
issuer_name,
trust_name,
security_name,
security_desc,
deposit_date,
maturity_date,
distribution_type,
distribution_frequency,
tax_class
From xdes.uit_div
Where status = 'R'
And exchg_cd='USNASD'
And acttime > CURRENT_DATE();

