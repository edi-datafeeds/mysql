-- filenameprefix=SI_COND
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_COND
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1

SELECT 
	consd_id,
	actflag,
	acttime,
	pub_date,
	cntrycd,
	issuer,
	local_code,
	us_code,
	isin,
	sedol,
	composite_global_id,
	bloomberg_composite_ticker,
	bloomberg_global_id,
	bloomberg_exchange_ticker,
	security,
	product_class,
	gross_sale,
	issued_capital,
	free_float,
	sale_value,
	sale_valume,
	total_trades,
	high_sale_price,
	low_sale_price,
	prev_sales_shares,
	cuurent_number,
	change_in_number,
	interest_change,
	change_in_average,
	days_to_cover,
	change_to_cover,
	stock_split_flag,
	new_issue_flag,
	position_shares,
	total_shares_issue,
	NIS,
	interest_ratio,
	secid
FROM xdes.sint_consd
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R'
ORDER BY acttime desc
