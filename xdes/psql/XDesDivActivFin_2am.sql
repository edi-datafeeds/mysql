-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_DIV_
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0
select
'detail_id'as f3,
'actflag'as f3,
'isin'as f3,
'uscode'as f3,
'sedol'as f3,
'commoncode'as f3,
'wkn'as f3,
'exchgcd'as f3,
'LocalCode'as f3,
'IssuerName'as f3,
'SecurityDesc'as f3,
'RecDate'as f3,
'ToDate'as f3,
'RegistrationDate'as f3,
'ExDate'as f3,
'CashPayDate'as f3,
'StockPayDate'as f3,
'DivPeriodCD'as f3,
'Frequency'as f3,
'Marker'as f3,
'DeclarationDate' as f3,
'TBAFlag'as f3,
'NilDividend'as f3,
'FYEDate'as f3,
'PeriodEndDate'as f3,
'Certdt' as f3,
'CAType'as f3,
'ActFlag'as f3,
'optionid'as f3,
'Optsrno'as f3,
'OptionType'as f3,
'Approxflag'as f3,
'DivRate'as f3,
'CurenCD'as f3,
'GrossDividend'as f3,
'TaxRate'as f3,
'Depfees'as f3,
'NetDividend'as f3,
'Equalisation'as f3,
'ResIssuer'as f3,
'ResSecurity'as f3,
'ResIsin'as f3,
'ResLocalCode'as f3,
'ResExchCD'as f3,
'RecindStockDiv'as f3,
'RecindCashDiv' as f3,
'RatioOld'as f3,
'RatioNew'as f3,
'Fractions'as f3,
'CouponNumber'as f3,
'DefaultOpt'as f3,
'OptElectionDate'as f3,
'OptElectionTime'as f3,
'FxRate'as f3,
'FxDate'as f3,
'Curcdpr'as f3,
'AnnouncedCur'as f3,
'Sduty'as f3,
'Sdutyprcnt'as f3,
'Cmfee'as f3,
'Cmfeeprcnt'as f3,
'Instpaydt'as f3,
'DripPrice'as f3,
'DripLastDate'as f3,
'DripPayDate'as f3,
'Crestdt'as f3,
'Frank'as f3,
'FrankDIv'as f3,
'UnFrankDiv'as f3,
'Captgain'as f3,
'Sec1250'as f3,
'Retcap'as f3,
'NonPIDAmt'as f3,
'PIDAmt'as f3,
'secid'as f3,
'DAPFlag' as f3,
'COMMENT'as f3;

-- # 1

SELECT DISTINCT 
	a.detail_id,
	a.actflag,
	a.isin,
	a.uscode,
	a.sedol,
	a.commoncode,
	a.wkn,
	a.exchgcd,
	a.LocalCode,
	a.IssuerName,
	a.SecurityDesc,
	a.RecDate,
	a.ToDate,
	a.RegistrationDate,
	a.ExDate,
	CASE WHEN(b.OptionType = 'S')THEN ''
		ELSE a.CashPayDate 
		END AS CashPayDate,
	CASE WHEN(b.OptionType = 'S' AND a.StockPayDate IS NOT NULL AND a.StockPayDate IS NOT NULL)THEN a.StockPayDate
	     WHEN(b.OptionType = 'S' AND a.CashPayDate IS NOT NULL AND a.CashPayDate IS NOT NULL)THEN a.CashPayDate
		ELSE '' 
		END AS StockPayDate,
	a.DivPeriodCD,
	a.Frequency,
	a.Marker,
	a.AnnouncedDate,
	a.TBAFlag,
	a.NilDividend,
	a.FYEDate,
	a.PeriodEndDate,
	CASE WHEN(b.OptionType = 'R' )THEN a.sfpdate 
		ELSE '' 
		END AS Certdt,
	a.CAType,
	b.ActFlag,
	b.optionid,
	''as Optsrno,
	b.OptionType,
	b.Approxflag,
	b.DivRate,
	b.CurenCD,
	b.GrossDividend,
	b.TaxRate,
	b.Depfees,
	b.NetDividend,
	b.Equalisation,
	b.ResIssuer,
	b.ResSecurity,
	CASE WHEN(b.OptionType = 'S' or b.OptionType = 'B')THEN a.isin
	     ELSE ''
        END AS ResIsin,	
	b.ResLocalCode,
	b.ResExchCD,
	b.RecindStockDiv,
	b.RecindCashDiv,
	b.RatioOld,
	b.RatioNew,
	b.Fractions,
	b.CouponID,
	CASE WHEN (b.DefaultOpt= 'T' or b.DefaultOpt= 'Y') THEN 'T'
	     ELSE 'F'
	END AS DefaultOpt,
	b.OptElectionDate,
	b.OptElectionTime,
	b.FxRate,
	a.fxdt,	
	b.Curcdpr,
	b.AnnouncedCur AS AnnouncedCur,
	'' AS Sduty,
	CASE WHEN(b.OptionType = 'R' )THEN a.MaxPrice 
		ELSE ''
		END AS Sdutyprcnt,
	'' AS Cmfee,
	CASE WHEN(b.OptionType = 'R' )THEN a.Quantity 
		ELSE '' 
		END AS Cmfeeprcnt,
	''AS Instpaydt,
	CASE WHEN(b.OptionType = 'S' )THEN ''
	ELSE b.DripPrice 
	END AS DripPrice,
	CASE WHEN(b.OptionType = 'R' )THEN b.OptElectionDate
		ELSE b.DripLastDate
		END AS DripLastDate,		
	CASE WHEN(b.OptionType = 'R' )THEN a.CashPayDate 
		ELSE '' 
		END AS DripPayDate,
	CASE WHEN(b.OptionType = 'R' )THEN a.snpdate 
		ELSE '' 
		END AS Crestdt,
	b.Frankflag AS Frank,
	b.FrankDIv,
	b.UnFrankDiv,
	''AS Captgain,
	''AS Sec1250,
	''AS Retcap,
	b.NonPIDAmt,
	b.PIDAmt,
	a.secid,
	a.DAP_Flag,
	REPLACE (a.DivNotes, '\n', '') AS COMMENT
FROM
	xdes.detail AS a
LEFT OUTER JOIN xdes. OPTIONS AS b ON a.Detail_ID = b.Detail_ID
	WHERE
		(
			(
                a.acttime >= CONCAT(CURDATE() - INTERVAl 1 DAY ,' 02:31:00')
        		AND 
                a.acttime <= CONCAT(CURDATE(),' 02:30:00')
			)
		OR
			(
                b.acttime >= CONCAT(CURDATE() - INTERVAl 1 DAY ,' 02:31:00')
        		AND 
				b.acttime <= CONCAT(CURDATE(),' 02:30:00')
			)
		)
		AND (b.AnnouncedCur = '' OR b.AnnouncedCur IS NULL) 
		AND a.sendflag = 'S'
		HAVING a.ExDate  >= CURDATE()  AND a.ExDate  <= CURDATE() + INTERVAL 1 DAY 
		AND a.ExchgCD like "US%"
ORDER BY a.acttime, b.acttime