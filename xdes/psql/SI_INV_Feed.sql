-- filenameprefix=SI_INV
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_INV
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1

SELECT 
	invest_id,
	actflag,
	#changed,
	pub_date,
	cntrycd,
	issuer,
	local_code,
	us_code,
	isin,
	sedol,
	composite_global_id,
	bloomberg_composite_ticker,
	bloomberg_global_id,
	bloomberg_exchange_ticker,
	security,
	pos_holder,
	bic,
	holder_net,
	inc_dec_pos,
	pos_date,
	can_date,
	prev_notification,
	gross_sales,
	secid
FROM xdes.sint_investor
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R'
ORDER BY acttime desc
