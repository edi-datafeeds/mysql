-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

Select distinct
r.ref_id as uit_id,
r.actflag,
r.acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.trust_name,
r.security_name,
r.security_desc,
r.deposit_date,
r.maturity_date,
p.distribution_type,
p.distribution_frequency,
r.tax_class

From xdes.uit_ref  as r

inner JOIN xdes.uit_py AS p on r.ref_id=p.ref_id and p.`status`='R'
WHERE
r.acttime > CURRENT_DATE()
and
r.exchg_cd = 'USNASD' AND (r.`status`='R')
and distribution_type IS not null and distribution_frequency IS NOT null
and distribution_type !='' AND distribution_frequency !=''

ORDER BY r.acttime desc

