-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

SELECT DISTINCT
r.ref_id AS uit_id,
r.actflag,
r.acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.trust_name,
r.security_name,
r.security_desc,
r.deposit_date,
r.maturity_date,
r.reference_distribution_type AS distribution_type,
r.reference_distribution_frequency AS distribution_frequency,
r.tax_class
FROM xdes.uit_ref AS r
WHERE r.acttime >= CURRENT_DATE()
-- WHERE r.acttime >= CURRENT_DATE() - INTERVAL 1 DAY AND r.acttime < CURRENT_DATE()
-- WHERE r.acttime >= '2019-11-05' AND r.acttime < '2019-11-06' 
AND r.exchg_cd = 'USNASD' AND (r.`status`='R')
ORDER BY r.acttime DESC;