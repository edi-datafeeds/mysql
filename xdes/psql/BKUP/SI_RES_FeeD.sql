-- filenameprefix=SI_RES
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_SI_RES
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 1

SELECT res_id,actflag,acttime,pub_date,cntrycd,issuer,local_code,us_code,isin,sedol,composite_global_id,bloomberg_composite_ticker,bloomberg_global_id,bloomberg_exchange_ticker,
security,start_date,end_date,notes,secid
FROM xdes.sint_restriction
where ( acttime >= (@fromdate ) and acttime < (@todate))
AND sendflag = 'R'
ORDER BY acttime desc
