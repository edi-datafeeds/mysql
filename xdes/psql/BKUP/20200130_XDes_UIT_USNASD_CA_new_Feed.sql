-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_CA_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

SELECT
r.ref_id as uit_id,
r.actflag,
case when r.acttime > p.acttime then r.acttime
     else p.acttime
     end as acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.trust_name,
r.security_name,
r.security_desc,
r.deposit_date,
r.maturity_date,
p.distribution_type,
p.distribution_frequency,
r.tax_class,
p.ex_date, 
p.rec_date,
p.pay_date, 
p.curen_cd,
p.grossdividend, 
p.netdividend,
p.announced_date, 
p.tba_flag, 
p.nil_dividend, 
p.approx_flag,
p.pay_id
From xdes.uit_ref  as r
LEFT OUTER JOIN xdes.uit_py AS p on r.ref_id=p.ref_id
WHERE
(
p.acttime >= CURRENT_DATE()
-- p.acttime > CURRENT_DATE() - INTERVAL 1 DAY AND p.acttime < CURRENT_DATE()
-- p.acttime >= '2019-11-22' AND p.acttime < '2019-11-23' 
)
AND 
r.exchg_cd = 'USNASD'
AND (p.`status`='R')
ORDER BY
                                p.acttime DESC

