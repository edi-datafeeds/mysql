-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_DIV_
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

select
'acttime' AS f3,
'detail_id'as f3,
'actflag'as f3,
'isin'as f3,
'uscode'as f3,
'sedol'as f3,
'exchgcd'as f3,
'LocalCode'as f3,
'IssuerName'as f3,
'SecurityDesc'as f3,
'RecDate'as f3,
'ExDate'as f3,
'CashPayDate'as f3,
'StockPayDate'as f3,
'DivPeriodCD'as f3,
'Frequency'as f3,
'Marker'as f3,
'DeclarationDate' as f3,
'TBAFlag'as f3,
'NilDividend'as f3,
'CAType'as f3,
'ActFlag'as f3,
'optionid'as f3,
'Optsrno'as f3,
'OptionType'as f3,
'Approxflag'as f3,
'DivRate'as f3,
'CurenCD'as f3,
'GrossDividend'as f3,
'TaxRate'as f3,
'Depfees'as f3,
'NetDividend'as f3,
'DefaultOpt'as f3,
'secid'as f3;

-- # 1
SELECT distinct
	CONCAT( LEFT(a.acttime ,10), " ", RIGHT(a.acttime ,8))  AS acttime,
	a.detail_id,
	a.actflag,
	a.isin,
	a.uscode,
	a.sedol,
	a.exchgcd,
	a.LocalCode,
	a.IssuerName,
	a.SecurityDesc,
	a.RecDate,
	a.ExDate,
	case when(b.OptionType = 'S')then ''
		ELSE a.CashPayDate 
		END AS CashPayDate,
	case when(b.OptionType = 'S' and a.StockPayDate is not NULL and a.StockPayDate is not null)then a.StockPayDate
	     when(b.OptionType = 'S' and a.CashPayDate is not NULL and a.CashPayDate is not null)then a.CashPayDate
		ELSE '' 
		END AS StockPayDate,
	a.DivPeriodCD,
	a.Frequency,
	a.Marker,
	a.AnnouncedDate,
	a.TBAFlag,
	a.NilDividend,
	a.CAType,
	b.ActFlag,
	b.optionid,
	''as Optsrno,
	b.OptionType,
	b.Approxflag,
	b.DivRate,
	b.CurenCD,
	b.GrossDividend,
	b.TaxRate,
	b.Depfees,
	b.NetDividend,
	case when (b.DefaultOpt= 'T' or b.DefaultOpt= 'Y') then 'T'
	     ELSE 'F'
	END AS DefaultOpt,
	a.secid
FROM
	xdes.detail AS a
LEFT OUTER JOIN xdes. OPTIONS AS b ON a.Detail_ID = b.Detail_ID
WHERE a.ExchgCD = @exchgcd
AND a.sendflag = 'S'
AND (
		(a.acttime >= (@fromdate ) 
		and a.acttime < (@todate  ))
OR
		(b.acttime >= (@fromdate ) 
		and b.acttime < (@todate  ))
		
	)
ORDER BY
	a.LocalCode, a.acttime
	
