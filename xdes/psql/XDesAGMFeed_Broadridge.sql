-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0
SELECT 
	'Actflag' as f3,
	'acttime' AS f3,
    'Unique Reference Number' AS f3,
    'ISIN' AS f3,
    'IssuerName' AS f3,
	'Meeting Date' AS f3,
	'Meeting Type' AS f3,
	'Meeting Time' AS f3,
	'Meeting Location' AS f3,	
    'Record Date' AS f3,
	'Announcement Date' AS f3,
    'Voting Rights (Y/N)' AS f3,
	'Market Deadline' AS f3,
	'SHARE TYPE (REGISTERED, BEARER, PB, PB)' AS f3,
	'Link 1: Meeting Announcement' AS f3,
    'Link 2: Meeting Announcement' AS f3,
	'Link 3:Proxy Card' AS f3,
    'Registrar/ Centralising / Issuer Agent Address' AS f3,
    'Postponement / Amendment / Cancellation /Reason' AS f3,
    'Comments' AS f3, 
    'Fee Related to Meeting (Y/N)' AS f3,
    'Fee Type' AS f3;

-- # 1

SELECT 
	actflag, acttime, 
    AGM_ID,
    ISIN,
    IssuerName,
	AgmDate,
	AGMEGM,
	AGMTime,
	city,	
    RdDate,
	announce_date,
    voting_rights,
	market_deadline,
	share_type,
	mt_ann1_src,
    mt_ann2,
	proxy_card ,
    registrar_add,
    post_reason,
    Comments, 
    meeting_fee,
    fee_type
FROM xdes.agm
where ( acttime >= (@fromdate ) and acttime < (@todate) and @exchgcd is not null )
AND ExchgCD = @exchgcd
AND sendflag = 'S'
ORDER BY 
acttime
