-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_DIV_
-- headerdate=yymmdd
-- datadateformat=yyyy-mm-dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=n
-- filetidy=n
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

SELECT
'detail_id'as f3,
'actflag'as f3,
'isin'as f3,
'uscode'as f3,
'sedol'as f3,
'commoncode'as f3,
'wkn'as f3,
'exchgcd'as f3,
'LocalCode'as f3,
'IssuerName'as f3,
'SecurityDesc'as f3,
'RecDate'as f3,
'ToDate'as f3,
'RegistrationDate'as f3,
'ExDate'as f3,
'CashPayDate'as f3,
'StockPayDate'as f3,
'DivPeriodCD'as f3,
'Frequency'as f3,
'Marker'as f3,
'DeclarationDate' as f3,
'TBAFlag'as f3,
'NilDividend'as f3,
'FYEDate'as f3,
'PeriodEndDate'as f3,
'Certdt' as f3,
'CAType'as f3,
'ActFlag'as f3,
'optionid'as f3,
'Optsrno'as f3,
'OptionType'as f3,
'Approxflag'as f3,
'DivRate'as f3,
'CurenCD'as f3,
'GrossDividend'as f3,
'TaxRate'as f3,
'Depfees'as f3,
'NetDividend'as f3,
'Equalisation'as f3,
'ResIssuer'as f3,
'ResSecurity'as f3,
'ResIsin'as f3,
'ResLocalCode'as f3,
'ResExchCD'as f3,
'RecindStockDiv'as f3,
'RecindCashDiv' as f3,
'RatioOld'as f3,
'RatioNew'as f3,
'Fractions'as f3,
'CouponNumber'as f3,
'DefaultOpt'as f3,
'OptElectionDate'as f3,
'OptElectionTime'as f3,
'FxRate'as f3,
'FxDate'as f3,
'Curcdpr'as f3,
'AnnouncedCur'as f3,
'Sduty'as f3,
'Sdutyprcnt'as f3,
'Cmfee'as f3,
'Cmfeeprcnt'as f3,
'Instpaydt'as f3,
'DripPrice'as f3,
'DripLastDate'as f3,
'DripPayDate'as f3,
'Crestdt'as f3,
'Frank'as f3,
'FrankDIv'as f3,
'UnFrankDiv'as f3,
'Captgain'as f3,
'Sec1250'as f3,
'Retcap'as f3,
'NonPIDAmt'as f3,
'PIDAmt'as f3,
'secid'as f3,
'DAPFlag' as f3,
'COMMENT'as f3,
'Tax Year End Date' as f3,
'Dividend Allocable to Tax Year' as f3,
'Income Dividends' as f3,
'Qualified Income Dividends' as f3,
'Short Term Capital Gains' as f3,
'Long Term Capital Gains' as f3,
'Total Capital Gains' as f3,
'Qualified Short Term Capital Gain' as f3,
'Qualified Long Term Capital Gain' as f3,
'Qualified Total Capital Gain' as f3,
'Foreign Tax Paid' as f3,
'Qualified Foreign Tax Paid' as f3,
'Section 1202 Gain' as f3,
'Collectibles(28%) Gain' as f3,
'Cash Liquidation Distribution' as f3,
'Noncash Liquidation Distribution' as f3,
'Exempt Interest Dividends' as f3,
'Percentage of AMT' as f3;

-- # 1
SELECT distinct 
	a.detail_id,
	a.actflag,
	a.isin,
	a.uscode,
	a.sedol,
	a.commoncode,
	a.wkn,
	a.exchgcd,
	a.LocalCode,
	a.IssuerName,
	a.SecurityDesc,
	a.RecDate,
	a.ToDate,
	a.RegistrationDate,
	a.ExDate,
	case when(b.OptionType = 'S')then ''
		ELSE a.CashPayDate 
		END AS CashPayDate,
	case when(b.OptionType = 'S' and a.StockPayDate is not NULL and a.StockPayDate is not null)then a.StockPayDate
	     when(b.OptionType = 'S' and a.CashPayDate is not NULL and a.CashPayDate is not null)then a.CashPayDate
		ELSE '' 
		END AS StockPayDate,
	a.DivPeriodCD,
	a.Frequency,
	a.Marker,
	a.AnnouncedDate,
	a.TBAFlag,
	a.NilDividend,
	a.FYEDate,
	a.PeriodEndDate,
	case when(b.OptionType = 'R' )then a.sfpdate 
		ELSE '' 
		END AS Certdt,
	a.CAType,
	b.ActFlag,
	b.optionid,
	''as Optsrno,
	b.OptionType,
	b.Approxflag,
	b.DivRate,
	b.CurenCD,
	b.GrossDividend,
	b.TaxRate,
	b.Depfees,
	b.NetDividend,
	b.Equalisation,
	b.ResIssuer,
	b.ResSecurity,
	case when(b.OptionType = 'S' or b.OptionType = 'B')then a.isin
	     ELSE ''
        END AS ResIsin,	
	b.ResLocalCode,
	b.ResExchCD,
	b.RecindStockDiv,
	b.RecindCashDiv,
	b.RatioOld,
	b.RatioNew,
	b.Fractions,
	b.CouponID,
	case when (b.DefaultOpt= 'T' or b.DefaultOpt= 'Y') then 'T'
	     ELSE 'F'
	END AS DefaultOpt,
	b.OptElectionDate,
	b.OptElectionTime,
	b.FxRate,
	a.fxdt,	
	b.Curcdpr,
	a.AnnouncedCur as AnnouncedCur,
	'' as Sduty,
	case when(b.OptionType = 'R' )then a.MaxPrice 
		ELSE ''
		END AS Sdutyprcnt,
	'' AS Cmfee,
	case when(b.OptionType = 'R' )then a.Quantity 
		ELSE '' 
		END as Cmfeeprcnt,
	''as Instpaydt,
	case when(b.OptionType = 'S' )then ''
	else b.DripPrice 
	end as DripPrice,
	case when(b.OptionType = 'R' )then b.OptElectionDate
		ELSE b.DripLastDate
		end as DripLastDate,		
	case when(b.OptionType = 'R' )then a.CashPayDate 
		ELSE '' 
		END AS DripPayDate,
	case when(b.OptionType = 'R' )then a.snpdate 
		ELSE '' 
		END AS Crestdt,
	b.Frankflag as Frank,
	b.FrankDIv,
	b.UnFrankDiv,
	''as Captgain,
	''as Sec1250,
	''as Retcap,
	b.NonPIDAmt,
	b.PIDAmt,
	a.secid,
	a.DAP_Flag,
	REPLACE (a.DivNotes, '\n', '') AS COMMENT,
    TAXYRENDDT,
	DIVALTAXYR,
	INCMDIV,
	QLYINCMDIV,
	SHRTTRMCG,
	LONGTRMCG,
	TOTCG,
	QLYSHRTCG,
	QLYLONGCG,
	QLYTOTCG,
	FGNTAXPD,
	QLYFGNTXPD,
	SEC1202,
	COLL28GAIN,
	CSLIQDST,
	NOCSLIQDST,
	EXMTINTDIV,
	PCTAMT
FROM
	xdes.detail AS a
LEFT OUTER JOIN xdes. OPTIONS AS b ON a.Detail_ID = b.Detail_ID
WHERE
	(
			( 
				a.acttime >= ( SELECT MAX(FromDate) FROM xdes.tbl_feedlog WHERE types = 'eod' ) 
				AND 
				a.acttime < ( SELECT MAX(ToDate) FROM xdes.tbl_feedlog WHERE types = 'eod' ) 
			)
		OR
			( 
				b.acttime >= ( SELECT MAX(FromDate) FROM xdes.tbl_feedlog WHERE types = 'eod' ) 
				AND 
				b.acttime < ( SELECT MAX(ToDate) FROM xdes.tbl_feedlog WHERE types = 'eod' ) 
			)
		
	)
	AND (b.AnnouncedCur = '' or b.AnnouncedCur is null)
	AND a.ExchgCD = @exchgcd
	AND a.sendflag = 'S'
	ORDER BY a.acttime,	b.acttime