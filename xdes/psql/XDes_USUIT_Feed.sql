-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

Select
p.pay_id as uit_id,
r.actflag,
case when r.acttime > p.acttime then r.acttime
     else p.acttime
     end as acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.security_desc,
r.cntry_of_Incorp,
p.tba_flag,
p.nil_dividend,
p.rec_date,
p.ex_date,
p.pay_date,
p.fye_date,
p.registration_date,
p.announced_date,
p.unit_type,
p.distribution_type,
r.umbrella_name,
p.distribution_frequency,
p.source,
p.uit_notes,
p.edi_notes,
p.approx_flag,
p.curen_cd,
p.group1_gross,
p.group1_net,
p.group2_gross,
p.group2_net,
p.franked,
p.unfranked,
p.app_div_amt,
p.tax_rate,
p.equalisation 
From xdes.uit_ref  as r
LEFT OUTER JOIN xdes.uit_py AS p on r.ref_id=p.ref_id
WHERE
(
p.acttime > '2018-03-01'
#CURRENT_DATE()
)
AND 
r.exchg_cd = 'USNASD'
AND (p.`status`='R')
ORDER BY
                                p.acttime DESC