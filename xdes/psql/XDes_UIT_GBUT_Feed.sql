-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_GBUT_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

Select 
uit_id,
actflag,
acttime,
isin,
us_code,
local_code,
sedol,
exchg_cd,
issuer_name,
security_desc,
cntry_of_Incorp,
tba_flag,
nil_dividend,
rec_date,
ex_date,
pay_date,
fye_date,
registration_date,
announced_date,
unit_type,
distribution_type,
umbrella_name,
distribution_frequency,
source,
uit_notes,
edi_notes,
approx_flag,
curen_cd,
group1_gross,
group1_net,
group2_gross,
group2_net,
franked,
unfranked,
app_div_amt,
tax_rate,
equalisation 
From xdes.uit_div
Where status = 'R'
And exchg_cd='GBUT'
And acttime > CURRENT_DATE();
