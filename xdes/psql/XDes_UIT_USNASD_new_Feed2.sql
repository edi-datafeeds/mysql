-- filenameprefix=
-- filenamedate=
-- filenamealt=
-- filenameextension=.txt
-- filenamesuffix=
-- headerprefix=EDI_UIT_USNASD_
-- headerdate=yymmdd
-- datadateformat=yyyy/mm/dd
-- datatimeformat=
-- forcetime=n
-- footertext=EDI_ENDOFFILE
-- fieldseparator=	
-- archive=y
-- archivepath=N:\No_Cull_Feeds\xdesfeed\
-- fieldheaders=y
-- filetidy=y
-- incremental=n
-- shownulls=n
-- zerorowchk=n

-- # 0

Select distinct
r.ref_id as uit_id,
r.actflag,
#case when r.acttime > p.acttime then r.acttime
	#else p.acttime
     #end as acttime,
r.acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.trust_name,
r.security_name,
r.security_desc,
r.deposit_date,
r.maturity_date,
p.distribution_type,
p.distribution_frequency,
r.tax_class
From xdes.uit_ref  as r
LEFT OUTER JOIN xdes.uit_py AS p on r.ref_id=p.ref_id
WHERE
r.trust_name like  'smart trust%' 
and r.isin NOT IN ('US83183F1012','US83183F1194','US83183F1277','US83183F1350','US83191A1025','US83191A1108','US83191A1280','US83191A1363','US83190Q1085','US83190Q1168','US83190Q1242','US83190Q1325','US83191D1063','US83191D1147','US83191D1220','US83191D1303','US83183A1025','US83183A1108','US83183A1280','US83183A1363','US83190Y1010','US83190Y1192','US83190Y1275','US83190Y1358','US83191E1047','US83191E1120','US83191E1203','US83191E1385','US83183D1063','US83183D1147','US83183D1220','US83183D1303','US83183R1059','US83183R1133','US83183R1216','US83183R1398','US83183B1008','US83183B1180','US83183B1263','US83183B1347')
#And r.acttime > CURRENT_DATE() -3
AND r.exchg_cd = 'USNASD' AND (r.`status`='R')
union
Select distinct
r.ref_id as uit_id,
r.actflag,
#case when r.acttime > p.acttime then r.acttime
	#else p.acttime
     #end as acttime,
r.acttime,
r.isin,
r.us_code,
r.local_code,
r.sedol,
r.exchg_cd,
r.issuer_name,
r.trust_name,
r.security_name,
r.security_desc,
r.deposit_date,
r.maturity_date,
p.distribution_type,
p.distribution_frequency,
r.tax_class
From xdes.uit_ref  as r
LEFT OUTER JOIN xdes.uit_py AS p on r.ref_id=p.ref_id
WHERE r.acttime > CURRENT_DATE() 
AND r.exchg_cd = 'USNASD' AND (r.`status`='R')

ORDER BY acttime DESC, acttime DESC
