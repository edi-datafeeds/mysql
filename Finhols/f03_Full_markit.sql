--filepath=o:\Datafeed\Finhols\f03_markit\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.F03
--suffix=
--fileheadertext=EDI_FINHOLS_BESPOKE_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\finhols\F03\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select
finhols.pubhol_holidays.hol_id,
finhols.pubhol_country.country_desc as countryname ,
finhols.pubhol_institution.description as institutionname,
finhols.pubhol_exch_map.exchgcd,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
     when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')
     else upper('exc')
     end as insttypecd,
finhols.pubhol_institution.gmt_offset,
finhols.pubhol_institution.set_det,
date_format(finhols.pubhol_holidays.hol_date,'%d %b %y') as holidaydate,
finhols.pubhol_holidays.hol_type,
finhols.pubhol_holidays.hol_name as holidayname,
finhols.pubhol_open_hrs.daysofweek as openingdays,
finhols.pubhol_open_hrs.openhrs,
finhols.pubhol_open_hrs.closehrs
from ((finhols.pubhol_country
inner join finhols.pubhol_institution on finhols.pubhol_country.country = finhols.pubhol_institution.country)
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code)
inner join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
inner join finhols.pubhol_exch_map on finhols.pubhol_holidays.fin_code = finhols.pubhol_exch_map.fin_code
where
exchgcd <> ''
and exchgcd is not null
and finhols.pubhol_holidays.hol_date  between '2008/08/01' and '2010/08/01'
and (finhols.pubhol_institution.country = 'at'
or finhols.pubhol_institution.country = 'be'
or finhols.pubhol_institution.country = 'bg'
or finhols.pubhol_institution.country = 'ch'
or finhols.pubhol_institution.country = 'cy'
or finhols.pubhol_institution.country = 'cz'
or finhols.pubhol_institution.country = 'de'
or finhols.pubhol_institution.country = 'dk'
or finhols.pubhol_institution.country = 'ee'
or finhols.pubhol_institution.country = 'es'
or finhols.pubhol_institution.country = 'fi'
or finhols.pubhol_institution.country = 'fr'
or finhols.pubhol_institution.country = 'uk'
or finhols.pubhol_institution.country = 'gr'
or finhols.pubhol_institution.country = 'hu'
or finhols.pubhol_institution.country = 'ie'
or finhols.pubhol_institution.country = 'is'
or finhols.pubhol_institution.country = 'it'
or finhols.pubhol_institution.country = 'lt'
or finhols.pubhol_institution.country = 'lu'
or finhols.pubhol_institution.country = 'lv'
or finhols.pubhol_institution.country = 'mt'
or finhols.pubhol_institution.country = 'nl'
or finhols.pubhol_institution.country = 'no'
or finhols.pubhol_institution.country = 'pl'
or finhols.pubhol_institution.country = 'pt'
or finhols.pubhol_institution.country = 'ro'
or finhols.pubhol_institution.country = 'se'
or finhols.pubhol_institution.country = 'si'
or finhols.pubhol_institution.country = 'sk')
order by finhols.pubhol_country.country_desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc

