--filepath=o:\Datafeed\Finhols\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.F03
--suffix=_November_2011
--fileheadertext=EDI_FINHOLS_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Finhols\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select
finhols.pubhol_country.country_desc as countryname,
finhols.pubhol_institution.description as institutionname,
finhols.pubhol_holidays.fin_code,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
     when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')
     else upper('exc')
     end as insttypecd,
finhols.pubhol_institution.gmt_offset,
finhols.pubhol_institution.set_det,
date_format(finhols.pubhol_holidays.hol_date,'%m %b %y') as holidaydate,
finhols.pubhol_holidays.hol_type,
case when finhols.pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') then '' else finhols.pubhol_holidays.hol_name end as holidayname,
case when finhols.pubhol_open_hrs.daysofweek is null or upper(finhols.pubhol_open_hrs.daysofweek='NULL') then '' else finhols.pubhol_open_hrs.daysofweek end as openingdays,
case when finhols.pubhol_open_hrs.openhrs is null or upper(finhols.pubhol_open_hrs.openhrs='NULL') then '' else finhols.pubhol_open_hrs.openhrs end as openhrs,
case when finhols.pubhol_open_hrs.closehrs is null or upper(finhols.pubhol_open_hrs.closehrs='NULL') then '' else finhols.pubhol_holidays.hol_name end as closehrs,
finhols.pubhol_open_hrs.notes as openingnotes,
finhols.pubhol_holidays.notes as holidaynotes,
finhols.pubhol_institution.notes as institutionnotes
from finhols.pubhol_country
inner join finhols.pubhol_institution on finhols.pubhol_country.country = finhols.pubhol_institution.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
inner join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
where (finhols.pubhol_holidays.hol_date between '2011-11-01' and '2011-11-30')
and finhols.pubhol_holidays.actflag<>'D'
order by finhols.pubhol_country.country desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc

