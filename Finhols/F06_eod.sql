-- arc=y
-- arp=n:\no_cull_feeds\finhols\f06\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.F06
-- fpx=
-- fsx=
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_FINHOLS_F06_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select distinct
finhols.pubhol_holidays.Hol_ID,
finhols.pubhol_holidays.acttime,
finhols.pubhol_holidays.actflag,
date_format(finhols.pubhol_holidays.hol_date,'%Y-%m-%d') as Holiday_Date,
DAYNAME(finhols.pubhol_holidays.hol_date) As Day_of_Week,
case when finhols.pubhol_holidays.hol_type = 'f' 
     then 'full' 
     when finhols.pubhol_holidays.hol_type = 'p' 
     then 'partial' 
     when finhols.pubhol_holidays.hol_type = 'w' 
     then 'weekend' 
     end as Hol_Type,
case when finhols.pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') 
     then '' 
     else finhols.pubhol_holidays.hol_name 
     end as Hol_Name, 
finhols.pubhol_institution.description  as Institution_Name,
finhols.pubhol_country.country_desc as Country, 
finhols.pubhol_country.country as Country_2_Code, 
finhols.pubhol_country.Country_3_Code, 
finhols.pubhol_country.Country_Numeric, 
finhols.pubhol_Country.Currency_Code, 
finhols.pubhol_Country.Currency_Numeric, 
finhols.pubhol_Country.Currency,
finhols.pubhol_Country.Region, 
finhols.pubhol_institution.Mic, 
finhols.pubhol_institution.Operating_Mic, 
finhols.pubhol_institution.OS_Flag, 
finhols.pubhol_institution.Location_Code, 
finhols.pubhol_institution.Fin_Code,
finhols.pubhol_institution.Fin_Type,
finhols.pubhol_institution.SET_DET, 
finhols.pubhol_institution.gmt_offset as UTC_Offset, 
finhols.pubhol_institution.Website, 
finhols.pubhol_institution.notes as Institution_Notes
from finhols.pubhol_institution
inner join finhols.pubhol_country on finhols.pubhol_institution.country = finhols.pubhol_country.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
where 
((finhols.pubhol_holidays.acttime > (select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 3)
and finhols.pubhol_holidays.hol_date>date_sub(now(), interval 40 day) and finhols.pubhol_holidays.hol_date<=now()));