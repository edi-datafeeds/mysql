-- arc=y
-- arp=n:\no_cull_feeds\finhols\TradingHrs\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_TradingHours
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_FINHOLS_TradingHours_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1 

SELECT DISTINCT
i.Fin_Code ,
i.Fin_Type,
i.COUNTRY,
c.Currency,
c.REGION,
i.DESCRIPTION as  Institution_Name,
i.MIC,
i.Gmt_Offset,
i.Time_Zone,
o.DaysOfWeek as Opening_Days,
o.OpenHrs as  Open_Hrs, 
o.CloseHrs as Close_Hrs,
o.Notes as  Opening_Notes
from finhols.pubhol_open_hrs as o
INNER JOIN finhols.pubhol_institution As i on o.Fin_Code=i.Fin_Code
INNER JOIN finhols.pubhol_country As c on i.country=c.country
where o.ActFlag !='D' and i.ActFlag !='D'
ORDER BY i.DESCRIPTION  asc
