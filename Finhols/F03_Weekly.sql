--filepath=o:\Datafeed\Finhols\f03\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.F03
--suffix=
--fileheadertext=EDI_FINHOLS_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\finhols\F03\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select
finhols.pubhol_country.country_desc as CountryName,
finhols.pubhol_institution.description as InstitutionName,
finhols.pubhol_holidays.FIN_CODE,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
     when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')
     else upper('exc')
     end as InstTypeCD,
finhols.pubhol_institution.Gmt_Offset,
finhols.pubhol_institution.SET_DET,
date_format(finhols.pubhol_holidays.hol_date,'%d %b %y') as HolidayDate,
finhols.pubhol_holidays.Hol_Type,
case when finhols.pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') then '' else finhols.pubhol_holidays.hol_name end as HolidayName,
case when finhols.pubhol_open_hrs.daysofweek is null or upper(finhols.pubhol_open_hrs.daysofweek='NULL') then '' else finhols.pubhol_open_hrs.daysofweek end as OpeningDays,
case when finhols.pubhol_open_hrs.openhrs is null or upper(finhols.pubhol_open_hrs.openhrs='NULL') then '' else finhols.pubhol_open_hrs.openhrs end as OpenHrs,
case when finhols.pubhol_open_hrs.closehrs is null or upper(finhols.pubhol_open_hrs.closehrs='NULL') then '' else finhols.pubhol_open_hrs.closehrs end as CloseHrs,
finhols.pubhol_open_hrs.notes as OpeningNotes,
finhols.pubhol_holidays.notes as HolidayNotes,
finhols.pubhol_institution.notes as InstitutionNOTES
from finhols.pubhol_country 
inner join finhols.pubhol_institution on finhols.pubhol_country.country = finhols.pubhol_institution.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
inner join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
where (finhols.pubhol_holidays.hol_date between adddate(curdate(),+2) and adddate(curdate(),+29))
and (finhols.pubhol_holidays.actflag<>'D' or finhols.pubhol_holidays.acttime>adddate(curdate(),-8))
order by finhols.pubhol_country.country_desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc

