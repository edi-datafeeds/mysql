--filepath=o:\Auto\scripts\mysql\finhols\f10_Holidays_Morning.sql
--filenameprefix=HOLIDAYS_
--filename=yyyymmdd
--filenamealt=
--fileextension=.F10
--suffix=
--fileheadertext=EDI_FINHOLS_F10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\finhols\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
finhols.pubhol_holidays.hol_id,
date_format(finhols.pubhol_holidays.hol_date,'%m %b %y') as holidaydate,
finhols.pubhol_holidays.hol_date,
finhols.pubhol_holidays.fin_code,
finhols.pubhol_holidays.hol_type,
finhols.pubhol_holidays.notes,
finhols.pubhol_holidays.hol_name,
finhols.pubhol_holidays.acttime,
finhols.pubhol_holidays.actflag
from finhols.pubhol_holidays
where
finhols.pubhol_holidays.fin_code like '%bank%'
and acttime>=(select max(fromdate) from finhols.feedlog where feedfreq = 'morning')
and acttime<(select max(todate) from finhols.feedlog where feedfreq = 'morning')
