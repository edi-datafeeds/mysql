--filepath=o:\AUTO\Script\mysql\Finhols\F10_Institution_morning.sql\
--filenameprefix=INSTITUTION_
--filename=yyyymmdd
--filenamealt=
--fileextension=.F10
--suffix=
--fileheadertext=EDI_FINHOLS_F10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\finhols\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
finhols.pubhol_institution.country,
finhols.pubhol_institution.fin_code,
finhols.pubhol_institution.description,
finhols.pubhol_institution.set_det,
finhols.pubhol_institution.notes,
finhols.pubhol_institution.gmt_offset,
finhols.pubhol_institution.acttime,
finhols.pubhol_institution.actflag
from
finhols.pubhol_institution
where
finhols.pubhol_institution.fin_code like '%bank%'
and acttime>=(select max(fromdate) from finhols.feedlog where feedfreq = 'morning')
and acttime<(select max(todate) from finhols.feedlog where feedfreq = 'morning')
