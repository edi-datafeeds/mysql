--filepath=o:\Auto\Scripts\mysql\Finhols\F10_OpenHrs_Morning.sql\
--filenameprefix=OPENHRS_
--filename=yyyymmdd
--filenamealt=
--fileextension=.F10
--suffix=
--fileheadertext=EDI_FINHOLS_F10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\finhols\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
finhols.pubhol_open_hrs.opn_id,
finhols.pubhol_open_hrs.acttime,
finhols.pubhol_open_hrs.actflag,
finhols.pubhol_open_hrs.fin_code,
-- description,
finhols.pubhol_open_hrs.daysofweek,
finhols.pubhol_open_hrs.openhrs,
finhols.pubhol_open_hrs.closehrs,
finhols.pubhol_open_hrs.notes
from finhols.pubhol_open_hrs
where
finhols.pubhol_open_hrs.fin_code like '%bank%'
and acttime>=(select max(fromdate) from finhols.feedlog where feedfreq = 'morning')
and acttime<(select max(todate) from finhols.feedlog where feedfreq = 'morning')