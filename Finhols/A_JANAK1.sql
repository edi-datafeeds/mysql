--filepath=o:\AUTO\Scripts\mysql\finhols\A_JANAK1.sql\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.hol
--suffix=
--fileheadertext=EDI_Finhols_
--fileheaderdate=YYYYMMDD
--datadateformat=YYYY/MM/DD
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Finhols\
--fieldheaders=
--filetidy=y
--fwoffsets=
--sevent=
--shownulls=


--# 1
select finhols.pubhol_holidays.hol_date as HolidayDate,
finhols.pubhol_holidays.notes as HolidayNotes,
finhols.pubhol_holidays.Hol_Name,
finhols.pubhol_institution.Country,
finhols.pubhol_institution.Description,
finhols.pubhol_institution.notes as instit_notes
from finhols.pubhol_holidays
inner join finhols.pubhol_institution on finhols.pubhol_holidays.fin_code = finhols.pubhol_institution.fin_code
where (finhols.pubhol_holidays.hol_Date between adddate(curdate(),+1) and adddate(curdate(),+100))
and upper(finhols.pubhol_institution.description) like '%STOCK EXCHANGE%'
and (finhols.pubhol_holidays.actflag<>'D' or finhols.pubhol_holidays.acttime>adddate(curdate(),-8))
ORDER BY finhols.pubhol_holidays.hol_date;