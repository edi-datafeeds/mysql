-- arc=y
-- arp=n:\bespoke\volkswagen\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(44)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq=3), '%Y%m%d')
-- fex=.csv
-- fpx=
-- fsx=_FinHols
-- fty=y
-- hdt=SELECT ''
-- hpx=
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=select ''

-- # 1

select distinct
finhols.pubhol_institution.Fin_Code as FinancialCenterID,
finhols.pubhol_Country.Currency_Code as ISOCurrencyCode,
finhols.pubhol_country.country as ISOCountryCode,
finhols.pubhol_institution.City as FinancialCentre,	
finhols.pubhol_institution.Location_Code as 'UN/LOCODE',
substring(date_format(finhols.pubhol_holidays.hol_date,'%Y-%m-%d'),1,4) as EventYear,
date_format(finhols.pubhol_holidays.hol_date,'%Y%m%d') as EventDate,
DAYNAME(finhols.pubhol_holidays.hol_date) As EventDayOfWeek,
case when finhols.pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') 
     then '' 
     else concat(finhols.pubhol_holidays.hol_name, ' ', finhols.pubhol_country.country)  
     end as EventName,
finhols.pubhol_institution.Fin_Type as FileType
from finhols.pubhol_institution
inner join finhols.pubhol_country on finhols.pubhol_institution.country = finhols.pubhol_country.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
where 
(finhols.pubhol_holidays.acttime > (select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 3)
  or finhols.pubhol_institution.acttime > (select max(wca.tbl_opslog.Feeddate) from wca.tbl_opslog where seq = 3))
and finhols.pubhol_holidays.hol_date between now() and date_add(now(), interval 365 day) 
and (finhols.pubhol_institution.Fin_Type='BNK' or finhols.pubhol_institution.Fin_Type='BCL' or finhols.pubhol_institution.Fin_Type='CUR')
order by finhols.pubhol_country.country_desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc