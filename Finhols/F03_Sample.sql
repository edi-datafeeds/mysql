--filepath=o:\Datafeed\Finhols\f03_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.F03
--suffix=
--fileheadertext=EDI_FINHOLS_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyymmdd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Finhols\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select distinct
finhols.pubhol_country.country_desc as countryname,
finhols.pubhol_institution.description as institutionname,
finhols.pubhol_holidays.fin_code,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk')
     when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
     when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')
     else upper('exc')
     end as insttypecd,
finhols.pubhol_institution.gmt_offset,
finhols.pubhol_institution.set_det,
finhols.pubhol_holidays.hol_date as holidaydate,
finhols.pubhol_holidays.hol_type,
case when finhols.pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') then '' else finhols.pubhol_holidays.hol_name end as holidayname,
case when finhols.pubhol_open_hrs.daysofweek is null or upper(finhols.pubhol_open_hrs.daysofweek='NULL') then '' else finhols.pubhol_open_hrs.daysofweek end as openingdays,
case when finhols.pubhol_open_hrs.openhrs is null or upper(finhols.pubhol_open_hrs.openhrs='NULL') then '' else finhols.pubhol_open_hrs.openhrs end as openhrs,
case when finhols.pubhol_open_hrs.closehrs is null or upper(finhols.pubhol_open_hrs.closehrs='NULL') then '' else finhols.pubhol_holidays.hol_name end as closehrs,
finhols.pubhol_open_hrs.notes as openingnotes,
finhols.pubhol_holidays.notes as holidaynotes,
finhols.pubhol_institution.notes as institutionnotes
from finhols.pubhol_country
inner join finhols.pubhol_institution on finhols.pubhol_country.country = finhols.pubhol_institution.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
inner join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
where 
/*finhols.pubhol_holidays.hol_date > '2010/12/31' and finhols.pubhol_holidays.hol_date < '2013/01/01'*/
/*and (finhols.pubhol_country.country = 'DE' or finhols.pubhol_country.country = 'IT')*/
(finhols.pubhol_holidays.acttime > curdate()
  or finhols.pubhol_institution.acttime > curdate()
  or finhols.pubhol_open_hrs.acttime > curdate()) */
order by finhols.pubhol_country.country_desc, finhols.pubhol_institution.description asc, finhols.pubhol_holidays.hol_date asc;

