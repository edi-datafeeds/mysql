--filepath=o:\Datafeed\Finhols\f06\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.F06
--suffix=
--fileheadertext=EDI_FINHOLS_F06_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\finhols\F06\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select distinct
hol_id,date_format(finhols.pubhol_holidays.hol_date,'%Y-%m-%d') as holiday_date,DAYNAME(finhols.pubhol_holidays.hol_date) As day_of_week,
case when hol_type = 'f' then	'full' when hol_type = 'p' then	'partial' when hol_type = 'w' then 'weekend' end as hol_type,
case when pubhol_holidays.hol_name is null or upper(finhols.pubhol_holidays.hol_name='NULL') then '' else finhols.pubhol_holidays.hol_name end as hol_name, 
pubhol_institution.description  as institution_name,
case when pubhol_open_hrs.DaysOfWeek is null or upper(pubhol_open_hrs.DaysOfWeek='NULL') then '' else pubhol_open_hrs.DaysOfWeek end as opening_days,
case when pubhol_open_hrs.openhrs is null or upper(finhols.pubhol_open_hrs.openhrs='NULL') then '' else finhols.pubhol_open_hrs.openhrs end as open_hrs,
case when pubhol_open_hrs.closehrs is null or upper(finhols.pubhol_open_hrs.closehrs='NULL') then '' else finhols.pubhol_open_hrs.closehrs end as close_hrs,
pubhol_open_hrs.notes as opening_notes,
pubhol_country.country_desc as country, pubhol_country.country as country_2_code, country_3_code, country_numeric, currency_code, currency_numeric, currency,
region, mic, operating_mic, os_flag, location_code, pubhol_institution.fin_code,
case when finhols.pubhol_holidays.fin_code like '%bank' then upper('bnk') when finhols.pubhol_holidays.fin_code like '%banks' then upper('bnk') when finhols.pubhol_holidays.fin_code like '%bankcl' then upper('bcl')     when finhols.pubhol_holidays.fin_code like '%cl' then upper('xcl')     else upper('exc')     end as fin_type,
set_det, gmt_offset as utc_offset, website, pubhol_institution.notes as institution_notes
from finhols.pubhol_institution
inner join finhols.pubhol_country on finhols.pubhol_institution.country = finhols.pubhol_country.country
inner join finhols.pubhol_holidays on finhols.pubhol_institution.fin_code = finhols.pubhol_holidays.fin_code
left outer join finhols.pubhol_open_hrs on finhols.pubhol_holidays.fin_code = finhols.pubhol_open_hrs.fin_code
where 	pubhol_institution.actflag <> 'd' and pubhol_holidays.actflag <> 'd'
and (pubhol_holidays.hol_date between adddate(curdate(),+2) and adddate(curdate(),+29))
and (pubhol_holidays.actflag<>'D' or pubhol_holidays.acttime>adddate(curdate(),-8))
order by pubhol_country.country_desc, pubhol_institution.description asc, pubhol_holidays.hol_date asc