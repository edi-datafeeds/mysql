SELECT table_name AS `Table`,

       index_name AS `Index`,

       GROUP_CONCAT(column_name ORDER BY seq_in_index) AS `Columns`

FROM information_schema.statistics

WHERE table_schema = 'prices'

GROUP BY 1,2;