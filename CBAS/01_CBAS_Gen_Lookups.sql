use prices;
DROP TABLE IF EXISTS cbas_rec;
CREATE TABLE `cbas_rec` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(1) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_rec select 'I', '2019-01-01 15:00:00', 'A', 'Apply';
insert ignore into prices.cbas_rec select 'I', '2019-01-01 15:00:00', 'P', 'Pending';
insert ignore into prices.cbas_rec select 'I', '2019-01-01 15:00:00', 'R', 'Rescind';

use prices;
DROP TABLE IF EXISTS cbas_pay;
CREATE TABLE `cbas_pay` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(1) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_pay select 'I', '2019-01-01 15:00:00', 'C', 'Cash';
insert ignore into prices.cbas_pay select 'I', '2019-01-01 15:00:00', 'D', 'Dissenter Rights';
insert ignore into prices.cbas_pay select 'I', '2019-01-01 15:00:00', 'S', 'Stock';
insert ignore into prices.cbas_pay select 'I', '2019-01-01 15:00:00', 'T', 'To Be Announced';

use prices;
DROP TABLE IF EXISTS cbas_frac;
CREATE TABLE `cbas_frac` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(1) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_frac select 'I', '2019-01-01 15:00:00', ' ', 'Blank';
insert ignore into prices.cbas_frac select 'I', '2019-01-01 15:00:00', 'C', 'Cash';
insert ignore into prices.cbas_frac select 'I', '2019-01-01 15:00:00', 'D', 'Round Down';
insert ignore into prices.cbas_frac select 'I', '2019-01-01 15:00:00', 'F', 'Fractions';
insert ignore into prices.cbas_frac select 'I', '2019-01-01 15:00:00', 'U', 'Round Up';

use prices;
DROP TABLE IF EXISTS cbas_err;
CREATE TABLE `cbas_err` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(4) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0000', 'No Errors';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0001', 'Stock does not exist';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0002', 'Stock not traded since event';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0004', 'Issue does not exist';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0008', 'Issue not traded since event';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0010', 'Option issue';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0020', 'No dividend price found for DRIP';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0040', 'Issue not traded since ex-date, using previous close';
insert ignore into prices.cbas_err select 'I', '2019-01-01 15:00:00', '0080', 'Not used';

use prices;
DROP TABLE IF EXISTS cbas_par;
CREATE TABLE `cbas_par` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(1) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_par select 'I', '2019-01-01 15:00:00', 'O', 'Old';
insert ignore into prices.cbas_par select 'I', '2019-01-01 15:00:00', 'N', 'New';
insert ignore into prices.cbas_par select 'I', '2019-01-01 15:00:00', 'C', 'Cash';

use prices;
DROP TABLE IF EXISTS cbas_tax;
CREATE TABLE `cbas_tax` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(1) NOT NULL,
  `Lookup` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_tax select 'I', '2019-01-01 15:00:00', ' ', 'Blank';
insert ignore into prices.cbas_tax select 'I', '2019-01-01 15:00:00', 'F', 'Tax-free';
insert ignore into prices.cbas_tax select 'I', '2019-01-01 15:00:00', 'N', 'Tax-none';
insert ignore into prices.cbas_tax select 'I', '2019-01-01 15:00:00', 'T', 'Taxable';

use prices;
DROP TABLE IF EXISTS cbas_secty;
CREATE TABLE `cbas_secty` (
  `RecCD` char(3) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` varchar(3) NOT NULL,
  `Lookup` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_secty
select distinct
wca.secty.Actflag,
wca.secty.Acttime,
wca.secty.SectyCD,
wca.secty.SecurityDescriptor
from wca.secty;

use prices;
DROP TABLE IF EXISTS cbas_cntry;
CREATE TABLE `cbas_cntry` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(2) NOT NULL,
  `Lookup` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_cntry
select distinct
case when actflag='D' then 'H' else actflag end,
wca.cntry.Acttime,
wca.cntry.CntryCD,
wca.cntry.Country
from wca.cntry;

use prices;
DROP TABLE IF EXISTS cbas_curen;
CREATE TABLE `cbas_curen` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(3) NOT NULL,
  `Lookup` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_curen
select distinct
case when actflag='D' then 'H' else actflag end,
wca.curen.Acttime,
wca.curen.CurenCD,
wca.curen.Currency
from wca.curen;

use prices;
DROP TABLE IF EXISTS cbas_exchg;
CREATE TABLE `cbas_exchg` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` SMALLINT(6) NOT NULL,
  `Lookup` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_exchg
select distinct
case when actflag='D' then 'H' else actflag end,
wca.exchg.Acttime,
wca.exchg.ExchgID,
wca.exchg.Exchgname
from wca.exchg;


DROP TABLE IF EXISTS cbas_dper;
CREATE TABLE `cbas_dper` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_dper
select distinct 
case when wca.dvprd.actflag='D' then 'H' else wca.dvprd.actflag end,
wca.dvprd.acttime, wca.dvprd.dvprdcd, wca.dvprd.divperiod
from wca.dvprd;


use prices;
DROP TABLE IF EXISTS cbas_event;
CREATE TABLE `cbas_event` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` char(10) NOT NULL,
  `Lookup` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'BON  ', 'Bonus';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'CALL ', 'Call';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'CAPRD', 'Capital Reduction';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'CONSD', 'Consolidation';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'ENT  ', 'Entitlement (Offer)';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'DIST ', 'Distribution';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'DIV  ', 'Dividend';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'DMRGR', 'De-merger';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'DRIP ', 'Dividend reinvestment plan';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'LCC  ', 'Local Code Change';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'MRGR ', 'Merger';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'RCAP ', 'Return of Capital';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'RTS  ', 'Rights';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'RTS-T', 'Rights Transient';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'SECRC', 'Security Reeventification';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'SCSWP', 'Security Swap';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'SD   ', 'Subdivision';
insert ignore into prices.cbas_event select 'I', '2019-01-01 15:00:00', 'TKOVR', 'Takeover (script ONLY)';


use prices;
DROP TABLE IF EXISTS cbas_class;
CREATE TABLE `cbas_class` (
  `RecCD` char(1) NOT NULL,
  `RecTM` datetime NOT NULL,
  `Code` SMALLINT(6) NOT NULL,
  `Lookup` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '25', 'Bonus (same), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '26', 'Bonus (diff), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '53', 'Capital Call, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '51', 'Capital Reduction, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '54', 'Capital Return, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '62', 'Consolidation, 1 record generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '86', 'Distribution, multiple records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '14', 'Cash Dividend: 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '15', 'Script Dividend (same) & DRIP, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '16', 'Script Dividend (diff), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '76', 'De-merger, multiple records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '72', 'Merger, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '45', 'Offer (same), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '46', 'Offer (diff), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '35', 'Rights (same), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '36', 'Rights (tran & diff), 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '66', 'Security Swap, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '67', 'Security Reclassification, 2 records generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '61', 'Subdivision, 1 record generated';
insert ignore into prices.cbas_class select 'I', '2019-01-01 15:00:00', '96', 'Takeover (script), 1 record generated';
