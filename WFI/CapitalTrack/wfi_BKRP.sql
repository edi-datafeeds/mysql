--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('BKRP') as TableName,
wca.bkrp.Actflag,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,  
wca.bkrp.BkrpID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bkrp.IssID,
wca.bkrp.NotificationDate,
wca.bkrp.FilingDate
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bkrp.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'