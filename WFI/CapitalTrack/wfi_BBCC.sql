--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate as Created,
wca.bbcc.Acttime as Changed,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.Oldcntrycd,
wca.bbcc.Oldcurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.Newcntrycd,
wca.bbcc.Newcurencd,
wca.bbcc.OldbbgcompId,
wca.bbcc.NewbbgcompId,
wca.bbcc.Oldbbgcomptk,
wca.bbcc.Newbbgcomptk,
wca.bbcc.ReleventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bbcc.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'