--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_MKTSG
--fileheadertext=MKTSG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MKTSG') as TableName,
wca.mktsg.Actflag,
wca.mktsg.AnnounceDate as Created,
wca.mktsg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.mktsg.ExchgCD,
wca.mktsg.MktSegment,
wca.mktsg.MIC,
wca.mktsg.MktsgID
from wca.mktsg
inner join wca.scexh on wca.mktsg.MktsgID = wca.scexh.MktsgID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.mktsg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'