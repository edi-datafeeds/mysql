--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CapitalTrack_Bus_Day
--fileheadertext=CapitalTrack_Bus_Day_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
SecID,
ISIN,
CommonCode,
DebtCurrency,
IssueAmount * 1000000 as IssueAmount,
OutstandingAmount * 1000000 as OutstandingAmount, 
IssuerName,
SecurityDesc,
date_format(curdate(), '%d/%m/%Y') as RateFixDate, -- see if we have equivalent field ask Dave if not then use now() as RateFixDate,
date_format(InterestFromDate, '%d/%m/%Y') as InterestFromDate,
date_format(InterestToDate, '%d/%m/%Y') as InterestToDate,
'' as NextFixDate, -- see if we have equivalent field ask Dave if not then leave as blank as NextFixDate,
date_format(PayDate, '%d/%m/%Y') as PayDate,
date_format(MaturityDate, '%d/%m/%Y') as MaturityDate,
InterestRate,
'1' as Denomination1,
Denomination2,
Denomination3,
Denomination4,
Denomination5,
Denomination6,
GrossInterest as Amount1,
'' as Amount2,
'' as Amount3,
'' as Amount4,
'' as Amount5,
'' as Amount6,
PoolFactor,
'' as CalcPF,
'' as FullRedemptionAmount,
'' as PartialRedemptionAmount,
Days,
InterestAccrualConvention,
'' as Ammended,
case when Relationship <> 'CAL' then '' else  RegistrarName end as 'Calculation Agent', 
City,
#'' as CalcAgent, -- check to see why there are No Calc Agents coming through via th filtering
#'' as CalcLocation, -- -- check to see why there are No Calc Agents coming through via th filtering, this is same as city etc
'' as Blank,
'' as Notes,
'' as PayFormula,
'' as IRFormula,
'' as FXRate,
AnlCoupRate,
'' as DCF,
case when Relationship <> 'PA' then '' else  RegistrarName end as 'Paying Agent', 

date_format(RecDate, '%d/%m/%Y') as RecDate,
'' as EC,
'' as Listing,
'' as 'Terminated',
'' as TermNotes,
'' as Entered,
'' as MailData,
'' as Message,
'' as CS,
'' as Corp_actions,
NilInt,


Statusflag
from portfolio.CapitalTrack_BusDay ct
where
(Relationship = 'CAL' OR Relationship = 'PA')
and PayDate <> '0000-00-00 00:00:00'
and ct.scagyid=
(select scagyid from wca.scagy as sub
where
ct.secid=sub.secid
and (sub.relationship='CAL' or sub.relationship='PA')
and sub.actflag <> 'd'
order by sub.relationship limit 1)
order by ct.secid,ct.relationship;