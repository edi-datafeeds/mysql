--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CONVT') as TableName,
wca.convt.Actflag,
wca.convt.AnnounceDate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.convt.FromDate,
wca.convt.ToDate,
wca.convt.RatioNew,
wca.convt.RatioOld,
wca.convt.CurenCD,
wca.convt.CurPair,
wca.convt.Price,
wca.convt.MandOptFlag,
wca.convt.ResSecID,
wca.convt.SectyCD as ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,
wca.convt.FXrate,
wca.convt.PartFinalFlag,
wca.convt.PriceAsPercent,
wca.convt.MinPrice,
wca.convt.MaxPrice,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.convt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'