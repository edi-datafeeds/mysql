--filepath=O:\Datafeed\Debt\CapitalTrack\ToLoad\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=CapitalTrack_Bus_Day
--fileheadertext=CapitalTrack_Bus_Day_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.X as CommonCode,
wca.bond.curencd as DebtCurrency,
wca.bond.IssueAmount,
wca.bond.OutstandingAmount,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
CASE WHEN wca.bond.MaturityDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.bond.MaturityDate END AS MaturityDate,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.Denomination7,
wca.scexh.ExchgCD,
wca.scexh.Localcode,
CASE WHEN wca.rd.RecDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.rd.RecDate END AS RecDate,
CASE WHEN wca.exdt.Exdate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.exdt.Exdate END AS ExDate,
CASE WHEN wca.exdt.Paydate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.exdt.Paydate END AS PayDate,
wca.bond.InterestBasis,
CASE WHEN wca.bond.IntCommencementDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.bond.IntCommencementDate END AS InterestCommencementDate,
CASE WHEN wca.bond.FirstCouponDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.bond.FirstCouponDate END AS FirstCouponDate,
CASE WHEN wca.bond.LastCouponDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.bond.LastCouponDate END AS LastCouponDate,
wca.bond.IntBusDayConv as InterestBusDayConv,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPaymentFrequency,
wca.int_my.NilInt,
wca.int_my.Days,
CASE WHEN wca.int_my.InterestFromDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.int_my.InterestFromDate END AS InterestFromDate,
CASE WHEN wca.int_my.InterestToDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.int_my.InterestToDate END AS InterestToDate,
wca.intpy.CurenCD as InterestCurrency,
wca.intpy.IntRate as InterestRate,
wca.intpy.AnlCoupRate,
wca.intpy.GrossInterest,
wca.redem.PoolFactor,
wca.redem.AmountRedeemed,
wca.redem.PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.scagy.scagyID,
wca.agncy.AgncyID,
wca.agncy.RegistrarName,
wca.scagy.Relationship,
wca.agncy.City,
case when wca.scmst.statusflag = '' then 'A' else wca.scmst.statusflag end as statusflag
from wca.int_my
inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
inner join wca.rd on wca.int_my.rdid= wca.rd.rdid
inner join wca.scmst on wca.rd.secid= wca.scmst.secid
inner join wca.issur on wca.scmst.issid= wca.issur.issid
inner join wca.bond on wca.scmst.secid= wca.bond.secid
left outer join wca.scagy on wca.bond.secid = wca.scagy.secid
			  and ('D'<>wca.scagy.actflag or wca.scagy.actflag is null)
inner join wca.agncy on wca.scagy.agncyid = wca.agncy.agncyid
			  and 'D'<>wca.agncy.actflag
left outer join wca.bondx on wca.bond.secid= wca.bondx.secid
              and 'D'<>wca.bondx.actflag
left outer join wca.scexh on wca.scmst.secid= wca.scexh.secid
              and ('D'<>wca.scexh.actflag OR wca.scexh.actflag IS NULL)
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid
              and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
              and 'D'<>wca.exdt.actflag
left outer join wca.redem on wca.scmst.secid= wca.redem.secid
              and ifnull(wca.redem.redemdate,'2001-01-01') > date_sub(ifnull(wca.int_my.interesttodate,'2099-01-01'), interval 7 day)
              and ifnull(wca.redem.redemdate,'2099-01-01') < date_add(ifnull(wca.int_my.interesttodate,'2001-01-01'), interval 7 day)
                                                  and 'D'<>wca.redem.actflag
where
(wca.int_my.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
    or  wca.intpy.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
    or  wca.rd.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3)
    or  ifnull(wca.redem.acttime,'2001-01-01')>(select max(feeddate) from wca.tbl_opslog where seq=3)
    or  ifnull(wca.exdt.acttime,'2001-01-01')>(select max(feeddate) from wca.tbl_opslog where seq=3))
and wca.scmst.actflag <> 'D'
and wca.bond.actflag <> 'D' 
and wca.issur.actflag <> 'D'
and (wca.intpy.actflag <> 'D' OR wca.intpy.actflag IS NULL)
and (wca.int_my.actflag <> 'D' OR wca.int_my.actflag IS NULL)
and wca.intpy.inttype = 'C'
and (wca.scagy.Relationship = 'CAL' OR wca.scagy.Relationship = 'PA');