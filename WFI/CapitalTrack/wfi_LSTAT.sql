--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LSTAT') as TableName,
wca.lstat.Actflag,
wca.lstat.AnnounceDate as Created,
wca.lstat.Acttime as Changed,
wca.lstat.LstatID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.lstat.ExchgCD,
wca.lstat.NotificationDate,
wca.lstat.EffectiveDate,
wca.lstat.LStatStatus,
wca.lstat.EventType,
wca.lstat.Reason
from wca.lstat
inner join wca.bond on wca.lstat.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.lstat.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'