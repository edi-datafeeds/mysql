--filepath=O:\Datafeed\Debt\CapitalTrack\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CapitalTrack\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCMST') as TableName,
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.scmstNotes As Notes,
wca.scmst.StatusReason,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.FYENPPDate,
wca.scmst.VALOREN,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate,
wca.scmst.UmprgID,
wca.scmst.NoParValue,
wca.scmst.ExpDate,
wca.scmst.VotePerSec,
wca.scmst.FreeFloat,
wca.scmst.FreeFloatDate,
wca.scmst.TreasuryShares,
wca.scmst.TreasurySharesDate,
scmst.FISN
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.InterestBasis like '%fr%'