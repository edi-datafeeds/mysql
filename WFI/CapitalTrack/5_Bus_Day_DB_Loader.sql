DROP TABLE IF EXISTS portfolio.CapitalTrack_BusDay;

CREATE TABLE portfolio.CapitalTrack_BusDay (
  `SecID` int(10) NOT NULL DEFAULT '0',
  `ISIN` char(12) DEFAULT NULL,
  `CommonCode` varchar(15) DEFAULT NULL,
  `DebtCurrency` char(3) DEFAULT NULL,
  `IssueAmount` varchar(20) DEFAULT NULL,
  `OutstandingAmount` varchar(20) DEFAULT NULL,
  `IssuerName` varchar(70) NOT NULL DEFAULT '',
  `SecurityDesc` varchar(70) DEFAULT NULL,
  `MaturityDate` datetime DEFAULT NULL,
  `Denomination1` varchar(20) DEFAULT NULL,
  `Denomination2` varchar(20) DEFAULT NULL,
  `Denomination3` varchar(20) DEFAULT NULL,
  `Denomination4` varchar(20) DEFAULT NULL,
  `Denomination5` varchar(20) DEFAULT NULL,
  `Denomination6` varchar(20) DEFAULT NULL,
  `Denomination7` varchar(20) DEFAULT NULL,
  `ExchgCD` varchar(6) NOT NULL DEFAULT '',
  `LocalCode` varchar(50) DEFAULT NULL,
  `RecDate` datetime DEFAULT NULL,
  `ExDate` datetime DEFAULT NULL,
  `PayDate` datetime DEFAULT NULL,
  `InterestBasis` varchar(10) DEFAULT NULL,
  `InterestCommencementDate` datetime DEFAULT NULL,
  `FirstCouponDate` datetime DEFAULT NULL,
  `LastCouponDate` datetime DEFAULT NULL,
  `InterestBusDayConv` varchar(10) DEFAULT NULL,
  `InterestAccrualConvention` varchar(10) DEFAULT NULL,
  `InterestPaymentFrequency` char(3) DEFAULT NULL,
  `NilInt` char(1) DEFAULT NULL,
  `Days` varchar(20) DEFAULT NULL,
  `InterestFromDate` datetime DEFAULT NULL,
  `InterestToDate` datetime DEFAULT NULL,
  `InterestCurrency` char(3) DEFAULT NULL,
  `InterestRate` varchar(20) DEFAULT NULL,
  `AnlCoupRate` varchar(20) DEFAULT NULL,
  `GrossInterest` varchar(20) DEFAULT NULL,
  `PoolFactor` varchar(20) DEFAULT NULL,
  `AmountRedeemed` varchar(20) DEFAULT NULL,
  `PriceAsPercent` varchar(20) DEFAULT NULL,
  `PremiumAsPercent` varchar(20) DEFAULT NULL,
  `ScagyID` int(10) NOT NULL DEFAULT '0',
  `AgncyID` int(10) NOT NULL DEFAULT '0',
  `RegistrarName` varchar(70) NOT NULL DEFAULT '',
  `Relationship` varchar(10) NOT NULL DEFAULT '',
  `City` varchar(50) NOT NULL DEFAULT '',
  `Statusflag` char(1) DEFAULT NULL, 
  PRIMARY KEY (`SecID`,`ExchgCD`,`RecDate`,`PayDate`,`Relationship`,`City`,`RegistrarName`) USING BTREE,
  KEY `IX_SCMST_ISIN` (`ISIN`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


load data local infile 'O:/Datafeed/Debt/CapitalTrack/ToLoad/CapitalTrack_Bus_Day.txt' 
into table portfolio.CapitalTrack_BusDay CHARACTER SET latin1
fields terminated by '\t'
lines terminated by '\n'
IGNORE 2 LINES;

