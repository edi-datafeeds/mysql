--filepath=O:\Datafeed\Debt\Selerity\CorpActions\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Selerity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
'COSNT' as TableName,
wca.cosnt.Actflag,
wca.cosnt.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.cosnt.Acttime) THEN wca.rd.Acttime ELSE wca.cosnt.Acttime END as Changed,
wca.cosnt.RdID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.SecurityDesc,
wca.issur.IssuerName,
wca.rd.Recdate,
wca.cosnt.ExpiryDate,
wca.cosnt.ExpiryTime,
SUBSTRING(wca.cosnt.TimeZone, 1,3) AS TimeZone,
wca.cosnt.CollateralRelease,
wca.cosnt.Currency,
wca.cosnt.Fee,
wca.cosnt.Notes
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.isstype = 'CORP'
and cosnt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)