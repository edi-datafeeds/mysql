--filepath=O:\Datafeed\Debt\Selerity\CorpActions\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=EDI_AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Selerity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SECID,
wca.scmst.ISIN,
wca.scmst.SecurityDesc,
wca.issur.IssuerName,
wca.auct.AuctionDate,
wca.auct.AuctionAmount,
wca.auct.IssueDate,
'' as AllotmentPriceAsPercent,
wca.auct.AmountAccepted,
wca.auct.AuctionID
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.isstype = 'CORP'
and auct.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
