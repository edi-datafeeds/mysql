--filepath=O:\Datafeed\Debt\Selerity\CorpActions\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_ISSDD
--fileheadertext=EDI_ISSDD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Selerity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('ISSDD') as TableName,
wca.issdd.Actflag,
wca.issdd.AnnounceDate as Created,
wca.issdd.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.SecurityDesc,
wca.issur.IssuerName,
wca.issdd.IssID,
wca.issdd.StartDate,
wca.issdd.EndDate,
wca.issdd.IssuerDebtDefaultID,
wca.issdd.Notes
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.isstype = 'CORP'
and issdd.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
