--filepath=O:\Datafeed\Debt\Selerity\CorpActions\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Selerity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('BKRP') as TableName,
wca.bkrp.Actflag,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,  
wca.bkrp.BkrpID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.scmst.SecurityDesc,
wca.issur.IssuerName,
wca.bkrp.IssID,
wca.bkrp.NotificationDate,
wca.bkrp.FilingDate
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.isstype = 'CORP'
and bkrp.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)