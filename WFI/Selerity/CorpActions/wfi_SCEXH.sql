--filepath=O:\Datafeed\Debt\Selerity\CorpActions\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Selerity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT 
upper('SCEXH') as TableName,
wca.scexh.Actflag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scmst.ISIN,
wca.scmst.SecurityDesc,
wca.issur.IssuerName,
wca.scexh.ExchgCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.DelistDate,
wca.scexh.LocalCode,
SUBSTRING(wca.scexh.JunkLocalcode,1,50) as JunkLocalcode,
wca.scexh.PriceTick,
wca.scexh.TickSize,
wca.scexh.ListCategory,
wca.scexh.ScexhNotes As Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.isstype = 'CORP'
and scexh.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
