--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('INCHG') as TableName,
wca.inchg.Actflag,
wca.inchg.AnnounceDate as Created,
wca.inchg.Acttime as Changed,
wca.inchg.InChgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.inchg.IssID,
wca.inchg.InChgDate,
wca.inchg.OldCntryCD,
wca.inchg.NewCntryCD,
wca.inchg.EventType 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.inchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

