--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT distinct
upper('BBEC') as tablename,
wca.bbec.ActFlag,
wca.bbec.AnnounceDate as Created,
wca.bbec.Acttime as Changed,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.bbec.BbeId,
wca.bbec.Oldexchgcd,
wca.bbec.Oldcurencd,
wca.bbec.EffectiveDate,
wca.bbec.Newexchgcd,
wca.bbec.Newcurencd,
wca.bbec.OldbbgexhId,
wca.bbec.NewbbgexhId,
wca.bbec.Oldbbgexhtk,
wca.bbec.Newbbgexhtk,
wca.bbec.ReleventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.bbec.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

