--filepath=O:\Datafeed\Debt\Mergent_RiskSpan_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Mergent_RiskSpan\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct 
upper('ICC') as TableName,
wca.icc.Actflag,
wca.icc.AnnounceDate as Created,
wca.icc.Acttime as Changed,
wca.icc.IccID,
wca.icc.SecID,
wca.scmst.ISIN,
wca.icc.EffectiveDate,
wca.icc.OldISIN,
wca.icc.NewISIN,
wca.icc.OldUSCode,
wca.icc.NewUSCode,
wca.icc.OldVALOREN,
wca.icc.NewVALOREN,
wca.icc.EventType,
wca.icc.RelEventID,
wca.icc.OldCommonCode,
wca.icc.NewCommonCode
from wca.icc
inner join wca.bond on wca.icc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.issur.cntryofincorp <> 'US'
and (wca.icc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))

