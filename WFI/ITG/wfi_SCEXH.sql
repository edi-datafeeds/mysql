--filepath=H:\y.laifa\FixedIncome\Feeds\ITG\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ITG\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('SCEXH') as TableName,
wca.scexh.Actflag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scmst.ISIN,
wca.scexh.ExchgCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.DelistDate,
wca.scexh.LocalCode,
SUBSTRING(wca.scexh.JunkLocalcode,1,50) as JunkLocalcode,
wca.scexh.ScexhNotes As Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'CABND' or wca.scexh.ExchgCD = 'CACNQ'
or wca.scexh.ExchgCD = 'CACVE' or wca.scexh.ExchgCD = 'CATNX'
or wca.scexh.ExchgCD = 'CATSE' or wca.scexh.ExchgCD = 'CAASE'
or wca.scexh.ExchgCD = 'CAMSE' or wca.scexh.ExchgCD = 'CAVSE')
and (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
OR wca.issur.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
-- or (scmst.secid = '4963349' or scmst.secid = '5430479')\n
