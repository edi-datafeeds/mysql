--filepath=H:\y.laifa\FixedIncome\Feeds\ITG\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ITG\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT DISTINCT
upper('SCMST') as TableName,
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.scmstNotes As Notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'CABND' or wca.scexh.ExchgCD = 'CACNQ'
or wca.scexh.ExchgCD = 'CACVE' or wca.scexh.ExchgCD = 'CATNX'
or wca.scexh.ExchgCD = 'CATSE' or wca.scexh.ExchgCD = 'CAASE'
or wca.scexh.ExchgCD = 'CAMSE' or wca.scexh.ExchgCD = 'CAVSE')
and (wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
OR wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
OR wca.issur.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
-- or (scmst.secid = '4963349' or scmst.secid = '5430479')\n


