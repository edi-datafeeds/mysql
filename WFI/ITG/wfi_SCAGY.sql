--filepath=H:\y.laifa\FixedIncome\Feeds\ITG\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ITG\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCAGY') as TableName,
wca.scagy.Actflag,
wca.scagy.AnnounceDate as Created,
wca.scagy.Acttime as Changed,
wca.scagy.ScagyID,
wca.scagy.SecID,
wca.scmst.ISIN,
wca.scagy.Relationship,
wca.scagy.AgncyID,
wca.scagy.GuaranteeType,
wca.scagy.SpStartDate,
wca.scagy.SpEndDate,
wca.scagy.Notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.ExchgCD = 'CABND' or wca.scexh.ExchgCD = 'CACNQ'
or wca.scexh.ExchgCD = 'CACVE' or wca.scexh.ExchgCD = 'CATNX'
or wca.scexh.ExchgCD = 'CATSE' or wca.scexh.ExchgCD = 'CAASE'
or wca.scexh.ExchgCD = 'CAMSE' or wca.scexh.ExchgCD = 'CAVSE')
and wca.scagy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
-- or (scmst.secid = '4963349' or scmst.secid = '5430479')\n
