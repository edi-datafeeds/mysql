--filepath=H:\y.laifa\FixedIncome\Feeds\ITG\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ITG\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGNCY') as TableName,
agncy.Actflag,
agncy.AnnounceDate as Created,
agncy.Acttime as Changed,
agncy.AgncyID,
agncy.RegistrarName,
agncy.Add1,
agncy.Add2,
agncy.Add3,
agncy.Add4,
agncy.Add5,
agncy.Add6,
agncy.City,
agncy.CntryCD,
agncy.Website,
agncy.Contact1,
agncy.Tel1,
agncy.Fax1,
agncy.Email1,
agncy.Contact2,
agncy.Tel2,
agncy.Fax2,
agncy.Email2,
agncy.Depository,
agncy.State
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
INNER JOIN wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.ExchgCD = 'CABND' or wca.scexh.ExchgCD = 'CACNQ'
or wca.scexh.ExchgCD = 'CACVE' or wca.scexh.ExchgCD = 'CATNX'
or wca.scexh.ExchgCD = 'CATSE' or wca.scexh.ExchgCD = 'CAASE'
or wca.scexh.ExchgCD = 'CAMSE' or wca.scexh.ExchgCD = 'CAVSE')
and wca.agncy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
-- or (scmst.secid = '4963349' or scmst.secid = '5430479')\n