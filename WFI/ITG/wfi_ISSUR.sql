--filepath=H:\y.laifa\FixedIncome\Feeds\ITG\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ITG\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssID,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.FinancialYearEnd,
wca.issur.Shortname,
wca.issur.LegalName,
-- case when wca.issur.cntryofincorp='AA' then 'S' when wca.issur.isstype='GOV' then 'G' when wca.issur.isstype='GOVAGENCY' then 'Y' else 'C' end as Isstype,
wca.issur.isstype,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.IssurNotes as Notes
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
INNER JOIN wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.ExchgCD = 'CABND' or wca.scexh.ExchgCD = 'CACNQ'
or wca.scexh.ExchgCD = 'CACVE' or wca.scexh.ExchgCD = 'CATNX'
or wca.scexh.ExchgCD = 'CATSE' or wca.scexh.ExchgCD = 'CAASE'
or wca.scexh.ExchgCD = 'CAMSE' or wca.scexh.ExchgCD = 'CAVSE')
and (wca.issur.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
-- or (scmst.secid = '4963349' or scmst.secid = '5430479')\n