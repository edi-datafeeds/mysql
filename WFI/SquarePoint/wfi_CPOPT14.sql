--filepath=O:\Datafeed\Debt\SquarePoint\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT14
--fileheadertext=EDI_CPOPT_14_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\SquarePoint_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CPOPT') as TableName,
wca.cpopt.Actflag,
wca.cpopt.AnnounceDate as Created,
wca.cpopt.Acttime as Changed,
wca.cpopt.CpoptID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.cptype,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select client.pfisin.code from client.pfisin where accid = 990)
and cpoptid>7418632