--filepath=O:\Datafeed\Debt\Fite_Analytics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssID,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.FinancialYearEnd,
wca.issur.Shortname,
wca.issur.LegalName,
-- case when wca.issur.cntryofincorp='AA' then 'S' when wca.issur.isstype='GOV' then 'G' when wca.issur.isstype='GOVAGENCY' then 'Y' else 'C' end as Isstype,
wca.issur.isstype,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.LEI,
wca.issur.NAICS,
wca.issur.CntryIncorpNumber,
wca.issur.ShellCompany,
wca.issur.IssurNotes as Notes
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
-- inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
-- (wca.continent.country = 'United States' or wca.continent.country = 'Canada')
-- and (wca.issur.Isstype = 'CORP' or wca.issur.Isstype = 'GOVAGENCY' or wca.issur.Isstype = 'GOV')
(wca.issur.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
