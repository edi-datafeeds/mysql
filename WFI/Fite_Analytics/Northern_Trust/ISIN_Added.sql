--filepath=O:\Upload\Acc\398\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISIN_Added
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\Northern_Trust\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT
CODE AS ISIN
#wca.bond.InterestBasis,
#wca.bond.LatestAppliedINTPYAnlCpnRateDate
FROM client.pfisin
LEFT outer JOIN wca.scmst ON client.pfisin.secid = wca.scmst.secid
LEFT outer JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
WHERE
client.pfisin.accid = 398
AND client.pfisin.actflag = 'I'
AND client.pfisin.acttime > CURDATE();