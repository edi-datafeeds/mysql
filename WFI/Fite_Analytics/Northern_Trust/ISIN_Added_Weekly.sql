--filepath=O:\Upload\Acc\398\weekly\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISIN_Added_Weekly
--fileheadertext=ISIN_Added_Weekly_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\Northern_Trust\Weekly\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT
CODE AS ISIN,
client.pfisin.acttime,
wca.bond.InterestBasis,
wca.bond.LatestAppliedINTPYAnlCpnRateDate
FROM client.pfisin
LEFT outer JOIN wca.scmst ON client.pfisin.secid = wca.scmst.secid
LEFT outer JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
WHERE
client.pfisin.accid = 398
AND pfisin.acttime > (select date_sub(max(pfisin.acttime), INTERVAL 7 DAY) FROM client.pfisin)

and pfisin.actflag <> 'D'
order by pfisin.acttime desc;