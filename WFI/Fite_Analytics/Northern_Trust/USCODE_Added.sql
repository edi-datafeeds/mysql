--filepath=O:\Upload\Acc\398\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_USCODE_Added
--fileheadertext=USCODE_Added_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\Northern_Trust\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT
CODE AS USCODE,
wca.bond.InterestBasis,
wca.bond.LatestAppliedINTPYAnlCpnRateDate
FROM client.pfuscode
LEFT outer JOIN wca.scmst ON client.pfuscode.secid = wca.scmst.secid
LEFT outer JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
WHERE
client.pfuscode.accid = 398
AND client.pfuscode.actflag = 'I'
AND client.pfuscode.acttime > CURDATE();