--filepath=O:\Datafeed\Debt\Fite_Analytics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_DIV
--fileheadertext=DIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('DIV') as Tablename,
wca.div_my.ActFlag,
wca.div_my.AnnounceDate as Created,
wca.div_my.Acttime as Changed,
wca.div_my.RdID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.div_my.DivPeriodCD,
wca.div_my.TbaFlag,
wca.div_my.DivNotes,
wca.div_my.NilDividend,
wca.div_my.Coupon,
wca.div_my.FYEDate,
wca.div_my.DivID,
wca.div_my.DivRescind,
wca.div_my.PeriodEndDate,
wca.div_my.Frequency,
wca.div_my.Marker,
wca.div_my.DeclarationDate,
wca.div_my.DeclCurenCD,
wca.div_my.DeclGrossAmt
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
-- inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
-- (wca.continent.country = 'United States' or wca.continent.country = 'Canada')
-- and (wca.issur.Isstype = 'CORP' or wca.issur.Isstype = 'GOVAGENCY' or wca.issur.Isstype = 'GOV')
(wca.div_my.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
;