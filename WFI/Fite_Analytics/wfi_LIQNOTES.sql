--filepath=O:\Datafeed\Debt\Fite_Analytics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LIQNOTES') as TableName,
wca.liq.Actflag,
wca.liq.LiqID,
wca.liq.Issid,
wca.liq.LiquidationTerms as Notes
from wca.liq
where
wca.liq.issid in (select wca.scmst.issid from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
-- left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
-- inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid)
-- where
-- (wca.continent.country = 'United States' or wca.continent.country = 'Canada')
-- and (wca.issur.Isstype = 'CORP' or wca.issur.Isstype = 'GOVAGENCY' or wca.issur.Isstype = 'GOV'))
AND wca.liq.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
