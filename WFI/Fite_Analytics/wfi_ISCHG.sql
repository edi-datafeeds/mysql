--filepath=O:\Datafeed\Debt\Fite_Analytics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('ISCHG') as TableName,
wca.ischg.Actflag,
wca.ischg.AnnounceDate as Created,
wca.ischg.Acttime as Changed,
wca.ischg.IschgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.ischg.IssID,
wca.ischg.NameChangeDate,
wca.ischg.IssOldName,
wca.ischg.IssNewName,
wca.ischg.EventType,
wca.ischg.LegalName,
wca.ischg.MeetingDateFlag, 
wca.ischg.IsChgNotes as Notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
-- inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
-- (wca.continent.country = 'United States' or wca.continent.country = 'Canada')
-- and (wca.issur.Isstype = 'CORP' or wca.issur.Isstype = 'GOVAGENCY' or wca.issur.Isstype = 'GOV')
(wca.ischg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))

and (bond.issuedate<=wca.ischg.namechangedate
     or bond.issuedate is null or wca.ischg.namechangedate is null
    )

