--filepath=O:\Datafeed\Debt\Fite_Analytics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Fite_Analytics\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CTCHG') as TableName,
wca.zctchg.Actflag,
wca.zctchg.AnnounceDate as Created,
wca.zctchg.Acttime as Changed, 
wca.zctchg.zCtChgID as CtChgID,
wca.zctchg.SecID,
wca.scmst.ISIN,
wca.zctchg.EffectiveDate,
wca.zctchg.OldResultantRatio,
wca.zctchg.NewResultantRatio,
wca.zctchg.OldSecurityRatio,
wca.zctchg.NewSecurityRatio,
wca.zctchg.OldCurrency,
wca.zctchg.NewCurrency,
wca.zctchg.OldCurPair,
wca.zctchg.NewCurPair,
wca.zctchg.OldConversionPrice,
wca.zctchg.NewConversionPrice,
wca.zctchg.ResSectyCD,
wca.zctchg.OldResSecID,
wca.zctchg.NewResSecID,
wca.zctchg.EventType,
wca.zctchg.RelEventID,
wca.zctchg.OldFromDate,
wca.zctchg.NewFromDate,
wca.zctchg.OldTodate,
wca.zctchg.NewToDate,
wca.zctchg.ConvtID,
wca.zctchg.OldFXRate,
wca.zctchg.NewFXRate,
wca.zctchg.OldPriceAsPercent,
wca.zctchg.NewPriceAsPercent,
wca.zctchg.OldMinPrice,
wca.zctchg.NewMinPrice,
wca.zctchg.OldMaxPrice,
wca.zctchg.NewMaxPrice,
wca.zctchg.zCtChgNotes as Notes
from wca.zctchg
inner join wca.bond on wca.zctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
-- inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
-- (wca.continent.country = 'United States' or wca.continent.country = 'Canada')
-- and (wca.issur.Isstype = 'CORP' or wca.issur.Isstype = 'GOVAGENCY' or wca.issur.Isstype = 'GOV')
(wca.zctchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 1)
or (wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 1) and wca.scexh.actflag = 'I'))
