--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SECID,
wca.auct.AuctionDate,
wca.auct.AuctionAmount,
wca.auct.IssueDate,
'' as AllotmentPriceAsPercent,
wca.auct.AmountAccepted,
wca.auct.AuctionID
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.auct.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'