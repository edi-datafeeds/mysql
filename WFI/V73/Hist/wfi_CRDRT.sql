--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRDRT') as TableName,
wca.crdrt.Actflag,
wca.crdrt.AnnounceDate as Created,
wca.crdrt.Acttime as Changed, 
wca.crdrt.SecID,
wca.crdrt.RatingAgency,
wca.crdrt.RatingDate,
wca.crdrt.Rating,
wca.crdrt.Direction,
wca.crdrt.WatchList,
wca.crdrt.WatchListReason
from wca.crdrt
inner join wca.bond on wca.crdrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.crdrt.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'