--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSDD
--fileheadertext=ISSDD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('ISSDD') as TableName,
wca.issdd.Actflag,
wca.issdd.AnnounceDate as Created,
wca.issdd.Acttime as Changed,
wca.issdd.IssID,
wca.issdd.StartDate,
wca.issdd.EndDate,
wca.issdd.IssuerDebtDefaultID,
wca.issdd.Notes
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issdd.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'