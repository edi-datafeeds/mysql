--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_SCXTC
--fileheadertext=SCXTC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCXTC') as TableName,
wca.scxtc.Actflag,
wca.scxtc.AnnounceDate as Created,
wca.scxtc.Acttime as Changed,
wca.scxtc.ISIN,
wca.bond.SecID,
wca.scxtc.ScExhID,
wca.scxtc.CurenCD,
wca.scxtc.ListDate,
wca.scxtc.LocalCode,
wca.scxtc.PriceTick,
wca.scxtc.TickSize,
wca.scxtc.ScxtcID
from wca.scxtc
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scxtc.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'