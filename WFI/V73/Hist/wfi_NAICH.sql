--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_NAICH
--fileheadertext=NAICH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('NAICH') as TableName,
wca.naich.Actflag,
wca.naich.AnnounceDate as Created,
wca.naich.Acttime as Changed,
wca.naich.IssID,
wca.naich.EffectiveDate,
wca.naich.OldNAICS,
wca.naich.NewNAICS,
wca.naich.NAICSChID
from wca.naich
inner join wca.scmst on wca.naich.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.naich.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'