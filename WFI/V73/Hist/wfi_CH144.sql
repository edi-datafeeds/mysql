--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_CH144
--fileheadertext=CH144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CH144') as TableName,
wca.ch144.Actflag,
wca.ch144.AnnounceDate as Created,
wca.ch144.Acttime as Changed,
wca.ch144.LK144ID,
wca.ch144.EffectiveDate,
wca.ch144.SerialNumber,
wca.ch144.OldSecID144A,
wca.ch144.NewSecID144a,
wca.scmst.ISIN as ISIN144A,
wca.ch144.OldSecIDRegS,
wca.ch144.NewSecIDRegS,
s1.ISIN as ISINRegS,
wca.ch144.OldSecIDUnrestricted,
wca.ch144.NewSecIDUnrestricted,
wca.ch144.OldLinkOutstandingAmount,
wca.ch144.NewLinkOutstandingAmount,
wca.ch144.OldLinkOutstandingAmountDate,
wca.ch144.NewLinkOutstandingAmountDate,
wca.ch144.EventType,
wca.ch144.RelEventID,
wca.ch144.CH144ID
from wca.ch144
inner join wca.scmst on wca.ch144.OldSecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.ch144.OldSecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.ch144.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'