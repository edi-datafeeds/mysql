--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_EXCHG
--fileheadertext=EXCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('EXCHG') as TableName,
wca.exchg.Actflag,
wca.exchg.AnnounceDate as Created,
wca.exchg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.exchg.ExchgCD,
wca.exchg.ExchgName,
wca.exchg.CntryCD,
wca.exchg.WklyDayOff1,
wca.exchg.WklyDayOff2,
wca.exchg.MIC,
wca.exchg.ExchgID
from wca.exchg
inner join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.exchg.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
and wca.scexh.actflag <> 'D'
-- and wca.scmst.statusflag <> 'I';