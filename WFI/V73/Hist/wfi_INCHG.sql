--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT
upper('INCHG') as TableName,
wca.inchg.Actflag,
wca.inchg.AnnounceDate as Created,
wca.inchg.Acttime as Changed,
wca.inchg.InChgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.inchg.IssID,
wca.inchg.InChgDate,
wca.inchg.OldCntryCD,
wca.inchg.NewCntryCD,
wca.inchg.EventType 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.inchg.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'