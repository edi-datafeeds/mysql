--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_PRCHG
--fileheadertext=PRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('PRCHG') as TableName,
wca.prchg.Actflag,
wca.prchg.AnnounceDate as Created,
wca.prchg.Acttime as Changed,
wca.prchg.PrchgID,
wca.prchg.SecID,
wca.prchg.EffectiveDate,
wca.prchg.EventType,
wca.prchg.OldExchgCD,
wca.prchg.NewExchgCD,
wca.prchg.Notes
from wca.prchg
inner join wca.bond on wca.prchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.prchg.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'