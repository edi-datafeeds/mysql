--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRCHG') as TableName,
wca.crchg.Actflag,
wca.crchg.AnnounceDate as Created,
wca.crchg.Acttime as Changed, 
wca.crchg.CrChgID,
wca.crchg.SecID,
wca.crchg.RatingAgency,
wca.crchg.NewRatingDate,
wca.crchg.OldRating,
wca.crchg.NewRating,
wca.crchg.OldDirection,
wca.crchg.OldWatchList,
wca.crchg.OldWatchListReason,
wca.crchg.EventType,
wca.crchg.OldRatingDate,
wca.crchg.NewDirection,
wca.crchg.NewWatchList,
wca.crchg.NewWatchListReason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.crchg.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'