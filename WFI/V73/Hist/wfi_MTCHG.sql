--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MTCHG') as TableName,
wca.mtchg.Actflag,
wca.mtchg.AnnounceDate as Created,
wca.mtchg.Acttime as Changed,
wca.mtchg.MtChgID,
wca.mtchg.SecID,
wca.scmst.ISIN,
wca.mtchg.NotificationDate,
wca.mtchg.OldMaturityDate,
wca.mtchg.NewMaturityDate,
wca.mtchg.Reason,
wca.mtchg.EventType,
wca.mtchg.OldMaturityBenchmark,
wca.mtchg.NewMaturityBenchmark,
wca.mtchg.Notes as Notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.mtchg.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'