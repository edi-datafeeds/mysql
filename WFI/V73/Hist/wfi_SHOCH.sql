--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_SHOCH
--fileheadertext=SHOCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SHOCH') as Tablename,
wca.shoch.ActFlag,
wca.shoch.AnnounceDate as Created,
wca.shoch.Acttime as Changed,
wca.shoch.SecID,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
wca.shoch.NewSos,
wca.shoch.ShochID,
wca.shoch.ShochNotes,
wca.shoch.EventType,
wca.shoch.RelEventID,
wca.shoch.OldOutstandingDate,
wca.shoch.NewOutstandingDate
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
where
wca.shoch.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
-- and wca.scmst.statusflag <> 'I'