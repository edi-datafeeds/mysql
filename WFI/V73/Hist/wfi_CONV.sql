--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((SELECT Curdate()), '%Y%m%d')
--fileextension=.txt
--suffix=_CONV
--fileheadertext=CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'CONV' as TableName,
wca.conv.Actflag,
wca.conv.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) THEN wca.rd.Acttime ELSE wca.conv.Acttime END as Changed,
wca.conv.ConvID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.conv.FromDate,
wca.conv.ToDate,
wca.conv.RatioNew,
wca.conv.RatioOld,
wca.conv.CurenCD as ConvCurrency,
wca.conv.CurPair,
wca.conv.Price,
wca.conv.MandOptFlag,
wca.conv.ResSecID,
wca.conv.ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.conv.Fractions,
wca.conv.FXrate,
wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.RDID,
wca.conv.PriceAsPercent,
wca.conv.AmountConverted,
wca.conv.SettlementDate,
wca.conv.ExpTime,
wca.conv.ExpTimeZone,
wca.conv.ConvNotes as Notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.conv.Actflag <> 'D'
and wca.bond.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
and (wca.rd.Actflag <> 'D' or wca.rd.Actflag is null)
and (wca.resscmst.Actflag <> 'D' or wca.resscmst.Actflag is null)
and (wca.resissur.Actflag <> 'D' or wca.resissur.Actflag is null)
-- and wca.scmst.statusflag <> 'I';