--filepath=O:\Datafeed\Debt\V73\Continent\South_America\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\Continent\South_America\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGYDT') as TableName,
agydt.Actflag,
agydt.AnnounceDate as Created,
agydt.Acttime as Changed,
agydt.AgydtID,
agydt.AgncyID,
agydt.EffectiveDate,
agydt.OldRegistrarName,
agydt.OldAdd1,
agydt.OldAdd2,
agydt.OldAdd3,
agydt.OldAdd4,
agydt.OldAdd5,
agydt.OldAdd6,
agydt.OldCity,
agydt.OldCntryCD,
agydt.OldWebSite,
agydt.OldContact1,
agydt.OldTel1,
agydt.OldFax1,
agydt.Oldemail1,
agydt.OldContact2,
agydt.OldTel2,
agydt.OldFax2,
agydt.Oldemail2,
agydt.OldState,
agydt.NewRegistrarName,
agydt.NewAdd1,
agydt.NewAdd2,
agydt.NewAdd3,
agydt.NewAdd4,
agydt.NewAdd5,
agydt.NewAdd6,
agydt.NewCity,
agydt.NewCntryCD,
agydt.NewWebSite,
agydt.NewContact1,
agydt.NewTel1,
agydt.NewFax1,
agydt.Newemail1,
agydt.NewContact2,
agydt.NewTel2,
agydt.NewFax2,
agydt.Newemail2,
agydt.NewState
from wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
(wca.continent.country = 'Mexico' or wca.continent.country = 'Argentina' or wca.continent.country = 'Chile'
or wca.continent.country = 'Columbia' or wca.continent.country = 'Peru ' or wca.continent.country = 'Brazil')
and (wca.agydt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
