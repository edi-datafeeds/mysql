--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Incremental\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('IFCHG') as TableName,
wca.ifchg.Actflag,
wca.ifchg.AnnounceDate as Created,
wca.ifchg.Acttime as Changed,
wca.ifchg.IfchgID,
wca.ifchg.SecID,
wca.scmst.ISIN,
wca.ifchg.NotificationDate,
wca.ifchg.OldIntPayFrqncy,
wca.ifchg.OldIntPayDate1,
wca.ifchg.OldIntPayDate2,
wca.ifchg.OldIntPayDate3,
wca.ifchg.OldIntPayDate4,
wca.ifchg.NewIntPayFrqncy,
wca.ifchg.NewIntPayDate1,
wca.ifchg.NewIntPayDate2,
wca.ifchg.NewIntPayDate3,
wca.ifchg.NewIntPayDate4,
wca.ifchg.Eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ifchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'