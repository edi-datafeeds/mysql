--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SECID,
wca.auct.AuctionDate,

-- wca.auct.AuctionAmount,
CASE WHEN locate('.',auct.AuctionAmount) >0
                THEN substring(auct.AuctionAmount,1,locate('.',auct.AuctionAmount)+5)
                ELSE auct.AuctionAmount
                END AS AuctionAmount,


wca.auct.IssueDate,

'' as AllotmentPriceAsPercent,



-- wca.auct.AmountAccepted,
CASE WHEN locate('.',auct.AmountAccepted) >0
                THEN substring(auct.AmountAccepted,1,locate('.',auct.AmountAccepted)+5)
                ELSE auct.AmountAccepted
                END AS AmountAccepted,


wca.auct.AuctionID
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.auct.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'