--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
'COSNT' as TableName,
wca.cosnt.Actflag,
wca.cosnt.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.cosnt.Acttime) THEN wca.rd.Acttime ELSE wca.cosnt.Acttime END as Changed,
wca.cosnt.RdID,
wca.scmst.SecID,
wca.rd.Recdate,
wca.scmst.ISIN,
wca.cosnt.ExpiryDate,
wca.cosnt.ExpiryTime,
SUBSTRING(wca.cosnt.TimeZone, 1,3) AS TimeZone,
wca.cosnt.CollateralRelease,
wca.cosnt.Currency,

-- wca.cosnt.Fee,
CASE WHEN locate('.',cosnt.Fee) >0
                THEN substring(cosnt.Fee,1,locate('.',cosnt.Fee)+5)
                ELSE cosnt.Fee
                END AS Fee,

wca.cosnt.Notes
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.cosnt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'