--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.zfrnfx.Actflag,
wca.zfrnfx.AnnounceDate as Created,
wca.zfrnfx.Acttime as Changed,
wca.zfrnfx.zfrnfxID as FrnfxID,
wca.zfrnfx.SecID,
wca.scmst.ISIN,
wca.zfrnfx.EffectiveDate,
wca.zfrnfx.OldFRNType,
-- wca.zfrnfx.OldFRNIndexBenchmark,

CASE WHEN wca.zfrnfx.OldFRNIndexBenchmark = '' AND wca.zfrnfx.OldFRNBenchmarkCntryCD <> ''   
	 THEN CONCAT(wca.zfrnfx.OldFRNBenchmarkCntryCD, wca.zfrnfx.OldFRNBenchmarkReference, wca.zfrnfx.OldFRNBenchmarkFrequency)
     WHEN wca.zfrnfx.OldFRNIndexBenchmark = '' AND wca.zfrnfx.OldFRNBenchmarkCntryCD = ''
     THEN CONCAT(wca.zfrnfx.OldFRNBenchmarkCurenCD, wca.zfrnfx.OldFRNBenchmarkReference, wca.zfrnfx.OldFRNBenchmarkFrequency)
     ELSE wca.zfrnfx.OldFRNIndexBenchmark END AS OldFRNIndexBenchmark,

-- wca.zfrnfx.OldMarkup As OldFrnMargin,
CASE WHEN locate('.',zfrnfx.OldMarkup) >0
                THEN substring(zfrnfx.OldMarkup,1,locate('.',zfrnfx.OldMarkup)+5)
                ELSE zfrnfx.OldMarkup
                END AS OldFrnMargin,

-- wca.zfrnfx.OldMinimumInterestRate,
CASE WHEN locate('.',zfrnfx.OldMinimumInterestRate) >0
                THEN substring(zfrnfx.OldMinimumInterestRate,1,locate('.',zfrnfx.OldMinimumInterestRate)+5)
                ELSE zfrnfx.OldMinimumInterestRate
                END AS OldMinimumInterestRate,

-- wca.zfrnfx.OldMaximumInterestRate,
CASE WHEN locate('.',zfrnfx.OldMaximumInterestRate) >0
                THEN substring(zfrnfx.OldMaximumInterestRate,1,locate('.',zfrnfx.OldMaximumInterestRate)+5)
                ELSE zfrnfx.OldMaximumInterestRate
                END AS OldMaximumInterestRate,

wca.zfrnfx.OldRounding,
wca.zfrnfx.NewFRNType,
-- wca.zfrnfx.NewFRNIndexBenchmark,

CASE WHEN wca.zfrnfx.NewFRNIndexBenchmark = '' AND wca.zfrnfx.NewFRNBenchmarkCntryCD <> ''   
	 THEN CONCAT(wca.zfrnfx.NewFRNBenchmarkCntryCD, wca.zfrnfx.NewFRNBenchmarkReference, wca.zfrnfx.NewFRNBenchmarkFrequency)
     WHEN wca.zfrnfx.NewFRNIndexBenchmark = '' AND wca.zfrnfx.NewFRNBenchmarkCntryCD = ''
     THEN CONCAT(wca.zfrnfx.NewFRNBenchmarkCurenCD, wca.zfrnfx.NewFRNBenchmarkReference, wca.zfrnfx.NewFRNBenchmarkFrequency)
     ELSE wca.zfrnfx.NewFRNIndexBenchmark END AS NewFRNIndexBenchmark,

-- wca.zfrnfx.NewMarkup As NewFrnMargin,
CASE WHEN locate('.',zfrnfx.NewMarkup) >0
                THEN substring(zfrnfx.NewMarkup,1,locate('.',zfrnfx.NewMarkup)+5)
                ELSE zfrnfx.NewMarkup
                END AS NewFrnMargin,

-- wca.zfrnfx.NewMinimumInterestRate,
CASE WHEN locate('.',zfrnfx.NewMinimumInterestRate) >0
                THEN substring(zfrnfx.NewMinimumInterestRate,1,locate('.',zfrnfx.NewMinimumInterestRate)+5)
                ELSE zfrnfx.NewMinimumInterestRate
                END AS NewMinimumInterestRate,

-- wca.zfrnfx.NewMaximumInterestRate,
CASE WHEN locate('.',zfrnfx.NewMaximumInterestRate) >0
                THEN substring(zfrnfx.NewMaximumInterestRate,1,locate('.',zfrnfx.NewMaximumInterestRate)+5)
                ELSE zfrnfx.NewMaximumInterestRate
                END AS NewMaximumInterestRate,

wca.zfrnfx.NewRounding,
wca.zfrnfx.Eventtype,
wca.zfrnfx.OldFrnIntAdjFreq,
wca.zfrnfx.NewFrnIntAdjFreq
from wca.zfrnfx
inner join wca.bond on wca.zfrnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zfrnfx.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'