--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('MPAY') as TableName,
wca.mpay.Actflag,
wca.mpay.AnnounceDate,
wca.mpay.Acttime,
wca.mpay.sEvent as EventType,
wca.mpay.EventID,
wca.mpay.OptionID,
wca.mpay.SerialID,
wca.mpay.SectyCD as ResSectyCD,
wca.mpay.ResSecID,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,

-- wca.mpay.RatioNew,
CASE WHEN locate('.',mpay.RatioNew) >0
                THEN substring(mpay.RatioNew,1,locate('.',mpay.RatioNew)+7)
                ELSE mpay.RatioNew
                END AS RatioNew,

-- wca.mpay.RatioOld,
CASE WHEN locate('.',mpay.RatioOld) >0
                THEN substring(mpay.RatioOld,1,locate('.',mpay.RatioOld)+7)
                ELSE mpay.RatioOld
                END AS RatioOld,

wca.mpay.Fractions,

-- wca.mpay.MinOfrQty,
CASE WHEN locate('.',mpay.MinOfrQty) >0
                THEN substring(mpay.MinOfrQty,1,locate('.',mpay.MinOfrQty)+0)
                ELSE mpay.MinOfrQty
                END AS MinOfrQty,

-- wca.mpay.MaxOfrQty,
CASE WHEN locate('.',mpay.MaxOfrQty) >0
                THEN substring(mpay.MaxOfrQty,1,locate('.',mpay.MaxOfrQty)+0)
                ELSE mpay.MaxOfrQty
                END AS MaxOfrQty,

-- wca.mpay.MinQlyQty,
CASE WHEN locate('.',mpay.MinQlyQty) >0
                THEN substring(mpay.MinQlyQty,1,locate('.',mpay.MinQlyQty)+0)
                ELSE mpay.MinQlyQty
                END AS MinQlyQty,

-- wca.mpay.MaxQlyQty,
CASE WHEN locate('.',mpay.MaxQlyQty) >0
                THEN substring(mpay.MaxQlyQty,1,locate('.',mpay.MaxQlyQty)+0)
                ELSE mpay.MaxQlyQty
                END AS MaxQlyQty,

wca.mpay.Paydate,
wca.mpay.CurenCD,

-- wca.mpay.MinPrice,
CASE WHEN locate('.',mpay.MinPrice) >0
                THEN substring(mpay.MinPrice,1,locate('.',mpay.MinPrice)+5)
                ELSE mpay.MinPrice
                END AS MinPrice,

-- wca.mpay.MaxPrice,
CASE WHEN locate('.',mpay.MaxPrice) >0
                THEN substring(mpay.MaxPrice,1,locate('.',mpay.MaxPrice)+5)
                ELSE mpay.MaxPrice
                END AS MaxPrice,

-- wca.mpay.TndrStrkPrice,
CASE WHEN locate('.',mpay.TndrStrkPrice) >0
                THEN substring(mpay.TndrStrkPrice,1,locate('.',mpay.TndrStrkPrice)+5)
                ELSE mpay.TndrStrkPrice
                END AS TndrStrkPrice,

-- wca.mpay.TndrStrkStep,
CASE WHEN locate('.',mpay.TndrStrkStep) >0
                THEN substring(mpay.TndrStrkStep,1,locate('.',mpay.TndrStrkStep)+5)
                ELSE mpay.TndrStrkStep
                END AS TndrStrkStep,

wca.mpay.Paytype,
wca.mpay.DutchAuction,
wca.mpay.DefaultOpt,
wca.mpay.OptElectionDate,
wca.mpay.OedExpTime,
wca.mpay.OedExpTimeZone,
wca.mpay.WrtExpTime,
wca.mpay.WrtExpTimeZone
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.mpay.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'