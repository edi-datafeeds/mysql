--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select
'REDEM' as TableName,
wca.redem.Actflag,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.RedemID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.redem.RedemDate,
wca.redem.CurenCD as RedemCurrency,

-- wca.redem.RedemPrice,
CASE WHEN locate('.',redem.RedemPrice) >0
                THEN substring(redem.RedemPrice,1,locate('.',redem.RedemPrice)+5)
                ELSE redem.RedemPrice
                END AS RedemPrice,

wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,

-- wca.redem.AmountRedeemed,
CASE WHEN locate('.',redem.AmountRedeemed) >0
                THEN substring(redem.AmountRedeemed,1,locate('.',redem.AmountRedeemed)+5)
                ELSE redem.AmountRedeemed
                END AS AmountRedeemed,

-- wca.redem.RedemPremium,
CASE WHEN locate('.',redem.RedemPremium) >0
                THEN substring(redem.RedemPremium,1,locate('.',redem.RedemPremium)+5)
                ELSE redem.RedemPremium
                END AS RedemPremium,

-- wca.redem.RedemPercent,
CASE WHEN locate('.',redem.RedemPercent) >0
                THEN substring(redem.RedemPercent,1,locate('.',redem.RedemPercent)+9)
                ELSE redem.RedemPercent
                END AS RedemPercent,

-- wca.redem.PoolFactor,
CASE WHEN locate('.',redem.PoolFactor) >0
                THEN substring(redem.PoolFactor,1,locate('.',redem.PoolFactor)+11)
                ELSE redem.PoolFactor
                END AS PoolFactor,

-- wca.redem.PriceAsPercent,
CASE WHEN locate('.',redem.PriceAsPercent) >0
                THEN substring(redem.PriceAsPercent,1,locate('.',redem.PriceAsPercent)+9)
                ELSE redem.PriceAsPercent
                END AS PriceAsPercent,

-- wca.redem.PremiumAsPercent,
CASE WHEN locate('.',redem.PremiumAsPercent) >0
                THEN substring(redem.PremiumAsPercent,1,locate('.',redem.PremiumAsPercent)+9)
                ELSE redem.PremiumAsPercent
                END AS PremiumAsPercent,

wca.redem.InDefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.ExpTime,
wca.redem.ExpTimeZone,
wca.redem.MatGraceDays,
wca.redem.MatGraceDayConvention,
wca.redem.TenderOfferor,
wca.redem.DutchAuction,
wca.redem.DutchMinPrice,
wca.redem.DutchMinAmount,
wca.redem.DutchMaxPrice,
wca.redem.DutchMaxAmount,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
wca.redem.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'