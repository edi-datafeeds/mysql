--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('RDNOM') as TableName,
wca.rdnom.Actflag,
wca.rdnom.AnnounceDate as Created,
wca.rdnom.Acttime as Changed, 
wca.rdnom.RdnomID,
wca.rdnom.SecID,
wca.scmst.ISIN,
wca.rdnom.EffectiveDate,

-- wca.rdnom.OldDenomination1,
CASE WHEN locate('.',rdnom.OldDenomination1) >0
                THEN substring(rdnom.OldDenomination1,1,locate('.',rdnom.OldDenomination1)+6)
                ELSE rdnom.OldDenomination1
                END AS OldDenomination1,

-- wca.rdnom.OldDenomination2,
CASE WHEN locate('.',rdnom.OldDenomination2) >0
                THEN substring(rdnom.OldDenomination2,1,locate('.',rdnom.OldDenomination2)+6)
                ELSE rdnom.OldDenomination2
                END AS OldDenomination2,

-- wca.rdnom.OldDenomination3,
CASE WHEN locate('.',rdnom.OldDenomination3) >0
                THEN substring(rdnom.OldDenomination3,1,locate('.',rdnom.OldDenomination3)+6)
                ELSE rdnom.OldDenomination3
                END AS OldDenomination3,

-- wca.rdnom.OldDenomination4,
CASE WHEN locate('.',rdnom.OldDenomination4) >0
                THEN substring(rdnom.OldDenomination4,1,locate('.',rdnom.OldDenomination4)+6)
                ELSE rdnom.OldDenomination4
                END AS OldDenomination4,

-- wca.rdnom.OldDenomination5,
CASE WHEN locate('.',rdnom.OldDenomination5) >0
                THEN substring(rdnom.OldDenomination5,1,locate('.',rdnom.OldDenomination5)+6)
                ELSE rdnom.OldDenomination5
                END AS OldDenomination5,

-- wca.rdnom.OldDenomination6,
CASE WHEN locate('.',rdnom.OldDenomination6) >0
                THEN substring(rdnom.OldDenomination6,1,locate('.',rdnom.OldDenomination6)+6)
                ELSE rdnom.OldDenomination6
                END AS OldDenomination6,

-- wca.rdnom.OldDenomination7,
CASE WHEN locate('.',rdnom.OldDenomination7) >0
                THEN substring(rdnom.OldDenomination7,1,locate('.',rdnom.OldDenomination7)+6)
                ELSE rdnom.OldDenomination7
                END AS OldDenomination7,

-- wca.rdnom.OldMinimumDenomination,
CASE WHEN locate('.',rdnom.OldMinimumDenomination) >0
                THEN substring(rdnom.OldMinimumDenomination,1,locate('.',rdnom.OldMinimumDenomination)+6)
                ELSE rdnom.OldMinimumDenomination
                END AS OldMinimumDenomination,

-- wca.rdnom.OldDenominationMultiple,
CASE WHEN locate('.',rdnom.OldDenominationMultiple) >0
                THEN substring(rdnom.OldDenominationMultiple,1,locate('.',rdnom.OldDenominationMultiple)+0)
                ELSE rdnom.OldDenominationMultiple
                END AS OldDenominationMultiple,

-- wca.rdnom.NewDenomination1,
CASE WHEN locate('.',rdnom.NewDenomination1) >0
                THEN substring(rdnom.NewDenomination1,1,locate('.',rdnom.NewDenomination1)+6)
                ELSE rdnom.NewDenomination1
                END AS NewDenomination1,

-- wca.rdnom.NewDenomination2,
CASE WHEN locate('.',rdnom.NewDenomination2) >0
                THEN substring(rdnom.NewDenomination2,1,locate('.',rdnom.NewDenomination2)+6)
                ELSE rdnom.NewDenomination2
                END AS NewDenomination2,

-- wca.rdnom.NewDenomination3,
CASE WHEN locate('.',rdnom.NewDenomination3) >0
                THEN substring(rdnom.NewDenomination3,1,locate('.',rdnom.NewDenomination3)+6)
                ELSE rdnom.NewDenomination3
                END AS NewDenomination3,

-- wca.rdnom.NewDenomination4,
CASE WHEN locate('.',rdnom.NewDenomination4) >0
                THEN substring(rdnom.NewDenomination4,1,locate('.',rdnom.NewDenomination4)+6)
                ELSE rdnom.NewDenomination4
                END AS NewDenomination4,

-- wca.rdnom.NewDenomination5,
CASE WHEN locate('.',rdnom.NewDenomination5) >0
                THEN substring(rdnom.NewDenomination5,1,locate('.',rdnom.NewDenomination5)+6)
                ELSE rdnom.NewDenomination5
                END AS NewDenomination5,

-- wca.rdnom.NewDenomination6,
CASE WHEN locate('.',rdnom.NewDenomination6) >0
                THEN substring(rdnom.NewDenomination6,1,locate('.',rdnom.NewDenomination6)+6)
                ELSE rdnom.NewDenomination6
                END AS NewDenomination6,

-- wca.rdnom.NewDenomination7,
CASE WHEN locate('.',rdnom.NewDenomination7) >0
                THEN substring(rdnom.NewDenomination7,1,locate('.',rdnom.NewDenomination7)+6)
                ELSE rdnom.NewDenomination7
                END AS NewDenomination7,

-- wca.rdnom.NewMinimumDenomination,
CASE WHEN locate('.',rdnom.NewMinimumDenomination) >0
                THEN substring(rdnom.NewMinimumDenomination,1,locate('.',rdnom.NewMinimumDenomination)+6)
                ELSE rdnom.NewMinimumDenomination
                END AS NewMinimumDenomination,

-- wca.rdnom.NewDenominationMultiple,
CASE WHEN locate('.',rdnom.NewDenominationMultiple) >0
                THEN substring(rdnom.NewDenominationMultiple,1,locate('.',rdnom.NewDenominationMultiple)+0)
                ELSE rdnom.NewDenominationMultiple
                END AS NewDenominationMultiple,

wca.rdnom.Notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.rdnom.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'