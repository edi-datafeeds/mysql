--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Incremental\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MTCHG') as TableName,
wca.zmtchg.Actflag,
wca.zmtchg.AnnounceDate as Created,
wca.zmtchg.Acttime as Changed,
wca.zmtchg.zmtchgID as MtChgID,
wca.zmtchg.SecID,
wca.scmst.ISIN,
wca.zmtchg.NotificationDate,
wca.zmtchg.OldMaturityDate,
wca.zmtchg.NewMaturityDate,
wca.zmtchg.Reason,
wca.zmtchg.EventType,


-- wca.zmtchg.OldMaturityBenchmark,

CASE WHEN wca.zmtchg.OldMaturityBenchmark = '' AND wca.zmtchg.OldMaturityBenchmarkCntryCD <> ''   
	 THEN CONCAT(wca.zmtchg.OldMaturityBenchmarkCntryCD, wca.zmtchg.OldMaturityBenchmarkReference, wca.zmtchg.OldMaturityBenchmarkFrequency)
     WHEN wca.zmtchg.OldMaturityBenchmark = '' AND wca.zmtchg.OldMaturityBenchmarkCntryCD = ''
     THEN CONCAT(wca.zmtchg.OldMaturityBenchmarkCurenCD, wca.zmtchg.OldMaturityBenchmarkReference, wca.zmtchg.OldMaturityBenchmarkFrequency)
     ELSE wca.zmtchg.OldMaturityBenchmark END AS OldMaturityBenchmark,


-- wca.zmtchg.NewMaturityBenchmark,

CASE WHEN wca.zmtchg.NewMaturityBenchmark = '' AND wca.zmtchg.NewMaturityBenchmarkCntryCD <> ''   
	 THEN CONCAT(wca.zmtchg.NewMaturityBenchmarkCntryCD, wca.zmtchg.NewMaturityBenchmarkReference, wca.zmtchg.NewMaturityBenchmarkFrequency)
     WHEN wca.zmtchg.NewMaturityBenchmark = '' AND wca.zmtchg.NewMaturityBenchmarkCntryCD = ''
     THEN CONCAT(wca.zmtchg.NewMaturityBenchmarkCurenCD, wca.zmtchg.NewMaturityBenchmarkReference, wca.zmtchg.NewMaturityBenchmarkFrequency)
     ELSE wca.zmtchg.NewMaturityBenchmark END AS NewMaturityBenchmark,



wca.zmtchg.zmtchgNotes as Notes
from wca.zmtchg
inner join wca.bond on wca.zmtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zmtchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'