--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCMST') as TableName,
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,

-- wca.scmst.ParValue,
CASE WHEN locate('.',scmst.ParValue) >0
                THEN substring(scmst.ParValue,1,locate('.',scmst.ParValue)+5)
                ELSE scmst.ParValue
                END AS ParValue,

wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.scmstNotes As Notes,
wca.scmst.StatusReason,

-- wca.scmst.IssuePrice,
CASE WHEN locate('.',scmst.IssuePrice) >0
                THEN substring(scmst.IssuePrice,1,locate('.',scmst.IssuePrice)+5)
                ELSE scmst.IssuePrice
                END AS IssuePrice,

-- wca.scmst.PaidUpValue,
CASE WHEN locate('.',scmst.PaidUpValue) >0
                THEN substring(scmst.PaidUpValue,1,locate('.',scmst.PaidUpValue)+5)
                ELSE scmst.PaidUpValue
                END AS PaidUpValue,

wca.scmst.Voting,
wca.scmst.FYENPPDate,
wca.scmst.VALOREN,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate,
wca.scmst.UmprgID,
wca.scmst.NoParValue,
wca.scmst.ExpDate,

-- wca.scmst.VotePerSec,
CASE WHEN locate('.',scmst.VotePerSec) >0
                THEN substring(scmst.VotePerSec,1,locate('.',scmst.VotePerSec)+6)
                ELSE scmst.VotePerSec
                END AS VotePerSec,

wca.scmst.FreeFloat,
wca.scmst.FreeFloatDate,
wca.scmst.TreasuryShares,
wca.scmst.TreasurySharesDate,
scmst.FISN
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'
-- or wca.scmst.isin in(select code from client.pfisin where accid = 991);