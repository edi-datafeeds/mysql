--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('REDMT') as TableName,
wca.redmt.Actflag,
wca.redmt.AnnounceDate as Created,
wca.redmt.Acttime as Changed,
wca.redmt.RedmtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.redmt.RedemptionDate as RedemDate,
wca.redmt.CurenCD as RedemCurrency,

-- wca.redmt.RedemptionPrice as RedemPrice,
CASE WHEN locate('.',redmt.RedemptionPrice) >0
                THEN substring(redmt.RedemptionPrice,1,locate('.',redmt.RedemptionPrice)+5)
                ELSE redmt.RedemptionPrice
                END AS RedemPrice,

wca.redmt.MandOptFlag,
wca.redmt.PartFinal,
wca.redmt.RedemptionType as RedemType,

-- wca.redmt.RedemptionAmount as RedemAmount,
CASE WHEN locate('.',redmt.RedemptionAmount) >0
                THEN substring(redmt.RedemptionAmount,1,locate('.',redmt.RedemptionAmount)+5)
                ELSE redmt.RedemptionAmount
                END AS RedemAmount,

-- wca.redmt.RedemptionPremium as RedemPremium,
CASE WHEN locate('.',redmt.RedemptionPremium) >0
                THEN substring(redmt.RedemptionPremium,1,locate('.',redmt.RedemptionPremium)+5)
                ELSE redmt.RedemptionPremium
                END AS RedemPremium,

-- wca.redmt.RedemInPercent,
CASE WHEN locate('.',redmt.RedemInPercent) >0
                THEN substring(redmt.RedemInPercent,1,locate('.',redmt.RedemInPercent)+9)
                ELSE redmt.RedemInPercent
                END AS RedemInPercent,

-- wca.redmt.PriceAsPercent,
CASE WHEN locate('.',redmt.PriceAsPercent) >0
                THEN substring(redmt.PriceAsPercent,1,locate('.',redmt.PriceAsPercent)+9)
                ELSE redmt.PriceAsPercent
                END AS PriceAsPercent,
                

-- wca.redmt.PremiumAsPercent,
CASE WHEN locate('.',redmt.PremiumAsPercent) >0
                THEN substring(redmt.PremiumAsPercent,1,locate('.',redmt.PremiumAsPercent)+9)
                ELSE redmt.PremiumAsPercent
                END AS PremiumAsPercent,

wca.redmt.RedmtNotes as Notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.redmt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid = '5251203'