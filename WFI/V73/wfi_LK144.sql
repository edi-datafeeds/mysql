--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V73\Daily\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LK144
--fileheadertext=LK144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V73\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LK144') as TableName,
wca.lk144.Actflag,
wca.lk144.AnnounceDate as Created,
wca.lk144.Acttime as Changed,
wca.lk144.LK144ID,
wca.lk144.SecID144A,
wca.scmst.isin as ISIN144A,
wca.lk144.SecIDRegS,
s1.isin as ISINRegS,
wca.lk144.SecIDUnrestricted,
wca.lk144.LinkOutstandingAmount,
wca.lk144.LinkOutstandingAmountDate
from wca.lk144
inner join wca.scmst on wca.lk144.SecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.lk144.SecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.lk144.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)