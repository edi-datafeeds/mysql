--filepath=O:\AUTO\Scripts\mysql\WFI\V73\BondSigma\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_BondSigma
--fileheadertext=EDI_WFI_BondSigma_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.ISIN,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
bondsigma.calcout.PayDate,
bondsigma.calcout.AnlCoupRate,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.refpricefull.Callput,
bondsigma.refpricefull.CpType,
bondsigma.refpricefull.CpoptID,
DATE_FORMAT(bondsigma.refpricefull.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(bondsigma.refpricefull.ToDate, '%Y-%m-%d') as ToDate,
bondsigma.refpricefull.CpCurrency,
bondsigma.refpricefull.CpPrice,
bondsigma.refpricefull.CpPriceAsPercent,
wca.bond.IssuePrice,
case when (wca.bond.Source like 'AU%' or wca.bond.Source like 'ASX%')
and wca.scmst.ISIN like 'AU%'
and (wca.bond.BondType = 'CD' OR wca.bond.BondType = 'CP')
and wca.bond.IssueDate is null then wca.bond.SourceDate 
else wca.bond.IssueDate end as IssueDate,
wca.bond.Perpetual,
case when bondsigma.calcout.ClosingPrice<>'' and bondsigma.calcout.ClosingPrice is not null
      then DATE_FORMAT(bondsigma.calcout.SettlementDate, '%Y-%m-%d')
      else ''
      end as SettlementDate,


     
case when bondsigma.refpricefull.ExchgCD<>'' and bondsigma.refpricefull.ExchgCD is not null
     then bondsigma.refpricefull.ExchgCD
       
     else scexh.ExchgCD
        
     end as ExchgCD,     
     
     
case when bondsigma.calcout.LocalCode<>'' and bondsigma.calcout.LocalCode is not null
     then bondsigma.calcout.LocalCode
     
     when scexh.LocalCode<>'' and scexh.LocalCode is not null
     then scexh.LocalCode
     
     else 'N/A'
     
     end as LocalCode,
     
     
     
     
     
case when bondsigma.calcout.PriceCurrency<>''
     -- then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     then DATE_FORMAT(bondsigma.calcout.MktCloseDate, '%Y-%m-%d')
     
     else ''
     end as MktCloseDate,
case when bondsigma.calcout.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(bondsigma.calcout.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
bondsigma.calcout.ClosingPrice,
bondsigma.calcout.Open,
bondsigma.calcout.High,
bondsigma.calcout.Low,
bondsigma.calcout.Mid,
bondsigma.calcout.Ask,
bondsigma.calcout.Bid,
bondsigma.calcout.BidSize,
bondsigma.calcout.AskSize,


bondsigma.calcout.TradedVolume,

CASE WHEN bondsigma.calcout.TradedVolume <> '' and bondsigma.calcout.TradedVolume is not null
THEN bondsigma.calcout.TradedVolume
ELSE '0' end as TradedVolume,


bondsigma.calcout.VolFlag,
bondsigma.calcout.PriceCurrency,
bondsigma.calcout.AccruedInterest,
bondsigma.calcout.YieldtoMaturity,
bondsigma.calcout.YieldtoCall,
bondsigma.calcout.YieldtoPut,
bondsigma.calcout.ModifiedDuration,
bondsigma.calcout.EffectiveDuration,
bondsigma.calcout.MacaulayDuration,
bondsigma.calcout.Convexity,
bondsigma.calcout.Yield,
bondsigma.calcout.YieldToWorst,
bondsigma.calcout.KeyRateDuration,
wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD
from wca.scmst
#inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 990=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join bondsigma.refpricefull on wca.scmst.secid=bondsigma.refpricefull.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
left outer join bondsigma.calcout on bondsigma.refpricefull.secid=bondsigma.calcout.secid
                                    and bondsigma.refpricefull.exchgcd=bondsigma.calcout.exchgcd
                                    and bondsigma.refpricefull.pricecurrency=bondsigma.calcout.pricecurrency
left outer join wca.scexh on bondsigma.refpricefull.exchgcd='' and wca.scmst.secid = wca.scexh.secid -- and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D'
                               where  bondsigma.refpricefull.ExchgCD is not null
                               and bondsigma.calcout.ClosingPrice >'0';