--filepath=O:\Datafeed\Debt\TD_Securities\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities_CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT distinct
upper('BBCC') as tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate as Created,
wca.bbcc.Acttime as Changed,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.Oldcntrycd,
wca.bbcc.Oldcurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.Newcntrycd,
wca.bbcc.Newcurencd,
wca.bbcc.OldbbgcompId,
wca.bbcc.NewbbgcompId,
wca.bbcc.Oldbbgcomptk,
wca.bbcc.Newbbgcomptk,
wca.bbcc.ReleventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.Exchgcd LIKE 'CA%'
and (wca.bbcc.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
