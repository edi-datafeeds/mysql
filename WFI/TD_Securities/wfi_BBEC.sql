--filepath=O:\Datafeed\Debt\TD_Securities\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities_CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT distinct
upper('BBEC') as tablename,
wca.bbec.ActFlag,
wca.bbec.AnnounceDate as Created,
wca.bbec.Acttime as Changed,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.bbec.BbeId,
wca.bbec.Oldexchgcd,
wca.bbec.Oldcurencd,
wca.bbec.EffectiveDate,
wca.bbec.Newexchgcd,
wca.bbec.Newcurencd,
wca.bbec.OldbbgexhId,
wca.bbec.NewbbgexhId,
wca.bbec.Oldbbgexhtk,
wca.bbec.Newbbgexhtk,
wca.bbec.ReleventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.Exchgcd LIKE 'CA%'
and (wca.bbec.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
