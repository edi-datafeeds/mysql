--filepath=O:\Datafeed\Debt\TD_Securities\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities_CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('FRNFX') as TableName,
wca.frnfx.Actflag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnfxID,
wca.frnfx.SecID,
wca.scmst.ISIN,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFRNType,
wca.frnfx.OldFRNIndexBenchmark,
wca.frnfx.OldMarkup As OldFrnMargin,
wca.frnfx.OldMinimumInterestRate,
wca.frnfx.OldMaximumInterestRate,
wca.frnfx.OldRounding,
wca.frnfx.NewFRNType,
wca.frnfx.NewFRNindexBenchmark,
wca.frnfx.NewMarkup As NewFrnMargin,
wca.frnfx.NewMinimumInterestRate,
wca.frnfx.NewMaximumInterestRate,
wca.frnfx.NewRounding,
wca.frnfx.Eventtype,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.Exchgcd LIKE 'CA%'
and (wca.frnfx.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
