--filepath=O:\Datafeed\Debt\TD_Securities\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities_CA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('SDCHG') as TableName,
wca.sdchg.Actflag,
wca.sdchg.AnnounceDate as Created, 
wca.sdchg.Acttime as Changed,
wca.sdchg.SdChgID,
wca.sdchg.SecID,
wca.scmst.ISIN,
wca.sdchg.CntryCD as OldCntryCD,
wca.sdchg.EffectiveDate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.EventType,
wca.sdchg.RcntryCD as OldRcntryCD,
wca.sdchg.RelEventID,
wca.sdchg.NewCntryCD,
wca.sdchg.NewRcntryCD,
wca.sdchg.sdchgNotes As Notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.Exchgcd LIKE 'CA%'
and (wca.sdchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
