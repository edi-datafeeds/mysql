--filepath=O:\Upload\Acc\292\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ReddIntelligence\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT
upper('INCHG') as TableName,
wca.inchg.Actflag,
wca.inchg.AnnounceDate as Created,
wca.inchg.Acttime as Changed,
wca.inchg.InChgID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.inchg.IssID,
wca.inchg.InChgDate,
wca.inchg.OldCntryCD,
wca.inchg.NewCntryCD,
wca.inchg.EventType 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=292 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=292 and actflag='U')
and inchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))