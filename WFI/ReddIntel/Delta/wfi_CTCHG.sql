--filepath=O:\Upload\Acc\292\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\ReddIntelligence\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CTCHG') as TableName,
wca.ctchg.Actflag,
wca.ctchg.AnnounceDate as Created,
wca.ctchg.Acttime as Changed, 
wca.ctchg.CtChgID,
wca.ctchg.SecID,
wca.scmst.ISIN,
wca.ctchg.EffectiveDate,
wca.ctchg.OldResultantRatio,
wca.ctchg.NewResultantRatio,
wca.ctchg.OldSecurityRatio,
wca.ctchg.NewSecurityRatio,
wca.ctchg.OldCurrency,
wca.ctchg.NewCurrency,
wca.ctchg.OldCurPair,
wca.ctchg.NewCurPair,
wca.ctchg.OldConversionPrice,
wca.ctchg.NewConversionPrice,
wca.ctchg.ResSectyCD,
wca.ctchg.OldResSecID,
wca.ctchg.NewResSecID,
wca.ctchg.EventType,
wca.ctchg.RelEventID,
wca.ctchg.OldFromDate,
wca.ctchg.NewFromDate,
wca.ctchg.OldTodate,
wca.ctchg.NewToDate,
wca.ctchg.ConvtID,
wca.ctchg.OldFXRate,
wca.ctchg.NewFXRate,
wca.ctchg.OldPriceAsPercent,
wca.ctchg.NewPriceAsPercent,
wca.ctchg.Notes
from wca.ctchg
inner join wca.bond on wca.ctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=292 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=292 and actflag='U')
and ctchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))