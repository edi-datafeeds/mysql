--filepath=O:\Datafeed\Debt\BondIT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BondIT\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('RDNOM') as TableName,
wca.rdnom.Actflag,
wca.rdnom.AnnounceDate as Created,
wca.rdnom.Acttime as Changed, 
wca.rdnom.RdnomID,
wca.rdnom.SecID,
wca.scmst.ISIN,
wca.rdnom.EffectiveDate,
wca.rdnom.OldDenomination1,
wca.rdnom.OldDenomination2,
wca.rdnom.OldDenomination3,
wca.rdnom.OldDenomination4,
wca.rdnom.OldDenomination5,
wca.rdnom.OldDenomination6,
wca.rdnom.OldDenomination7,
wca.rdnom.OldMinimumDenomination,
wca.rdnom.OldDenominationMultiple,
wca.rdnom.NewDenomination1,
wca.rdnom.NewDenomination2,
wca.rdnom.NewDenomination3,
wca.rdnom.NewDenomination4,
wca.rdnom.NewDenomination5,
wca.rdnom.NewDenomination6,
wca.rdnom.NewDenomination7,
wca.rdnom.NewMinimumDenomination,
wca.rdnom.NewDenominationMultiple,
wca.rdnom.Notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.Exchgcd = 'USAMEX' or wca.scexh.Exchgcd = 'USBATS' or wca.scexh.Exchgcd = 'USBND' or wca.scexh.Exchgcd = 'USCOMP' or
wca.scexh.Exchgcd = 'USFNBB' or wca.scexh.Exchgcd = 'USNASD' or wca.scexh.Exchgcd = 'USNYSE' or wca.scexh.Exchgcd = 'USOTC' or
wca.scexh.Exchgcd = 'USOTCB' or wca.scexh.Exchgcd = 'USPAC' or wca.scexh.Exchgcd = 'USPORT' or wca.scexh.Exchgcd = 'USTRCE')
and (wca.rdnom.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
