--filepath=O:\Datafeed\Debt\BondIT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONV
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BondIT\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select distinct
'CONV' as TableName,
wca.conv.Actflag,
wca.conv.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) THEN wca.rd.Acttime ELSE wca.conv.Acttime END as Changed,
wca.conv.ConvID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.conv.FromDate,
wca.conv.ToDate,
wca.conv.RatioNew,
wca.conv.RatioOld,
wca.conv.CurenCD as ConvCurrency,
wca.conv.CurPair,
wca.conv.Price,
wca.conv.MandOptFlag,
wca.conv.ResSecID,
wca.conv.ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.conv.Fractions,
wca.conv.FXrate,
wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.RDID,
wca.conv.PriceAsPercent,
wca.conv.AmountConverted,
wca.conv.SettlementDate,
wca.conv.ExpTime,
wca.conv.ExpTimeZone,
wca.conv.ConvNotes as Notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.Exchgcd = 'USAMEX' or wca.scexh.Exchgcd = 'USBATS' or wca.scexh.Exchgcd = 'USBND' or wca.scexh.Exchgcd = 'USCOMP' or
wca.scexh.Exchgcd = 'USFNBB' or wca.scexh.Exchgcd = 'USNASD' or wca.scexh.Exchgcd = 'USNYSE' or wca.scexh.Exchgcd = 'USOTC' or
wca.scexh.Exchgcd = 'USOTCB' or wca.scexh.Exchgcd = 'USPAC' or wca.scexh.Exchgcd = 'USPORT' or wca.scexh.Exchgcd = 'USTRCE')
and (wca.conv.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
