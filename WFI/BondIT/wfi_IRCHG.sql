--filepath=O:\Datafeed\Debt\BondIT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BondIT\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('IRCHG') as TableName,
wca.irchg.Actflag,
wca.irchg.AnnounceDate as Created,
wca.irchg.Acttime as Changed,
wca.irchg.IrchgID,
wca.irchg.SecID,
wca.scmst.ISIN,
wca.irchg.EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.Eventtype,
wca.irchg.Notes 
from wca.irchg
inner join wca.bond on wca.irchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
(wca.scexh.Exchgcd = 'USAMEX' or wca.scexh.Exchgcd = 'USBATS' or wca.scexh.Exchgcd = 'USBND' or wca.scexh.Exchgcd = 'USCOMP' or
wca.scexh.Exchgcd = 'USFNBB' or wca.scexh.Exchgcd = 'USNASD' or wca.scexh.Exchgcd = 'USNYSE' or wca.scexh.Exchgcd = 'USOTC' or
wca.scexh.Exchgcd = 'USOTCB' or wca.scexh.Exchgcd = 'USPAC' or wca.scexh.Exchgcd = 'USPORT' or wca.scexh.Exchgcd = 'USTRCE')
and (wca.irchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
