--filepath=O:\Datafeed\Debt\BondIT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BondIT\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('BKRPNOTES') as TableName,
wca.bkrp.Actflag,
wca.bkrp.BkrpID,
wca.bkrp.IssId,
wca.bkrp.BkrpNotes as Notes
from wca.bkrp
where
wca.bkrp.issid in (select wca.scmst.issid from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
and (wca.scexh.Exchgcd = 'USAMEX' or wca.scexh.Exchgcd = 'USBATS' or wca.scexh.Exchgcd = 'USBND' or wca.scexh.Exchgcd = 'USCOMP' or
wca.scexh.Exchgcd = 'USFNBB' or wca.scexh.Exchgcd = 'USNASD' or wca.scexh.Exchgcd = 'USNYSE' or wca.scexh.Exchgcd = 'USOTC' or
wca.scexh.Exchgcd = 'USOTCB' or wca.scexh.Exchgcd = 'USPAC' or wca.scexh.Exchgcd = 'USPORT' or wca.scexh.Exchgcd = 'USTRCE'))
and wca.bkrp.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)




