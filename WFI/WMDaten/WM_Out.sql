--filepath=O:\Prodman\Dev\WFI\Feeds\WMDaten\Out\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WM_Out
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\WMDaten\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
ISIN,
DATE_FORMAT(SPOTDATE, "%m/%d/%Y") AS 'SPOTDATE'
from portfolio.WM_Port
where
SPOTDATE >= curdate()
;