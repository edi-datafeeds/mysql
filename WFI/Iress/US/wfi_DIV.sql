--filepath=O:\Datafeed\Debt\Iress\US\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_DIV
--fileheadertext=EDI_DIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Iress\US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('DIV') as Tablename,
wca.div_my.ActFlag,
wca.div_my.AnnounceDate as Created,
wca.div_my.Acttime as Changed,
wca.div_my.RdID,
wca.div_my.DivPeriodCD,
wca.div_my.TbaFlag,
wca.div_my.DivNotes,
wca.div_my.NilDividend,
wca.div_my.Coupon,
wca.div_my.FYEDate,
wca.div_my.DivID,
wca.div_my.DivRescind,
wca.div_my.PeriodEndDate,
wca.div_my.Frequency,
wca.div_my.Marker,
wca.div_my.DeclarationDate,
wca.div_my.DeclCurenCD,
wca.div_my.DeclGrossAmt
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'United States'
and (wca.div_my.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
;