--filepath=O:\Datafeed\Debt\Iress\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Iress\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LCC') as TableName,
wca.lcc.Actflag,
wca.lcc.AnnounceDate as Created,
wca.lcc.Acttime as Changed,
wca.lcc.LccID,
wca.lcc.SecID,
wca.scmst.ISIN,
wca.lcc.ExchgCD,
wca.lcc.EffectiveDate,
wca.lcc.OldLocalCode,
wca.lcc.NewLocalCode,
wca.lcc.EventType,
wca.lcc.RelEventID
from wca.lcc
inner join wca.bond on wca.lcc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
(wca.scexh.ExchgCD = 'AUASX'
or wca.scexh.ExchgCD = 'AUBND'
or wca.scexh.ExchgCD = 'AUNSE'
or wca.scexh.ExchgCD = 'AUSIMV'
or wca.scexh.ExchgCD = 'NZBND'
or wca.scexh.ExchgCD = 'NZSE')
and wca.lcc.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
