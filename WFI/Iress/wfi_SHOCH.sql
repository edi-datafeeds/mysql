--filepath=O:\Datafeed\Debt\Iress\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SHOCH
--fileheadertext=EDI_SHOCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Iress\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT DISTINCT
upper('SHOCH') as TableName,
wca.shoch.Actflag,
wca.shoch.AnnounceDate as Created,
wca.shoch.Acttime as Changed,
wca.shoch.SecID,
wca.scmst.ISIN,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
wca.shoch.NewSos,
wca.shoch.ShochID as EventID,
wca.shoch.EventType,
wca.shoch.RelEventID,
wca.shoch.OldOutstandingDate,
wca.shoch.NewOutstandingDate,
wca.shoch.ShochNotes
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid 
where
(wca.scexh.ExchgCD = 'AUASX'
or wca.scexh.ExchgCD = 'AUBND'
or wca.scexh.ExchgCD = 'AUNSE'
or wca.scexh.ExchgCD = 'AUSIMV'
or wca.scexh.ExchgCD = 'NZBND'
or wca.scexh.ExchgCD = 'NZSE')
and wca.shoch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
