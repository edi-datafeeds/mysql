--filepath=O:\Datafeed\Cambridge\WorldQuant\In\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Cambridge_WorldQuant_Daily_Full
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\Cambridge\WorldQuant\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.sbnd.ISIN AS 'BndISIN',
wca.seqs.ISIN AS 'EqsISIN',
issur.IssType,
region.Region,
region.Country,
bond.CurenCD
from wca.issur
left outer join wca.scmst as seqs on wca.issur.issid = seqs.issid and 'eqs' = seqs.SecTyCD and 'D' <> seqs.Actflag AND 'I' <> seqs.statusflag
left outer join wca.scmst as sbnd on wca.issur.issid = sbnd.issid and 'bnd' = sbnd.SecTyCD and 'D' <> sbnd.Actflag AND 'I' <> sbnd.statusflag
left outer join wca.bond on sbnd.secid = bond.secid
left outer join wca.scexh on bond.secid = scexh.secid
left outer join wca.exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca.region on exchg.cntrycd = region.cntrycd
where
seqs.secid is not null
and sbnd.secid is not null
and (wca.sbnd.ISIN <> '' or wca.seqs.ISIN <> '')
and wca.scexh.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.bond.actflag <> 'D';