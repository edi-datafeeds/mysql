--filepath=O:\Datafeed\Debt\Concerto\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Concerto\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCCHG') as TableName,
wca.scchg.Actflag,
wca.scchg.AnnounceDate as Created,
wca.scchg.Acttime as Changed,
wca.scchg.ScChgID,
wca.scchg.SecID,
wca.scmst.ISIN,
wca.scchg.DateofChange,
wca.scchg.SecOldName,
wca.scchg.SecNewName,
wca.scchg.EventType,
wca.scchg.OldSectyCD,
wca.scchg.NewSectyCD,
wca.scchg.OldRegS144A,
wca.scchg.NewRegS144A,
wca.scchg.ScChgNotes as Notes
from wca.scchg
inner join wca.bond on wca.scchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
wca.scchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND (wca.scexh.ExchgCD = 'CHSSX' or wca.scexh.ExchgCD = 'CHBND')
AND wca.scmst.CurenCD = 'CHF';


