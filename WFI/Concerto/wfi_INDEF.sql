--filepath=O:\Datafeed\Debt\Concerto\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_INDEF
--fileheadertext=EDI_INDEF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Concerto\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select
'INDEF' as TableName,
wca.indef.Actflag,
wca.indef.AnnounceDate as Created,
wca.indef.Acttime as Changed,
wca.indef.IndefID,
wca.indef.SecID,
wca.scmst.ISIN,
wca.indef.DefaultType,
wca.indef.DefaultDate,
wca.indef.Notes
from wca.indef
inner join wca.bond on wca.indef.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
wca.indef.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND (wca.scexh.ExchgCD = 'CHSSX' or wca.scexh.ExchgCD = 'CHBND')
AND wca.scmst.CurenCD = 'CHF';



