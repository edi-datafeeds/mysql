--filepath=O:\Datafeed\Debt\Concerto\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Concerto\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('AGM') as TableName,
wca.agm.Actflag,
wca.agm.AnnounceDate as Created,
wca.agm.Acttime as Changed, 
wca.agm.AGMID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.agm.IssID,
wca.agm.AGMDate,
wca.agm.AGMEGM,
wca.agm.AGMNo,
wca.agm.FYEDate,
wca.agm.AGMTime,
wca.agm.Add1,
wca.agm.Add2,
wca.agm.Add3,
wca.agm.Add4,
wca.agm.Add5,
wca.agm.Add6,
wca.agm.City,
wca.agm.CntryCD,
wca.agm.BondSecID
FROM wca.agm
INNER JOIN wca.scmst ON wca.agm.BondSecID = wca.scmst.SecID
INNER JOIN wca.bond ON wca.agm.BondSecID = wca.bond.SecID
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
wca.agm.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND (wca.scexh.ExchgCD = 'CHSSX' or wca.scexh.ExchgCD = 'CHBND')
AND wca.scmst.CurenCD = 'CHF';

