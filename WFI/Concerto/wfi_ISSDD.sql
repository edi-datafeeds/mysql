--filepath=O:\Datafeed\Debt\Concerto\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_ISSDD
--fileheadertext=EDI_ISSDD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Concerto\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('ISSDD') as TableName,
wca.issdd.Actflag,
wca.issdd.AnnounceDate as Created,
wca.issdd.Acttime as Changed,
wca.issdd.IssID,
wca.issdd.StartDate,
wca.issdd.EndDate,
wca.issdd.IssuerDebtDefaultID,
wca.issdd.Notes
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
wca.issdd.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND (wca.scexh.ExchgCD = 'CHSSX' or wca.scexh.ExchgCD = 'CHBND')
AND wca.scmst.CurenCD = 'CHF';

