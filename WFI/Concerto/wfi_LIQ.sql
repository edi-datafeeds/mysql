--filepath=O:\Datafeed\Debt\Concerto\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Concerto\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LIQ') as TableName,
wca.liq.Actflag,
wca.liq.AnnounceDate as Created,
wca.liq.Acttime as Changed,
wca.liq.LiqID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.liq.IssID,
wca.liq.Liquidator,
wca.liq.LiqAdd1,
wca.liq.LiqAdd2,
wca.liq.LiqAdd3,
wca.liq.LiqAdd4,
wca.liq.LiqAdd5,
wca.liq.LiqAdd6,
wca.liq.LiqCity,
wca.liq.LiqCntryCD,
wca.liq.LiqTel,
wca.liq.LiqFax,
wca.liq.LiqEmail,
wca.liq.RdDate
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
where
wca.liq.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND (wca.scexh.ExchgCD = 'CHSSX' or wca.scexh.ExchgCD = 'CHBND')
AND wca.scmst.CurenCD = 'CHF';

