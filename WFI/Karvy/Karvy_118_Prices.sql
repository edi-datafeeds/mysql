--filepath=O:\Upload\Acc\118\feed\Prices\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Prices
--fileheadertext=EDI_Prices_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\118\feed\Prices\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode as pricefilesymbol,
prices.lasttrade.ISIN,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,	
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.Bidsize,
prices.lasttrade.Asksize,
prices.lasttrade.TradedVolume,	
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,	
prices.lasttrade.VolFlag,
prices.lasttrade.Issuername,	
prices.lasttrade.SecTyCD,
prices.lasttrade.SecurityDesc,	
prices.lasttrade.Sedol,
prices.lasttrade.Uscode,
prices.lasttrade.PrimaryExchgCD,	
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,	
prices.lasttrade.TotalTrades,	
prices.lasttrade.Comment
from prices.lasttrade
where
isin in(select code from client.pfisin where accid = 118);