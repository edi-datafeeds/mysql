--filepath=O:\Upload\Acc\387\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\387\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
'COSNT' as TableName,
wca.cosnt.Actflag,
wca.cosnt.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.cosnt.Acttime) THEN wca.rd.Acttime ELSE wca.cosnt.Acttime END as Changed,
wca.cosnt.RdID,
wca.scmst.SecID,
wca.rd.Recdate,
wca.scmst.ISIN,
wca.cosnt.ExpiryDate,
wca.cosnt.ExpiryTime,
SUBSTRING(wca.cosnt.TimeZone, 1,3) AS TimeZone,
wca.cosnt.CollateralRelease,
wca.cosnt.Currency,
wca.cosnt.Fee,
wca.cosnt.Notes
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='U')
and cosnt.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))