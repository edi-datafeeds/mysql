--filepath=O:\Upload\Acc\387\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\387\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('INTBC') as TableName,
wca.intbc.Actflag,
wca.intbc.AnnounceDate as Created,
wca.intbc.Acttime as Changed,
wca.intbc.IntbcID,
wca.intbc.SecID,
wca.scmst.ISIN,
wca.intbc.EffectiveDate,
wca.intbc.OldIntBasis,
wca.intbc.NewIntBasis,
wca.intbc.OldIntBDC,
wca.intbc.NewIntBDC
from wca.intbc
inner join wca.bond on wca.intbc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='U')
and intbc.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))