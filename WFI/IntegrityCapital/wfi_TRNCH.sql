--filepath=O:\Upload\Acc\387\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\387\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('TRNCH') as TableName,
wca.trnch.Actflag,
wca.trnch.AnnounceDate as Created,
wca.trnch.Acttime as Changed,
wca.trnch.TrnchID,
wca.trnch.SecID,
wca.scmst.ISIN,
wca.trnch.TrnchNumber,
wca.trnch.TrancheDate,
wca.trnch.TrancheAmount,
wca.trnch.ExpiryDate,
wca.trnch.Notes as Notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='U')
and trnch.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))