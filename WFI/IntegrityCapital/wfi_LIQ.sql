--filepath=O:\Upload\Acc\387\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\387\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LIQ') as TableName,
wca.liq.Actflag,
wca.liq.AnnounceDate as Created,
wca.liq.Acttime as Changed,
wca.liq.LiqID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.liq.IssID,
wca.liq.Liquidator,
wca.liq.LiqAdd1,
wca.liq.LiqAdd2,
wca.liq.LiqAdd3,
wca.liq.LiqAdd4,
wca.liq.LiqAdd5,
wca.liq.LiqAdd6,
wca.liq.LiqCity,
wca.liq.LiqCntryCD,
wca.liq.LiqTel,
wca.liq.LiqFax,
wca.liq.LiqEmail,
wca.liq.RdDate
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='U')
and liq.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))