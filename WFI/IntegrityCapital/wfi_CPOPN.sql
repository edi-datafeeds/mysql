--filepath=O:\Upload\Acc\387\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPN
--fileheadertext=EDI_CPOPN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\387\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


SELECT 
upper('CPOPN') as TableName,
wca.cpopn.Actflag,
wca.cpopn.AnnounceDate as Created,
wca.cpopn.Acttime as Changed,
wca.cpopn.CpopnID,
wca.cpopn.SecID,
wca.cpopn.CallPut, 
wca.cpopn.Notes as Notes 
from wca.cpopn
inner join wca.bond on wca.cpopn.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=387 and actflag='U')
and cpopn.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))