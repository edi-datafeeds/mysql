--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_Products_Coverage
--fileheadertext=Equity_Products_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT distinct
wca.region.Region,
#wca.exchg.cntrycd,
wca.region.Country,
wca.exchg.MIC AS 'MIC Code',
wca.exchg.ExchgName AS 'Exchange Name',
CASE WHEN wca.scmst.secid is not null THEN 'Yes' ELSE '-' END AS 'WCA',
CASE WHEN wca.scmst.secid is not null THEN 'Yes' ELSE '-' END AS 'WDI',
CASE WHEN wca.scmst.secid is not null THEN 'Yes' ELSE '-' END AS 'WSO',
CASE WHEN portfolio.ipo_exchanges.exchgcd is not null THEN 'Yes' ELSE '-' END AS 'IPO',
CASE WHEN prices.lasttrade.secid IS NOT null THEN 'Yes' ELSE '-' END AS 'EODP',
CASE WHEN prices.lasttrade.secid IS NOT null THEN 'Yes' ELSE '-' END AS 'ADJF'
FROM wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'D'<>ifnull(wca.scexh.liststatus,'D') and 'D'<>ifnull(wca.scexh.actflag,'D')
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
LEFT outer JOIN prices.lasttrade ON wca.scmst.secid = prices.lasttrade.secid
left outer JOIN portfolio.ipo_exchanges ON wca.scexh.exchgcd = portfolio.ipo_exchanges.exchgcd
WHERE
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and substring(wca.scexh.exchgcd,3,3)<>'BND'
and wca.scexh.exchgcd <> 'USTRCE'
group by wca.scexh.exchgcd;