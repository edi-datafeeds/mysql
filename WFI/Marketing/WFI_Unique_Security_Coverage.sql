--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Unique_Security_Coverage
--fileheadertext=WFI_Unique_Security_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
count(distinct wca.bond.SecID) as 'Security Count'
from
wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I';