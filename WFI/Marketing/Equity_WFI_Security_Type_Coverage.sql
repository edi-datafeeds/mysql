--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_WFI_Security_Type_Coverage
--fileheadertext=Equity_WFI_Security_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.SectyCD,
secty.SecurityDescriptor as 'Security Type',
count(distinct wca.scmst.SecID) as 'Security Count',
concat(count(distinct wca.scmst.secid)/(select count(distinct wca.scmst.secid)
from wca.scmst
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from
wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
group by scmst.SectyCD

union


select distinct
'Total' as SectyCD,
'' as 'Security Type',
count(distinct wca.scmst.SecID) as 'Security Count',
concat(count(distinct wca.scmst.secid)/(select count(distinct wca.scmst.secid)
from wca.scmst
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from
wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I';