--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_Region_Coverage
--fileheadertext=Equity_Region_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--# 
select distinct
wca.region.Region,
count(distinct wca.scmst.SecID) 'Security Count',
concat(count(distinct wca.scmst.secid, wca.region.Region)/(select count(distinct wca.scmst.secid, wca.region.Region)
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
group by wca.region.Region

union

select distinct
'Total' as Region,
count(distinct wca.scmst.SecID, wca.region.Region) 'Security Count',
concat(count(distinct wca.scmst.secid, wca.region.Region)/(select count(distinct wca.scmst.secid, wca.region.Region)
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I';