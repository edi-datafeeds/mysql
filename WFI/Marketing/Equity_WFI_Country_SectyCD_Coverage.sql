--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_WFI_Country_Security_Type_Coverage
--fileheadertext=Equity_WFI_Country_Security_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.exchg.CntryCD,
wca.region.Country,
wca.scmst.SectyCD,
secty.SecurityDescriptor as 'Security Type',
count(distinct wca.scmst.SecID) as 'Security Count',
concat(count(distinct wca.scmst.secid, wca.exchg.cntrycd)/(select count(distinct wca.scmst.secid, wca.exchg.cntrycd)
from wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from
wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
group by wca.region.Country, scmst.SectyCD


union


select distinct
'' as CntryCD,
'' as Country,
'Total' as SectyCD,
'' as 'Security Type',
count(distinct wca.scmst.SecID, wca.exchg.cntrycd) as 'Security Count',
concat(count(distinct wca.scmst.secid, wca.exchg.cntrycd)/(select count(distinct wca.scmst.secid, wca.exchg.cntrycd)
from wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from
wca.scmst
inner join wca.secty on wca.scmst.sectycd = wca.secty.SectyCD
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null;