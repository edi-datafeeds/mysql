--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Bond_Type_Coverage
--fileheadertext=WFI_Bond_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
BondTypeCD,
BondType,
SecurityCount as 'Security Count',
Percentage
from
(
select distinct 
case when bond.BondType = '' then 'BD' else bond.BondType end as BondTypeCD,
case when wca.lookup.lookup is null then 'Bond' else wca.lookup.lookup end as BondType,
count(distinct wca.bond.secid) as SecurityCount,
concat(count(distinct wca.bond.secid)/(select count(distinct wca.bond.secid)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.lookup on wca.bond.BondType = wca.lookup.code and wca.lookup.typegroup = 'BondType'
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.lookup on wca.bond.BondType = wca.lookup.code and wca.lookup.typegroup = 'BondType'
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
group by BondTypeCD
) as T1

union

select distinct 
'Total' as BondTypeCD,
'' as BondType,       
count(distinct wca.bond.secid) as SecurityCount,
concat(count(distinct wca.bond.secid)/(select count(distinct wca.bond.secid)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I';
