--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Country_Issuer_Type_Coverage
--fileheadertext=WFI_Country_Issuer_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.exchg.CntryCD,
wca.region.Country,
wca.issur.isstype as IssTypeCD,
wca.lookup.lookup as 'Issuer Type',
count(distinct wca.bond.SecID) as 'Security Count',
concat(count(distinct wca.bond.secid, wca.exchg.cntrycd)/(select count(distinct wca.bond.secid, wca.exchg.cntrycd)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
group by wca.region.Country, wca.issur.isstype


union


select distinct
'' as CntryCD,
'' as Country,
'Total' as IssTypeCD,
'' as 'Issuer Type',
count(distinct wca.bond.SecID, wca.exchg.cntrycd) as 'Security Count',
concat(count(distinct wca.bond.secid, wca.exchg.cntrycd)/(select count(distinct wca.bond.secid, wca.exchg.cntrycd)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null;