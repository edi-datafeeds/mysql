--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_Unique_Security_Coverage
--fileheadertext=Equity_Unique_Security_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
count(distinct wca.scmst.SecID) as 'Security Count'
from
wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
where
wca.scmst.actflag <> 'D' 
and wca.scmst.statusflag <> 'I';