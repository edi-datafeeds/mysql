-- arc=y
-- arp=N:\Internal\Marketing_Coverage\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Equity_Event_Coverage_Date
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=Equity_Event_Coverage_Date_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y


-- #
use wca;
-- #
SELECT 'Annual General Meeting' as Event, COUNT(distinct etab.agmid) as 'Event Count'
FROM agm as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Announcement' as Event, COUNT(distinct etab.annid) as 'Event Count'
FROM ann as etab
inner join scmst on etab.issid=scmst.issid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Bankruptcy' as Event, COUNT(distinct etab.bkrpid) as 'Event Count'
FROM bkrp as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Financial Year Change' as Event, COUNT(distinct etab.fychgid) as 'Event Count'
FROM fychg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Incorporation Change' as Event, COUNT(distinct etab.inchgid) as 'Event Count'
FROM inchg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Issuer Name Change' as Event, COUNT(distinct etab.ischgid) as 'Event Count'
FROM ischg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Lawsuits' as Event, COUNT(distinct etab.lawstid) as 'Event Count'
FROM lawst as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Liquidation' as Event, COUNT(distinct etab.liqid) as 'Event Count'
FROM liq as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

-- all rdid events

SELECT 'Arrangement' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM arr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Bonus' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM bon as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Bonus Rights' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM br as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Consolidation' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM consd as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Distribution' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM dist as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Dividend' as Event, COUNT(distinct etab.rdid) as 'Event Count'
FROM div_my as etab
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'De-Merger' as Event, COUNT(distinct etab.rdid) as 'Event Count'
FROM dmrgr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Divestment' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM dvst as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Entitlement Issue' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM ent as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Merger' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM mrgr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Purchase Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM po as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Preferential Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM prf as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Rights' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM rts as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Security Swap' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM scswp as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Sub Division' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM sd as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Odd Lot Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count' 
FROM oddlt as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

-- all divid events

SELECT 'Dividend Reinvestment Plan' as Event, COUNT(distinct etab.divid) as 'Event Count'
FROM drip as etab
inner join div_my on etab.divid=div_my.divid
inner join rd on div_my.rdid=rd.rdid
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Franking' as Event, COUNT(distinct etab.divid) as 'Event Count' 
FROM frank as etab
inner join div_my on etab.divid=div_my.divid
inner join rd on div_my.rdid=rd.rdid
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

-- all <tablename>ID events

SELECT 'Assimilation' as Event, COUNT(distinct etab.assmid) as 'Event Count' 
FROM assm as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Buy Back' as Event, COUNT(distinct etab.bbid) as 'Event Count' 
FROM bb as etab 
inner join scmst on etab.secid=scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Call' as Event, COUNT(distinct etab.callid) as 'Event Count' 
FROM call_my as etab
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Capital Reduction' as Event, COUNT(distinct etab.caprdid) as 'Event Count' 
FROM caprd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Conversion' as Event, COUNT(distinct etab.convid) as 'Event Count' 
FROM conv as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Certificate of Exchange' as Event, COUNT(distinct etab.ctxid) as 'Event Count' 
FROM ctx as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Currency Redenomination' as Event, COUNT(distinct etab.currdid) as 'Event Count' 
FROM currd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'International Code Change' as Event, COUNT(distinct etab.iccid) as 'Event Count' 
FROM icc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Par Value Denomination' as Event, COUNT(distinct etab.pvrdid) as 'Event Count' 
FROM pvrd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Return of Capital' as Event, COUNT(distinct etab.rcapid) as 'Event Count' 
FROM rcap as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Redemption' as Event, COUNT(distinct etab.redemid) as 'Event Count' 
FROM redem as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Security Name Change' as Event, COUNT(distinct etab.scchgid) as 'Event Count' 
FROM scchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'SEDOL Change' as Event, COUNT(distinct etab.sdchgid) as 'Event Count' 
FROM sdchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Security Reclassfication' as Event, COUNT(distinct etab.secrcid) as 'Event Count' 
FROM secrc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Take Over' as Event, COUNT(distinct etab.tkovrid) as 'Event Count' 
FROM tkovr as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Shares Outstanding Change' as Event, COUNT(distinct etab.shochid) as 'Event Count' 
FROM shoch as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

--  all listing level events

SELECT 'New Listing' as Event, COUNT(distinct etab.scexhid) as 'Event Count' 
FROM nlist as etab 
inner join scexh on etab.scexhid=scexh.scexhid 
inner join scmst on scexh.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Local Code Change' as Event, COUNT(distinct etab.lccid) as 'Event Count' 
FROM lcc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Listing Status' as Event, COUNT(distinct etab.lstatid) as 'Event Count' 
FROM lstat as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'Lot Change' as Event, COUNT(distinct etab.ltchgid) as 'Event Count' 
FROM ltchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'

union

SELECT 'IPO' as Event, COUNT(distinct etab.ipoid) as 'Event Count' 
FROM ipo as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
