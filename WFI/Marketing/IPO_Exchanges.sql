use portfolio;
DROP TABLE IF EXISTS `ipo_exchanges`;
CREATE TABLE  `ipo_exchanges` (
  `ExchgCD` varchar(9) NOT NULL DEFAULT '',  
  PRIMARY KEY (`ExchgCD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


insert ignore into portfolio.ipo_exchanges
SELECT DISTINCT
wca.scexh.exchgcd
FROM wca.scmst
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'D'<>ifnull(wca.scexh.liststatus,'D') and 'D'<>ifnull(wca.scexh.actflag,'D')
inner JOIN wca.ipo ON wca.scmst.secid = wca.ipo.secid and wca.scexh.exchgcd=ifnull(wca.scmst.primaryexchgcd,'') and 'D'<>ifnull(wca.ipo.actflag,'D')
WHERE
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
group by wca.scexh.exchgcd