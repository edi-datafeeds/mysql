-- arc=y
-- arp=N:\Internal\Marketing_Coverage\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_WFI_Event_Coverage_Date
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=WFI_Event_Coverage_Date_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y


-- #

SELECT distinct
'Annual General Meeting' as Event,
count(distinct wca.agm.AGMID) as 'Event Count'
FROM wca.agm
INNER JOIN wca.scmst ON wca.agm.BondSecID = wca.scmst.SecID
INNER JOIN wca.bond ON wca.agm.BondSecID = wca.bond.SecID
where
agm.announcedate BETWEEN @fdate AND @tdate
and wca.agm.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Auction' as Event,
count(distinct wca.auct.auctionid) as 'Event Count'
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
auct.announcedate BETWEEN @fdate AND @tdate
and wca.auct.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Bankruptcy' as Event,
count(distinct wca.bkrp.bkrpid) as 'Event Count'
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
bkrp.announcedate BETWEEN @fdate AND @tdate
and wca.bkrp.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Bond Liquidation' as Event,
count(distinct wca.bndlq.bndlqid) as 'Event Count'
from wca.bndlq
inner join wca.rd on wca.bndlq.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
bndlq.announcedate BETWEEN @fdate AND @tdate
and wca.bndlq.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Conversion' as Event,
count(distinct wca.conv.convid) as 'Event Count'
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
conv.announcedate BETWEEN @fdate AND @tdate
and wca.conv.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Consent' as Event,
count(distinct wca.cosnt.RdID) as 'Event Count'
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
cosnt.announcedate BETWEEN @fdate AND @tdate
and wca.cosnt.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Currency Redenomination' as Event,
count(distinct wca.currd.CurrdID) as 'Event Count'
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
currd.announcedate BETWEEN @fdate AND @tdate
and wca.currd.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Dividend' as Event,
count(distinct wca.div_my.DivID) as 'Event Count'
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
div_my.announcedate BETWEEN @fdate AND @tdate
and wca.div_my.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'In Default' as Event,
count(distinct wca.indef.IndefID) as 'Event Count'
from wca.indef
inner join wca.bond on wca.indef.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
indef.announcedate BETWEEN @fdate AND @tdate
and wca.indef.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Interest Payment Announcement' as Event,
count(distinct wca.int_my.RdID) as 'Event Count'
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
(intpy.announcedate BETWEEN @fdate AND @tdate
and wca.intpy.Actflag = 'I' and wca.scmst.statusflag <> 'I'
or int_my.announcedate BETWEEN @fdate AND @tdate
and wca.int_my.Actflag = 'I' and wca.scmst.statusflag <> 'I'
or rd.announcedate BETWEEN @fdate AND @tdate
and wca.rd.Actflag = 'I' and wca.scmst.statusflag <> 'I'
or exdt.announcedate BETWEEN @fdate AND @tdate
and wca.exdt.Actflag = 'I' and wca.scmst.statusflag <> 'I')


union


select distinct
'Issuer Debt Default' as Event,
count(distinct wca.issdd.IssuerDebtDefaultID) as 'Event Count'
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
issdd.announcedate BETWEEN @fdate AND @tdate
and wca.issdd.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Law Suit(Class Action)' as Event,
count(distinct wca.lawst.LawstID) as 'Event Count'
from wca.lawst
inner join wca.scmst on wca.lawst.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
lawst.announcedate BETWEEN @fdate AND @tdate
and wca.lawst.Actflag = 'I' and wca.scmst.statusflag <> 'I'



union


select distinct
'Liquidation' as Event,
count(distinct wca.liq.LiqID) as 'Event Count'
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
liq.announcedate BETWEEN @fdate AND @tdate
and wca.liq.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Multiple Payment' as Event,
count(distinct wca.mpay.EventID) as 'Event Count'
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
mpay.announcedate BETWEEN @fdate AND @tdate
and wca.mpay.Actflag = 'I' and wca.scmst.statusflag <> 'I'


union


select distinct
'Redemption' as Event,
count(distinct wca.redem.RedemID) as 'Event Count'
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
redem.announcedate BETWEEN @fdate AND @tdate
and wca.redem.Actflag = 'I' and wca.scmst.statusflag <> 'I'
