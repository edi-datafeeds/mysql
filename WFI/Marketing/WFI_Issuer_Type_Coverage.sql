--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Issuer_Type_Coverage
--fileheadertext=WFI_Issuer_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.issur.isstype as IssTypeCD,
wca.lookup.lookup as 'Issuer Type',
count(distinct wca.bond.SecID) as 'Security Count',
concat(count(distinct wca.bond.secid)/(select count(distinct wca.bond.secid)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
group by wca.issur.isstype

union

select distinct
'Total' as IssTypeCD,
'' as 'Issuer Type',
count(distinct wca.bond.SecID) as 'Security Count',
concat(count(distinct wca.bond.secid)/(select count(distinct wca.bond.secid)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.lookup on wca.issur.isstype = wca.lookup.code and (wca.lookup.typegroup = 'ISSTYPE' or wca.lookup.typegroup = 'ISSTYP')
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.issur.actflag <> 'D'
and wca.scmst.statusflag <> 'I'