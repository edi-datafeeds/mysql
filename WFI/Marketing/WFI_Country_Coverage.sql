--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Country_Coverage
--fileheadertext=WFI_Country_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.region.CntryCD,
wca.region.Country,
count(distinct wca.bond.SecID) 'Security Count',
concat(count(distinct wca.bond.SecID, wca.region.Country)/(select count(distinct wca.bond.SecID, wca.region.Country)
from
wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
group by wca.region.Country

union

select distinct
'Total' as CntryCD,
'' as Country,
count(distinct wca.bond.SecID, wca.region.Country) 'Security Count',
concat(count(distinct wca.bond.SecID, wca.region.Country)/(select count(distinct wca.bond.SecID, wca.region.Country)
from
wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I';
