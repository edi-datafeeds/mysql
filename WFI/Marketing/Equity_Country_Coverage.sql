--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Equity_Country_Coverage
--fileheadertext=Equity_Country_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.exchg.CntryCD,
wca.region.Country,
count(distinct wca.scmst.SecID) 'Security Count',
concat(count(distinct wca.scmst.SecID, wca.region.Country)/(select count(distinct wca.scmst.SecID, wca.region.Country)
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
group by wca.region.Country

union

select distinct
'Total' as CntryCD,
'' as Country,
count(distinct wca.scmst.SecID, wca.region.Country) 'Security Count',
concat(count(distinct wca.scmst.SecID, wca.region.Country)/(select count(distinct wca.scmst.SecID, wca.region.Country)
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I'
)*100, '%') as Percentage
from wca.scmst
inner join wca.sectygrp on wca.scmst.sectycd = wca.sectygrp.sectycd and 3> sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.scmst.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and wca.scmst.statusflag <> 'I';