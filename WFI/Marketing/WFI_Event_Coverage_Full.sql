--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Event_Coverage_Full
--fileheadertext=WFI_Event_Coverage_Full_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT distinct
'Annual General Meeting' as Event,
count(distinct wca.agm.AGMID) as 'Event Count'
FROM wca.agm
INNER JOIN wca.scmst ON wca.agm.BondSecID = wca.scmst.SecID
INNER JOIN wca.bond ON wca.agm.BondSecID = wca.bond.SecID
where
wca.agm.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Auction' as Event,
count(distinct wca.auct.auctionid) as 'Event Count'
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.auct.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'

union


select distinct
'Bankruptcy' as Event,
count(distinct wca.bkrp.bkrpid) as 'Event Count'
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bkrp.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Bond Liquidation' as Event,
count(distinct wca.bndlq.bndlqid) as 'Event Count'
from wca.bndlq
inner join wca.rd on wca.bndlq.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bndlq.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Conversion' as Event,
count(distinct wca.conv.convid) as 'Event Count'
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.conv.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Consent' as Event,
count(distinct wca.cosnt.RdID) as 'Event Count'
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.cosnt.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Currency Redenomination' as Event,
count(distinct wca.currd.CurrdID) as 'Event Count'
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.currd.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Dividend' as Event,
count(distinct wca.div_my.DivID) as 'Event Count'
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.div_my.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'In Default' as Event,
count(distinct wca.indef.IndefID) as 'Event Count'
from wca.indef
inner join wca.bond on wca.indef.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.indef.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Interest Payment Announcement' as Event,
count(distinct wca.int_my.RdID) as 'Event Count'
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
wca.intpy.Actflag <> 'D'
and wca.int_my.Actflag <> 'D'
and wca.rd.Actflag <> 'D'
and wca.exdt.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union



select distinct
'Issuer Debt Default' as Event,
count(distinct wca.issdd.IssuerDebtDefaultID) as 'Event Count'
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issdd.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Law Suit(Class Action)' as Event,
count(distinct wca.lawst.LawstID) as 'Event Count'
from wca.lawst
inner join wca.scmst on wca.lawst.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.lawst.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Liquidation' as Event,
count(distinct wca.liq.LiqID) as 'Event Count'
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.liq.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Multiple Payment' as Event,
count(distinct wca.mpay.EventID) as 'Event Count'
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.mpay.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'


union


select distinct
'Redemption' as Event,
count(distinct wca.redem.RedemID) as 'Event Count'
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
wca.redem.Actflag <> 'D'
and wca.scmst.statusflag <> 'I'