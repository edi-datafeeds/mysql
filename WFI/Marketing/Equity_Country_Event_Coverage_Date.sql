-- arc=y
-- arp=N:\Internal\Marketing_Coverage\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_Equity_Country_Event_Coverage_Date
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=Equity_Country_Event_Coverage_Date_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE
-- dfo=y


-- #
use wca;
-- #
SELECT 'Annual General Meeting' as Event, COUNT(distinct etab.agmid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM agm as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Announcement' as Event, COUNT(distinct etab.annid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM ann as etab
inner join scmst on etab.issid=scmst.issid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Bankruptcy' as Event, COUNT(distinct etab.bkrpid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM bkrp as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Financial Year Change' as Event, COUNT(distinct etab.fychgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM fychg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Incorporation Change' as Event, COUNT(distinct etab.inchgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM inchg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Issuer Name Change' as Event, COUNT(distinct etab.ischgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM ischg as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Lawsuits' as Event, COUNT(distinct etab.lawstid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM lawst as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Liquidation' as Event, COUNT(distinct etab.liqid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM liq as etab
inner join scmst on etab.issid=scmst.issid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

-- all rdid events

SELECT 'Arrangement' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM arr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Bonus' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM bon as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Bonus Rights' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM br as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Consolidation' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM consd as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Distribution' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM dist as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Dividend' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM div_my as etab
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'De-Merger' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM dmrgr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Divestment' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM dvst as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Entitlement Issue' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM ent as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Merger' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM mrgr as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Purchase Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM po as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Preferential Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM prf as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Rights' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM rts as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Security Swap' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM scswp as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Sub Division' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM sd as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Odd Lot Offer' as Event, COUNT(distinct etab.rdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM oddlt as etab 
inner join rd on etab.rdid=rd.rdid 
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

-- all divid events

SELECT 'Dividend Reinvestment Plan' as Event, COUNT(distinct etab.divid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country
FROM drip as etab
inner join div_my on etab.divid=div_my.divid
inner join rd on div_my.rdid=rd.rdid
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Franking' as Event, COUNT(distinct etab.divid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM frank as etab
inner join div_my on etab.divid=div_my.divid
inner join rd on div_my.rdid=rd.rdid
inner join scmst on rd.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

-- all <tablename>ID events

SELECT 'Assimilation' as Event, COUNT(distinct etab.assmid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM assm as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Buy Back' as Event, COUNT(distinct etab.bbid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM bb as etab 
inner join scmst on etab.secid=scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Call' as Event, COUNT(distinct etab.callid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM call_my as etab
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Capital Reduction' as Event, COUNT(distinct etab.caprdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM caprd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Conversion' as Event, COUNT(distinct etab.convid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM conv as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Certificate of Exchange' as Event, COUNT(distinct etab.ctxid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM ctx as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Currency Redenomination' as Event, COUNT(distinct etab.currdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM currd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'International Code Change' as Event, COUNT(distinct etab.iccid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM icc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Par Value Denomination' as Event, COUNT(distinct etab.pvrdid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM pvrd as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Return of Capital' as Event, COUNT(distinct etab.rcapid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM rcap as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Redemption' as Event, COUNT(distinct etab.redemid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM redem as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Security Name Change' as Event, COUNT(distinct etab.scchgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM scchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'SEDOL Change' as Event, COUNT(distinct etab.sdchgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM sdchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Security Reclassfication' as Event, COUNT(distinct etab.secrcid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM secrc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Take Over' as Event, COUNT(distinct etab.tkovrid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM tkovr as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Shares Outstanding Change' as Event, COUNT(distinct etab.shochid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM shoch as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

--  all listing level events

SELECT 'New Listing' as Event, COUNT(distinct etab.scexhid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM nlist as etab 
inner join scexh on etab.scexhid=scexh.scexhid 
inner join scmst on scexh.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Local Code Change' as Event, COUNT(distinct etab.lccid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM lcc as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Listing Status' as Event, COUNT(distinct etab.lstatid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM lstat as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'Lot Change' as Event, COUNT(distinct etab.ltchgid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM ltchg as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country

union

SELECT 'IPO' as Event, COUNT(distinct etab.ipoid) as 'Event Count',
wca.exchg.CntryCD,
wca.region.Country 
FROM ipo as etab 
inner join scmst on etab.secid=scmst.secid 
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where etab.announcedate BETWEEN @fdate AND @tdate
and scmst.statusflag <> 'I' and etab.Actflag = 'I'
and wca.exchg.cntrycd is not NULL
GROUP BY wca.region.Country
