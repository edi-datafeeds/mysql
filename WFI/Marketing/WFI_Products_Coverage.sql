--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Products_Coverage
--fileheadertext=WFI_Products_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT distinct
wca.region.Region,
#wca.exchg.cntrycd,
wca.region.Country,
wca.exchg.MIC AS 'MIC Code',
wca.exchg.ExchgName AS 'Exchange Name',
CASE WHEN wca.scmst.secid is not null THEN 'Yes' ELSE '-' END AS 'WFI',
CASE WHEN prices.lasttrade.secid IS NOT null THEN 'Yes' ELSE '-' END AS 'EODP',
CASE WHEN prices.lasttrade.secid IS NOT null THEN 'Yes' ELSE '-' END AS 'ADJF'
FROM wca.scmst
inner join wca.bond ON wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid and 'D'<>ifnull(wca.scexh.liststatus,'D') and 'D'<>ifnull(wca.scexh.actflag,'D')
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
LEFT outer JOIN prices.lasttrade ON wca.scmst.secid = prices.lasttrade.secid
WHERE
wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and substring(wca.scexh.exchgcd,3,3)<>'BND'
group by wca.scexh.exchgcd;