--filepath=O:\Prodman\Dev\WFI\Feeds\Internal\Marketing\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WFI_Country_Bond_Type_Coverage
--fileheadertext=WFI_Country_Bond_Type_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Internal\Marketing_Coverage\
--fieldheaders=Y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select DISTINCT
CntryCD,
Country,
BondTypeCD,
BondType,
SecurityCount as 'Security Count',
Percentage
from
(
select DISTINCT
wca.exchg.CntryCD,
wca.region.Country, 
case when bond.BondType = '' then 'BD' else bond.BondType end as BondTypeCD,
case when wca.lookup.lookup is null then 'Bond' else wca.lookup.lookup end as BondType,
count(distinct wca.bond.secid) as SecurityCount,
concat(count(distinct wca.bond.secid, wca.exchg.cntrycd)/(select count(distinct wca.bond.secid, wca.exchg.cntrycd)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.lookup on wca.bond.BondType = wca.lookup.code and wca.lookup.typegroup = 'BondType'
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.lookup on wca.bond.BondType = wca.lookup.code and wca.lookup.typegroup = 'BondType'
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
group by wca.region.Country, BondTypeCD
) as T1

union

select DISTINCT
'' as CntryCD,
'' as Country, 
'Total' as BondTypeCD,
'' as BondType,       
count(distinct wca.bond.secid, wca.exchg.cntrycd) as SecurityCount,
concat(count(distinct wca.bond.secid, wca.exchg.cntrycd)/(select count(distinct wca.bond.secid, wca.exchg.cntrycd)
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null
)*100, '%') as Percentage
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.region on ifnull(wca.exchg.cntrycd,'UB') = wca.region.cntrycd
where
wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.statusflag <> 'I'
and wca.exchg.cntrycd is not null;