--filepath=O:\Prodman\Dev\WFI\Feeds\Markit\Output\SRF\Full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ENSO_Full
--fileheadertext=EDI_WFI_ENSO_Full_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
SELECT DISTINCT
wca.bond.Actflag,
wca.bond.AnnounceDate AS Created,
CASE
	WHEN (bond.acttime > scmst.acttime)
        AND (bond.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (bond.acttime > issur.acttime)
        AND (bond.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN bond.acttime
    WHEN
        (scmst.acttime > IFNULL(scexh.acttime, '2000-01-01'))
        AND (scmst.acttime > issur.acttime)
        AND (scmst.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scmst.acttime
    WHEN
        (scexh.acttime > issur.acttime)
        AND (scexh.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN scexh.acttime
    WHEN
        (issur.acttime > IFNULL(exchg.acttime, '2000-01-01')) THEN issur.acttime
END AS Changed,
wca.scmst.SecID,
wca.scmst.IssID,
CASE
    WHEN wca.bbe.BbeID IS NULL THEN 0
    ELSE wca.bbe.BbeID
END AS BbeID,
CASE
    WHEN wca.scexh.ScexhID IS NULL THEN 0
    ELSE wca.scexh.ScexhID
END AS ScexhID,
    
    
CASE						      
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.SedolID
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.SedolID
    ELSE '0'
END AS SedolID,

  
CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.actflag
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.actflag
    ELSE ''
END AS SedolActflag,   
wca.scmst.ISIN,
wca.scmst.USCode,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' THEN xsedol.Sedol
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' THEN bsedol.Sedol
    ELSE ''
END as Sedol,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.Defunct
    WHEN bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.Defunct
    ELSE ''
END as SedolDefunct,


CASE
    WHEN xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.rcntrycd
    ELSE ''
END as SedolRegCntryCD,

wca.bbe.BbgexhId,
wca.bbe.Bbgexhtk,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.scmst.SecurityDesc,
wca.scmst.CurenCD,
wca.scmst.SectyCD,
wca.issur.IssuerName,
wca.issur.GICS,
wca.issur.CntryofIncorp,
wca.issur.isstype,
wca.bond.InterestRate,
wca.bond.MatPrice,
CASE
    WHEN wca.bond.largeparvalue <> '' THEN wca.bond.largeparvalue
    WHEN wca.bond.parvalue <> '' THEN wca.bond.parvalue
    ELSE wca.scmst.parvalue
END AS NominalValue,
wca.bond.OutstandingAmount,
wca.scmst.ParValue,
wca.bondx.DenominationMultiple,
wca.bond.MaturityDate,
wca.bond.MaturityStructure,
wca.issur.IndusID,
wca.scmst.CFI,
wca.issur.CIK,
wca.issur.SIC,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.SeniorJunior,
wca.bond.Subordinate,

wca.bond.guaranteed,
CASE
    WHEN wca.scagy.Relationship <> 'GUR' THEN ''
    ELSE wca.scagy.Relationship
END AS Relationship,
wca.agncy.RegistrarName,     


CASE
    WHEN wca.scmst.statusflag = '' THEN 'A'
    ELSE wca.scmst.statusflag
END AS StatusFlag
FROM wca.bond
INNER JOIN wca.scmst ON wca.bond.secid = wca.scmst.secid
LEFT OUTER JOIN wca.bondx ON wca.bond.secid = wca.bondx.secid
INNER JOIN wca.issur ON wca.scmst.issid = wca.issur.issid

LEFT OUTER JOIN wca.scagy on wca.bond.secid = wca.scagy.secid
LEFT OUTER JOIN wca.agncy on wca.scagy.agncyid = wca.agncy.agncyid

LEFT OUTER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
AND 'D' <> scexh.actflag
LEFT OUTER JOIN wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd


LEFT OUTER JOIN wca.sedol as xsedol ON wca.scexh.secid = xsedol.secid
AND wca.exchg.cntrycd = xsedol.cntrycd
AND 'D' <> xsedol.actflag
  
LEFT OUTER JOIN wca.sedol as bsedol ON wca.bond.secid = bsedol.secid
AND 'zz' = bsedol.cntrycd  
AND 'D' <> bsedol.actflag


LEFT OUTER JOIN wca.bbe ON wca.scmst.secid = wca.bbe.secid
AND 'D' <> wca.bbe.actflag

WHERE
     -- wca.scmst.statusflag <> 'I'
     -- AND (wca.scexh.liststatus <> 'D'
     --   OR wca.scexh.liststatus IS NULL)
     wca.scmst.actflag <> 'D'
     AND wca.bond.actflag <> 'D'
     AND (wca.scexh.actflag <> 'D'
        OR wca.scexh.actflag IS NULL)
     AND wca.issur.actflag <> 'D'
     -- AND wca.scagy.actflag <> 'D'
     -- AND wca.agncy.actflag <> 'D'