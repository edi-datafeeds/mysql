--filepath=O:\Datafeed\Debt\Rockall\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_Rockall_Delta
--fileheadertext=EDI_Rockall_Delta_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Rockall\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
CASE
    WHEN wca.bond.acttime > wca.scmst.acttime AND wca.bond.acttime > wca.scexh.acttime then wca.bond.acttime
    WHEN wca.scmst.acttime > wca.bond.acttime AND wca.scmst.acttime > wca.scexh.acttime then wca.scmst.acttime
    WHEN wca.scexh.acttime > wca.scmst.acttime AND wca.scexh.acttime > wca.bond.acttime then wca.scexh.acttime
    ELSE wca.scmst.acttime
END AS Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.USCode,
wca.scexh.ExchgCD,
wca.scexh.LocalCode,
wca.bond.Municipal,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.bond.MaturityDate,
wca.bond.BondType,
wca.bond.MaturityStructure,
wca.issur.Isstype,
wca.scmst.Holding,
wca.bond.InterestBasis,
wca.scmst.SectyCD,
wca.issur.IssID,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.ExchgCD like 'US%'
and (wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'));
