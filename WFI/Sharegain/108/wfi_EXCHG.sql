--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_EXCHG
--fileheadertext=EDI_EXCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('EXCHG') as TableName,
wca.exchg.Actflag,
wca.exchg.AnnounceDate as Created,
wca.exchg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.exchg.ExchgCD,
wca.exchg.ExchgName,
wca.exchg.CntryCD,
wca.exchg.WklyDayOff1,
wca.exchg.WklyDayOff2,
wca.exchg.MIC,
wca.exchg.ExchgID
from wca.exchg
inner join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and exchg.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))