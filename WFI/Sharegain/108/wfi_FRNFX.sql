--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.frnfx.Actflag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnfxID,
wca.frnfx.SecID,
wca.scmst.ISIN,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFRNType,
wca.frnfx.OldFRNIndexBenchmark,
wca.frnfx.OldMarkup As OldFrnMargin,
wca.frnfx.OldMinimumInterestRate,
wca.frnfx.OldMaximumInterestRate,
wca.frnfx.OldRounding,
wca.frnfx.NewFRNType,
wca.frnfx.NewFRNindexBenchmark,
wca.frnfx.NewMarkup As NewFrnMargin,
wca.frnfx.NewMinimumInterestRate,
wca.frnfx.NewMaximumInterestRate,
wca.frnfx.NewRounding,
wca.frnfx.Eventtype,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and frnfx.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))