--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CURRD') as TableName,
wca.currd.Actflag,
wca.currd.AnnounceDate as Created,
wca.currd.Acttime as Changed,
wca.currd.CurrdID,
wca.currd.SecID,
wca.scmst.ISIN,
wca.currd.EffectiveDate,
wca.currd.OldCurenCD,
wca.currd.NewCurenCD,
wca.currd.OldParValue,
wca.currd.NewParValue,
wca.currd.EventType,
wca.currd.CurRdNotes as Notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and currd.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))