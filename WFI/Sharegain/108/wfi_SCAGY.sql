--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCAGY') as TableName,
wca.scagy.Actflag,
wca.scagy.AnnounceDate as Created,
wca.scagy.Acttime as Changed,
wca.scagy.ScagyID,
wca.scagy.SecID,
wca.scmst.ISIN,
wca.scagy.Relationship,
wca.scagy.AgncyID,
wca.scagy.GuaranteeType,
wca.scagy.SpStartDate,
wca.scagy.SpEndDate,
wca.scagy.Notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and scagy.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))