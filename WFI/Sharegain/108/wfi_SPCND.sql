--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPCND
--fileheadertext=EDI_SPCND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPCND') as TableName,
spcnd.Actflag,
spcnd.AnnounceDate as Created,
spcnd.Acttime as Changed,
scmst.SecID,
spcnd.SpcndID,
spcnd.Markup,
spcnd.UnderlyingCondition,
spcnd.WithReferenceTo,
spcnd.Level1Currency,
spcnd.Level1Percent,
spcnd.Level1Value,
spcnd.Level2Currency,
spcnd.Level2Percent,
spcnd.Level2Value
from wca.spcnd
inner join wca.spun on wca.spcnd.SpunID = wca.spun.SpunID
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and spcnd.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))