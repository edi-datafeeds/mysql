--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SP
--fileheadertext=EDI_SP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SP') as TableName,
sp.Actflag,
sp.AnnounceDate as Created,
sp.Acttime as Changed,
sp.SecID,
sp.StructureName,
sp.Strategy,
sp.UnderlyingClassification,
sp.SettlementCurrency,
sp.ClassificationAgency,
sp.ClassificationCode,
sp.IssuerBrand,
sp.IssuerProductName,
sp.Feature1,
sp.Feature2,
sp.Feature3,
sp.Feature4,
sp.Feature5,
sp.Feature6,
sp.Feature7,
sp.ExpiryTimeZone,
sp.OfferOpenDate,
sp.OfferOpenTime,
sp.OfferCloseDate,
sp.OfferCloseTime,
sp.InitialFixingDate,
sp.InitialFixingTime,
sp.FinalFixingDate,
sp.FinalFixingTime,
sp.StrikeDate,
sp.FirstTradeDate,
sp.LastTradeDate,
sp.BondFloor,
sp.QuoteStyle,
sp.CapitalProtectionLevel,
sp.ConditionalCapitalProtection,
sp.ProtectionCushion,
sp.ParticipationFactor1,
sp.ParticipationType1,
sp.ParticipationFactor2,
sp.ParticipationType2,
sp.UnderlyingBasketCapLevel,
sp.TenureMaxYield,
sp.AnnualisedMaxYield,
sp.QuantoCurrency,
sp.Notes
from wca.sp
inner join wca.bond on wca.sp.secid = wca.bond.secid
inner join wca.scmst on wca.sp.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and sp.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))