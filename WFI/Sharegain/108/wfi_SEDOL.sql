--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SEDOL') as TableName,
wca.sedol.Actflag,
wca.sedol.AnnounceDate as Created,
wca.sedol.Acttime as Changed, 
wca.sedol.SedolId,
wca.sedol.SecID,
wca.scmst.ISIN,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD 
from wca.sedol
inner join wca.bond on wca.sedol.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and sedol.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))