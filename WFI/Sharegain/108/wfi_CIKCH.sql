--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CIKCH
--fileheadertext=EDI_CIKCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CIKCH') as TableName,
wca.cikch.Actflag,
wca.cikch.AnnounceDate as Created,
wca.cikch.Acttime as Changed,
wca.cikch.IssID,
wca.cikch.EffectiveDate,
wca.cikch.OldCIK,
wca.cikch.NewCIK,
wca.cikch.CIKChID
from wca.cikch
inner join wca.scmst on wca.cikch.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and cikch.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))