--filepath=O:\Upload\Acc\108\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\108\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('BKRP') as TableName,
wca.bkrp.Actflag,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,  
wca.bkrp.BkrpID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bkrp.IssID,
wca.bkrp.NotificationDate,
wca.bkrp.FilingDate
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=108 and actflag<>'D')
and bkrp.acttime >= (select date_sub(max(feeddate), INTERVAL 6 MONTH) from wca.tbl_Opslog))