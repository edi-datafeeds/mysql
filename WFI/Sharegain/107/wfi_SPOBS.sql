--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPOBS
--fileheadertext=EDI_SPOBS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPOBS') as TableName,
spobs.Actflag,
spobs.AnnounceDate as Created,
spobs.Acttime as Changed,
spobs.SecID,
spobs.SpobsID,
spobs.ObservationPayoutType,
spobs.ObservationFromDate,
spobs.ObservaionToDate,
spobs.ObservationDays,
spobs.ObservationDaysConvention,
spobs.ObservationBusDayConv,
spobs.DeterminationFromDate,
spobs.DeterminationToDate,
spobs.DeterminationDays,
spobs.DeterminationDaysConvention,
spobs.DeterminationBusDayConv,
spobs.PayoutDate,
spobs.PayoutDays,
spobs.PayouttDaysConvention,
spobs.PayoutBusDayConv,
spobs.FixedPayoutCurrency,
spobs.FixedPayoutValue,
spobs.FixedPayoutPercent,
spobs.MinimumPayoutCurrency,
spobs.MinimumPayoutValue,
spobs.MinimumPayoutPercent,
spobs.MaximumPayoutCurrency,
spobs.MaximumPayoutValue,
spobs.MaximumPayoutPercent,
spobs.Multiplier,
spobs.TriggerAction,
spobs.BonusCurrency,
spobs.BonusValue,
spobs.BonusLevelPercent,
spobs.OptionStyle
from wca.spobs
inner join wca.scmst on wca.spobs.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=107 and actflag<>'D')
and spobs.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))