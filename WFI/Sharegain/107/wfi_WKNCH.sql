--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('WKNCH') as TableName,
wca.wknch.Actflag,
wca.wknch.AnnounceDate as Created,
wca.wknch.Acttime as Changed,
wca.wknch.SecID,
wca.wknch.EffectiveDate,
wca.wknch.WknChID,
wca.wknch.Eventtype,
wca.wknch.RelEventID,
wca.wknch.OldWKN,
wca.wknch.NewWKN
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=107 and actflag<>'D')
and wknch.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))