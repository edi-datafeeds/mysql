--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SHOCH
--fileheadertext=EDI_SHOCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SHOCH') as Tablename,
wca.shoch.ActFlag,
wca.shoch.AnnounceDate as Created,
wca.shoch.Acttime as Changed,
wca.shoch.SecID,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
wca.shoch.NewSos,
wca.shoch.ShochID,
wca.shoch.ShochNotes,
wca.shoch.EventType,
wca.shoch.RelEventID,
wca.shoch.OldOutstandingDate,
wca.shoch.NewOutstandingDate
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=107 and actflag<>'D')
and shoch.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))