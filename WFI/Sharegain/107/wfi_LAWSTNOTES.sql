--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LAWSTNOTES') as TableName,
wca.lawst.Actflag,
wca.lawst.LawstID,
wca.lawst.IssId,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
lawst.Issid in (select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where (client.pfisin.accid=107 and client.pfisin.actflag='I')
or (client.pfisin.accid=107 and client.pfisin.actflag<>'D'
and LAWST.acttime >= (select date_sub(max(feeddate), INTERVAL 6 MONTH) from wca.tbl_Opslog)))