--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
'INTPY' as TableName,
CASE WHEN wca.int_my.Actflag = 'D' or wca.int_my.actflag='C' or wca.intpy.actflag is null THEN wca.int_my.Actflag ELSE wca.intpy.Actflag END as Actflag,
wca.int_my.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.int_my.Acttime) and (wca.rd.Acttime > wca.exdt.Acttime) THEN wca.rd.Acttime WHEN (wca.exdt.Acttime is not null) and (wca.exdt.Acttime > wca.int_my.Acttime) THEN wca.exdt.Acttime ELSE wca.int_my.Acttime END as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.intpy.BCurenCD as DebtCurrency,
wca.intpy.BParValue as NominalValue,
wca.scexh.ExchgCD,
wca.int_my.RdID,
case when wca.intpy.optionid is null then '1' else wca.intpy.optionid end as OptionID,
wca.rd.Recdate,
wca.exdt.Exdate,
wca.exdt.Paydate,
wca.int_my.InterestFromDate,
wca.int_my.InterestToDate,
wca.int_my.Days,
wca.intpy.CurenCD as Interest_Currency,
wca.intpy.IntRate as Interest_Rate,
wca.intpy.GrossInterest,
wca.intpy.NetInterest,
wca.intpy.DomesticTaxRate,
wca.intpy.NonResidentTaxRate,
wca.intpy.RescindInterest,
wca.intpy.AgencyFees,
wca.intpy.CouponNo,
-- null as CouponID,
wca.intpy.DefaultOpt,
wca.intpy.OptElectionDate,
wca.intpy.AnlCoupRate,
wca.int_my.InDefPay,
wca.int_my.NilInt,
wca.int_my.InterestGraceDays,
wca.int_my.InterestGraceDayConvention
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=107 and actflag<>'D')
and (wca.intpy.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog)
or wca.int_my.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog)
or wca.rd.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog)
or wca.exdt.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog)))
