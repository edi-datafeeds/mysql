--filepath=O:\Upload\Acc\107\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPUN
--fileheadertext=EDI_SPUN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\107\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPUN') as TableName,
spun.Actflag,
spun.AnnounceDate as Created,
spun.Acttime as Changed,
spun.SecID,
spun.SpunID,
spun.UnderlyingSecID,
resscmst.ISIN as UnderlyingISIN,
resscmst.SectyCD as UnderlyingSectyCD,
spun.ExchgCD,
spun.Weight,
spun.NumberOfUnderlyings,
spun.Ratio,
spun.ReferencePage1,
spun.ReferencePage2,
spun.ReferencePage3,
spun.InitialFixingCurrency,
spun.InitialFixingPrice,
spun.InitialFixingPercent,
spun.StrikeFixingCurrency,
spun.StrikeFixingPrice,
spun.StrikeFixingPercent,
spun.FixingPlace,
spun.FixingTime
from wca.spun
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.spun.UnderlyingSecID = wca.resscmst.secid
where 
(wca.scmst.isin in (select code from client.pfisin where accid=107 and actflag<>'D')
and spun.acttime >= (select date_sub(max(feeddate), INTERVAL 3 MONTH) from wca.tbl_Opslog))