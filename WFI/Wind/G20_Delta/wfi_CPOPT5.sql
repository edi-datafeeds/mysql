--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_CPOPT5
--fileheadertext=EDI_CPOPT_5_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CPOPT') as TableName,
wca.cpopt.Actflag,
wca.cpopt.AnnounceDate as Created,
wca.cpopt.Acttime as Changed,
wca.cpopt.CpoptID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.cptype,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid

where
wca.scmst.secid in(select code from client.pfsecid where accid = '990')
and wca.scexh.ExchgCD LIKE 'ES%'
and cpoptid>=6534853 and cpoptid < 8534853
