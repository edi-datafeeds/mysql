--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SDCHG') as TableName,
wca.sdchg.Actflag,
wca.sdchg.AnnounceDate as Created, 
wca.sdchg.Acttime as Changed,
wca.sdchg.SdChgID,
wca.sdchg.SecID,
wca.scmst.ISIN,
wca.sdchg.CntryCD as OldCntryCD,
wca.sdchg.EffectiveDate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.EventType,
wca.sdchg.RcntryCD as OldRcntryCD,
wca.sdchg.RelEventID,
wca.sdchg.NewCntryCD,
wca.sdchg.NewRcntryCD,
wca.sdchg.sdchgNotes As Notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.sdchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND wca.issur.isstype = 'GOV'
AND (wca.issur.CntryofIncorp = 'GB'
OR wca.issur.CntryofIncorp = 'US'
OR wca.issur.CntryofIncorp = 'AR'
OR wca.issur.CntryofIncorp = 'AU'
OR wca.issur.CntryofIncorp = 'BR'
OR wca.issur.CntryofIncorp = 'CA'
OR wca.issur.CntryofIncorp = 'CN'
OR wca.issur.CntryofIncorp = 'FR'
OR wca.issur.CntryofIncorp = 'DE'
OR wca.issur.CntryofIncorp = 'IN'
OR wca.issur.CntryofIncorp = 'ID'
OR wca.issur.CntryofIncorp = 'IT'
OR wca.issur.CntryofIncorp = 'JP'
OR wca.issur.CntryofIncorp = 'MX'
OR wca.issur.CntryofIncorp = 'RU'
OR wca.issur.CntryofIncorp = 'SA'
OR wca.issur.CntryofIncorp = 'ZA'
OR wca.issur.CntryofIncorp = 'KR'
OR wca.issur.CntryofIncorp = 'TR')