--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCEXH') as TableName,
wca.scexh.Actflag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scmst.ISIN,
wca.scexh.ExchgCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.DelistDate,
wca.scexh.LocalCode,
SUBSTRING(wca.scexh.JunkLocalcode,1,50) as JunkLocalcode,
wca.scexh.PriceTick,
wca.scexh.TickSize,
wca.scexh.ScexhNotes As Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND wca.issur.isstype = 'GOV'
AND (wca.issur.CntryofIncorp = 'GB'
OR wca.issur.CntryofIncorp = 'US'
OR wca.issur.CntryofIncorp = 'AR'
OR wca.issur.CntryofIncorp = 'AU'
OR wca.issur.CntryofIncorp = 'BR'
OR wca.issur.CntryofIncorp = 'CA'
OR wca.issur.CntryofIncorp = 'CN'
OR wca.issur.CntryofIncorp = 'FR'
OR wca.issur.CntryofIncorp = 'DE'
OR wca.issur.CntryofIncorp = 'IN'
OR wca.issur.CntryofIncorp = 'ID'
OR wca.issur.CntryofIncorp = 'IT'
OR wca.issur.CntryofIncorp = 'JP'
OR wca.issur.CntryofIncorp = 'MX'
OR wca.issur.CntryofIncorp = 'RU'
OR wca.issur.CntryofIncorp = 'SA'
OR wca.issur.CntryofIncorp = 'ZA'
OR wca.issur.CntryofIncorp = 'KR'
OR wca.issur.CntryofIncorp = 'TR')