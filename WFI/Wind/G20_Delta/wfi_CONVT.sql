--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CONVT') as TableName,
wca.convt.Actflag,
wca.convt.AnnounceDate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.convt.FromDate,
wca.convt.ToDate,
wca.convt.RatioNew,
wca.convt.RatioOld,
wca.convt.CurenCD,
wca.convt.CurPair,
wca.convt.Price,
wca.convt.MandOptFlag,
wca.convt.ResSecID,
wca.convt.SectyCD as ResSectyCD,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,
wca.convt.FXrate,
wca.convt.PartFinalFlag,
wca.convt.PriceAsPercent,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.convt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND wca.issur.isstype = 'GOV'
AND (wca.issur.CntryofIncorp = 'GB'
OR wca.issur.CntryofIncorp = 'US'
OR wca.issur.CntryofIncorp = 'AR'
OR wca.issur.CntryofIncorp = 'AU'
OR wca.issur.CntryofIncorp = 'BR'
OR wca.issur.CntryofIncorp = 'CA'
OR wca.issur.CntryofIncorp = 'CN'
OR wca.issur.CntryofIncorp = 'FR'
OR wca.issur.CntryofIncorp = 'DE'
OR wca.issur.CntryofIncorp = 'IN'
OR wca.issur.CntryofIncorp = 'ID'
OR wca.issur.CntryofIncorp = 'IT'
OR wca.issur.CntryofIncorp = 'JP'
OR wca.issur.CntryofIncorp = 'MX'
OR wca.issur.CntryofIncorp = 'RU'
OR wca.issur.CntryofIncorp = 'SA'
OR wca.issur.CntryofIncorp = 'ZA'
OR wca.issur.CntryofIncorp = 'KR'
OR wca.issur.CntryofIncorp = 'TR')