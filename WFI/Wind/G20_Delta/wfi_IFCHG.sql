--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('IFCHG') as TableName,
wca.ifchg.Actflag,
wca.ifchg.AnnounceDate as Created,
wca.ifchg.Acttime as Changed,
wca.ifchg.IfchgID,
wca.ifchg.SecID,
wca.scmst.ISIN,
wca.ifchg.NotificationDate,
wca.ifchg.OldIntPayFrqncy,
wca.ifchg.OldIntPayDate1,
wca.ifchg.OldIntPayDate2,
wca.ifchg.OldIntPayDate3,
wca.ifchg.OldIntPayDate4,
wca.ifchg.NewIntPayFrqncy,
wca.ifchg.NewIntPayDate1,
wca.ifchg.NewIntPayDate2,
wca.ifchg.NewIntPayDate3,
wca.ifchg.NewIntPayDate4,
wca.ifchg.Eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.ifchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
AND wca.issur.isstype = 'GOV'
AND (wca.issur.CntryofIncorp = 'GB'
OR wca.issur.CntryofIncorp = 'US'
OR wca.issur.CntryofIncorp = 'AR'
OR wca.issur.CntryofIncorp = 'AU'
OR wca.issur.CntryofIncorp = 'BR'
OR wca.issur.CntryofIncorp = 'CA'
OR wca.issur.CntryofIncorp = 'CN'
OR wca.issur.CntryofIncorp = 'FR'
OR wca.issur.CntryofIncorp = 'DE'
OR wca.issur.CntryofIncorp = 'IN'
OR wca.issur.CntryofIncorp = 'ID'
OR wca.issur.CntryofIncorp = 'IT'
OR wca.issur.CntryofIncorp = 'JP'
OR wca.issur.CntryofIncorp = 'MX'
OR wca.issur.CntryofIncorp = 'RU'
OR wca.issur.CntryofIncorp = 'SA'
OR wca.issur.CntryofIncorp = 'ZA'
OR wca.issur.CntryofIncorp = 'KR'
OR wca.issur.CntryofIncorp = 'TR')