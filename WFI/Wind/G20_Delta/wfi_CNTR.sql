--filepath=O:\Datafeed\Debt\Wind\G20_Delta\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_CNTR
--fileheadertext=EDI_CNTR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\G20\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CNTR') as TableName,
wca.cntr.Actflag,
wca.cntr.AnnounceDate as Created,
wca.cntr.Acttime as Changed,
wca.cntr.CntrID,
wca.cntr.CentreName,
wca.cntr.CntryCD
from wca.cntr
