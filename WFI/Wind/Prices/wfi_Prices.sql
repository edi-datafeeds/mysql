--filepath=O:\Datafeed\Debt\Wind\Prices\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_G20_Prices
--fileheadertext=EDI_G20_Prices_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\Prices\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode as pricefilesymbol,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,	
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.Bidsize,
prices.lasttrade.Asksize,
prices.lasttrade.TradedVolume,	
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,	
prices.lasttrade.VolFlag,
prices.lasttrade.Issuername,	
prices.lasttrade.SecTyCD,
prices.lasttrade.SecurityDesc,	
prices.lasttrade.Sedol,
prices.lasttrade.Uscode,
prices.lasttrade.PrimaryExchgCD,	
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,	
prices.lasttrade.TotalTrades,	
wca.bond.MaturityDate,	
wca.bond.Perpetual,
prices.lasttrade.Comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid 
where
-- wca.scmst.isin in (select code from client.pfisin where accid = '990')
wca.issur.isstype = 'GOV'
AND (wca.issur.CntryofIncorp = 'GB'
OR wca.issur.CntryofIncorp = 'US'
OR wca.issur.CntryofIncorp = 'AR'
OR wca.issur.CntryofIncorp = 'AU'
OR wca.issur.CntryofIncorp = 'BR'
OR wca.issur.CntryofIncorp = 'CA'
OR wca.issur.CntryofIncorp = 'CN'
OR wca.issur.CntryofIncorp = 'FR'
OR wca.issur.CntryofIncorp = 'DE'
OR wca.issur.CntryofIncorp = 'IN'
OR wca.issur.CntryofIncorp = 'ID'
OR wca.issur.CntryofIncorp = 'IT'
OR wca.issur.CntryofIncorp = 'JP'
OR wca.issur.CntryofIncorp = 'MX'
OR wca.issur.CntryofIncorp = 'RU'
OR wca.issur.CntryofIncorp = 'SA'
OR wca.issur.CntryofIncorp = 'ZA'
OR wca.issur.CntryofIncorp = 'KR'
OR wca.issur.CntryofIncorp = 'TR');
