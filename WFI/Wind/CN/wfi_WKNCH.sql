--filepath=O:\Datafeed\Debt\Wind\CN\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\CN\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('WKNCH') as TableName,
wca.wknch.Actflag,
wca.wknch.AnnounceDate as Created,
wca.wknch.Acttime as Changed,
wca.wknch.SecID,
wca.wknch.EffectiveDate,
wca.wknch.WknChID,
wca.wknch.Eventtype,
wca.wknch.RelEventID,
wca.wknch.OldWKN,
wca.wknch.NewWKN
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scmst.CurenCD <> 'CNY'
AND wca.issur.CntryofIncorp = 'CN'
AND wca.wknch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
