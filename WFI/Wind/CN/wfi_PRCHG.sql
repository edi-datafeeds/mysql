--filepath=O:\Datafeed\Debt\Wind\CN\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_PRCHG
--fileheadertext=EDI_PRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=N:\debt\Wind\CN\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('PRCHG') as TableName,
wca.prchg.Actflag,
wca.prchg.AnnounceDate as Created,
wca.prchg.Acttime as Changed,
wca.prchg.PrchgID,
wca.prchg.SecID,
wca.prchg.EffectiveDate,
wca.prchg.EventType,
wca.prchg.OldExchgCD,
wca.prchg.NewExchgCD,
wca.prchg.Notes
from wca.prchg
inner join wca.bond on wca.prchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where

wca.scmst.CurenCD <> 'CNY'
AND wca.issur.CntryofIncorp = 'CN'
AND wca.prchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
