--filepath=O:\Datafeed\Debt\WorldQuant\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_WorldQuant_Delta
--fileheadertext=EDI_WorldQuant_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\WorldQuant\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
wca.issur.announcedate as Created,
wca.bond.acttime as Changed,
wca.bond.ActFlag,

wca.seqs.SecID as 'EqsSecID',
wca.seqs.ISIN as 'EqsISIN',
wca.seqs.USCode as 'EqsUSCode',



wca.sbnd.SecID AS 'BndSecID',


-- CASE WHEN wca.sbnd.SecID is null AND wca.sprf.SecID IS NULL THEN wca.spfs.SecID
--      WHEN wca.sbnd.SecID is null AND wca.spfs.SecID IS NULL THEN wca.sprf.SecID
--      ELSE wca.sbnd.SecID END AS 'BndSecID', 
 
 
 
wca.sbnd.ISIN AS 'BndISIN', 
 
-- CASE WHEN wca.sbnd.ISIN is null AND wca.sprf.ISIN IS NULL THEN wca.spfs.ISIN
--      WHEN wca.sbnd.ISIN is null AND wca.spfs.ISIN IS NULL THEN wca.sprf.ISIN
--      ELSE wca.sbnd.ISIN END AS 'BndISIN',
     
     
     
wca.sbnd.USCode AS 'BndUSCode',     
     
-- CASE WHEN wca.sbnd.USCode is null AND wca.sprf.USCode IS NULL THEN wca.spfs.USCode
--      WHEN wca.sbnd.USCode is null AND wca.spfs.USCode IS NULL THEN wca.sprf.USCode
--      ELSE wca.sbnd.USCode END AS 'BndUSCode',


wca.issur.IssID,
wca.issur.Issuername,
wca.issur.CntryofIncorp,
wca.bond.IssueDate,
wca.bond.IssuePrice,
wca.bond.MaturityDate,
wca.bond.MatPrice,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.interestBasis,


wca.sbnd.curencd AS 'CurenCD',

-- CASE WHEN wca.sbnd.curencd is null AND wca.sprf.curencd IS NULL THEN wca.spfs.curencd
--      WHEN wca.sbnd.curencd is null AND wca.spfs.curencd IS NULL THEN wca.sprf.curencd
--      ELSE wca.sbnd.curencd END AS 'CurenCD',
     
     
     

wca.bond.LastCouponDate,
wca.bond.BondType,

case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue when wca.bond.parvalue<>'' and wca.bond.parvalue<> 0 then wca.bond.parvalue else wca.seqs.parvalue end as NominalValue,


wca.bond.parvalue,

wca.bond.Callable,
wca.bond.Puttable,
wca.bond.PerformanceBenchmarkISIN,

wca.seqs.SecurityDesc AS 'EqsSecurityDesc',

wca.sbnd.SecurityDesc AS 'BndSecurityDesc',


-- CASE WHEN wca.sbnd.SecurityDesc is null AND wca.sprf.SecurityDesc IS NULL THEN wca.spfs.SecurityDesc
--      WHEN wca.sbnd.SecurityDesc is null AND wca.spfs.SecurityDesc IS NULL THEN wca.sprf.SecurityDesc
--      ELSE wca.sbnd.SecurityDesc END AS 'BndSecurityDesc',




-- CASE WHEN wca.sbnd.SectyCD is null AND wca.sprf.SectyCD IS NULL THEN wca.spfs.SectyCD
--      WHEN wca.sbnd.SectyCD is null AND wca.spfs.SectyCD IS NULL THEN wca.sprf.SectyCD
--      ELSE wca.sbnd.SectyCD END AS 'SectyCD',



wca.issur.IssType,
wca.bond.DebtMarket,
wca.bond.BondSrc,
wca.bond.IssueAmount,
wca.bond.OutstandingAmount,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,
wca.bond.FRNType,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup AS FRNMargin,
wca.bond.minimuminterestrate as FRNMinInterestRate,
wca.bond.maximuminterestrate as FRNMaxInterestRate,
wca.bond.Subordinate,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.DenominationMultiple,
wca.bond.strip,
case when wca.seqs.StatusFlag = '' then 'A' else wca.seqs.StatusFlag end as EqsStatusFlag


-- case when wca.sbnd.StatusFlag = '' then 'A'
-- 	 when wca.sbnd.StatusFlag = '' then 'A'
-- else wca.sbnd.StatusFlag end as EqsStatusFlag,
-- 
-- CASE WHEN wca.sbnd.StatusFlag is null AND wca.sprf.StatusFlag IS NULL AND wca.spfs.StatusFlag = '' THEN 'A' ELSE wca.spfs.StatusFlag
--      WHEN wca.sbnd.StatusFlag is null AND wca.spfs.StatusFlag IS NULL AND wca.sprf.StatusFlag = '' THEN 'A' ELSE wca.sprf.StatusFlag 
--      ELSE wca.sbnd.StatusFlag = '' THEN 'A' ELSE wca.sprf.StatusFlag END AS 'BndSecurityDesc',
-- 
from wca.issur
-- inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 226=client.pfisin.accid and 'D'<>client.pfisin.actflag

left outer join wca.scmst as seqs on wca.issur.issid = seqs.issid and 'eqs' = seqs.SecTyCD
left outer join wca.scmst as sbnd on wca.issur.issid = sbnd.issid and 'bnd' = sbnd.SecTyCD

-- left outer join wca.scmst as sprf on wca.issur.issid = sprf.issid and 'prf' = sprf.SecTyCD and 'D' <> sprf.Actflag AND 'I' <> sprf.statusflag
-- left outer join wca.scmst as spfs on wca.issur.issid = spfs.issid and 'pfs' = spfs.SecTyCD and 'D' <> spfs.Actflag AND 'I' <> spfs.statusflag


-- left outer join wca.bond on sbnd.secid or sprf.secid or spfs.secid = wca.bond.secid


left outer join wca.bond on sbnd.secid = bond.secid


left outer join wca.bondx on wca.bond.secid = wca.bondx.secid

where
seqs.secid is not null
and sbnd.secid is not null

and (wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or seqs.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))

