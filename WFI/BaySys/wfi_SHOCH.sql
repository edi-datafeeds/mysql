--filepath=O:\Datafeed\Debt\Baysys\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SHOCH
--fileheadertext=EDI_SHOCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BaySys\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SHOCH') as Tablename,
wca.shoch.ActFlag,
wca.shoch.AnnounceDate as Created,
wca.shoch.Acttime as Changed,
wca.shoch.SecID,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
wca.shoch.NewSos,
wca.shoch.ShochID,
wca.shoch.ShochNotes,
wca.shoch.EventType,
wca.shoch.RelEventID,
wca.shoch.OldOutstandingDate,
wca.shoch.NewOutstandingDate
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'United States'
and (wca.shoch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
