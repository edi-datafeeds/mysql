--filepath=O:\Datafeed\Debt\Baysys\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_MKTSG
--fileheadertext=EDI_MKTSG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BaySys\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MKTSG') as TableName,
wca.mktsg.Actflag,
wca.mktsg.AnnounceDate as Created,
wca.mktsg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.mktsg.ExchgCD,
wca.mktsg.MktSegment,
wca.mktsg.MIC,
wca.mktsg.MktsgID
from wca.mktsg
inner join wca.scexh on wca.mktsg.MktsgID = wca.scexh.MktsgID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where
wca.continent.country = 'United States'
and (wca.mktsg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or (wca.scexh.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3) and wca.scexh.actflag = 'I'))
