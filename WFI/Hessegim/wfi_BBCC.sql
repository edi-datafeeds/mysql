--filepath=O:\Upload\Acc\291\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Hessegim\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate as Created,
wca.bbcc.Acttime as Changed,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.Oldcntrycd,
wca.bbcc.Oldcurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.Newcntrycd,
wca.bbcc.Newcurencd,
wca.bbcc.OldbbgcompId,
wca.bbcc.NewbbgcompId,
wca.bbcc.Oldbbgcomptk,
wca.bbcc.Newbbgcomptk,
wca.bbcc.ReleventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=291 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=291 and actflag='U')
and bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))