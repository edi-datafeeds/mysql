--filepath=O:\Upload\Acc\291\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Hessegim\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('IFCHG') as TableName,
wca.ifchg.Actflag,
wca.ifchg.AnnounceDate as Created,
wca.ifchg.Acttime as Changed,
wca.ifchg.IfchgID,
wca.ifchg.SecID,
wca.scmst.ISIN,
wca.ifchg.NotificationDate,
wca.ifchg.OldIntPayFrqncy,
wca.ifchg.OldIntPayDate1,
wca.ifchg.OldIntPayDate2,
wca.ifchg.OldIntPayDate3,
wca.ifchg.OldIntPayDate4,
wca.ifchg.NewIntPayFrqncy,
wca.ifchg.NewIntPayDate1,
wca.ifchg.NewIntPayDate2,
wca.ifchg.NewIntPayDate3,
wca.ifchg.NewIntPayDate4,
wca.ifchg.Eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=291 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=291 and actflag='U')
and ifchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))