--filepath=O:\Upload\Acc\291\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Hessegim\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
upper('BBC') as tablename,
wca.bbc.ActFlag,
wca.bbc.AnnounceDate as Created,
wca.bbc.Acttime as Changed,
wca.bbc.BbcId,
wca.bbc.SecId,
wca.bbc.Cntrycd,
wca.bbc.Curencd,
wca.bbc.BbgcompId,
wca.bbc.Bbgcomptk
from wca.bbc
inner join wca.scmst on wca.bbc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=291 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=291 and actflag='U')
and bbc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))