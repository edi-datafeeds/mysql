--filepath=O:\Upload\Acc\291\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_MIFID
--fileheadertext=EDI_MIFID_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Hessegim\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MIFID') as TableName,
wca.mifid.Actflag,
wca.mifid.AnnounceDate as Created,
wca.mifid.Acttime as Changed,
wca.scmst.ISIN,
wca.mifid.SecID,
wca.mifid.StartDate,
wca.mifid.CompAuth,
wca.mifid.CntryCD,
wca.mifid.EndDate,
wca.mifid.MiFIDID
from wca.mifid
inner join wca.bond on wca.mifid.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=291 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=291 and actflag='U')
and mifid.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))