--filepath=O:\Prodman\Dev\WFI\Feeds\Markit\Russian_New_Issuance\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_InterestpaymentFrequency
--fileheadertext=InterestpaymentFrequency_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CandealReports\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct

wca.scmst.SECID,
wca.scmst.USCode,
wca.scmst.ISIN,
wca.bond.BondType,
case when (wca.issur.ISSID = '83230' or wca.issur.ISSID = '82548' or wca.issur.ISSID = '82514'
or wca.issur.ISSID = '84017' or wca.issur.ISSID = '84250' or wca.issur.ISSID = '84018'
or wca.issur.ISSID = '82482' or wca.issur.ISSID = '84252' or wca.issur.ISSID = '112427'
or wca.issur.ISSID = '84256') then 'STATE GOV' else wca.issur.IssType end as IssType,
wca.scmst.SecurityDesc,
wca.bond.SecurityCharge,
wca.bond.MaturityDate,
wca.bond.IssueDate,
wca.bond.InterestAccrualConvention,
wca.bond.FRNIndexBenchmark,
wca.bond.FirstCouponDate,
wca.bond.InterestPaymentFrequency,
wca.scexh.LocalCode,
wca.scexh.ExchgCD,
wca.bond.InterestRate,
wca.issur.IssuerName,
wca.bond.OutstandingAmount,
wca.bond.PrivatePlacement,
wca.auct.AuctionDate,
wca.bond.FirstAnnouncementDate,
wca.bond.IntCommencementDate AS DatedDate,
wca.bond.IntCommencementDate,

puscode.uscode AS BenchUSCODE,


case when wca.scmst.statusflag = '' then 'A' else wca.scmst.statusflag end as Statusflag,
wca.bond.Actflag,


puscode.isstype AS BenchMarkType,


wca.bond.CurenCD


FROM wca.scmst
INNER JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur ON wca.scmst.issid = wca.issur.issid
left outer join wca.trnch on wca.scmst.secid = wca.trnch.secid

LEFT OUTER JOIN portfolio.pbm as puscode on wca.bond.PerformanceBenchmarkISIN = puscode.ISIN


INNER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.auct ON wca.bond.secid = wca.auct.secid


WHERE wca.scexh.ExchgCD LIKE 'CA%'
AND wca.bond.CurenCD = 'CAD'
AND wca.scmst.actflag <> 'D'
AND wca.scexh.actflag <> 'D'


AND InterestPaymentFrequency = ''


AND wca.bond.strip = ''
AND wca.trnch.TrnchID IS NULL
AND wca.bond.InterestBasis <> 'ZC' 



and (wca.scmst.statusflag <> 'I' AND wca.bond.MaturityDate > CURDATE()
OR wca.bond.MaturityDate < CURDATE() AND wca.scmst.statusflag = 'D');