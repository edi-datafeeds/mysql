--filepath=O:\Datafeed\Bespoke\SmartStream_Candeal\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SmartStream_Candeal
--fileheadertext=EDI_SmartStream_Candeal_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\SmartStream_Candeal\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.USCode,
wca.scmst.ISIN,
wca.bond.BondType,
case when (wca.issur.ISSID = '83230' or wca.issur.ISSID = '82548' or wca.issur.ISSID = '82514'
or wca.issur.ISSID = '84017' or wca.issur.ISSID = '84250' or wca.issur.ISSID = '84018'
or wca.issur.ISSID = '82482' or wca.issur.ISSID = '84252' or wca.issur.ISSID = '112427'
or wca.issur.ISSID = '84256') then 'STATE GOV' else wca.issur.IssType end as IssType,
wca.scmst.SecurityDesc,
wca.bond.SecurityCharge,
wca.bond.MaturityDate,
wca.bond.IssueDate,
wca.bond.InterestAccrualConvention,
wca.bond.FRNIndexBenchmark,
wca.bond.FirstCouponDate,
wca.bond.InterestPaymentFrequency,
wca.scexh.LocalCode,
wca.scexh.ExchgCD,
wca.bond.InterestRate,
wca.issur.IssuerName,
wca.bond.OutstandingAmount,
wca.bond.PrivatePlacement,
CASE WHEN wca.auct.AuctionDate IS NULL THEN '0000-00-00 00:00:00' ELSE wca.auct.AuctionDate END AS AuctionDate,
wca.bond.FirstAnnouncementDate,
wca.bond.IntCommencementDate AS DatedDate,

pbiscmst.uscode as BenchMarkUSCODE,


case when wca.scmst.statusflag = '' then 'A' else wca.scmst.statusflag end as Statusflag,
wca.bond.Actflag,


pbiissur.isstype as BenchMarkType,


wca.bond.CurenCD

FROM wca.scmst
INNER JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
left outer JOIN wca.scmst as pbiscmst ON  wca.bond.PerformanceBenchmarkISIN =  pbiscmst.isin and wca.bond.PerformanceBenchmarkISIN <> ''
left outer join wca.issur on wca.scmst.issid = wca.issur.issid

left outer join wca.issur as pbiissur on pbiscmst.issid = pbiissur.issid


INNER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.auct ON wca.bond.secid = wca.auct.secid


WHERE wca.scexh.ExchgCD LIKE 'CA%'
AND wca.bond.CurenCD = 'CAD'

AND wca.bond.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- and wca.bond.acttime between '2016-08-09 08:35:50' and CURDATE()
