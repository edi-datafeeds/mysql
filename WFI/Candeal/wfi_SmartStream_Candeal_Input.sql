--filepath=O:\Datafeed\Bespoke\SmartStream_Candeal\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=PerformanceBenchmark
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\SmartStream_Candeal\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.secid,
wca.scmst.ISIN,
wca.bond.PerformanceBenchmarkISIN,
'' as BenchMarkUSCODE,
wca.scmst.USCode,
wca.issur.isstype,
'' as BenchMarkType
-- wca.issur.isstype
from wca.scmst

#INNER JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
left outer JOIN wca.bond ON  wca.scmst.isin = wca.bond.PerformanceBenchmarkISIN 
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
WHERE
wca.bond.PerformanceBenchmarkISIN <> '';