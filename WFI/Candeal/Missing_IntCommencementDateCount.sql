--filepath=O:\Prodman\Dev\WFI\Feeds\Markit\Russian_New_Issuance\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_IntCommencementDate
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\CandealReports\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
count(wca.scmst.secid) AS Count
FROM wca.scmst
INNER JOIN wca.bond ON wca.scmst.secid = wca.bond.secid
INNER JOIN wca.issur ON wca.scmst.issid = wca.issur.issid
left outer join wca.trnch on wca.scmst.secid = wca.trnch.secid

LEFT OUTER JOIN portfolio.pbm as puscode on wca.bond.PerformanceBenchmarkISIN = puscode.ISIN


INNER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
LEFT OUTER JOIN wca.auct ON wca.bond.secid = wca.auct.secid


WHERE wca.scexh.ExchgCD LIKE 'CA%'
AND wca.bond.CurenCD = 'CAD'
AND wca.scmst.actflag <> 'D'
AND wca.scexh.actflag <> 'D'

AND wca.bond.IntCommencementDate is null


AND wca.bond.strip = ''
AND wca.trnch.TrnchID IS NULL
AND wca.bond.InterestBasis <> 'ZC' 



and (wca.scmst.statusflag <> 'I' AND wca.bond.MaturityDate > CURDATE()
OR wca.bond.MaturityDate < CURDATE() AND wca.scmst.statusflag = 'D');