--filepath=O:\Upload\Acc\126\feed\BondSigma\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BondSigma
--fileheadertext=EDI_BondSigma_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\126\feed\BondSigma\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
client.pfisin.code as Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
bondsigma.calcout.PayDate,
bondsigma.calcout.AnlCoupRate,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.calcout.Callput,
bondsigma.calcout.CpType,
bondsigma.calcout.CpoptID,
DATE_FORMAT(bondsigma.calcout.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(bondsigma.calcout.ToDate, '%Y-%m-%d') as ToDate,
bondsigma.calcout.CpCurrency,
bondsigma.calcout.CpPrice,
bondsigma.calcout.CpPriceAsPercent,
wca.bond.IssuePrice,
case when (wca.bond.Source like 'AU%' or wca.bond.Source like 'ASX%')
and wca.scmst.ISIN like 'AU%'
and (wca.bond.BondType = 'CD' OR wca.bond.BondType = 'CP')
and wca.bond.IssueDate is null then wca.bond.SourceDate 
else wca.bond.IssueDate end as IssueDate,
wca.bond.Perpetual,
case when bondsigma.calcout.ClosingPrice<>'' and bondsigma.calcout.ClosingPrice is not null
     then DATE_FORMAT(bondsigma.calcout.SettlementDate, '%Y-%m-%d')
     else ''
     end as SettlementDate,



-- case when bondsigma.refpricefull.ExchgCD<>'' and bondsigma.refpricefull.ExchgCD is not null
--     then bondsigma.refpricefull.ExchgCD
--     else wca.scmst.primaryexchgcd
--     end as ExchgCD,
     
case when bondsigma.calcout.ExchgCD<>'' and bondsigma.calcout.ExchgCD is not null
     then bondsigma.calcout.ExchgCD
     else scexh.ExchgCD
     end as ExchgCD,     
     
     
case when bondsigma.calcout.LocalCode<>'' and bondsigma.calcout.LocalCode is not null
     then bondsigma.calcout.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when bondsigma.calcout.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when bondsigma.calcout.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(bondsigma.calcout.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
bondsigma.calcout.ClosingPrice,
bondsigma.calcout.Open,
bondsigma.calcout.High,
bondsigma.calcout.Low,
bondsigma.calcout.Mid,
bondsigma.calcout.Ask,
bondsigma.calcout.Bid,
bondsigma.calcout.BidSize,
bondsigma.calcout.AskSize,
bondsigma.calcout.TradedVolume,
bondsigma.calcout.VolFlag,
bondsigma.calcout.PriceCurrency,
bondsigma.calcout.AccruedInterest,
bondsigma.calcout.YieldtoMaturity,
bondsigma.calcout.YieldtoCall,
bondsigma.calcout.YieldtoPut,
bondsigma.calcout.ModifiedDuration,
bondsigma.calcout.EffectiveDuration,
bondsigma.calcout.MacaulayDuration,
bondsigma.calcout.Convexity,
bondsigma.calcout.Yield,
bondsigma.calcout.YieldToWorst,
bondsigma.calcout.KeyRateDuration,
wca.bond.BondType,
wca.bond.Subordinate,
wca.bond.SeniorJunior,
wca.bond.Tier,
wca.bond.UppLow,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 126=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join bondsigma.calcout on wca.scmst.secid=bondsigma.calcout.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid

#left outer join bondsigma.calcout on bondsigma.refpricefull.secid=bondsigma.calcout.secid
 #                                   and bondsigma.refpricefull.exchgcd=bondsigma.calcout.exchgcd
   #                                 and bondsigma.refpricefull.pricecurrency=bondsigma.calcout.pricecurrency
left outer join wca.scexh on bondsigma.calcout.exchgcd='' and wca.scmst.secid = wca.scexh.secid -- and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D';