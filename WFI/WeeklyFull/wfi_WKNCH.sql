--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('WKNCH') as TableName,
wca.wknch.actflag,
wca.wknch.announcedate as created,
wca.wknch.acttime as changed,
wca.wknch.secid,
wca.wknch.effectivedate,
wca.wknch.wknchid,
wca.wknch.eventtype,
wca.wknch.releventid,
wca.wknch.oldwkn,
wca.wknch.newwkn
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.wknch.actflag<>'d'
and wca.scmst.statusflag<>'i'
