--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT
upper('BBEC') as tablename,
wca.BBEC.actflag,
wca.BBEC.announcedate,
wca.BBEC.acttime,
wca.BBEC.bbecid,
wca.BBEC.secid,
wca.BBEC.bbeid,
wca.BBEC.oldexchgcd,
wca.BBEC.oldcurencd,
wca.BBEC.effectivedate,
wca.BBEC.newexchgcd,
wca.BBEC.newcurencd,
wca.BBEC.oldbbgexhid,
wca.BBEC.newbbgexhid,
wca.BBEC.oldbbgexhtk,
wca.BBEC.newbbgexhtk,
wca.BBEC.releventid,
wca.BBEC.eventtype,
wca.BBEC.notes
FROM wca.bbec as BBEC
inner join wca.scmst on wca.BBEC.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
WHERE
wca.BBEC.actflag<>'D'
and wca.scmst.statusflag<>'I'

