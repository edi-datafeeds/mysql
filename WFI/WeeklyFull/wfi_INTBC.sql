--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('INTBC') as TableName,
wca.intbc.actflag,
wca.intbc.announcedate as created,
wca.intbc.acttime as changed,
wca.intbc.intbcid,
wca.intbc.secid,
wca.scmst.isin,
wca.intbc.effectivedate,
wca.intbc.oldintbasis,
wca.intbc.newintbasis,
wca.intbc.oldintbdc,
wca.intbc.newintbdc
from wca.intbc
inner join wca.bond on wca.intbc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.intbc.actflag<>'d'
and wca.scmst.statusflag<>'i'