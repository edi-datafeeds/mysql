--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('ISSUR') as TableName,
wca.issur.actflag,
wca.issur.announcedate as created,
wca.issur.acttime as changed,
wca.issur.issid,
wca.issur.issuername,
wca.issur.indusid,
wca.issur.cntryofincorp,
wca.issur.financialyearend,
wca.issur.shortname,
wca.issur.legalname,
wca.issur.cntryofdom,
wca.issur.stateofdom,
cast(wca.issur.issurnotes as char) As Notes,
case when wca.issur.cntryofincorp='aa' then 's' when wca.issur.isstype='gov' then 'g' when wca.issur.isstype='govagency' then 'y' else 'c' end as debttype
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issur.actflag<>'d'
and wca.scmst.statusflag<>'i'



