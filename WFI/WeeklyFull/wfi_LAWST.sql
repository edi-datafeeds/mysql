--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LAWST
--fileheadertext=EDI_LAWST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 
select distinct
upper('LAWST') as TableName,
wca.lawst.actflag,
wca.lawst.announcedate as created,
wca.lawst.acttime as changed,
wca.lawst.lawstid,
wca.bond.secid,
wca.scmst.isin,
wca.lawst.issid,
wca.lawst.effectivedate,
wca.lawst.latype,
wca.lawst.regdate
from wca.lawst
inner join wca.scmst on wca.lawst.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.lawst.actflag<>'d'
and wca.scmst.statusflag<>'i'