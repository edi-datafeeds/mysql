--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BDC
--fileheadertext=EDI_BDC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
upper('BDC') as tablename,
wca.bdc.actflag,
wca.bdc.announcedate as created,
wca.bdc.acttime as changed,
wca.bdc.bdcid,
wca.bdc.secid,
wca.scmst.isin,
wca.bdc.bdcappliedto,
wca.bdc.cntrid,
wca.bdc.notes
from wca.bdc
inner join wca.bond on wca.bdc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bdc.actflag<>'d'
and wca.scmst.statusflag<>'i'