--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('AGM') as tablename,
wca.agm.actflag,
wca.agm.announcedate as created,
wca.agm.acttime as changed, 
wca.agm.agmid,
wca.bond.secid,
wca.scmst.isin,
wca.agm.issid,
wca.agm.agmdate,
wca.agm.agmegm,
wca.agm.agmno,
wca.agm.fyedate,
wca.agm.agmtime,
wca.agm.add1,
wca.agm.add2,
wca.agm.add3,
wca.agm.add4,
wca.agm.add5,
wca.agm.add6,
wca.agm.city,
wca.agm.cntrycd,
wca.agm.bondsecid
from wca.agm
inner join wca.scmst on wca.agm.bondsecid = wca.scmst.secid
inner join wca.bond on wca.agm.bondsecid = wca.bond.secid
where
wca.agm.actflag <> 'd'
and wca.scmst.statusflag <> 'i'