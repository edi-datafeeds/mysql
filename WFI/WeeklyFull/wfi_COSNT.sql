--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
'COSNT' as TableName,
wca.cosnt.actflag,
wca.cosnt.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.cosnt.acttime) then wca.rd.acttime else wca.cosnt.acttime end as changed,
wca.cosnt.rdid,
wca.scmst.secid,
wca.rd.recdate,
wca.scmst.isin,
wca.cosnt.expirydate,
wca.cosnt.expirytime,
SUBSTRING(wca.cosnt.TimeZone, 1,3) AS TimeZone,
wca.cosnt.collateralrelease,
wca.cosnt.currency,
wca.cosnt.fee,
wca.cosnt.notes
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.cosnt.actflag<>'d'
and wca.scmst.statusflag<>'i'