--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('MPAY') as TableName,
wca.mpay.actflag,
wca.mpay.announcedate,
wca.mpay.acttime,
wca.mpay.sevent as eventtype,
wca.mpay.eventid,
wca.mpay.optionid,
wca.mpay.serialid,
wca.mpay.sectycd as ressectycd,
wca.mpay.ressecid,
wca.resscmst.isin as resisin,
wca.resissur.issuername as resissuername,
wca.resscmst.securitydesc as ressecuritydesc,
wca.mpay.rationew,
wca.mpay.ratioold,
wca.mpay.fractions,
wca.mpay.minofrqty,
wca.mpay.maxofrqty,
wca.mpay.minqlyqty,
wca.mpay.maxqlyqty,
wca.mpay.paydate,
wca.mpay.curencd,
wca.mpay.minprice,
wca.mpay.maxprice,
wca.mpay.tndrstrkprice,
wca.mpay.tndrstrkstep,
wca.mpay.paytype,
wca.mpay.dutchauction,
wca.mpay.defaultopt,
wca.mpay.optelectiondate
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = resissur.issid
where
wca.mpay.actflag<>'d'
and wca.scmst.statusflag<>'i'