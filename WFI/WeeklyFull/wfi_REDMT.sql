--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('REDMT') as TableName,
wca.redmt.actflag,
wca.redmt.announcedate as created,
wca.redmt.acttime as changed,
wca.redmt.redmtid,
wca.scmst.secid,
wca.scmst.isin,
wca.redmt.redemptiondate as redemdate,
wca.redmt.curencd as redemcurrency,
wca.redmt.redemptionprice as redemprice,
wca.redmt.mandoptflag,
wca.redmt.partfinal,
wca.redmt.redemptiontype as redemtype,
wca.redmt.redemptionamount as redemamount,
wca.redmt.redemptionpremium as redempremium,
wca.redmt.redeminpercent,
wca.redmt.priceaspercent,
wca.redmt.premiumaspercent,
wca.redmt.redmtnotes as notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.redmt.actflag<>'d'
and wca.scmst.statusflag<>'i'