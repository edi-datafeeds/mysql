--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('ISCHG') as TableName,
wca.ischg.actflag,
wca.ischg.announcedate as created,
wca.ischg.acttime as changed,
wca.ischg.ischgid,
wca.bond.secid,
wca.scmst.isin,
wca.ischg.issid,
wca.ischg.namechangedate,
wca.ischg.issoldname,
wca.ischg.issnewname,
wca.ischg.eventtype,
wca.ischg.legalname,
wca.ischg.meetingdateflag, 
wca.ischg.ischgnotes as notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.ischg.actflag<>'d'
and (wca.bond.issuedate<=wca.ischg.namechangedate
     or wca.bond.issuedate is null or wca.ischg.namechangedate is null
    )
and wca.scmst.statusflag<>'i'