--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CRDRT') as TableName,
wca.crdrt.actflag,
wca.crdrt.announcedate as created,
wca.crdrt.acttime as changed, 
wca.crdrt.secid,
wca.crdrt.ratingagency,
wca.crdrt.ratingdate,
wca.crdrt.rating,
wca.crdrt.direction,
wca.crdrt.watchlist,
wca.crdrt.watchlistreason
from wca.crdrt
inner join wca.bond on wca.crdrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.crdrt.actflag<>'d'
and wca.scmst.statusflag<>'i'
