--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('ICC') as TableName,
wca.icc.actflag,
wca.icc.announcedate as created,
wca.icc.acttime as changed,
wca.icc.iccid,
wca.icc.secid,
wca.scmst.isin,
wca.icc.effectivedate,
wca.icc.oldisin,
wca.icc.newisin,
wca.icc.olduscode,
wca.icc.newuscode,
wca.icc.oldvaloren,
wca.icc.newvaloren,
wca.icc.eventtype,
wca.icc.releventid,
wca.icc.oldcommoncode,
wca.icc.newcommoncode
from wca.icc
inner join wca.bond on wca.icc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.icc.actflag<>'d'
and wca.scmst.statusflag<>'i'