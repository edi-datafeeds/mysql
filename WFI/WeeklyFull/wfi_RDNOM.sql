--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('RDNOM') as TableName,
wca.rdnom.actflag,
wca.rdnom.announcedate as created,
wca.rdnom.acttime as changed, 
wca.rdnom.rdnomid,
wca.rdnom.secid,
wca.scmst.isin,
wca.rdnom.effectivedate,
wca.rdnom.olddenomination1,
wca.rdnom.olddenomination2,
wca.rdnom.olddenomination3,
wca.rdnom.olddenomination4,
wca.rdnom.olddenomination5,
wca.rdnom.olddenomination6,
wca.rdnom.olddenomination7,
wca.rdnom.oldminimumdenomination,
wca.rdnom.olddenominationmultiple,
wca.rdnom.newdenomination1,
wca.rdnom.newdenomination2,
wca.rdnom.newdenomination3,
wca.rdnom.newdenomination4,
wca.rdnom.newdenomination5,
wca.rdnom.newdenomination6,
wca.rdnom.newdenomination7,
wca.rdnom.newminimumdenomination,
wca.rdnom.newdenominationmultiple,
wca.rdnom.notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.rdnom.actflag<>'d'
and wca.scmst.statusflag<>'i'