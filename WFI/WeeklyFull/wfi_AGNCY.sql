--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('AGNCY') as tablename,
wca.agncy.actflag,
wca.agncy.announcedate as created,
wca.agncy.acttime as changed,
wca.agncy.agncyid,
wca.agncy.registrarname,
wca.agncy.add1,
wca.agncy.add2,
wca.agncy.add3,
wca.agncy.add4,
wca.agncy.add5,
wca.agncy.add6,
wca.agncy.city,
wca.agncy.cntrycd,
wca.agncy.website,
wca.agncy.contact1,
wca.agncy.tel1,
wca.agncy.fax1,
wca.agncy.email1,
wca.agncy.contact2,
wca.agncy.tel2,
wca.agncy.fax2,
wca.agncy.email2,
wca.agncy.depository,
wca.agncy.state
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.agncy.actflag<>'d'