--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('MTCHG') as TableName,
wca.mtchg.actflag,
wca.mtchg.announcedate as created,
wca.mtchg.acttime as changed,
wca.mtchg.mtchgid,
wca.mtchg.secid,
wca.scmst.isin,
wca.mtchg.notificationdate,
wca.mtchg.oldmaturitydate,
wca.mtchg.newmaturitydate,
wca.mtchg.reason,
wca.mtchg.eventtype,
wca.mtchg.oldmaturitybenchmark,
wca.mtchg.newmaturitybenchmark,
wca.mtchg.notes as notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.mtchg.actflag<>'d'
and wca.scmst.statusflag<>'i'