--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SCAGY') as TableName,
wca.scagy.actflag,
wca.scagy.announcedate as created,
wca.scagy.acttime as changed,
wca.scagy.scagyid,
wca.scagy.secid,
wca.scmst.isin,
wca.scagy.relationship,
wca.scagy.agncyid,
wca.scagy.guaranteetype,
wca.scagy.spstartdate,
wca.scagy.spenddate,
wca.scagy.notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scagy.actflag<>'d'
and wca.scmst.statusflag<>'i'