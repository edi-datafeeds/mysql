--filepath=O:\datafeed\WFI\WeeklyFull\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('LTCHG') as TableName,
wca.ltchg.actflag,
wca.ltchg.announcedate as created,
wca.ltchg.acttime as changed,
wca.ltchg.ltchgid,
wca.scmst.secid,
wca.scmst.isin,
wca.ltchg.exchgcd,
wca.ltchg.effectivedate,
wca.ltchg.oldlot,
wca.ltchg.oldmintrdqty,
wca.ltchg.newlot,
wca.ltchg.newmintrdgqty
from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ltchg.actflag<>'d'
and wca.scmst.statusflag<>'i'