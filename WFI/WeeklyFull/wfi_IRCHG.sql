--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('IRCHG') as TableName,
wca.irchg.actflag,
wca.irchg.announcedate as created,
wca.irchg.acttime as changed,
wca.irchg.irchgid,
wca.irchg.secid,
wca.scmst.isin,
wca.irchg.effectivedate,
wca.irchg.oldinterestrate,
wca.irchg.newinterestrate,
wca.irchg.eventtype,
wca.irchg.notes 
from wca.irchg
inner join wca.bond on wca.irchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.irchg.actflag<>'d'
and wca.scmst.statusflag<>'i'