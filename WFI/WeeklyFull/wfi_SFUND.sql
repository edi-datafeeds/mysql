--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('sfund') as tablename,
wca.sfund.actflag,
wca.sfund.announcedate as created,
wca.sfund.acttime as changed,
wca.sfund.sfundid,
wca.sfund.secid,
wca.scmst.isin,
wca.sfund.sfunddate,
wca.sfund.curencd as sinking_currency,
wca.sfund.amount,
wca.sfund.amountaspercent,
wca.sfund.sfundnotes as notes
from wca.sfund
inner join wca.bond on wca.sfund.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sfund.actflag<>'d'
and wca.scmst.statusflag<>'i'
