--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CONVT') as tablename,
wca.convt.actflag,
wca.convt.announcedate as created,
wca.convt.acttime as changed,
wca.convt.convtid,
wca.scmst.secid,
wca.scmst.isin,
wca.convt.fromdate,
wca.convt.todate,
wca.convt.rationew,
wca.convt.ratioold,
wca.convt.curencd,
wca.convt.price,
wca.convt.mandoptflag,
wca.convt.ressecid,
wca.convt.sectycd as ressectycd,
wca.resscmst.isin as resisin,
wca.resissur.issuername as resissuername,
wca.resscmst.securitydesc as ressecuritydesc,
wca.convt.fractions,
wca.convt.fxrate,
wca.convt.partfinalflag,
wca.convt.priceaspercent,
wca.convt.convtnotes as notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.convt.actflag<>'d'
and wca.scmst.statusflag<>'i'