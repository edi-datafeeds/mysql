--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('FRNFX') as TableName,
wca.frnfx.actflag,
wca.frnfx.announcedate as created,
wca.frnfx.acttime as changed,
wca.frnfx.frnfxid,
wca.frnfx.secid,
wca.scmst.isin,
wca.frnfx.effectivedate,
wca.frnfx.oldfrntype,
wca.frnfx.oldfrnindexbenchmark,
wca.frnfx.oldmarkup as oldfrnmargin,
wca.frnfx.oldminimuminterestrate,
wca.frnfx.oldmaximuminterestrate,
wca.frnfx.oldrounding,
wca.frnfx.newfrntype,
wca.frnfx.newfrnindexbenchmark,
wca.frnfx.newmarkup as newfrnmargin,
wca.frnfx.newminimuminterestrate,
wca.frnfx.newmaximuminterestrate,
wca.frnfx.newrounding,
wca.frnfx.eventtype,
wca.frnfx.oldfrnintadjfreq,
wca.frnfx.newfrnintadjfreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.frnfx.actflag<>'d'
and wca.scmst.statusflag<>'i'