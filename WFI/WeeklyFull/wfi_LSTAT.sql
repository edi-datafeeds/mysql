--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('LSTAT') as TableName,
wca.lstat.actflag,
wca.lstat.announcedate as created,
wca.lstat.acttime as changed,
wca.lstat.lstatid,
wca.scmst.secid,
wca.scmst.isin,
wca.lstat.exchgcd,
wca.lstat.notificationdate,
wca.lstat.effectivedate,
wca.lstat.lstatstatus,
wca.lstat.eventtype,
wca.lstat.reason
from wca.lstat
inner join wca.bond on wca.lstat.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.lstat.actflag<>'d'
and wca.scmst.statusflag<>'i'