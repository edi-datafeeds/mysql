--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('PVRD') as TableName,
wca.pvrd.actflag,
wca.pvrd.announcedate as created,
wca.pvrd.acttime as changed,
wca.pvrd.pvrdid,
wca.pvrd.secid,
wca.scmst.isin,
wca.pvrd.effectivedate,
wca.pvrd.curencd,
wca.pvrd.oldparvalue,
wca.pvrd.newparvalue,
wca.pvrd.eventtype,
wca.pvrd.pvrdnotes as notes
from wca.pvrd
inner join wca.bond on wca.pvrd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.pvrd.actflag<>'d'
and wca.scmst.statusflag<>'i'