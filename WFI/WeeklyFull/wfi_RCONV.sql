--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('RCONV') as TableName,
wca.rconv.actflag,
wca.rconv.announcedate as created,
wca.rconv.acttime as changed,
wca.rconv.rconvid,
wca.rconv.secid,
wca.scmst.isin,
wca.rconv.effectivedate,
wca.rconv.oldinterestaccrualconvention,
wca.rconv.newinterestaccrualconvention,
wca.rconv.oldconvmethod,
wca.rconv.newconvmethod,
wca.rconv.eventtype,
wca.rconv.notes
from wca.rconv
inner join wca.bond on wca.rconv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.rconv.actflag<>'d'
and wca.scmst.statusflag<>'i'