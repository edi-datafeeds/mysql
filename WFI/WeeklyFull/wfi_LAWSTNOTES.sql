--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LAWSTNOTES') as TableName,
LAWST.Actflag,
LAWST.LawstID,
LAWST.LawstNotes as Notes
FROM LAWST
where
LAWST.Issid in (select wca.dbo.scmst.issid from wca.dbo.scmst
inner join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid)
and LAWST.actflag<>'D'