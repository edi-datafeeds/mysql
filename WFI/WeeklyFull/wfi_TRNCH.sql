--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('TRNCH') as TableName,
wca.trnch.actflag,
wca.trnch.announcedate as created,
wca.trnch.acttime as changed,
wca.trnch.trnchid,
wca.trnch.secid,
wca.scmst.isin,
wca.trnch.trnchnumber,
wca.trnch.tranchedate,
wca.trnch.trancheamount,
wca.trnch.expirydate,
wca.trnch.notes as notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.trnch.actflag<>'d'
and wca.scmst.statusflag<>'i'


