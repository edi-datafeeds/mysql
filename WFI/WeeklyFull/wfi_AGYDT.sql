--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('AGYDT') as tablename,
wca.agydt.actflag,
wca.agydt.announcedate as created,
wca.agydt.acttime as changed,
wca.agydt.agydtid,
wca.agydt.agncyid,
wca.agydt.effectivedate,
wca.agydt.oldregistrarname,
wca.agydt.oldadd1,
wca.agydt.oldadd2,
wca.agydt.oldadd3,
wca.agydt.oldadd4,
wca.agydt.oldadd5,
wca.agydt.oldadd6,
wca.agydt.oldcity,
wca.agydt.oldcntrycd,
wca.agydt.oldwebsite,
wca.agydt.oldcontact1,
wca.agydt.oldtel1,
wca.agydt.oldfax1,
wca.agydt.oldemail1,
wca.agydt.oldcontact2,
wca.agydt.oldtel2,
wca.agydt.oldfax2,
wca.agydt.oldemail2,
wca.agydt.oldstate,
wca.agydt.newregistrarname,
wca.agydt.newadd1,
wca.agydt.newadd2,
wca.agydt.newadd3,
wca.agydt.newadd4,
wca.agydt.newadd5,
wca.agydt.newadd6,
wca.agydt.newcity,
wca.agydt.newcntrycd,
wca.agydt.newwebsite,
wca.agydt.newcontact1,
wca.agydt.newtel1,
wca.agydt.newfax1,
wca.agydt.newemail1,
wca.agydt.newcontact2,
wca.agydt.newtel2,
wca.agydt.newfax2,
wca.agydt.newemail2,
wca.agydt.newstate
from wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.agydt.actflag<>'d'
and wca.scmst.statusflag<>'i'
