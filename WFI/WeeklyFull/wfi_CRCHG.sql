--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=EDI_CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CRCHG') as TableName,
wca.crchg.actflag,
wca.crchg.announcedate as created,
wca.crchg.acttime as changed, 
wca.crchg.crchgid,
wca.crchg.ratingagency,
wca.crchg.ratingdate,
wca.crchg.oldrating,
wca.crchg.newrating,
wca.crchg.direction,
wca.crchg.watchlist,
wca.crchg.watchlistreason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.crchg.actflag<>'d'
and wca.scmst.statusflag<>'i'
