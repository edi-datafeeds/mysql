--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT
upper('BBCC') as tablename,
wca.bbcc.actflag,
wca.bbcc.announcedate,
wca.bbcc.acttime,
wca.bbcc.bbccid,
wca.bbcc.secid,
wca.bbcc.bbcid,
wca.bbcc.oldcntrycd,
wca.bbcc.oldcurencd,
wca.bbcc.effectivedate,
wca.bbcc.newcntrycd,
wca.bbcc.newcurencd,
wca.bbcc.oldbbgcompid,
wca.bbcc.newbbgcompid,
wca.bbcc.oldbbgcomptk,
wca.bbcc.newbbgcomptk,
wca.bbcc.releventid,
wca.bbcc.eventtype,
wca.bbcc.notes
FROM wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
WHERE
wca.bbcc.actflag<>'D'
and wca.scmst.statusflag<>'I'
