--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('IFCHG') as TableName,
wca.ifchg.actflag,
wca.ifchg.announcedate as created,
wca.ifchg.acttime as changed,
wca.ifchg.ifchgid,
wca.ifchg.secid,
wca.scmst.isin,
wca.ifchg.notificationdate,
wca.ifchg.oldintpayfrqncy,
wca.ifchg.oldintpaydate1,
wca.ifchg.oldintpaydate2,
wca.ifchg.oldintpaydate3,
wca.ifchg.oldintpaydate4,
wca.ifchg.newintpayfrqncy,
wca.ifchg.newintpaydate1,
wca.ifchg.newintpaydate2,
wca.ifchg.newintpaydate3,
wca.ifchg.newintpaydate4,
wca.ifchg.eventtype
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ifchg.actflag<>'d'
and wca.scmst.statusflag<>'i'