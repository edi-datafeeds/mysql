--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('LCC') as TableName,
wca.lcc.actflag,
wca.lcc.announcedate as created,
wca.lcc.acttime as changed,
wca.lcc.lccid,
wca.lcc.secid,
wca.scmst.isin,
wca.lcc.exchgcd,
wca.lcc.effectivedate,
wca.lcc.oldlocalcode,
wca.lcc.newlocalcode,
wca.lcc.eventtype,
wca.lcc.releventid
from wca.lcc
inner join wca.bond on wca.lcc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.lcc.actflag<>'d'
and wca.scmst.statusflag<>'i'