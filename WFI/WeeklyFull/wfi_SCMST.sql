--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SCMST') as TableName,
wca.scmst.actflag,
wca.scmst.announcedate as created,
wca.scmst.acttime as changed,
wca.scmst.secid,
wca.scmst.isin,
wca.scmst.parentsecid,
wca.scmst.issid,
wca.scmst.sectycd,
wca.scmst.securitydesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.primaryexchgcd,
wca.scmst.curencd,
wca.scmst.parvalue,
wca.scmst.uscode,
wca.scmst.x as commoncode,
wca.scmst.holding,
wca.scmst.structcd,
wca.scmst.wkn,
wca.scmst.regs144a,
wca.scmst.scmstnotes as notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where 
wca.scmst.actflag<>'d'
and wca.scmst.statusflag<>'i'