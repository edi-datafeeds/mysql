--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SDCHG') as TableName,
wca.sdchg.actflag,
wca.sdchg.announcedate as created, 
wca.sdchg.acttime as changed,
wca.sdchg.sdchgid,
wca.sdchg.secid,
wca.scmst.isin,
wca.sdchg.cntrycd as oldcntrycd,
wca.sdchg.effectivedate,
wca.sdchg.oldsedol,
wca.sdchg.newsedol,
wca.sdchg.eventtype,
wca.sdchg.rcntrycd as oldrcntrycd,
wca.sdchg.releventid,
wca.sdchg.newcntrycd,
wca.sdchg.newrcntrycd,
wca.sdchg.sdchgnotes as notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sdchg.actflag<>'d'
and wca.scmst.statusflag<>'i'