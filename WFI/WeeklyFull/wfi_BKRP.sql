--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('BKRP') as TableName,
wca.bkrp.actflag,
wca.bkrp.announcedate as created,
wca.bkrp.acttime as changed,
wca.bkrp.bkrpid,
wca.bond.secid,
wca.scmst.isin,
wca.bkrp.issid,
wca.bkrp.notificationdate,
wca.bkrp.filingdate
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bkrp.actflag<>'d'
and wca.scmst.statusflag<>'i'