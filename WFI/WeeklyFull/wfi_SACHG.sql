--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SACHG') as TableName,
wca.sachg.actflag,
wca.sachg.announcedate as created,
wca.sachg.acttime as changed,
wca.sachg.sachgid,
wca.sachg.scagyid,
wca.bond.secid,
wca.scmst.isin,
wca.sachg.effectivedate,
wca.sachg.oldrelationship,
wca.sachg.newrelationship,
wca.sachg.oldagncyid,
wca.sachg.newagncyid,
wca.sachg.oldspstartdate,
wca.sachg.newspstartdate,
wca.sachg.oldspenddate,
wca.sachg.newspenddate,
wca.sachg.oldguaranteetype,
wca.sachg.newguaranteetype
from wca.sachg
inner join wca.scagy on wca.sachg.scagyid = wca.scagy.scagyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sachg.actflag<>'d'
and wca.scmst.statusflag<>'i'