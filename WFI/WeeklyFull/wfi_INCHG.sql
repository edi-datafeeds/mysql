--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
upper('INCHG') as TableName,
wca.inchg.actflag,
wca.inchg.announcedate as created,
wca.inchg.acttime as changed,
wca.inchg.inchgid,
wca.bond.secid,
wca.scmst.isin,
wca.inchg.issid,
wca.inchg.inchgdate,
wca.inchg.oldcntrycd,
wca.inchg.newcntrycd,
wca.inchg.eventtype 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.inchg.actflag<>'d'
and (wca.bond.issuedate<=wca.inchg.inchgdate
     or wca.bond.issuedate is null or wca.inchg.inchgdate is null
    )
and wca.scmst.statusflag<>'i'