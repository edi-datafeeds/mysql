--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'REDEM' as TableName,
wca.redem.actflag,
wca.redem.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.redem.acttime) then wca.rd.acttime else wca.redem.acttime end as changed,
wca.redem.redemid,
wca.scmst.secid,
wca.scmst.isin,
wca.rd.recdate,
wca.redem.redemdate,
wca.redem.curencd as redemcurrency,
wca.redem.redemprice,
wca.redem.mandoptflag,
wca.redem.partfinal,
wca.redem.redemtype,
wca.redem.amountredeemed,
wca.redem.redempremium,
wca.redem.redempercent,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,

wca.redem.priceaspercent,
wca.redem.premiumaspercent,
wca.redem.indefpay,
wca.redem.tenderopendate,
wca.redem.tenderclosedate,
wca.redem.redemnotes as notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
wca.redem.actflag<>'d'
and redemtype<>'tender'
and wca.scmst.statusflag<>'i'