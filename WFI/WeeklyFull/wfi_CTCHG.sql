--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CTCHG') as TableName,
wca.ctchg.actflag,
wca.ctchg.announcedate as created,
wca.ctchg.acttime as changed, 
wca.ctchg.ctchgid,
wca.ctchg.secid,
wca.scmst.isin,
wca.ctchg.effectivedate,
wca.ctchg.oldresultantratio,
wca.ctchg.newresultantratio,
wca.ctchg.oldsecurityratio,
wca.ctchg.newsecurityratio,
wca.ctchg.oldcurrency,
wca.ctchg.newcurrency,
wca.ctchg.oldconversionprice,
wca.ctchg.newconversionprice,
wca.ctchg.ressectycd,
wca.ctchg.oldressecid,
wca.ctchg.newressecid,
wca.ctchg.eventtype,
wca.ctchg.releventid,
wca.ctchg.oldfromdate,
wca.ctchg.newfromdate,
wca.ctchg.oldtodate,
wca.ctchg.newtodate,
wca.ctchg.convtid,
wca.ctchg.oldfxrate,
wca.ctchg.newfxrate,
wca.ctchg.oldpriceaspercent,
wca.ctchg.newpriceaspercent,
wca.ctchg.notes
from wca.ctchg
inner join wca.bond on wca.ctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ctchg.actflag<>'d'
and wca.scmst.statusflag<>'i'
