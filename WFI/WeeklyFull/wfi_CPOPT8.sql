--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CPOPT08
--fileheadertext=EDI_CPOPT08_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CPOPT') as TableName,
wca.cpopt.actflag,
wca.cpopt.announcedate as created,
wca.cpopt.acttime as changed,
wca.cpopt.cpoptid,
wca.scmst.secid,
wca.scmst.isin,
wca.cpopt.callput, 
wca.cpopt.fromdate,
wca.cpopt.todate,
wca.cpopt.noticefrom,
wca.cpopt.noticeto,
wca.cpopt.currency,
wca.cpopt.price,
wca.cpopt.mandatoryoptional,
wca.cpopt.minnoticedays,
wca.cpopt.maxnoticedays,
wca.cpopt.cptype,
wca.cpopt.priceaspercent,
wca.cpopt.inwholepart,
wca.cpopt.formulabasedprice,
wca.cpopn.notes as notes 
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.cpopn on wca.cpopt.secid = wca.cpopn.secid and wca.cpopt.callput = wca.cpopn.callput
where
wca.cpopt.actflag<>'d'
and cpoptid>4419424 and cpoptid < 4945603