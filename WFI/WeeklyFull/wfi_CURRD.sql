--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CURRD') as TableName,
wca.currd.actflag,
wca.currd.announcedate as created,
wca.currd.acttime as changed,
wca.currd.currdid,
wca.currd.secid,
wca.scmst.isin,
wca.currd.effectivedate,
wca.currd.oldcurencd,
wca.currd.newcurencd,
wca.currd.oldparvalue,
wca.currd.newparvalue,
wca.currd.eventtype,
wca.currd.currdnotes as notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.currd.actflag<>'d'
and wca.scmst.statusflag<>'i'