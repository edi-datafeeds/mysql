--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_INDEF
--fileheadertext=EDI_INDEF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'INDEF' as TableName,
wca.indef.actflag,
wca.indef.announcedate as created,
wca.indef.acttime as changed,
wca.indef.indefid,
wca.indef.secid,
wca.scmst.isin,
wca.indef.defaulttype,
wca.indef.defaultdate,
wca.indef.notes
from wca.indef
inner join wca.bond on wca.indef.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.indef.actflag<>'d'
and wca.scmst.statusflag<>'i'