--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('BOCHG') as tablename,
wca.bochg.actflag,
wca.bochg.announcedate as created,
wca.bochg.acttime as changed,  
wca.bochg.bochgid,
wca.bochg.releventid,
wca.bochg.secid,
wca.scmst.isin,
wca.bochg.effectivedate,
wca.bochg.oldoutvalue,
wca.bochg.newoutvalue,
wca.bochg.eventtype,
wca.bochg.oldoutdate,
wca.bochg.newoutdate,
wca.bochg.bochgnotes as notes
from wca.bochg
inner join wca.bond on wca.bochg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bochg.actflag<>'d'
and wca.scmst.statusflag<>'i'