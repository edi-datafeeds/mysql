--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select distinct
upper('INTPY') as TableName,
case when wca.int_my.actflag = 'd' or wca.int_my.actflag='c' or wca.intpy.actflag is null then wca.int_my.actflag else wca.intpy.actflag end as actflag,
wca.int_my.announcedate as created,
case when (wca.rd.acttime is not null) and (wca.rd.acttime > wca.int_my.acttime) and (wca.rd.acttime > wca.exdt.acttime) then wca.rd.acttime when (wca.exdt.acttime is not null) and (wca.exdt.acttime > wca.int_my.acttime) then wca.exdt.acttime else wca.int_my.acttime end as changed,
wca.scmst.secid,
wca.scmst.isin,
wca.intpy.bcurencd as debtcurrency,
wca.intpy.bparvalue as nominalvalue,
wca.scexh.exchgcd,
wca.int_my.rdid,
Case WHEN wca.intpy.optionid is null then '1' else wca.intpy.optionid end as optionid,
wca.rd.recdate,
wca.exdt.exdate,
wca.exdt.paydate,
wca.int_my.interestfromdate,
wca.int_my.interesttodate,
wca.int_my.days,
wca.intpy.curencd as interest_currency,
wca.intpy.intrate as interest_rate,
wca.intpy.grossinterest,
wca.intpy.netinterest,
wca.intpy.domestictaxrate,
wca.intpy.nonresidenttaxrate,
wca.intpy.rescindinterest,
wca.intpy.agencyfees,
wca.intpy.couponno,
wca.intpy.defaultopt,
wca.intpy.optelectiondate,
wca.intpy.anlcouprate,
wca.int_my.indefpay
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
(wca.rd.actflag<>'d'
or wca.scmst.actflag<>'d'
or wca.bond.actflag<>'d'
or wca.issur.actflag<>'d'
or wca.int_my.actflag<>'d'
or wca.intpy.actflag<>'d')
and wca.scmst.statusflag<>'i'