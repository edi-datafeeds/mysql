--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT distinct
upper('BBE') as tablename,
wca.bbe.actflag,
wca.bbe.announcedate,
wca.bbe.acttime,
wca.bbe.bbeid,
wca.bbe.secid,
wca.bbe.exchgcd,
wca.bbe.curencd,
wca.bbe.bbgexhid,
wca.bbe.bbgexhtk
from wca.bbe
inner join wca.scmst on wca.bbe.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bbe.actflag<>'D'
and wca.scmst.statusflag<>'I'