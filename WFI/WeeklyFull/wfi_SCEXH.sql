--filepath=O:\datafeed\WFI\WeeklyFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=O:\datafeed\WFI\WeeklyFull\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SCEXH') as TableName,
wca.scexh.actflag,
wca.scexh.announcedate as created,
wca.scexh.acttime as changed,
wca.scexh.scexhid,
wca.scexh.secid,
wca.scmst.isin,
wca.scexh.exchgcd,
wca.scexh.liststatus,
wca.scexh.lot,
wca.scexh.mintrdgqty,
wca.scexh.listdate,
'' as tradestatus,
wca.scexh.localcode,
SUBSTRING(wca.scexh.JunkLocalCode, 1, 50) as JunkLocalCode,
wca.scexh.scexhnotes as notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scexh.actflag<>'d'
and wca.scmst.statusflag<>'i'
