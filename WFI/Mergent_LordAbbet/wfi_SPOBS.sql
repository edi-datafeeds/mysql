--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPOBS
--fileheadertext=EDI_SPOBS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPOBS') as TableName,
spobs.Actflag,
spobs.AnnounceDate as Created,
spobs.Acttime as Changed,
spobs.SecID,
wca.scmst.ISIN,
spobs.SpobsID,
spobs.ObservationPayoutType,
spobs.ObservationFromDate,
spobs.ObservaionToDate,
spobs.ObservationDays,
spobs.ObservationDaysConvention,
spobs.ObservationBusDayConv,
spobs.DeterminationFromDate,
spobs.DeterminationToDate,
spobs.DeterminationDays,
spobs.DeterminationDaysConvention,
spobs.DeterminationBusDayConv,
spobs.PayoutDate,
spobs.PayoutDays,
spobs.PayouttDaysConvention,
spobs.PayoutBusDayConv,
spobs.FixedPayoutCurrency,
spobs.FixedPayoutValue,
spobs.FixedPayoutPercent,
spobs.MinimumPayoutCurrency,
spobs.MinimumPayoutValue,
spobs.MinimumPayoutPercent,
spobs.MaximumPayoutCurrency,
spobs.MaximumPayoutValue,
spobs.MaximumPayoutPercent,
spobs.Multiplier,
spobs.TriggerAction,
spobs.BonusCurrency,
spobs.BonusValue,
spobs.BonusLevelPercent,
spobs.OptionStyle
from wca.spobs
inner join wca.scmst on wca.spobs.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.spobs.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
