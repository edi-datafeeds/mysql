--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_PFDS
--fileheadertext=EDI_PFDS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('PFDS') as Tablename,
wca.pfds.Actflag,
wca.pfds.AnnounceDate as Created,
wca.pfds.Acttime as Changed,
wca.pfds.SecID,
wca.scmst.ISIN,
wca.pfds.SecuritiesOffered,
wca.pfds.GreenShoeOption,
wca.pfds.LiquidationPreference,
wca.pfds.PfdsID
from wca.pfds
inner join wca.bond on wca.pfds.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.pfds.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
