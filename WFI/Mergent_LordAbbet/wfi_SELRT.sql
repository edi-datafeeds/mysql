--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SELRT
--fileheadertext=EDI_SELRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SELRT') as TableName,
wca.selrt.Actflag,
wca.selrt.AnnounceDate as Created,
wca.selrt.Acttime as Changed,
wca.selrt.SelrtID,
wca.selrt.SecID,
wca.scmst.ISIN,
wca.selrt.CntryCD,
wca.selrt.Restriction
from wca.selrt
inner join wca.bond on wca.selrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.selrt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
