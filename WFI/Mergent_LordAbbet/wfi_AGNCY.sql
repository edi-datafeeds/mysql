--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGNCY') as TableName,
agncy.Actflag,
agncy.AnnounceDate as Created,
agncy.Acttime as Changed,
agncy.AgncyID,
agncy.RegistrarName,
agncy.Add1,
agncy.Add2,
agncy.Add3,
agncy.Add4,
agncy.Add5,
agncy.Add6,
agncy.City,
agncy.CntryCD,
agncy.Website,
agncy.Contact1,
agncy.Tel1,
agncy.Fax1,
agncy.Email1,
agncy.Contact2,
agncy.Tel2,
agncy.Fax2,
agncy.Email2,
agncy.Depository,
agncy.State
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.agncy.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
