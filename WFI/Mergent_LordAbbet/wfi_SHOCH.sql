--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SHOCH
--fileheadertext=EDI_SHOCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SHOCH') as Tablename,
wca.shoch.ActFlag,
wca.shoch.AnnounceDate as Created,
wca.shoch.Acttime as Changed,
wca.shoch.SecID,
wca.scmst.ISIN,
wca.shoch.EffectiveDate,
wca.shoch.OldSos,
wca.shoch.NewSos,
wca.shoch.ShochID,
wca.shoch.ShochNotes,
wca.shoch.EventType,
wca.shoch.RelEventID,
wca.shoch.OldOutstandingDate,
wca.shoch.NewOutstandingDate
from wca.shoch
inner join wca.scmst on wca.shoch.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
