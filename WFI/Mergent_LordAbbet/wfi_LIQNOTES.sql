--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LIQNOTES') as TableName,
wca.liq.Actflag,
wca.liq.LiqID,
wca.liq.Issid,
wca.liq.LiquidationTerms as Notes
from wca.liq
where
liq.Issid in (select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where (client.pfisin.accid=114 and client.pfisin.actflag='I')
or (client.pfisin.accid=114 and client.pfisin.actflag='U'
and liq.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)))