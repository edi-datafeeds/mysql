--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_EXCHG
--fileheadertext=EDI_EXCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('EXCHG') as TableName,
wca.exchg.Actflag,
wca.exchg.AnnounceDate as Created,
wca.exchg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.exchg.ExchgCD,
wca.exchg.ExchgName,
wca.exchg.CntryCD,
wca.exchg.WklyDayOff1,
wca.exchg.WklyDayOff2,
wca.exchg.MIC,
wca.exchg.ExchgID
from wca.exchg
inner join wca.scexh on wca.exchg.exchgcd = wca.scexh.exchgcd
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
#left outer join wca.cntry on wca.exchg.cntrycd = wca.cntry.cntrycd
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
#AND (wca.scexh.exchgcd like 'CN%' OR wca.scexh.exchgcd like 'HK%' OR wca.scexh.exchgcd like 'ID%'
#OR wca.scexh.exchgcd like 'IN%' OR wca.scexh.exchgcd like 'MY%' OR wca.scexh.exchgcd like 'PH%'
#OR wca.scexh.exchgcd like 'SG%' OR wca.scexh.exchgcd like 'TH%' OR wca.scexh.exchgcd like 'TW%')
#and (wca.exchg.MIC = 'XPHS' or wca.exchg.MIC = 'XSES' or wca.exchg.MIC = 'XKLS' or wca.exchg.MIC = 'XIDX' or wca.exchg.MIC = 'XHKG')
#and (wca.scexh.exchgcd like 'AU%' or wca.scexh.exchgcd like 'GB%' or wca.scexh.exchgcd like 'HK%' or wca.scexh.exchgcd like 'DE%' or wca.scexh.exchgcd like 'US%')
#and (wca.cntry.cntrycd = 'CH' OR wca.cntry.cntrycd = 'DE' OR wca.cntry.cntrycd = 'FR' OR wca.cntry.cntrycd = 'GB'
#OR wca.cntry.cntrycd = 'IT' OR wca.cntry.cntrycd = 'BE' OR wca.cntry.cntrycd = 'DK' OR wca.cntry.cntrycd = 'ES'
#OR wca.cntry.cntrycd = 'FI' OR wca.cntry.cntrycd = 'IS' OR wca.cntry.cntrycd = 'LU' OR wca.cntry.cntrycd = 'NL'
#OR wca.cntry.cntrycd = 'NO' OR wca.cntry.cntrycd = 'SE' OR wca.cntry.cntrycd = 'AU' OR wca.cntry.cntrycd = 'JP'
#OR wca.cntry.cntrycd = 'KR' OR wca.cntry.cntrycd = 'ZA')
and wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
