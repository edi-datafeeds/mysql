--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LEI
--fileheadertext=EDI_LEI_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LEI') as TableName,
lei.Actflag,
lei.Announcedate as 'Created',
lei.Acttime as 'Changed',
lei.LEIID,
lei.LEI,
lei.ManagingLOU,
lei.RegStatus,
lei.LegalName,
lei.PrevName,
lei.OtherName,
lei.IncorpDate,
lei.DissDate,
lei.ROAdd1,
lei.ROAdd2,
lei.ROAdd3,
lei.ROAdd4,
lei.ROAdd5,
lei.ROCity,
lei.ROState,
lei.ROCntry,
lei.ROPostCode,
lei.HOAdd1,
lei.HOAdd2,
lei.HOAdd3,
lei.HOAdd4,
lei.HOAdd5,
lei.HOCity,
lei.HOState,
lei.HOCntry,
lei.HOPostcode,
lei.Regulator,
lei.RegulatoryStatus,
lei.RegulatoryID
from wca.lei
inner join wca.issur on wca.lei.lei = wca.issur.lei
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.lei.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
