--filepath=O:\Upload\Acc\114\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_DIV
--fileheadertext=EDI_DIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\114\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('DIV') as Tablename,
wca.div_my.ActFlag,
wca.div_my.AnnounceDate as Created,
wca.div_my.Acttime as Changed,
wca.div_my.RdID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.div_my.DivPeriodCD,
wca.div_my.TbaFlag,
wca.div_my.DivNotes,
wca.div_my.NilDividend,
wca.div_my.Coupon,
wca.div_my.FYEDate,
wca.div_my.DivID,
wca.div_my.DivRescind,
wca.div_my.PeriodEndDate,
wca.div_my.Frequency,
wca.div_my.Marker,
wca.div_my.DeclarationDate,
wca.div_my.DeclCurenCD,
wca.div_my.DeclGrossAmt
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=114 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=114 and actflag='U')
and wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
