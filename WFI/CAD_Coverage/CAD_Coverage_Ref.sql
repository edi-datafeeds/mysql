--filepath=O:\Datafeed\Debt\CAD_USD_Coverage\CAD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CAD_Coverage_Ref
--fileheadertext=EDI_CAD_Coverage_Ref_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
select distinct
wca.scmst.USCode,
wca.scmst.ISIN,
wca.bond.SecID,
wca.issur.IssuerName,
wca.bond.MaturityDate,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestBasis,
wca.bond.BondType,
wca.bond.InterestAccrualConvention,
wca.issur.IssType,
wca.bond.IssueAmount,
wca.bond.OutstandingAmount,
wca.bond.SecuredBy,
wca.bond.SecurityCharge
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.bond.CurenCD = 'CAD'
and wca.issur.actflag <> 'D'
and wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.StatusFlag <> 'I'
order by wca.scmst.isin desc;