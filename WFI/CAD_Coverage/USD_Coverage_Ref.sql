--filepath=O:\Prodman\Dev\WFI\Feeds\CAD_Coverage\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CAD_Coverage_Ref
--fileheadertext=EDI_CAD_Coverage_Ref_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
select distinct
wca.bond.SecID,
wca.scmst.ISIN,
wca.scmst.USCode,
wca.issur.IssuerName,
wca.bond.MaturityDate,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestBasis,
wca.bond.BondType,
wca.bond.InterestAccrualConvention,
wca.issur.IssType
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.bond.CurenCD = 'CAD'
and wca.issur.actflag <> 'D'
and wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.StatusFlag <> 'I'
order by wca.scmst.isin desc;