--filepath=O:\Datafeed\Debt\Trumid_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
upper('CONVT') as Tablename,
wca.convt.ActFlag,
wca.convt.Announcedate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.convt.FromDate,
wca.convt.ToDate,
wca.convt.RatioNew,
wca.convt.RatioOld,
wca.convt.Curencd,
wca.convt.CurPair,
wca.convt.Price,
wca.convt.MandoptFlag,
wca.convt.ResSecId,
wca.convt.Sectycd as ResSectycd,
resscmst.Isin as ResIsin,
resissur.Issuername as ResIssuername,
resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,
wca.convt.FxRate,
wca.convt.PartFinalFlag,
wca.convt.PriceAsPercent,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = wca.resissur.issid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
and wca.convt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)

