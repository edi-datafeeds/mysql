--filepath=O:\Datafeed\Debt\Trumid_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
upper('SCEXH') as Tablename,
wca.scexh.ActFlag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScexhId,
wca.scexh.SecId,
wca.scmst.Isin,
wca.scexh.Exchgcd,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
'' as TradeStatus,
wca.scexh.LocalCode,

SUBSTRING_INDEX(
        SUBSTRING_INDEX(
         SUBSTRING_INDEX(
          SUBSTRING_INDEX(
           SUBSTRING_INDEX(
            SUBSTRING_INDEX(
             SUBSTRING_INDEX(
              SUBSTRING_INDEX(
               SUBSTRING_INDEX(
                SUBSTRING_INDEX(
                 SUBSTRING_INDEX(
                  SUBSTRING_INDEX(
				   
				    SUBSTRING_INDEX(
                     SUBSTRING_INDEX(wca.scexh.Localcode, '.', 1),
                      '/', 1),
                     '-', 1),
		    ' ', 1),
                  '1', 1),
                 '2', 1),
                '3', 1),
               '4', 1),
              '5', 1),
             '6', 1),
            '7', 1),
           '8', 1),
          '9', 1),
         '0', 1) AS LocalCodeNew,

-- SUBSTRING(wca.scexh.JunkLocalcode,1,50) as JunkLocalcode,
case when wca.scexh.JunkLocalcode not like '%ESC%' then '' else wca.scexh.JunkLocalcode end as JunkLocalcode,

wca.scexh.ScexhNotes as Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
and wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
