--filepath=O:\Datafeed\Debt\Trumid_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select distinct
'INTPY' as Tablename,
case when wca.int_my.ActFlag = 'd' or wca.int_my.ActFlag='c' or wca.intpy.ActFlag is null then wca.int_my.ActFlag else wca.intpy.ActFlag end as ActFlag,
wca.int_my.AnnounceDate as Created,
case when (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.int_my.Acttime) and (wca.rd.Acttime > wca.exdt.Acttime) then wca.rd.Acttime when (wca.exdt.Acttime is not null) and (wca.exdt.Acttime > wca.int_my.Acttime) then wca.exdt.Acttime else wca.int_my.Acttime end as Changed,
wca.scmst.SecId,
wca.scmst.Isin,
wca.intpy.BCurencd as DebtCurrency,
wca.intpy.BParValue as NominalValue,
wca.scexh.Exchgcd,
wca.int_my.RdId,
case when wca.intpy.optionid is null then '1' else wca.intpy.optionid end as OptionID,
wca.rd.RecDate,
wca.exdt.ExDate,
wca.exdt.PayDate,
wca.int_my.InterestFromDate,
wca.int_my.InterestToDate,
wca.int_my.Days,
wca.intpy.Curencd as Interest_Currency,
wca.intpy.intrate as Interest_Rate,
wca.intpy.GrossInterest,
wca.intpy.NetInterest,
wca.intpy.DomesticTaxRate,
wca.intpy.NonResidentTaxRate,
wca.intpy.RescindInterest,
wca.intpy.AgencyFees,
wca.intpy.CouponNo,
#null as CouponId,
wca.intpy.DefaultOpt,
wca.intpy.OptElectionDate,
wca.intpy.AnlCoupRate,
wca.int_my.IndefPay
from wca.int_my
inner join wca.rd on wca.int_my.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.bond.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid 
     and wca.scexh.exchgcd = wca.exdt.exchgcd and 'int' = wca.exdt.eventtype
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
and (wca.intpy.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.int_my.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.rd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
or wca.exdt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))










