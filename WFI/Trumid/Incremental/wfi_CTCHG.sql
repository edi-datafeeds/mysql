--filepath=O:\Datafeed\Debt\Trumid_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
upper('ctchg') as Tablename,
wca.ctchg.ActFlag,
wca.ctchg.AnnounceDate as Created,
wca.ctchg.Acttime as Changed, 
wca.ctchg.Ctchgid,
wca.ctchg.SecId,
wca.scmst.Isin,
wca.ctchg.EffectiveDate,
wca.ctchg.OldResultantRatio,
wca.ctchg.NewResultantRatio,
wca.ctchg.OldSecurityRatio,
wca.ctchg.NewSecurityRatio,
wca.ctchg.OldCurrency,
wca.ctchg.NewCurrency,
wca.ctchg.OldCurPair,
wca.ctchg.NewCurPair,
wca.ctchg.OldConversionPrice,
wca.ctchg.NewConversionPrice,
wca.ctchg.ResSectycd,
wca.ctchg.OldResSecid,
wca.ctchg.NewResSecid,
wca.ctchg.EventType,
wca.ctchg.ReleventId,
wca.ctchg.OldFromDate,
wca.ctchg.NewFromDate,
wca.ctchg.OldToDate,
wca.ctchg.NewToDate,
wca.ctchg.ConvtId,
wca.ctchg.OldFxRate,
wca.ctchg.NewFxRate,
wca.ctchg.OldPriceasPercent,
wca.ctchg.NewPriceasPercent,
wca.ctchg.Notes
from wca.ctchg
inner join wca.bond on wca.ctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
and wca.ctchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)

