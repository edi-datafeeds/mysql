--filepath=O:\Datafeed\Debt\Trumid_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
upper('REDMT') as Tablename,
wca.redmt.ActFlag,
wca.redmt.AnnounceDate as Created,
wca.redmt.Acttime as Changed,
wca.redmt.RedmtId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.redmt.RedemptionDate as RedemDate,
wca.redmt.Curencd as RedemCurrency,
wca.redmt.RedemptionPrice as RedemPrice,
wca.redmt.MandOptFlag,
wca.redmt.PartFinal,
wca.redmt.RedemptionType as RedemType,
wca.redmt.RedemptionAmount as RedemAmount,
wca.redmt.RedemptionPremium as RedemPremium,
wca.redmt.RedemInPercent,
wca.redmt.PriceAsPercent,
wca.redmt.PremiumAsPercent,
wca.redmt.RedmtNotes as Notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
and wca.redmt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)





