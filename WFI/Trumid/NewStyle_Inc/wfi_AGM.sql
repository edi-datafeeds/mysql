--filepath=O:\Datafeed\Debt\Trumid_NewStyle_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('AGM') as TableName,
wca.agm.Actflag,
wca.agm.AnnounceDate as Created,
wca.agm.Acttime as Changed, 
wca.agm.AGMID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.agm.IssID,
wca.agm.AGMDate,
wca.agm.AGMEGM,
wca.agm.AGMNo,
wca.agm.FYEDate,
wca.agm.AGMTime,
wca.agm.Add1,
wca.agm.Add2,
wca.agm.Add3,
wca.agm.Add4,
wca.agm.Add5,
wca.agm.Add6,
wca.agm.City,
wca.agm.CntryCD,
wca.agm.BondSecID
FROM wca.agm
INNER JOIN wca.scmst ON wca.agm.BondSecID = wca.scmst.SecID
INNER JOIN wca.bond ON wca.agm.BondSecID = wca.bond.SecID
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
AND wca.agm.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)


