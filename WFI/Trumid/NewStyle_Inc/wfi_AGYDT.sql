--filepath=O:\Datafeed\Debt\Trumid_NewStyle_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGYDT') as TableName,
agydt.Actflag,
agydt.AnnounceDate as Created,
agydt.Acttime as Changed,
agydt.AgydtID,
agydt.AgncyID,
agydt.EffectiveDate,
agydt.OldRegistrarName,
agydt.OldAdd1,
agydt.OldAdd2,
agydt.OldAdd3,
agydt.OldAdd4,
agydt.OldAdd5,
agydt.OldAdd6,
agydt.OldCity,
agydt.OldCntryCD,
agydt.OldWebSite,
agydt.OldContact1,
agydt.OldTel1,
agydt.OldFax1,
agydt.Oldemail1,
agydt.OldContact2,
agydt.OldTel2,
agydt.OldFax2,
agydt.Oldemail2,
agydt.OldState,
agydt.NewRegistrarName,
agydt.NewAdd1,
agydt.NewAdd2,
agydt.NewAdd3,
agydt.NewAdd4,
agydt.NewAdd5,
agydt.NewAdd6,
agydt.NewCity,
agydt.NewCntryCD,
agydt.NewWebSite,
agydt.NewContact1,
agydt.NewTel1,
agydt.NewFax1,
agydt.Newemail1,
agydt.NewContact2,
agydt.NewTel2,
agydt.NewFax2,
agydt.Newemail2,
agydt.NewState
from wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
AND wca.agydt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)


