--filepath=O:\Datafeed\Debt\Trumid_NewStyle_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LIQ') as TableName,
wca.liq.Actflag,
wca.liq.AnnounceDate as Created,
wca.liq.Acttime as Changed,
wca.liq.LiqID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.liq.IssID,
wca.liq.Liquidator,
wca.liq.LiqAdd1,
wca.liq.LiqAdd2,
wca.liq.LiqAdd3,
wca.liq.LiqAdd4,
wca.liq.LiqAdd5,
wca.liq.LiqAdd6,
wca.liq.LiqCity,
wca.liq.LiqCntryCD,
wca.liq.LiqTel,
wca.liq.LiqFax,
wca.liq.LiqEmail,
wca.liq.RdDate
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
AND wca.liq.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)

