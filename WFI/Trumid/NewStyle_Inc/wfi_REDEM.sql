--filepath=O:\Datafeed\Debt\Trumid_NewStyle_Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\Trumid_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
'REDEM' as TableName,
wca.redem.Actflag,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.RedemID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.redem.RedemDate,
wca.redem.CurenCD as RedemCurrency,
wca.redem.RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
wca.redem.AmountRedeemed,
wca.redem.RedemPremium,
wca.redem.RedemPercent,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,
wca.redem.PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.redem.InDefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.ExpTime,
wca.redem.ExpTimeZone,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
inner join wca.scexh on wca.bond.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scexh.ExchgCD = 'USAMEX' or wca.scexh.ExchgCD = 'USBATS' or wca.scexh.ExchgCD = 'USBND'
or wca.scexh.ExchgCD = 'USBOS' or wca.scexh.ExchgCD = 'USCHI' or wca.scexh.ExchgCD = 'USCIN'
or wca.scexh.ExchgCD = 'USCOMP' or wca.scexh.ExchgCD = 'USFNBB' or wca.scexh.ExchgCD = 'USNASD'
or wca.scexh.ExchgCD = 'USNNAS' or wca.scexh.ExchgCD = 'USNYSE' or wca.scexh.ExchgCD = 'USOTC'
or wca.scexh.ExchgCD = 'USOTCB' or wca.scexh.ExchgCD = 'USPAC' or wca.scexh.ExchgCD = 'USPHIL'
or wca.scexh.ExchgCD = 'USPINK' or wca.scexh.ExchgCD = 'USPORT' or wca.scexh.ExchgCD = 'USPSGM'
or wca.scexh.ExchgCD = 'USTRCE' or wca.scexh.ExchgCD = 'USUL' )
and (wca.issur.IssType = 'CORP' or wca.issur.IssType = 'GOV')
AND wca.redem.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)

