--filepath=O:\Upload\Acc\295\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_GICCH
--fileheadertext=EDI_GICCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BLT_BorsaItaliana\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('GICCH') as TableName,
wca.gicch.Actflag,
wca.gicch.AnnounceDate as Created,
wca.gicch.Acttime as Changed,
wca.gicch.IssID,
wca.gicch.EffectiveDate,
wca.gicch.OldGICS,
wca.gicch.NewGICS,
wca.gicch.GICSChID
from wca.gicch
inner join wca.scmst on wca.gicch.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=295 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=295 and actflag='U')
and gicch.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))