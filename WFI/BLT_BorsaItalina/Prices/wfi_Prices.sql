--filepath=O:\Upload\Acc\295\Feed\Prices\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_Prices
--fileheadertext=EDI_Prices_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BLT_BorsaItaliana\Prices\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
prices.lasttrade.MIC,
prices.lasttrade.LocalCode as pricefilesymbol,
prices.lasttrade.Isin,
prices.lasttrade.Currency,
prices.lasttrade.PriceDate,	
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Close,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Last,
prices.lasttrade.Bid,
prices.lasttrade.Bidsize,
prices.lasttrade.Asksize,
prices.lasttrade.TradedVolume,	
prices.lasttrade.SecID,
prices.lasttrade.MktCloseDate,	
prices.lasttrade.VolFlag,
prices.lasttrade.Issuername,	
prices.lasttrade.SecTyCD,
prices.lasttrade.SecurityDesc,	
prices.lasttrade.Sedol,
prices.lasttrade.Uscode,
prices.lasttrade.PrimaryExchgCD,	
prices.lasttrade.ExchgCD,
prices.lasttrade.TradedValue,	
prices.lasttrade.TotalTrades,	
prices.lasttrade.Comment
from prices.lasttrade
inner join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid = '295' and actflag <> 'D')
and (prices.lasttrade.ExchgCD = 'GBLSE' or prices.lasttrade.ExchgCD = 'PTBVL' or prices.lasttrade.ExchgCD = 'NLENA'
or prices.lasttrade.ExchgCD = 'GBENLN' or prices.lasttrade.ExchgCD = 'FRPEN' or prices.lasttrade.ExchgCD = 'BEENB')