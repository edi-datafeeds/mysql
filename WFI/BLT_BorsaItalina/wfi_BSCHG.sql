--filepath=O:\Upload\Acc\295\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BLT_BorsaItaliana\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('BSCHG') as TableName,
wca.bschg.Actflag,
wca.bschg.AnnounceDate as Created,
wca.bschg.Acttime as Changed,
wca.bschg.BschgID,
wca.bschg.SecID,
wca.scmst.ISIN,
wca.bschg.NotificationDate,
wca.bschg.OldBondType,
wca.bschg.NewBondType,
wca.bschg.OldCurenCD,
wca.bschg.NewCurenCD,
wca.bschg.OldPIU,
wca.bschg.NewPIU,
wca.bschg.OldInterestBasis,
wca.bschg.NewInterestBasis,
wca.bschg.Eventtype,
wca.bschg.OldInterestCurrency,
wca.bschg.NewInterestCurrency,
wca.bschg.OldMaturityCurrency,
wca.bschg.NewMaturityCurrency,
wca.bschg.OldIntBusDayConv,
wca.bschg.NewIntBusDayConv,
wca.bschg.OldMatBusDayConv,
wca.bschg.NewMatBusDayConv,
wca.bschg.BschgNotes as Notes
from wca.bschg
inner join wca.bond on wca.bschg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=295 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=295 and actflag='U')
and bschg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))