--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssID,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.FinancialYearEnd,
wca.issur.Shortname,
wca.issur.LegalName,
-- case when wca.issur.cntryofincorp='AA' then 'S' when wca.issur.isstype='GOV' then 'G' when wca.issur.isstype='GOVAGENCY' then 'Y' else 'C' end as Isstype,
wca.issur.isstype,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.LEI,
wca.issur.NAICS,
wca.issur.CntryIncorpNumber,
wca.issur.ShellCompany,
wca.issur.IssurNotes as Notes
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and issur.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))