--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCEXH') as TableName,
wca.scexh.Actflag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScExhID,
wca.scexh.SecID,
wca.scmst.ISIN,
wca.scexh.ExchgCD,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
wca.scexh.DelistDate,
wca.scexh.LocalCode,
SUBSTRING(wca.scexh.JunkLocalcode,1,50) as JunkLocalcode,
wca.scexh.PriceTick,
wca.scexh.TickSize,
wca.scexh.ListCategory,
wca.scexh.ScexhNotes As Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and scexh.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))