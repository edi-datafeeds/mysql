--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('CTCHG') as TableName,
wca.zctchg.Actflag,
wca.zctchg.AnnounceDate as Created,
wca.zctchg.Acttime as Changed, 
wca.zctchg.zCtChgID as CtChgID,
wca.zctchg.SecID,
wca.scmst.ISIN,
wca.zctchg.EffectiveDate,
wca.zctchg.OldResultantRatio,
wca.zctchg.NewResultantRatio,
wca.zctchg.OldSecurityRatio,
wca.zctchg.NewSecurityRatio,
wca.zctchg.OldCurrency,
wca.zctchg.NewCurrency,
wca.zctchg.OldCurPair,
wca.zctchg.NewCurPair,
wca.zctchg.OldConversionPrice,
wca.zctchg.NewConversionPrice,
wca.zctchg.ResSectyCD,
wca.zctchg.OldResSecID,
wca.zctchg.NewResSecID,
wca.zctchg.EventType,
wca.zctchg.RelEventID,
wca.zctchg.OldFromDate,
wca.zctchg.NewFromDate,
wca.zctchg.OldTodate,
wca.zctchg.NewToDate,
wca.zctchg.ConvtID,
wca.zctchg.OldFXRate,
wca.zctchg.NewFXRate,
wca.zctchg.OldPriceAsPercent,
wca.zctchg.NewPriceAsPercent,
wca.zctchg.OldMinPrice,
wca.zctchg.NewMinPrice,
wca.zctchg.OldMaxPrice,
wca.zctchg.NewMaxPrice,
wca.zctchg.zCtChgNotes as Notes
from wca.zctchg
inner join wca.bond on wca.zctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and zctchg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))