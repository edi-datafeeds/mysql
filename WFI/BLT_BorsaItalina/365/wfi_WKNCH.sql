--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('WKNCH') as TableName,
wca.wknch.Actflag,
wca.wknch.AnnounceDate as Created,
wca.wknch.Acttime as Changed,
wca.wknch.SecID,
wca.wknch.EffectiveDate,
wca.wknch.WknChID,
wca.wknch.Eventtype,
wca.wknch.RelEventID,
wca.wknch.OldWKN,
wca.wknch.NewWKN
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and wknch.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))