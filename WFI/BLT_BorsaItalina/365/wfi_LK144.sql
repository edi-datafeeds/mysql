--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LK144
--fileheadertext=EDI_LK144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LK144') as TableName,
wca.lk144.Actflag,
wca.lk144.AnnounceDate as Created,
wca.lk144.Acttime as Changed,
wca.lk144.LK144ID,
wca.lk144.SecID144A,
wca.scmst.isin as ISIN144A,
wca.lk144.SecIDRegS,
s1.isin as ISINRegS,
wca.lk144.SecIDUnrestricted,
wca.lk144.LinkOutstandingAmount,
wca.lk144.LinkOutstandingAmountDate
from wca.lk144
inner join wca.scmst on wca.lk144.SecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.lk144.SecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and lk144.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))