--filepath=O:\Upload\Acc\365\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BNDLQ
--fileheadertext=EDI_BNDLQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\365\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BNDLQ') as TableName,
wca.bndlq.Actflag,
wca.bndlq.AnnounceDate as Created,
wca.bndlq.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.bndlq.RdID,
wca.bndlq.BndlqID,
wca.bndlq.CurenCD,
wca.bndlq.LiquidationPrice,
wca.bndlq.LiqSeq,
wca.bndlq.Notes
from wca.bndlq
inner join wca.rd on wca.bndlq.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=365 and actflag='U')
and bndlq.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))