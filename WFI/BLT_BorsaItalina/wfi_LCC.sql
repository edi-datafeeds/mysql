--filepath=O:\Upload\Acc\295\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BLT_BorsaItaliana\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LCC') as TableName,
wca.lcc.Actflag,
wca.lcc.AnnounceDate as Created,
wca.lcc.Acttime as Changed,
wca.lcc.LccID,
wca.lcc.SecID,
wca.scmst.ISIN,
wca.lcc.ExchgCD,
wca.lcc.EffectiveDate,
wca.lcc.OldLocalCode,
wca.lcc.NewLocalCode,
wca.lcc.EventType,
wca.lcc.RelEventID
from wca.lcc
inner join wca.bond on wca.lcc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=295 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=295 and actflag='U')
and lcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))