--filepath=O:\Datafeed\Debt\Quovo\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Quovo\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCAGY') as TableName,
wca.scagy.Actflag,
wca.scagy.AnnounceDate as Created,
wca.scagy.Acttime as Changed,
wca.scagy.ScagyID,
wca.scagy.SecID,
wca.scmst.ISIN,
wca.scagy.Relationship,
wca.scagy.AgncyID,
wca.scagy.GuaranteeType,
wca.scagy.SpStartDate,
wca.scagy.SpEndDate,
wca.scagy.Notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid and wca.scexh.actflag <> 'D'
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where

wca.continent.cntrycd = 'US'
and wca.scagy.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
