--filepath=O:\Datafeed\Debt\Quovo\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Quovo\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('SFUND') as TableName,
wca.sfund.Actflag,
wca.sfund.AnnounceDate as Created,
wca.sfund.Acttime as Changed,
wca.sfund.SfundID,
wca.sfund.SecID,
wca.scmst.ISIN,
wca.sfund.SfundDate,
wca.sfund.CurenCD as Sinking_Currency,
wca.sfund.Amount,
wca.sfund.AmountasPercent,
wca.sfund.SfundNotes as Notes
from wca.sfund
inner join wca.bond on wca.sfund.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid and wca.scexh.actflag <> 'D'
left outer join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
inner join wca.continent on wca.exchg.cntrycd = wca.continent.cntrycd
where

wca.continent.cntrycd = 'US'
and wca.sfund.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
