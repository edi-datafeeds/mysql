--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LAWSTNOTES') as TableName,
wca.lawst.Actflag,
wca.lawst.LawstID,
wca.lawst.IssId,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
lawst.Issid in (select wca.scmst.issid from client.pfuscode
inner join wca.scmst on client.pfuscode.code = wca.scmst.isin
where (client.pfuscode.accid=396 and client.pfuscode.actflag='I')
or (client.pfuscode.accid=396 and client.pfuscode.actflag='U'
and lawst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)))