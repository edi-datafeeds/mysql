--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CH144
--fileheadertext=CH144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CH144') as TableName,
wca.ch144.Actflag,
wca.ch144.AnnounceDate as Created,
wca.ch144.Acttime as Changed,
wca.ch144.LK144ID,
wca.ch144.EffectiveDate,
wca.ch144.SerialNumber,
wca.ch144.OldSecID144A,
wca.ch144.NewSecID144a,
wca.scmst.ISIN as ISIN144A,
wca.ch144.OldSecIDRegS,
wca.ch144.NewSecIDRegS,
s1.ISIN as ISINRegS,
wca.ch144.OldSecIDUnrestricted,
wca.ch144.NewSecIDUnrestricted,
wca.ch144.OldLinkOutstandingAmount,
wca.ch144.NewLinkOutstandingAmount,
wca.ch144.OldLinkOutstandingAmountDate,
wca.ch144.NewLinkOutstandingAmountDate,
wca.ch144.EventType,
wca.ch144.RelEventID,
wca.ch144.CH144ID
from wca.ch144
inner join wca.scmst on wca.ch144.OldSecID144A = wca.scmst.secid
inner join wca.scmst as s1 on wca.ch144.OldSecIDRegS = s1.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(wca.scmst.uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(wca.scmst.uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.ch144.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))