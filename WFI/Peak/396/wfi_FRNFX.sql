--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.zfrnfx.Actflag,
wca.zfrnfx.AnnounceDate as Created,
wca.zfrnfx.Acttime as Changed,
wca.zfrnfx.zFrnfxID AS FrnfxID,
wca.zfrnfx.SecID,
wca.scmst.ISIN,
wca.zfrnfx.EffectiveDate,
wca.zfrnfx.OldFRNType,
wca.zfrnfx.OldMarkup As OldFrnMargin,
wca.zfrnfx.OldMinimumInterestRate,
wca.zfrnfx.OldMaximumInterestRate,
wca.zfrnfx.OldRounding,
wca.zfrnfx.NewFRNType,
wca.zfrnfx.NewMarkup As NewFrnMargin,
wca.zfrnfx.NewMinimumInterestRate,
wca.zfrnfx.NewMaximumInterestRate,
wca.zfrnfx.NewRounding,
wca.zfrnfx.Eventtype,
wca.zfrnfx.OldFrnIntAdjFreq,
wca.zfrnfx.NewFrnIntAdjFreq,
wca.zfrnfx.OldFRNBenchmarkCntryCD,
wca.zfrnfx.OldFRNBenchmarkCurenCD,
wca.zfrnfx.OldFRNBenchmarkReference,
wca.zfrnfx.OldFRNBenchmarkFrequency,
wca.zfrnfx.NewFRNBenchmarkCntryCD,
wca.zfrnfx.NewFRNBenchmarkCurenCD,
wca.zfrnfx.NewFRNBenchmarkReference,
wca.zfrnfx.NewFRNBenchmarkFrequency
from wca.zfrnfx
inner join wca.bond on wca.zfrnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.zfrnfx.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))