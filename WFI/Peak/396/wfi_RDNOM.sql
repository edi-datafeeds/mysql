--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('RDNOM') as TableName,
wca.rdnom.Actflag,
wca.rdnom.AnnounceDate as Created,
wca.rdnom.Acttime as Changed, 
wca.rdnom.RdnomID,
wca.rdnom.SecID,
wca.scmst.ISIN,
wca.rdnom.EffectiveDate,
wca.rdnom.OldDenomination1,
wca.rdnom.OldDenomination2,
wca.rdnom.OldDenomination3,
wca.rdnom.OldDenomination4,
wca.rdnom.OldDenomination5,
wca.rdnom.OldDenomination6,
wca.rdnom.OldDenomination7,
wca.rdnom.OldMinimumDenomination,
wca.rdnom.OldDenominationMultiple,
wca.rdnom.NewDenomination1,
wca.rdnom.NewDenomination2,
wca.rdnom.NewDenomination3,
wca.rdnom.NewDenomination4,
wca.rdnom.NewDenomination5,
wca.rdnom.NewDenomination6,
wca.rdnom.NewDenomination7,
wca.rdnom.NewMinimumDenomination,
wca.rdnom.NewDenominationMultiple,
wca.rdnom.Notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
