--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BNDPA
--fileheadertext=BNDPA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BNDPA') as TableName,
wca.bndpa.Actflag,
wca.bndpa.AnnounceDate as Created,
wca.bndpa.Acttime as Changed,
wca.bndpa.SecID,
wca.scmst.ISIN,
wca.bndpa.EffectiveDate,
wca.bndpa.ImpactOn,
wca.bndpa.AlterationValue,
wca.bndpa.BndpaID
from wca.bndpa
inner join wca.bond on wca.bndpa.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.bndpa.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))