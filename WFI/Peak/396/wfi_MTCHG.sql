--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MTCHG') as TableName,
wca.zmtchg.Actflag,
wca.zmtchg.AnnounceDate as Created,
wca.zmtchg.Acttime as Changed,
wca.zmtchg.zMtChgID as MtChgID,
wca.zmtchg.SecID,
wca.scmst.ISIN,
wca.zmtchg.NotificationDate,
wca.zmtchg.OldMaturityDate,
wca.zmtchg.NewMaturityDate,
wca.zmtchg.Reason,
wca.zmtchg.EventType,
wca.zmtchg.OldMaturityBenchmarkCntryCD,
wca.zmtchg.OldMaturityBenchmarkCurenCD,
wca.zmtchg.OldMaturityBenchmarkReference,
wca.zmtchg.OldMaturityBenchmarkFrequency,
wca.zmtchg.NewMaturityBenchmarkCntryCD,
wca.zmtchg.NewMaturityBenchmarkCurenCD,
wca.zmtchg.NewMaturityBenchmarkReference,
wca.zmtchg.NewMaturityBenchmarkFrequency,
wca.zmtchg.zMtChgNotes as Notes
from wca.zmtchg
inner join wca.bond on wca.zmtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.zmtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
