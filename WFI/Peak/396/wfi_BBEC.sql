--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBEC') as tablename,
wca.bbec.ActFlag,
wca.bbec.AnnounceDate as Created,
wca.bbec.Acttime as Changed,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.scmst.ISIN,
wca.bbec.BbeId,
wca.bbec.Oldexchgcd,
wca.bbec.Oldcurencd,
wca.bbec.EffectiveDate,
wca.bbec.Newexchgcd,
wca.bbec.Newcurencd,
wca.bbec.OldbbgexhId,
wca.bbec.NewbbgexhId,
wca.bbec.Oldbbgexhtk,
wca.bbec.Newbbgexhtk,
wca.bbec.ReleventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))

