--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRDRT') as TableName,
wca.crdrt.Actflag,
wca.crdrt.AnnounceDate as Created,
wca.crdrt.Acttime as Changed, 
wca.crdrt.SecID,
wca.scmst.ISIN,
wca.crdrt.RatingAgency,
wca.crdrt.RatingDate,
wca.crdrt.Rating,
wca.crdrt.Direction,
wca.crdrt.WatchList,
wca.crdrt.WatchListReason
from wca.crdrt
inner join wca.bond on wca.crdrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.crdrt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
