--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SDCHG') as TableName,
wca.sdchg.Actflag,
wca.sdchg.AnnounceDate as Created, 
wca.sdchg.Acttime as Changed,
wca.sdchg.SdChgID,
wca.sdchg.SecID,
wca.scmst.ISIN,
wca.sdchg.CntryCD as OldCntryCD,
wca.sdchg.EffectiveDate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.EventType,
wca.sdchg.RcntryCD as OldRcntryCD,
wca.sdchg.RelEventID,
wca.sdchg.NewCntryCD,
wca.sdchg.NewRcntryCD,
wca.sdchg.sdchgNotes As Notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.sdchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))