--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CURRD') as TableName,
wca.currd.Actflag,
wca.currd.AnnounceDate as Created,
wca.currd.Acttime as Changed,
wca.currd.CurrdID,
wca.currd.SecID,
wca.scmst.ISIN,
wca.currd.EffectiveDate,
wca.currd.OldCurenCD,
wca.currd.NewCurenCD,
wca.currd.OldParValue,
wca.currd.NewParValue,
wca.currd.EventType,
wca.currd.CurRdNotes as Notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.currd.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
