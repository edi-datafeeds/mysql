--filepath=O:\upload\acc\396\feed\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MUISS
--fileheadertext=MUISS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUISS') as TableName,
wca.muiss.Actflag,
wca.muiss.AnnounceDate as Created,
wca.muiss.Acttime as Changed,
wca.muiss.IssID,
wca.muiss.MuIssID,
wca.muiss.MuniIssuanceName,
wca.muiss.MuniSeries,
wca.muiss.RefundingFlag,
wca.muiss.MuniSeriesIssueAmount,
wca.muiss.MuniCategory,
wca.muiss.RepaymentSource,
wca.muiss.TaxStatus,
wca.muiss.TaxExemptAmount,
wca.muiss.TaxExemptBankQualified,
wca.muiss.Notes
from wca.muiss
inner join wca.scmst on wca.muiss.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.muiss.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))