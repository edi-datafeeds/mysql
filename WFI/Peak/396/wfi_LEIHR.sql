--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LEIHR
--fileheadertext=LEIHR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LEIHR') as TableName,
leihr.Actflag,
leihr.Announcedate as 'Created',
leihr.Acttime as 'Changed',
leihr.LeihrID,
leihr.BLei,
leihr.DpLei,
leihr.DpStatus,
leihr.UpLei,
leihr.UpStatus,
leihr.IbLei,
leihr.IbStatus
from wca.leihr
inner join wca.lei on wca.leihr.blei = wca.lei.lei
inner join wca.issur on wca.lei.lei = wca.issur.lei
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.leihr.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
