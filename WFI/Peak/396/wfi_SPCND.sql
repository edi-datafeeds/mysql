--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPCND
--fileheadertext=SPCND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPCND') as TableName,
spcnd.Actflag,
spcnd.AnnounceDate as Created,
spcnd.Acttime as Changed,
scmst.SecID,
wca.scmst.ISIN,
spcnd.SpcndID,
spcnd.Markup,
spcnd.UnderlyingCondition,
spcnd.WithReferenceTo,
spcnd.Level1Currency,
spcnd.Level1Percent,
spcnd.Level1Value,
spcnd.Level2Currency,
spcnd.Level2Percent,
spcnd.Level2Value
from wca.spcnd
inner join wca.spun on wca.spcnd.SpunID = wca.spun.SpunID
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.spcnd.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
