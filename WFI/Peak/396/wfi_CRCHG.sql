--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRCHG') as TableName,
wca.crchg.Actflag,
wca.crchg.AnnounceDate as Created,
wca.crchg.Acttime as Changed, 
wca.crchg.CrChgID,
wca.crchg.SecID,
wca.scmst.ISIN,
wca.crchg.RatingAgency,
wca.crchg.NewRatingDate,
wca.crchg.OldRating,
wca.crchg.NewRating,
wca.crchg.OldDirection,
wca.crchg.OldWatchList,
wca.crchg.OldWatchListReason,
wca.crchg.EventType,
wca.crchg.OldRatingDate,
wca.crchg.NewDirection,
wca.crchg.NewWatchList,
wca.crchg.NewWatchListReason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.crchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
