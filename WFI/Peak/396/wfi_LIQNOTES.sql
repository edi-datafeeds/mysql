--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LIQNOTES') as TableName,
wca.liq.Actflag,
wca.liq.LiqID,
wca.liq.Issid,
wca.liq.LiquidationTerms as Notes
from wca.liq
where
liq.Issid in (select wca.scmst.issid from client.pfuscode
inner join wca.scmst on client.pfuscode.code = wca.scmst.isin
where (client.pfuscode.accid=396 and client.pfuscode.actflag='I')
or (client.pfuscode.accid=396 and client.pfuscode.actflag='U'
and liq.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)))    