--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_PFDS
--fileheadertext=PFDS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('PFDS') as Tablename,
wca.pfds.Actflag,
wca.pfds.AnnounceDate as Created,
wca.pfds.Acttime as Changed,
wca.pfds.SecID,
wca.scmst.ISIN,
wca.pfds.SecuritiesOffered,
wca.pfds.GreenShoeOption,
wca.pfds.LiquidationPreference,
wca.pfds.PfdsID
from wca.pfds
inner join wca.bond on wca.pfds.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.pfds.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
