--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGYDT') as TableName,
agydt.Actflag,
agydt.AnnounceDate as Created,
agydt.Acttime as Changed,
agydt.AgydtID,
agydt.AgncyID,
agydt.EffectiveDate,
agydt.OldRegistrarName,
agydt.OldAdd1,
agydt.OldAdd2,
agydt.OldAdd3,
agydt.OldAdd4,
agydt.OldAdd5,
agydt.OldAdd6,
agydt.OldCity,
agydt.OldCntryCD,
agydt.OldWebSite,
agydt.OldContact1,
agydt.OldTel1,
agydt.OldFax1,
agydt.Oldemail1,
agydt.OldContact2,
agydt.OldTel2,
agydt.OldFax2,
agydt.Oldemail2,
agydt.OldState,
agydt.NewRegistrarName,
agydt.NewAdd1,
agydt.NewAdd2,
agydt.NewAdd3,
agydt.NewAdd4,
agydt.NewAdd5,
agydt.NewAdd6,
agydt.NewCity,
agydt.NewCntryCD,
agydt.NewWebSite,
agydt.NewContact1,
agydt.NewTel1,
agydt.NewFax1,
agydt.Newemail1,
agydt.NewContact2,
agydt.NewTel2,
agydt.NewFax2,
agydt.Newemail2,
agydt.NewState
from wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
