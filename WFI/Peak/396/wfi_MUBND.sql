--filepath=O:\upload\acc\396\feed\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MUBND
--fileheadertext=MUBND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUBND') as TableName,
wca.mubnd.Actflag,
wca.mubnd.AnnounceDate as Created,
wca.mubnd.Acttime as Changed,
wca.mubnd.MuBndID,
wca.mubnd.SecID,
wca.mubnd.MuIssID,
wca.mubnd.TaxCreditBasis,
wca.mubnd.TaxCreditRate,
wca.mubnd.FirstTaxCreditDate,
wca.mubnd.MuniMaturityStyle,
wca.mubnd.Notes
from wca.mubnd
inner join wca.bond on wca.mubnd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.mubnd.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))