--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SCXTC
--fileheadertext=SCXTC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCXTC') as TableName,
wca.scxtc.Actflag,
wca.scxtc.AnnounceDate as Created,
wca.scxtc.Acttime as Changed,
wca.scxtc.ISIN,
wca.bond.SecID,
wca.scxtc.ScExhID,
wca.scxtc.CurenCD,
wca.scxtc.ListDate,
wca.scxtc.LocalCode,
wca.scxtc.PriceTick,
wca.scxtc.TickSize,
wca.scxtc.ScxtcID
from wca.scxtc
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))