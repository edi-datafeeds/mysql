--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CIKCH
--fileheadertext=CIKCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CIKCH') as TableName,
wca.cikch.Actflag,
wca.cikch.AnnounceDate as Created,
wca.cikch.Acttime as Changed,
wca.cikch.IssID,
wca.cikch.EffectiveDate,
wca.cikch.OldCIK,
wca.cikch.NewCIK,
wca.cikch.CIKChID
from wca.cikch
inner join wca.scmst on wca.cikch.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.cikch.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))