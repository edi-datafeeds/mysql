--filepath=O:\upload\acc\396\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SecID,
wca.scmst.ISIN,
wca.auct.AuctionDate,
wca.auct.AuctionAmount as AmountOffered,
wca.auct.IssueDate,
wca.auct.AmountAccepted,
wca.auct.AuctionID,
wca.auct.AuctionAnnouncementDate,
wca.auct.AuctionStatus,
wca.auct.BidAmount,
wca.auct.Bids,
wca.auct.NonCompetitiveBidAmount,
wca.auct.NonCompetitiveBids,
wca.auct.CompetitiveBidAmount,
wca.auct.CompetitiveBids,
wca.auct.BidsAccepted,
wca.auct.NonCompetitiveAmountAccepted,
wca.auct.NonCompetitiveBidsAccepted,
wca.auct.CompetitiveAmountAccepted,
wca.auct.CompetitiveBidsAccepted,
wca.auct.IssuersQuotaAmount,
wca.auct.CutOffPrice,
wca.auct.CutoffYield,
wca.auct.HighestAcceptedPrice,
wca.auct.LowestAcceptedYield,
wca.auct.AverageAcceptedPrice,
wca.auct.AverageAcceptedYield,
wca.auct.LowestAcceptedPrice,
wca.auct.HighestAcceptedYield,
wca.auct.WeightedAveragePrice,
wca.auct.WeightedAverageYield,
wca.auct.BidToCoverRatio,
wca.auct.YieldTailAtAuction
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.auct.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))
