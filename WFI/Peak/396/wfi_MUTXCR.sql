--filepath=O:\upload\acc\396\feed\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MUTXCR
--fileheadertext=MUTXCR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\396\feed\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUTXCR') as TableName,
wca.mutxcr.Actflag,
wca.mutxcr.AnnounceDate as Created,
wca.mutxcr.Acttime as Changed,
wca.mutxcr.MuTxCrID,
wca.mutxcr.MuBndID,
wca.mutxcr.TaxCreditDate,
wca.mutxcr.TaxCreditAmount
from wca.mutxcr
inner join wca.mubnd on wca.mutxcr.MuBndID = wca.mubnd.MuBndID
inner join wca.scmst on wca.mubnd.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(uscode in (select code from client.pfuscode where accid=396 and actflag='I'))
or
(uscode in (select code from client.pfuscode where accid=396 and actflag='U')
and wca.mutxcr.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))