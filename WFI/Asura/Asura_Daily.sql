--filepath=O:\Datafeed\Debt\Asura\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_Asura
--fileheadertext=EDI_Asura_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Asura\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT DISTINCT 

-- BOND

wca.bond.Actflag as BOND_ActFlag,
wca.bond.AnnounceDate as BOND_Created,
wca.bond.Acttime as BOND_Changed,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bond.BondType,
wca.bond.DebtMarket,
wca.bond.CurenCD as DebtCurrency,
case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue else wca.bond.parvalue end as NominalValue,
case when (wca.bond.Source like 'AU%' or wca.bond.Source like 'ASX%')
and wca.scmst.ISIN like 'AU%'
and (wca.bond.BondType = 'CD' OR wca.bond.BondType = 'CP')
and wca.bond.IssueDate is null then wca.bond.SourceDate 
else wca.bond.IssueDate end as IssueDate,
wca.bond.IssueCurrency,
wca.bond.IssuePrice,
wca.bond.IssueAmount,
wca.bond.IssueAmountDate,
wca.bond.OutstandingAmount,
wca.bond.OutstandingAmountDate,
wca.bond.InterestBasis,
wca.bond.InterestRate,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.OddFirstCoupon,
wca.bond.FirstCouponDate,
wca.bond.OddLastCoupon,
wca.bond.LastCouponDate,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,
wca.bondx.DomesticTaxRate,
wca.bondx.NonResidentTaxRate,
wca.bond.FRNType,
wca.bond.FRNIndexBenchmark,
wca.bond.Markup as FrnMargin,
wca.bond.MinimumInterestRate as FrnMinInterestRate,
wca.bond.MaximumInterestRate as FrnMaxInterestRate,
wca.bond.Rounding,
wca.bondx.Series,
wca.bondx.Class,
wca.bondx.OnTap,
wca.bondx.MaximumTapAmount,
wca.bondx.TapExpiryDate,
wca.bond.Guaranteed,
wca.bond.SecuredBy,
wca.bond.SecurityCharge,
wca.bond.Subordinate,
wca.bond.SeniorJunior,
wca.bond.WarrantAttached,
wca.bond.MaturityStructure,
wca.bond.Perpetual,
wca.bond.MaturityDate,
wca.bond.MaturityExtendible,
wca.bond.Callable,
wca.bond.Puttable,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.Denomination7,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.bond.Strip,
wca.bond.StripInterestNumber, 
wca.bond.Bondsrc,
wca.bond.MaturityBenchmark,
wca.bond.ConventionMethod,
wca.bond.FrnIntAdjFreq as FrnInterestAdjFreq,
wca.bond.IntBusDayConv as InterestBusDayConv,
wca.bond.InterestCurrency,
wca.bond.MatBusDayConv as MaturityBusDayConv,
wca.bond.MaturityCurrency, 
wca.bondx.TaxRules,
wca.bond.VarIntPayDate as VarInterestPaydate,
wca.bond.PriceAsPercent,
wca.bond.PayOutMode,
wca.bond.Cumulative,
case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
     when rtrim(wca.bond.largeparvalue)='' then null
     when rtrim(wca.bond.MatPriceAsPercent)='' then null
     else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
wca.bond.MatPriceAsPercent,
wca.bond.SinkingFund,
wca.bondx.GovCity,
wca.bondx.GovState,
wca.bondx.GovCntry,
wca.bondx.GovLawLkup as GovLaw,
wca.bondx.GoverningLaw as GovLawNotes,
wca.bond.Municipal,
wca.bond.PrivatePlacement,
wca.bond.Syndicated,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.Collateral,
wca.bond.CoverPool,
wca.bond.PikPay,
wca.bond.YieldAtIssue,
wca.bond.CoCoTrigger,
wca.bond.CoCoAct,
wca.bond.NonViability as CoCoNonViability,
wca.bondx.Taxability,
wca.bond.LatestAppliedINTPYAnlCpnRateDate,
wca.bond.LatestAppliedINTPYAnlCpnRate,
wca.bond.FirstAnnouncementDate,
wca.bond.PerformanceBenchmarkISIN,
wca.bondx.InterestGraceDays,
wca.bondx.InterestGraceDayConvention,
wca.bondx.MatGraceDays,
wca.bondx.MatGraceDayConvention,
wca.bond.ExpMatDate,
wca.bond.GreenBond,
wca.bond.IndicativeIssAmtDate,
wca.bond.IndicativeIssAmt,
wca.bond.CoCoCapRatioTriggerPercent,
wca.bond.ZCMarker,
wca.bond.Notes AS BOND_Notes,


-- SCMST


wca.scmst.Actflag AS SCMST_ActFlag,
wca.scmst.AnnounceDate as SCMST_Created,
wca.scmst.Acttime as SCMST_Changed,
wca.scmst.ParentSecID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.CIC,
wca.scmst.StatusReason,
wca.scmst.IssuePrice,
wca.scmst.PaidUpValue,
wca.scmst.Voting,
wca.scmst.FYENPPDate,
wca.scmst.VALOREN,
wca.scmst.SharesOutstanding,
wca.scmst.SharesOutstandingDate,
wca.scmst.UmprgID,
wca.scmst.NoParValue,
wca.scmst.ExpDate,
wca.scmst.VotePerSec,
wca.scmst.FreeFloat,
wca.scmst.FreeFloatDate,
wca.scmst.TreasuryShares,
wca.scmst.TreasurySharesDate,
wca.scmst.FISN,
wca.scmst.scmstNotes As SCMST_Notes,


-- ISSUR


wca.issur.Actflag AS ISSUR_ActFlag,
wca.issur.AnnounceDate as ISSUR_Created,
wca.issur.Acttime as ISSUR_Changed,
wca.issur.IssID,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.FinancialYearEnd,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.isstype,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.LEI,
wca.issur.NAICS,
wca.issur.CntryIncorpNumber,
wca.issur.ShellCompany,
wca.issur.IssurNotes as ISSUR_Notes





from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
(wca.scmst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.bond.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.issur.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog));