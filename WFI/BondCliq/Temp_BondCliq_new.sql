--filepath=O:\Prodman\Dev\WFI\Feeds\BondCliq\Output\Load\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=Temp_BondCliq
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.ISIN,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,

CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN DATE_FORMAT(wca.exdt.paydate, '%Y-%m-%d')
ELSE ''
END AS PayDate,


CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN wca.intpy.AnlCoupRate
ELSE ''
END AS AnlCoupRate,

DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,

'' as Callput,
'' as CpType,
'' as CpoptID,

'' as FromDate,
'' as ToDate,
'' as CpCurrency,
'' as CpPrice,
'' as CpPriceAsPercent,

wca.bond.IssuePrice,
wca.bond.IssueDate,
wca.bond.Perpetual,

case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,

case when scexh.ExchgCD<>'' and scexh.ExchgCD is not null
     then scexh.ExchgCD
     end as ExchgCD,


CASE WHEN wca.scexh.LocalCode REGEXP '^[[:digit:]]' or substring(wca.scexh.LocalCode,1,1) ='.' THEN wca.scexh.LocalCode
     WHEN wca.scexh.LocalCode<>'' and wca.scexh.LocalCode is not null      
     THEN
       SUBSTRING_INDEX(
        SUBSTRING_INDEX(
         SUBSTRING_INDEX(
          SUBSTRING_INDEX(
           SUBSTRING_INDEX(
            SUBSTRING_INDEX(
             SUBSTRING_INDEX(
              SUBSTRING_INDEX(
               SUBSTRING_INDEX(
                SUBSTRING_INDEX(
                 SUBSTRING_INDEX(
                  SUBSTRING_INDEX(
                   SUBSTRING_INDEX(
                    SUBSTRING_INDEX(
                     SUBSTRING_INDEX(wca.scexh.LocalCode, '.', 1),
                      '/', 1),
                     '_', 1),
                    '-', 1),
                   ' ', 1),
                  '1', 1),
                 '2', 1),
                '3', 1),
               '4', 1),
              '5', 1),
             '6', 1),
            '7', 1),
           '8', 1),
          '9', 1),
         '0', 1) 
               
         else 'N/A'
         end AS LocalCode,
         
       
 case when prices.lasttrade.Currency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
     
case when prices.lasttrade.PriceDate='0000-00-00'
     then null
     else DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d')
     end as LastTradeDate,
     
 case when prices.lasttrade.Close<>'' and prices.lasttrade.Close is not null
     then prices.lasttrade.Close
     else '0'
     end as ClosingPrice,
     
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.VolFlag,    
     
 case when wca.scmst.CurenCD<>'' and wca.scmst.CurenCD is not null
     then wca.scmst.CurenCD
     end as PriceCurrency,
     
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity,
'' as Yield,
'' as YieldToWorst,
'' as KeyRateDuration,

wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD,
wca.issur.SIC,
wca.issur.NAICS,
wca.issur.GICS,
wca.bond.IssueAmount,
wca.bond.FirstCouponDate,
wca.bond.LastCouponDate,
wca.bond.IntCommencementDate
     

from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid

left outer join wca.rd on wca.bond.secid = wca.rd.secid
left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
left outer join wca.exdt ON wca.intpy.rdid = wca.exdt.RdID and 'INT'=wca.exdt.eventtype

inner join wca.scexh on wca.scmst.secid= wca.scexh.secid
left outer join prices.lasttrade on wca.scmst.secid = prices.lasttrade.secid and lasttrade.exchgcd=scexh.exchgcd
where
wca.scmst.CurenCD <>'' and wca.scmst.CurenCD is not null
and wca.scmst.actflag <> 'D'
and wca.scexh.actflag<>'D'
-- and wca.scmst.uscode = '912909AG3'
-- and wca.scmst.uscode in (select code from client.pfuscode where accid = 990)
and scexh.exchgcd like 'US%'
                               
union -- --------------------------------------------------------------------------------------------------------------

select distinct
wca.scmst.ISIN,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,

CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN DATE_FORMAT(wca.exdt.paydate, '%Y-%m-%d')
ELSE ''
END AS PayDate,


CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN wca.intpy.AnlCoupRate
ELSE ''
END AS AnlCoupRate,

DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,

'' as Callput,
'' as CpType,
'' as CpoptID,

'' as FromDate,
'' as ToDate,
'' as CpCurrency,
'' as CpPrice,
'' as CpPriceAsPercent,

wca.bond.IssuePrice,
wca.bond.IssueDate,
wca.bond.Perpetual,

case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,
     
  case when prices.lasttrade.ExchgCD<>'' and prices.lasttrade.ExchgCD is not null
     then prices.lasttrade.ExchgCD
     end as ExchgCD,
     
     
 CASE WHEN prices.lasttrade.LocalCode REGEXP '^[[:digit:]]' or substring(prices.lasttrade.LocalCode,1,1) ='.' THEN prices.lasttrade.LocalCode
     WHEN prices.lasttrade.LocalCode<>'' and prices.lasttrade.LocalCode is not null      
     THEN
       SUBSTRING_INDEX(
        SUBSTRING_INDEX(
         SUBSTRING_INDEX(
          SUBSTRING_INDEX(
           SUBSTRING_INDEX(
            SUBSTRING_INDEX(
             SUBSTRING_INDEX(
              SUBSTRING_INDEX(
               SUBSTRING_INDEX(
                SUBSTRING_INDEX(
                 SUBSTRING_INDEX(
                  SUBSTRING_INDEX(
                   SUBSTRING_INDEX(
                    SUBSTRING_INDEX(
                     SUBSTRING_INDEX(prices.lasttrade.LocalCode, '.', 1),
                      '/', 1),
                     '_', 1),
                    '-', 1),
                   ' ', 1),
                  '1', 1),
                 '2', 1),
                '3', 1),
               '4', 1),
              '5', 1),
             '6', 1),
            '7', 1),
           '8', 1),
          '9', 1),
         '0', 1) 
               
         else 'N/A'
         end AS LocalCode,
         
                
 case when prices.lasttrade.Currency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
     
case when prices.lasttrade.PriceDate='0000-00-00'
     then null
     else DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d')
     end as LastTradeDate,
     
 case when prices.lasttrade.Close<>'' and prices.lasttrade.Close is not null
     then prices.lasttrade.Close
     else '0'
     end as ClosingPrice,
     
     
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,
prices.lasttrade.VolFlag,

 case when prices.lasttrade.Currency<>'' and prices.lasttrade.Currency is not null
     then prices.lasttrade.Currency
     end as PriceCurrency,
     
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity,
'' as Yield,
'' as YieldToWorst,
'' as KeyRateDuration,

wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD,
wca.issur.SIC,
wca.issur.NAICS,
wca.issur.GICS,
wca.bond.IssueAmount,
wca.bond.FirstCouponDate,
wca.bond.LastCouponDate,
wca.bond.IntCommencementDate

     
     
from prices.lasttrade
left outer join wca.scmst on prices.lasttrade.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.rd on wca.bond.secid = wca.rd.secid
left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
left outer join wca.exdt ON wca.intpy.rdid = wca.exdt.RdID and 'INT'=wca.exdt.eventtype

inner join wca.scexh on wca.scmst.secid= wca.scexh.secid


where
prices.lasttrade.Currency <>'' and prices.lasttrade.Currency is not null
and wca.scmst.actflag <> 'D'
and wca.scexh.actflag<>'D'
-- and lasttrade.uscode = '912909AG3'
-- and lasttrade.uscode in (select code from client.pfuscode where accid = 990)
and lasttrade.exchgcd like 'US%';