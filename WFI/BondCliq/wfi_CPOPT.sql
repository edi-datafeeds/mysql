--filepath=O:\Prodman\Dev\WFI\Feeds\BondCliq\Output\Full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT distinct
upper('CPOPT') as TableName,
wca.cpopt.Actflag,
wca.cpopt.AnnounceDate as Created,
wca.cpopt.Acttime as Changed,
wca.cpopt.CpoptID,
bondsigma.bondcliq_temp.secid,
bondsigma.bondcliq_temp.ISIN,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.cptype,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice
from wca.cpopt
inner join bondsigma.bondcliq_temp on wca.cpopt.secid = bondsigma.bondcliq_temp.secid
where
wca.cpopt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
-- wca.cpopt.acttime = '2017-07-10 09:03:58'
