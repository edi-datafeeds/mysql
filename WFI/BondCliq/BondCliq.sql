--filepath=O:\Prodman\Dev\WFI\Feeds\BondCliq\Output\Full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BondCliq
--fileheadertext=EDI_BondCliq_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
isin,
secid,
uscode,
issuername,
DebtType,
SecurityDesc,
CntryofIncorp,
SectyCd,
DebtCurrency,
NominalValue,
OutstandingAmount,

-- InterestType,
case when uscode = '984121CQ4' then 'FXD' else InterestType end as InterestType,  

-- InterestRate,
case when uscode = '984121CQ4' then '3.625' else InterestRate end as InterestRate,  


InterestPaymentFrequency,
InterestAccrualConvention,
case when PayDate='0000-00-00'
     then null
     else DATE_FORMAT(PayDate, '%Y-%m-%d') end as PayDate,

-- AnlCoupRate,
case when uscode = '984121CQ4' then '3.625' else AnlCoupRate end as AnlCoupRate,  


case when MaturityDate='0000-00-00'
     then null
     else DATE_FORMAT(MaturityDate, '%Y-%m-%d') end as MaturityDate,
MatPriceAsPercent,
FRNIndexBenchmark,
Markup,
MaturityStructure,
Callable,
Puttable,
CallPut,
CPType,
CpoptID,
FromDate,
ToDate,
CpCurrency,
CpPrice,
CPPriceAsPercent,
IssuePrice,
case when IssueDate='0000-00-00'
     then null
     else DATE_FORMAT(IssueDate, '%Y-%m-%d') end as IssueDate,
Perpetual,
case when SettlementDate='0000-00-00'
     then null
     else DATE_FORMAT(SettlementDate, '%Y-%m-%d') end as SettlementDate,
ExchgCd,
LocalCode,

case when MktCloseDate='0000-00-00'
     then null
     else DATE_FORMAT(MktCloseDate, '%Y-%m-%d') end as MktCloseDate,
     
case when LastTradeDate='0000-00-00'
     then null
     else DATE_FORMAT(LastTradeDate, '%Y-%m-%d') end as LastTradeDate,     
ClosingPrice,
Open,
High,
Low,
Mid,
Ask,
Bid,
BidSize,
AskSize,
TradedVolume,
VolFlag,
PriceCurrency,
AccruedInterest,
YieldtoMaturity,
YieldtoCall,
YieldtoPut,
ModifiedDuration,
EffectiveDuration,
MacaulayDuration,
Convexity,
Yield,
YieldToWorst,
KeyRateDuration,
BondType,
Subordinate,
MinimumDenomination,
DenominationMultiple,
IssID,
IndusID,
Shortname,
Legalname,
CntryofDom,
StateofDom,
PrimaryExchgCD,
SIC,
NAICS,
GICS,
IssueAmount,

case when FirstCouponDate='0000-00-00'
     then null
     else DATE_FORMAT(FirstCouponDate, '%Y-%m-%d') end as FirstCouponDate, 
     
     case when LastCouponDate='0000-00-00'
     then null
     else DATE_FORMAT(LastCouponDate, '%Y-%m-%d') end as LastCouponDate, 
     
     case when IntCommencementDate='0000-00-00'
     then null
     else DATE_FORMAT(IntCommencementDate, '%Y-%m-%d') end as IntCommencementDate
from bondsigma.bondcliq_temp;
