--filepath=O:\Prodman\Dev\WFI\Feeds\BondCliq\Output\Full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CPOPN
--fileheadertext=EDI_CPOPN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


SELECT distinct
upper('CPOPN') as TableName,
wca.cpopn.Actflag,
wca.cpopn.AnnounceDate as Created,
wca.cpopn.Acttime as Changed,
wca.cpopn.CpopnID,
wca.cpopn.SecID,
wca.cpopn.CallPut, 
wca.cpopn.Notes as Notes 
from wca.cpopn
inner join bondsigma.bondcliq_temp on wca.cpopn.secid = bondsigma.bondcliq_temp.secid
where
wca.cpopn.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
-- wca.cpopn.acttime = '2017-07-10 09:03:58'

