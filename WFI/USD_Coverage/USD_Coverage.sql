--filepath=O:\Prodman\Dev\WFI\Feeds\USD_Coverage\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_USD_Coverage
--fileheadertext=EDI_USD_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
select distinct


wca.bond.Actflag as Bond_Actflag,
wca.bond.announcedate as Bond_Created,     
wca.bond.acttime as Bond_Changed,
   

wca.bond.SecID,
wca.scmst.ISIN,
wca.scmst.USCode,
wca.issur.IssID,
wca.scexh.LocalCode,
wca.issur.IssuerName,
wca.issur.CntryOfIncorp,
wca.bond.IssueDate,
wca.bond.IssuePrice,
wca.bond.MaturityDate,
wca.bond.MatPrice,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestBasis,
wca.bond.CurenCD,
wca.bond.LastCouponDate,
wca.bond.BondType,
wca.bond.ParValue,
wca.bond.Callable,
wca.bond.Puttable,
wca.bond.PerformanceBenchmarkISIN,
wca.scmst.SecurityDesc,
wca.issur.IssType,
wca.scexh.ExchgCD,
wca.bond.DebtMarket,
wca.bond.BondSrc,
wca.bond.IssueAmount,
wca.bond.OutstandingAmount,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,
wca.bond.FRNType,
wca.bond.FRNIndexBenchmark,
wca.bond.markup as FRNMargin,
wca.bond.minimuminterestrate as FRNMinInterestRate,
wca.bond.maximuminterestrate as FRNMaxInterestRate,
wca.bond.Subordinate,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.DenominationMultiple,
wca.bond.strip,
wca.scmst.ParentSecID,
wca.bond.MaturityBenchmark,

-- CPOPT
upper('CPOPT') as TableName,
wca.cpopt.Actflag as Cpopt_Actflag,
wca.cpopt.Acttime as Cpopt_Changed,

wca.cpopt.CpoptID,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.cptype,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice,

-- IRCHG
upper('IRCHG') as TableName,
wca.irchg.Actflag as Irchg_Actflag,
wca.irchg.Acttime as Irchg_Changed,

wca.irchg.IrchgID,
wca.irchg.EffectiveDate as irchg_EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.Eventtype irchg_Eventtype,
wca.irchg.Notes, 

-- INTBC
upper('INTBC') as TableName,
wca.intbc.Actflag as Intbc_Actflag,
wca.intbc.Acttime as Intbc_Changed,

wca.intbc.IntbcID,
wca.intbc.EffectiveDate,
wca.intbc.OldIntBasis,
wca.intbc.NewIntBasis,
wca.intbc.OldIntBDC,
wca.intbc.NewIntBDC,

-- IFCHG
upper('IFCHG') as TableName,
wca.ifchg.Actflag as Ifchg_Actflag,
wca.ifchg.Acttime as Ifchg_Changed,

wca.ifchg.IfchgID,
wca.ifchg.NotificationDate,
wca.ifchg.OldIntPayFrqncy,
wca.ifchg.OldIntPayDate1,
wca.ifchg.OldIntPayDate2,
wca.ifchg.OldIntPayDate3,
wca.ifchg.OldIntPayDate4,
wca.ifchg.NewIntPayFrqncy,
wca.ifchg.NewIntPayDate1,
wca.ifchg.NewIntPayDate2,
wca.ifchg.NewIntPayDate3,
wca.ifchg.NewIntPayDate4,
wca.ifchg.Eventtype as ifchg_Eventtype,

issur.IndusID,
issur.SIC,
issur.NAICS,
issur.GICS,

#prices.lasttrade.MktCloseDate,
#prices.lasttrade.PriceDate,
#prices.lasttrade.Currency as PriceCurrency,
#prices.lasttrade.Close as ClosingPrice,
#prices.lasttrade.Open,
#prices.lasttrade.High,
#prices.lasttrade.Low,
#prices.lasttrade.Mid,
#prices.lasttrade.Ask,
#prices.lasttrade.Bid,
#prices.lasttrade.BidSize,
#prices.lasttrade.AskSize,
#prices.lasttrade.TradedVolume,



case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.scexh on wca.scmst.secid = wca.scexh.secid

left outer join wca.cpopt on wca.scmst.secid = wca.cpopt.secid
left outer JOIN wca.irchg on wca.scmst.secid = wca.irchg.secid
left outer join wca.intbc on wca.scmst.secid = wca.intbc.secid
LEFT OUTER JOIN wca.ifchg on wca.scmst.secid = wca.ifchg.secid

#left outer join prices.lasttrade on scexh.secid = prices.lasttrade.secid and scexh.exchgcd = prices.lasttrade.exchgcd

where
wca.bond.CurenCD = 'USD'
-- wca.scexh.ExchgCD like 'CA%'
-- wca.scmst.uscode in (select code from client.pfuscode where accid = 990)
-- or wca.scmst.uscode in (select code from client.pfuscode where accid = 990))
and wca.issur.actflag <> 'D'
and wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.StatusFlag <> 'I'
and (wca.scexh.actflag <> 'D' or wca.scexh.actflag is null)
and (wca.cpopt.actflag <> 'D' or wca.cpopt.actflag is null)

-- and (wca.irchg.actflag <> 'D' or wca.irchg.actflag is null)

and (wca.intbc.actflag <> 'D' or wca.intbc.actflag is null)
and (wca.ifchg.actflag <> 'D' or wca.ifchg.actflag is null)

-- order by wca.scmst.isin desc;