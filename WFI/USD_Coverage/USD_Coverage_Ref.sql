--filepath=O:\Datafeed\Debt\CAD_USD_Coverage\USD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_USD_Coverage_Ref
--fileheadertext=EDI_USD_Coverage_Ref_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
select distinct
wca.scmst.USCode,
wca.scmst.ISIN,
wca.bond.SecID,
wca.issur.IssuerName,
wca.bond.MaturityDate,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestBasis,
wca.bond.BondType,
wca.bond.InterestAccrualConvention,
wca.bond.IssueDate,

wca.bond.FRNIndexBenchmark,
wca.bond.markup as FRNMargin,
wca.irchg.Actflag as Irchg_Actflag,
wca.irchg.Acttime as Irchg_Changed,
wca.irchg.IrchgID,
wca.irchg.EffectiveDate as irchg_EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.Eventtype irchg_Eventtype,




prices.lasttrade.MktCloseDate,
prices.lasttrade.PriceDate,
prices.lasttrade.Currency as PriceCurrency,
prices.lasttrade.Close as ClosingPrice,
prices.lasttrade.Open,
prices.lasttrade.High,
prices.lasttrade.Low,
prices.lasttrade.Mid,
prices.lasttrade.Ask,
prices.lasttrade.Bid,
prices.lasttrade.BidSize,
prices.lasttrade.AskSize,
prices.lasttrade.TradedVolume,

wca.bond.SecuredBy,
wca.bond.SecurityCharge

from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join prices.lasttrade on scmst.secid = prices.lasttrade.secid
left outer JOIN wca.irchg on wca.scmst.secid = wca.irchg.secid
where
wca.bond.CurenCD = 'USD'
and wca.issur.actflag <> 'D'
and wca.bond.actflag <> 'D'
and wca.scmst.actflag <> 'D'
and wca.scmst.StatusFlag <> 'I'
order by wca.scmst.isin desc;