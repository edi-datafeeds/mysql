--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('SDCHG') as TableName,
sdchg.Actflag,
sdchg.AnnounceDate as Created, 
sdchg.Acttime as Changed,
sdchg.SdChgID,
sdchg.SecID,
scmst.ISIN,
sdchg.CntryCD as OldCntryCD,
sdchg.EffectiveDate,
sdchg.OldSedol,
sdchg.NewSedol,
sdchg.EventType,
sdchg.RcntryCD as OldRcntryCD,
sdchg.RelEventID,
sdchg.NewCntryCD,
sdchg.NewRcntryCD,
sdchg.sdchgNotes As Notes
from wca.sdchg
inner join wca.bond on sdchg.secid = bond.secid
inner join wca.scmst on bond.secid = scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and sdchg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and sdchg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))
