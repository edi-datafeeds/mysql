--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('ISCHG') as TableName,
ischg.Actflag,
ischg.AnnounceDate as Created,
ischg.Acttime as Changed,
ischg.IschgID,
bond.SecID,
scmst.ISIN,
ischg.IssID,
ischg.NameChangeDate,
ischg.IssOldName,
ischg.IssNewName,
ischg.EventType,
ischg.LegalName,
ischg.MeetingDateFlag, 
ischg.IsChgNotes as Notes
from wca.ischg
inner join wca.scmst on ischg.issid = scmst.issid
inner join wca.bond on scmst.secid = bond.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and ischg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and ischg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )