--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
'INTPY' as TableName,
CASE WHEN int_my.Actflag = 'D' or int_my.actflag='C' or intpy.actflag is null THEN int_my.Actflag ELSE intpy.Actflag END as Actflag,
int_my.AnnounceDate as Created,
CASE WHEN (rd.Acttime is not null) and (rd.Acttime > int_my.Acttime) and (rd.Acttime > exdt.Acttime) THEN rd.Acttime WHEN (exdt.Acttime is not null) and (exdt.Acttime > int_my.Acttime) THEN exdt.Acttime ELSE int_my.Acttime END as Changed,
scmst.SecID,
scmst.ISIN,
intpy.BCurenCD as DebtCurrency,
intpy.BParValue as NominalValue,
scexh.ExchgCD,
int_my.RdID,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
rd.Recdate,
exdt.Exdate,
exdt.Paydate,
int_my.InterestFromDate,
int_my.InterestToDate,
int_my.Days,
intpy.CurenCD as Interest_Currency,
intpy.IntRate as Interest_Rate,
intpy.GrossInterest,
intpy.NetInterest,
intpy.DomesticTaxRate,
intpy.NonResidentTaxRate,
intpy.RescindInterest,
intpy.AgencyFees,
intpy.CouponNo,
-- null as CouponID,
intpy.DefaultOpt,
intpy.OptElectionDate,
CASE WHEN isin in (select code from portfolio.factset_isin) THEN ''
when int_my.NilInt = 'T' then '0' else intpy.AnlCoupRate end as AnlCoupRate,
int_my.InDefPay,
int_my.NilInt,
int_my.InterestGraceDays,
int_my.InterestGraceDayConvention
from wca.int_my
inner join wca.rd on int_my.rdid = rd.rdid
inner join wca.bond on rd.secid = bond.secid
inner join wca.scmst on bond.secid = scmst.secid
inner join wca.issur on scmst.issid = issur.issid
left outer join wca.scexh on bond.secid = scexh.secid
left outer join wca.exdt on rd.rdid = exdt.rdid 
     and wca.scexh.exchgcd = exdt.exchgcd and 'int' = exdt.eventtype
left outer join wca.intpy on int_my.rdid = intpy.rdid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and (INTPY.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or int_my.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or RD.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or EXDT.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))

or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and (INTPY.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or int_my.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or RD.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or EXDT.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)));
