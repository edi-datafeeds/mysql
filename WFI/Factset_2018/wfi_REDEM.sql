--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select
'REDEM' as TableName,
wca.redem.Actflag,
wca.redem.AnnounceDate as Created,
CASE WHEN (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) THEN wca.rd.Acttime ELSE wca.redem.Acttime END as Changed,
wca.redem.RedemID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.rd.Recdate,
wca.redem.RedemDate,
wca.redem.CurenCD as RedemCurrency,
-- redem.RedemPrice,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPrice end as RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
-- redem.AmountRedeemed,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.AmountRedeemed end as AmountRedeemed,
-- redem.RedemPremium,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPremium end as RedemPremium,
-- redem.RedemPercent,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPercent end as RedemPercent,
-- redem.PoolFactor,
case when wca.redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' WHEN INSTR('.',wca.redem.poolfactor) < 5 THEN substring(wca.redem.poolfactor,1,INSTR('.',wca.redem.poolfactor)+9)
     ELSE substring(wca.redem.poolfactor,1,INSTR('.',wca.redem.poolfactor)+8)    
END AS poolfactor,
-- redem.PriceAsPercent,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null) or redem.PremiumAsPercent = '' and (redem.PriceAsPercent = '100.000000000' or redem.PriceAsPercent = '100') and redem.RedemType <> 'MAT' then '' else redem.PriceAsPercent end as PriceAsPercent,
-- redem.PremiumAsPercent,
case when redem.secid in (select secid from wca.redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null) then '' else redem.PremiumAsPercent end as PremiumAsPercent,
wca.redem.InDefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.ExpTime,
wca.redem.ExpTimeZone,
wca.redem.MatGraceDays,
wca.redem.MatGraceDayConvention,
wca.redem.TenderOfferor,
wca.redem.DutchAuction,
wca.redem.DutchMinPrice,
wca.redem.DutchMinAmount,
wca.redem.DutchMaxPrice,
wca.redem.DutchMaxAmount,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and redem.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and redem.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));