--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('ICC') as TableName,
icc.Actflag,
icc.AnnounceDate as Created,
icc.Acttime as Changed,
icc.IccID,
icc.SecID,
scmst.ISIN,
icc.EffectiveDate,
icc.OldISIN,
icc.NewISIN,
icc.OldUSCode,
icc.NewUSCode,
icc.OldVALOREN,
icc.NewVALOREN,
icc.EventType,
icc.RelEventID,
icc.OldCommonCode,
icc.NewCommonCode
from wca.icc
-- inner join bond on icc.secid = bond.secid
inner join wca.scmst on icc.secid = scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and icc.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and icc.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));

