--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_DIV
--fileheadertext=EDI_DIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('DIV') as Tablename,
wca.div_my.ActFlag,
wca.div_my.AnnounceDate as Created,
wca.div_my.Acttime as Changed,
wca.div_my.RdID,
wca.div_my.DivPeriodCD,
wca.div_my.TbaFlag,
wca.div_my.DivNotes,
wca.div_my.NilDividend,
wca.div_my.Coupon,
wca.div_my.FYEDate,
wca.div_my.DivID,
wca.div_my.DivRescind,
wca.div_my.PeriodEndDate,
wca.div_my.Frequency,
wca.div_my.Marker,
wca.div_my.DeclarationDate,
wca.div_my.DeclCurenCD,
wca.div_my.DeclGrossAmt
from wca.div_my
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and div_my.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and div_my.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));