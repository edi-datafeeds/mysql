--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LAWSTNOTES') as TableName,
wca.lawst.Actflag,
wca.lawst.LawstID,
wca.lawst.IssId,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
((wca.lawst.issid in (select issid from client.pfisin where accid = 212 and actflag = 'I')
OR wca.lawst.issid in (select issid from client.pfsecid where accid = 212 and actflag = 'I')))
or
((wca.lawst.issid in (select issid from client.pfisin where accid = 212 and actflag = 'U')
or wca.lawst.issid in (select issid from client.pfsecid where accid = 212 and actflag = 'U'))
and wca.lawst.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3))
