--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT distinct
upper('BOND') as Tablename,
bond.Actflag,
bond.AnnounceDate as Created,
bond.Acttime as Changed,
bond.SecID,
scmst.ISIN,
bond.BondType,
bond.DebtMarket,
bond.CurenCD as DebtCurrency,
case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue else wca.bond.parvalue end as NominalValue,

bond.IssueDate,
bond.IssueCurrency,
bond.IssuePrice,
bond.IssueAmount,
bond.IssueAmountDate,
bond.OutstandingAmount,
bond.OutstandingAmountDate,
bond.InterestBasis,
CASE WHEN isin in (select code from portfolio.factset_isin) THEN '' 
	 WHEN ifnull(int_my.NilInt,'F') = 'T' then '0' else bond.interestRate end as interestRate,
bond.InterestAccrualConvention,
bond.InterestPaymentFrequency,
bond.IntCommencementDate,
bond.OddFirstCoupon,
bond.FirstCouponDate,
bond.OddLastCoupon,
bond.LastCouponDate,
bond.InterestPayDate1,
bond.InterestPayDate2,
bond.InterestPayDate3,
bond.InterestPayDate4,
bondx.DomesticTaxRate,
bondx.NonResidentTaxRate,
bond.FRNType,
bond.FRNIndexBenchmark,
bond.Markup as FrnMargin,
bond.MinimumInterestRate as FrnMinInterestRate,
bond.MaximumInterestRate as FrnMaxInterestRate,
bond.Rounding,
bondx.Series,
bondx.Class,
bondx.OnTap,
bondx.MaximumTapAmount,
bondx.TapExpiryDate,
bond.Guaranteed,
bond.SecuredBy,
bond.SecurityCharge,
bond.Subordinate,
bond.SeniorJunior,
bond.WarrantAttached,
bond.MaturityStructure,
bond.Perpetual,
bond.MaturityDate,
bond.MaturityExtendible,
bond.Callable,
bond.Puttable,
bondx.Denomination1,
bondx.Denomination2,
bondx.Denomination3,
bondx.Denomination4,
bondx.Denomination5,
bondx.Denomination6,
bondx.Denomination7,
bondx.MinimumDenomination,
bondx.DenominationMultiple,
bond.Strip,
bond.StripInterestNumber, 
bond.Bondsrc,
bond.MaturityBenchmark,
bond.ConventionMethod,
bond.FrnIntAdjFreq as FrnInterestAdjFreq,
bond.IntBusDayConv as InterestBusDayConv,
bond.InterestCurrency,
bond.MatBusDayConv as MaturityBusDayConv,
bond.MaturityCurrency, 
bondx.TaxRules,
bond.VarIntPayDate as VarInterestPaydate,
bond.PriceAsPercent,
bond.PayOutMode,
bond.Cumulative,
case when bond.matprice<>'' then cast(bond.matprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(bond.MatPriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(bond.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
bond.MatPriceAsPercent,
bond.SinkingFund,
bondx.GovCity,
bondx.GovState,
bondx.GovCntry,
bondx.GovLawLkup as GovLaw,
bondx.GoverningLaw as GovLawNotes,
bond.Municipal,
bond.PrivatePlacement,
bond.Syndicated,
bond.Tier,
bond.UppLow,
bond.Collateral,
bond.CoverPool,
bond.PikPay,
bond.YieldAtIssue,
bond.CoCoTrigger,
bond.CoCoAct,
bond.NonViability,
bondx.Taxability,
bond.LatestAppliedINTPYAnlCpnRateDate as LatestApplicablePayDate,
bond.FirstAnnouncementDate,
bond.PerformanceBenchmarkISIN,
bondx.InterestGraceDays,
bondx.InterestGraceDayConvention,
bondx.MatGraceDays,
bondx.MatGraceDayConvention,
bond.Notes
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
left outer join wca.rd on wca.bond.secid = wca.rd.secid
left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))

AND (wca.rd.rdid is null or wca.rd.rdid =
(select subrd.rdid from wca.rd as subrd
left outer join wca.int_my as subint on subrd.rdid=subint.rdid
where subrd.secid=rd.secid
order by subint.rdid desc limit 1));


