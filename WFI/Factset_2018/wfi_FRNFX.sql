--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
frnfx.Actflag,
frnfx.AnnounceDate as Created,
frnfx.Acttime as Changed,
frnfx.FrnfxID,
frnfx.SecID,
scmst.ISIN,
frnfx.EffectiveDate,
frnfx.OldFRNType,
frnfx.OldFRNIndexBenchmark,
frnfx.OldMarkup As OldFrnMargin,
frnfx.OldMinimumInterestRate,
frnfx.OldMaximumInterestRate,
frnfx.OldRounding,
frnfx.NewFRNType,
frnfx.NewFRNindexBenchmark,
frnfx.NewMarkup As NewFrnMargin,
frnfx.NewMinimumInterestRate,
frnfx.NewMaximumInterestRate,
frnfx.NewRounding,
frnfx.Eventtype,
frnfx.OldFrnIntAdjFreq,
frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on frnfx.secid = bond.secid
inner join wca.scmst on bond.secid = scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and frnfx.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and frnfx.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));