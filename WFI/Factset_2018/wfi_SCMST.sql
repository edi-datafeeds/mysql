--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT
upper('SCMST') as TableName,
scmst.Actflag,
scmst.AnnounceDate as Created,
scmst.Acttime as Changed,
scmst.SecID,
scmst.ISIN,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.StatusFlag = '' then 'A' else scmst.StatusFlag end as StatusFlag,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.USCode,
scmst.X as CommonCode,
scmst.Holding,
scmst.StructCD,
scmst.WKN,
scmst.RegS144A,
scmst.CFI,
scmst.CIC,
scmst.scmstNotes As Notes,

-- # New Fields Below

scmst.StatusReason,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.FYENPPDate,
scmst.VALOREN,
scmst.SharesOutstanding,
scmst.SharesOutstandingDate,
scmst.UmprgID,
scmst.NoParValue,
scmst.ExpDate,
scmst.VotePerSec,
scmst.FreeFloat,
scmst.FreeFloatDate,
scmst.TreasuryShares,
scmst.TreasurySharesDate
from wca.scmst
INNER JOIN wca.bond ON scmst.SecID = bond.SecID
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and scmst.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and scmst.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))
