--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('ISSUR') as TableName,
issur.Actflag,
issur.AnnounceDate as Created,
issur.Acttime as Changed,
issur.IssID,
issur.SIC,
issur.CIK,
issur.GICS,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp,
issur.FinancialYearEnd,
issur.Shortname,
issur.LegalName,
-- case when issur.cntryofincorp='AA' then 'S' when issur.isstype='GOV' then 'G' when issur.isstype='GOVAGENCY' then 'Y' else 'C' end as Isstype,
issur.isstype,
issur.CntryofDom,
issur.StateofDom,
issur.LEI,
issur.NAICS,
issur.issurnotes AS Notes
from wca.issur
inner join wca.scmst on issur.issid = scmst.issid
inner join wca.bond on scmst.secid = bond.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and issur.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and issur.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))