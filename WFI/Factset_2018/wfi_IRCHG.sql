--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('IRCHG') as TableName,
irchg.Actflag,
irchg.AnnounceDate as Created,
irchg.Acttime as Changed,
irchg.IrchgID,
irchg.SecID,
scmst.ISIN,
irchg.EffectiveDate,
-- irchg.OldInterestRate,
CASE WHEN isin in (select code from portfolio.factset_isin) THEN '' else irchg.OldInterestRate end as OldInterestRate, 
-- irchg.NewInterestRate,
CASE WHEN isin in (select code from portfolio.factset_isin) THEN '' else irchg.NewInterestRate end as NewInterestRate, 
irchg.Eventtype,
irchg.Notes 
from wca.irchg
inner join wca.bond on irchg.secid = bond.secid
inner join wca.scmst on bond.secid = scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and wca.irchg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and wca.irchg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));