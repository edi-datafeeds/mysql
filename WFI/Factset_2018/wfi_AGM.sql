--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('AGM') as TableName,
agm.Actflag,
agm.AnnounceDate as Created,
agm.Acttime as Changed, 
agm.AGMID,
bond.SecID,
scmst.ISIN,
agm.IssID,
agm.AGMDate,
agm.AGMEGM,
agm.AGMNo,
agm.FYEDate,
agm.AGMTime,
agm.Add1,
agm.Add2,
agm.Add3,
agm.Add4,
agm.Add5,
agm.Add6,
agm.City,
agm.CntryCD,
agm.BondSecID
FROM wca.agm
INNER JOIN wca.scmst ON agm.BondSecID = scmst.SecID
INNER JOIN wca.bond ON agm.BondSecID = bond.SecID
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and agm.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and agm.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));