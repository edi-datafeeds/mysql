--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('BKRPNOTES') as TableName,
wca.bkrp.Actflag,
wca.bkrp.BkrpID,
wca.bkrp.IssId,
wca.bkrp.BkrpNotes as Notes
from wca.bkrp
where
((wca.bkrp.issid in (select issid from client.pfisin where accid = 212 and actflag = 'I')
OR wca.bkrp.issid in (select issid from client.pfsecid where accid = 212 and actflag = 'I')))
or
((wca.bkrp.issid in (select issid from client.pfisin where accid = 212 and actflag = 'U')
or wca.bkrp.issid in (select issid from client.pfsecid where accid = 212 and actflag = 'U'))
and wca.bkrp.acttime>(select max(feeddate) from wca.tbl_opslog where seq=3))
