--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SACHG') as TableName,
wca.sachg.Actflag,
wca.sachg.AnnounceDate as Created,
wca.sachg.Acttime as Changed,
wca.sachg.SachgID,
wca.sachg.ScagyID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.sachg.EffectiveDate,
wca.sachg.OldRelationship,
wca.sachg.NewRelationship,
wca.sachg.OldAgncyID,
wca.sachg.NewAgncyID,
wca.sachg.OldSpStartDate,
wca.sachg.NewSpStartDate,
wca.sachg.OldSpEndDate,
wca.sachg.NewSpEndDate,
wca.sachg.OldGuaranteeType,
wca.sachg.NewGuaranteeType
from wca.sachg
inner join wca.scagy on wca.sachg.scagyid = wca.scagy.scagyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and sachg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and sachg.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));