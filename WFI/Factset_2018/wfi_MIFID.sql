--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_MIFID
--fileheadertext=EDI_MIFID_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MIFID') as TableName,
wca.mifid.Actflag,
wca.mifid.AnnounceDate as Created,
wca.mifid.Acttime as Changed,
wca.scmst.ISIN,
wca.mifid.SecID,
wca.mifid.StartDate,
wca.mifid.CompAuth,
wca.mifid.CntryCD,
wca.mifid.EndDate,
wca.mifid.MiFIDID
from wca.mifid
inner join wca.bond on wca.mifid.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and mifid.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and mifid.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3));