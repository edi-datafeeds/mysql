--filepath=O:\Upload\Acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Debt\Factset\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('WKNCH') as TableName,
wknch.Actflag,
wknch.AnnounceDate as Created,
wknch.Acttime as Changed,
wknch.SecID,
wknch.EffectiveDate,
wknch.WknChID,
wknch.Eventtype,
wknch.RelEventID,
wknch.OldWKN,
wknch.NewWKN
from wca.wknch
inner join wca.bond on wknch.secid = bond.secid
inner join wca.scmst on bond.secid = scmst.secid
where
isin in (select code from client.pfisin where accid=212 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=212 and actflag='U')
and wknch.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=212 and actflag='U')
and wknch.acttime > (select max(feeddate) from wca.tbl_Opslog where seq = 3))
