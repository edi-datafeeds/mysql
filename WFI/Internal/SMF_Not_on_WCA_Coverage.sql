--filepath=O:\Datafeed\Debt\Internal\Quarterly\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SMF_Not_on_WCA_Coverage
--fileheadertext=EDI_SMF_Not_on_WCA_Coverage_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\Internal\SMF_Not_on_WCA_Coverage\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select DISTINCT
smf4.security.ISIN,
smf4.security.SEDOL,
smf4.security.OPOL,
smf4.issuer.IssuerName,
smf4.security.SecType,
smf4.sectype.TypeGroup,
smf4.sectype.Name,
smf4.security.LongDesc,
smf4.security.CloseDate as MaturityDate,
case when smf4.security.Statusflag = 'C' THEN 'Coded'
	 when smf4.security.Statusflag = 'D' THEN 'Defunct'
     when smf4.security.Statusflag = 'T' THEN 'Trading'
END AS Statusflag
from smf4.security
left outer join smf4.issuer on smf4.security.issuerid = smf4.issuer.issuerid
left outer join smf4.sectype on smf4.security.SecType = smf4.sectype.code
where
smf4.sectype.TypeGroup = 'Debt'
and smf4.security.ISIN not in(select isin from wca.scmst)
and smf4.security.ISIN not in(select isin from pipelinemaster.ignored)
and smf4.security.ISIN not in(select isin from pipelinemaster.defunct)
AND smf4.security.Statusflag <> 'D';