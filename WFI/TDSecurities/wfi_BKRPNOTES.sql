--filepath=O:\Upload\Acc\293\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('BKRPNOTES') as TableName,
wca.bkrp.Actflag,
wca.bkrp.BkrpID,
wca.bkrp.IssId,
wca.bkrp.BkrpNotes as Notes
from wca.bkrp
where
wca.bkrp.issid in (select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where client.pfisin.accid=293
and (wca.bkrp.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfisin.actflag='I'))

or wca.bkrp.issid in (select wca.scmst.issid from client.pfuscode inner join wca.scmst on client.pfuscode.code = wca.scmst.uscode where client.pfuscode.accid=293
and (wca.bkrp.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfuscode.actflag='I'))