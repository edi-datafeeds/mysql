--filepath=O:\Upload\Acc\293\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT distinct
upper('BBE') as tablename,
wca.bbe.ActFlag,
wca.bbe.AnnounceDate as Created,
wca.bbe.Acttime as Changed,
wca.bbe.BbeId,
wca.bbe.SecId,
wca.bbe.Exchgcd,
wca.bbe.Curencd,
wca.bbe.BbgexhId,
wca.bbe.Bbgexhtk
from wca.bbe
inner join wca.scmst on wca.bbe.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=293 and actflag='I')
or uscode in (select code from client.pfuscode where accid=293 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=293 and actflag='U')
and bbe.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or uscode in (select code from client.pfuscode where accid=293 and actflag='U')
and bbe.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))