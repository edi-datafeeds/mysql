--filepath=O:\Upload\Acc\293\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SXTCH
--fileheadertext=EDI_SXTCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SXTCH') as TableName,
wca.sxtch.Actflag,
wca.sxtch.AnnounceDate as Created,
wca.sxtch.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.sxtch.ScxtcID,
wca.sxtch.EffectiveDate,
wca.sxtch.OldCurenCD,
wca.sxtch.NewCurenCD,
wca.sxtch.OldLocalCode,
wca.sxtch.NewLocalCode,
wca.sxtch.OldPriceTick,
wca.sxtch.NewPriceTick,
wca.sxtch.OldTickSize,
wca.sxtch.NewTickSize,
wca.sxtch.OldISIN,
wca.sxtch.NewISIN,
wca.sxtch.SxtChID
from wca.sxtch
inner join wca.scxtc on wca.sxtch.ScxtcID = wca.scxtc.ScxtcID
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(wca.scmst.isin in (select code from client.pfisin where accid=293 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=293 and actflag='I'))
or 
(wca.scmst.isin in (select code from client.pfisin where accid=293 and actflag='U')
and wca.sxtch.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=293 and actflag='U')
and wca.sxtch.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))