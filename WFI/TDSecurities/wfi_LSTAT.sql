--filepath=O:\Upload\Acc\293\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\TD_Securities\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LSTAT') as TableName,
wca.lstat.Actflag,
wca.lstat.AnnounceDate as Created,
wca.lstat.Acttime as Changed,
wca.lstat.LstatID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.lstat.ExchgCD,
wca.lstat.NotificationDate,
wca.lstat.EffectiveDate,
wca.lstat.LStatStatus,
wca.lstat.EventType,
wca.lstat.Reason
from wca.lstat
inner join wca.bond on wca.lstat.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=293 and actflag='I')
or uscode in (select code from client.pfuscode where accid=293 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=293 and actflag='U')
and lstat.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or uscode in (select code from client.pfuscode where accid=293 and actflag='U')
and lstat.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))