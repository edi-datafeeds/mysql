--filepath=O:\Datafeed\WFI\Full_Licence\V78i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MUBND
--fileheadertext=EDI_MUBND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUBND') as TableName,
wca.mubnd.Actflag,
wca.mubnd.AnnounceDate as Created,
wca.mubnd.Acttime as Changed,
wca.mubnd.MuBndID,
wca.mubnd.SecID,
wca.mubnd.MuIssID,
wca.mubnd.TaxCreditBasis,
wca.mubnd.TaxCreditRate,
wca.mubnd.FirstTaxCreditDate,
wca.mubnd.MuniMaturityStyle,
wca.mubnd.Notes
from wca.mubnd
inner join wca.bond on wca.mubnd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.mubnd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)