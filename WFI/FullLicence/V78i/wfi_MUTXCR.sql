--filepath=O:\Datafeed\WFI\Full_Licence\V78i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MUTXCR
--fileheadertext=EDI_MUTXCR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUTXCR') as TableName,
wca.mutxcr.Actflag,
wca.mutxcr.AnnounceDate as Created,
wca.mutxcr.Acttime as Changed,
wca.mutxcr.MuTxCrID,
wca.mutxcr.MuBndID,
wca.mutxcr.TaxCreditDate,
wca.mutxcr.TaxCreditAmount
from wca.mutxcr
inner join wca.mubnd on wca.mutxcr.MuBndID = wca.mubnd.MuBndID
inner join wca.scmst on wca.mubnd.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.mutxcr.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)