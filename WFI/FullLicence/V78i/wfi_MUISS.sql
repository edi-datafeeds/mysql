--filepath=O:\Datafeed\WFI\Full_Licence\V78i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MUISS
--fileheadertext=EDI_MUISS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('MUISS') as TableName,
wca.muiss.Actflag,
wca.muiss.AnnounceDate as Created,
wca.muiss.Acttime as Changed,
wca.muiss.IssID,
wca.muiss.MuIssID,
wca.muiss.MuniIssuanceName,
wca.muiss.MuniSeries,
wca.muiss.RefundingFlag,
wca.muiss.MuniSeriesIssueAmount,
wca.muiss.MuniCategory,
wca.muiss.RepaymentSource,
wca.muiss.TaxStatus,
wca.muiss.TaxExemptAmount,
wca.muiss.TaxExemptBankQualified,
wca.muiss.Notes
from wca.muiss
inner join wca.scmst on wca.muiss.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.muiss.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)