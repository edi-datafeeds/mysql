--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_GICCH
--fileheadertext=EDI_GICCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('GICCH') as TableName,
wca.gicch.Actflag,
wca.gicch.AnnounceDate as Created,
wca.gicch.Acttime as Changed,
wca.gicch.IssID,
wca.gicch.EffectiveDate,
wca.gicch.OldGICS,
wca.gicch.NewGICS,
wca.gicch.GICSChID
from wca.gicch
inner join wca.scmst on wca.gicch.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.gicch.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
