--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SPCND
--fileheadertext=EDI_SPCND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPCND') as TableName,
spcnd.Actflag,
spcnd.AnnounceDate as Created,
spcnd.Acttime as Changed,
scmst.SecID,
wca.scmst.ISIN,
spcnd.SpcndID,
spcnd.Markup,
spcnd.UnderlyingCondition,
spcnd.WithReferenceTo,
spcnd.Level1Currency,
spcnd.Level1Percent,
spcnd.Level1Value,
spcnd.Level2Currency,
spcnd.Level2Percent,
spcnd.Level2Value
from wca.spcnd
inner join wca.spun on wca.spcnd.SpunID = wca.spun.SpunID
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spcnd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
