--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_NAICH
--fileheadertext=EDI_NAICH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('NAICH') as TableName,
wca.naich.Actflag,
wca.naich.AnnounceDate as Created,
wca.naich.Acttime as Changed,
wca.naich.IssID,
wca.naich.EffectiveDate,
wca.naich.OldNAICS,
wca.naich.NewNAICS,
wca.naich.NAICSChID
from wca.naich
inner join wca.scmst on wca.naich.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.naich.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
