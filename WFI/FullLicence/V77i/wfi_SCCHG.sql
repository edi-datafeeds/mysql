--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCCHG') as TableName,
wca.scchg.Actflag,
wca.scchg.AnnounceDate as Created,
wca.scchg.Acttime as Changed,
wca.scchg.ScChgID,
wca.scchg.SecID,
wca.scmst.ISIN,
wca.scchg.DateofChange,
wca.scchg.SecOldName,
wca.scchg.SecNewName,
wca.scchg.EventType,
wca.scchg.OldSectyCD,
wca.scchg.NewSectyCD,
wca.scchg.OldRegS144A,
wca.scchg.NewRegS144A,
wca.scchg.ScChgNotes as Notes
from wca.scchg
inner join wca.bond on wca.scchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
