--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SEDOL') as TableName,
wca.sedol.Actflag,
wca.sedol.AnnounceDate as Created,
wca.sedol.Acttime as Changed, 
wca.sedol.SedolId,
wca.sedol.SecID,
wca.scmst.ISIN,
wca.sedol.CntryCD,
wca.sedol.Sedol,
wca.sedol.Defunct,
wca.sedol.RcntryCD 
from wca.sedol
inner join wca.bond on wca.sedol.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sedol.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
