--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_ISSDD
--fileheadertext=EDI_ISSDD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('ISSDD') as TableName,
wca.issdd.Actflag,
wca.issdd.AnnounceDate as Created,
wca.issdd.Acttime as Changed,
wca.issdd.IssID,
wca.issdd.StartDate,
wca.issdd.EndDate,
wca.issdd.IssuerDebtDefaultID,
wca.issdd.Notes
from wca.issdd
inner join wca.scmst on wca.issdd.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issdd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
