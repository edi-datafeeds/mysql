--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MTCHG') as TableName,
wca.zmtchg.Actflag,
wca.zmtchg.AnnounceDate as Created,
wca.zmtchg.Acttime as Changed,
wca.zmtchg.zMtChgID as MtChgID,
wca.zmtchg.SecID,
wca.scmst.ISIN,
wca.zmtchg.NotificationDate,
wca.zmtchg.OldMaturityDate,
wca.zmtchg.NewMaturityDate,
wca.zmtchg.Reason,
wca.zmtchg.EventType,
wca.zmtchg.OldMaturityBenchmarkCntryCD,
wca.zmtchg.OldMaturityBenchmarkCurenCD,
wca.zmtchg.OldMaturityBenchmarkReference,
wca.zmtchg.OldMaturityBenchmarkFrequency,
wca.zmtchg.NewMaturityBenchmarkCntryCD,
wca.zmtchg.NewMaturityBenchmarkCurenCD,
wca.zmtchg.NewMaturityBenchmarkReference,
wca.zmtchg.NewMaturityBenchmarkFrequency,
wca.zmtchg.zMtChgNotes as Notes
from wca.zmtchg
inner join wca.bond on wca.zmtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zmtchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
