--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
upper('BBC') as tablename,
wca.bbc.ActFlag,
wca.bbc.AnnounceDate as Created,
wca.bbc.Acttime as Changed,
wca.bbc.BbcId,
wca.bbc.SecID,
wca.scmst.ISIN,
wca.bbc.Cntrycd,
wca.bbc.Curencd,
wca.bbc.BbgcompId,
wca.bbc.Bbgcomptk
from wca.bbc
inner join wca.scmst on wca.bbc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bbc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
