--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_AUCT
--fileheadertext=EDI_AUCT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('AUCT') as TableName,
wca.auct.Actflag,
wca.auct.AnnounceDate as Created,
wca.auct.Acttime as Changed,
wca.auct.SecID,
wca.scmst.ISIN,
wca.auct.AuctionDate,
wca.auct.AuctionAmount as AmountOffered,
wca.auct.IssueDate,
wca.auct.AmountAccepted,
wca.auct.AuctionID,
wca.auct.AuctionAnnouncementDate,
wca.auct.AuctionStatus,
wca.auct.BidAmount,
wca.auct.Bids,
wca.auct.NonCompetitiveBidAmount,
wca.auct.NonCompetitiveBids,
wca.auct.CompetitiveBidAmount,
wca.auct.CompetitiveBids,
wca.auct.BidsAccepted,
wca.auct.NonCompetitiveAmountAccepted,
wca.auct.NonCompetitiveBidsAccepted,
wca.auct.CompetitiveAmountAccepted,
wca.auct.CompetitiveBidsAccepted,
wca.auct.IssuersQuotaAmount,
wca.auct.CutOffPrice,
wca.auct.CutoffYield,
wca.auct.HighestAcceptedPrice,
wca.auct.LowestAcceptedYield,
wca.auct.AverageAcceptedPrice,
wca.auct.AverageAcceptedYield,
wca.auct.LowestAcceptedPrice,
wca.auct.HighestAcceptedYield,
wca.auct.WeightedAveragePrice,
wca.auct.WeightedAverageYield,
wca.auct.BidToCoverRatio,
wca.auct.YieldTailAtAuction
from wca.auct
inner join wca.scmst on wca.auct.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.auct.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
