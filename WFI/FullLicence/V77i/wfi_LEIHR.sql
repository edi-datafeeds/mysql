--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LEIHR
--fileheadertext=EDI_LEIHR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('LEIHR') as TableName,
leihr.Actflag,
leihr.Announcedate as 'Created',
leihr.Acttime as 'Changed',
leihr.LeihrID,
leihr.BLei,
leihr.DpLei,
leihr.DpStatus,
leihr.UpLei,
leihr.UpStatus,
leihr.IbLei,
leihr.IbStatus
from wca.leihr
inner join wca.lei on wca.leihr.blei = wca.lei.lei
inner join wca.issur on wca.lei.lei = wca.issur.lei
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.leihr.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
