--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('PVRD') as TableName,
wca.pvrd.Actflag,
wca.pvrd.AnnounceDate as Created,
wca.pvrd.Acttime as Changed,
wca.pvrd.PvRdID,
wca.pvrd.SecID,
wca.scmst.ISIN,
wca.pvrd.EffectiveDate,
wca.pvrd.CurenCD,
wca.pvrd.OldParValue,
wca.pvrd.NewParValue,
wca.pvrd.EventType,
wca.pvrd.PvRdNotes as Notes
from wca.pvrd
inner join wca.bond on wca.pvrd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.pvrd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
