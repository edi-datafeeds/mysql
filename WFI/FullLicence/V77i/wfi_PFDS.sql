--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_PFDS
--fileheadertext=EDI_PFDS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('PFDS') as Tablename,
wca.pfds.Actflag,
wca.pfds.AnnounceDate as Created,
wca.pfds.Acttime as Changed,
wca.pfds.SecID,
wca.scmst.ISIN,
wca.pfds.SecuritiesOffered,
wca.pfds.GreenShoeOption,
wca.pfds.LiquidationPreference,
wca.pfds.PfdsID
from wca.pfds
inner join wca.bond on wca.pfds.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.pfds.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
