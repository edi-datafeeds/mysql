--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MKTSG
--fileheadertext=EDI_MKTSG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MKTSG') as TableName,
wca.mktsg.Actflag,
wca.mktsg.AnnounceDate as Created,
wca.mktsg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.mktsg.ExchgCD,
wca.mktsg.MktSegment,
wca.mktsg.MIC,
wca.mktsg.MktsgID
from wca.mktsg
inner join wca.scexh on wca.mktsg.MktsgID = wca.scexh.MktsgID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.mktsg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
