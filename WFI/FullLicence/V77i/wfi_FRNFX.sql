--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('FRNFX') as TableName,
wca.zfrnfx.Actflag,
wca.zfrnfx.AnnounceDate as Created,
wca.zfrnfx.Acttime as Changed,
wca.zfrnfx.zFrnfxID AS FrnfxID,
wca.zfrnfx.SecID,
wca.scmst.ISIN,
wca.zfrnfx.EffectiveDate,
wca.zfrnfx.OldFRNType,
wca.zfrnfx.OldMarkup As OldFrnMargin,
wca.zfrnfx.OldMinimumInterestRate,
wca.zfrnfx.OldMaximumInterestRate,
wca.zfrnfx.OldRounding,
wca.zfrnfx.NewFRNType,
wca.zfrnfx.NewMarkup As NewFrnMargin,
wca.zfrnfx.NewMinimumInterestRate,
wca.zfrnfx.NewMaximumInterestRate,
wca.zfrnfx.NewRounding,
wca.zfrnfx.Eventtype,
wca.zfrnfx.OldFrnIntAdjFreq,
wca.zfrnfx.NewFrnIntAdjFreq,
wca.zfrnfx.OldFRNBenchmarkCntryCD,
wca.zfrnfx.OldFRNBenchmarkCurenCD,
wca.zfrnfx.OldFRNBenchmarkReference,
wca.zfrnfx.OldFRNBenchmarkFrequency,
wca.zfrnfx.NewFRNBenchmarkCntryCD,
wca.zfrnfx.NewFRNBenchmarkCurenCD,
wca.zfrnfx.NewFRNBenchmarkReference,
wca.zfrnfx.NewFRNBenchmarkFrequency
from wca.zfrnfx
inner join wca.bond on wca.zfrnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.zfrnfx.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
