--filepath=O:\Datafeed\WFI\Full_Licence\V77i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate as Created,
wca.bbcc.Acttime as Changed,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.scmst.ISIN,
wca.bbcc.BbcId,
wca.bbcc.Oldcntrycd,
wca.bbcc.Oldcurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.Newcntrycd,
wca.bbcc.Newcurencd,
wca.bbcc.OldbbgcompId,
wca.bbcc.NewbbgcompId,
wca.bbcc.Oldbbgcomptk,
wca.bbcc.Newbbgcomptk,
wca.bbcc.ReleventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bbcc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
