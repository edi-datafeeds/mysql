--filepath=O:\Datafeed\Debt\Markit_GlobalInc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_MIFID
--fileheadertext=EDI_MIFID_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Markit_Global\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MIFID') as TableName,
wca.mifid.Actflag,
wca.mifid.AnnounceDate as Created,
wca.mifid.Acttime as Changed,
wca.scmst.ISIN,
wca.mifid.SecID,
wca.mifid.StartDate,
wca.mifid.CompAuth,
wca.mifid.CntryCD,
wca.mifid.EndDate,
wca.mifid.MiFIDID
from wca.mifid
inner join wca.bond on wca.mifid.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.mifid.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)