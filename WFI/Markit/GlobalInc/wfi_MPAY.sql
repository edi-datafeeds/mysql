--filepath=O:\Datafeed\Debt\Markit_GlobalInc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Markit_Global\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('MPAY') as TableName,
wca.mpay.Actflag,
wca.mpay.AnnounceDate,
wca.mpay.Acttime,
wca.mpay.sEvent as EventType,
wca.mpay.EventID,
wca.mpay.OptionID,
wca.mpay.SerialID,
wca.mpay.SectyCD as ResSectyCD,
wca.mpay.ResSecID,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.MinOfrQty,
wca.mpay.MaxOfrQty,
wca.mpay.MinQlyQty,
wca.mpay.MaxQlyQty,
wca.mpay.Paydate,
wca.mpay.CurenCD,
wca.mpay.MinPrice,
wca.mpay.MaxPrice,
wca.mpay.TndrStrkPrice,
wca.mpay.TndrStrkStep,
wca.mpay.Paytype,
wca.mpay.DutchAuction,
wca.mpay.DefaultOpt,
wca.mpay.OptElectionDate,
wca.mpay.OedExpTime,
wca.mpay.OedExpTimeZone,
wca.mpay.WrtExpTime,
wca.mpay.WrtExpTimeZone
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.mpay.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)