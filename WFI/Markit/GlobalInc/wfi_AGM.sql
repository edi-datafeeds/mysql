--filepath=O:\Datafeed\Debt\Markit_GlobalInc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Markit_Global\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('AGM') as TableName,
wca.agm.Actflag,
wca.agm.AnnounceDate as Created,
wca.agm.Acttime as Changed, 
wca.agm.AGMID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.agm.IssID,
wca.agm.AGMDate,
wca.agm.AGMEGM,
wca.agm.AGMNo,
wca.agm.FYEDate,
wca.agm.AGMTime,
wca.agm.Add1,
wca.agm.Add2,
wca.agm.Add3,
wca.agm.Add4,
wca.agm.Add5,
wca.agm.Add6,
wca.agm.City,
wca.agm.CntryCD,
wca.agm.BondSecID
FROM wca.agm
INNER JOIN wca.scmst ON wca.agm.BondSecID = wca.scmst.SecID
INNER JOIN wca.bond ON wca.agm.BondSecID = wca.bond.SecID
where
wca.agm.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)