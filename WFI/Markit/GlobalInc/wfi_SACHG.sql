--filepath=O:\Datafeed\Debt\Markit_GlobalInc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Markit_Global\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SACHG') as TableName,
wca.sachg.Actflag,
wca.sachg.AnnounceDate as Created,
wca.sachg.Acttime as Changed,
wca.sachg.SachgID,
wca.sachg.ScagyID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.sachg.EffectiveDate,
wca.sachg.OldRelationship,
wca.sachg.NewRelationship,
wca.sachg.OldAgncyID,
wca.sachg.NewAgncyID,
wca.sachg.OldSpStartDate,
wca.sachg.NewSpStartDate,
wca.sachg.OldSpEndDate,
wca.sachg.NewSpEndDate,
wca.sachg.OldGuaranteeType,
wca.sachg.NewGuaranteeType
from wca.sachg
inner join wca.scagy on wca.sachg.scagyid = wca.scagy.scagyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sachg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)