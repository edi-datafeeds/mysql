--filepath=O:\Datafeed\Debt\Markit_Weekly\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_RU_List
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Russia\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
wca.bond.SecID,
wca.scmst.ISIN,
wca.issur.IssuerName as Issuer,
wca.scmst.SecurityDesc as Description,
wca.scexh.ExchgCD,
wca.issur.IssID,
wca.bond.IssueCurrency as Currency,
wca.exchg.Exchgname as EXCHANGE,
case when wca.scmst.statusflag = 'I' THEN 'Inactive' when wca.scmst.statusflag = 'A' or wca.scmst.statusflag = ''THEN 'Active'
WHEN wca.scmst.statusflag = 'D' THEN 'In Default' else wca.scmst.statusflag end as Security_Status,
wca.issur.CntryofIncorp as Country_of_Incorporation,
wca.sedol.SEDOL,
wca.scexh.LocalCode as Symbol,
-- wca.issur.Isstype as security_Type,
wca.scmst.SectyCD as Security_Type,
wca.scmst.USCode as US_Code,
wca.bond.IssueDate as Issue_Date,
wca.bond.CurenCD as Current_Currency,
wca.bond.IssuePrice as Issue_Price,
wca.bond.issuecurrency as Issue_Currency,
wca.bond.IssueAmount as Issue_Amount_in_Millions,
wca.bond.OutstandingAmount as Outstanding_In_Millions,
wca.bond.InterestBasis Interest_Basis,
wca.bond.InterestRate as Interest_Rate,
wca.bond.MaturityDate as Maturity_Date
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.ExchgCD = wca.exchg.ExchgCD
left outer join wca.sedol on wca.scmst.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
where issur.issid in (select code from client.pfissid where accid = '277')
and wca.bond.IssueDate > CURRENT_DATE() - INTERVAL 1 MONTH
order by wca.bond.IssueDate desc


