--filepath=O:\Datafeed\Debt\Markit_SRF\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_Markit_SRF_Global_Delta
--fileheadertext=EDI_WFI_SRF_Global_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_SRF\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--#
SELECT DISTINCT
    CASE 
                                WHEN wca.scexh.ScexhID IS NULL THEN 0
        ELSE wca.scexh.ScexhID
    END AS ScexhID,     
    
    -- CASE
--         WHEN wca.bbc.BbcID IS NULL THEN 0
--         ELSE wca.bbc.BbcID
--     END AS BbcID,
    
    '0' AS BbcID, 
    
    CASE
        WHEN wca.bbe.BbeID IS NULL THEN 0
        ELSE wca.bbe.BbeID
    END AS BbeID,
    
    
 --    CASE
--         WHEN wca.sedol.SedolID is null then 0
--         ELSE wca.sedol.SedolID
--     END AS SedolID,
    
    
    case
                                when xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.SedolID
                                when bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.SedolID
                                else '0'
    end as SedolID,
    
    
    
    #wca.sedol.actflag as SedolActflag,   
    
    case
                                when xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.actflag
                                when bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.actflag
                                else ''
                end as SedolActflag,
    
    
    
    wca.bond.Actflag,    
    case when (bond.acttime > scmst.acttime)
                  and (bond.acttime > ifnull(scexh.acttime,'2000-01-01'))
      and (bond.acttime > issur.acttime)
      and (bond.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (bond.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then bond.acttime 
 
      when (scmst.acttime > ifnull(scexh.acttime,'2000-01-01'))
      and (scmst.acttime > issur.acttime)
      and (scmst.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scmst.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scmst.acttime
    
    
     
     when (scexh.acttime > issur.acttime)
      and (scexh.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (scexh.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then scexh.acttime
     
     when (issur.acttime > ifnull(mktsg.acttime,'2000-01-01'))
      and (issur.acttime > ifnull(exchg.acttime,'2000-01-01'))
     then issur.acttime
     
     when (ifnull(mktsg.acttime,'2000-01-01') > ifnull(exchg.acttime,'2000-01-01'))
     then mktsg.acttime
     else exchg.acttime
     end as Changed,    
    wca.scexh.AnnounceDate AS Created,
    wca.scmst.SecID,
    wca.scmst.IssID,
    wca.scmst.isin AS ISIN,
    wca.scmst.uscode AS USCode,
    wca.issur.IssuerName,
    wca.issur.CntryofIncorp,
    wca.issur.SIC,
    wca.issur.CIK,
    wca.issur.IndusID,
    wca.scmst.SectyCD,
    wca.scmst.SecurityDesc,
    wca.scmst.ParValue,
    wca.scmst.CurenCD AS PVCurrency,
    CASE
        WHEN wca.scmst.StatusFlag = '' THEN UPPER('A')
        ELSE wca.scmst.StatusFlag
    END AS StatusFlag,
    wca.scmst.PrimaryExchgCD,
    '' as BbgCurrency,
    '' as BbgCompositeGlobalID,
    '' as BbgCompositeTicker,
    wca.bbe.bbgexhid AS BbgGlobalID,
    wca.bbe.bbgexhtk AS BbgExchangeTicker,
    
    
    #wca.sedol.Sedol,
    case
                                when xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.Sedol
                                when bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.Sedol
                                else ''
                end as Sedol,
    
    
    #wca.sedol.Defunct as SedolDefunct,
    case
                                when xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.Defunct
                                when bsedol.sedol is not null and bsedol.sedol <> '' then bsedol.Defunct
                                else ''
                end as SedolDefunct,
    
    
    #wca.sedol.rcntrycd as SedolRegCntryCD,
    case
                                when xsedol.sedol is not null and xsedol.sedol <> '' then xsedol.rcntrycd
                                else ''
                end as SedolRegCntryCD,
    
    
           
    wca.scmst.StructCD,
    wca.exchg.CntryCD AS ExchgCntry,
    wca.scexh.ExchgCD,
    wca.exchg.Mic,
    CASE
        WHEN wca.mktsg.mktsgid IS NULL THEN ''
        ELSE wca.mktsg.mic
    END AS Micseg,
    wca.scexh.LocalCode,
    CASE
        WHEN
            wca.scexh.ListStatus = 'N'
                OR wca.scexh.ListStatus = 'R'
                OR wca.scexh.ListStatus = ''
        THEN
            UPPER('L')
        ELSE wca.scexh.ListStatus
    END AS ListStatus
FROM
    wca.bond
    
        INNER JOIN
    wca.scmst ON wca.bond.secid = wca.scmst.secid
        
        LEFT OUTER JOIN
    wca.scexh ON wca.scmst.secid = wca.scexh.secid 
        
        INNER JOIN
    wca.issur ON wca.scmst.issid = wca.issur.issid
        
        LEFT OUTER JOIN
    wca.mktsg ON wca.scexh.mktsgid = wca.mktsg.mktsgid 
        
        LEFT OUTER JOIN
    wca.exchg ON wca.scexh.exchgcd = wca.exchg.exchgcd
        
              
  
  
             LEFT OUTER JOIN
    wca.sedol as xsedol ON wca.scexh.secid = xsedol.secid
        AND wca.exchg.cntrycd = xsedol.cntrycd
        
  
        
        LEFT OUTER JOIN
    wca.sedol as bsedol ON wca.bond.secid = bsedol.secid
        AND 'zz' = bsedol.cntrycd  
        
  
         
        
        
        LEFT OUTER JOIN
                        wca.bbe ON wca.scmst.secid = wca.bbe.secid
                        -- AND wca.scexh.exchgcd = wca.bbe.exchgcd
                       -- AND wca.sedol.curencd<>''
                        -- AND wca.bbe.curencd = wca.sedol.curencd
                        -- AND 'D' <> wca.bbe.actflag
                        
                        -- LEFT OUTER JOIN
                    -- wca.bbc ON wca.bbe.secid = wca.bbc.secid
                        -- AND wca.exchg.cntrycd = wca.bbc.cntrycd
                        -- AND wca.bbe.curencd = wca.bbc.curencd
        
        
        
WHERE
        

(wca.bond.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scexh.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.issur.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.mktsg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.exchg.acttime >= (select max(feeddate) from wca.tbl_opslog where seq = 3))