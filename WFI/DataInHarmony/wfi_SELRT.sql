--filepath=O:\Datafeed\Debt\DataInHarmony\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SELRT
--fileheadertext=EDI_SELRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\DataInHarmony\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SELRT') as TableName,
wca.selrt.Actflag,
wca.selrt.AnnounceDate as Created,
wca.selrt.Acttime as Changed,
wca.selrt.SelrtID,
wca.selrt.SecID,
wca.scmst.ISIN,
wca.selrt.CntryCD,
wca.selrt.Restriction
from wca.selrt
inner join wca.bond on wca.selrt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.selrt.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
