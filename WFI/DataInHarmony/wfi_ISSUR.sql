--filepath=O:\Datafeed\Debt\DataInHarmony\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\DataInHarmony\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('ISSUR') as TableName,
wca.issur.Actflag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssID,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.GICS,
wca.issur.IssuerName,
wca.issur.IndusID,
wca.issur.CntryofIncorp,
wca.issur.FinancialYearEnd,
wca.issur.Shortname,
wca.issur.LegalName,
-- case when wca.issur.cntryofincorp='AA' then 'S' when wca.issur.isstype='GOV' then 'G' when wca.issur.isstype='GOVAGENCY' then 'Y' else 'C' end as Isstype,
wca.issur.isstype,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.issur.IssurNotes as Notes
from wca.issur
-- inner join wca.scmst on wca.issur.issid = wca.scmst.issid
-- inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.issur.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
