--filepath=O:\Datafeed\Debt\DataInHarmony\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\DataInHarmony\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LTCHG') as TableName,
wca.ltchg.Actflag,
wca.ltchg.AnnounceDate as Created,
wca.ltchg.Acttime as Changed,
wca.ltchg.LtChgID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.ltchg.ExchgCD,
wca.ltchg.EffectiveDate,
wca.ltchg.OldLot,
wca.ltchg.OldMinTrdQty,
wca.ltchg.NewLot,
wca.ltchg.NewMinTrdgQty
from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.ltchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
