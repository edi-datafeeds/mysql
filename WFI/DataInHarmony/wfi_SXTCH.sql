--filepath=O:\Datafeed\Debt\DataInHarmony\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_SXTCH
--fileheadertext=EDI_SXTCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\DataInHarmony\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SXTCH') as TableName,
wca.sxtch.Actflag,
wca.sxtch.AnnounceDate as Created,
wca.sxtch.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.sxtch.ScxtcID,
wca.sxtch.EffectiveDate,
wca.sxtch.OldCurenCD,
wca.sxtch.NewCurenCD,
wca.sxtch.OldLocalCode,
wca.sxtch.NewLocalCode,
wca.sxtch.OldPriceTick,
wca.sxtch.NewPriceTick,
wca.sxtch.OldTickSize,
wca.sxtch.NewTickSize,
wca.sxtch.OldISIN,
wca.sxtch.NewISIN,
wca.sxtch.SxtChID
from wca.sxtch
inner join wca.scxtc on wca.sxtch.ScxtcID = wca.scxtc.ScxtcID
inner join wca.scexh on wca.scxtc.ScExhID = wca.scexh.ScExhID
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sxtch.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
