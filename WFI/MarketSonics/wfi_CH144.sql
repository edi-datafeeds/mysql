--filepath=O:\Datafeed\Debt\MarketSonics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CH144
--fileheadertext=EDI_CH144_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('CH144') as TableName,
wca.ch144.Actflag,
wca.ch144.AnnounceDate as Created,
wca.ch144.Acttime as Changed,
wca.ch144.LK144ID,
wca.ch144.EffectiveDate,
wca.ch144.SerialNumber,
wca.ch144.OldSecID144A,
wca.ch144.NewSecID144a,
wca.ch144.OldSecIDRegS,
wca.ch144.NewSecIDRegS,
wca.ch144.OldSecIDUnrestricted,
wca.ch144.NewSecIDUnrestricted,
wca.ch144.OldLinkOutstandingAmount,
wca.ch144.NewLinkOutstandingAmount,
wca.ch144.OldLinkOutstandingAmountDate,
wca.ch144.NewLinkOutstandingAmountDate,
wca.ch144.EventType,
wca.ch144.RelEventID,
wca.ch144.CH144ID
from wca.ch144
inner join wca.scmst on wca.ch144.OldSecID144A = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.Exchgcd LIKE 'US%'
and wca.issur.isstype = 'GOV'

AND wca.issur.actflag <> 'D'

and wca.ch144.actflag <> 'D'
AND wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and wca.scmst.statusflag <> 'I'