--filepath=O:\Datafeed\Debt\MarketSonics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('FRNFX') as TableName,
wca.frnfx.Actflag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnfxID,
wca.frnfx.SecID,
wca.scmst.ISIN,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFRNType,
wca.frnfx.OldFRNIndexBenchmark,
wca.frnfx.OldMarkup As OldFrnMargin,
wca.frnfx.OldMinimumInterestRate,
wca.frnfx.OldMaximumInterestRate,
wca.frnfx.OldRounding,
wca.frnfx.NewFRNType,
wca.frnfx.NewFRNindexBenchmark,
wca.frnfx.NewMarkup As NewFrnMargin,
wca.frnfx.NewMinimumInterestRate,
wca.frnfx.NewMaximumInterestRate,
wca.frnfx.NewRounding,
wca.frnfx.Eventtype,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.Exchgcd LIKE 'US%'
and wca.issur.isstype = 'GOV'

AND wca.issur.actflag <> 'D'

and wca.frnfx.actflag <> 'D'
AND wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and wca.scmst.statusflag <> 'I'