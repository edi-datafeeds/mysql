--filepath=O:\Datafeed\Debt\MarketSonics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('IRCHG') as TableName,
wca.irchg.Actflag,
wca.irchg.AnnounceDate as Created,
wca.irchg.Acttime as Changed,
wca.irchg.IrchgID,
wca.irchg.SecID,
wca.scmst.ISIN,
wca.irchg.EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.Eventtype,
wca.irchg.Notes 
from wca.irchg
inner join wca.bond on wca.irchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.Exchgcd LIKE 'US%'
and wca.issur.isstype = 'GOV'

AND wca.issur.actflag <> 'D'

and wca.irchg.actflag <> 'D'
AND wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and wca.scmst.statusflag <> 'I'