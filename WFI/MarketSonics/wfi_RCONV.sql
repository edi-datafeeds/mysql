--filepath=O:\Datafeed\Debt\MarketSonics\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT distinct
upper('RCONV') as TableName,
wca.rconv.Actflag,
wca.rconv.AnnounceDate as Created,
wca.rconv.Acttime as Changed,
wca.rconv.RconvID,
wca.rconv.SecID,
wca.scmst.ISIN,
wca.rconv.EffectiveDate,
wca.rconv.OldInterestAccrualConvention,
wca.rconv.NewInterestAccrualConvention,
wca.rconv.OldConvMethod,
wca.rconv.NewConvMethod,
wca.rconv.Eventtype,
wca.rconv.Notes
from wca.rconv
inner join wca.bond on wca.rconv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.scexh.Exchgcd LIKE 'US%'
and wca.issur.isstype = 'GOV'

AND wca.issur.actflag <> 'D'

and wca.rconv.actflag <> 'D'
AND wca.scmst.actflag <> 'D'
and wca.scexh.actflag <> 'D'
and wca.scexh.liststatus <> 'D'
and wca.scmst.statusflag <> 'I'