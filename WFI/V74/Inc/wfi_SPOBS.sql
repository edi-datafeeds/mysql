--filepath=O:\Datafeed\Debt\V74\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SPOBS
--fileheadertext=EDI_SPOBS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V74i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPOBS') as TableName,
spobs.Actflag,
spobs.AnnounceDate as Created,
spobs.Acttime as Changed,
spobs.SecID,
spobs.SpobsID,
spobs.ObservationPayoutType,
spobs.ObservationFromDate,
spobs.ObservaionToDate,
spobs.ObservationDays,
spobs.ObservationDaysConvention,
spobs.ObservationBusDayConv,
spobs.DeterminationFromDate,
spobs.DeterminationToDate,
spobs.DeterminationDays,
spobs.DeterminationDaysConvention,
spobs.DeterminationBusDayConv,
spobs.PayoutDate,
spobs.PayoutDays,
spobs.PayouttDaysConvention,
spobs.PayoutBusDayConv,
spobs.FixedPayoutCurrency,
spobs.FixedPayoutValue,
spobs.FixedPayoutPercent,
spobs.MinimumPayoutCurrency,
spobs.MinimumPayoutValue,
spobs.MinimumPayoutPercent,
spobs.MaximumPayoutCurrency,
spobs.MaximumPayoutValue,
spobs.MaximumPayoutPercent,
spobs.Multiplier,
spobs.TriggerAction,
spobs.BonusCurrency,
spobs.BonusValue,
spobs.BonusLevelPercent,
spobs.OptionStyle
from wca.spobs
inner join wca.scmst on wca.spobs.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spobs.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)
