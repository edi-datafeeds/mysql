--filepath=O:\Datafeed\Debt\V74\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SPCND
--fileheadertext=EDI_SPCND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V74\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPCND') as TableName,
spcnd.Actflag,
spcnd.AnnounceDate as Created,
spcnd.Acttime as Changed,
scmst.SecID,
spcnd.SpcndID,
spcnd.Markup,
spcnd.UnderlyingCondition,
spcnd.WithReferenceTo,
spcnd.Level1Currency,
spcnd.Level1Percent,
spcnd.Level1Value,
spcnd.Level2Currency,
spcnd.Level2Percent,
spcnd.Level2Value
from wca.spcnd
inner join wca.spun on wca.spcnd.SpunID = wca.spun.SpunID
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spcnd.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)
