--filepath=O:\Datafeed\Debt\V74\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_SPUN
--fileheadertext=EDI_SPUN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WFI\V74\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPUN') as TableName,
spun.Actflag,
spun.AnnounceDate as Created,
spun.Acttime as Changed,
spun.SecID,
spun.SpunID,
spun.UnderlyingSecID,
spun.ExchgCD,
spun.Weight,
spun.NumberOfUnderlyings,
spun.Ratio,
spun.ReferencePage1,
spun.ReferencePage2,
spun.ReferencePage3,
spun.InitialFixingCurrency,
spun.InitialFixingPrice,
spun.InitialFixingPercent,
spun.StrikeFixingCurrency,
spun.StrikeFixingPrice,
spun.StrikeFixingPercent,
spun.FixingPlace,
spun.FixingTime
from wca.spun
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spun.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog)
-- or wca.scmst.secid in(select code from client.pfsecid where accid = 990)
