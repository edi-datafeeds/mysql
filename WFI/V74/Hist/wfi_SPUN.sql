--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V74\Hist\Output
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SPUN
--fileheadertext=EDI_SPUN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\WFI\V74\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPUN') as TableName,
spun.Actflag,
spun.AnnounceDate as Created,
spun.Acttime as Changed,
spun.SecID,
spun.SpunID,
spun.UnderlyingSecID,
spun.ExchgCD,
spun.Weight,
spun.NumberOfUnderlyings,
spun.Ratio,
spun.ReferencePage1,
spun.ReferencePage2,
spun.ReferencePage3,
spun.InitialFixingCurrency,
spun.InitialFixingPrice,
spun.InitialFixingPercent,
spun.StrikeFixingCurrency,
spun.StrikeFixingPrice,
spun.StrikeFixingPercent,
spun.FixingPlace,
spun.FixingTime
from wca.spun
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spun.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
