--filepath=O:\Prodman\Dev\WFI\Feeds\Generic\V74\Hist\Output
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SPCND
--fileheadertext=EDI_SPCND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\WFI\V74\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT Distinct
upper('SPCND') as TableName,
spcnd.Actflag,
spcnd.AnnounceDate as Created,
spcnd.Acttime as Changed,
scmst.SecID,
spcnd.SpcndID,
spcnd.Markup,
spcnd.UnderlyingCondition,
spcnd.WithReferenceTo,
spcnd.Level1Currency,
spcnd.Level1Percent,
spcnd.Level1Value,
spcnd.Level2Currency,
spcnd.Level2Percent,
spcnd.Level2Value
from wca.spcnd
inner join wca.spun on wca.spcnd.SpunID = wca.spun.SpunID
inner join wca.scmst on wca.spun.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.spcnd.Actflag <> 'D'
and wca.scmst.Actflag <> 'D'
