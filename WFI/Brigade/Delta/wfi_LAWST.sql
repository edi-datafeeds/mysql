--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LAWST
--fileheadertext=EDI_LAWST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 
select distinct
upper('LAWST') as Tablename,
wca.lawst.ActFlag,
wca.lawst.AnnounceDate as Created,
wca.lawst.Acttime as Changed,
wca.lawst.LawstId,
wca.bond.SecId,
wca.scmst.Isin,
wca.lawst.IssId,
wca.lawst.EffectiveDate,
wca.lawst.LaType,
wca.lawst.RegDate
from wca.lawst
inner join wca.scmst on wca.lawst.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.lawst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.lawst.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





