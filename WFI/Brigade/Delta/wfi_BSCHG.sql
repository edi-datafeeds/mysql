--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('BSCHG') as Tablename,
wca.bschg.ActFlag,
wca.bschg.AnnounceDate as Created,
wca.bschg.Acttime as Changed,
wca.bschg.BschgId,
wca.bschg.SecId,
wca.scmst.Isin,
wca.bschg.NotificationDate,
wca.bschg.OldBondType,
wca.bschg.NewBondType,
wca.bschg.OldCurencd,
wca.bschg.NewCurencd,
wca.bschg.OldPIU,
wca.bschg.NewPIU,
wca.bschg.OldInterestBasis,
wca.bschg.NewInterestBasis,
wca.bschg.EventType,
wca.bschg.OldInterestCurrency,
wca.bschg.NewInterestCurrency,
wca.bschg.OldMaturityCurrency,
wca.bschg.NewMaturityCurrency,
wca.bschg.OldIntBusDayConv,
wca.bschg.NewIntBusDayConv,
wca.bschg.OldMatBusDayConv,
wca.bschg.NewMatBusDayConv,
wca.bschg.BschgNotes as Notes
from wca.bschg
inner join wca.bond on wca.bschg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.bschg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.bschg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

