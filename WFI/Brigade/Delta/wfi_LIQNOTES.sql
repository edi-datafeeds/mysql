--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('LIQNOTES') as Tablename,
wca.liq.ActFlag,
wca.liq.LiqId,
wca.liq.IssId,
wca.liq.LiquidationTerms
from wca.liq
where
liq.Issid in (select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where (client.pfisin.accid=258 and client.pfisin.actflag='I'
and liq.acttime> (select date_sub(max(feeddate), interval '02:24' hour_minute) from wca.tbl_opslog where seq = 3 ))
or (client.pfisin.accid=258 and client.pfisin.actflag='U'
and liq.acttime> (select date_sub(max(feeddate), interval '02:24' hour_minute) from wca.tbl_opslog where seq = 3 )))
