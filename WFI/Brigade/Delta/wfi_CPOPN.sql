--filepath=O:\Datafeed\Debt\Brigade\Full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CPOPN
--fileheadertext=EDI_CPOPN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CPOPN') as TableName,
wca.cpopn.Actflag,
wca.cpopn.AnnounceDate as Created,
wca.cpopn.Acttime as Changed,
wca.cpopn.CpopnID,
wca.cpopn.SecID,
wca.cpopn.CallPut, 
wca.cpopn.Notes as Notes 
FROM wca.cpopn
INNER JOIN wca.bond ON wca.cpopn.SecID = wca.bond.SecID
INNER JOIN wca.scmst ON wca.bond.SecID = wca.scmst.SecID
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.cpopn.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.cpopn.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
