--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'REDEM' as Tablename,
wca.redem.Actflag,
wca.redem.Announcedate as Created,
case when (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) then wca.rd.Acttime else wca.redem.Acttime end as Changed,
wca.redem.Redemid,
wca.scmst.Secid,
wca.scmst.Isin,
wca.rd.RecDate,
wca.redem.RedemDate,
wca.redem.Curencd as RedemCurrency,
wca.redem.RedemPrice,
-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.RedemPrice end as RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
wca.redem.AmountRedeemed,
-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.AmountRedeemed end as AmountRedeemed,
wca.redem.RedemPremium,
-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.RedemPremium end as RedemPremium,
wca.redem.RedemPercent,
-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.RedemPercent end as RedemPercent,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,

-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.PoolFactor end as PoolFactor,
wca.redem.PriceAsPercent,
-- case when PremiumAsPercent = '' and (wca.redem.RedemType = 'AMT' or wca.redem.RedemType = 'CALL' or wca.redem.RedemType = 'ER') then '' else wca.redem.PriceAsPercent end as PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.redem.IndefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.redem.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.redem.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





