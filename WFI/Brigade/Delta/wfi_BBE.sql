--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
SELECT distinct
upper('BBE') as Tablename,
wca.bbe.ActFlag,
wca.bbe.AnnounceDate,
wca.bbe.Acttime,
wca.bbe.BbeId,
wca.bbe.SecId,
wca.bbe.Exchgcd,
wca.bbe.Curencd,
wca.bbe.BbgexhId,
wca.bbe.Bbgexhtk
from wca.bbe
inner join wca.bond on wca.bbe.secid = wca.bond.secid
inner join wca.scmst on wca.bbe.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.bbe.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.bbe.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

