--filepath=O:\Datafeed\Debt\Brigade\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Brigade\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('MTCHG') as Tablename,
wca.mtchg.ActFlag,
wca.mtchg.AnnounceDate as Created,
wca.mtchg.Acttime as Changed,
wca.mtchg.MtchgId,
wca.mtchg.SecId,
wca.scmst.Isin,
wca.mtchg.NotificationDate,
wca.mtchg.OldMaturityDate,
wca.mtchg.NewMaturityDate,
wca.mtchg.Reason,
wca.mtchg.EventType,
wca.mtchg.OldMaturityBenchmark,
wca.mtchg.NewMaturityBenchmark,
wca.mtchg.Notes as Notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=258 and actflag='I')
and wca.mtchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
or 
(isin in (select code from client.pfisin where accid=258 and actflag='U')
and wca.mtchg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





