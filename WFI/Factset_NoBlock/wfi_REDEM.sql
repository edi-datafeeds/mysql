--filepath=O:\Datafeed\Debt\Factset_NoBlock\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\NoBlock\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'REDEM' as Tablename,
redem.Actflag,
redem.Announcedate as Created,
case when (rd.Acttime is not null) and (rd.Acttime > redem.Acttime) then rd.Acttime else redem.Acttime end as Changed,
redem.Redemid,
scmst.Secid,
scmst.Isin,
rd.RecDate,
redem.RedemDate,
redem.Curencd as RedemCurrency,
redem.RedemPrice,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPrice end as RedemPrice,
redem.MandOptFlag,
redem.PartFinal,
redem.RedemType,
redem.AmountRedeemed,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.AmountRedeemed end as AmountRedeemed,
redem.RedemPremium,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPremium end as RedemPremium,
redem.RedemPercent,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.RedemPercent end as RedemPercent,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null or PremiumAsPercent = '' and subredem.RedemType <> 'MAT') then '' else redem.PoolFactor end as PoolFactor,
redem.PriceAsPercent,
-- case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null) or redem.PremiumAsPercent = '' and redem.PriceAsPercent = '100.000000000' and redem.RedemType <> 'MAT' then '' else redem.PriceAsPercent end as PriceAsPercent,
redem.PremiumAsPercent,
case when redem.secid in (select secid from redem as subredem where redem.secid=subredem.secid and subredem.PoolFactor <> '' and subredem.PoolFactor is not null) then '' else redem.PremiumAsPercent end as PremiumAsPercent,
redem.IndefPay,
redem.TenderOpenDate,
redem.TenderCloseDate,
redem.RedemNotes as Notes
from redem
inner join bond on redem.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join rd on redem.rdid = rd.rdid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and redem.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))