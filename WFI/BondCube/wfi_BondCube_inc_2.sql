--filepath=O:\Datafeed\Bespoke\BondCube\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BondCube_2
--fileheadertext=EDI_BondCube_2_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\BondCube\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.scmst.ActFlag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SECID,
wca.scmst.USCODE,
wca.sedol.SEDOL,
wca.exchg.MIC,
wca.scmst.WKN,
wca.bond.InterestRate,
wca.bond.CurenCD,
wca.scexh.LocalCode,
wca.bond.IssueDate,
wca.bond.MaturityDate,
wca.bondx.MinimumDenomination,
wca.bond.ParValue,
wca.issur.IssuerName,
wca.scmst.securitydesc,
wca.indus.IndusName,
case when wca.bond.Perpetual = 'P' or wca.bond.Perpetual = 'U' or wca.bond.Perpetual = 'I' Then 'Y' else wca.bond.Perpetual end as Perpetual,  
case when wca.scmst.statusflag = '' then 'A' else wca.scmst.statusflag end as statusflag
from wca.bond
left outer join wca.bondx on bond.secid  = bondx.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid= wca.issur.issid
left outer join wca.indus on wca.issur.indusid= wca.indus.indusid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgCD = wca.exchg.ExchgCD
left outer join wca.sedol on wca.scmst.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
where
wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
and wca.bond.BondType <> 'PFS'
and wca.bond.BondType <> 'PRF'