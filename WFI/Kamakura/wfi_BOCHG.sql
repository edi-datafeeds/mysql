--filepath=O:\Datafeed\Debt\Kamakura\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_opslog.Feeddate),'%Y%m%d' ) from wca.tbl_opslog
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Kamakura\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BOCHG') as TableName,
wca.bochg.Actflag,
wca.bochg.AnnounceDate as Created,
wca.bochg.Acttime as Changed,  
wca.bochg.BochgID,
wca.bochg.RelEventID,
wca.bochg.SecID,
wca.scmst.ISIN,
wca.bochg.EffectiveDate,
wca.bochg.OldOutValue,
wca.bochg.NewOutValue,
wca.bochg.EventType,
wca.bochg.OldOutDate,
wca.bochg.NewOutDate,
wca.bochg.BochgNotes as Notes
from wca.bochg
inner join wca.bond on wca.bochg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bochg.acttime > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)


