--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_yield
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\226\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
client.pfisin.code as Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyield.Callput,
priceshist.rawyield.CpType,
priceshist.rawyield.CpoptID,
DATE_FORMAT(priceshist.rawyield.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(priceshist.rawyield.ToDate, '%Y-%m-%d') as ToDate,
priceshist.rawyield.CpCurrency,
priceshist.rawyield.CpPrice,
priceshist.rawyield.CpPriceAsPercent,
case when priceshist.rawyield.ClosingPrice<>'' and priceshist.rawyield.ClosingPrice is not null
     then DATE_FORMAT(priceshist.rawyield.SettlementDate, '%Y-%m-%d')
     else ''
     end as SettlementDate,
case when priceshist.rawyield.ExchgCD<>'' and priceshist.rawyield.ExchgCD is not null
     then priceshist.rawyield.ExchgCD
     else wca.scmst.primaryexchgcd
     end as ExchgCD,
case when priceshist.rawyield.LocalCode<>'' and priceshist.rawyield.LocalCode is not null
     then priceshist.rawyield.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when priceshist.rawyield.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when priceshist.rawyield.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(priceshist.rawyield.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
priceshist.rawyield.ClosingPrice,
priceshist.rawyield.PriceCurrency,
priceshist.yieldcalc.AccruedInterest,
priceshist.yieldcalc.YieldtoMaturity,
priceshist.yieldcalc.YieldtoCall,
priceshist.yieldcalc.YieldtoPut,
priceshist.yieldcalc.ModifiedDuration,
priceshist.yieldcalc.EffectiveDuration,
priceshist.yieldcalc.MacaulayDuration,
priceshist.yieldcalc.Convexity,
wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 226=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join priceshist.rawyield on wca.scmst.secid=priceshist.rawyield.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
left outer join priceshist.yieldcalc on priceshist.rawyield.secid=priceshist.yieldcalc.secid
                                    and priceshist.rawyield.mktclosedate=priceshist.yieldcalc.mktclosedate
                                    and priceshist.rawyield.exchgcd=priceshist.yieldcalc.exchgcd
                                    and priceshist.rawyield.pricecurrency=priceshist.yieldcalc.pricecurrency
left outer join wca.scexh on priceshist.rawyield.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D';
