--filepath=O:\Datafeed\Bespoke\BondSigma\Calcin\
--filenameprefix=EDI_BondSigma_
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.csv
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\BondSigma\Calcin\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct
-- wca.rd.rdid,
bondsigma.refpricefull.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null then wca.intpy.intrate else wca.bond.InterestRate end as InterestRate2,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
-- case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null then wca.exdt.Paydate else '' end as Paydate,
-- case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null and wca.bond.InterestPaymentFrequency = 'QTR' and wca.exdt.paydate >= DATE_ADD(curdate(), INTERVAL -3 MONTH) then wca.exdt.Paydate else '1' end as Paydate,
-- case when (wca.bond.InterestRate = '' or wca.bond.InterestRate is null) AND (wca.intpy.intrate <> '') and wca.bond.InterestPaymentFrequency = 'ANL' and wca.exdt.paydate >= DATE_ADD(curdate(), INTERVAL -12 MONTH) then wca.exdt.Paydate else '1' end as Paydate,
-- case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null and wca.bond.InterestPaymentFrequency = 'QTR' and wca.exdt.paydate >= DATE_ADD(curdate(), INTERVAL -3 MONTH) then wca.exdt.Paydate else '1' end as Paydate,

CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN DATE_FORMAT(wca.exdt.paydate, '%Y-%m-%d')
ELSE ''
END AS paydate,


CASE
WHEN (wca.bond.InterestRate = '' OR wca.bond.InterestRate IS NULL)
    AND wca.intpy.intrate <> ''
    AND wca.intpy.intrate IS NOT NULL
    AND (
         (wca.bond.InterestPaymentFrequency = 'MNT'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 1 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'QTR'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 3 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'SMA'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 6 MONTH))
         OR (wca.bond.InterestPaymentFrequency = 'ANL'
             AND wca.exdt.paydate >= DATE_SUB(NOW(), INTERVAL 12 MONTH))
    )
THEN wca.intpy.AnlCoupRate
ELSE ''
END AS AnlCoupRate,


-- case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null then wca.intpy.AnlCoupRate else '' end as AnlCoupRate,
-- wca.exdt.Paydate,
-- wca.intpy.AnlCoupRate,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.refpricefull.Callput,
bondsigma.refpricefull.CpType,
bondsigma.refpricefull.CpoptID,

DATE_FORMAT(bondsigma.refpricefull.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(bondsigma.refpricefull.ToDate, '%Y-%m-%d') as ToDate,



bondsigma.refpricefull.CpCurrency,
case when bondsigma.refpricefull.CpPrice='0' then '' else bondsigma.refpricefull.CpPrice end as CpPrice,
bondsigma.refpricefull.CpPriceAsPercent,
wca.bond.IssuePrice,
-- wca.bond.IssueDate,
DATE_FORMAT(wca.bond.IssueDate, '%Y-%m-%d') as IssueDate,
wca.bond.Perpetual,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,
bondsigma.refpricefull.ExchgCD,
bondsigma.refpricefull.LocalCode,
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
case when bondsigma.refpricefull.LastTradeDate='0000-00-00' then null else DATE_FORMAT(bondsigma.refpricefull.LastTradeDate, '%Y-%m-%d') end as LastTradeDate,
bondsigma.refpricefull.ClosingPrice,
bondsigma.refpricefull.Open,
bondsigma.refpricefull.High,
bondsigma.refpricefull.Low,
bondsigma.refpricefull.Mid,
bondsigma.refpricefull.Ask,
bondsigma.refpricefull.Bid,
bondsigma.refpricefull.BidSize,
bondsigma.refpricefull.AskSize,
bondsigma.refpricefull.TradedVolume,
bondsigma.refpricefull.VolFlag,
bondsigma.refpricefull.PriceCurrency,
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity,
'' as Yield,
'' as YieldToWorst,
'' as KeyRateDuration
from bondsigma.refpricefull
inner join wca.scmst on bondsigma.refpricefull.secid=wca.scmst.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid
inner join wca.bond on wca.scmst.secid=wca.bond.secid

left outer join wca.rd on wca.bond.secid = wca.rd.secid
left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
left outer join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
left outer join wca.exdt ON wca.intpy.rdid = wca.exdt.RdID and 'INT'=wca.exdt.eventtype
where

-- wca.scmst.secid = '3991267'

-- and wca.rd.secid is not null
-- and wca.exdt.RdID is not null


-- wca.scmst.isin in (select client.pfisin.code from client.pfisin where accid = 990)
-- wca.scmst.secid = '1992602'
-- wca.bond.interestBasis<>'FXD'
-- wca.bond.callable<>'Y'
-- and wca.bond.puttable<>'Y'
-- and bondsigma.refpricefull.cpoptid is null
bondsigma.refpricefull.pricecurrency <> ''
-- and bondsigma.refpricefull.closingprice <> ''
-- and bondsigma.refpricefull.closingprice <> '0'
and bondsigma.refpricefull.closingprice is not null

AND (wca.rd.rdid =
(select subrd.rdid from wca.rd as subrd
left outer join wca.exdt as subexdt ON subrd.rdid=subexdt.RdID and 'INT'=subexdt.eventtype
left outer join wca.int_my as subint on subrd.rdid=subint.rdid
left outer join wca.intpy as subintpy on subint.rdid = subintpy.rdid
where subrd.secid=wca.rd.secid
order by subexdt.paydate desc limit 1)
or wca.rd.rdid is null)

and bondsigma.refpricefull.LastTradeDate = (SELECT MAX(t2.LastTradeDate)
                 FROM bondsigma.refpricefull t2
                 WHERE t2.secid = bondsigma.refpricefull.secid);