--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_CDS_TMX_BondSigma
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\no_cull_feeds\CDS_Yield\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct


bondsigma.cds_calcout.ISIN,
bondsigma.cds_calcout.SecID,
bondsigma.cds_calcout.UsCode,


wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,


bondsigma.cds_calcout.NominalValue,


wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,


bondsigma.cds_calcout.InterestRate,
bondsigma.cds_calcout.InterestPaymentFrequency,


wca.bond.InterestAccrualConvention,
bondsigma.cds_calcout.PayDate,
bondsigma.cds_calcout.AnlCoupRate,


DATE_FORMAT(bondsigma.cds_calcout.MaturityDate, '%Y-%m-%d') as MaturityDate,
bondsigma.cds_calcout.MatPriceAsPercent,

wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.cds_calcout.Callput,
bondsigma.cds_calcout.CpType,
bondsigma.cds_calcout.CpoptID,
DATE_FORMAT(bondsigma.cds_calcout.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(bondsigma.cds_calcout.ToDate, '%Y-%m-%d') as ToDate,
bondsigma.cds_calcout.CpCurrency,
bondsigma.cds_calcout.CpPrice,
bondsigma.cds_calcout.CpPriceAsPercent,

bondsigma.cds_calcout.IssuePrice,
bondsigma.cds_calcout.IssueDate,
bondsigma.cds_calcout.Perpetual,


case when bondsigma.cds_calcout.ClosingPrice<>'' and bondsigma.cds_calcout.ClosingPrice is not null
     then DATE_FORMAT(bondsigma.cds_calcout.SettlementDate, '%Y-%m-%d')
     else ''
     end as SettlementDate,



-- case when bondsigma.cds_calcout.ExchgCD<>'' and bondsigma.cds_calcout.ExchgCD is not null
--     then bondsigma.cds_calcout.ExchgCD
--     else wca.scmst.primaryexchgcd
--     end as ExchgCD,
  
  
  
  
 case when bondsigma.cds_calcout.ExchgCD<>'' and bondsigma.cds_calcout.ExchgCD is not null
      then bondsigma.cds_calcout.ExchgCD
      else scexh.ExchgCD
      end as ExchgCD,     
     
   
     
     
 case when bondsigma.cds_calcout.LocalCode<>'' and bondsigma.cds_calcout.LocalCode is not null
      then bondsigma.cds_calcout.LocalCode
      else wca.scexh.LocalCode
      end as LocalCode,
     
   
     
case when bondsigma.cds_calcout.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
     
     
case when bondsigma.cds_calcout.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(bondsigma.cds_calcout.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
     
     
     
bondsigma.cds_calcout.ClosingPrice,
bondsigma.cds_calcout.Open,
bondsigma.cds_calcout.High,
bondsigma.cds_calcout.Low,
bondsigma.cds_calcout.Mid,
bondsigma.cds_calcout.Ask,
bondsigma.cds_calcout.Bid,
bondsigma.cds_calcout.Bidsize,
bondsigma.cds_calcout.Asksize,
bondsigma.cds_calcout.TradedVolume,
bondsigma.cds_calcout.VolFlag,

bondsigma.cds_calcout.PriceCurrency,



bondsigma.cds_calcout.AccruedInterest,
bondsigma.cds_calcout.YieldtoMaturity,
bondsigma.cds_calcout.YieldtoCall,
bondsigma.cds_calcout.YieldtoPut,
bondsigma.cds_calcout.ModifiedDuration,
bondsigma.cds_calcout.EffectiveDuration,
bondsigma.cds_calcout.MacaulayDuration,
bondsigma.cds_calcout.Convexity,
bondsigma.cds_calcout.Yield,
bondsigma.cds_calcout.YieldtoWorst,
bondsigma.cds_calcout.KeyRateDuration,

wca.bond.BondType,
wca.bond.Subordinate,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD

from bondsigma.cds_calcout

left outer join wca.scmst on bondsigma.cds_calcout.secid = wca.scmst.secid
-- left outer join bondsigma.refpricefull on wca.scmst.secid=bondsigma.refpricefull.secid


left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid

-- inner join bondsigma.cds_calcout on bondsigma.refpricefull.secid=bondsigma.cds_calcout.secid
   --                                  and bondsigma.refpricefull.exchgcd=bondsigma.cds_calcout.exchgcd
      --                              and bondsigma.refpricefull.pricecurrency=bondsigma.cds_calcout.pricecurrency
left outer join wca.scexh on bondsigma.cds_calcout.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D';