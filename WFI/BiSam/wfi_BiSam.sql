--filepath=O:\Upload\Acc\264\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
--fileextension=.txt
--suffix=_Printing
--fileheadertext=EDI_BiSam_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\264\Feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
scmst.ActFlag,
scmst.AnnounceDate as Created,
scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.sedol.SEDOL,
-- wca.scmst.uscode,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scexh.ExchgCD,
wca.scexh.LocalCode,
case when wca.scmst.SecTyCD <> 'BND' then wca.scmst.CurenCD else wca.bond.CurenCD end as CurenCD,
wca.issur.CntryOfIncorp,
wca.scmst.SecTyCD,
wca.bond.BondType,
wca.bond.MaturityDate,
wca.bond.Perpetual,
wca.bond.InterestBasis,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
wca.bond.IssueDate,
bondsigma.calcout.FromDate,
bondsigma.calcout.ToDate,
bondsigma.calcout.CPType,
wca.bond.Markup as FrnMargin,
wca.bond.FRNIndexBenchmark,

wca.scmst.USCODE,
wca.bbc.BbgCompid,
wca.bbc.BbgComptk,
wca.issur.IssID,
wca.issur.ShortName as IssuerShortName,
wca.issur.LegalName as IssuerLegalName,
wca.issur.IssType as IssuerType, 
wca.cpopt.CallPut,
wca.bond.FirstCouponDate,
wca.bond.LastCouponDate,
wca.bond.ParValue,
wca.bond.FRNType,
wca.sedol.RCntryCD,
wca.sedol.CntryCD,
NULL as 'S&PRating',
NULL as FitchRating,
NULL as MoodyRating,
NULL as DBRSRating,
NULL AS GICS,
wca.issur.SIC,
wca.scmst.CFI,
wca.issur.IndusID,
wca.scmst.SharesOutstanding
from wca.scmst
left outer join wca.bond on wca.scmst.secid= wca.bond.secid
left outer join wca.cpopt on wca.scmst.secid = wca.cpopt.secid
inner join wca.issur on wca.scmst.issid= wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgCD = wca.exchg.ExchgCD
left outer join wca.bbc on wca.scexh.secid = bbc.secid and wca.exchg.cntrycd=bbc.cntrycd and 'D'<>bbc.actflag
left outer join wca.sedol on wca.scmst.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join bondsigma.calcout on wca.scmst.secid = bondsigma.calcout.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=264 and actflag='I')
or wca.sedol.sedol in (select code from client.pfsedol where accid=264 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=264 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=264 and actflag='U')
and wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.sedol.sedol in (select code from client.pfsedol where accid=264 and actflag='U')
and wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
or wca.scmst.uscode in (select code from client.pfuscode where accid=264 and actflag='U')
and wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog))