--filepath=O:\Upload\Acc\281\feed\NewInserts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_NewIssuers
--fileheadertext=EDI_New_Inserts_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\NewInserts\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
DATE_FORMAT(wca.bond.Announcedate, '%Y/%m/%d') AS Created,
wca.bond.Actflag,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bond.BondType,
wca.bond.MaturityDate,
wca.bond.MaturityStructure,
wca.bond.Perpetual,
wca.bond.Callable,
wca.bond.Puttable,
wca.issur.IssID,
wca.issur.IssuerName
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
where
wca.bond.actflag ='I'
and wca.bond.acttime >= curdate()
and wca.scmst.ISIN <> '';