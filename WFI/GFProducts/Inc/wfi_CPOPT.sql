--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
SELECT 
upper('CPOPT') as TableName,
wca.cpopt.Actflag,
wca.cpopt.AnnounceDate as Created,
wca.cpopt.Acttime as Changed,
wca.cpopt.CpoptID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.cptype,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(cpopt.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(cpopt.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')


