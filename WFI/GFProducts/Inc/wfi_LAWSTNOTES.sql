--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LAWSTNOTES') as TableName,
wca.lawst.Actflag,
wca.lawst.LawstID,
wca.lawst.IssId,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
(lawst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and lawst.issid in (select issid from client.pfisin where accid=281 and actflag='U')
or
(lawst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and lawst.issid in (select issid from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
or
(lawst.issid in (select issid from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1))))