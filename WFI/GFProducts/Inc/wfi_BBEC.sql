--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBEC') as tablename,
wca.bbec.ActFlag,
wca.bbec.AnnounceDate as Created,
wca.bbec.Acttime as Changed,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.bbec.BbeId,
wca.bbec.Oldexchgcd,
wca.bbec.Oldcurencd,
wca.bbec.EffectiveDate,
wca.bbec.Newexchgcd,
wca.bbec.Newcurencd,
wca.bbec.OldbbgexhId,
wca.bbec.NewbbgexhId,
wca.bbec.Oldbbgexhtk,
wca.bbec.Newbbgexhtk,
wca.bbec.ReleventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(bbec.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(bbec.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')


