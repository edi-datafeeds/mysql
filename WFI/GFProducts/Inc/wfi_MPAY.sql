--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('MPAY') as TableName,
wca.mpay.Actflag,
wca.mpay.AnnounceDate,
wca.mpay.Acttime,
wca.mpay.sEvent as EventType,
wca.mpay.EventID,
wca.mpay.OptionID,
wca.mpay.SerialID,
wca.mpay.SectyCD as ResSectyCD,
wca.mpay.ResSecID,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.MinOfrQty,
wca.mpay.MaxOfrQty,
wca.mpay.MinQlyQty,
wca.mpay.MaxQlyQty,
wca.mpay.Paydate,
wca.mpay.CurenCD,
wca.mpay.MinPrice,
wca.mpay.MaxPrice,
wca.mpay.TndrStrkPrice,
wca.mpay.TndrStrkStep,
wca.mpay.Paytype,
wca.mpay.DutchAuction,
wca.mpay.DefaultOpt,
wca.mpay.OptElectionDate,
wca.mpay.OedExpTime,
wca.mpay.OedExpTimeZone,
wca.mpay.WrtExpTime,
wca.mpay.WrtExpTimeZone
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
(mpay.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.scmst.isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(mpay.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.scmst.isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(wca.scmst.isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')