--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SCMST') as TableName,
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.scmstNotes As Notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(scmst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(scmst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')

union

SELECT 
upper('SCMST') as TableName,
wca.scmst.Actflag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.scmst.ParentSecID,
wca.scmst.IssID,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgCD,
wca.scmst.CurenCD,
wca.scmst.ParValue,
wca.scmst.USCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.StructCD,
wca.scmst.WKN,
wca.scmst.RegS144A,
wca.scmst.CFI,
wca.scmst.scmstNotes As Notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
-- wca.scmst.actflag = 'D'
(wca.scmst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.scmst.SecID in (Select wca.icc.SecID from wca.icc where newisin = ''
and wca.icc.oldisin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S')
or
(wca.scmst.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and wca.scmst.SecID in (Select wca.icc.SecID from wca.icc where newisin = ''
and wca.icc.oldisin in (select code from client.pfisin where accid=281 and actflag='I')
and wca.bond.MaturityStructure <> 'S')
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
or
wca.scmst.SecID in (Select wca.icc.SecID from wca.icc where newisin = ''
and wca.icc.oldisin in (select code from client.pfisin where accid=281 and actflag='I')
and wca.bond.MaturityStructure <> 'S')
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)));