--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate as Created,
wca.bbcc.Acttime as Changed,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.Oldcntrycd,
wca.bbcc.Oldcurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.Newcntrycd,
wca.bbcc.Newcurencd,
wca.bbcc.OldbbgcompId,
wca.bbcc.NewbbgcompId,
wca.bbcc.Oldbbgcomptk,
wca.bbcc.Newbbgcomptk,
wca.bbcc.ReleventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(bbcc.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(bbcc.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')


