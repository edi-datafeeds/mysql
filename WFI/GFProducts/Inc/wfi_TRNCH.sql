--filepath=O:\Upload\Acc\281\feed\Inc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('TRNCH') as TableName,
wca.trnch.Actflag,
wca.trnch.AnnounceDate as Created,
wca.trnch.Acttime as Changed,
wca.trnch.TrnchID,
wca.trnch.SecID,
wca.scmst.ISIN,
wca.trnch.TrnchNumber,
wca.trnch.TrancheDate,
wca.trnch.TrancheAmount,
wca.trnch.ExpiryDate,
wca.trnch.Notes as Notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(trnch.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='U')
and wca.bond.MaturityStructure <> 'S'
or
(trnch.acttime >(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)
and isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3<>(select seq from wca.tbl_opslog order by acttime desc limit 1)
and wca.bond.MaturityStructure <> 'S'
or
(isin in (select code from client.pfisin where accid=281 and actflag='I') 
and 3=(select seq from wca.tbl_opslog order by acttime desc limit 1)))
and wca.bond.MaturityStructure <> 'S')