DROP TABLE IF EXISTS bondsigma.cds;

CREATE TABLE bondsigma.cds (
  `TradeEntryTimestamp` varchar(26) NOT NULL,
  `ISIN` char(12) DEFAULT NULL,
  `USCode` varchar(9) DEFAULT NULL,
  `CurrenCD` char(3) DEFAULT NULL,
  `TradedVolume` varchar(20) DEFAULT NULL,
  `Price` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`TradeEntryTimestamp`),
  KEY `ISIN` (`ISIN`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



load data local infile 'O:/Datafeed/Debt/CDS/FIPS/Port/CDS_FIPS.txt' 
into table bondsigma.cds CHARACTER SET latin1
fields terminated by '\t'
lines terminated by '\n'
-- IGNORE 1 LINES
-- (TradeEntryTimestamp, ISIN, USCode, CurrenCD, TradedVolume, Price);
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,@col12,@col13,@col14,@col15,@col16,@col17,@col18)
set TradeEntryTimestamp=@col1,ISIN=@col8,USCode=@col7,CurrenCD=@col14,TradedVolume=@col15,Price=@col16;