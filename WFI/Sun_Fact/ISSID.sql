--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=ISSID_PORT
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=N
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
#wca.bond.secid,
#wca.scmst.isin,
wca.issur.issid
#wca.bond.MaturityStructure
from wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bond.MaturityStructure = 'c'
and wca.issur.issid is not null;