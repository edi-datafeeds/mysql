--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=N
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.resscmst.ISIN as ResISIN
from wca.convt
INNER join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
#inner join wca.bond on resscmst.secid = wca.bond.secid
AND wca.convt.actflag <> 'D'
#and (wca.zctchg.actflag <> 'D' or wca.zctchg.actflag is null)
#and wca.bond.actflag <> 'D'
and wca.resscmst.actflag <> 'D'
and wca.resscmst.StatusFlag <> 'I'
AND wca.resscmst.ISIN <> '';