--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=N
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
wca.resscmst.ISIN as ResISIN
from wca.zctchg
INNER join wca.scmst as resscmst on wca.zctchg.NEWressecid = wca.resscmst.secid
#inner join wca.bond on resscmst.secid = wca.bond.secid
AND wca.zctchg.actflag <> 'D'
#and (wca.zctchg.actflag <> 'D' or wca.zctchg.actflag is null)
#and wca.bond.actflag <> 'D'
and wca.resscmst.actflag <> 'D'
and wca.resscmst.StatusFlag <> 'I'
AND wca.resscmst.ISIN <> '';