--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=
--filenamealt=
--fileextension=.txt
--suffix=WFI_Coverage_Fields
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=N
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
wca.scmst.SecID,
wca.scmst.ISIN,
wca.issur.IssID,
bond.MaturityStructure,
scmst.SectyCD,
scmst.SecurityDesc,
issur.IssuerName
from wca.issur
left outer join wca.scmst on wca.issur.issid = wca.scmst.issid
left outer join wca.bond on wca.scmst.secid = wca.bond.secid
where
#(wca.bond.MaturityStructure = 'C' or wca.bond.MaturityStructure = 'E')
wca.scmst.statusflag <> 'I' 
AND wca.scmst.actflag <> 'D'
AND wca.bond.actflag <> 'D';