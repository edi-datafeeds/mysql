--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WCA_Conversion_Events
--fileheadertext=EDI WCA Conversion Events Feed 
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
evf_announcement.EventID,
'Announcement' as Event,
evf_announcement.Country_of_Incorporation as CntryofIncorp,
evf_announcement.Issuer_Name as IssuerName,
evf_announcement.ISIN,
evf_announcement.Sedol,
evf_announcement.us_code as USCode,
evf_announcement.Local_Code LocalCode,
evf_announcement.Exchange_code as ExchgCD,
evf_announcement.security_description as SecurityDesc,
evf_announcement.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_announcement.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_announcement
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_announcement.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_announcement.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_announcement.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Arrangement.EventID,
'Arrangement' as Event,
evf_Arrangement.Country_of_Incorporation as CntryofIncorp,
evf_Arrangement.Issuer_Name as IssuerName,
evf_Arrangement.ISIN,
evf_Arrangement.Sedol,
evf_Arrangement.us_code as USCode,
evf_Arrangement.Local_Code LocalCode,
evf_Arrangement.Exchange_code as ExchgCD,
evf_Arrangement.security_description as SecurityDesc,
evf_Arrangement.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Arrangement.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Arrangement
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Arrangement.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Arrangement.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Arrangement.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Assimilation.EventID,
'Assimilation' as Event,
evf_Assimilation.Country_of_Incorporation as CntryofIncorp,
evf_Assimilation.Issuer_Name as IssuerName,
evf_Assimilation.ISIN,
evf_Assimilation.Sedol,
evf_Assimilation.us_code as USCode,
evf_Assimilation.Local_Code LocalCode,
evf_Assimilation.Exchange_code as ExchgCD,
evf_Assimilation.security_description as SecurityDesc,
evf_Assimilation.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Assimilation.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Assimilation
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Assimilation.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Assimilation.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Assimilation.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Bankruptcy.EventID,
'Bankruptcy' as Event,
evf_Bankruptcy.Country_of_Incorporation as CntryofIncorp,
evf_Bankruptcy.Issuer_Name as IssuerName,
evf_Bankruptcy.ISIN,
evf_Bankruptcy.Sedol,
evf_Bankruptcy.us_code as USCode,
evf_Bankruptcy.Local_Code LocalCode,
evf_Bankruptcy.Exchange_code as ExchgCD,
evf_Bankruptcy.security_description as SecurityDesc,
evf_Bankruptcy.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Bankruptcy.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Bankruptcy
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Bankruptcy.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Bankruptcy.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Bankruptcy.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Bonus.EventID,
'Bonus' as Event,
evf_Bonus.Country_of_Incorporation as CntryofIncorp,
evf_Bonus.Issuer_Name as IssuerName,
evf_Bonus.ISIN,
evf_Bonus.Sedol,
evf_Bonus.us_code as USCode,
evf_Bonus.Local_Code LocalCode,
evf_Bonus.Exchange_code as ExchgCD,
evf_Bonus.security_description as SecurityDesc,
evf_Bonus.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Bonus.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Bonus
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Bonus.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Bonus.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Bonus.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Bonus_Rights.EventID,
'Bonus Rights' as Event,
evf_Bonus_Rights.Country_of_Incorporation as CntryofIncorp,
evf_Bonus_Rights.Issuer_Name as IssuerName,
evf_Bonus_Rights.ISIN,
evf_Bonus_Rights.Sedol,
evf_Bonus_Rights.us_code as USCode,
evf_Bonus_Rights.Local_Code LocalCode,
evf_Bonus_Rights.Exchange_code as ExchgCD,
evf_Bonus_Rights.security_description as SecurityDesc,
evf_Bonus_Rights.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Bonus_Rights.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Bonus_Rights
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Bonus_Rights.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Bonus_Rights.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Bonus_Rights.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Buy_Back.EventID,
'Buy Back' as Event,
evf_Buy_Back.Country_of_Incorporation as CntryofIncorp,
evf_Buy_Back.Issuer_Name as IssuerName,
evf_Buy_Back.ISIN,
evf_Buy_Back.Sedol,
evf_Buy_Back.us_code as USCode,
evf_Buy_Back.Local_Code LocalCode,
evf_Buy_Back.Exchange_code as ExchgCD,
evf_Buy_Back.security_description as SecurityDesc,
evf_Buy_Back.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Buy_Back.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Buy_Back
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Buy_Back.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Buy_Back.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Buy_Back.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_call.EventID,
'Call' as Event,
evf_call.Country_of_Incorporation as CntryofIncorp,
evf_call.Issuer_Name as IssuerName,
evf_call.ISIN,
evf_call.Sedol,
evf_call.us_code as USCode,
evf_call.Local_Code LocalCode,
evf_call.Exchange_code as ExchgCD,
evf_call.security_description as SecurityDesc,
evf_call.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_call.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_call
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_call.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_call.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_call.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Capital_Reduction.EventID,
'Capital Reduction' as Event,
evf_Capital_Reduction.Country_of_Incorporation as CntryofIncorp,
evf_Capital_Reduction.Issuer_Name as IssuerName,
evf_Capital_Reduction.ISIN,
evf_Capital_Reduction.Sedol,
evf_Capital_Reduction.us_code as USCode,
evf_Capital_Reduction.Local_Code LocalCode,
evf_Capital_Reduction.Exchange_code as ExchgCD,
evf_Capital_Reduction.security_description as SecurityDesc,
evf_Capital_Reduction.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Capital_Reduction.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Capital_Reduction
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Capital_Reduction.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Capital_Reduction.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Capital_Reduction.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Certificate_Exchange.EventID,
'Certificate Exchange' as Event,
evf_Certificate_Exchange.Country_of_Incorporation as CntryofIncorp,
evf_Certificate_Exchange.Issuer_Name as IssuerName,
evf_Certificate_Exchange.ISIN,
evf_Certificate_Exchange.Sedol,
evf_Certificate_Exchange.us_code as USCode,
evf_Certificate_Exchange.Local_Code LocalCode,
evf_Certificate_Exchange.Exchange_code as ExchgCD,
evf_Certificate_Exchange.security_description as SecurityDesc,
evf_Certificate_Exchange.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Certificate_Exchange.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Certificate_Exchange
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Certificate_Exchange.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Certificate_Exchange.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Certificate_Exchange.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Class_Action.EventID,
'Class_Action' as Event,
evf_Class_Action.Country_of_Incorporation as CntryofIncorp,
evf_Class_Action.Issuer_Name as IssuerName,
evf_Class_Action.ISIN,
evf_Class_Action.Sedol,
evf_Class_Action.us_code as USCode,
evf_Class_Action.Local_Code LocalCode,
evf_Class_Action.Exchange_code as ExchgCD,
evf_Class_Action.security_description as SecurityDesc,
evf_Class_Action.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Class_Action.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Class_Action
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Class_Action.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Class_Action.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Class_Action.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Company_Meeting.EventID,
'Company Meeting' as Event,
evf_Company_Meeting.Country_of_Incorporation as CntryofIncorp,
evf_Company_Meeting.Issuer_Name as IssuerName,
evf_Company_Meeting.ISIN,
evf_Company_Meeting.Sedol,
evf_Company_Meeting.us_code as USCode,
evf_Company_Meeting.Local_Code LocalCode,
evf_Company_Meeting.Exchange_code as ExchgCD,
evf_Company_Meeting.security_description as SecurityDesc,
evf_Company_Meeting.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Company_Meeting.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Company_Meeting
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Company_Meeting.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Company_Meeting.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Company_Meeting.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Consolidation.EventID,
'Consolidation' as Event,
evf_Consolidation.Country_of_Incorporation as CntryofIncorp,
evf_Consolidation.Issuer_Name as IssuerName,
evf_Consolidation.ISIN,
evf_Consolidation.Sedol,
evf_Consolidation.us_code as USCode,
evf_Consolidation.Local_Code LocalCode,
evf_Consolidation.Exchange_code as ExchgCD,
evf_Consolidation.security_description as SecurityDesc,
evf_Consolidation.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Consolidation.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Consolidation
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Consolidation.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Consolidation.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Consolidation.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Currency_Redenomination.EventID,
'Currency Redenomination' as Event,
evf_Currency_Redenomination.Country_of_Incorporation as CntryofIncorp,
evf_Currency_Redenomination.Issuer_Name as IssuerName,
evf_Currency_Redenomination.ISIN,
evf_Currency_Redenomination.Sedol,
evf_Currency_Redenomination.us_code as USCode,
evf_Currency_Redenomination.Local_Code LocalCode,
evf_Currency_Redenomination.Exchange_code as ExchgCD,
evf_Currency_Redenomination.security_description as SecurityDesc,
evf_Currency_Redenomination.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Currency_Redenomination.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Currency_Redenomination
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Currency_Redenomination.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Currency_Redenomination.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Currency_Redenomination.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Demerger.EventID,
'Demerger' as Event,
evf_Demerger.Country_of_Incorporation as CntryofIncorp,
evf_Demerger.Issuer_Name as IssuerName,
evf_Demerger.ISIN,
evf_Demerger.Sedol,
evf_Demerger.us_code as USCode,
evf_Demerger.Local_Code LocalCode,
evf_Demerger.Exchange_code as ExchgCD,
evf_Demerger.security_description as SecurityDesc,
evf_Demerger.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Demerger.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Demerger
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Demerger.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Demerger.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Demerger.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Depository_Receipt_Change.EventID,
'Depository Receipt Change' as Event,
evf_Depository_Receipt_Change.Country_of_Incorporation as CntryofIncorp,
evf_Depository_Receipt_Change.Issuer_Name as IssuerName,
evf_Depository_Receipt_Change.ISIN,
evf_Depository_Receipt_Change.Sedol,
evf_Depository_Receipt_Change.us_code as USCode,
evf_Depository_Receipt_Change.Local_Code LocalCode,
evf_Depository_Receipt_Change.Exchange_code as ExchgCD,
evf_Depository_Receipt_Change.security_description as SecurityDesc,
evf_Depository_Receipt_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Depository_Receipt_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Depository_Receipt_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Depository_Receipt_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Depository_Receipt_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Depository_Receipt_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Distribution.EventID,
'Distribution' as Event,
evf_Distribution.Country_of_Incorporation as CntryofIncorp,
evf_Distribution.Issuer_Name as IssuerName,
evf_Distribution.ISIN,
evf_Distribution.Sedol,
evf_Distribution.us_code as USCode,
evf_Distribution.Local_Code LocalCode,
evf_Distribution.Exchange_code as ExchgCD,
evf_Distribution.security_description as SecurityDesc,
evf_Distribution.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Distribution.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Distribution
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Distribution.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Distribution.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Distribution.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Divestment.EventID,
'Divestment' as Event,
evf_Divestment.Country_of_Incorporation as CntryofIncorp,
evf_Divestment.Issuer_Name as IssuerName,
evf_Divestment.ISIN,
evf_Divestment.Sedol,
evf_Divestment.us_code as USCode,
evf_Divestment.Local_Code LocalCode,
evf_Divestment.Exchange_code as ExchgCD,
evf_Divestment.security_description as SecurityDesc,
evf_Divestment.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Divestment.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Divestment
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Divestment.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Divestment.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Divestment.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Dividend.EventID,
'Dividend' as Event,
evf_Dividend.Country_of_Incorporation as CntryofIncorp,
evf_Dividend.Issuer_Name as IssuerName,
evf_Dividend.ISIN,
evf_Dividend.Sedol,
evf_Dividend.us_code as USCode,
evf_Dividend.Local_Code LocalCode,
evf_Dividend.Exchange_code as ExchgCD,
evf_Dividend.security_description as SecurityDesc,
evf_Dividend.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Dividend.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Dividend
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Dividend.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Dividend.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Dividend.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Dividend_Reinvestment_Plan.EventID,
'Dividend Reinvestment Plan' as Event,
evf_Dividend_Reinvestment_Plan.Country_of_Incorporation as CntryofIncorp,
evf_Dividend_Reinvestment_Plan.Issuer_Name as IssuerName,
evf_Dividend_Reinvestment_Plan.ISIN,
evf_Dividend_Reinvestment_Plan.Sedol,
evf_Dividend_Reinvestment_Plan.us_code as USCode,
evf_Dividend_Reinvestment_Plan.Local_Code LocalCode,
evf_Dividend_Reinvestment_Plan.Exchange_code as ExchgCD,
evf_Dividend_Reinvestment_Plan.security_description as SecurityDesc,
evf_Dividend_Reinvestment_Plan.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Dividend_Reinvestment_Plan.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Dividend_Reinvestment_Plan
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Dividend_Reinvestment_Plan.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Dividend_Reinvestment_Plan.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Dividend_Reinvestment_Plan.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Dividend_UnitTrust.EventID,
'Dividend Unit Trust' as Event,
evf_Dividend_UnitTrust.Country_of_Incorporation as CntryofIncorp,
evf_Dividend_UnitTrust.Issuer_Name as IssuerName,
evf_Dividend_UnitTrust.ISIN,
evf_Dividend_UnitTrust.Sedol,
evf_Dividend_UnitTrust.us_code as USCode,
evf_Dividend_UnitTrust.Local_Code LocalCode,
evf_Dividend_UnitTrust.Exchange_code as ExchgCD,
evf_Dividend_UnitTrust.security_description as SecurityDesc,
evf_Dividend_UnitTrust.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Dividend_UnitTrust.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Dividend_UnitTrust
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Dividend_UnitTrust.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Dividend_UnitTrust.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Dividend_UnitTrust.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Entitlement.EventID,
'Entitlement' as Event,
evf_Entitlement.Country_of_Incorporation as CntryofIncorp,
evf_Entitlement.Issuer_Name as IssuerName,
evf_Entitlement.ISIN,
evf_Entitlement.Sedol,
evf_Entitlement.us_code as USCode,
evf_Entitlement.Local_Code LocalCode,
evf_Entitlement.Exchange_code as ExchgCD,
evf_Entitlement.security_description as SecurityDesc,
evf_Entitlement.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Entitlement.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Entitlement
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Entitlement.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Entitlement.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Entitlement.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Financial_Year_Change.EventID,
'Financial Year Change' as Event,
evf_Financial_Year_Change.Country_of_Incorporation as CntryofIncorp,
evf_Financial_Year_Change.Issuer_Name as IssuerName,
evf_Financial_Year_Change.ISIN,
evf_Financial_Year_Change.Sedol,
evf_Financial_Year_Change.us_code as USCode,
evf_Financial_Year_Change.Local_Code LocalCode,
evf_Financial_Year_Change.Exchange_code as ExchgCD,
evf_Financial_Year_Change.security_description as SecurityDesc,
evf_Financial_Year_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Financial_Year_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Financial_Year_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Financial_Year_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Financial_Year_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Financial_Year_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Franking.EventID,
'Franking' as Event,
evf_Franking.Country_of_Incorporation as CntryofIncorp,
evf_Franking.Issuer_Name as IssuerName,
evf_Franking.ISIN,
evf_Franking.Sedol,
evf_Franking.us_code as USCode,
evf_Franking.Local_Code LocalCode,
evf_Franking.Exchange_code as ExchgCD,
evf_Franking.security_description as SecurityDesc,
evf_Franking.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Franking.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Franking
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Franking.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Franking.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Franking.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Incorporation_Change.EventID,
'Incorporation Change' as Event,
evf_Incorporation_Change.Country_of_Incorporation as CntryofIncorp,
evf_Incorporation_Change.Issuer_Name as IssuerName,
evf_Incorporation_Change.ISIN,
evf_Incorporation_Change.Sedol,
evf_Incorporation_Change.us_code as USCode,
evf_Incorporation_Change.Local_Code LocalCode,
evf_Incorporation_Change.Exchange_code as ExchgCD,
evf_Incorporation_Change.security_description as SecurityDesc,
evf_Incorporation_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Incorporation_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Incorporation_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Incorporation_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Incorporation_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Incorporation_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_International_Code_Change.EventID,
'International Code Change' as Event,
evf_International_Code_Change.Country_of_Incorporation as CntryofIncorp,
evf_International_Code_Change.Issuer_Name as IssuerName,
evf_International_Code_Change.ISIN,
evf_International_Code_Change.Sedol,
evf_International_Code_Change.us_code as USCode,
evf_International_Code_Change.Local_Code LocalCode,
evf_International_Code_Change.Exchange_code as ExchgCD,
evf_International_Code_Change.security_description as SecurityDesc,
evf_International_Code_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_International_Code_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_International_Code_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_International_Code_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_International_Code_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_International_Code_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Issuer_Name_Change.EventID,
'Issuer Name Change' as Event,
evf_Issuer_Name_Change.Country_of_Incorporation as CntryofIncorp,
evf_Issuer_Name_Change.Issuer_Name as IssuerName,
evf_Issuer_Name_Change.ISIN,
evf_Issuer_Name_Change.Sedol,
evf_Issuer_Name_Change.us_code as USCode,
evf_Issuer_Name_Change.Local_Code LocalCode,
evf_Issuer_Name_Change.Exchange_code as ExchgCD,
evf_Issuer_Name_Change.security_description as SecurityDesc,
evf_Issuer_Name_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Issuer_Name_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Issuer_Name_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Issuer_Name_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Issuer_Name_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Issuer_Name_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Liquidation.EventID,
'Liquidation' as Event,
evf_Liquidation.Country_of_Incorporation as CntryofIncorp,
evf_Liquidation.Issuer_Name as IssuerName,
evf_Liquidation.ISIN,
evf_Liquidation.Sedol,
evf_Liquidation.us_code as USCode,
evf_Liquidation.Local_Code LocalCode,
evf_Liquidation.Exchange_code as ExchgCD,
evf_Liquidation.security_description as SecurityDesc,
evf_Liquidation.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Liquidation.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Liquidation
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Liquidation.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Liquidation.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Liquidation.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Listing_Status_Change.EventID,
'Listing Status Change' as Event,
evf_Listing_Status_Change.Country_of_Incorporation as CntryofIncorp,
evf_Listing_Status_Change.Issuer_Name as IssuerName,
evf_Listing_Status_Change.ISIN,
evf_Listing_Status_Change.Sedol,
evf_Listing_Status_Change.us_code as USCode,
evf_Listing_Status_Change.Local_Code LocalCode,
evf_Listing_Status_Change.Exchange_code as ExchgCD,
evf_Listing_Status_Change.security_description as SecurityDesc,
evf_Listing_Status_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Listing_Status_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Listing_Status_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Listing_Status_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Listing_Status_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Listing_Status_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Local_Code_Change.EventID,
'Local Code Change' as Event,
evf_Local_Code_Change.Country_of_Incorporation as CntryofIncorp,
evf_Local_Code_Change.Issuer_Name as IssuerName,
evf_Local_Code_Change.ISIN,
evf_Local_Code_Change.Sedol,
evf_Local_Code_Change.us_code as USCode,
evf_Local_Code_Change.Local_Code LocalCode,
evf_Local_Code_Change.Exchange_code as ExchgCD,
evf_Local_Code_Change.security_description as SecurityDesc,
evf_Local_Code_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Local_Code_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Local_Code_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Local_Code_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Local_Code_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Local_Code_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Lot_Change.EventID,
'Lot Change' as Event,
evf_Lot_Change.Country_of_Incorporation as CntryofIncorp,
evf_Lot_Change.Issuer_Name as IssuerName,
evf_Lot_Change.ISIN,
evf_Lot_Change.Sedol,
evf_Lot_Change.us_code as USCode,
evf_Lot_Change.Local_Code LocalCode,
evf_Lot_Change.Exchange_code as ExchgCD,
evf_Lot_Change.security_description as SecurityDesc,
evf_Lot_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Lot_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Lot_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Lot_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Lot_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Lot_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Merger.EventID,
'Merger' as Event,
evf_Merger.Country_of_Incorporation as CntryofIncorp,
evf_Merger.Issuer_Name as IssuerName,
evf_Merger.ISIN,
evf_Merger.Sedol,
evf_Merger.us_code as USCode,
evf_Merger.Local_Code LocalCode,
evf_Merger.Exchange_code as ExchgCD,
evf_Merger.security_description as SecurityDesc,
evf_Merger.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Merger.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Merger
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Merger.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Merger.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Merger.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_New_Listing.EventID,
'New Listing' as Event,
evf_New_Listing.Country_of_Incorporation as CntryofIncorp,
evf_New_Listing.Issuer_Name as IssuerName,
evf_New_Listing.ISIN,
evf_New_Listing.Sedol,
evf_New_Listing.us_code as USCode,
evf_New_Listing.Local_Code LocalCode,
evf_New_Listing.Exchange_code as ExchgCD,
evf_New_Listing.security_description as SecurityDesc,
evf_New_Listing.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_New_Listing.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_New_Listing
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_New_Listing.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_New_Listing.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_New_Listing.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Odd_Lot_Offer.EventID,
'Odd Lot Offer' as Event,
evf_Odd_Lot_Offer.Country_of_Incorporation as CntryofIncorp,
evf_Odd_Lot_Offer.Issuer_Name as IssuerName,
evf_Odd_Lot_Offer.ISIN,
evf_Odd_Lot_Offer.Sedol,
evf_Odd_Lot_Offer.us_code as USCode,
evf_Odd_Lot_Offer.Local_Code LocalCode,
evf_Odd_Lot_Offer.Exchange_code as ExchgCD,
evf_Odd_Lot_Offer.security_description as SecurityDesc,
evf_Odd_Lot_Offer.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Odd_Lot_Offer.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Odd_Lot_Offer
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Odd_Lot_Offer.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Odd_Lot_Offer.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Odd_Lot_Offer.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Parvalue_Redenomination.EventID,
'Parvalue Redenomination' as Event,
evf_Parvalue_Redenomination.Country_of_Incorporation as CntryofIncorp,
evf_Parvalue_Redenomination.Issuer_Name as IssuerName,
evf_Parvalue_Redenomination.ISIN,
evf_Parvalue_Redenomination.Sedol,
evf_Parvalue_Redenomination.us_code as USCode,
evf_Parvalue_Redenomination.Local_Code LocalCode,
evf_Parvalue_Redenomination.Exchange_code as ExchgCD,
evf_Parvalue_Redenomination.security_description as SecurityDesc,
evf_Parvalue_Redenomination.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Parvalue_Redenomination.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Parvalue_Redenomination
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Parvalue_Redenomination.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Parvalue_Redenomination.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Parvalue_Redenomination.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Preference_Conversion.EventID,
'Preference Conversion' as Event,
evf_Preference_Conversion.Country_of_Incorporation as CntryofIncorp,
evf_Preference_Conversion.Issuer_Name as IssuerName,
evf_Preference_Conversion.ISIN,
evf_Preference_Conversion.Sedol,
evf_Preference_Conversion.us_code as USCode,
evf_Preference_Conversion.Local_Code LocalCode,
evf_Preference_Conversion.Exchange_code as ExchgCD,
evf_Preference_Conversion.security_description as SecurityDesc,
evf_Preference_Conversion.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Preference_Conversion.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Preference_Conversion
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Preference_Conversion.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Preference_Conversion.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Preference_Conversion.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Preference_Redemption.EventID,
'Preference Redemption' as Event,
evf_Preference_Redemption.Country_of_Incorporation as CntryofIncorp,
evf_Preference_Redemption.Issuer_Name as IssuerName,
evf_Preference_Redemption.ISIN,
evf_Preference_Redemption.Sedol,
evf_Preference_Redemption.us_code as USCode,
evf_Preference_Redemption.Local_Code LocalCode,
evf_Preference_Redemption.Exchange_code as ExchgCD,
evf_Preference_Redemption.security_description as SecurityDesc,
evf_Preference_Redemption.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Preference_Redemption.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Preference_Redemption
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Preference_Redemption.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Preference_Redemption.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Preference_Redemption.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Preferential_Offer.EventID,
'Preferential Offer' as Event,
evf_Preferential_Offer.Country_of_Incorporation as CntryofIncorp,
evf_Preferential_Offer.Issuer_Name as IssuerName,
evf_Preferential_Offer.ISIN,
evf_Preferential_Offer.Sedol,
evf_Preferential_Offer.us_code as USCode,
evf_Preferential_Offer.Local_Code LocalCode,
evf_Preferential_Offer.Exchange_code as ExchgCD,
evf_Preferential_Offer.security_description as SecurityDesc,
evf_Preferential_Offer.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Preferential_Offer.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Preferential_Offer
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Preferential_Offer.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Preferential_Offer.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Preferential_Offer.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Purchase_Offer.EventID,
'Purchase Offer' as Event,
evf_Purchase_Offer.Country_of_Incorporation as CntryofIncorp,
evf_Purchase_Offer.Issuer_Name as IssuerName,
evf_Purchase_Offer.ISIN,
evf_Purchase_Offer.Sedol,
evf_Purchase_Offer.us_code as USCode,
evf_Purchase_Offer.Local_Code LocalCode,
evf_Purchase_Offer.Exchange_code as ExchgCD,
evf_Purchase_Offer.security_description as SecurityDesc,
evf_Purchase_Offer.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Purchase_Offer.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Purchase_Offer
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Purchase_Offer.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Purchase_Offer.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Purchase_Offer.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Return_of_Capital.EventID,
'Return of Capital' as Event,
evf_Return_of_Capital.Country_of_Incorporation as CntryofIncorp,
evf_Return_of_Capital.Issuer_Name as IssuerName,
evf_Return_of_Capital.ISIN,
evf_Return_of_Capital.Sedol,
evf_Return_of_Capital.us_code as USCode,
evf_Return_of_Capital.Local_Code LocalCode,
evf_Return_of_Capital.Exchange_code as ExchgCD,
evf_Return_of_Capital.security_description as SecurityDesc,
evf_Return_of_Capital.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Return_of_Capital.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Return_of_Capital
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Return_of_Capital.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Return_of_Capital.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Return_of_Capital.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Rights.EventID,
'Rights' as Event,
evf_Rights.Country_of_Incorporation as CntryofIncorp,
evf_Rights.Issuer_Name as IssuerName,
evf_Rights.ISIN,
evf_Rights.Sedol,
evf_Rights.us_code as USCode,
evf_Rights.Local_Code LocalCode,
evf_Rights.Exchange_code as ExchgCD,
evf_Rights.security_description as SecurityDesc,
evf_Rights.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Rights.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Rights
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Rights.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Rights.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Rights.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Security_Description_Change.EventID,
'Security Description Change' as Event,
evf_Security_Description_Change.Country_of_Incorporation as CntryofIncorp,
evf_Security_Description_Change.Issuer_Name as IssuerName,
evf_Security_Description_Change.ISIN,
evf_Security_Description_Change.Sedol,
evf_Security_Description_Change.us_code as USCode,
evf_Security_Description_Change.Local_Code LocalCode,
evf_Security_Description_Change.Exchange_code as ExchgCD,
evf_Security_Description_Change.security_description as SecurityDesc,
evf_Security_Description_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Security_Description_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Security_Description_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Security_Description_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Security_Description_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Security_Description_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Security_Reclassification.EventID,
'Security Reclassification' as Event,
evf_Security_Reclassification.Country_of_Incorporation as CntryofIncorp,
evf_Security_Reclassification.Issuer_Name as IssuerName,
evf_Security_Reclassification.ISIN,
evf_Security_Reclassification.Sedol,
evf_Security_Reclassification.us_code as USCode,
evf_Security_Reclassification.Local_Code LocalCode,
evf_Security_Reclassification.Exchange_code as ExchgCD,
evf_Security_Reclassification.security_description as SecurityDesc,
evf_Security_Reclassification.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Security_Reclassification.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Security_Reclassification
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Security_Reclassification.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Security_Reclassification.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Security_Reclassification.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Security_Swap.EventID,
'Security Swap' as Event,
evf_Security_Swap.Country_of_Incorporation as CntryofIncorp,
evf_Security_Swap.Issuer_Name as IssuerName,
evf_Security_Swap.ISIN,
evf_Security_Swap.Sedol,
evf_Security_Swap.us_code as USCode,
evf_Security_Swap.Local_Code LocalCode,
evf_Security_Swap.Exchange_code as ExchgCD,
evf_Security_Swap.security_description as SecurityDesc,
evf_Security_Swap.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Security_Swap.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Security_Swap
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Security_Swap.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Security_Swap.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Security_Swap.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Sedol_Change.EventID,
'Sedol Change' as Event,
evf_Sedol_Change.Country_of_Incorporation as CntryofIncorp,
evf_Sedol_Change.Issuer_Name as IssuerName,
evf_Sedol_Change.ISIN,
evf_Sedol_Change.Sedol,
evf_Sedol_Change.us_code as USCode,
evf_Sedol_Change.Local_Code LocalCode,
evf_Sedol_Change.Exchange_code as ExchgCD,
evf_Sedol_Change.security_description as SecurityDesc,
evf_Sedol_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Sedol_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Sedol_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Sedol_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Sedol_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Sedol_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Shares_Outstanding_Change.EventID,
'Shares Outstanding Change' as Event,
evf_Shares_Outstanding_Change.Country_of_Incorporation as CntryofIncorp,
evf_Shares_Outstanding_Change.Issuer_Name as IssuerName,
evf_Shares_Outstanding_Change.ISIN,
evf_Shares_Outstanding_Change.Sedol,
evf_Shares_Outstanding_Change.us_code as USCode,
evf_Shares_Outstanding_Change.Local_Code LocalCode,
evf_Shares_Outstanding_Change.Exchange_code as ExchgCD,
evf_Shares_Outstanding_Change.security_description as SecurityDesc,
evf_Shares_Outstanding_Change.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Shares_Outstanding_Change.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Shares_Outstanding_Change
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Shares_Outstanding_Change.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Shares_Outstanding_Change.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Shares_Outstanding_Change.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Subdivision.EventID,
'Subdivision' as Event,
evf_Subdivision.Country_of_Incorporation as CntryofIncorp,
evf_Subdivision.Issuer_Name as IssuerName,
evf_Subdivision.ISIN,
evf_Subdivision.Sedol,
evf_Subdivision.us_code as USCode,
evf_Subdivision.Local_Code LocalCode,
evf_Subdivision.Exchange_code as ExchgCD,
evf_Subdivision.security_description as SecurityDesc,
evf_Subdivision.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Subdivision.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Subdivision
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Subdivision.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Subdivision.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Subdivision.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)

union

select distinct
evf_Takeover.EventID,
'Takeover' as Event,
evf_Takeover.Country_of_Incorporation as CntryofIncorp,
evf_Takeover.Issuer_Name as IssuerName,
evf_Takeover.ISIN,
evf_Takeover.Sedol,
evf_Takeover.us_code as USCode,
evf_Takeover.Local_Code LocalCode,
evf_Takeover.Exchange_code as ExchgCD,
evf_Takeover.security_description as SecurityDesc,
evf_Takeover.SecID,
SunFact_WFI_Fields.SecID as SecID_2,
SunFact_WFI_Fields.ISIN as ISIN_2,
evf_Takeover.IssID,
SunFact_WFI_Fields.IssID as IssID_2,
SunFact_WFI_Fields.MaturityStructure,
SunFact_WFI_Fields.SectyCD,
SunFact_WFI_Fields.SecurityDesc,
SunFact_WFI_Fields.IssuerName
from wca2.evf_Takeover
left outer join portfolio.SunFact_WFI_Fields on wca2.evf_Takeover.issid = portfolio.SunFact_WFI_Fields.issid
where
evf_Takeover.isin in(select mycode from portfolio.SunFact_isin)
and wca2.evf_Takeover.changed > (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
