--filepath=O:\Datafeed\Debt\SunFact\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ISIN
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=
--fieldheaders=N
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct 
#SunFact_issid.mycode as IssID,
scmst.ISIN
#bond.MaturityStructure,
#scmst.SectyCD,
#scmst.SecurityDesc,
#issur.IssuerName
from portfolio.SunFact_issid
left outer join wca.scmst on portfolio.SunFact_issid.mycode = wca.scmst.issid
left outer join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bond.MaturityStructure = 'C'
and wca.scmst.isin <> ''
and wca.scmst.actflag <> 'D'
and wca.scmst.StatusFlag <> 'I';