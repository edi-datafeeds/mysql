--filenameprefix=
--filename=
--filenamesql=SELECT DATE_FORMAT(now(), '%Y-%m')
--fileextension=.txt
--suffix=_yield_full
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\225\full\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct
wca.scmst.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.issur.IssID,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.BondType,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.IssueDate,
wca.bond.OutstandingAmount,
wca.bond.SeniorJunior,
wca.bond.Subordinate,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.FirstCouponDate,
wca.bond.InterestAccrualConvention,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.Perpetual,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
bondsigma.refpricefull.Callput,
bondsigma.refpricefull.CpType,
bondsigma.refpricefull.CpoptID,
DATE_FORMAT(bondsigma.refpricefull.FromDate, '%Y-%m-%d') as CpFromDate,
DATE_FORMAT(bondsigma.refpricefull.ToDate, '%Y-%m-%d') as CpToDate,
bondsigma.refpricefull.CpCurrency,
bondsigma.refpricefull.CpPrice,
bondsigma.refpricefull.CpPriceAsPercent,
DATE_FORMAT(bondsigma.calcout.SettlementDate, '%Y-%m-%d') as SettlementDate,
wca.scmst.PrimaryExchgCD,
case when bondsigma.refpricefull.ExchgCD<>''
     then bondsigma.refpricefull.ExchgCD
     else wca.scexh.exchgcd
     end as ExchgCD,
case when bondsigma.refpricefull.LocalCode<>''
     then bondsigma.refpricefull.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when bondsigma.refpricefull.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when bondsigma.refpricefull.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(bondsigma.refpricefull.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
bondsigma.refpricefull.ClosingPrice,
bondsigma.refpricefull.PriceCurrency,
bondsigma.calcout.AccruedInterest,
bondsigma.calcout.YieldtoMaturity,
bondsigma.calcout.YieldtoCall,
bondsigma.calcout.YieldtoPut,
bondsigma.calcout.ModifiedDuration,
bondsigma.calcout.EffectiveDuration,
bondsigma.calcout.MacaulayDuration,
bondsigma.calcout.Convexity
from bondsigma.refpricefull
inner join wca.scmst on bondsigma.refpricefull.secid=wca.scmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join bondsigma.calcout on bondsigma.refpricefull.secid=bondsigma.calcout.secid
                                    and bondsigma.refpricefull.exchgcd=bondsigma.calcout.exchgcd
                                    and bondsigma.refpricefull.pricecurrency=bondsigma.calcout.pricecurrency
left outer join wca.scexh on bondsigma.refpricefull.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D' and wca.scexh.localcode<>'';

