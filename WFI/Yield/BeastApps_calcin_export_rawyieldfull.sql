--filepath=O:\Datafeed\Bespoke\BeastApps\Calcin\
--filenameprefix=EDI_BeastApps_
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.csv
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\BeastApps\Calcin\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
priceshist.refpricefull.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
-- ###case when wca.bond.InterestRate = '' or wca.bond.InterestRate is null then wca.intpy.intrate else wca.bond.InterestRate end as InterestRate2,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.refpricefull.Callput,
priceshist.refpricefull.CpType,
priceshist.refpricefull.CpoptID,
DATE_FORMAT(priceshist.refpricefull.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(priceshist.refpricefull.ToDate, '%Y-%m-%d') as ToDate,
priceshist.refpricefull.CpCurrency,
case when priceshist.refpricefull.CpPrice='0' then '' else priceshist.refpricefull.CpPrice end as CpPrice,
priceshist.refpricefull.CpPriceAsPercent,
wca.bond.IssuePrice,
wca.bond.IssueDate,
wca.bond.Perpetual,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,
priceshist.refpricefull.ExchgCD,
priceshist.refpricefull.LocalCode,
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
case when priceshist.refpricefull.LastTradeDate='0000-00-00' then null else DATE_FORMAT(priceshist.refpricefull.LastTradeDate, '%Y-%m-%d') end as LastTradeDate,
priceshist.refpricefull.ClosingPrice,
priceshist.refpricefull.Open,
priceshist.refpricefull.High,
priceshist.refpricefull.Low,
priceshist.refpricefull.Mid,
priceshist.refpricefull.Ask,
priceshist.refpricefull.Bid,
priceshist.refpricefull.BidSize,
priceshist.refpricefull.AskSize,
priceshist.refpricefull.TradedVolume,
priceshist.refpricefull.VolFlag,
priceshist.refpricefull.PriceCurrency,
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity,
'' as Yield,
'' as YieldToWorst,
'' as KeyRateDuration
from priceshist.refpricefull
inner join wca.scmst on priceshist.refpricefull.secid=wca.scmst.secid
inner join wca.issur on wca.scmst.issid=wca.issur.issid
inner join wca.bond on wca.scmst.secid=wca.bond.secid
-- ###inner join wca.rd on wca.bond.secid = wca.rd.secid
-- ###left outer join wca.int_my on wca.rd.rdid=wca.int_my.rdid
-- ###inner join wca.intpy on wca.int_my.rdid = wca.intpy.rdid
where
-- wca.bond.interestBasis<>'FXD'
-- wca.bond.callable<>'Y'
-- and wca.bond.puttable<>'Y'
priceshist.refpricefull.cpoptid is null
and priceshist.refpricefull.pricecurrency <> ''
and priceshist.refpricefull.closingprice <> ''
and priceshist.refpricefull.closingprice <> '0'
and priceshist.refpricefull.closingprice is not null
-- ### wca.intpy.rdid =
-- ###(select subx.rdid
-- ###from wca.exdt as subx
-- ###inner join wca.rd as subr on subx.rdid=subr.rdid 
-- ###where subr.secid = wca.rd.secid
-- ###order by subx.paydate desc limit 1)
-- group by wca.bond.secid
-- ###order by wca.bond.secid desc
-- wca.scmst.statusflag <> 'I' 