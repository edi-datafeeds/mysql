--filenameprefix=
--filename=
--filenamesql=SELECT DATE_FORMAT(now(), '%Y-%m')
--fileextension=.txt
--suffix=_yield_990
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\990\full\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct
wca.scmst.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.issur.IssID,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.BondType,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.IssueDate,
wca.bond.OutstandingAmount,
wca.bond.SeniorJunior,
wca.bond.Subordinate,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.FirstCouponDate,
wca.bond.InterestAccrualConvention,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.Perpetual,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyieldfull.Callput,
priceshist.rawyieldfull.CpType,
priceshist.rawyieldfull.CpoptID,
DATE_FORMAT(priceshist.rawyieldfull.FromDate, '%Y-%m-%d') as CpFromDate,
DATE_FORMAT(priceshist.rawyieldfull.ToDate, '%Y-%m-%d') as CpToDate,
priceshist.rawyieldfull.CpCurrency,
priceshist.rawyieldfull.CpPrice,
priceshist.rawyieldfull.CpPriceAsPercent,
DATE_FORMAT(priceshist.yieldcalc.SettlementDate, '%Y-%m-%d') as SettlementDate,
wca.scmst.PrimaryExchgCD,
case when priceshist.rawyieldfull.ExchgCD<>''
     then priceshist.rawyieldfull.ExchgCD
     else wca.scexh.exchgcd
     end as ExchgCD,
case when priceshist.rawyieldfull.LocalCode<>''
     then priceshist.rawyieldfull.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when priceshist.rawyieldfull.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when priceshist.rawyieldfull.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(priceshist.rawyieldfull.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
priceshist.rawyieldfull.ClosingPrice,
priceshist.rawyieldfull.PriceCurrency,
priceshist.yieldcalc.AccruedInterest,
priceshist.yieldcalc.YieldtoMaturity,
priceshist.yieldcalc.YieldtoCall,
priceshist.yieldcalc.YieldtoPut,
priceshist.yieldcalc.ModifiedDuration,
priceshist.yieldcalc.EffectiveDuration,
priceshist.yieldcalc.MacaulayDuration,
priceshist.yieldcalc.Convexity
from priceshist.rawyieldfull
inner join wca.scmst on priceshist.rawyieldfull.secid=wca.scmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join priceshist.yieldcalc on priceshist.rawyieldfull.secid=priceshist.yieldcalc.secid
                                    and priceshist.rawyieldfull.exchgcd=priceshist.yieldcalc.exchgcd
                                    and priceshist.rawyieldfull.pricecurrency=priceshist.yieldcalc.pricecurrency
left outer join wca.scexh on priceshist.rawyieldfull.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D' and wca.scexh.localcode<>''
where 
wca.scmst.isin in (select code from client.pfisin where accid = 990 and actflag<>'D')