delete from priceshist.rawyieldfull;
insert into priceshist.rawyieldfull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
null as Callput,
null as CpType,
null as CpoptID,
null as FromDate,
null as ToDate,
null as CpCurrency,
null as CpPrice,
null as CpPriceAsPercent,
prices.lasttrade.LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
cpopt.cpoptid is null
and scmst.statusflag<>'I'
and scmst.actflag<>'D';
insert ignore into priceshist.rawyieldfull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
prices.lasttrade.LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid = (select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.rawyieldfull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
prices.lasttrade.LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is not null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.rawyieldfull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
prices.lasttrade.LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is null and fromdate<now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.rawyieldfull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
prices.lasttrade.LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is not null and fromdate<now()
order by wca.cpopt.secid, fromdate limit 1);
delete from priceshist.yieldcalc_temp;
delete from priceshist.yieldcalc;