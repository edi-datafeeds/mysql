--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_yield
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\225\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
client.pfisin.code as Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.issur.IssID,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.BondType,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.IssueDate,
wca.bond.OutstandingAmount,
wca.bond.SeniorJunior,
wca.bond.Subordinate,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.FirstCouponDate,
wca.bond.InterestAccrualConvention,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.Perpetual,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyield.Callput,
priceshist.rawyield.CpType,
priceshist.rawyield.CpoptID,
DATE_FORMAT(priceshist.rawyield.FromDate, '%Y-%m-%d') as CpFromDate,
DATE_FORMAT(priceshist.rawyield.ToDate, '%Y-%m-%d') as CpToDate,
priceshist.rawyield.CpCurrency,
priceshist.rawyield.CpPrice,
priceshist.rawyield.CpPriceAsPercent,
DATE_FORMAT(priceshist.rawyield.SettlementDate, '%Y-%m-%d') as SettlementDate,
wca.scmst.PrimaryExchgCD,
priceshist.rawyield.ExchgCD,
priceshist.rawyield.LocalCode,
DATE_FORMAT(priceshist.rawyield.MktCloseDate, '%Y-%m-%d') as MktCloseDate,
DATE_FORMAT(priceshist.rawyield.LastTradeDate, '%Y-%m-%d') as LastTradeDate,
priceshist.rawyield.ClosingPrice,
priceshist.rawyield.PriceCurrency,
priceshist.yieldcalc.AccruedInterest,
priceshist.yieldcalc.YieldtoMaturity,
priceshist.yieldcalc.YieldtoCall,
priceshist.yieldcalc.YieldtoPut,
priceshist.yieldcalc.ModifiedDuration,
priceshist.yieldcalc.EffectiveDuration,
priceshist.yieldcalc.MacaulayDuration,
priceshist.yieldcalc.Convexity
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 225=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join priceshist.rawyield on wca.scmst.secid=priceshist.rawyield.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join priceshist.yieldcalc on priceshist.rawyield.mktclosedate=priceshist.yieldcalc.mktclosedate
                                    and priceshist.rawyield.secid=priceshist.yieldcalc.secid
                                    and priceshist.rawyield.exchgcd=priceshist.yieldcalc.exchgcd
                                    and priceshist.rawyield.pricecurrency=priceshist.yieldcalc.pricecurrency
where
priceshist.rawyield.mktclosedate=
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
or
priceshist.rawyield.secid is null;
