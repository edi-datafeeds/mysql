use wca;
DROP TABLE IF EXISTS `priceshist`.`refpricefull`;
CREATE TABLE  `priceshist`.`refpricefull` (
  `secid` int(10) NOT NULL DEFAULT '0',
  `ExchgCD` varchar(9) NOT NULL DEFAULT '',
  `PriceCurrency` char(3) NOT NULL DEFAULT '',
  `Isin` char(12) DEFAULT NULL,
  `Callput` char(1) DEFAULT NULL,
  `CpType` varchar(10) DEFAULT NULL,
  `CpoptID` int(10) DEFAULT NULL,
  `FromDate` datetime DEFAULT NULL,
  `ToDate` datetime DEFAULT NULL,
  `CpCurrency` char(3) DEFAULT NULL,
  `CpPrice` varchar(20) DEFAULT NULL,
  `CpPriceAsPercent` varchar(20) DEFAULT NULL,
  `LocalCode` varchar(80) NOT NULL DEFAULT '',
  `LastTradeDate` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ClosingPrice` varchar(20) DEFAULT NULL,
  `Open` varchar(20) DEFAULT NULL,
  `High` varchar(20) DEFAULT NULL,
  `Low` varchar(20) DEFAULT NULL,
  `Mid` varchar(20) DEFAULT NULL,
  `Ask` varchar(20) DEFAULT NULL,
  `Bid` varchar(20) DEFAULT NULL,
  `BidSize` varchar(20) DEFAULT NULL,
  `AskSize` varchar(20) DEFAULT NULL,
  `TradedVolume` varchar(20) DEFAULT NULL,
  `VolFlag` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`secid`,`ExchgCD`,`PriceCurrency`,`LocalCode`) USING BTREE,
  KEY `Index_2` (`secid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
insert ignore into priceshist.refpricefull
select
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
'' as Callput,
'' as CpType,
null as CpoptID,
null as FromDate,
null as ToDate,
'' as CpCurrency,
'' as CpPrice,
'' as CpPriceAsPercent,
case when prices.lasttrade.LocalCode is null then '' else prices.lasttrade.LocalCode end as LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice,
case when prices.lasttrade.Open='0' then '' else prices.lasttrade.Open end,
case when prices.lasttrade.High='0' then '' else prices.lasttrade.High end,
case when prices.lasttrade.Low='0' then '' else prices.lasttrade.Low end,
case when prices.lasttrade.Mid='0' then '' else prices.lasttrade.Mid end,
case when prices.lasttrade.Ask='0' then '' else prices.lasttrade.Ask end,
case when prices.lasttrade.Bid='0' then '' else prices.lasttrade.Bid end,
case when prices.lasttrade.BidSize='0' then '' else prices.lasttrade.BidSize end,
case when prices.lasttrade.AskSize='0' then '' else prices.lasttrade.AskSize end,
case when prices.lasttrade.TradedVolume='0' then '' else prices.lasttrade.TradedVolume end,
case when prices.lasttrade.VolFlag='0' then '' else prices.lasttrade.VolFlag end
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
cpopt.cpoptid is null
and scmst.statusflag<>'I'
and scmst.actflag<>'D';
insert ignore into priceshist.refpricefull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when prices.lasttrade.LocalCode is null then '' else prices.lasttrade.LocalCode end as LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice,
case when prices.lasttrade.Open='0' then '' else prices.lasttrade.Open end,
case when prices.lasttrade.High='0' then '' else prices.lasttrade.High end,
case when prices.lasttrade.Low='0' then '' else prices.lasttrade.Low end,
case when prices.lasttrade.Mid='0' then '' else prices.lasttrade.Mid end,
case when prices.lasttrade.Ask='0' then '' else prices.lasttrade.Ask end,
case when prices.lasttrade.Bid='0' then '' else prices.lasttrade.Bid end,
case when prices.lasttrade.BidSize='0' then '' else prices.lasttrade.BidSize end,
case when prices.lasttrade.AskSize='0' then '' else prices.lasttrade.AskSize end,
case when prices.lasttrade.TradedVolume='0' then '' else prices.lasttrade.TradedVolume end,
case when prices.lasttrade.VolFlag='0' then '' else prices.lasttrade.VolFlag end
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid = (select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.refpricefull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when prices.lasttrade.LocalCode is null then '' else prices.lasttrade.LocalCode end as LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice,
case when prices.lasttrade.Open='0' then '' else prices.lasttrade.Open end,
case when prices.lasttrade.High='0' then '' else prices.lasttrade.High end,
case when prices.lasttrade.Low='0' then '' else prices.lasttrade.Low end,
case when prices.lasttrade.Mid='0' then '' else prices.lasttrade.Mid end,
case when prices.lasttrade.Ask='0' then '' else prices.lasttrade.Ask end,
case when prices.lasttrade.Bid='0' then '' else prices.lasttrade.Bid end,
case when prices.lasttrade.BidSize='0' then '' else prices.lasttrade.BidSize end,
case when prices.lasttrade.AskSize='0' then '' else prices.lasttrade.AskSize end,
case when prices.lasttrade.TradedVolume='0' then '' else prices.lasttrade.TradedVolume end,
case when prices.lasttrade.VolFlag='0' then '' else prices.lasttrade.VolFlag end
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is not null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.refpricefull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when prices.lasttrade.LocalCode is null then '' else prices.lasttrade.LocalCode end as LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice,
case when prices.lasttrade.Open='0' then '' else prices.lasttrade.Open end,
case when prices.lasttrade.High='0' then '' else prices.lasttrade.High end,
case when prices.lasttrade.Low='0' then '' else prices.lasttrade.Low end,
case when prices.lasttrade.Mid='0' then '' else prices.lasttrade.Mid end,
case when prices.lasttrade.Ask='0' then '' else prices.lasttrade.Ask end,
case when prices.lasttrade.Bid='0' then '' else prices.lasttrade.Bid end,
case when prices.lasttrade.BidSize='0' then '' else prices.lasttrade.BidSize end,
case when prices.lasttrade.AskSize='0' then '' else prices.lasttrade.AskSize end,
case when prices.lasttrade.TradedVolume='0' then '' else prices.lasttrade.TradedVolume end,
case when prices.lasttrade.VolFlag='0' then '' else prices.lasttrade.VolFlag end
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is null and fromdate<now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.refpricefull
select distinct
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
wca.scmst.Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when prices.lasttrade.LocalCode is null then '' else prices.lasttrade.LocalCode end as LocalCode,
prices.lasttrade.PriceDate as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice,
case when prices.lasttrade.Open='0' then '' else prices.lasttrade.Open end,
case when prices.lasttrade.High='0' then '' else prices.lasttrade.High end,
case when prices.lasttrade.Low='0' then '' else prices.lasttrade.Low end,
case when prices.lasttrade.Mid='0' then '' else prices.lasttrade.Mid end,
case when prices.lasttrade.Ask='0' then '' else prices.lasttrade.Ask end,
case when prices.lasttrade.Bid='0' then '' else prices.lasttrade.Bid end,
case when prices.lasttrade.BidSize='0' then '' else prices.lasttrade.BidSize end,
case when prices.lasttrade.AskSize='0' then '' else prices.lasttrade.AskSize end,
case when prices.lasttrade.TradedVolume='0' then '' else prices.lasttrade.TradedVolume end,
case when prices.lasttrade.VolFlag='0' then '' else prices.lasttrade.VolFlag end
from wca.scmst
inner join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
inner join wca.cpopt on wca.bond.secid = wca.cpopt.secid
where
scmst.statusflag<>'I'
and scmst.actflag<>'D'
and cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and todate is not null and fromdate<now()
order by wca.cpopt.secid, fromdate limit 1);
delete from priceshist.yieldcalc_temp;
delete from priceshist.yieldcalc;