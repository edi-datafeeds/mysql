--filenameprefix=edi_morningstar_
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.csv
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\no_cull_feeds\calcin\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
priceshist.rawyield.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyield.Callput,
priceshist.rawyield.CpType,
priceshist.rawyield.CpoptID,
DATE_FORMAT(priceshist.rawyield.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(priceshist.rawyield.ToDate, '%Y-%m-%d') as ToDate,
priceshist.rawyield.CpCurrency,
case when priceshist.rawyield.CpPrice='0' then '' else priceshist.rawyield.CpPrice end as CpPrice,
priceshist.rawyield.CpPriceAsPercent,
DATE_FORMAT(priceshist.rawyield.SettlementDate, '%Y-%m-%d') as SettlementDate,
priceshist.rawyield.ExchgCD,
priceshist.rawyield.LocalCode,
DATE_FORMAT(priceshist.rawyield.MktCloseDate, '%Y-%m-%d') as MktCloseDate,
case when priceshist.rawyield.LastTradeDate='0000-00-00' then null else DATE_FORMAT(priceshist.rawyield.LastTradeDate, '%Y-%m-%d') end as LastTradeDate,
priceshist.rawyield.ClosingPrice,
priceshist.rawyield.PriceCurrency,
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity
from priceshist.rawyield
inner join wca.scmst on priceshist.rawyield.secid=wca.scmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
where
priceshist.rawyield.mktclosedate = (select cast(STR_TO_DATE(mktclosedate, '%Y-%m-%d') as char) from priceshist.rawyield order by mktclosedate desc limit 1)
and wca.bond.interestBasis='FXD'
and wca.bond.callable<>'Y'
and wca.bond.puttable<>'Y'
and priceshist.rawyield.cpoptid is null
and priceshist.rawyield.closingprice <> ''
and priceshist.rawyield.closingprice is not null;
