delete from priceshist.rawyield;
insert into priceshist.rawyield
select distinct
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
client.pfisin.code as Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as settlementdate,prices.lasttrade.LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and (client.pfisin.accid=226)
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
where
cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and cptype<>'KO'
and todate is null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1)
or cpopt.cpoptid is null;
insert ignore into priceshist.rawyield
select distinct
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
client.pfisin.code as Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as settlementdate,prices.lasttrade.LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and (client.pfisin.accid=226)
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
where
cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and cptype<>'KO'
and todate is not null and fromdate>now()
order by wca.cpopt.secid, fromdate limit 1);
insert ignore into priceshist.rawyield
select distinct
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
client.pfisin.code as Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as settlementdate,prices.lasttrade.LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and (client.pfisin.accid=226)
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
where
cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and cptype<>'KO'
and todate is null and fromdate<now()
order by wca.cpopt.secid, fromdate desc limit 1);
insert ignore into priceshist.rawyield
select distinct
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
wca.scmst.secid,
case when prices.lasttrade.ExchgCD is null then '' else prices.lasttrade.ExchgCD end as ExchgCD,
case when prices.lasttrade.Currency is null then '' else prices.lasttrade.Currency end as PriceCurrency,
client.pfisin.code as Isin,
wca.cpopt.Callput,
wca.cpopt.CpType,
wca.cpopt.CpoptID,
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.Currency as CpCurrency,
wca.cpopt.Price as CpPrice,
wca.cpopt.PriceAsPercent as CpPriceAsPercent,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as settlementdate,prices.lasttrade.LocalCode,
DATE_FORMAT(prices.lasttrade.PriceDate, '%Y-%m-%d') as LastTradeDate,
case when prices.lasttrade.Close='0' then '' else prices.lasttrade.Close end as ClosingPrice
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and (client.pfisin.accid=226)
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.cpopt on wca.bond.secid = wca.cpopt.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join prices.lasttrade on wca.scmst.secid=prices.lasttrade.SecID
where
cpopt.cpoptid =
(select cpoptid from wca.cpopt
where scmst.secid=wca.cpopt.secid
and cpopt.actflag<>'D'
and cptype<>'KO'
and todate is not null and fromdate<now()
order by wca.cpopt.secid, fromdate desc limit 1);
delete from priceshist.yieldcalc_temp;
delete from priceshist.yieldcalc;
