--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_yield
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\226\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
client.pfisin.code as Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA'
     then 'SUPRANATIONAL' when wca.issur.isstype=''
     then 'CORPORATE' else wca.issur.isstype
     end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyieldfull.Callput,
priceshist.rawyieldfull.CpType,
priceshist.rawyieldfull.CpoptID,
DATE_FORMAT(priceshist.rawyieldfull.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(priceshist.rawyieldfull.ToDate, '%Y-%m-%d') as ToDate,
priceshist.rawyieldfull.CpCurrency,
priceshist.rawyieldfull.CpPrice,
priceshist.rawyieldfull.CpPriceAsPercent,
case when priceshist.rawyieldfull.ClosingPrice<>'' and priceshist.rawyieldfull.ClosingPrice is not null
     then DATE_FORMAT(priceshist.rawyieldfull.SettlementDate, '%Y-%m-%d')
     else ''
     end as SettlementDate,
case when priceshist.rawyieldfull.ExchgCD<>'' and priceshist.rawyieldfull.ExchgCD is not null
     then priceshist.rawyieldfull.ExchgCD
     else wca.scmst.primaryexchgcd
     end as ExchgCD,
case when priceshist.rawyieldfull.LocalCode<>'' and priceshist.rawyieldfull.LocalCode is not null
     then priceshist.rawyieldfull.LocalCode
     else wca.scexh.LocalCode
     end as LocalCode,
case when priceshist.rawyieldfull.PriceCurrency<>''
     then (select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1)
     else ''
     end as MktCloseDate,
case when priceshist.rawyieldfull.LastTradeDate='0000-00-00'
     then null 
     else DATE_FORMAT(priceshist.rawyieldfull.LastTradeDate, '%Y-%m-%d')
     end as LastTradeDate,
priceshist.rawyieldfull.ClosingPrice,
priceshist.rawyieldfull.PriceCurrency,
priceshist.yieldcalc.AccruedInterest,
priceshist.yieldcalc.YieldtoMaturity,
priceshist.yieldcalc.YieldtoCall,
priceshist.yieldcalc.YieldtoPut,
priceshist.yieldcalc.ModifiedDuration,
priceshist.yieldcalc.EffectiveDuration,
priceshist.yieldcalc.MacaulayDuration,
priceshist.yieldcalc.Convexity,
wca.bond.BondType,
wca.bond.Subordinate,
wca.bond.MinimumDenomination,
wca.bond.DenominationMultiple,
wca.issur.IssID,
wca.issur.IndusID,
wca.issur.Shortname,
wca.issur.LegalName,
wca.issur.CntryofDom,
wca.issur.StateofDom,
wca.scmst.PrimaryExchgCD
from client.pfisin
inner join wca.scmst on client.pfisin.code=wca.scmst.isin and 226=client.pfisin.accid and 'D'<>client.pfisin.actflag
left outer join priceshist.rawyieldfull on wca.scmst.secid=priceshist.rawyieldfull.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
left outer join priceshist.yieldcalc on priceshist.rawyieldfull.secid=priceshist.yieldcalc.secid
                                    and priceshist.rawyieldfull.mktclosedate=priceshist.yieldcalc.mktclosedate
                                    and priceshist.rawyieldfull.exchgcd=priceshist.yieldcalc.exchgcd
                                    and priceshist.rawyieldfull.pricecurrency=priceshist.yieldcalc.pricecurrency
left outer join wca.scexh on priceshist.rawyieldfull.exchgcd='' and wca.scmst.secid = wca.scexh.secid and wca.scmst.primaryexchgcd = wca.scexh.exchgcd
                               and wca.scexh.actflag<>'D' and wca.scexh.liststatus<>'D';
