--filenameprefix=
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.txt
--suffix=_yield_full
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\225\full\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
wca.scmst.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.issur.IssID,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.BondType,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.IssueDate,
wca.bond.OutstandingAmount,
wca.bond.SeniorJunior,
wca.bond.Subordinate,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.FirstCouponDate,
wca.bond.InterestAccrualConvention,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.Perpetual,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyieldfull.Callput,
priceshist.rawyieldfull.CpType,
priceshist.rawyieldfull.CpoptID,
DATE_FORMAT(priceshist.rawyieldfull.FromDate, '%Y-%m-%d') as CpFromDate,
DATE_FORMAT(priceshist.rawyieldfull.ToDate, '%Y-%m-%d') as CpToDate,
priceshist.rawyieldfull.CpCurrency,
priceshist.rawyieldfull.CpPrice,
priceshist.rawyieldfull.CpPriceAsPercent,
'' as SettlementDate,
wca.scmst.PrimaryExchgCD,
priceshist.rawyieldfull.ExchgCD,
priceshist.rawyieldfull.LocalCode,
'' as MktCloseDate,
'' as LastTradeDate,
priceshist.rawyieldfull.ClosingPrice,
priceshist.rawyieldfull.PriceCurrency,
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity
from priceshist.rawyieldfull
inner join wca.scmst on priceshist.rawyieldfull.secid=wca.scmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
