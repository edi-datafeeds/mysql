--filenameprefix=edi_morningstar_
--filename=
--filenamesql=select cast(STR_TO_DATE(now(), '%Y-%m-%d') as char)
--fileextension=.csv
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\no_cull_feeds\calcin\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
priceshist.rawyieldfull.Isin,
wca.scmst.SecID,
wca.scmst.UsCode,
wca.issur.Issuername,
case when wca.issur.cntryofincorp='AA' then 'SUPRANATIONAL' when wca.issur.isstype='' then 'CORPORATE' else wca.issur.isstype end as DebtType,
wca.scmst.SecurityDesc,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.bond.curencd as DebtCurrency,
wca.bond.parvalue as NominalValue,
wca.bond.OutstandingAmount,
wca.bond.interestBasis as InterestType,
wca.bond.InterestRate,
wca.bond.InterestPaymentFrequency,
wca.bond.InterestAccrualConvention,
DATE_FORMAT(wca.bond.MaturityDate, '%Y-%m-%d') as MaturityDate,
wca.bond.MatPriceAsPercent,
wca.bond.FrnIndexBenchmark,
wca.bond.Markup,
wca.bond.MaturityStructure,
wca.bond.Callable,
wca.bond.Puttable,
priceshist.rawyieldfull.Callput,
priceshist.rawyieldfull.CpType,
priceshist.rawyieldfull.CpoptID,
DATE_FORMAT(priceshist.rawyieldfull.FromDate, '%Y-%m-%d') as FromDate,
DATE_FORMAT(priceshist.rawyieldfull.ToDate, '%Y-%m-%d') as ToDate,
priceshist.rawyieldfull.CpCurrency,
case when priceshist.rawyieldfull.CpPrice='0' then '' else priceshist.rawyieldfull.CpPrice end as CpPrice,
priceshist.rawyieldfull.CpPriceAsPercent,
case when wca.issur.cntryofincorp='AA' or wca.issur.isstype=''
     then (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Wednesday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Thursday'
           or dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 5 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day))))
     else (select if(dayname((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1))
           ='Friday',
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 3 day)),
           (select date_add((select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1),
           interval 1 day))))
     end as SettlementDate,
priceshist.rawyieldfull.ExchgCD,
priceshist.rawyieldfull.LocalCode,
(select cast(STR_TO_DATE(wca.tbl_opslog.feeddate, '%Y-%m-%d') as char) from wca.tbl_opslog where wca.tbl_opslog.seq=3 order by wca.tbl_opslog.feeddate desc limit 1) as MktCloseDate,
case when priceshist.rawyieldfull.LastTradeDate='0000-00-00' then null else DATE_FORMAT(priceshist.rawyieldfull.LastTradeDate, '%Y-%m-%d') end as LastTradeDate,
priceshist.rawyieldfull.ClosingPrice,
priceshist.rawyieldfull.PriceCurrency,
'' as AccruedInterest,
'' as YieldtoMaturity,
'' as YieldtoCall,
'' as YieldtoPut,
'' as ModifiedDuration,
'' as EffectiveDuration,
'' as MacaulayDuration,
'' as Convexity
from priceshist.rawyieldfull
inner join wca.scmst on priceshist.rawyieldfull.secid=wca.scmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.issur on wca.scmst.issid=wca.issur.issid
where
wca.bond.interestBasis='FXD'
and wca.bond.callable<>'Y'
and wca.bond.puttable<>'Y'
and priceshist.rawyieldfull.cpoptid is null
and priceshist.rawyieldfull.pricecurrency <> ''
and priceshist.rawyieldfull.closingprice <> ''
and priceshist.rawyieldfull.closingprice <> '0'
and priceshist.rawyieldfull.closingprice is not null;
