--filepath=O:\Datafeed\Deriv\deriv_trigger\
--filenameprefix=deriv_trigger_
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=
--fileheadertext=EDI_DERIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Temp\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT 
	eventcd, eventid,actflag, changed, created, secid, isin, uscode,
	issuername, cntryofincorp, sectycd, securitydesc, statusflag,
	primaryexchgcd,exchgcd, mic,localcode, liststatus, date1type, date1,
	date2type, date2, date3type, date3, date4type, date4,ticket_symbol,
	contract_name, contract_size, expiry_date 
FROM derivdes.excel_deriv_entry 
WHERE table_dt = CURDATE() 
-- - INTERVAL 1 DAY
;
