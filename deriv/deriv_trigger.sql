--filepath=O:\Datafeed\Deriv\deriv_trigger\
--filenameprefix=trg_
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%d%b%Y')
--fileextension=.csv
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=O:\Datafeed\Deriv\deriv_trigger\archive\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
SELECT 
	CONCAT('\"eventcd\"'), 
	CONCAT('\"eventid\"'),
	CONCAT('\"actflag\"'),
	CONCAT('\"changed\"'),
	CONCAT('\"created\"'),
	CONCAT('\"secid\"'),
	CONCAT('\"isin\"'),
	CONCAT('\"uscode\"'),
	CONCAT('\"issuername\"'),
	CONCAT('\"cntryofincorp\"'),
	CONCAT('\"sectycd\"'),
	CONCAT('\"securitydesc\"'),
	CONCAT('\"statusflag\"'),
	CONCAT('\"primaryexchgcd\"'),
	CONCAT('\"exchgcd\"'),
	CONCAT('\"mic\"'),
	CONCAT('\"localcode\"'),
	CONCAT('\"liststatus\"'),
	CONCAT('\"date1type\"'),
	CONCAT('\"date1\"'),
	CONCAT('\"date2type\"'),
	CONCAT('\"date2\"'),
	CONCAT('\"date3type\"'),
	CONCAT('\"date3\"'),
	CONCAT('\"date4type\"'),
	CONCAT('\"date4\"'),
	CONCAT('\"ticker_symbol\"'),
	CONCAT('\"contract_name\"'),
	CONCAT('\"contract_size\"'),
	CONCAT('\"expiry_date \"')
UNION ALL
SELECT 
	CONCAT('\"',eventcd,'\"') AS eventcd, 
	CONCAT('\"',eventid,'\"') AS eventid,
	CONCAT('\"',actflag,'\"') AS actflag,
	CONCAT('\"',changed,'\"') AS changed,
	CONCAT('\"',created,'\"') AS created,
	CONCAT('\"',secid,'\"') AS secid,
	CONCAT('\"',isin,'\"') AS isin,
	CONCAT('\"',uscode,'\"') AS uscode,
	CONCAT('\"',issuername,'\"') AS issuername,
	CONCAT('\"',cntryofincorp,'\"') AS cntryofincorp,
	CONCAT('\"',sectycd,'\"') AS sectycd,
	CONCAT('\"',securitydesc,'\"') AS securitydesc,
	CONCAT('\"',statusflag,'\"') AS statusflag,
	CONCAT('\"',primaryexchgcd,'\"') AS primaryexchgcd,
	CONCAT('\"',exchgcd,'\"') AS exchgcd,
	CONCAT('\"',mic,'\"') AS mic,
	CONCAT('\"',localcode,'\"') AS localcode,
	CONCAT('\"',liststatus,'\"') AS liststatus,
	CONCAT('\"',date1type,'\"') AS date1type,
	CONCAT('\"',date1,'\"') AS date1,
	CONCAT('\"',date2type,'\"') AS date2type,
	CONCAT('\"',date2,'\"') AS date2,
	CONCAT('\"',date3type,'\"') AS date3type,
	CONCAT('\"',date3,'\"') AS date3,
	CONCAT('\"',date4type,'\"') AS date4type,
	CONCAT('\"',date4,'\"') AS date4,
	CONCAT('\"',ticket_symbol,'\"') AS ticker_symbol,
	CONCAT('\"',contract_name,'\"') AS contract_name,
	CONCAT('\"',contract_size,'\"') AS contract_size,
	CONCAT('\"',expiry_date ,'\"') AS expiry_date
FROM derivdes.excel_deriv_entry 
WHERE table_dt >= CURDATE()  /*- INTERVAL 10 DAY */
;