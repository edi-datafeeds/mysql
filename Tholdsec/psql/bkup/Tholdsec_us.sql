-- hpx=select concat('EDI_THS_US_', DATE_FORMAT(max(feeddate), '%Y%m%d')) from wca.tbl_opslog where seq = 3;
-- fsx=_US_THS
-- eor=Select concat(char(13),char(10))
-- dtm=select '';
-- hdt=SELECT '';
-- arp=N:\No_Cull_Feeds\Tholdsec\

-- # 0

SELECT DISTINCT
	date_format(a.acttime, '%Y-%m-%d') AS FeedDate,
	a.Symbol,
	sm.Isin,
	sm.USCode,
	a.marketcat AS ExchgCD,
	iss.IssuerName,
	sm.SecurityDesc,
	sm.SecID
FROM
	client.tholdsec AS a
LEFT OUTER JOIN wca.scexh AS b ON a.symbol = b.localcode
AND (
	a.marketcat = b.exchgcd
	OR (
		a.marketcat = 'USNASD'
		AND b.exchgcd = 'USOTC'
	)
)
LEFT OUTER JOIN wca.scmst AS sm ON b.secid = sm.secid
LEFT OUTER JOIN wca.issur AS iss ON sm.issid = iss.issid
where a.feeddate = (select max(feeddate) from wca.tbl_opslog where seq = 3)-4
ORDER BY
	b.localcode;

