-- arc=y
-- arp=n:\bespoke\taniscott\ixcon\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_IXCON_Taniscott
-- fty=n
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_IXCON_Taniscott_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
select
ixcon.ixcon.actflag,
ixcon.ixcon.acttime,
ixcon.ixcon.effectivedate,
ixcon.ixcon.ixnamid,
ixcon.ixcon.secid,
wca.scmst.issid,
ixcon.ixcon.issuername,
ixcon.ixcon.securitydesc,
ixcon.ixcon.sectycd,
ixcon.ixcon.isin,
ixcon.ixcon.localcode,
ixcon.ixcon.onindex
from ixcon.ixcon
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
left outer join wca.scmst on ixcon.ixcon.secid = wca.scmst.secid
where
ixcon.ixcon.ixnamid=97
and ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()))
order by ixnamid, issuername;