--filepath=o:\datafeed\bespoke\oms_dag\ixcon\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.152
--suffix=_IXCON
--fileheadertext=EDI_IXCON_152_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\bespoke\oms_dag\ixcon\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
ixcon.ixcon.actflag,
ixcon.ixcon.acttime,
ixcon.ixcon.effectivedate,
ixcon.ixcon.ixnamid,
ixcon.ixcon.secid,
wca.scmst.issid,
ixcon.ixcon.issuername,
ixcon.ixcon.securitydesc,
ixcon.ixcon.sectycd,
ixcon.ixcon.isin,
ixcon.ixcon.localcode,
ixcon.ixcon.onindex
from ixcon.ixcon
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
left outer join wca.scmst on ixcon.ixcon.secid = wca.scmst.secid
where
(ixcon.ixcon.ixnamid=11
or ixcon.ixcon.ixnamid=16
or ixcon.ixcon.ixnamid=1
or ixcon.ixcon.ixnamid=6
or ixcon.ixcon.ixnamid=14
or ixcon.ixcon.ixnamid=15
or ixcon.ixcon.ixnamid=458
or ixcon.ixcon.ixnamid=459
or ixcon.ixcon.ixnamid=42)
and ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()))
order by ixnamid, issuername;