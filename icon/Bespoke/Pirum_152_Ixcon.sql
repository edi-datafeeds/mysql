--filepath=o:\datafeed\bespoke\pirum\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.152
--suffix=_IXCON
--fileheadertext=EDI_IXCON_152_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\bespoke\pirum\ixcon\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
ixcon.ixcon.actflag,
ixcon.ixcon.acttime,
ixcon.ixcon.effectivedate,
ixcon.ixcon.ixnamid,
ixcon.ixcon.secid,
wca.scmst.issid,
ixcon.ixcon.issuername,
ixcon.ixcon.securitydesc,
ixcon.ixcon.sectycd,
ixcon.ixcon.isin,
ixcon.ixcon.localcode,
ixcon.ixcon.onindex
from ixcon.ixcon
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
left outer join wca.scmst on ixcon.ixcon.secid = wca.scmst.secid
where
(ixcon.ixcon.ixnamid=14
or ixcon.ixcon.ixnamid=15
or ixcon.ixcon.ixnamid=24
or ixcon.ixcon.ixnamid=26
or ixcon.ixcon.ixnamid=46
or ixcon.ixcon.ixnamid=47
or ixcon.ixcon.ixnamid=60
or ixcon.ixcon.ixnamid=61
or ixcon.ixcon.ixnamid=70
or ixcon.ixcon.ixnamid=84
or ixcon.ixcon.ixnamid=85
or ixcon.ixcon.ixnamid=97
or ixcon.ixcon.ixnamid=222
or ixcon.ixcon.ixnamid=499
or ixcon.ixcon.ixnamid=500
or ixcon.ixcon.ixnamid=562
or ixcon.ixcon.ixnamid=631
or ixcon.ixcon.ixnamid=743
or ixcon.ixcon.ixnamid=760
or ixcon.ixcon.ixnamid=985
or ixcon.ixcon.ixnamid=1690
or ixcon.ixcon.ixnamid=2574
or ixcon.ixcon.ixnamid=82
or ixcon.ixcon.ixnamid=1
or ixcon.ixcon.ixnamid=2
or ixcon.ixcon.ixnamid=3
or ixcon.ixcon.ixnamid=6
or ixcon.ixcon.ixnamid=10
or ixcon.ixcon.ixnamid=11
or ixcon.ixcon.ixnamid=12
or ixcon.ixcon.ixnamid=13
or ixcon.ixcon.ixnamid=16
or ixcon.ixcon.ixnamid=17
or ixcon.ixcon.ixnamid=18
or ixcon.ixcon.ixnamid=20
or ixcon.ixcon.ixnamid=22
or ixcon.ixcon.ixnamid=25
or ixcon.ixcon.ixnamid=36
or ixcon.ixcon.ixnamid=37
or ixcon.ixcon.ixnamid=38
or ixcon.ixcon.ixnamid=39
or ixcon.ixcon.ixnamid=40
or ixcon.ixcon.ixnamid=41
or ixcon.ixcon.ixnamid=45
or ixcon.ixcon.ixnamid=48
or ixcon.ixcon.ixnamid=48
or ixcon.ixcon.ixnamid=53
or ixcon.ixcon.ixnamid=54
or ixcon.ixcon.ixnamid=55
or ixcon.ixcon.ixnamid=59
or ixcon.ixcon.ixnamid=62
or ixcon.ixcon.ixnamid=63
or ixcon.ixcon.ixnamid=66
or ixcon.ixcon.ixnamid=72
or ixcon.ixcon.ixnamid=73
or ixcon.ixcon.ixnamid=74
or ixcon.ixcon.ixnamid=75
or ixcon.ixcon.ixnamid=78
or ixcon.ixcon.ixnamid=79
or ixcon.ixcon.ixnamid=86
or ixcon.ixcon.ixnamid=90
or ixcon.ixcon.ixnamid=92
or ixcon.ixcon.ixnamid=93
or ixcon.ixcon.ixnamid=98
or ixcon.ixcon.ixnamid=110
or ixcon.ixcon.ixnamid=111
or ixcon.ixcon.ixnamid=116
or ixcon.ixcon.ixnamid=117
or ixcon.ixcon.ixnamid=126
or ixcon.ixcon.ixnamid=134
or ixcon.ixcon.ixnamid=139
or ixcon.ixcon.ixnamid=141
or ixcon.ixcon.ixnamid=149
or ixcon.ixcon.ixnamid=159
or ixcon.ixcon.ixnamid=160
or ixcon.ixcon.ixnamid=168
or ixcon.ixcon.ixnamid=169
or ixcon.ixcon.ixnamid=171
or ixcon.ixcon.ixnamid=180
or ixcon.ixcon.ixnamid=410
or ixcon.ixcon.ixnamid=412
or ixcon.ixcon.ixnamid=413
or ixcon.ixcon.ixnamid=450
or ixcon.ixcon.ixnamid=454
or ixcon.ixcon.ixnamid=457
or ixcon.ixcon.ixnamid=459
or ixcon.ixcon.ixnamid=463
or ixcon.ixcon.ixnamid=464
or ixcon.ixcon.ixnamid=468
or ixcon.ixcon.ixnamid=469
or ixcon.ixcon.ixnamid=477
or ixcon.ixcon.ixnamid=561
or ixcon.ixcon.ixnamid=741
or ixcon.ixcon.ixnamid=744
or ixcon.ixcon.ixnamid=801
or ixcon.ixcon.ixnamid=802
or ixcon.ixcon.ixnamid=835
or ixcon.ixcon.ixnamid=837
or ixcon.ixcon.ixnamid=845
or ixcon.ixcon.ixnamid=886
or ixcon.ixcon.ixnamid=927
or ixcon.ixcon.ixnamid=931
or ixcon.ixcon.ixnamid=974
or ixcon.ixcon.ixnamid=975
or ixcon.ixcon.ixnamid=976
or ixcon.ixcon.ixnamid=977
or ixcon.ixcon.ixnamid=978
or ixcon.ixcon.ixnamid=979
or ixcon.ixcon.ixnamid=980
or ixcon.ixcon.ixnamid=981
or ixcon.ixcon.ixnamid=982
or ixcon.ixcon.ixnamid=1137
or ixcon.ixcon.ixnamid=1245
or ixcon.ixcon.ixnamid=1251
or ixcon.ixcon.ixnamid=1259
or ixcon.ixcon.ixnamid=1286
or ixcon.ixcon.ixnamid=1415
or ixcon.ixcon.ixnamid=1679
or ixcon.ixcon.ixnamid=1692
or ixcon.ixcon.ixnamid=1777
or ixcon.ixcon.ixnamid=2613
or ixcon.ixcon.ixnamid=3356
or ixcon.ixcon.ixnamid=3531
or ixcon.ixcon.ixnamid=3940
or ixcon.ixcon.ixnamid=3942
or ixcon.ixcon.ixnamid=5322
or ixcon.ixcon.ixnamid=51
or ixcon.ixcon.ixnamid=43
or ixcon.ixcon.ixnamid=44)
and ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()))
order by ixnamid, issuername;