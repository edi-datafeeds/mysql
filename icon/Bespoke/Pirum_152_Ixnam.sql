--filepath=o:\datafeed\bespoke\pirum\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.152
--suffix=_IXNAM
--fileheadertext=EDI_IXNAM_152_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\bespoke\pirum\ixna\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1 
select
ixcon.ixnam.actflag,
ixcon.ixnam.acttime,
ixcon.ixnam.ixnamid,
ixcon.ixnam.exchgcd,
ixcon.ixnam.pricelocallink as pricecode1link,
ixcon.ixnam.priceisinlink as pricecode2link,
ixcon.ixnam.indexname,
case when substring(ixcon.ixnam.compositiondate,12,8) = '00:00:00'
then ixcon.ixnam.compositiondate
else ''
end as compositiondate,
ixcon.ixnam.pricenamelink,
ixcon.ixnam.indexisin,
ixcon.ixnam.cntrycd,
ixcon.ixnam.indexcurencd,
ixcon.ixnam.changefrequency,
ixcon.ixnam.changedetails
from ixcon.ixnam
where
(ixcon.ixnam.ixnamid=14
or ixcon.ixnam.ixnamid=15
or ixcon.ixnam.ixnamid=24
or ixcon.ixnam.ixnamid=26
or ixcon.ixnam.ixnamid=46
or ixcon.ixnam.ixnamid=47
or ixcon.ixnam.ixnamid=60
or ixcon.ixnam.ixnamid=61
or ixcon.ixnam.ixnamid=70
or ixcon.ixnam.ixnamid=84
or ixcon.ixnam.ixnamid=85
or ixcon.ixnam.ixnamid=97
or ixcon.ixnam.ixnamid=222
or ixcon.ixnam.ixnamid=499
or ixcon.ixnam.ixnamid=500
or ixcon.ixnam.ixnamid=562
or ixcon.ixnam.ixnamid=631
or ixcon.ixnam.ixnamid=743
or ixcon.ixnam.ixnamid=760
or ixcon.ixnam.ixnamid=985
or ixcon.ixnam.ixnamid=1690
or ixcon.ixnam.ixnamid=2574
or ixcon.ixnam.ixnamid=82
or ixcon.ixnam.ixnamid=1
or ixcon.ixnam.ixnamid=2
or ixcon.ixnam.ixnamid=3
or ixcon.ixnam.ixnamid=6
or ixcon.ixnam.ixnamid=10
or ixcon.ixnam.ixnamid=11
or ixcon.ixnam.ixnamid=12
or ixcon.ixnam.ixnamid=13
or ixcon.ixnam.ixnamid=16
or ixcon.ixnam.ixnamid=17
or ixcon.ixnam.ixnamid=18
or ixcon.ixnam.ixnamid=20
or ixcon.ixnam.ixnamid=22
or ixcon.ixnam.ixnamid=25
or ixcon.ixnam.ixnamid=36
or ixcon.ixnam.ixnamid=37
or ixcon.ixnam.ixnamid=38
or ixcon.ixnam.ixnamid=39
or ixcon.ixnam.ixnamid=40
or ixcon.ixnam.ixnamid=41
or ixcon.ixnam.ixnamid=45
or ixcon.ixnam.ixnamid=48
or ixcon.ixnam.ixnamid=48
or ixcon.ixnam.ixnamid=53
or ixcon.ixnam.ixnamid=54
or ixcon.ixnam.ixnamid=55
or ixcon.ixnam.ixnamid=59
or ixcon.ixnam.ixnamid=62
or ixcon.ixnam.ixnamid=63
or ixcon.ixnam.ixnamid=66
or ixcon.ixnam.ixnamid=72
or ixcon.ixnam.ixnamid=73
or ixcon.ixnam.ixnamid=74
or ixcon.ixnam.ixnamid=75
or ixcon.ixnam.ixnamid=78
or ixcon.ixnam.ixnamid=79
or ixcon.ixnam.ixnamid=86
or ixcon.ixnam.ixnamid=90
or ixcon.ixnam.ixnamid=92
or ixcon.ixnam.ixnamid=93
or ixcon.ixnam.ixnamid=98
or ixcon.ixnam.ixnamid=110
or ixcon.ixnam.ixnamid=111
or ixcon.ixnam.ixnamid=116
or ixcon.ixnam.ixnamid=117
or ixcon.ixnam.ixnamid=126
or ixcon.ixnam.ixnamid=134
or ixcon.ixnam.ixnamid=139
or ixcon.ixnam.ixnamid=141
or ixcon.ixnam.ixnamid=149
or ixcon.ixnam.ixnamid=159
or ixcon.ixnam.ixnamid=160
or ixcon.ixnam.ixnamid=168
or ixcon.ixnam.ixnamid=169
or ixcon.ixnam.ixnamid=171
or ixcon.ixnam.ixnamid=180
or ixcon.ixnam.ixnamid=410
or ixcon.ixnam.ixnamid=412
or ixcon.ixnam.ixnamid=413
or ixcon.ixnam.ixnamid=450
or ixcon.ixnam.ixnamid=454
or ixcon.ixnam.ixnamid=457
or ixcon.ixnam.ixnamid=459
or ixcon.ixnam.ixnamid=463
or ixcon.ixnam.ixnamid=464
or ixcon.ixnam.ixnamid=468
or ixcon.ixnam.ixnamid=469
or ixcon.ixnam.ixnamid=477
or ixcon.ixnam.ixnamid=561
or ixcon.ixnam.ixnamid=741
or ixcon.ixnam.ixnamid=744
or ixcon.ixnam.ixnamid=801
or ixcon.ixnam.ixnamid=802
or ixcon.ixnam.ixnamid=835
or ixcon.ixnam.ixnamid=837
or ixcon.ixnam.ixnamid=845
or ixcon.ixnam.ixnamid=886
or ixcon.ixnam.ixnamid=927
or ixcon.ixnam.ixnamid=931
or ixcon.ixnam.ixnamid=974
or ixcon.ixnam.ixnamid=975
or ixcon.ixnam.ixnamid=976
or ixcon.ixnam.ixnamid=977
or ixcon.ixnam.ixnamid=978
or ixcon.ixnam.ixnamid=979
or ixcon.ixnam.ixnamid=980
or ixcon.ixnam.ixnamid=981
or ixcon.ixnam.ixnamid=982
or ixcon.ixnam.ixnamid=1137
or ixcon.ixnam.ixnamid=1245
or ixcon.ixnam.ixnamid=1251
or ixcon.ixnam.ixnamid=1259
or ixcon.ixnam.ixnamid=1286
or ixcon.ixnam.ixnamid=1415
or ixcon.ixnam.ixnamid=1679
or ixcon.ixnam.ixnamid=1692
or ixcon.ixnam.ixnamid=1777
or ixcon.ixnam.ixnamid=2613
or ixcon.ixnam.ixnamid=3356
or ixcon.ixnam.ixnamid=3531
or ixcon.ixnam.ixnamid=3940
or ixcon.ixnam.ixnamid=3942
or ixcon.ixnam.ixnamid=5322
or ixcon.ixnam.ixnamid=51
or ixcon.ixnam.ixnamid=43
or ixcon.ixnam.ixnamid=44)
and ixcon.ixnam.actflag <> 'D'
order by ixnamid;