--filepath=o:\datafeed\bespoke\sentifi\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.152
--suffix=_IXNAM
--fileheadertext=EDI_IXNAM_152_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bespoke\sentifi\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1 
select
ixcon.ixnam.actflag,
ixcon.ixnam.acttime,
ixcon.ixnam.ixnamid,
ixcon.ixnam.exchgcd,
ixcon.ixnam.pricelocallink as pricecode1link,
ixcon.ixnam.priceisinlink as pricecode2link,
ixcon.ixnam.indexname,
case when substring(ixcon.ixnam.compositiondate,12,8) = '00:00:00'
then ixcon.ixnam.compositiondate
else ''
end as compositiondate,
ixcon.ixnam.pricenamelink,
ixcon.ixnam.indexisin,
ixcon.ixnam.cntrycd,
ixcon.ixnam.indexcurencd,
ixcon.ixnam.changefrequency,
ixcon.ixnam.changedetails
from ixcon.ixnam
where
(ixcon.ixnam.ixnamid=4
or ixcon.ixnam.ixnamid=16
or ixcon.ixnam.ixnamid=17
or ixcon.ixnam.ixnamid=18
or ixcon.ixnam.ixnamid=20
or ixcon.ixnam.ixnamid=59
or ixcon.ixnam.ixnamid=93
or ixcon.ixnam.ixnamid=83
or ixcon.ixnam.ixnamid=87
or ixcon.ixnam.ixnamid=88
or ixcon.ixnam.ixnamid=141
or ixcon.ixnam.ixnamid=151
or ixcon.ixnam.ixnamid=685
or ixcon.ixnam.ixnamid=511
or ixcon.ixnam.ixnamid=566
or ixcon.ixnam.ixnamid=611
or ixcon.ixnam.ixnamid=612
or ixcon.ixnam.ixnamid=618
or ixcon.ixnam.ixnamid=619
or ixcon.ixnam.ixnamid=620
or ixcon.ixnam.ixnamid=621
or ixcon.ixnam.ixnamid=622
or ixcon.ixnam.ixnamid=623
or ixcon.ixnam.ixnamid=624
or ixcon.ixnam.ixnamid=625
or ixcon.ixnam.ixnamid=627
or ixcon.ixnam.ixnamid=644
or ixcon.ixnam.ixnamid=645
or ixcon.ixnam.ixnamid=646
or ixcon.ixnam.ixnamid=647
or ixcon.ixnam.ixnamid=648
or ixcon.ixnam.ixnamid=649
or ixcon.ixnam.ixnamid=650
or ixcon.ixnam.ixnamid=651
or ixcon.ixnam.ixnamid=652
or ixcon.ixnam.ixnamid=654
or ixcon.ixnam.ixnamid=655
or ixcon.ixnam.ixnamid=656
or ixcon.ixnam.ixnamid=657
or ixcon.ixnam.ixnamid=665
or ixcon.ixnam.ixnamid=666
or ixcon.ixnam.ixnamid=667
or ixcon.ixnam.ixnamid=668
or ixcon.ixnam.ixnamid=669
or ixcon.ixnam.ixnamid=670
or ixcon.ixnam.ixnamid=671
or ixcon.ixnam.ixnamid=672
or ixcon.ixnam.ixnamid=673
or ixcon.ixnam.ixnamid=681
or ixcon.ixnam.ixnamid=682
or ixcon.ixnam.ixnamid=683
or ixcon.ixnam.ixnamid=684
or ixcon.ixnam.ixnamid=686
or ixcon.ixnam.ixnamid=687
or ixcon.ixnam.ixnamid=688
or ixcon.ixnam.ixnamid=689
or ixcon.ixnam.ixnamid=690
or ixcon.ixnam.ixnamid=691
or ixcon.ixnam.ixnamid=692
or ixcon.ixnam.ixnamid=693
or ixcon.ixnam.ixnamid=694
or ixcon.ixnam.ixnamid=695
or ixcon.ixnam.ixnamid=696
or ixcon.ixnam.ixnamid=697
or ixcon.ixnam.ixnamid=698
or ixcon.ixnam.ixnamid=699
or ixcon.ixnam.ixnamid=700
or ixcon.ixnam.ixnamid=701
or ixcon.ixnam.ixnamid=702
or ixcon.ixnam.ixnamid=703
or ixcon.ixnam.ixnamid=818
or ixcon.ixnam.ixnamid=931
or ixcon.ixnam.ixnamid=963
or ixcon.ixnam.ixnamid=969
or ixcon.ixnam.ixnamid=974
or ixcon.ixnam.ixnamid=975
or ixcon.ixnam.ixnamid=976
or ixcon.ixnam.ixnamid=977
or ixcon.ixnam.ixnamid=978
or ixcon.ixnam.ixnamid=979
or ixcon.ixnam.ixnamid=980
or ixcon.ixnam.ixnamid=981
or ixcon.ixnam.ixnamid=982
or ixcon.ixnam.ixnamid=1298
or ixcon.ixnam.ixnamid=1310
or ixcon.ixnam.ixnamid=1420
or ixcon.ixnam.ixnamid=1429
or ixcon.ixnam.ixnamid=1450
or ixcon.ixnam.ixnamid=1466
or ixcon.ixnam.ixnamid=1486
or ixcon.ixnam.ixnamid=1487
or ixcon.ixnam.ixnamid=1488
or ixcon.ixnam.ixnamid=1489
or ixcon.ixnam.ixnamid=1490
or ixcon.ixnam.ixnamid=1491
or ixcon.ixnam.ixnamid=1492
or ixcon.ixnam.ixnamid=1493
or ixcon.ixnam.ixnamid=1496
or ixcon.ixnam.ixnamid=1587
or ixcon.ixnam.ixnamid=1617
or ixcon.ixnam.ixnamid=1621
or ixcon.ixnam.ixnamid=1623
or ixcon.ixnam.ixnamid=1627
or ixcon.ixnam.ixnamid=1680
or ixcon.ixnam.ixnamid=1681
or ixcon.ixnam.ixnamid=2000
or ixcon.ixnam.ixnamid=2237
or ixcon.ixnam.ixnamid=2238
or ixcon.ixnam.ixnamid=2250
or ixcon.ixnam.ixnamid=2251
or ixcon.ixnam.ixnamid=2262
or ixcon.ixnam.ixnamid=2263
or ixcon.ixnam.ixnamid=2266
or ixcon.ixnam.ixnamid=2268
or ixcon.ixnam.ixnamid=2269
or ixcon.ixnam.ixnamid=2431
or ixcon.ixnam.ixnamid=2432
or ixcon.ixnam.ixnamid=2433
or ixcon.ixnam.ixnamid=2434
or ixcon.ixnam.ixnamid=2435
or ixcon.ixnam.ixnamid=2441
or ixcon.ixnam.ixnamid=2442
or ixcon.ixnam.ixnamid=2443
or ixcon.ixnam.ixnamid=2444
or ixcon.ixnam.ixnamid=2445
or ixcon.ixnam.ixnamid=2446
or ixcon.ixnam.ixnamid=2447
or ixcon.ixnam.ixnamid=2448
or ixcon.ixnam.ixnamid=2449
or ixcon.ixnam.ixnamid=2450
or ixcon.ixnam.ixnamid=2451
or ixcon.ixnam.ixnamid=2452
or ixcon.ixnam.ixnamid=2453
or ixcon.ixnam.ixnamid=2454
or ixcon.ixnam.ixnamid=2505
or ixcon.ixnam.ixnamid=2506
or ixcon.ixnam.ixnamid=2507
or ixcon.ixnam.ixnamid=2508
or ixcon.ixnam.ixnamid=2509
or ixcon.ixnam.ixnamid=2510
or ixcon.ixnam.ixnamid=2511
or ixcon.ixnam.ixnamid=2512
or ixcon.ixnam.ixnamid=2513
or ixcon.ixnam.ixnamid=2514
or ixcon.ixnam.ixnamid=2516
or ixcon.ixnam.ixnamid=2599
or ixcon.ixnam.ixnamid=2600
or ixcon.ixnam.ixnamid=2601
or ixcon.ixnam.ixnamid=2634
or ixcon.ixnam.ixnamid=2635)
and ixcon.ixnam.actflag <> 'D'
order by ixnamid;