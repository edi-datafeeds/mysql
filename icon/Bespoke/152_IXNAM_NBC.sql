-- arc=y
-- arp=n:\bespoke\nbc\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- fex=.152
-- fpx=
-- fsx=_IXNAM
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog), '%Y%m%d')
-- hpx=EDI_IXNAM_152_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1 
select
ixcon.ixnam.actflag,
ixcon.ixnam.acttime,
ixcon.ixnam.ixnamid,
ixcon.ixnam.exchgcd,
ixcon.ixnam.pricelocallink as pricecode1link,
ixcon.ixnam.priceisinlink as pricecode2link,
ixcon.ixnam.indexname,
case when substring(ixcon.ixnam.compositiondate,12,8) = '00:00:00'
then ixcon.ixnam.compositiondate
else ''
end as compositiondate,
ixcon.ixnam.pricenamelink,
ixcon.ixnam.indexisin,
ixcon.ixnam.cntrycd,
ixcon.ixnam.indexcurencd,
ixcon.ixnam.changefrequency,
ixcon.ixnam.changedetails
from ixcon.ixnam
where
(ixcon.ixnam.ixnamid=22
or ixcon.ixnam.ixnamid=24
or ixcon.ixnam.ixnamid=25
or ixcon.ixnam.ixnamid=26
or ixcon.ixnam.ixnamid=27
or ixcon.ixnam.ixnamid=45
or ixcon.ixnam.ixnamid=222
or ixcon.ixnam.ixnamid=495
or ixcon.ixnam.ixnamid=630
or ixcon.ixnam.ixnamid=631
or ixcon.ixnam.ixnamid=632
or ixcon.ixnam.ixnamid=664
or ixcon.ixnam.ixnamid=763)
and ixcon.ixnam.ixnamid>0
and ixcon.ixnam.actflag <> 'D'
order by ixnamid;