--filepath=o:\datafeed\bespoke\sentifi\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.152
--suffix=_IXCON
--fileheadertext=EDI_IXCON_152_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bespoke\sentifi\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
ixcon.ixcon.actflag,
ixcon.ixcon.acttime,
ixcon.ixcon.effectivedate,
ixcon.ixcon.ixnamid,
ixcon.ixcon.secid,
wca.scmst.issid,
ixcon.ixcon.issuername,
ixcon.ixcon.securitydesc,
ixcon.ixcon.sectycd,
ixcon.ixcon.isin,
ixcon.ixcon.localcode,
ixcon.ixcon.onindex
from ixcon.ixcon
inner join ixcon.ixnam on ixcon.ixcon.ixnamid = ixcon.ixnam.ixnamid
left outer join wca.scmst on ixcon.ixcon.secid = wca.scmst.secid
where
(ixcon.ixcon.ixnamid=4
or ixcon.ixcon.ixnamid=16
or ixcon.ixcon.ixnamid=17
or ixcon.ixcon.ixnamid=18
or ixcon.ixcon.ixnamid=20
or ixcon.ixcon.ixnamid=59
or ixcon.ixcon.ixnamid=93
or ixcon.ixcon.ixnamid=83
or ixcon.ixcon.ixnamid=87
or ixcon.ixcon.ixnamid=88
or ixcon.ixcon.ixnamid=141
or ixcon.ixcon.ixnamid=151
or ixcon.ixcon.ixnamid=685
or ixcon.ixcon.ixnamid=511
or ixcon.ixcon.ixnamid=566
or ixcon.ixcon.ixnamid=611
or ixcon.ixcon.ixnamid=612
or ixcon.ixcon.ixnamid=618
or ixcon.ixcon.ixnamid=619
or ixcon.ixcon.ixnamid=620
or ixcon.ixcon.ixnamid=621
or ixcon.ixcon.ixnamid=622
or ixcon.ixcon.ixnamid=623
or ixcon.ixcon.ixnamid=624
or ixcon.ixcon.ixnamid=625
or ixcon.ixcon.ixnamid=627
or ixcon.ixcon.ixnamid=644
or ixcon.ixcon.ixnamid=645
or ixcon.ixcon.ixnamid=646
or ixcon.ixcon.ixnamid=647
or ixcon.ixcon.ixnamid=648
or ixcon.ixcon.ixnamid=649
or ixcon.ixcon.ixnamid=650
or ixcon.ixcon.ixnamid=651
or ixcon.ixcon.ixnamid=652
or ixcon.ixcon.ixnamid=654
or ixcon.ixcon.ixnamid=655
or ixcon.ixcon.ixnamid=656
or ixcon.ixcon.ixnamid=657
or ixcon.ixcon.ixnamid=665
or ixcon.ixcon.ixnamid=666
or ixcon.ixcon.ixnamid=667
or ixcon.ixcon.ixnamid=668
or ixcon.ixcon.ixnamid=669
or ixcon.ixcon.ixnamid=670
or ixcon.ixcon.ixnamid=671
or ixcon.ixcon.ixnamid=672
or ixcon.ixcon.ixnamid=673
or ixcon.ixcon.ixnamid=681
or ixcon.ixcon.ixnamid=682
or ixcon.ixcon.ixnamid=683
or ixcon.ixcon.ixnamid=684
or ixcon.ixcon.ixnamid=686
or ixcon.ixcon.ixnamid=687
or ixcon.ixcon.ixnamid=688
or ixcon.ixcon.ixnamid=689
or ixcon.ixcon.ixnamid=690
or ixcon.ixcon.ixnamid=691
or ixcon.ixcon.ixnamid=692
or ixcon.ixcon.ixnamid=693
or ixcon.ixcon.ixnamid=694
or ixcon.ixcon.ixnamid=695
or ixcon.ixcon.ixnamid=696
or ixcon.ixcon.ixnamid=697
or ixcon.ixcon.ixnamid=698
or ixcon.ixcon.ixnamid=699
or ixcon.ixcon.ixnamid=700
or ixcon.ixcon.ixnamid=701
or ixcon.ixcon.ixnamid=702
or ixcon.ixcon.ixnamid=703
or ixcon.ixcon.ixnamid=818
or ixcon.ixcon.ixnamid=931
or ixcon.ixcon.ixnamid=963
or ixcon.ixcon.ixnamid=969
or ixcon.ixcon.ixnamid=974
or ixcon.ixcon.ixnamid=975
or ixcon.ixcon.ixnamid=976
or ixcon.ixcon.ixnamid=977
or ixcon.ixcon.ixnamid=978
or ixcon.ixcon.ixnamid=979
or ixcon.ixcon.ixnamid=980
or ixcon.ixcon.ixnamid=981
or ixcon.ixcon.ixnamid=982
or ixcon.ixcon.ixnamid=1298
or ixcon.ixcon.ixnamid=1310
or ixcon.ixcon.ixnamid=1420
or ixcon.ixcon.ixnamid=1429
or ixcon.ixcon.ixnamid=1450
or ixcon.ixcon.ixnamid=1466
or ixcon.ixcon.ixnamid=1486
or ixcon.ixcon.ixnamid=1487
or ixcon.ixcon.ixnamid=1488
or ixcon.ixcon.ixnamid=1489
or ixcon.ixcon.ixnamid=1490
or ixcon.ixcon.ixnamid=1491
or ixcon.ixcon.ixnamid=1492
or ixcon.ixcon.ixnamid=1493
or ixcon.ixcon.ixnamid=1496
or ixcon.ixcon.ixnamid=1587
or ixcon.ixcon.ixnamid=1617
or ixcon.ixcon.ixnamid=1621
or ixcon.ixcon.ixnamid=1623
or ixcon.ixcon.ixnamid=1627
or ixcon.ixcon.ixnamid=1680
or ixcon.ixcon.ixnamid=1681
or ixcon.ixcon.ixnamid=2000
or ixcon.ixcon.ixnamid=2237
or ixcon.ixcon.ixnamid=2238
or ixcon.ixcon.ixnamid=2250
or ixcon.ixcon.ixnamid=2251
or ixcon.ixcon.ixnamid=2262
or ixcon.ixcon.ixnamid=2263
or ixcon.ixcon.ixnamid=2266
or ixcon.ixcon.ixnamid=2268
or ixcon.ixcon.ixnamid=2269
or ixcon.ixcon.ixnamid=2431
or ixcon.ixcon.ixnamid=2432
or ixcon.ixcon.ixnamid=2433
or ixcon.ixcon.ixnamid=2434
or ixcon.ixcon.ixnamid=2435
or ixcon.ixcon.ixnamid=2441
or ixcon.ixcon.ixnamid=2442
or ixcon.ixcon.ixnamid=2443
or ixcon.ixcon.ixnamid=2444
or ixcon.ixcon.ixnamid=2445
or ixcon.ixcon.ixnamid=2446
or ixcon.ixcon.ixnamid=2447
or ixcon.ixcon.ixnamid=2448
or ixcon.ixcon.ixnamid=2449
or ixcon.ixcon.ixnamid=2450
or ixcon.ixcon.ixnamid=2451
or ixcon.ixcon.ixnamid=2452
or ixcon.ixcon.ixnamid=2453
or ixcon.ixcon.ixnamid=2454
or ixcon.ixcon.ixnamid=2505
or ixcon.ixcon.ixnamid=2506
or ixcon.ixcon.ixnamid=2507
or ixcon.ixcon.ixnamid=2508
or ixcon.ixcon.ixnamid=2509
or ixcon.ixcon.ixnamid=2510
or ixcon.ixcon.ixnamid=2511
or ixcon.ixcon.ixnamid=2512
or ixcon.ixcon.ixnamid=2513
or ixcon.ixcon.ixnamid=2514
or ixcon.ixcon.ixnamid=2516
or ixcon.ixcon.ixnamid=2599
or ixcon.ixcon.ixnamid=2600
or ixcon.ixcon.ixnamid=2601
or ixcon.ixcon.ixnamid=2634
or ixcon.ixcon.ixnamid=2635)
and ixcon.ixcon.onindex<>'F' and ixcon.ixcon.status='R'
and ixcon.ixnam.actflag <>'D'
and ((uploadstatus<>'D' and uploadstatus<>'A' and uploadstatus is not null)
or (uploadstatus='D' and effectivedate>now())
or (uploadstatus='A' and effectivedate<now()))
order by ixnamid, issuername;