--filepath=o:\datafeed\icon\151\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.151
--suffix=_IXCON
--fileheadertext=EDI_IXCON_151_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\icon\151\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select
icon.ixcon.actflag,
icon.ixcon.acttime,
icon.ixcon.ixnamid,
icon.ixcon.secid,
wca.scmst.issid,
icon.ixcon.issuername,
icon.ixcon.securitydesc,
icon.ixcon.sectycd,
icon.ixcon.isin,
icon.ixcon.localcode,
icon.ixcon.onindex
from icon.ixcon
inner join icon.ixnam on icon.ixcon.ixnamid = icon.ixnam.ixnamid
left outer join wca.scmst on icon.ixcon.secid = wca.scmst.secid
where
icon.ixcon.latest='T' and icon.ixcon.confirmed='T' and icon.ixcon.onindex='T'
and icon.ixnam.actflag <>'D'
and (icon.ixnam.ixnamid <77
or icon.ixnam.ixnamid=79
or icon.ixnam.ixnamid=90)
order by ixnamid, issuername