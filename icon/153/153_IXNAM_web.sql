--filepath=h:\s.bhuskute\ixcon\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.153
--suffix=_IXNAM
--fileheadertext=EDI_IXNAM_153_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\icon\153\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1 
select
ixcon.ixnam.actflag,
ixcon.ixnam.acttime,
ixcon.ixnam.ixnamid,
ixcon.ixnam.exchgcd,
ixcon.ixnam.pricelocallink as pricecode1link,
ixcon.ixnam.priceisinlink as pricecode2link,
ixcon.ixnam.indexname,
case when substring(ixcon.ixnam.compositiondate,12,8) = '00:00:00'
then ixcon.ixnam.compositiondate
else ''
end as compositiondate,
ixcon.ixnam.pricenamelink,
ixcon.ixnam.indexisin,
ixcon.ixnam.cntrycd,
ixcon.ixnam.indexcurencd,
ixcon.ixnam.changefrequency,
ixcon.ixnam.changedetails
from ixcon.ixnam
where
ixcon.ixnam.ixnamid>0
and ixcon.ixnam.actflag <> 'D'
order by ixnamid;