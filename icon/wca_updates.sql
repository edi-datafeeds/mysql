drop table icon.ixcon_temp;
create table icon.ixcon_temp like icon.ixcon;
insert into icon.ixcon_temp select icon.ixcon.* from icon.ixcon
inner join wca.scmst on icon.ixcon.secid = wca.scmst.secid
inner join wca.issur on icon.ixcon.issid = wca.issur.issid
inner join icon.ixnam on icon.ixcon.ixnamid = icon.ixnam.ixnamid
inner join wca.scexh on icon.ixcon.secid = wca.scexh.secid
           and icon.ixnam.exchgcd = wca.scexh.exchgcd
where
(icon.ixcon.isin <> wca.scmst.isin
or icon.ixcon.issuername <> wca.issur.issuername
or icon.ixcon.localcode <> wca.scexh.localcode
or icon.ixcon.securitydesc <> wca.scmst.securitydesc
or icon.ixcon.issid <> wca.scmst.issid
or icon.ixcon.sectycd <> wca.scmst.sectycd);
update icon.ixcon_temp
set changedby = 'wca',
acttime = now(),
actflag = 'U';
update icon.ixcon_temp
inner join wca.scmst on icon.ixcon_temp.secid = wca.scmst.secid
inner join wca.issur on icon.ixcon_temp.issid = wca.issur.issid
inner join icon.ixnam on icon.ixcon_temp.ixnamid = icon.ixnam.ixnamid
inner join wca.scexh on icon.ixcon_temp.secid = wca.scexh.secid
           and icon.ixnam.exchgcd = wca.scexh.exchgcd
set icon.ixcon_temp.isin =
    case when icon.ixcon_temp.isin <> wca.scmst.isin
         then wca.scmst.isin
         else icon.ixcon_temp.isin
end,
    icon.ixcon_temp.issuername =
    case when icon.ixcon_temp.issuername <> wca.issur.issuername
         then wca.issur.issuername
         else icon.ixcon_temp.issuername
end,
    icon.ixcon_temp.localcode =
    case when icon.ixcon_temp.localcode <> wca.scexh.localcode
         then wca.scexh.localcode
         else icon.ixcon_temp.localcode
end,
    icon.ixcon_temp.securitydesc =
    case when icon.ixcon_temp.securitydesc <> wca.scmst.securitydesc
         then wca.scmst.securitydesc
         else icon.ixcon_temp.securitydesc
end,
    icon.ixcon_temp.issid =
    case when icon.ixcon_temp.issid <> wca.scmst.issid
         then wca.scmst.issid
         else icon.ixcon_temp.issid
end,
    icon.ixcon_temp.sectycd =
    case when icon.ixcon_temp.sectycd <> wca.scmst.sectycd
         then wca.scmst.sectycd
         else icon.ixcon_temp.sectycd
end;
delete from icon.ixcon where secid in (select secid from icon.ixcon_temp where icon.ixcon_temp.ixnamid = icon.ixcon.ixnamid);
insert into icon.ixcon select * from icon.ixcon_temp;
