--filepath=o:\datafeed\icon\151\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.151
--suffix=_IXNAM
--fileheadertext=EDI_IXNAM_151_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\icon\151\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1 
select
icon.ixnam.actflag,
icon.ixnam.acttime,
icon.ixnam.ixnamid,
icon.ixnam.exchgcd,
icon.ixnam.pricelocallink as pricecode1link,
icon.ixnam.priceisinlink as pricecode2link,
icon.ixnam.indexname,
case when substring(icon.ixnam.compositiondate,12,8) = '00:00:00'
then icon.ixnam.compositiondate
else ''
end as compositiondate,
icon.ixnam.pricenamelink,
icon.ixnam.indexisin,
icon.ixnam.cntrycd,
icon.ixnam.exchgcd,
icon.ixnam.indexcurencd,
icon.ixnam.changefrequency,
icon.ixnam.changedetails
from icon.ixnam
where
icon.ixnam.ixnamid>0
and icon.ixnam.actflag <> 'D'
and (icon.ixnam.ixnamid <77
or icon.ixnam.ixnamid=79
or icon.ixnam.ixnamid=90)
