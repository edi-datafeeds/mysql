USE ixcon;
UPDATE ixnam as i 
INNER JOIN ixcon As R On i.IxNamID=R.IxNamID And R.`status`='R'
Set i.prevreleased=i.lastreleased;
UPDATE ixnam as i
INNER JOIN (select IxNamID, max(Acttime) as lastreleased  from ixcon As P where `status`='R' Group by  IxNamID) As u on i.IxNamID=u.IxNamID
set i.lastreleased= u.lastreleased;
