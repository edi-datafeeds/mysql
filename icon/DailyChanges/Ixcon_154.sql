-- arc=y
-- arp=n:\icon\changes\
-- ddt=yyyy/mm/dd
-- dfn=y
-- dfo=y
-- dft=n
-- dsp=select char(9)
-- dsn=n
-- dtm=hh:mm:ss
-- dzc=y
-- fdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- fex=.txt
-- fpx=
-- fsx=_154
-- fty=y
-- hdt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
-- hpx=EDI_Ixcon_Daily_Changes_
-- hsx=
-- eor=Select concat(char(13),char(10))
-- eof=EDI_ENDOFFILE

-- # 1
Select seq,ChangedBy,Acttime,'I' As Actflag,IxNamID,SecID,EffectiveDate,Latest,Confirmed,OnIndex,PrevOnIndex,IssuerName,SecurityDesc,SectyCD,
Isin,LocalCode,NameLink,IsinLink,LocalLink,SedolLink,LinkedOn,IssID,status
From(
select I.* from ixcon as I
INNER JOIN(select IxNamID,LastReleased as LastR  from ixnam) As L on I.IxNamID=L.IxNamID and I.Acttime=L.LastR
Where  I.SecID  NOT IN (
select Distinct I.SecID  from ixcon as I
INNER JOIN(select IxNamID,PrevReleased as PrevR  from ixnam) As P on I.IxNamID=P.IxNamID and I.Acttime=P.PrevR
ORDER BY I.SecID DESC)
) AS nw
where Acttime >CURRENT_DATE()
ORDER BY ixnamId ASC,NW.SecID ASC;

-- # 2
Select dl.seq,dl.ChangedBy,DATE_FORMAT(Now(),'%Y-%m-%d %H:%i:%s') as Acttime,'D' As Actflag,dl.IxNamID,dl.SecID,DATE_FORMAT(i.LastReleased,'%Y-%m-%d') As EffectiveDate,dl.Latest,dl.Confirmed,dl.OnIndex,dl.PrevOnIndex,dl.IssuerName,dl.SecurityDesc,dl.SectyCD,
dl.Isin,dl.LocalCode,dl.NameLink,dl.IsinLink,dl.LocalLink,dl.SedolLink,dl.LinkedOn,dl.IssID,dl.status
From(
select I.* from ixcon as I
INNER JOIN(select IxNamID,PrevReleased as PrevR  from ixnam) As P on I.IxNamID=P.IxNamID and I.Acttime=P.PrevR
Where  I.SecID  NOT IN (
select Distinct I.SecID  from ixcon as I
INNER JOIN(select IxNamID,LastReleased as LastR  from ixnam) As L on I.IxNamID=L.IxNamID and I.Acttime=L.LastR 
ORDER BY I.SecID DESC)
) AS dl
INNER JOIN ixnam as i on dl.IxNamID=i.IxNamID
where i.LastReleased >CURRENT_DATE()
ORDER BY ixnamId ASC,dl.SecID ASC;
